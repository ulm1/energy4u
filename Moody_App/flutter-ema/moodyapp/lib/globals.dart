library my_prj.globals;

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'Constant/sharedpreference_page.dart';


String text = "lightText";
final appColor = const Color(0xff015496); //blue
final appColorPink = const Color(0xffC72A61); //pink

class LightText extends StatelessWidget {
  final String text;
  final textColor;
  final textFontWeight;
  final textAlignment;

  LightText(
      {@required this.text,
      this.textColor,
      this.textFontWeight,
      this.textAlignment});

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      textAlign: textAlignment,
      style:
          TextStyle(fontSize: 15, color: textColor, fontWeight: textFontWeight),
    );
  }
}

class MediumText extends StatelessWidget {
  final String text;
  final textColor;
  final textFontWeight;
  final textAlignment;

  MediumText(
      {@required this.text,
      this.textColor,
      this.textFontWeight,
      this.textAlignment});

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      textAlign: textAlignment,
      style:
          TextStyle(fontSize: 20, color: textColor, fontWeight: textFontWeight),
    );
  }
}

class LargeText extends StatelessWidget {
  final String text;
  final textColor;
  final textFontWeight;
  final textAlignment;

  LargeText(
      {@required this.text,
      this.textColor,
      this.textFontWeight,
      this.textAlignment});

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      textAlign: textAlignment,
      style: TextStyle(
        fontSize: 30,
        color: textColor,
        fontWeight: textFontWeight,
      ),
    );
  }
}

SharedPreferences prefrenceObjects;
String peerId;
TextEditingController chatController = TextEditingController();
ScrollController listScrollController = ScrollController();
TextEditingController fileController = TextEditingController();
String peerName;
