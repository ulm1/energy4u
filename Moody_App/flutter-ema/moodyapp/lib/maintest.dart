import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:hive/hive.dart';
import 'package:provider/provider.dart';
import 'package:after_layout/after_layout.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:hive_flutter/hive_flutter.dart';

import 'data/Hive/screenHive.dart';

void main() async {
  await Hive.initFlutter();

  WidgetsFlutterBinding.ensureInitialized();
  // set orientation to portrait up

  Hive.registerAdapter(ScreenTimeAdapter());
  await Hive.openBox<ScreenTime>('screenHive');

  await SystemChrome.setPreferredOrientations(
    [DeviceOrientation.portraitUp],
  );
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // create an instance of `RouteObserver`
  final RouteObserver<PageRoute> _routeObserver = RouteObserver();

  @override
  Widget build(BuildContext context) {
    return Provider.value(
      // provide the `RouteObserver` to subtree widgets through `context`
      value: _routeObserver,
      child: MaterialApp(
        title: 'Flutter Demo',
        // set route observer for the `Navigator`
        navigatorObservers: [_routeObserver],
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        routes: routes,
      ),
    );
  }
}
abstract class RouteAwareState<T extends StatefulWidget> extends State<T>
    with RouteAware,WidgetsBindingObserver, AfterLayoutMixin<T> {
  RouteObserver<PageRoute> routeObserver;
  bool enteredScreen = false;
  String event = "";
  Box contactBox;
  var screenTime;

  // use `afterFirstLayout()`, because we should wait for
  // the `initState() completed before getting objects from `context`
  @override
  // add @mustCallSuper annotation to prevent being overridden
  @mustCallSuper
  void afterFirstLayout(BuildContext context) {
    if (mounted) {
      // get the instance of `RouteObserver` from `context`
      routeObserver = context.read<RouteObserver<PageRoute>>();
      // subscribe for the change of route
      routeObserver?.subscribe(this, ModalRoute.of(context) as PageRoute);
      // execute asynchronously as soon as possible
      Timer.run(_enterScreen);
    }
  }
  var preference;
  @override
  initState() async {
    WidgetsBinding.instance.addObserver(this);
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
    });

    contactBox =  Hive.box<ScreenTime>('screenHive');
    screenTime = ScreenTime();



    screenTime.endTime = DateTime.now().toString();
    screenTime.endEvent = event;
    contactBox.add(screenTime);



    SharedPreferences.getInstance().then((pref) {
      setState(() {
        event = pref.getString('event')?? "";

      });
    });

    super.initState();
  }


  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    print('state = $state');

    preference = await SharedPreferences.getInstance();

    preference.setString('event', "entered");
    print("entered");


    if(state== AppLifecycleState.resumed){
      enteredScreen = true;
      preference.setString('event', "resumed");
      print("resumed");

      screenTime.startTime = DateTime.now().toString();
      screenTime.startEvent = event;
    }

    if(state== AppLifecycleState.paused){
      _leaveScreen();       preference.setString('event', "paused");
      print("paused");

      screenTime.endTime = DateTime.now().toString();
      screenTime.endEvent = event;
      contactBox.add(screenTime);

    }
    if(state== AppLifecycleState.inactive){
      _leaveScreen();       preference.setString('event', "inactive");
      print("inactive");

      screenTime.endTime = DateTime.now().toString();
      screenTime.endEvent = event;
      contactBox.add(screenTime);
    }
    if(state== AppLifecycleState.detached){
      _leaveScreen();       preference.setString('event', "detached");
      print("detached");

      screenTime.endTime = DateTime.now().toString();
      screenTime.endEvent = event;
      contactBox.add(screenTime);

    }

  }

  void _enterScreen() {
    onEnterScreen();
    enteredScreen = true;
  }

  void _leaveScreen() {
    onLeaveScreen();
    enteredScreen = false;
  }

  @override
  @mustCallSuper
  void dispose() {
    if (enteredScreen) {
      _leaveScreen();
    }
    routeObserver?.unsubscribe(this);
    WidgetsBinding.instance.removeObserver(this);

    super.dispose();
  }

  @override
  @mustCallSuper
  void didPopNext() {
    print("didPopNext");
    screenTime.screenName = "Settings Screen";
    screenTime.startTime = DateTime.now().toString();
    screenTime.startEvent = event;

    Timer.run(_enterScreen);
  }

  @override
  @mustCallSuper
  void didPop() {
    print("didPop");
    screenTime.endTime = DateTime.now().toString();
    screenTime.endEvent = event;
    contactBox.add(screenTime);
    _leaveScreen();
  }

  @override
  @mustCallSuper
  void didPushNext() {
    print("didPushNext");
    screenTime.endTime = DateTime.now().toString();
    screenTime.endEvent = event;
    contactBox.add(screenTime);
    _leaveScreen();
  }

  /// this method will always be executed on enter this screen
  void onEnterScreen();

  /// this method will always be executed on leaving this screen
  void onLeaveScreen();

}






final Map<String, Widget Function(BuildContext)> routes = {
  '/': (_) => HomeScreen(),
  '/chart-screen': (_) => ChartScreen(),
  '/settings-screen': (_) => SettingsScreen(),
};

class ChartScreen extends StatefulWidget {
  const ChartScreen();

  @override
  _ChartScreenState createState() => _ChartScreenState();
}

class _ChartScreenState extends RouteAwareState<ChartScreen> {
  @override
  void onEnterScreen() async {
    print('on enter chart screen');
    // set orientation to landscape
    await SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight,
    ]);
  }

  @override
  void onLeaveScreen() async {
    print('on leave chart screen');
    // set orientation back to portrait
    await SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            onPressed: () {
              Navigator.of(context).pushNamed('/settings-screen');
            },
            icon: Icon(Icons.settings),
          ),
        ],
      ),
      body: SafeArea(
        child: Stack(
          children: [
            Center(
              child: Text('Chart Screen'),
            ),
          ],
        ),
      ),
    );
  }
}
class HomeScreen extends StatelessWidget {
  const HomeScreen();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: 15.0,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              TextButton(
                style: TextButton.styleFrom(
                  backgroundColor: Theme.of(context).primaryColor,
                  primary: Colors.white,
                ),
                onPressed: () {
                  Navigator.of(context).pushNamed('/chart-screen');
                },
                child: Text('Chart Screen'),
              ),
              const SizedBox(height: 10.0),
              TextButton(
                style: TextButton.styleFrom(
                  backgroundColor: Theme.of(context).primaryColor,
                  primary: Colors.white,
                ),
                onPressed: () {

                  Future.delayed(Duration.zero, () {
                    Navigator.of(context).pushNamed('/settings-screen');
                  });

                },
                child: Text('Settings Screen'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}


class SettingsScreen extends StatefulWidget {
  const SettingsScreen();

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends RouteAwareState<SettingsScreen> {

  @override
  initState()  {
    contactBox =  Hive.box<ScreenTime>('screenHive');
    super.initState();





  }
  _deleteInfo(int index) {
    contactBox.deleteAt(index);
    print('Item deleted from box at index: $index');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body:ValueListenableBuilder(
        valueListenable: contactBox.listenable(),
        builder: (context, Box box, widget) {
          if (box.isEmpty) {
            return Center(
              child: Text('Empty'),
            );
          } else {
            return ListView.builder(
              itemCount: box.length,
              itemBuilder: (context, index) {
                var currentBox = box;
                var personData = currentBox.getAt(index);
                return InkWell(

                  child: ListTile(
                    title: Text(personData.screenName.toString() + personData.startEvent.toString()),
                    subtitle: Text(personData.startTime.toString()   + personData.endTime.toString()),
                    trailing: IconButton(
                      onPressed: () => _deleteInfo(index),
                      icon: Icon(
                        Icons.delete,
                        color: Colors.red,
                      ),
                    ),

                  ),
                );
              },
            );
          }
        },
      ),
    );
  }

  @override
  void onEnterScreen() {



    print("Entered");

  }

  @override
  void onLeaveScreen() {



    print("left");

    print(contactBox.get(0));


  }
}
