
import 'package:hive/hive.dart';

import 'QUESTIONNAIRE/models/question_model.dart';

class Boxes {
  static Box<QuestionnaireModel> getQuestionnaire() =>
      Hive.box<QuestionnaireModel>('question_model');
}