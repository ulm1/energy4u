import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:moodyapp/widgets/network_status_service.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:provider/provider.dart' as p;

import 'Helper/CommonWidgets/CommonWidgets.dart';
import 'Helper/Constant.dart';
import 'Localization/t_key.dart';
import 'network_aware_widget.dart';

class TcPage extends StatefulWidget {
  _WelcomePage createState() => _WelcomePage();
}

class _WelcomePage extends State<TcPage> {
  WebViewController _controller;
  bool privacy = false;
  bool license = false;
  bool home = false;
  bool vis = false;
  String text =  '';


  @override
  Widget build(BuildContext context) {

    if (privacy == false && license == false) {
      setState(() {
        text = TKeys.termstitle.translate(context);
      });
    }

    return Scaffold(
      appBar: new AppBar(
        centerTitle: true,
        // title: setHeaderTitle(AppTranslations.of(context).text(AppTitle.appTitle),Colors.white),
        title: setHeaderTitle(
            text,
            Colors.white),
        backgroundColor: AppColor.themeColor,
        elevation: 1.0,
        leading: new IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.white),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      body: Container(
        child:p.StreamProvider<NetworkStatus>(
          create: (context) =>
          NetworkStatusService().networkStatusController.stream,
          child: NetworkAwareWidget(
            onlineChild:  Container(
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                      child: WebView(
                        initialUrl: 'https://www.dynamic-project.de/terms/',
                        javascriptMode: JavascriptMode.unrestricted,
                        onWebViewCreated: (WebViewController webViewController) {
                          // Get reference to WebView controller to access it globally
                          _controller = webViewController;

                        },
                        javascriptChannels: <JavascriptChannel>[
                          // Set Javascript Channel to WebView
                          _extractDataJSChannel(context),
                        ].toSet(),
                        onPageStarted: (String url) {
                          print('Page started loading: $url');
                        },
                        onPageFinished: (String url) {
                          print('Page finished loading: $url');
                          // In the final result page we check the url to make sure  it is the last page.
                          if (url.contains('/finalresponse.html')) {

                            // ignore: deprecated_member_use
                            _controller.evaluateJavascript(
                                "(function(){Flutter.postMessage(window.document.body.outerHTML)})();");
                          }
                        },
                      ),
                    ),
                    FloatingActionButton(
                      onPressed: () async {

                        Navigator.of(context).pop();

                      },
                      child: Icon(Icons.check),
                    )
                  ],
                ),
              ),
            ),
            offlineChild: Container(
              child: Center(
                child: Text(
                  TKeys.Internet.translate(context),
                  style: TextStyle(
                      color: Colors.grey[400],
                      fontWeight: FontWeight.w600,
                      fontSize: 20.0),
                ),
              ),
            ),
          ),
        ),
      ),

    );
  }

  JavascriptChannel _extractDataJSChannel(BuildContext context) {
    return JavascriptChannel(
      name: 'Flutter',
      onMessageReceived: (JavascriptMessage message) {
        print(message);
        String pageBody = message.message;
        print('page body: $pageBody');
      },
    );
  }
}

class LPage extends StatefulWidget {
  _LPage createState() => _LPage();
}

class _LPage extends State<LPage> {
  WebViewController _controller;
  bool privacy = false;
  bool license = false;
  bool home = false;
  bool vis = false;
  String text =  '';


  @override
  Widget build(BuildContext context) {

      setState(() {
        text = TKeys.Licenses.translate(context);
      });


    return Scaffold(
      appBar: new AppBar(
        centerTitle: true,
        // title: setHeaderTitle(AppTranslations.of(context).text(AppTitle.appTitle),Colors.white),
        title: setHeaderTitle(
            text,
            Colors.white),
        backgroundColor: AppColor.themeColor,
        elevation: 1.0,
        leading: new IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.white),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ),


      body: Container(
        child:p.StreamProvider<NetworkStatus>(
          create: (context) =>
          NetworkStatusService().networkStatusController.stream,
          child: NetworkAwareWidget(
            onlineChild:  Container(
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                      child: WebView(
                        initialUrl: 'https://www.dynamic-project.de/licenses/',
                        javascriptMode: JavascriptMode.unrestricted,
                        onWebViewCreated: (WebViewController webViewController) {
                          // Get reference to WebView controller to access it globally
                          _controller = webViewController;

                        },
                        javascriptChannels: <JavascriptChannel>[
                          // Set Javascript Channel to WebView
                          _extractDataJSChannel(context),
                        ].toSet(),
                        onPageStarted: (String url) {
                          print('Page started loading: $url');
                        },
                        onPageFinished: (String url) {
                          print('Page finished loading: $url');
                          // In the final result page we check the url to make sure  it is the last page.
                          if (url.contains('/finalresponse.html')) {

                            // ignore: deprecated_member_use
                            _controller.evaluateJavascript(
                                "(function(){Flutter.postMessage(window.document.body.outerHTML)})();");
                          }
                        },
                      ),
                    ),
                    FloatingActionButton(
                      onPressed: () async {

                        Navigator.of(context).pop();

                      },
                      child: Icon(Icons.check),
                    )
                  ],
                ),
              ),
            ),
            offlineChild: Container(
              child: Center(
                child: Text(
                  TKeys.Internet.translate(context),
                  style: TextStyle(
                      color: Colors.grey[400],
                      fontWeight: FontWeight.w600,
                      fontSize: 20.0),
                ),
              ),
            ),
          ),
        ),
      ),

    );
  }

  JavascriptChannel _extractDataJSChannel(BuildContext context) {
    return JavascriptChannel(
      name: 'Flutter',
      onMessageReceived: (JavascriptMessage message) {
        print(message);
        String pageBody = message.message;
        print('page body: $pageBody');
      },
    );
  }
}
class PPage extends StatefulWidget {
  _PPage createState() => _PPage();
}

class _PPage extends State<PPage> {
  WebViewController _controller;
  bool privacy = false;
  bool license = false;
  bool home = false;
  bool vis = false;
  String text =  '';


  @override
  Widget build(BuildContext context) {

      setState(() {
        text = TKeys.Privacy.translate(context);
      });


    return Scaffold(
      appBar: new AppBar(
        centerTitle: true,
        // title: setHeaderTitle(AppTranslations.of(context).text(AppTitle.appTitle),Colors.white),
        title: setHeaderTitle(
            text,
            Colors.white),
        backgroundColor: AppColor.themeColor,
        elevation: 1.0,
        leading: new IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.white),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      body: Container(
        child:p.StreamProvider<NetworkStatus>(
          create: (context) =>
          NetworkStatusService().networkStatusController.stream,
          child: NetworkAwareWidget(
            onlineChild:  Container(
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                      child: WebView(
                        initialUrl: 'https://www.dynamic-project.de/privacy/',
                        javascriptMode: JavascriptMode.unrestricted,
                        onWebViewCreated: (WebViewController webViewController) {
                          // Get reference to WebView controller to access it globally
                          _controller = webViewController;

                        },
                        javascriptChannels: <JavascriptChannel>[
                          // Set Javascript Channel to WebView
                          _extractDataJSChannel(context),
                        ].toSet(),
                        onPageStarted: (String url) {
                          print('Page started loading: $url');
                        },
                        onPageFinished: (String url) {
                          print('Page finished loading: $url');
                          // In the final result page we check the url to make sure  it is the last page.
                          if (url.contains('/finalresponse.html')) {

                            // ignore: deprecated_member_use
                            _controller.evaluateJavascript(
                                "(function(){Flutter.postMessage(window.document.body.outerHTML)})();");
                          }
                        },
                      ),
                    ),
                    FloatingActionButton(
                      onPressed: () async {

                        Navigator.of(context).pop();

                      },
                      child: Icon(Icons.check),
                    )
                  ],
                ),
              ),
            ),
            offlineChild: Container(
              child: Center(
                child: Text(
                  TKeys.Internet.translate(context),
                  style: TextStyle(
                      color: Colors.grey[400],
                      fontWeight: FontWeight.w600,
                      fontSize: 20.0),
                ),
              ),
            ),
          ),
        ),
      ),

    );
  }

  JavascriptChannel _extractDataJSChannel(BuildContext context) {
    return JavascriptChannel(
      name: 'Flutter',
      onMessageReceived: (JavascriptMessage message) {
        print(message);
        String pageBody = message.message;
        print('page body: $pageBody');
      },
    );
  }
}