import 'package:flutter/material.dart';
import 'package:moodyapp/static/htimes.dart';

class Cigaratte {
  final int  points;
  final double pricePerCigaratte;
  final int dailyCigarattes;
  final String lang;
  final String dat;


  double cigarattePerSecond;
  double moneyPerSecond;
  double toUp = 0;

  Cigaratte(
      {this.points,
      this.pricePerCigaratte,
      this.dailyCigarattes,
      this.lang,this.dat}) {
    cigarattePerSecond = dailyCigarattes / (24 * 60 * 60.0);
    moneyPerSecond = cigarattePerSecond * pricePerCigaratte;
  }

  int calculatePassedTime() {
    var questionnaires = [1,2, 10, 15,20,30,35,40,45,50,55,60,65,80];

    var completed = points / 5;

    var i = questionnaires.length - 1;
    while ( i >=0 ) {
      if (completed > questionnaires[i]) {

       // level = i+1;
        break;
      }
      i--;
    }
    var left =questionnaires[i+1]  - completed ;


    DateTime now = DateTime.now();
    return completed.round();
  }


  Duration calculatePassedT() {
    DateTime now = DateTime.now();
    print(dat);

    print(now.difference(DateTime.parse(dat)));

    return now.difference(DateTime.parse(dat));
  }

  double get getSavedMoney {
    return  moneyPerSecond;
  }



  double get getsavedCigarattes {
    return cigarattePerSecond;
  }

  double get getdayPercentage {
    final now = DateTime.now();
    //DateTime midnight = startDate.add(Duration(days: calculatePassedTime().inDays + 1));
    return 100;
  }

  Map get upcomingEvent {
    int maduration = calculatePassedTime();
    Map event;
    for (Map element in htimes) {
      if (element["time"] > maduration) {
        event = element;
        return event;
      }
    }
    return {
      "id": "15",
      "time": Duration(days: 365 * 100),
    };
  }

  List<int> get generateDayItem {
    List<int> daylist = [];

    for (int i = calculatePassedTime() - 2;
        i < calculatePassedTime() + 5;
        i++) {
      if (i > 0) daylist.add(i);
    }
    for (int i = calculatePassedTime() - 3; i < 0; i++) {
      daylist.add(i + 8);
    }

    return daylist;
  }
}

/* void main() {
  Cigaratte cigara = Cigaratte(
      dailyCigarattes: 20,
      pricePerCigaratte: 12,
      startDate: DateTime.now().subtract(Duration(days: 0, seconds: 500)));
  print(cigara.upcomingEvent);
}
 */
