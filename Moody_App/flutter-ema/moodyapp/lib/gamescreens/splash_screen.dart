import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:moodyapp/Helper/SharedManager.dart';
import 'package:moodyapp/Localization/localization_service.dart';
import 'package:moodyapp/gamescreens/home_screen.dart';
import 'package:moodyapp/gamescreens/welcome_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  SplashScreen({Key key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final localizationController = Get.find<LocalizationController>();

  _donavigate() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    if (pref.getString("startTime") != null) {
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (BuildContext context) => HomeScreen()));
    } else {
      Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (BuildContext context) => WelcomeScreen()));
    }
  }

  @override
  void initState() {
    super.initState();
    _donavigate();
  }

  @override
  Widget build(BuildContext context) {
    return  GetBuilder<LocalizationController>(
        init: localizationController,
        builder: (LocalizationController controller){
          return MaterialApp(
            debugShowCheckedModeBanner:false,
            home: new Text(""
            ),

            theme: SharedManager.shared.getThemeType(),
            locale: controller.currentLanguage != ''
                ? Locale(controller.currentLanguage, '')
                : null,
            localeResolutionCallback: LocalizationService.localeResolutionCallBack,
            supportedLocales: LocalizationService.supportedLocales,
            localizationsDelegates: LocalizationService.localizationsDelegate,
          );
        }
    );
  }
}
