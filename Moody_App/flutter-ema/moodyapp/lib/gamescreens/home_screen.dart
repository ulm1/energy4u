import 'dart:async';
import 'dart:math';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:intl/intl.dart';
import 'package:moodyapp/Helper/CommonWidgets/CommonWidgets.dart';
import 'package:moodyapp/Helper/Constant.dart';
import 'package:moodyapp/Localization/localization_service.dart';
import 'package:moodyapp/Screens/TabBarScreens/Tabbar/Tabbar.dart';
import 'package:moodyapp/Screens/contanst/contanst.dart';
import 'package:moodyapp/comps/cigaratte.dart';
import 'package:moodyapp/comps/getlang.dart';
import 'package:moodyapp/comps/particlePainer.dart';
import 'package:moodyapp/comps/progress_painter.dart';
import 'package:moodyapp/constants.dart';
import 'package:moodyapp/gamescreens/guide_screen.dart';
import 'package:moodyapp/gamescreens/progress_screen.dart';
import 'package:moodyapp/gamescreens/reason_screen.dart';
import 'package:moodyapp/gamescreens/settings_screen.dart';
import 'package:moodyapp/gamescreens/wallet_screen.dart';
import 'package:moodyapp/size_config.dart';
import 'package:moodyapp/static/lang.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../Localization/t_key.dart';
import 'guideview_screen.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  Cigaratte cigaraManager;
  String lang = "";
  String currency = "";
  String tiptext = "";
  int points = 0;
  String datedd = "";
  var level = 1;



  int _testValue;
  Future<void> loadData() async {
    SharedPreferences pref = await SharedPreferences.getInstance();

    pref.setString("startTime",DateTime.now().toString());

    SharedPreferences.getInstance().then((SharedPreferences sp) {
      pref = sp;
      _testValue =  pref.getInt("dailycigarattes");
      // will be null if never previously saved
      if (_testValue == null) {
        _testValue = 0;
        pref.setDouble("pricePerCigaratte", 0);
        pref.setInt("dailycigarattes", 0);
        pref.setString("startTime",DateTime.now().toString());
        pref.setInt("points",0);
        pref.setInt("completedQuestionnaires",0);
        pref.setString("QuestTime","");
        pref.setString("level","");

        print(_testValue);
print(_testValue);

      }
      setState(() {});
    });




    datedd = pref.getString("QuestTime");

    currency = pref.getString("currency");
    points = pref.getInt("points");



    cigaraManager = Cigaratte(
        dailyCigarattes: pref.getInt("dailycigarattes"),
        pricePerCigaratte: pref.getDouble("pricePerCigaratte"),
        points: points,
      dat: datedd
    );
  }

  int lastRndInt = -1;
  @override
  void initState() {
    loadData();
    lang = getLang();

    super.initState();
    Timer.periodic(Duration(seconds: 1), (timer) {
      if (mounted) {
        setState(() {});
      }    });
    Random rnd = new Random();
    int rndInt = rnd.nextInt(langs[lang]["tipsandfacts"].length);
    while (rndInt == lastRndInt)
      rndInt = rnd.nextInt(langs[lang]["tipsandfacts"].length);
    lastRndInt = rndInt;
    tiptext = langs[lang]["tipsandfacts"][rndInt];
    tipOpacity = 1;
    Timer(Duration(seconds: 8), () {
      tipOpacity = 0;
      setState(() {});
    });
    Timer.periodic(Duration(seconds: 10), (timer) {
      tipOpacity = 1;
      tiptext = langs[lang]["tipsandfacts"]
          [rnd.nextInt(langs[lang]["tipsandfacts"].length)];

      if (mounted) {
        setState(() {});
      }
      Timer(Duration(seconds: 8), () {
        tipOpacity = 0;
        if (mounted) {
          setState(() {});
        }      });
    });
  }

  showAlertDialog(BuildContext context) {
    Widget cancelButton = FlatButton(
      child: Text(
        "${langs[lang]["home"]["cancel"]}",
      ),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    Widget helpButton = FlatButton(
      child: Text(
        "${langs[lang]["guide"]["ifyouslip"]["title"]}",
      ),
      onPressed: () {
        Navigator.of(context).pop();
        Navigator.push(context, MaterialPageRoute(builder: (_) {
          return GuideViewScreen(id: "ifyouslip", lang: lang);
        }));
      },
    );
    Widget helpButton2 = FlatButton(
      child: Text(
        "${langs[lang]["guide"]["managecravings"]["title"]}",
      ),
      onPressed: () {
        Navigator.of(context).pop();
        Navigator.push(context, MaterialPageRoute(builder: (_) {
          return GuideViewScreen(id: "managecravings", lang: lang);
        }));
      },
    );
    Widget continueButton = FlatButton(
      child: Text(
        "${langs[lang]["home"]["reset"]}",
        style: TextStyle(color: Colors.red),
      ),
      onPressed: () async {
        Navigator.of(context).pop();
        SharedPreferences pref = await SharedPreferences.getInstance();
        pref.setString("startTime", DateTime.now().toIso8601String());
        loadData();
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("${langs[lang]["home"]["resetall"]}"),
      content: Text("${langs[lang]["home"]["resetallq"]}"),
      actions: [
        continueButton,
        helpButton,
        helpButton2,
        cancelButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  double tipOpacity = 0;
  final localizationController = Get.put(LocalizationController());

  @override
  Widget build(BuildContext context) {

    var levels = [1,100, 200, 300,400,500,600,700,800,900,1000];
    var questionnaires = [1,2, 10, 15,20,30,35,40,45,50,55,60,65,80];



   print("hello"+ level.toString());


    var i = levels.length - 1;
    while ( i >=0 ) {
      if (points > levels[i]) {
        level = i+1;
        break;
      }
      i--;
    }
    print("hello"+ level.toString());

    var completedPercentage = points % 100;



    if (cigaraManager == null) return Text("");
    Size sc = MediaQuery.of(context).size;
    SizeConfig().init(context);
    return GetBuilder<LocalizationController>(
        init: localizationController,
        builder: (LocalizationController controller){
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          home: Scaffold(
            appBar: buildAppBar(context),
            body: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: getProportionateScreenHeight(20),
                ),
                SizedBox(
                  height: getProportionateScreenHeight(60),
                ),
                AspectRatio(
                  aspectRatio: 2.2,
                  child: Stack(
                    children: [


                      ParticlePainterWidget(
                        widgetSize: MediaQuery.of(context).size,
                      ),
                      CustomPaint(
                        painter: ProgressPainter(
                          completedPercentage: completedPercentage,
                          circleWidth: 15,
                          defaultCircleColor: Colors.blue,
                          percentageCompletedCircleColor:
                              Colors.grey.withOpacity(0.2),
                        ),
                        child: Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                '${points }'+ ' pts',
                                style: TextStyle(
                                    fontSize: getProportionateScreenWidth(52),
                                    fontWeight: FontWeight.bold,
                                    shadows: [
                                      Shadow(
                                          color: kShadowColor.withOpacity(.2),
                                          blurRadius: 32)
                                    ]),
                              ),
                              Text(
                                '${langs[lang]["home"]["day"].toUpperCase()} ${ level}',
                                style: TextStyle(
                                  fontSize: getProportionateScreenWidth(26),
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 50),
                  child: AnimatedOpacity(
                    duration: Duration(milliseconds: 1000),
                    opacity: tipOpacity,
                    child: FittedBox(
                      child: Text(
                        tiptext,
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: getProportionateScreenWidth(14)),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: getProportionateScreenHeight(15),
                ),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        width: getProportionateScreenWidth(15),
                      ),

                      SizedBox(
                        width: getProportionateScreenWidth(15),
                      ),
                      buildCategoryCard(sc, context,
                          color: Colors.blue,
                          icon: Icons.timelapse,
                          text: "${langs[lang]["home"]["progress"]}",
                          id: "progress", onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (_) {
                          return ProgressPage(
                            cigaratteManager: cigaraManager,
                            points: points,
                          );
                        }));
                      }),
                      SizedBox(
                        width: getProportionateScreenWidth(15),
                      ),

                      SizedBox(
                        width: getProportionateScreenWidth(15),
                      ),

                      SizedBox(
                        width: getProportionateScreenWidth(15),
                      ),

                      SizedBox(
                        width: getProportionateScreenWidth(15),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: getProportionateScreenHeight(5),
                ),

                Text(
                  "${langs[lang]["home"]["achievement"]}",
                  textAlign: TextAlign.center,
                  style: Theme.of(context)
                      .textTheme
                      .bodyText2
                      .copyWith(fontSize: getProportionateScreenWidth(18)),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: AutoSizeText(
                    "${langs[lang]["progressDescription"][cigaraManager.upcomingEvent["id"]]}",
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.bodyText1,
                    maxLines: 2,
                  ),
                ),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [
                      datedd.isNotEmpty?buildInfoCard(
                          context,
                          "${cigaraManager.calculatePassedT().inDays} ${langs[lang]["home"]["da"].toLowerCase()}, ${cigaraManager.calculatePassedT().inHours % 24} ${langs[lang]["home"]["hour"].toLowerCase()}, ${cigaraManager.calculatePassedT().inMinutes % 60} ${langs[lang]["home"]["minute"].toLowerCase()}, ${cigaraManager.calculatePassedT().inSeconds % 60} ${langs[lang]["home"]["second"].toLowerCase()}",
                          "${langs[lang]["home"]["timePassed"]}",
                          Icons.timer,
                          Colors.teal):Container(),

                    ],
                  ),
                ),
                Expanded(child: SizedBox.shrink()),
                buildBottom(),
                SizedBox(
                  height: getProportionateScreenHeight(25),
                )
              ],
            ),
          ),
          locale: controller.currentLanguage != ''
              ? Locale(controller.currentLanguage, '')
              : null,
          localeResolutionCallback: LocalizationService.localeResolutionCallBack,
          supportedLocales: LocalizationService.supportedLocales,
          localizationsDelegates: LocalizationService.localizationsDelegate,
        );
      }
    );
  }

  SingleChildScrollView buildBottom() {
    var levels = [1,100, 200, 300,400,500,600,700,800,900,1000];

    int vr = 13;
    int days = cigaraManager.calculatePassedTime();
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          for (int i in levels)
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  width: getProportionateScreenWidth(days > 100
                      ? 3
                      : days > 10
                          ? 6
                          : 10),
                ),
                buildDayItem(
                    count: (i / 100).toInt()+1,
                    checked: level  > i+1,
                    current: level  == i),
                SizedBox(
                  width: getProportionateScreenWidth(days > 100
                      ? 3
                      : days > 10
                          ? 6
                          : 10),
                ),
              ],
            )
        ],
      ),
    );
  }

  String _converToText(int passed, int requiredTime) {
    if (requiredTime > passed) {
      return "${langs[lang]["home"]["daysafter"].replaceAll("X", (requiredTime - passed).toString())}";
    } else if (requiredTime > passed) {
      int diff = requiredTime - passed;
      return "${diff}:${diff % 60}:${diff % 60} ${langs[lang]["home"]["minafter"]}";
    }
    return "";
  }

  Column buildDayItem({int count, bool checked, bool current}) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        checked
            ? Icon(
                Icons.check,
                color: Colors.green,
              )
            : current
                ? Icon(
                    Icons.adjust,
                    color: Colors.green,
                  )
                : Icon(
                    Icons.adjust,
                    color: Theme.of(context).backgroundColor,
                  ),
        SizedBox(
          height: 4,
        ),
        Container(
          padding: EdgeInsets.all(getProportionateScreenWidth(12)),
          decoration: BoxDecoration(
              borderRadius:
                  BorderRadius.circular(getProportionateScreenWidth(24)),
              color: checked ? Colors.lightGreen : Colors.grey[100],
              boxShadow: [
                BoxShadow(
                    color: kShadowColor.withOpacity(.32),
                    blurRadius: 12,
                    offset: Offset(5, 5)),
                BoxShadow(
                    color: Colors.white, blurRadius: 12, offset: Offset(-5, -5))
              ]),
          child: AutoSizeText(
            "$count",
            style: TextStyle(color: checked ? Colors.white : Colors.grey),
            maxLines: 1,
          ),
        )
      ],
    );
  }

  Padding buildInfoCard(BuildContext context, String title, String text,
      IconData icon, Color color) {
    return Padding(
      padding: EdgeInsets.all(getProportionateScreenWidth(20)),
      child: SizedBox(
        width: getProportionateScreenWidth(222),
        child: Container(
          height: getProportionateScreenHeight(150),
          padding: EdgeInsets.all(getProportionateScreenWidth(20)),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              border: Border.all(
                color: Theme.of(context).primaryIconTheme.color,
              ),
              color: Theme.of(context).backgroundColor,
              boxShadow: [
                BoxShadow(
                    color: kShadowColor.withOpacity(.32),
                    offset: Offset(6, 6),
                    blurRadius: 12)
              ]),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: getProportionateScreenWidth(150),
                    child: AutoSizeText(
                      title,
                      style: Theme.of(context).textTheme.bodyText2.copyWith(
                          fontSize: getProportionateScreenWidth(16),
                          color: color),
                      maxLines: 2,
                    ),
                  ),
                  Icon(
                    icon,
                    size: getProportionateScreenWidth(28),
                  ),
                ],
              ),
              SizedBox(height: 5),
              AutoSizeText(
                text,
                style: Theme.of(context).textTheme.bodyText2.copyWith(
                    fontWeight: FontWeight.bold,
                    fontSize: getProportionateScreenWidth(14)),
                maxLines: 2,
              ),
            ],
          ),
        ),
      ),
    );
  }

  InkWell buildCategoryCard(Size sc, BuildContext context,
      {Color color, IconData icon, String text, String id, Function onTap}) {
    return InkWell(
      onTap: onTap,
      child: Ink(
        padding: EdgeInsets.symmetric(
            horizontal: 10, vertical: getProportionateScreenHeight(15)),
        decoration: BoxDecoration(
            border: Border.all(
              color: Theme.of(context).primaryIconTheme.color,
            ),
            borderRadius: BorderRadius.circular(12),
            color: Theme.of(context).backgroundColor,
            boxShadow: [
              BoxShadow(
                  color: Colors.white,
                  blurRadius: 15,
                  offset: Offset(-4, -4),
                  spreadRadius: 1.0),
            ]),
        child: ConstrainedBox(
          constraints: BoxConstraints(
            minWidth: getProportionateScreenWidth(120),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Hero(
                tag: id,
                child: Icon(
                  icon,
                  color: color,
                  size: getProportionateScreenWidth(26),
                ),
              ),
              SizedBox(
                width: 3,
              ),
              AutoSizeText(
                text,
                style: Theme.of(context).textTheme.headline4.copyWith(
                    color: color,
                    fontSize: getProportionateScreenWidth(20),
                    fontWeight: FontWeight.w600),
              ),
            ],
          ),
        ),
      ),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      actions: [],

      centerTitle: true,
      // title: setHeaderTitle(AppTranslations.of(context).text(AppTitle.appTitle),Colors.white),
      title: setHeaderTitle(TKeys.dashbAchievments.translate(context),Colors.white),
      backgroundColor: AppColor.themeColor,
      elevation: 1.0,
      leading: new IconButton(
        icon: Icon(Icons.arrow_back_ios,color:Colors.white),
        onPressed: (){
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(
                  builder: (context) => TabBarScreen()),
              ModalRoute.withName('/TabBarScreen'));        },
      ),

    );
  }
}
