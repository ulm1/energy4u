import 'dart:async';
import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:moodyapp/comps/cigaratte.dart';
import 'package:moodyapp/comps/getlang.dart';
import 'package:moodyapp/comps/progress_painter.dart';
import 'package:moodyapp/static/htimes.dart';
import 'package:moodyapp/static/lang.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../size_config.dart';
import 'guide_screen.dart';

class ProgressPage extends StatefulWidget {
  final Cigaratte cigaratteManager;
  final int points;

  ProgressPage({Key key, this.cigaratteManager,this.points}) : super(key: key);

  @override
  _ProgressPageState createState() => _ProgressPageState();
}

class _ProgressPageState extends State<ProgressPage> {
  String lang = "";
  Timer statetimer;
  final controller = ScrollController();
  double offset = 0;
  @override
  void initState() {
    super.initState();
    lang = getLang();
    statetimer = Timer.periodic(Duration(seconds: 1), (timer) {
      setState(() {});
    });
  }

  @override
  void dispose() {
    controller.addListener(onScroll);
    statetimer.cancel();
    super.dispose();
  }

  void onScroll() {
    setState(() {
      offset = (controller.hasClients) ? controller.offset : 0;
    });
  }
  int level = 0;


  String _converToText(int passed, int requiredTime,int points) {
    var questionnaires = [1,2, 10, 15,20,30,35,40,45,50,55,60,65,80];

    var completed = points / 5;

    var i = questionnaires.length - 1;
    while ( i >=0 ) {
      if (completed > questionnaires[i]) {

        level = i+1;
        break;
      }
      i--;
    }
    var left =questionnaires[i+1]  - completed ;

    print("my process completed"+ level.toString());
    print("my pro"+ left.round().toString());

    var completedPercentage = points % 100;



    if (requiredTime > completed) {
      return "${requiredTime - completed.round()} ${langs[lang]["process"]["daysleft"]}";
    } else if (requiredTime > completed) {
      int diff = requiredTime - completed.round();
      return "${diff}:${diff % 60}:${diff % 60} ${langs[lang]["process"]["minleft"]}";
    } else {
      return "${langs[lang]["process"]["completed"]}";
    }
  }

  double _getPercentage(int passed, int requiredTime) =>
      passed > requiredTime
          ? 100
          : 100 -
              ((requiredTime - passed) /
                  requiredTime *
                  100);



  ClipPath buildHeader(BuildContext context, double offset) {
    return ClipPath(
      clipper: MyClipper(),
      child: Container(
        padding: EdgeInsets.only(left: 40, top: 30, right: 20),
        height: getProportionateScreenHeight(350),
        width: double.infinity,
        decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [Color(0xFFD66D75), Color(0XFFE29587)]),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [

            Expanded(
              child: Stack(
                children: [
                  Positioned(
                    top: (offset < 0) ? 0 : offset,
                    child: SvgPicture.asset(
                      "assets/images/girlcigarabreak.svg",
                      width: getProportionateScreenWidth(120),
                      fit: BoxFit.contain,
                      alignment: Alignment.topCenter,
                    ),
                  ),
                  Positioned(
                    top: (offset < 0)
                        ? 0
                        : offset + getProportionateScreenWidth(40),
                    left: getProportionateScreenWidth(140),
                    child: Text(
                      langs[lang]["guideps"]["guideto"],
                      style: Theme.of(context).textTheme.headline3.copyWith(
                          color: Colors.white,
                          fontSize: getProportionateScreenWidth(32)),
                    ),
                  ),
                  Container()
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
        backgroundColor: Colors.white,
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
        //    buildHeader(context, offset),

            Expanded(
              child: ListView.builder(
                  itemCount: htimes.length,
                  itemBuilder: (context, i) {
                    return Container(
                        decoration: BoxDecoration(),
                        padding:
                            EdgeInsets.all(getProportionateScreenWidth(16)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                              flex: 5,
                              child: Column(
                                children: [
                                  AutoSizeText(
                                    _converToText(
                                        widget.cigaratteManager
                                            .calculatePassedTime(),
                                        htimes[i]["time"],widget.points),
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyText2
                                        .copyWith(
                                            color: Colors.black,
                                            fontSize:
                                                getProportionateScreenWidth(
                                                    22)),
                                    textAlign: TextAlign.center,
                                    maxLines: 1,
                                  ),
                                  AutoSizeText(
                                    langs[lang]["progressDescription"]
                                        [htimes[i]["id"]],
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline4
                                        .copyWith(
                                            color: Colors.black,
                                            fontSize:
                                                getProportionateScreenWidth(
                                                    15)),
                                    textAlign: TextAlign.center,
                                    maxLines: 3,
                                  ),
                                ],
                              ),
                            ),
                            Expanded(
                              flex: 2,
                              child: Container(
                                width: getProportionateScreenHeight(42),
                                height: getProportionateScreenHeight(42),
                                child: CustomPaint(
                                  child: Center(
                                    child: _getPercentage(
                                                widget.cigaratteManager
                                                    .calculatePassedTime(),
                                                htimes[i]["time"]) ==
                                            100
                                        ? Icon(
                                            Icons.check,
                                            color: Colors.blue,
                                          )
                                        : AutoSizeText(
                                            "${htimes[i]["time"] - widget.cigaratteManager.calculatePassedTime()}",
                                            style: TextStyle(
                                              color: Colors.blue,
                                            ),
                                            maxLines: 1,
                                            textScaleFactor: 1.0),
                                  ),
                                  painter: ProgressPainter(
                                      completedPercentage:  _getPercentage(
                                          widget.cigaratteManager
                                              .calculatePassedTime(),
                                          htimes[i]["time"]).round(),
                                      circleWidth: 10,
                                      defaultCircleColor:
                                          Colors.blue,
                                      percentageCompletedCircleColor:
                                          Colors.grey),
                                ),
                              ),
                            ),
                          ],
                        ));
                  }),
            )
          ],
        ),
        appBar: buildAppBar(context));
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: Color(0xFFD66D75),
      leading: new IconButton(
        icon: new Icon(Icons.arrow_back, color: Colors.white),
        onPressed: () => Navigator.of(context).pop(),
      ),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Hero(
            tag: "guide",
            child:Icon(
              Icons.timelapse,
              color: Colors.white,
              size: getProportionateScreenWidth(26),
            ),
          ),
          SizedBox(
            width: 5,
          ),
          Text(
            "${langs[lang]["home"]["progress"]}",
            style: Theme.of(context).textTheme.bodyText2.copyWith(
                color: Colors.white, fontSize: getProportionateScreenWidth(26)),
          )
        ],
      ),

    );
  }
}
