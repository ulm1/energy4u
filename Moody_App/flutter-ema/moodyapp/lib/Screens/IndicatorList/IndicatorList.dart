import 'package:moodyapp/Localization/app_translations_delegate.dart';
import 'package:moodyapp/Localization/application.dart';
import 'package:moodyapp/Screens/TestIndicatorDetails/TestIndicatorDetails.dart';
import 'package:moodyapp/Screens/contanst/contanst.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart' as foundation;
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:moodyapp/Helper/CommonWidgets/CommonWidgets.dart';
import 'package:moodyapp/Helper/Constant.dart';
import 'package:moodyapp/Helper/SharedManager.dart';
import 'package:syncfusion_flutter_charts/charts.dart';



void main()=> runApp(new IndicatorList());

bool get isIos =>
    foundation.defaultTargetPlatform == foundation.TargetPlatform.iOS;

class IndicatorList extends StatefulWidget {
  @override
  _IndicatorListState createState() => _IndicatorListState();
}


class _IndicatorListState extends State<IndicatorList> {



  AppTranslationsDelegate _newLocaleDelegate;
  @override
  void initState() {

    super.initState();
    _newLocaleDelegate = AppTranslationsDelegate(newLocale:null);
    application.onLocaleChanged = onLocaleChange;
  }



  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner:false,
      localizationsDelegates: [
        _newLocaleDelegate,
        //provides localised strings
        GlobalMaterialLocalizations.delegate,
        //provides RTL support
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        SharedManager.shared.language
      ],
      home: DefaultTabController(
        length: 1,
        initialIndex: 0,
        child: Scaffold(
            appBar: new AppBar(
              centerTitle: true,
              // title: setHeaderTitle(AppTranslations.of(context).text(AppTitle.appTitle),Colors.white),
              title: setHeaderTitle("Achievements",Colors.white),
              backgroundColor: AppColor.themeColor,
              elevation: 1.0,
              leading: new IconButton(
                icon: Icon(Icons.arrow_back_ios,color:Colors.white),
                onPressed: (){
                  Navigator.of(context).pop();
                },
              ),
              bottom: TabBar(
                indicatorColor: Colors.red,
                tabs: <Widget>[

                  Tab(
                    text: "",
                  )
                ],
              ),
            ),
            body: TabBarView(
              children: <Widget>[IosSecondPage()],
            )),
      ),
    );
  }

  void onLocaleChange(Locale locale) {
    setState(() {



      _newLocaleDelegate = AppTranslationsDelegate(newLocale: locale);
    });
  }
}



List<PremiumIndicator> listPremium = [
//1
  PremiumIndicator("Achievement 1:",
      "first Achievement.",Icon(Icons.visibility_off,color: Colors.red,)),
  PremiumIndicator("Achievement 2:",
      "first Achievement.",Icon(Icons.visibility_off,color: Colors.red,)),
  PremiumIndicator("Achievement 3:",
      "first Achievement.",Icon(Icons.visibility_off,color: Colors.red,)),
  PremiumIndicator("Achievement 4:",
      "first Achievement.",Icon(Icons.visibility_off,color: Colors.red,)),
  PremiumIndicator("Achievement 5:",
      "first Achievement.",Icon(Icons.visibility_off,color: Colors.red,)),

];



class IosSecondPage extends StatelessWidget {
  const IosSecondPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height:MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: new Padding(
        padding: new EdgeInsets.only(top: 15),
        child: _setPremiumIndicatorList(listPremium),
      ),
    );
  }
}

double monVal;
double tueVal;
double WedVal;
double ThurVal;
double FriVal;
double SatVal;
double SunVal;
double total;
double today;
String minDate;
double minAmount;
String maxDate;
double maxAmount;
String title;




_setPremiumIndicatorList(List<PremiumIndicator>data){
  return new ListView.builder(
    itemCount: data.length,
    itemBuilder: (context,index){
      return new InkWell(
        onTap: () async {

        },

        child: new Container(
          // height:100,
          padding: new EdgeInsets.all(8),
          child: new Material(
            color: Colors.white,
            elevation: 2.0,
            borderRadius: BorderRadius.circular(8),
            child: new Padding(
                padding: EdgeInsets.all(8),
                child: new Row(
                  children: <Widget>[
                    data[index].icon,
                    SizedBox(width: 12,),
                    new Container(
                      height: 40,
                      width: 1,
                      color: AppColor.themeColor,
                    ),
                    SizedBox(width: 12,),
                    new Expanded(
                        child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            setCommonText(data[index].title,Colors.black, 17.0, FontWeight.w500, 1),
                            SizedBox(height: 3,),
                            setCommonText(data[index].subTitle,Colors.grey, 16.0, FontWeight.w400, 3),
                          ],
                        )
                    ),
                    SizedBox(width: 12,),
                    new Icon(Icons.check_circle,color:AppColor.themeColor),
                  ],
                )
            ),
          ),
        ),
      );
    },
  );
}


class BasicIndicator{
  String title;
  Icon icon;
  BasicIndicator(this.title,this.icon);
}

class PremiumIndicator{
  String title;
  String subTitle;
  Icon icon;

  PremiumIndicator(this.title,this.subTitle,this.icon);
}