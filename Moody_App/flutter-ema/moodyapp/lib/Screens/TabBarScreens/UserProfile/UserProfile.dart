import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:hive/hive.dart';
import 'package:intl/intl.dart';
import 'package:moodyapp/Helper/CommonWidgets/CommonWidgets.dart';
import 'package:moodyapp/Helper/Constant.dart';
import 'package:moodyapp/Helper/SharedManager.dart';
import 'package:moodyapp/Localization/app_translations.dart';
import 'package:moodyapp/Localization/app_translations_delegate.dart';
import 'package:moodyapp/Localization/application.dart';
import 'package:moodyapp/Localization/localization_service.dart';
import 'package:moodyapp/Localization/t_key.dart';
import 'package:moodyapp/Screens/IndicatorList/TestIndicators2.dart';
import 'package:moodyapp/Screens/addResults/AddResultsScreen.dart';
import 'package:moodyapp/Screens/contanst/contanst.dart';
import 'package:moodyapp/services/database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

import '../../../QUESTIONNAIRE/models/question_model.dart';
import '../../../boxes.dart';
import '../../../feedbackform.dart';
import '../../../main.dart';
import '../../../t&C.dart';
import '../../Notifications/mainNotification.dart';




class UserProfile extends StatefulWidget {

   UserProfile();


  @override
  _UserProfileState createState() => _UserProfileState();
}

bool isLoading = true;


SharedPreferences preferences;

class _UserProfileState extends State<UserProfile> {
  //DatabaseService databaseService = new DatabaseService();


  List profileList = [];




  final List<String> languagesList = application.supportedLanguages;
  final List<String> languageCodesList = application.supportedLanguagesCodes;
  _setCommonViewForGoal() {

    return new Container(
      height: MediaQuery.of(context).size.width/1.4,
      padding: new EdgeInsets.all(15),
      child: isLoading
          ? Container(
        child: Center(child: CircularProgressIndicator()),
      ):
      new Material(
        color: Colors.white,
        elevation: 2.0,
        borderRadius: BorderRadius.circular(5),
        child: new Padding(
            padding: new EdgeInsets.all(10),
            child: new Column(
              children: <Widget>[
                new Expanded(
                  child: new Container(
                    child: new Row(
                      children: <Widget>[
                        new Expanded(
                          child: new Container(
                            child: Icon(Icons.ac_unit,color: Colors.black,size: 25,),
                          ),
                        ),
                        new Container(
                          width: 2,
                          color: Colors.grey[300],
                        ),
                        SizedBox(width: 5,),
                        new Expanded(
                          child: new Container(
                            child: Icon(Icons.auto_delete_outlined,color: Colors.black,size: 25,),
                          ),
                        ),
                        new Container(
                          width: 2,
                          color: Colors.grey[300],
                        ),
                        SizedBox(width: 5,),
                        new Expanded(
                          child: new Container(
                            child: Icon(Icons.art_track_sharp,color: Colors.black,size: 25,),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                new Container(
                  height: 2,
                  color: Colors.grey[300],
                ),
                new Expanded(
                  child: new Container(
                    child: new Row(
                      children: <Widget>[
                        new Expanded(
                          child: new Container(
                            child: Icon(Icons.auto_delete_outlined,color: Colors.black,size: 25,),
                          ),
                        ),
                        new Container(
                          width: 2,
                          color: Colors.grey[300],
                        ),
                        SizedBox(width: 5,),
                        new Expanded(
                          child: new Container(
                            child: Icon(Icons.auto_delete_outlined,color: Colors.black,size: 25,),
                          ),
                        ),
                        new Container(
                          width: 2,
                          color: Colors.grey[300],
                        ),
                        SizedBox(width: 5,),
                        new Expanded(
                          child: new Container(
                            child: Icon(Icons.auto_delete_outlined,color: Colors.black,size: 25,),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            )
        ),
      ),
    );
  }

  _setDetailsView(){


    SharedPreferences preferences;
    Future<void> initializePreference() async{
      preferences = await SharedPreferences.getInstance();
    }





    DateTime now = new DateTime.now();
    DateTime date = new DateTime(now.year, now.month, now.day);
    return new Container(
      height: 120,
      color:Colors.grey[100],
      padding: new EdgeInsets.all(20),
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          new Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              setCommonText(TKeys.MyMoodDiary.translate(context), AppColor.themeColor, 25.0, FontWeight.w600, 1),
              SizedBox(width: 3,),
              setCommonText("", Colors.grey, 18.0, FontWeight.w600, 1),
            ],
          ),
          SizedBox(height: 10,),
          new Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Icon(Icons.watch_later,color:Colors.grey),
              SizedBox(width: 3,),
              setCommonText(DateFormat('dd.MM.yyyy').format(DateTime.now()).toString(), Colors.grey, 17.0, FontWeight.w400, 1),
            ],
          )
        ],
      ),
    );
  }
  _setChardIndicatorView(){

    final int moodday = preferences.getInt('Monday');
    final int tuesday = preferences.getInt('Tuesday');
    final int wednesday = preferences.getInt('Wednesday');
    final int thursday = preferences.getInt('Thursday');
    final int friday = preferences.getInt('friday');
    final int saturday = preferences.getInt('Saturday');
    final int sunday = preferences.getInt('Sunday');

    return new Container(
      height: 200,
      child: SfCartesianChart(
          primaryXAxis: CategoryAxis(),
          // Enable legend
          legend: Legend(isVisible: true),
          // Enable tooltip
          tooltipBehavior: TooltipBehavior(enable: false),
          series: <LineSeries<SalesData, String>>[
            LineSeries<SalesData, String>(
                dataSource:  <SalesData>[
                  SalesData('Mon', moodday == null ? 0 : moodday.toDouble() *10),
                  SalesData('Tue', tuesday == null ? 0 : tuesday.toDouble()*10),
                  SalesData('Wed', wednesday == null ? 0 : wednesday.toDouble()*10),
                  SalesData('Thu', thursday == null ? 0 : thursday.toDouble()*10),
                  SalesData('Fri', friday == null ? 0 : friday.toDouble()*10),
                  SalesData('Sat', saturday == null ? 0 : saturday.toDouble()*10),
                  SalesData('Sun', sunday == null ? 0 : sunday.toDouble()*10),
                ],
                xValueMapper: (SalesData sales, _) => sales.year,
                yValueMapper: (SalesData sales, _) => sales.sales,
                // Enable data label
                dataLabelSettings: DataLabelSettings(isVisible: true)
            )
          ]
      ),
    );
  }

  _setMainIndicatiorDescriptionView()  {

String mood = "";
String day= "";
    List profileList = [];


final String quest5 = preferences.getString('question4');
final int moodday = preferences.getInt('Monday');
final int tuesday = preferences.getInt('Tuesday');
final int wednesday = preferences.getInt('Wednesday');
final int thursday = preferences.getInt('Thursday');
final int friday = preferences.getInt('friday');
final int saturday = preferences.getInt('Saturday');
final int sunday = preferences.getInt('Sunday');


List<SalesData> data = [
  SalesData('Mon', moodday == null ? 0 : moodday.toDouble() *10),
  SalesData('Tue', tuesday == null ? 0 : tuesday.toDouble()*10),
  SalesData('Wed', wednesday == null ? 0 : wednesday.toDouble()*10),
  SalesData('Thu', thursday == null ? 0 : thursday.toDouble()*10),
  SalesData('Fri', friday == null ? 0 : friday.toDouble()*10),
  SalesData('Sat', saturday == null ? 0 : saturday.toDouble()*10),
  SalesData('Sun', sunday == null ? 0 : sunday.toDouble()*10),
];


print("hh" + data.where((object) => object.sales == 0).length.toString()); // 2


if (quest5!= null )
    {

      mood = quest5;

      

    }
var now = new DateTime.now();
var formatter = new DateFormat('dd.MM.yyyy');
String formattedDate = formatter.format(now);


    return new Container(
      height: MediaQuery.of(context).size.width,
      width: MediaQuery.of(context).size.width,
      padding: new EdgeInsets.all(15),
      child: new Material(
        color: Colors.white,
        elevation: 2.0,
        borderRadius: new BorderRadius.circular(5),
        child: new Padding(
          padding: new EdgeInsets.all(15),
          child: new Container(
            child: new Column(
              children: <Widget>[
                new Expanded(
                  child: new Container(
                    child: new Row(
                      children: <Widget>[
                        new Expanded(
                          child: _setCommonViewForDescription(TKeys.Total.translate(context),"Average Mood",(10 *data.where((object) => object.sales != 0).length).toString()),
                        ),
                        new Container(
                          width: 2,
                          color: Colors.grey[300],
                        ),
                        SizedBox(width: 8,),
                        new Expanded(
                          child: _setCommonViewForDescription(TKeys.Today.translate(context),  formattedDate,mood.toString()),
                        ),
                      ],
                    ),
                  ),
                ),
                new Container(
                  height: 2,
                  width: MediaQuery.of(context).size.width,
                  color: Colors.grey[300],
                ),
                new Expanded(
                  child: new Container(
                    child:new Row(
                      children: <Widget>[
                        new Expanded(
                          child: _setCommonViewForDescription(TKeys.Worst.translate(context),"",(data
                              .reduce((item1, item2) =>(item1.sales >0 && item2.sales>0) && (item1.sales < item2.sales) ? item1 : item2).sales.toString())),
                        ),
                        new Container(
                          width: 2,
                          color: Colors.grey[300],
                        ),
                        SizedBox(width: 8,),
                        new Expanded(
                          child: _setCommonViewForDescription(TKeys.Happiest.translate(context),"",(data
                              .reduce((item1, item2) => item1.sales > item2.sales ? item1 : item2).sales.toString())),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _setCommonViewForDescription(String title,String date,String bpm){
    return new Container(
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          setCommonText(title, AppColor.themeColor, 20.0, FontWeight.w700, 1),
          SizedBox(height:3),
          setCommonText(date, Colors.grey, 16.0, FontWeight.w500, 1),
          SizedBox(height:10),
          new Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              setCommonText(bpm, Colors.black, 22.0, FontWeight.w600, 1),
              SizedBox(width: 3,),
              setCommonText("", Colors.grey, 17.0, FontWeight.w600, 1),
            ],
          )
        ],
      ),
    );
  }




  _setBottomView(){
    // final Map<dynamic, dynamic> languagesMap = {
    //   languagesList[0]: languageCodesList[0],
    //   languagesList[1]: languageCodesList[1]
    // };

    return new Container(
      height:profileList.length * 80.0,
      color: Colors.grey[200],
      padding: new EdgeInsets.all(15),
      child: new GridView.count(
        crossAxisCount: 1,
        childAspectRatio: 5.5,
        physics: NeverScrollableScrollPhysics(),
        children: List<Widget>.generate(profileList.length,(index){
          return new Container(
            height: 70,
            padding: new EdgeInsets.only(top:5,bottom: 5),
            child: new InkWell(
              onTap: (){


                if(index ==0){
                  Navigator.of(context,rootNavigator: false).push(MaterialPageRoute(builder: (context)=>UI22()));
                }

                if(index ==1){
                  Navigator.of(context,rootNavigator: false).push(MaterialPageRoute(builder: (context)=>LPage()));



              //    Navigator.of(context,rootNavigator: false).push(MaterialPageRoute(builder: (context)=>AddReults(RouterName.id.toString())));
                }

                if(index ==2){
                  Navigator.of(context,rootNavigator: false).push(MaterialPageRoute(builder: (context)=>TcPage()));
   // Navigator.of(context,rootNavigator: false).pushAndRemoveUntil(MaterialPageRoute(builder: (context)=>HomePage(null)),ModalRoute.withName('/MyHomePage'));
    // Navigator.of(context,rootNavigator: true).pushAndRemoveUntil(MaterialPageRoute(builder: (context)=>LoginPage()),ModalRoute.withName('/login'));
    }
                if(index ==3){
                  Navigator.of(context,rootNavigator: false).push(MaterialPageRoute(builder: (context)=>PPage()));
                  // Navigator.of(context,rootNavigator: false).pushAndRemoveUntil(MaterialPageRoute(builder: (context)=>HomePage(null)),ModalRoute.withName('/MyHomePage'));
                  // Navigator.of(context,rootNavigator: true).pushAndRemoveUntil(MaterialPageRoute(builder: (context)=>LoginPage()),ModalRoute.withName('/login'));
                }
                if(index ==4){
                  Navigator.of(context,rootNavigator: false).push(MaterialPageRoute(builder: (context)=>mainNotification()));
                  // Navigator.of(context,rootNavigator: false).pushAndRemoveUntil(MaterialPageRoute(builder: (context)=>HomePage(null)),ModalRoute.withName('/MyHomePage'));
                  // Navigator.of(context,rootNavigator: true).pushAndRemoveUntil(MaterialPageRoute(builder: (context)=>LoginPage()),ModalRoute.withName('/login'));
                }

              },

              child: new Material(
                elevation: 2.0,
                borderRadius: new BorderRadius.circular(5),
                child: new Padding(
                  padding: new EdgeInsets.only(left: 15,right: 15),
                  child: new Row(
                    children: <Widget>[
                      profileList[index]['icon'],
                      SizedBox(width: 12,),
                      new Container(height: 30,color: Colors.grey[300],width: 2,),
                      SizedBox(width: 12,),
                      new Expanded(
                          child: setCommonText(profileList[index]['title'], Colors.grey, 16.0, FontWeight.w500,1)
                      ),
                      SizedBox(width: 12,),
                      new Icon(Icons.arrow_forward_ios,size: 18,color:AppColor.themeColor),
                    ],
                  ),
                ),
              ),
            ),
          );
        }),
      ),
    );
  }


  AppTranslationsDelegate _newLocaleDelegate;
  @override
  initState()  {




    super.initState();
    isLoading = false;
    initializePreference().whenComplete((){
      setState(() {});
    });

    SharedManager.shared.isOnboarding = true;
    _newLocaleDelegate = AppTranslationsDelegate(newLocale:null);
    application.onLocaleChanged = onLocaleChange;
  }



  final localizationController = Get.put(LocalizationController());


  @override
  Widget build(BuildContext context) {

  //  List<QuestionnaireModel> person = contactBox.values;

    //var result =  qwe.map((e) => e.answer.reduce((value, element) => value));
   // print(result);





    this.profileList = [
     // {"title":AppTranslations.of(context).text(AppTitle.profileGoalSetting),"icon":Icon(Icons.local_hospital,color: AppColor.themeColor,size: 18,)},
      {"title":"Feedback","icon":Icon(Icons.add_comment,color: AppColor.themeColor,size: 18,)},
      {"title":TKeys.Licenses.translate(context),"icon":Icon(Icons.add_chart,color: AppColor.themeColor,size: 18,)},
      {"title":TKeys.termstitle.translate(context),"icon":Icon(Icons.title,color: AppColor.themeColor,size: 18,)},
      {"title":TKeys.Privacy.translate(context),"icon":Icon(Icons.lock,color: AppColor.themeColor,size: 18,)},
      {"title":TKeys.Notifications.translate(context),"icon":Icon(Icons.notifications,color: AppColor.themeColor,size: 18,)},
    ];



    return GetBuilder<LocalizationController>(
        init: localizationController,
        builder: (LocalizationController controller){
        return MaterialApp(
          debugShowCheckedModeBanner:false,
          home: Scaffold(
            appBar: new AppBar(
              centerTitle: true,
              // title: setHeaderTitle(AppTranslations.of(context).text(AppTitle.appTitle),Colors.white),
              title: setHeaderTitle(TKeys.drawerProfile.translate(context),Colors.white),
              backgroundColor: AppColor.themeColor,
              elevation: 1.0,
              actions: setCommonCartNitificationView(context),
            ),
            body: isLoading
                ? Container(
              child: Center(child: CircularProgressIndicator()),
            ) :  new Container(
                color: Colors.grey[200],
                child: new ListView(
                  children: <Widget>[
                    SizedBox(height: 40,),
                    _setDetailsView(),
                    _setChardIndicatorView(),
                    _setMainIndicatiorDescriptionView(),


                    _setBottomView()
                  ],
                )
            ),
          ),
          routes: {
            '/UserProfile': (BuildContext context) => UserProfile()
          },
          theme: SharedManager.shared.getThemeType(),
          locale: controller.currentLanguage != ''
              ? Locale(controller.currentLanguage, '')
              : null,
          localeResolutionCallback: LocalizationService.localeResolutionCallBack,
          supportedLocales: LocalizationService.supportedLocales,
          localizationsDelegates: LocalizationService.localizationsDelegate,
        );
      }
    );
  }
  //This is for localization
  void onLocaleChange(Locale locale) {
    setState(() {
      _newLocaleDelegate = AppTranslationsDelegate(newLocale: locale);
    });
  }

  Future<void> setupapp() async {



  }

  Future<void> initializePreference() async{
    preferences = await SharedPreferences.getInstance();
  }}