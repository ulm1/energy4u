import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:moodyapp/Helper/bottom2.dart';
import 'package:moodyapp/Helper/bottombar.dart';
import 'package:moodyapp/Localization/app_translations_delegate.dart';
import 'package:moodyapp/Localization/application.dart';
import 'package:moodyapp/Localization/localization_service.dart';
import 'package:moodyapp/Localization/t_key.dart';
import 'package:moodyapp/Screens/AddDeviceScreen/FindQuestionnaireScreen.dart';
import 'package:moodyapp/Screens/QuestionnaireBlogs/QuestionnaireListScreen.dart';
import 'package:moodyapp/Screens/QuestionnaireList/QuestionnaireList.dart';
import 'package:moodyapp/Screens/TabBarScreens/DashBoard/Dashboard.dart';
import 'package:moodyapp/Screens/TabBarScreens/Indicators/TestIndicators.dart';
import 'package:moodyapp/Screens/TabBarScreens/UserProfile/UserProfile.dart';
import 'package:moodyapp/Screens/contanst/contanst.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:moodyapp/Helper/Constant.dart';
import 'package:moodyapp/Helper/SharedManager.dart';
import 'package:moodyapp/Localization/app_translations.dart';

import '../../../QUESTIONNAIRE/models/question_model.dart';
import '../../../boxes.dart';
import '../../../globals.dart';
import '../../../main.dart';

import '../../../globals.dart';

void main() => runApp(new TabBarScreen());


class TabBarScreen extends StatefulWidget {

  @override
  _TabBarScreenState createState() => _TabBarScreenState();
}

class _TabBarScreenState extends State<TabBarScreen> {
  final box = Boxes.getQuestionnaire();
  bool isButtonClick = false;

  final localizationController = Get.put(LocalizationController());

  @override
  void initState() {

    super.initState();
    SharedManager.shared.isOnboarding = true;
  }

  List<Widget> listScreen = [
    DashboardScreen(),
    QuestionnaireList(),
    QuestBlogsScreen("firebaseId"),
    InfoScreen2(),
    UserProfile(),
  ];
  String _lastSelected = 'TAB: 0';

  void _selectedTab(int index) {
    setState(() {
      _lastSelected = 'TAB: $index';
    });
  }


  void _selectedFab(int index) {
    setState(() {
      _lastSelected = 'FAB: $index';
    });
  }

  @override
  Widget build(BuildContext context) {

      return GetBuilder<LocalizationController>(
          init: localizationController,
          builder: (LocalizationController controller){
        return MaterialApp(
            debugShowCheckedModeBanner: false,
            home: new Scaffold(
              body: listScreen[SharedManager.shared.currentIndex],
              bottomNavigationBar: BottomNavigationBar(
                  type: BottomNavigationBarType.fixed,
                  currentIndex: SharedManager.shared.currentIndex,
                  onTap: onTabTapped,
                  items: [
                    new BottomNavigationBarItem(
                      icon: Icon(
                        Icons.home,
                      ),
                      activeIcon: Icon(
                        Icons.home,
                      ),
                      label: TKeys.drawerHome.translate(context)
                      ,
                    ),
                    new BottomNavigationBarItem(
                      icon: Icon(Icons.calendar_today),
                        label: TKeys.drawerCalender.translate(context)
                    ),
                    new BottomNavigationBarItem(
                      icon: Icon(
                        Icons.grain,
                        color: Colors.white.withAlpha(0),
                      ),
                      label: ""
                      ,
                    ),
                    new BottomNavigationBarItem(
                        icon: Icon(Icons.grain),
                        label: TKeys.drawerJournal.translate(context)
                    ),
                    new BottomNavigationBarItem(
                      icon: Icon(Icons.person),
                      label: TKeys.drawerProfile.translate(context)
                      ,
                    ),
                  ]),

              floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,


              floatingActionButton: FloatingActionButton(
                child: new Icon(Icons.add,color:isButtonClick?Colors.blue:Colors.black,),
                onPressed: (){
                  Navigator.of(context).push(MaterialPageRoute(builder: (context)=>FindQuestionnaireScreen()));


                  setState(() {
                    isButtonClick = true;
                    SharedManager.shared.currentIndex = 1;
                    isButtonClick = false;

                  });
                },
                backgroundColor: Colors.grey[300],
              ),

            ),



            /*
              floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
              floatingActionButton: _buildFab(
                  context), // This trailing comma makes auto-formatting nicer for build methods.
            ),

            */
            locale: controller.currentLanguage != ''
                ? Locale(controller.currentLanguage, '')
                : null,
            localeResolutionCallback: LocalizationService.localeResolutionCallBack,
            supportedLocales: LocalizationService.supportedLocales,
            localizationsDelegates: LocalizationService.localizationsDelegate,
            routes: {'/TabBar': (BuildContext context) => TabBarScreen()},
            theme: SharedManager.shared.getThemeType());
      }
    );
  }

  void onTabTapped(int index) {
    setState(() {
      if (index == 2) {
        isButtonClick = false;
       // SharedManager.shared.currentIndex = index;
      }
      else {
        SharedManager.shared.currentIndex = index;
      }
    });
  }

  Widget _buildFab(BuildContext context) {
    final icons = [ Icons.question_answer, Icons.sentiment_satisfied_alt_sharp, Icons.keyboard_voice ];
    return AnchoredOverlay(
      showOverlay: true,
      overlayBuilder: (context, offset) {
        return CenterAbout(
          position: Offset(offset.dx, offset.dy - icons.length * 35.0),
          child: FabWithIcons(
              icons: icons,
              onIconTapped:(iconType) {


                print("iconType => $iconType");
              }
          ),
        );
      },
      child: FloatingActionButton(
        onPressed: () { },
        tooltip: 'Increment',
        child: Icon(Icons.add),
        elevation: 2.0,
      ),
    );
  }

  //This is for localization

}
