
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:moodyapp/Constant/sharedpreference_page.dart';
import 'package:moodyapp/Localization/app_translations_delegate.dart';
import 'package:moodyapp/Localization/application.dart';
import 'package:moodyapp/Localization/localization_service.dart';
import 'package:moodyapp/Localization/t_key.dart';

import 'package:moodyapp/Screens/QuestionnaireList/QuestionnaireList.dart';
import 'package:moodyapp/Screens/AddDeviceScreen/FindQuestionnaireScreen.dart';
import 'package:moodyapp/Screens/IndicatorList/IndicatorList.dart';
import 'package:moodyapp/Screens/IndicatorList/TestIndicators2.dart';
import 'package:moodyapp/Screens/LoginPage/ComonLoginWidgets.dart';
import 'package:moodyapp/Screens/TabBarScreens/Indicators/TestIndicators.dart';
import 'package:moodyapp/Screens/contanst/contanst.dart';
import 'package:moodyapp/data/model/user.dart';
import 'package:moodyapp/data/repositories/user/provider_api_user.dart';
import 'package:moodyapp/gamescreens/splash_screen.dart';
import 'package:moodyapp/gamescreens/startgame.dart';
import 'package:moodyapp/services/database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:moodyapp/Helper/CommonWidgets/CommonWidgets.dart';
import 'package:moodyapp/Helper/Constant.dart';
import 'package:moodyapp/Helper/SharedManager.dart';
import 'package:moodyapp/Localization/app_translations.dart';
import 'package:parse_server_sdk_flutter/parse_server_sdk.dart';


import '../../../globals.dart';


void main() => runApp(new DashboardScreen());
bool notifi = false;

class DashboardScreen extends StatefulWidget {
   String firebaseId = "myID";
  DashboardScreen();


  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class   Event {
  final String title;
  final double monvalue;
  final double tuevalue;
  final double wedvalue;
  final double thurvalue;
  final double frivalue;
  final double satvalue;
  final double sunvalue;
  final double totalvalue;
  final double todayvalue;
  final double minvalue;
  final double maxvalue;

  Event(this.title, this.monvalue, this.tuevalue, this.wedvalue, this.thurvalue, this.frivalue, this.satvalue, this.sunvalue, this.totalvalue, this.todayvalue, this.minvalue, this.maxvalue);

}


class _DashboardScreenState extends State<DashboardScreen> {




  @override
  void dispose() {
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {

    print(TKeys.dashbQuest.translate(context));

  List listData = [
      {"title":TKeys.dashbQuest.translate(context),"icon":Icon(Icons.query_builder,color:Colors.white,size: 40,),"availability":TKeys.QuestAvaliability.translate(context),"isSelect":false},
         {"title":TKeys.dashbAppointment.translate(context),"icon":Icon(Icons.insert_invitation,color:Colors.white,size: 40,),"availability":TKeys.appointmentAvaliability.translate(context),"isSelect":false},
    {"title":TKeys.dashbAchievments.translate(context),"icon":Icon(Icons.wifi_tethering,color:Colors.white,size: 40,),"availability":TKeys.AchievmentsAvaliability.translate(context),"isSelect":false},

  ];

_setMainInformationView() {
 // final ParseUser user = await ParseUser.currentUser();
 // UserProviderApi user = new UserProviderApi();


  return new Container(
    // height: 185,
    padding: new EdgeInsets.all(20),
    // color: Colors.red,
    child: new Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        new Row(
          children: <Widget>[

            //
            setCommonText(TKeys.dashbHello.translate(context) ,Colors.black,25.0,FontWeight.w500,1),
            setCommonText(CreateAccountString.fullName,Colors.black,25.0,FontWeight.w500,1),
          ],
        ),
        SizedBox(height: 5,),
        setCommonText(TKeys.dashbTitleNote.translate(context),Colors.grey,25.0,FontWeight.w500,2),
        SizedBox(height: 5,),
      ],
    ),
  );
}

  List<Event> events = [];






_setGridViewListing(){



  return new Container(
    height: 520,
    // color: Colors.yellow,
    padding: new EdgeInsets.all(20),
    child: new GridView.count(
      physics: NeverScrollableScrollPhysics(),
        crossAxisCount: 2,
        childAspectRatio: (3/2.5),
        children: new List<Widget>.generate(listData.length, (index) {
          return new GridTile(
            child: new InkWell(
                onTap: ()  async {



                  setState(() {
                    for(var i = 0; i < listData.length; i++){
                       listData[i]['isSelect'] = false;
                    }
                    listData[index]['isSelect'] = true;
                  });
                  // FindDoctorScreen
                  switch (index) {

                    case 0:
                      Navigator.of(context).push(MaterialPageRoute(builder: (context)=>FindQuestionnaireScreen()));
                      break;
                      case 1:
                        Navigator.of(context).push(MaterialPageRoute(builder: (context)=>QuestionnaireList()));
                      break;
                      case 2:
                      Navigator.of(context).push(MaterialPageRoute(builder: (context)=>Startgame()));
                      break;
                    default:
                  }
                },
                child: new Container(
                padding: new EdgeInsets.all(5),
                child: new Material(
                 color: (listData[index]['isSelect'])?AppColor.themeColor:Colors.black54,
                  elevation: 2.0,
                  borderRadius: BorderRadius.circular(5),
                    child: new Container(
                    padding: new EdgeInsets.all(12),
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                         listData[index]['icon'],
                         new Column(
                           mainAxisAlignment: MainAxisAlignment.start,
                           crossAxisAlignment: CrossAxisAlignment.start,
                           children: <Widget>[
                             setCommonText(listData[index]['title'], Colors.white,16.0, FontWeight.w700,2),
                             SizedBox(height: 3,),
                             setCommonText(listData[index]['availability'], Colors.white,12.0, FontWeight.w500,2),
                           ],
                         )
                      ],
                    ),
                  ),
                )
              ),
            ),
          );
        }),
      ),
    );
}

AppTranslationsDelegate _newLocaleDelegate;
  @override
  void initState() {
    super.initState();



  }
    final localizationController = Get.find<LocalizationController>();





     return GetBuilder<LocalizationController>(
        init: localizationController,
        builder: (LocalizationController controller){
        return MaterialApp(
          debugShowCheckedModeBanner:false,
          home: new Scaffold(
            appBar: new AppBar(
              centerTitle: true,
              title: setHeaderTitle(TKeys.appTitle.translate(context),Colors.white),
              backgroundColor: AppColor.themeColor,
              elevation: 1.0,
              actions: setCommonCartNitificationView(context),
            ),
            body: new Container(
              color: Colors.white,
              child: new ListView(
                children: <Widget>[
                  _setMainInformationView(),
                  _setGridViewListing()
                ],
              ),
            ),
          ),
          routes: {
            '/Dashboard': (BuildContext context) => DashboardScreen()
          },
          theme: SharedManager.shared.getThemeType(),
          locale: controller.currentLanguage != ''
              ? Locale(controller.currentLanguage, '')
              : null,
          localeResolutionCallback: LocalizationService.localeResolutionCallBack,
          supportedLocales: LocalizationService.supportedLocales,
          localizationsDelegates: LocalizationService.localizationsDelegate,
        );
      }
    );
  }

  //This is for localization

}

