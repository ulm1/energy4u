import 'package:moodyapp/Screens/TabBarScreens/Tabbar/Tabbar.dart';
import 'package:moodyapp/Screens/create_account_height/create_account_height_page.dart';
import 'package:moodyapp/Screens/create_account_weight/create_account_weight_page.dart';
import 'package:moodyapp/services/database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:moodyapp/Screens/contanst/contanst.dart';
import 'package:moodyapp/utils/utils.dart';

import 'package:provider/provider.dart';

import '../MyCustomDialogOne.dart';
import 'create_account_gender_page_model.dart';

class CreateAccountGenderPage extends StatelessWidget {
  CreateAccountGenderPage();

  GlobalKey _keyList = GlobalKey();
  String gender = "";

  // ignore: avoid_init_to_null
  static Widget ProviderPage() {
    return ChangeNotifierProvider<CreateAccountGenderPageModel>(
      create: (context) => CreateAccountGenderPageModel(),
      child: CreateAccountGenderPage(),
    );
  }

  static CreateAccountGenderPageModel of(BuildContext context) =>
      Provider.of<CreateAccountGenderPageModel>(context, listen: false);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Text("Personality Trait Questionnaire"),
        ),
        body: Container(
            padding: EdgeInsets.only(top: 35),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(bottom: 20, left: 30, right: 30),
                    child: Text(
                      "Choose your Gender:",
                      style: TextStyle(
                          color:  Colors.black,
                          fontSize: 18,
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                  Expanded(
                    key: _keyList,
                    child: Stack(
                      children: <Widget>[
                        Center(
                          child: Container(
                              height: 50,
                              decoration: BoxDecoration(
                                color: R.color.light_blue,
                                boxShadow: [
                                  BoxShadow(
                                    color: R.color.dark_black.withAlpha(75),
                                    blurRadius: 20.0,
                                    spreadRadius: 0.0,
                                    offset: Offset(
                                      1.0,
                                      1.0,
                                    ),
                                  )
                                ],
                              )),
                        ),
                        Container(
                          child: CupertinoPicker(
                            useMagnifier: false,
                            magnification: 0.7,
                            backgroundColor: Colors.transparent,
                            diameterRatio: 20.0,
                            children: <Widget>[
                              _ItemPicker("Male",
                                  selected: CreateAccountGenderPage.of(context)
                                          .genderSelected ==
                                      0),
                              _ItemPicker("Female",
                                  selected: CreateAccountGenderPage.of(context)
                                          .genderSelected ==
                                      1),
                              _ItemPicker("Non-Binary",
                                  selected: CreateAccountGenderPage.of(context)
                                          .genderSelected ==
                                      2),
                              _ItemPicker("Other",
                                  selected: CreateAccountGenderPage.of(context)
                                          .genderSelected ==
                                      3),
                            ],
                            itemExtent: 70,
                            onSelectedItemChanged: (int value) {
                              CreateAccountGenderPage.of(context)
                                  .updateGender(value);
                            },
                          ),
                          margin: EdgeInsets.only(left: 30, right: 30),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                        top: 20, bottom: 30, left: 30, right: 30),
                    child: button(
                        width: 250,
                        text: "Next",
                        onPressed: () {
                          var gender = CreateAccountGenderPage.of(context)
                              .genderSelected;

                          if (gender == 0) {
                            RouterName.gender = "Male";
                          }
                          if (gender == 1) {
                            RouterName.gender = "Female";
                          }
                          if (gender == 2) {
                            RouterName.gender = "Non-Binary";
                          }
                          if (gender == 3) {
                            RouterName.gender = "Other";
                          }


                          //final data = CreateAccountGenderPage.of(context).data;


                          showDialog(
                            context: context,
                            builder: (context) {
                              return CustomDialogOne(
                                title: "Congratulations!",
                                content: "You completed your first Questionnaire!",
                                negativeBtnText: "Done",
                              );
                            },
                          );


    Navigator.of(context).pushAndRemoveUntil(
    MaterialPageRoute(builder: (context) => TabBarScreen()),
    ModalRoute.withName('/TabBar'));
    },
                        ),
                  )
                ])));
  }
}

Widget _ItemPicker(String mss,
        {TextAlign textAlign = TextAlign.left, bool selected = false}) =>
    Container(
      alignment: Alignment.centerLeft,
      child: Text(
        mss,
        textAlign: textAlign,
        style: TextStyle(
            color: selected ?  Colors.black :  Colors.black, fontSize: 24),
      ),
    );
