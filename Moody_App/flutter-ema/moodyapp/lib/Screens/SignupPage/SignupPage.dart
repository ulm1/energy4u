import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:moodyapp/Localization/localization_service.dart';
import 'package:moodyapp/Localization/t_key.dart';
import 'package:moodyapp/Screens/LoginPage/ComonLoginWidgets.dart';
import 'package:moodyapp/Screens/TabBarScreens/Tabbar/Tabbar.dart';
import 'package:moodyapp/Screens/contanst/contanst.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:moodyapp/Helper/Constant.dart';
import 'package:moodyapp/Helper/SharedManager.dart';
import 'package:moodyapp/Localization/app_translations.dart';
import 'package:moodyapp/Screens/create_account_weight/create_account_weight_page.dart';
import 'package:moodyapp/data/Hive/userhive.dart';
import 'package:moodyapp/pages/boxes.dart';
import 'package:parse_server_sdk_flutter/parse_server_sdk.dart';
import 'package:random_string/random_string.dart';

import '../MyCustomDialogOne.dart';

void mian() => runApp(new SignupPage());

class SignupPage extends StatefulWidget {
  @override
  _SignupPageState createState() => _SignupPageState();
}

class _SignupPageState extends State<SignupPage> {
  final userName = TextEditingController();
  final password = TextEditingController();
  final email = TextEditingController();


  

  _setSignUPView(){
    return new Container(
      padding: new EdgeInsets.all(20),
      width: MediaQuery.of(context).size.width,
      // color: Colors.red,
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[

          SizedBox(height: 50,),
          setTextFiels1(TKeys.Email.translate(context),Icons.person,userName),

          SizedBox(height: 25,),
          setTextFiels1(TKeys.password.translate(context),Icons.lock,password),
          SizedBox(height: 25,),

          new InkWell(
            onTap: () async {
              bool emailValid = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(userName.text);

              if(!emailValid){
                showDialog(
                  context: context,
                  builder: (context) {
                    return CustomDialogOne(
                      title: TKeys.Pleaseuser.translate(context),
                      content:
                      TKeys.EmailNotValid.translate(context),
                      negativeBtnText: TKeys.done.translate(context),
                    );
                  },
                );
              }


              if (userName.text.isNotEmpty&&password.text.isNotEmpty) {


                QueryBuilder<ParseUser> queryUsers =
                QueryBuilder<ParseUser>(ParseUser.forQuery())..whereContains("username",userName.text);
                final ParseResponse apiResponse = await queryUsers.query();
                print(queryUsers);

                if (apiResponse.results != null) {
                  showDialog(
                    context: context,
                    builder: (context) {
                      return CustomDialogOne(
                        title: TKeys.Pleaseuser.translate(context),
                        content:
                        TKeys.EmailExists.translate(context),
                        negativeBtnText: TKeys.done.translate(context),
                      );
                    },
                  );
                }

                if (!apiResponse.success) {
                  showDialog(
                    context: context,
                    builder: (context) {
                      return CustomDialogOne(
                        title: TKeys.Pleaseuser.translate(context),
                        content:
                        TKeys.NoConnection.translate(context),
                        negativeBtnText: TKeys.done.translate(context),
                      );
                    },
                  );
                }

                if (apiResponse.success && apiResponse.results == null && emailValid) {
                  final ParseCloudFunction function = ParseCloudFunction('editUserProperty');
                  ParseUser currentUser = await ParseUser.currentUser();
                  final Map<String, dynamic> params = <String, dynamic>{
                    'objectId':  currentUser.objectId,
                    'email': userName.text,
                    'password': password.text

                  };
                  final ParseResponse parseResponse =
                  await function.executeObjectFunction<ParseObject>(parameters: params);
                  if (parseResponse.success && parseResponse.result != null) {
                    if (parseResponse.result['result'] is ParseObject) {
                      //Transforms the return into a ParseObject
                      final ParseObject parseObject = parseResponse.result['result'];
                      function.save();

                      final person = Boxes.getTransactions().get(0)
                        ..email = function.get("email")
                        ..password = function.get("password")
                        ..anonymous = function.get("anonymous")
                        ..gamification = function.get("gamification");
                        person.save();                    }
                  }




                  /*

                  ParseUser currentUser = await ParseUser.currentUser();
                  print(currentUser.username);

                  final Map<String, dynamic> params = <String, dynamic>{
                    'objectId' :currentUser.objectId,
                    'username': userName.text,
                  //  'password': password.text,
                   // 'gamification': false,
                  //  'anonymous': false
                  };
                  final ParseResponse parseResponse =
                  await function.execute(parameters: params);
                  if (parseResponse.success) {
                    print(parseResponse.result);
                    //currentUser.update();
                    print(currentUser.username);


                    currentUser.save();

                    print(currentUser.username);

                    final person = Boxes.getTransactions().get(0)
                      ..username = userName.text
                      ..password = password.text;
                    //  ..anonymous = object.get<bool>('anonymous')
                   //   ..gamification=object.get<bool>('gamification');

                  //  person.save();

                  }









                  ParseUser currentUser = await ParseUser.currentUser();

                  final ParseCloudFunction function = ParseCloudFunction('editUserProperty');
                  final Map<String, dynamic> params = <String, dynamic>{
                    'objectId' :currentUser.objectId,
                    'username': userName.text,
                    'password': password.text,
                    'gamification': '',
                    'anonymous': ''

                  };
                  final ParseResponse parseResponse =
                  await function.executeObjectFunction<ParseObject>(parameters: params);
                  if (parseResponse.success && parseResponse.result != null) {
                    if (parseResponse.result['result'] is ParseObject) {
                      //Transforms the return into a ParseObject
                      final ParseObject object = parseResponse.result['result'];
                      print(currentUser.get("username"));
                      print(object.get<String>('username'));


                        currentUser.get("username");

                        var dietPlan = ParseObject('_User')
                          ..objectId = currentUser.objectId
                          ..set('username', object.get<String>('username'))
                          ..set('password', object.get<String>('password'))
                          ..set('gamification',object.get<bool>('gamification'))
                          ..set('anonymous', object.get<bool>('anonymous'));

                        print(currentUser.get("gamification"));
                      print(object.get<String>('username'));

              await dietPlan..save();


                        final person = Boxes.getTransactions().get(0)
                          ..username = object.get<String>('username')
                          ..password = object.get<String>('password')
                          ..anonymous = object.get<bool>('anonymous')
                          ..gamification=object.get<bool>('gamification');

                      //                 person.save();

                      }


                  }


*/
            //      return apiResponse.results as List<ParseObject>;
                } else {

                   if (userName.text.isEmpty) {
                    showDialog(
                      context: context,
                      builder: (context) {
                        return CustomDialogOne(
                          title: TKeys.Pleaseuser.translate(context),
                          content:
                          TKeys.signupInfo.translate(context),
                          negativeBtnText: TKeys.done.translate(context),
                        );
                      },
                    );
                  }
                  if (password.text.isEmpty) {
                    showDialog(
                      context: context,
                      builder: (context) {
                        return CustomDialogOne(
                          title: TKeys.Pleaseuser.translate(context),
                          content:
                          TKeys.signupInfo.translate(context),
                          negativeBtnText: TKeys.done.translate(context),
                        );
                      },
                    );
                  }
                }
              }







                //  currentUser["email"] = email.text;
                //  currentUser["password"] = email.text;




                //  print(box.values.last.username);







            },
            child: new Container(
              height: 45,
              width: MediaQuery.of(context).size.width,
              child: new Material(
                color: AppColor.themeColor,
                borderRadius: BorderRadius.circular(22.5),
                elevation: 5.0,
                child: new Center(
                  child: new Text(TKeys.SignUp.translate(context),
                    textDirection: SharedManager.shared.direction,
                    style: new TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.w500
                    ),
                  ),
                ),
              ),
            ),
          ),
          SizedBox(height: 25,),

          SizedBox(height: 100,),
          new Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
            //  new Text(AppTranslations.of(context).text(AppTitle.signUpNote),
            new Text(TKeys.signUpNote.translate(context),

          textDirection: SharedManager.shared.direction,
                style: new TextStyle(
                    color: Colors.grey,
                    fontSize: 16,
                    fontWeight: FontWeight.w500
                ),
              ),
              SizedBox(width: 2,),
              InkWell(
                child: new Text(TKeys.signIN.translate(context),
                  textDirection: SharedManager.shared.direction,
                  style: new TextStyle(
                      color: AppColor.themeColor,
                      fontSize: 14,
                      fontWeight: FontWeight.w600
                  ),
                ),
                onTap: (){
                  Navigator.of(context).pop();
                },
              )
            ],
          )
        ],
      ),
    );
  }
  final localizationController = Get.put(LocalizationController());

  @override
  Widget build(BuildContext context) {
    return GetBuilder<LocalizationController>(
        init: localizationController,
        builder: (LocalizationController controller){
        return MaterialApp(
          debugShowCheckedModeBanner:false,
          home: new Scaffold(
            appBar: new AppBar(
              elevation: 0,
              backgroundColor: Colors.white,
              leading: new IconButton(
                icon: Icon(Icons.arrow_back_ios,color:AppColor.themeColor),
                onPressed: (){
                  Navigator.of(context).pop();
                },
              ),
            ),
            body: new Container(
              color: Colors.white,
              child: new ListView(
                children: <Widget>[
                  _setSignUPView()
                ],
              ),
            ),
          ),
          theme: SharedManager.shared.getThemeType(),
          locale: controller.currentLanguage != ''
              ? Locale(controller.currentLanguage, '')
              : null,
          localeResolutionCallback: LocalizationService.localeResolutionCallBack,
          supportedLocales: LocalizationService.supportedLocales,
          localizationsDelegates: LocalizationService.localizationsDelegate,
        );
      }
    );
  }
}