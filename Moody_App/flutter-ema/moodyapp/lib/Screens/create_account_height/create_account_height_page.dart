import 'package:moodyapp/Screens/create_account_weight/create_account_weight_page.dart';
import 'package:moodyapp/services/database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:moodyapp/Screens/contanst/contanst.dart';
import 'package:moodyapp/utils/utils.dart';

import 'package:provider/provider.dart';

import 'create_account_height_page_model.dart';


class CreateAccountHightPage extends StatelessWidget {
  CreateAccountHightPage();

  GlobalKey _keyList = GlobalKey();
  String gender = "";

  // ignore: avoid_init_to_null
  static Widget ProviderPage() {
    return ChangeNotifierProvider<CreateAccountHightPageModel>(
      create: (context) => CreateAccountHightPageModel(),
      child: CreateAccountHightPage(),
    );
  }

  static CreateAccountHightPageModel of(BuildContext context) =>
      Provider.of<CreateAccountHightPageModel>(context, listen: false);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Text("Personality Trait Questionnaire"),
        ),
        body: Container(
            padding: EdgeInsets.only(top: 35),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(bottom: 20, left: 30, right: 30),
                    child: Text(
                      "Do you have any common eye conditions?",
                      style: TextStyle(
                          color:  Colors.black,
                          fontSize: 18,
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                  Expanded(
                    key: _keyList,
                    child: Stack(
                      children: <Widget>[
                        Center(
                          child: Container(
                              height: 50,
                              decoration: BoxDecoration(
                                color: R.color.light_blue,
                                boxShadow: [
                                  BoxShadow(
                                    color: R.color.dark_black.withAlpha(75),
                                    blurRadius: 20.0,
                                    spreadRadius: 0.0,
                                    offset: Offset(
                                      1.0,
                                      1.0,
                                    ),
                                  )
                                ],
                              )),
                        ),
                        Container(
                          child: CupertinoPicker(
                            useMagnifier: false,
                            magnification: 0.7,
                            backgroundColor: Colors.transparent,
                            diameterRatio: 20.0,
                            children: <Widget>[
                              _ItemPicker("None",
                                  selected: CreateAccountHightPage.of(context)
                                      .genderSelected ==
                                      0),
                              _ItemPicker("Nearsightedness",
                                  selected: CreateAccountHightPage.of(context)
                                      .genderSelected ==
                                      1),
                              _ItemPicker("Farsightedness",
                                  selected: CreateAccountHightPage.of(context)
                                      .genderSelected ==
                                      2),
                              _ItemPicker("Amblyopia",
                                  selected: CreateAccountHightPage.of(context)
                                      .genderSelected ==
                                      3),
                              _ItemPicker("Cataract",
                                  selected: CreateAccountHightPage.of(context)
                                      .genderSelected ==
                                      4),
                              _ItemPicker("Diabetic Retinopathy",
                                  selected: CreateAccountHightPage.of(context)
                                      .genderSelected ==
                                      5),
                              _ItemPicker("Glaucoma",
                                  selected: CreateAccountHightPage.of(context)
                                      .genderSelected ==
                                      6),
                              _ItemPicker("Other",
                                  selected: CreateAccountHightPage.of(context)
                                      .genderSelected ==
                                      7),
                            ],
                            itemExtent: 70,
                            onSelectedItemChanged: (int value) {
                              CreateAccountHightPage.of(context)
                                  .updateGender(value);
                            },
                          ),
                          margin: EdgeInsets.only(left: 30, right: 30),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                        top: 20, bottom: 30, left: 30, right: 30),
                    child: button(
                        width: 250,
                        text: "Next",
                        onPressed: () {
                          var gender = CreateAccountHightPage.of(context)
                              .genderSelected;

                          if (gender == 0) {
                            RouterName.eyeProblems = "None";
                          }
                          if (gender == 1) {
                            RouterName.eyeProblems = "Nearsightedness";
                          }
                          if (gender == 2) {
                            RouterName.eyeProblems = "Farsightedness";
                          }
                          if (gender == 3) {
                            RouterName.eyeProblems = "Amblyopia";
                          }
                          if (gender == 4) {
                            RouterName.eyeProblems = "Cataract";
                          }
                          if (gender == 5) {
                            RouterName.eyeProblems = "Diabetic Retinopathy";
                          }
                          if (gender == 6) {
                            RouterName.eyeProblems = "Glaucoma";
                          }
                          if (gender == 7) {
                            RouterName.eyeProblems = "Other";
                          }



                          print(RouterName.gender);
                          //final data = CreateAccountGenderPage.of(context).data;

                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      CreateAccountWeightPageModel()));
                        }),
                  )
                ])));
  }
}

Widget _ItemPicker(String mss,
    {TextAlign textAlign = TextAlign.left, bool selected = false}) =>
    Container(
      alignment: Alignment.centerLeft,
      child: Text(
        mss,
        textAlign: textAlign,
        style: TextStyle(
            color: selected ?  Colors.black :  Colors.black, fontSize: 24),
      ),
    );
