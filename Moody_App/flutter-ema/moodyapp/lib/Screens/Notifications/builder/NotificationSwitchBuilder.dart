import 'package:flutter/material.dart';

import '../../../Helper/Constant.dart';
import '../../../Localization/t_key.dart';
import '../../../notification_manager.dart';
import '../utils/notificationHelper.dart';


class NotificationSwitchBuilder extends StatefulWidget {
  @override
  _NotificationSwitchBuilderState createState() =>
      _NotificationSwitchBuilderState();
}

class _NotificationSwitchBuilderState extends State<NotificationSwitchBuilder> {
  bool isSwitched = true;



  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Center(
          child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
            Switch(
              value: isSwitched,
              onChanged: (value) {
                if (!value) {
                  turnOffNotification(flutterLocalNotificationsPlugin);
                }
                setState(() {
                  isSwitched = value;
                });
              },
              activeTrackColor: Colors.lightBlueAccent,
              activeColor: AppColor.themeColor,
            ),
            Text(
                isSwitched? TKeys.On.translate(context):TKeys.Off.translate(context),
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ])),
    );
  }
}
