import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';


import '../../../Helper/Constant.dart';
import '../../../main.dart';
import '../actions/actions.dart';
import '../mainNotification.dart';
import '../models/Reminder.dart';
import '../store/store.dart';
import '../utils/notificationHelper.dart';
import 'ReminderAlertBuilder.dart';

class RemindersList extends StatelessWidget {
  final List<Reminder> reminders;
  RemindersList({this.reminders});

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
        separatorBuilder: (context, index) {
          return Divider();
        },
        itemCount: reminders.length,
        itemBuilder: (context, index) {
          final item = reminders[index];
          return Card(
            child: ListTile(
                title: Row(
                  children: <Widget>[

                    Text(item.name)
                  ],
                ),
                subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.only(top: 10),
                          child: Row(
                            children: <Widget>[
                              Text(
                                "Start time: ",
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              Text(df.format(DateTime.parse(item.time))),
                              Expanded(
                                flex: 5,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [

                                    IconButton(
                                      icon: Icon(Icons.delete),
                                      iconSize: 48,
                                      color: AppColor.themeColor,
                                      onPressed: () {
                                        _configureCustomReminder(false);

                                      },
                                    ),


                                  ],
                                ),
                              )

                            ],
                          )),
                      Text(Reminder.parseRepeatIntervalToString(item.repeat))
                    ])),
          );
        });
    ;
  }
}

void _configureCustomReminder(bool value) {
  getStore().dispatch(RemoveReminderAction(custom));


      getStore().dispatch(RemoveReminderAction(custom));
  turnOffNotification(flutterLocalNotificationsPlugin);
    }

