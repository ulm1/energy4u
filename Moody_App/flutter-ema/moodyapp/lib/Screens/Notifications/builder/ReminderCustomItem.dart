import 'package:flutter/material.dart';

import '../../../Helper/Constant.dart';
import '../../../Localization/t_key.dart';
import 'ReminderAlertBuilder.dart';

class ReminderCustomItem2 extends StatelessWidget {
  final bool checkBoxValue;
  final void Function(bool) onChanged;
  final String iconName;
  final void Function() showTimeDialog;
  ReminderCustomItem2(
      {this.checkBoxValue, this.onChanged, this.iconName, this.showTimeDialog});

  @override
  Widget build(BuildContext context) {
    return Card(
        margin: new EdgeInsets.only(left: 0, right: 0, bottom: 10),
        child: CheckboxListTile(
            value: checkBoxValue,
            onChanged: onChanged,
            title: Row(children: <Widget>[
              Row(
                children: <Widget>[   Column(
                  children: <Widget>[

                    SizedBox(
                      child: Padding(
                          padding: new EdgeInsets.only(top: 10, bottom: 10),
                          child: ElevatedButton(
                            child: Text(
                              'SET TIME',
                              style: TextStyle(
                                  fontSize: 10, color: Colors.white),
                            ),
                            onPressed: showTimeDialog,
                          )),
                      width: 90,
                      height: 40,
                    )
                  ],
                )

                ],
              )
            ])));
  }
}

class ReminderCustomItem extends StatelessWidget {

  final bool checkBoxValue;
  final void Function(bool) onChanged;
  final String iconName;
  final void Function() showTimeDialog;
  ReminderCustomItem(
      {this.checkBoxValue, this.onChanged, this.iconName, this.showTimeDialog});

  Widget _buildDialogContent(BuildContext context) {
    return Stack(
      alignment: Alignment.topCenter,
      children: <Widget>[
        Container(
          // Bottom rectangular box
          margin:
          EdgeInsets.only(top: 40), // to push the box half way below circle
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(12),
          ),
          padding: EdgeInsets.only(
              top: 60, left: 20, right: 20), // spacing inside the box
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[

              ButtonBar(
                buttonMinWidth: 100,
                alignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  ElevatedButton(
                    child: Text(
                      TKeys.SetTime.translate(context),
                      style: TextStyle(
                          fontSize: 10, color: Colors.white),
                    ),
                    onPressed: showTimeDialog,
                  )
                ],
              ),
            ],
          ),
        ),
        CircleAvatar(
          // Top Circle with icon
          maxRadius: 40.0,
          child: Icon(Icons.message),
          backgroundColor: AppColor.themeColor,
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: _buildDialogContent(context),
    );
  }
}