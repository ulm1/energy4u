import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';


import '../../../Helper/Constant.dart';
import '../../../Localization/t_key.dart';
import '../../../notification_manager.dart';
import '../actions/actions.dart';
import '../models/Reminder.dart';
import '../store/store.dart';
import '../utils/notificationHelper.dart';
import 'ReminderCustomItem.dart';
import 'ReminderItem.dart';



const String playMusic = 'Play music';
const String lookAfterPlants = 'Look after plants';
const String walk = '5 min walk';
const String questionnaireNotify = 'Time to take a Questionnaire';
const String custom = 'Questionnaire Time';

const remindersIcons = {
  playMusic: Icons.audiotrack,
  lookAfterPlants: Icons.local_florist,
  walk: Icons.directions_walk,
  questionnaireNotify: Icons.local_drink,
  custom: Icons.image,
};

class ReminderAlertBuilder extends StatefulWidget {
  ReminderAlertBuilder({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _ReminderAlertBuilderState createState() => _ReminderAlertBuilderState();
}



class _ReminderAlertBuilderState extends State<ReminderAlertBuilder> {
  bool playMusicReminder = false;
  bool lookAfterPlantsReminder = false;
  bool walkFor5minReminder = false;
  bool drinkSomeWaterReminder = false;
  bool customReminder = false;
  double margin = Platform.isIOS ? 10 : 5;

  TimeOfDay customNotificationTime;

  @override
  Widget build(BuildContext context) {
    _prepareState();
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          IconButton(
            icon: Icon(Icons.edit),
            iconSize: 48,
            color: AppColor.themeColor,
            onPressed: () {
              _showMaterialDialog();

            },
          ),
        ],
      ),
    );
  }






  _showMaterialDialog() {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.0))),
              contentPadding: EdgeInsets.all(0.0),
              backgroundColor: Colors.white,
              content: StatefulBuilder(
                  builder: (BuildContext context, StateSetter setState) {
                    return Center(
                      child: Container(
                        width: MediaQuery.of(context).size.width - 10,
                        height: MediaQuery.of(context).size.height - 80,
                        padding: EdgeInsets.all(20),
                        color: Colors.white,
                        child: Column(
                          children: [

                            Padding(
                                padding: new EdgeInsets.only(
                                    bottom: margin, top: margin),
                                child: Text(
                                  TKeys.Notifications.translate(context),
                                  style: TextStyle(
                                      fontSize: 20,
                                      color: Colors.black,
                                      decoration: TextDecoration.none,
                                      fontWeight: FontWeight.w500),
                                )),
                            ReminderCustomItem(
                              checkBoxValue: customReminder,
                              iconName: custom,
                              onChanged: (value) {
                                setState(() {
                                  customReminder = value;
                                });
                                _configureCustomReminder(value);
                              },
                              showTimeDialog: () {
                                _showTimeDialog(setState);
                              },
                            ),
                            Padding(
                              padding: new EdgeInsets.only(
                                  top: margin * 2, bottom: margin),
                              child: ElevatedButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                  child: Text(
                                      TKeys.Done.translate(context),

                                    style: TextStyle(color: Colors.white),
                                  )),
                            )
                          ],
                        ),
                      ),
                    );
                  }));
        });
  }

  _showTimeDialog(StateSetter setState) async {
    TimeOfDay selectedTime = await showTimePicker(
      initialTime: TimeOfDay.now(),
      context: context,
    );

    setState(() {
      customNotificationTime = selectedTime;
      customReminder = true;
    });

    _configureCustomReminder(true);
  }

  _prepareState() {
    List<Reminder> list = getStore().state.remindersState.reminders;

    list.forEach((item) {
      switch (item.name) {
        case playMusic:
          playMusicReminder = true;
          break;
        case lookAfterPlants:
          lookAfterPlantsReminder = true;
          break;
        case walk:
          walkFor5minReminder = true;
          break;
        case questionnaireNotify:
          drinkSomeWaterReminder = true;
          break;
        case custom:
          customReminder = true;
          break;
        default:
          return;
      }
    });
  }

  void _configurePlayMusic(bool value) {
    if (value) {
      getStore().dispatch(SetReminderAction(
          time: new DateTime.now().toIso8601String(),
          name: playMusic,
          repeat: RepeatInterval.daily));

      scheduleNotificationPeriodically(flutterLocalNotificationsPlugin, '0',
          playMusic, RepeatInterval.daily);
    } else {
      turnOffNotificationById(flutterLocalNotificationsPlugin, 0);
      getStore().dispatch(RemoveReminderAction(playMusic));
    }
  }

  void _configureLookAfterPlants(bool value) {
    if (value) {
      getStore().dispatch(SetReminderAction(
          time: new DateTime.now().toIso8601String(),
          name: lookAfterPlants,
          repeat: RepeatInterval.daily));
      scheduleNotificationPeriodically(flutterLocalNotificationsPlugin, '1',
          lookAfterPlants, RepeatInterval.weekly);
    } else {
      getStore().dispatch(RemoveReminderAction(lookAfterPlants));
      turnOffNotificationById(flutterLocalNotificationsPlugin, 1);
    }
  }

  void _configure5minWalk(bool value) {
    if (value) {
      getStore().dispatch(SetReminderAction(
          time: new DateTime.now().toIso8601String(),
          name: walk,
          repeat: RepeatInterval.hourly));
      scheduleNotificationPeriodically(
          flutterLocalNotificationsPlugin, '2', walk, RepeatInterval.hourly);
    } else {
      getStore().dispatch(RemoveReminderAction(walk));
      turnOffNotificationById(flutterLocalNotificationsPlugin, 2);
    }
  }

  void _configureDrinkSomeWater(bool value) {
    if (value) {
      getStore().dispatch(SetReminderAction(
          time: new DateTime.now().toIso8601String(),
          name: questionnaireNotify,
          repeat: RepeatInterval.everyMinute));
      scheduleNotificationPeriodically(flutterLocalNotificationsPlugin, '3',
          questionnaireNotify, RepeatInterval.everyMinute);
    } else {
      getStore().dispatch(RemoveReminderAction(questionnaireNotify));
      turnOffNotificationById(flutterLocalNotificationsPlugin, 3);
    }
  }

  void _configureCustomReminder(bool value) {
    getStore().dispatch(RemoveReminderAction(custom));

    if (customNotificationTime != null) {
      if (value) {
        var now = new DateTime.now();
        var notificationTime = new DateTime(now.year, now.month, now.day,
            customNotificationTime.hour, customNotificationTime.minute);

        if (notificationTime.isBefore(now)) {
          notificationTime = notificationTime.add(const Duration(days: 1));
        }

        getStore().dispatch(SetReminderAction(
            time: notificationTime.toIso8601String(),
            name: custom,
            repeat: RepeatInterval.daily));


        Timer(notificationTime.add(Duration(seconds: 1)).difference(DateTime.now()), () =>   scheduleNotificationPeriodically(flutterLocalNotificationsPlugin, '3',
            questionnaireNotify, RepeatInterval.daily));

      } else {
        getStore().dispatch(RemoveReminderAction(custom));
        turnOffNotification(flutterLocalNotificationsPlugin);
      }
    }
  }
}

