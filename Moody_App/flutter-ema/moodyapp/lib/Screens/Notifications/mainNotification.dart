import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';

import 'package:intl/intl.dart';
import 'package:moodyapp/Screens/Notifications/models/Reminder.dart';
import 'package:moodyapp/Screens/Notifications/store/AppState.dart';
import 'package:moodyapp/Screens/Notifications/store/store.dart';
import 'package:moodyapp/Screens/Notifications/utils/notificationHelper.dart';
import 'package:redux/redux.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import '../../Helper/CommonWidgets/CommonWidgets.dart';
import '../../Helper/Constant.dart';
import '../../Localization/localization_service.dart';
import '../../Localization/t_key.dart';
import '../../main.dart';
import 'builder/NotificationSwitchBuilder.dart';
import 'builder/ReminderAlertBuilder.dart';
import 'builder/RemindersListViewBuilder.dart';
import 'models/index.dart';



class mainNotification extends StatefulWidget {
  _WelcomePage createState() => _WelcomePage();
}

class _WelcomePage extends State<mainNotification> {

  @override
  void initState() {
    super.initState();
  }


  final localizationController = Get.put(LocalizationController());



  @override
  Widget build(BuildContext context) {
    return GetBuilder<LocalizationController>(
        init: localizationController,
        builder: (LocalizationController controller){
        return StoreProvider<AppState>(
          child: MaterialApp(
            debugShowCheckedModeBanner: false,

            title: 'REMINDERS',
              home: Scaffold(
              appBar: new AppBar(
                centerTitle: true,
                // title: setHeaderTitle(AppTranslations.of(context).text(AppTitle.appTitle),Colors.white),
                title: setHeaderTitle(
                    TKeys.Notifications.translate(context),
                    Colors.white),
                backgroundColor: AppColor.themeColor,
                elevation: 1.0,
                leading: new IconButton(
                  icon: Icon(Icons.arrow_back_ios, color: Colors.white),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ),
                body: Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Padding(
                              padding: EdgeInsets.all(10),
                              child: ReminderAlertBuilder()),
                          Padding(
                              padding: EdgeInsets.all(10),
                              child: NotificationSwitchBuilder()),
                        ],
                      ),
                      Padding(
                          padding: EdgeInsets.all(10),
                          child: Text(
                            TKeys.Notifications.translate(context),
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          )),
                      Padding(
                          padding: EdgeInsets.all(20),
                          child: Container(
                              decoration: BoxDecoration(
                                border: Border.all(
                                  color: Colors.white,
                                  width: 2,
                                ),
                                borderRadius:
                                BorderRadius.all(Radius.circular(15.0)),
                              ),
                              child: SizedBox(
                                child: StoreConnector<AppState, List<Reminder>>(
                                    converter: (store) =>
                                    store.state.remindersState.reminders,
                                    builder: (context, reminders) {
                                      return RemindersList(reminders: reminders);
                                    }),
                                height: Platform.isAndroid ? 420 : 550,
                              ))),
                    ],
                  ),
                ),
              ),
          locale: controller.currentLanguage != ''
              ? Locale(controller.currentLanguage, '')
              : null,
          localeResolutionCallback: LocalizationService.localeResolutionCallBack,
          supportedLocales: LocalizationService.supportedLocales,
          localizationsDelegates: LocalizationService.localizationsDelegate,




          ),
          store: mystore,
        );

      }

    );

    ;
  }
}