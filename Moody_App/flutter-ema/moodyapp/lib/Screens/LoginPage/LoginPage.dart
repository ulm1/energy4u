import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:moodyapp/Constant/sharedpreference_page.dart';
import 'package:moodyapp/Localization/app_translations_delegate.dart';
import 'package:moodyapp/Localization/application.dart';
import 'package:moodyapp/Localization/localization_service.dart';
import 'package:moodyapp/Localization/t_key.dart';
import 'package:moodyapp/Screens/SignupPage/SignupPage.dart';
import 'package:moodyapp/Screens/TabBarScreens/Tabbar/Tabbar.dart';
import 'package:moodyapp/Screens/contanst/contanst.dart';
import 'package:moodyapp/services/database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:moodyapp/Helper/Constant.dart';
import 'package:moodyapp/Helper/SharedManager.dart';
import 'package:moodyapp/Localization/app_translations.dart';

import '../../Helper/CommonWidgets/CommonWidgets.dart';
import '../../globals.dart';
import '../MyCustomDialogOne.dart';
import 'ComonLoginWidgets.dart';

bool isLoading = false;
String firebaseId;
final userName = TextEditingController();
final password = TextEditingController();
bool value = false;
bool _passwordVisible = false;

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {




  _setLoginView() {
    return new Container(
      padding: new EdgeInsets.all(20),
      // height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      // color: Colors.red,
      child: isLoading
          ? Container(
              child: Center(child: CircularProgressIndicator()),
            )
          : new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[

                SizedBox(
                  height: 25,
                ),
                SizedBox(
                  height: 25,
                ),
                SizedBox(
                  height: 50,
                ),
                setTextFiels1(
                  TKeys.loginUserName.translate(context),
                  Icons.person,
                  userName,
                ),
                SizedBox(
                  height: 50,
                ),
                setTextFiels1(TKeys.password.translate(context),Icons.lock,password),


                SizedBox(
                  height: 25,
                ),
                /*
                new Row(
                  children: <Widget>[
                    new Checkbox(
                      value: value,
                      onChanged: (val) {
                        setState(() {
                          value = val;
                        });
                      },
                    ),
                    new Text(
                      AppTranslations.of(context).text("rememberMe"),
                      textDirection: SharedManager.shared.direction,
                      style: new TextStyle(
                          color: Colors.grey,
                          fontSize: 16,
                          fontWeight: FontWeight.w500),
                    ),
                  ],
                ),

                 */
                SizedBox(
                  height: 25,
                ),
                new InkWell(
                  onTap: () {
                    if (userName.text.isNotEmpty&&password.text.isNotEmpty) {
                      callApi();
                    }
                    if (userName.text.isEmpty) {
                      showDialog(
                        context: context,
                        builder: (context) {
                          return CustomDialogOne(
                            title: TKeys.Pleaseuser.translate(context),
                            content:
                            TKeys.signupInfo.translate(context),
                            negativeBtnText: TKeys.done.translate(context),
                          );
                        },
                      );
                    }
                    if (password.text.isEmpty) {
                      showDialog(
                        context: context,
                        builder: (context) {
                          return CustomDialogOne(
                            title: TKeys.Pleaseuser.translate(context),
                            content:
                            TKeys.signupInfo.translate(context),
                            negativeBtnText: TKeys.done.translate(context),
                          );
                        },
                      );
                    }

                  },
                  // Navigator.of(context).push(MaterialPageRoute(builder: (context)=>TabBarScreen()));
                  child: new Container(
                    height: 45,
                    width: MediaQuery.of(context).size.width,
                    child: new Material(
                      color: AppColor.themeColor,
                      borderRadius: BorderRadius.circular(22.5),
                      elevation: 5.0,
                      child: new Center(
                        child: new Text(
                          TKeys.signIN.translate(context),
                          style: new TextStyle(
                              color: Colors.white,
                              fontSize: 16,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 25,
                ),
                SizedBox(
                  height: 20,
                ),
                SizedBox(
                  height: 100,
                ),
                new Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    new Text(
                      TKeys.loginDontHaveAccount.translate(context),
                      textDirection: SharedManager.shared.direction,
                      style: new TextStyle(
                          color: Colors.grey,
                          fontSize: 16,
                          fontWeight: FontWeight.w500),
                    ),
                    SizedBox(
                      width: 2,
                    ),
                    InkWell(
                      child: new Text(
                        TKeys.SignUp.translate(context),
                        textDirection: SharedManager.shared.direction,
                        style: new TextStyle(
                            color: AppColor.themeColor,
                            fontSize: 14,
                            fontWeight: FontWeight.w600),
                      ),
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => SignupPage()));
                      },
                    )
                  ],
                )
              ],
            ),
    );
  }

  callApi() async {
    // if (_submit()) {
    if (userName.text != null && userName.text != null) {
      try {
        setState(() {
          isLoading = true;
        });

        prefrenceObjects.setString(SharedPreferenceKey.FIREBASEID, firebaseId);
        RouterName.usern = userName.text;
        RouterName.id = userName.text;
        print(userName.text);
        setState(() {
          isLoading = false;
        });
        Navigator.of(context).push(
            MaterialPageRoute(builder: (context) => TabBarScreen()));
        // Navigator.push(
        //   context,
        //   MaterialPageRoute(
        //     builder: (context) => IntroSlide(),
        //   ),
        // );
        return null;
        // }
      } catch (e) {
        setState(() {
          isLoading = false;
        });
        print("hello" + e.toString());
        showDialog(
          context: context,
          builder: (context) {
            return CustomDialogOne(
              title: TKeys.WrongUsername.translate(context),
              content: TKeys.reEnterUsername.translate(context),
              negativeBtnText: TKeys.done.translate(context),
            );
          },
        );

        /*
            final snackBar = SnackBar(
              content: Text('Wrong Username'),
              action: SnackBarAction(
                label: '',
                onPressed: () {
                  // Some code to undo the change.
                },
              ),
            );
            */

        // Find the Scaffold in the widget tree and use
        // it to show a SnackBar.
        // Scaffold.of(context).showSnackBar(snackBar);
      }
    } else {
      _showAlert(TKeys.allInfo.translate(context), "", "");
    }
  }

  // Alert Box
  void _showAlert(String value, String userName, String password) {
    @override
    Widget build(BuildContext context) {
      return RaisedButton(
          child: Text("Button moved to separate widget"),
          onPressed: () {
            Scaffold.of(context).showSnackBar(SnackBar(
              content: Text('Button moved to separate widget'),
              duration: Duration(seconds: 3),
            ));
          });
    }
  }

  AppTranslationsDelegate _newLocaleDelegate;
  @override
  void initState() {
    super.initState();
    _passwordVisible = false;

    _newLocaleDelegate = AppTranslationsDelegate(newLocale: null);
    application.onLocaleChanged = onLocaleChange;
  }
  final localizationController = Get.put(LocalizationController());

  @override
  Widget build(BuildContext context) {
    return GetBuilder<LocalizationController>(
        init: localizationController,
        builder: (LocalizationController controller){
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          home: new Scaffold(
            appBar: new AppBar(
              centerTitle: true,
              // title: setHeaderTitle(AppTranslations.of(context).text(AppTitle.appTitle),Colors.white),
              title: setHeaderTitle(
                  TKeys.signIN.translate(context),
                  Colors.white),
              backgroundColor: AppColor.themeColor,
              elevation: 1.0,
              leading: new IconButton(
                icon: Icon(Icons.arrow_back_ios, color: Colors.white),
                onPressed: () {
                  Navigator.of(context, rootNavigator: true).pop(context);                },
              ),
            ),

            body: new Container(
                child: new ListView(
              children: <Widget>[
                _setLoginView(),
              ],
            )),
          ),
          routes: {'/login': (BuildContext context) => LoginPage()},
          theme: SharedManager.shared.getThemeType(),
          locale: controller.currentLanguage != ''
              ? Locale(controller.currentLanguage, '')
              : null,
          localeResolutionCallback: LocalizationService.localeResolutionCallBack,
          supportedLocales: LocalizationService.supportedLocales,
          localizationsDelegates: LocalizationService.localizationsDelegate,
        );
      }
    );
  }

  void onLocaleChange(Locale locale) {
    setState(() {
      _newLocaleDelegate = AppTranslationsDelegate(newLocale: locale);
    });
  }

  void getMyUsername() async {
    print(text);
    if (text.isNotEmpty) {
      value = true;
      userName.text = text;
    }
  }
}
