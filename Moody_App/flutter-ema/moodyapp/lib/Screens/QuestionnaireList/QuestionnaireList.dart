import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:moodyapp/Helper/CommonWidgets/CommonWidgets.dart';
import 'package:moodyapp/Helper/Constant.dart';
import 'package:moodyapp/Helper/SharedManager.dart';
import 'package:moodyapp/Localization/app_translations.dart';
import 'package:moodyapp/Localization/app_translations_delegate.dart';
import 'package:moodyapp/Localization/application.dart';
import 'package:moodyapp/Localization/localization_service.dart';
import 'package:moodyapp/Localization/t_key.dart';
import 'package:moodyapp/Screens/contanst/contanst.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'package:table_calendar/table_calendar.dart';

void main() => runApp(new QuestionnaireList());

class QuestionnaireList extends StatefulWidget {
  @override
  _QuestionnaireListState createState() => _QuestionnaireListState();
}

class _QuestionnaireListState extends State<QuestionnaireList>
    with TickerProviderStateMixin {
  final Map<DateTime, List> _holidays = {
    DateTime(2019, 1, 1): ['New Year\'s Day'],
    DateTime(2019, 1, 6): ['Epiphany'],
    DateTime(2019, 2, 14): ['Valentine\'s Day'],
    DateTime(2019, 4, 21): ['Easter Sunday'],
    DateTime(2019, 4, 22): ['Easter Monday'],
  };

  Map<DateTime, List> _events;
  List selectedEvents;
  AnimationController _animationController;
  CalendarController _calendarController;
 // final firestoreInstance = FirebaseFirestore.instance;

  @override
  void initState() {
    super.initState();
    final _selectedDay = DateTime.now();
/*
    firestoreInstance
        .collection("users")
        .doc(RouterName.id)
        .collection("Quiz")
        .get()
        .then((querySnapshot) {
      querySnapshot.docs.forEach((result) {
        print("KKKKK" + result.get('quizTitle'));

        result.get('quizTitle');
      });
    });

 */

    // final _selectedDay22 = DateTime.parse(_selectedDay2.toString());
    _events = {
      //   _selectedDay22.add(Duration(hours:2, minutes:3, seconds:2)): ['Event'],
    };

    selectedEvents = _events[_selectedDay] ?? [];
    _calendarController = CalendarController();

    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 400),
    );

    _animationController.forward();


  }

  @override
  void dispose() {
    _animationController.dispose();
    _calendarController.dispose();
    super.dispose();
  }

  void _onDaySelected(DateTime day, List events, _) {
    print('CALLBACK: _onDaySelected');
    setState(() {
      selectedEvents = events;
    });
  }

  void _onVisibleDaysChanged(
      DateTime first, DateTime last, CalendarFormat format) {
    print('CALLBACK: _onVisibleDaysChanged');
  }

  _setColenderView() {
    return new Container(
      child: new Material(
        color: AppColor.themeColor,
        elevation: 10.0,
        child: TableCalendar(
          calendarController: _calendarController,
          events: _events,
          holidays: _holidays,
          startingDayOfWeek: StartingDayOfWeek.monday,
          daysOfWeekStyle: DaysOfWeekStyle(
              weekdayStyle: TextStyle(color: Colors.white),
              weekendStyle: TextStyle(color: Colors.white)),
          calendarStyle: CalendarStyle(
            weekdayStyle: TextStyle(
              color: Colors.white,
            ),
            weekendStyle:
                TextStyle(color: Colors.white, fontWeight: FontWeight.w600),
            selectedColor: Colors.deepOrange[400],
            todayColor: Colors.orange[400],
            markersColor: Colors.black,
            outsideDaysVisible: false,
          ),
          headerStyle: HeaderStyle(
            formatButtonTextStyle:
                TextStyle().copyWith(color: Colors.white, fontSize: 15.0),
            formatButtonDecoration: BoxDecoration(
              color: Colors.deepOrange,
              borderRadius: BorderRadius.circular(16.0),
            ),
          ),
          onDaySelected: _onDaySelected,
          onVisibleDaysChanged: _onVisibleDaysChanged,
        ),
      ),
    );
  }

  _setAppointmentList() {
    return Container(
      color: Colors.grey[100],
      height: 550,
      padding: new EdgeInsets.all(20),
      child: new ListView.builder(
        physics: NeverScrollableScrollPhysics(),
        itemCount: 1,
        itemBuilder: (context, index) {
          return getAppointmentListView(index, context);
        },
      ),
    );
  }

  final localizationController = Get.put(LocalizationController());
  @override
  Widget build(BuildContext context) {
    return GetBuilder<LocalizationController>(
        init: localizationController,
        builder: (LocalizationController controller){
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          home: new Scaffold(
            body: new Container(
              color: Colors.grey[100],
              child: ListView(
                children: <Widget>[
                  _setColenderView(),
                  SizedBox(
                    height: 20,
                  ),
                  _setAppointmentList()
                ],
              ),
            ),
            appBar: new AppBar(
              centerTitle: true,
              // title: setHeaderTitle(AppTranslations.of(context).text(AppTitle.appTitle),Colors.white),
              title: setHeaderTitle(
                  TKeys.dashbAppointment.translate(context),
                  Colors.white),
              backgroundColor: AppColor.themeColor,
              elevation: 1.0,
              leading: new IconButton(
                icon: Icon(Icons.arrow_back_ios, color: Colors.white),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ),
          ),
          theme: SharedManager.shared.getThemeType(),
          locale: controller.currentLanguage != ''
              ? Locale(controller.currentLanguage, '')
              : null,
          localeResolutionCallback: LocalizationService.localeResolutionCallBack,
          supportedLocales: LocalizationService.supportedLocales,
          localizationsDelegates: LocalizationService.localizationsDelegate,
        );
      }
    );
  }

  //This is for localization

}
