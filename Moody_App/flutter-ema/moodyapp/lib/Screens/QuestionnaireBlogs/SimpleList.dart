import 'package:flutter/material.dart';

import '../../globals.dart';

class SimpleList extends StatefulWidget {
  @override
  _SimpleList createState() => _SimpleList();
}

class _SimpleList extends State<SimpleList> {
  List<String> inputs = List<String>();

  @override
  void initState() {
    super.initState();
    setState(() {
      // Items For List
      inputs.add("Movie");
      inputs.add("Food");
      inputs.add("Song");
      inputs.add("Dance");
      inputs.add("Actor");
      inputs.add("Artist");
      inputs.add("Travelling");
      inputs.add("Study");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: appColor,
        title: MediumText(text: 'List'),
      ),
      body: ListView.builder(
          itemCount: inputs.length,
          itemBuilder: (BuildContext context, int index) {
            return Container(
              padding: EdgeInsets.only(left: 10.0, right: 10.0),
              child: Column(
                children: <Widget>[
                  // Item Container
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: LightText(text: inputs[index]),
                      ),
                      IconButton(
                        icon: Icon(Icons.arrow_forward),
                        color: Colors.grey,
                        onPressed: () {},
                      ),
                    ],
                  ),
                  Divider(
                    color: Colors.grey[600],
                  ),
                ],
              ),
            );
          }),
    );
  }
}
