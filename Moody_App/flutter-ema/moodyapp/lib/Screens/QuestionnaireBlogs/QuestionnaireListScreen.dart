import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:moodyapp/Localization/localization_service.dart';
import 'package:moodyapp/Localization/t_key.dart';
import 'package:moodyapp/Screens/contanst/contanst.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:moodyapp/Helper/CommonWidgets/CommonWidgets.dart';
import 'package:moodyapp/Helper/Constant.dart';
import 'package:moodyapp/Helper/SharedManager.dart';
import 'package:moodyapp/Localization/app_translations.dart';
import 'package:moodyapp/Localization/app_translations_delegate.dart';
import 'package:moodyapp/Localization/application.dart';

import '../../globals.dart';
import 'QuestList.dart';

void main() => runApp(new QuestBlogsScreen("firebaseI"));


class QuestBlogsScreen extends StatefulWidget {

  final String firebaseId;

  QuestBlogsScreen(this.firebaseId);

  @override
  _QuestBlogsScreenState createState() => _QuestBlogsScreenState();
}

class _QuestBlogsScreenState extends State<QuestBlogsScreen> {

  AppTranslationsDelegate _newLocaleDelegate;
  @override
  void initState() {
    super.initState();
    _newLocaleDelegate = AppTranslationsDelegate(newLocale:null);
    application.onLocaleChanged = onLocaleChange;
  }
  final localizationController = Get.put(LocalizationController());

  @override
  Widget build(BuildContext context) {
    return GetBuilder<LocalizationController>(
        init: localizationController,
        builder: (LocalizationController controller){
        return MaterialApp(
          debugShowCheckedModeBanner:false,
          home: DefaultTabController(
            length: 1,
            initialIndex: 0,
            child: Scaffold(
              appBar: new AppBar(
                centerTitle: true,
                // title: setHeaderTitle(AppTranslations.of(context).text(AppTitle.appTitle),Colors.white),
                title: setHeaderTitle(TKeys.drawerBlogs.translate(context),Colors.white),
                backgroundColor: AppColor.themeColor,
                elevation: 1.0,
                bottom: TabBar(
                  indicatorColor: Colors.red,
                  tabs: <Widget>[
                    Tab(
                      text:"",
                    ),
                  ],
                ),
                actions: setCommonCartNitificationView(context),
              ),
              body: TabBarView(
                children: <Widget>[
                  QuestList(),
                ],
              ),
            ),
          ),
          routes: {
            '/DrugsBlogsScreen': (BuildContext context) => QuestBlogsScreen(RouterName.id.toString())
          },
          theme: SharedManager.shared.getThemeType(),
          locale: controller.currentLanguage != ''
              ? Locale(controller.currentLanguage, '')
              : null,
          localeResolutionCallback: LocalizationService.localeResolutionCallBack,
          supportedLocales: LocalizationService.supportedLocales,
          localizationsDelegates: LocalizationService.localizationsDelegate,
        );
      }
    );
  }
  //This is for localization
  void onLocaleChange(Locale locale) {
    setState(() {
      _newLocaleDelegate = AppTranslationsDelegate(newLocale: locale);
    });
  }

}