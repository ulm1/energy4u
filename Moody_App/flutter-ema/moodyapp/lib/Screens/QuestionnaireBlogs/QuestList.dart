import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:moodyapp/QUESTIONNAIRE/models/question_model.dart';
import 'package:moodyapp/Screens/IndicatorList/TestIndicators2.dart';
import 'package:moodyapp/Screens/QuestDetails/QuestDetails.dart';
import 'package:moodyapp/Screens/TabBarScreens/Indicators/TestIndicators.dart';
import 'package:moodyapp/Screens/contanst/contanst.dart';
import 'package:moodyapp/pages/questionnaire.dart';
import 'package:moodyapp/services/database.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:moodyapp/Helper/Constant.dart';
import 'package:moodyapp/Helper/SharedManager.dart';
import 'package:moodyapp/Localization/app_translations_delegate.dart';
import 'package:moodyapp/Localization/application.dart';
import 'package:intl/intl.dart';

import '../../main.dart';
import 'CommonBLogs.dart';

void main() => runApp(new QuestList());

class QuestList extends StatefulWidget {
  Stream quizStream;
//  DatabaseService databaseService = new DatabaseService();
  @override
  _QuestListState createState() => _QuestListState();
}

class QuestionnaireTile extends StatelessWidget {
  final String imageUrl, title, quizId, description, firebaseId;
  final int noOfQuestions;

  QuestionnaireTile(
      {@required this.title,
      this.imageUrl,
      @required this.description,
      @required this.quizId,
      @required this.noOfQuestions,
      @required this.firebaseId});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    Questionnaire(quizId,"")));
      },
      child: Card(
        child: Column(
          children: <Widget>[
            ListTile(
              leading: CircleAvatar(
                backgroundImage: NetworkImage(imageUrl),
              ),
              title: Text(title),
              subtitle: Text(quizId),
              trailing: Text(description),
            ),
          ],
        ),
      ),
    );
  }
}

class _QuestListState extends State<QuestList> {
  Stream quizStream;
 // DatabaseService databaseService = new DatabaseService();
  Box contactBox;

  // Delete info from people box
  _deleteInfo(int index) {
    contactBox.deleteAt(index);

    print('Item deleted from box at index: $index');
  }

  @override
  void initState() {
    super.initState();
    // Get reference to an already opened box
    contactBox = Hive.box<QuestionnaireModel>('question_model');
  }

  quizList() {
    return ValueListenableBuilder(
        valueListenable: contactBox.listenable(),
        builder: (context, Box box, widget) {
          if (box.isEmpty) {
            return Center(
              child: Text('Empty'),
            );
          } else {
            return ListView.builder(
                itemCount: box.length,
                itemBuilder: (context, index) {
                  var currentBox = box;
                  var personData = currentBox.getAt(index);
                  return SizedBox(
                      height: 200.0,
                      child: InkWell(
                      onTap: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => QuestScreen(
                        index: index,
                        person: personData.questions,
                        personData: personData,
                          update: false

                      ),
                    ),
                  ),
                  child: QuestionnaireTile(
                  noOfQuestions: personData.length,
                  imageUrl:
                  "https://i.pinimg.com/564x/a6/22/55/a6225503c05cebb2b3763ab9583ecdf5.jpg",
                  title:personData.questionnaireName.toString(),
                  description: personData.questionnaireName.toString(),
                  quizId:personData.questionnaireDate)));
            },
          );
            };});


  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);
    final width = MediaQuery.of(context).size.width;
    var dummytext =
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry.";
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: new Scaffold(
        body: StreamBuilder(
            stream: quizStream,
            builder: (context, snapshot) {
              return snapshot.data == null
                  ? Container()
                  : Container(
                      color: Colors.grey[200],
                      child: new GridView.count(
                        crossAxisCount: 1,
                        children: List<Widget>.generate(
                            snapshot.data.documents.length, (index) {
                          var id = snapshot.data.documents[index]
                              .get("quizId")
                              .toString();
                          var ind = index + 1;
                          var date =
                              snapshot.data.documents[index].get('quizTitle');
                          var moonLanding = DateTime.parse(date); // 8:18pm

                          // DateTime now = DateTime(date);
                          //  final DateTime now = DateTime(int.tryParse(date));
                          final DateFormat formatter = DateFormat('yyyy-MM-dd');
                          //  final String formatted = formatter.format(now);
                          String date2 = new DateFormat.yMMMd()
                              .add_jm()
                              .format(moonLanding)
                              .toString();
                          return new Hero(
                            tag: index,
                            child: new InkWell(
                              onTap: () {
                                Navigator.of(context, rootNavigator: false)
                                    .push(MaterialPageRoute(
                                        builder: (context) => TestIndicatorDetails()));
                              },
                              child: setCommonBlog(
                                  AppImage.blogFoodImage,
                                  "Questionnaire " + ind.toString(),
                                  date2,
                                  2,
                                  2),
                            ),
                          );
                        }),
                      ));
            }),
      ),
      theme: SharedManager.shared.getThemeType(),
      localizationsDelegates: [
        //provides localised strings
        GlobalMaterialLocalizations.delegate,
        //provides RTL support
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [SharedManager.shared.language],
    );
  }

  AppTranslationsDelegate _newLocaleDelegate;

  @override
  /*

  void initState() {
    databaseService.getQuizData(RouterName.id.toString()).then((value) {
      setState(() {
        quizStream = value;
      });
    });
    super.initState();
    _newLocaleDelegate = AppTranslationsDelegate(newLocale: null);
    application.onLocaleChanged = onLocaleChange;
  }

   */

  //This is for localization
  void onLocaleChange(Locale locale) {
    setState(() {
      _newLocaleDelegate = AppTranslationsDelegate(newLocale: locale);
    });
  }
}
