import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart' as noti;
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:html_character_entities/html_character_entities.dart';
import 'package:http/src/response.dart';
import 'package:intl/intl.dart';
import 'package:moodyapp/pages/result.dart';
import 'package:moodyapp/utils/api_service.dart';
import 'package:moodyapp/utils/api_service.dart';
import 'package:moodyapp/utils/api_service.dart';
import 'package:moodyapp/widgets/network_status_service.dart';
import 'package:moodyapp/network_aware_widget.dart';
import 'package:get/get.dart';
import 'package:random_string/random_string.dart';
import 'package:moodyapp/data/Hive/userhive.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import 'package:syncfusion_flutter_sliders/sliders.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'Helper/CommonWidgets/CommonWidgets.dart';
import 'Helper/Constant.dart';
import 'Helper/SharedManager.dart';
import 'Localization/app_translations.dart';
import 'Localization/app_translations_delegate.dart';
import 'Localization/application.dart';
import 'Localization/localization_service.dart';
import 'Localization/t_key.dart';
import 'QUESTIONNAIRE/controllers/quiz/quiz_controller.dart';
import 'QUESTIONNAIRE/controllers/quiz/quiz_state.dart';
import 'QUESTIONNAIRE/enums/difficulty.dart';
import 'QUESTIONNAIRE/models/failure_model.dart';
import 'QUESTIONNAIRE/models/question_model.dart';
import 'QUESTIONNAIRE/repositories/quiz/quiz_repository.dart';
import 'Screens/LoginPage/LoginPage.dart';
import 'Screens/MyCustomDialogOne.dart';
import 'Screens/Notifications/store/AppState.dart';
import 'Screens/Notifications/store/store.dart';
import 'Screens/Notifications/utils/notificationHelper.dart';
import 'Screens/TabBarScreens/Tabbar/Tabbar.dart';
import 'Screens/contanst/contanst.dart';
import 'Screens/create_account/create_account_page.dart';
import 'Screens/create_account_birthday/create_account_birthday_page.dart';
import 'Screens/create_account_fullname/create_account_fullname_page.dart';
import 'Screens/create_account_gender/create_account_gender_page.dart';
import 'Screens/create_account_height/create_account_height_page.dart';
import 'boxes.dart';
import 'data/Hive/userhive.dart' as trans;
import 'data/base/api_response.dart';
import 'data/model/diet_plan.dart';
import 'data/model/user.dart';
import 'data/repositories/diet_plan/contract_provider_diet_plan.dart';
import 'data/repositories/diet_plan/repository_diet_plan.dart';
import 'data/repositories/user/repository_user.dart';
import 'domain/constants/application_constants.dart';
import 'domain/utils/db_utils.dart';
import 'models/question_model.dart';
import 'pages/decision_page.dart';
import 'package:parse_server_sdk_flutter/parse_server_sdk.dart';
import 'package:provider/provider.dart' as p;

import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:moodyapp/widgets/questionnaire_play_widgets.dart';
import 'package:moodyapp/pages/boxes.dart' as boxes;
import 'data/repositories/diet_plan/repository_diet_plan.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart' as redu;

const String SETTINGS_BOX = "settings";
const String connected = "connected";

const String SignUp_BOX = "signUp";
const String questionBox = "QuestionAnsweredBox";
final df = new DateFormat('dd-MM-yyyy hh:mm a');

bool exists;
var questionmodelhive;
final noti.FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
noti.FlutterLocalNotificationsPlugin();
noti.NotificationAppLaunchDetails notificationAppLaunchDetails;
redu.Store<AppState> mystore;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  loadData();
  await initStore();
  mystore = getStore();
  notificationAppLaunchDetails =
  await flutterLocalNotificationsPlugin.getNotificationAppLaunchDetails();
  await initNotifications(flutterLocalNotificationsPlugin);
  requestIOSPermissions(flutterLocalNotificationsPlugin);


  await Hive.initFlutter();
  Hive.registerAdapter(AnswerOptionAdapter());
  Hive.registerAdapter(AnswerOptionQuestionAdapter());
  Hive.registerAdapter(FluffyAnswerOptionAdapter());
  Hive.registerAdapter(PersonAdapter());
  Hive.registerAdapter(QuestionnaireModelAdapter());
  Hive.registerAdapter(QuestionAdapter());
  Hive.registerAdapter(AnswerTextAdapter());
  Hive.registerAdapter(QuestionTextAdapter());


  await Hive.openBox(SETTINGS_BOX);
  await Hive.openBox(connected);

  await Hive.openBox(SignUp_BOX);
  await Hive.openBox(questionBox);
  await Hive.openBox<Person>('userhive');
  await Hive.openBox<QuestionnaireModel>('question_model');


  exists = await Hive.boxExists('userhive');


  final box = boxes.Boxes.getTransactions();
  // box.clear();
  print(box.isEmpty);
  _setTargetPlatformForDesktop();

  if (box.isEmpty) {
    print("randomkey generate");
    String random = randomAlphaNumeric(16);
    final username = random;
    final email = username + "@anonymous.com";
    final password = username;
    addPerson(username, password, email, true);
  }


  runApp(ProviderScope(child: MyApp()));
}

Future addPerson(String username, String password, String email,
    bool anonymous) async {
  final person = Person()
    ..username = username
    ..password = password
    ..email = email
    ..anonymous = anonymous;

  final box = boxes.Boxes.getTransactions();

  box.add(person);
  print("Added to mainBox" + box.getAt(0).toString());
}

void _setTargetPlatformForDesktop() {
  TargetPlatform targetPlatform;

  if (Platform.isMacOS) {
    targetPlatform = TargetPlatform.iOS;
  } else if (Platform.isLinux || Platform.isWindows) {
    targetPlatform = TargetPlatform.android;
  }
  if (targetPlatform != null) {
    debugDefaultTargetPlatformOverride = targetPlatform;
  }
}

final localizationController = Get.put(LocalizationController());

class MyApp extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return GetBuilder<LocalizationController>(
        init: localizationController,
        builder: (LocalizationController controller) {
          return MaterialApp(
            title: '',
            home: MainScreen(),
            debugShowCheckedModeBanner: false,
            locale: controller.currentLanguage != ''
                ? Locale(controller.currentLanguage, '')
                : null,
            localeResolutionCallback: LocalizationService
                .localeResolutionCallBack,
            supportedLocales: LocalizationService.supportedLocales,
            localizationsDelegates: LocalizationService.localizationsDelegate,
          );
        }
    );
  }
}

class MyApps2 extends StatefulWidget {
  @override
  _MyAppState2 createState() => _MyAppState2();
}

class _MyAppState2 extends State<MyApps2> {
  AppTranslationsDelegate _newLocaleDelegate;
  final _controller = new PageController();
  static const _kDuration = const Duration(milliseconds: 300);
  static const _kCurve = Curves.ease;
  final _kArrowColor = Colors.black;
  final List<Widget> _pages = <Widget>[LoginPage()];

  @override
  void initState() {
    super.initState();
    _newLocaleDelegate = AppTranslationsDelegate(newLocale: null);
    application.onLocaleChanged = onLocaleChange;
  }

  final localizationController = Get.put(LocalizationController());

  @override
  Widget build(BuildContext context) {
    return GetBuilder<LocalizationController>(
        init: localizationController,
        builder: (LocalizationController controller) {
          return new MaterialApp(
              debugShowCheckedModeBanner: false,
              home: new Scaffold(
                body: new IconTheme(
                  data: new IconThemeData(color: _kArrowColor),
                  child: SharedManager.shared.isOnboarding
                      ? LoginPage()
                      : Container(
                    color: Colors.white,
                    child: new Stack(
                      children: <Widget>[
                        new PageView.builder(
                          physics: new AlwaysScrollableScrollPhysics(),
                          controller: _controller,
                          itemCount: _pages.length,
                          itemBuilder: (BuildContext context, int index) {
                            return _pages[index % _pages.length];
                          },
                        ),
                        new Positioned(
                          bottom: 0.0,
                          left: 0.0,
                          right: 0.0,
                          child: new Container(
                            color: AppColor.themeColor.withOpacity(0.0),
                            padding: const EdgeInsets.all(20.0),
                            child: new Center(
                              child: new DotsIndicator(
                                controller: _controller,
                                itemCount: _pages.length,
                                color: AppColor.themeColor,
                                onPageSelected: (int page) {
                                  _controller.animateToPage(
                                    page,
                                    duration: _kDuration,
                                    curve: _kCurve,
                                  );
                                },
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              locale: controller.currentLanguage != ''
                  ? Locale(controller.currentLanguage, '')
                  : null,
              localeResolutionCallback: LocalizationService
                  .localeResolutionCallBack,
              supportedLocales: LocalizationService.supportedLocales,
              localizationsDelegates: LocalizationService.localizationsDelegate,
              routes: {
                RouterName.CREATE_ACCOUNT: (context) =>
                    CreateAccountPage.ProviderPage(),
                RouterName.CREATE_ACCOUNT_BIRTHDAY: (context) =>
                    CreateAccountBirthdayPage.ProviderPage(),
                RouterName.CREATE_ACCOUNT_GENDER: (context) =>
                    CreateAccountGenderPage.ProviderPage(),
                RouterName.CREATE_ACCOUNT_FULLNAME: (context) =>
                    CreateAccountFullnamePage.ProviderPage(),
                RouterName.CREATE_ACCOUNT_HEIGHT: (context) =>
                    CreateAccountHightPage.ProviderPage(),

                //RouterName.CREATE_ACCOUNT_HEIGHT: (context) =>
                //   CreateAccountHeightPage.ProviderPage(),

                // '/MyHomePage': (BuildContext context) => MyHomePage(),
              },
              theme: SharedManager.shared.getThemeType());
        }
    );
  }

  //This is for localization
  void onLocaleChange(Locale locale) {
    setState(() {
      _newLocaleDelegate = AppTranslationsDelegate(newLocale: locale);
    });
  }
}

class MyApps3 extends StatefulWidget {
  @override
  _MyAppState3 createState() => _MyAppState3();
}

class _MyAppState3 extends State<MyApps3> {
  AppTranslationsDelegate _newLocaleDelegate;
  final _controller = new PageController();
  static const _kDuration = const Duration(milliseconds: 300);
  static const _kCurve = Curves.ease;
  final _kArrowColor = Colors.black;
  final List<Widget> _pages = <Widget>[LoginPage()];

  @override
  void initState() {
    super.initState();
    _newLocaleDelegate = AppTranslationsDelegate(newLocale: null);
    application.onLocaleChanged = onLocaleChange;
  }

  final localizationController = Get.put(LocalizationController());

  @override
  Widget build(BuildContext context) {
    return GetBuilder<LocalizationController>(
        init: localizationController,
        builder: (LocalizationController controller) {
          return new MaterialApp(
              debugShowCheckedModeBanner: false,
              home: new Scaffold(
                body: new IconTheme(
                  data: new IconThemeData(color: _kArrowColor),
                  child: SharedManager.shared.isOnboarding
                      ? CreateAccountPage.ProviderPage()
                      : Container(
                    color: Colors.white,
                    child: new Stack(
                      children: <Widget>[
                        new PageView.builder(
                          physics: new AlwaysScrollableScrollPhysics(),
                          controller: _controller,
                          itemCount: _pages.length,
                          itemBuilder: (BuildContext context, int index) {
                            return _pages[index % _pages.length];
                          },
                        ),
                        new Positioned(
                          bottom: 0.0,
                          left: 0.0,
                          right: 0.0,
                          child: new Container(
                            color: AppColor.themeColor.withOpacity(0.0),
                            padding: const EdgeInsets.all(20.0),
                            child: new Center(
                              child: new DotsIndicator(
                                controller: _controller,
                                itemCount: _pages.length,
                                color: AppColor.themeColor,
                                onPageSelected: (int page) {
                                  _controller.animateToPage(
                                    page,
                                    duration: _kDuration,
                                    curve: _kCurve,
                                  );
                                },
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              locale: controller.currentLanguage != ''
                  ? Locale(controller.currentLanguage, '')
                  : null,
              localeResolutionCallback: LocalizationService
                  .localeResolutionCallBack,
              supportedLocales: LocalizationService.supportedLocales,
              localizationsDelegates: LocalizationService.localizationsDelegate,
              routes: {
                RouterName.CREATE_ACCOUNT: (context) =>
                    CreateAccountPage.ProviderPage(),
                RouterName.CREATE_ACCOUNT_BIRTHDAY: (context) =>
                    CreateAccountBirthdayPage.ProviderPage(),
                RouterName.CREATE_ACCOUNT_GENDER: (context) =>
                    CreateAccountGenderPage.ProviderPage(),
                RouterName.CREATE_ACCOUNT_FULLNAME: (context) =>
                    CreateAccountFullnamePage.ProviderPage(),
                RouterName.CREATE_ACCOUNT_HEIGHT: (context) =>
                    CreateAccountHightPage.ProviderPage(),

                //RouterName.CREATE_ACCOUNT_HEIGHT: (context) =>
                //   CreateAccountHeightPage.ProviderPage(),

                // '/MyHomePage': (BuildContext context) => MyHomePage(),
              },
              theme: SharedManager.shared.getThemeType());
        }
    );
  }

  //This is for localization
  void onLocaleChange(Locale locale) {
    setState(() {
      _newLocaleDelegate = AppTranslationsDelegate(newLocale: locale);
    });
  }
}

class DotsIndicator extends AnimatedWidget {
  DotsIndicator({
    this.controller,
    this.itemCount,
    this.onPageSelected,
    this.color: Colors.white,
  }) : super(listenable: controller);

  /// The PageController that this DotsIndicator is representing.
  final PageController controller;

  /// The number of items managed by the PageController
  final int itemCount;

  /// Called when a dot is tapped
  final ValueChanged<int> onPageSelected;

  /// The color of the dots.
  ///
  /// Defaults to `Colors.white`.
  final Color color;

  // The base size of the dots
  static const double _kDotSize = 8.0;

  // The increase in the size of the selected dot
  static const double _kMaxZoom = 2.0;

  // The distance between the center of each dot
  static const double _kDotSpacing = 25.0;

  Widget _buildDot(int index) {
    double selectedness = Curves.easeOut.transform(
      max(
        0.0,
        1.0 - ((controller.page ?? controller.initialPage) - index).abs(),
      ),
    );
    double zoom = 1.0 + (_kMaxZoom - 1.0) * selectedness;
    return new Container(
      width: _kDotSpacing,
      child: new Center(
        child: new Material(
          color: color,
          type: MaterialType.circle,
          child: new Container(
            width: _kDotSize * zoom,
            height: _kDotSize * zoom,
            child: new InkWell(
              onTap: () => onPageSelected(index),
            ),
          ),
        ),
      ),
    );
  }

  Widget build(BuildContext context) {
    return new Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: new List<Widget>.generate(itemCount, _buildDot),
    );
  }
}

class MainScreen extends StatelessWidget {

  final localizationController = Get.put(LocalizationController());

  @override
  Widget build(BuildContext context) {
    return GetBuilder<LocalizationController>(
        init: localizationController,
        builder: (LocalizationController controller) {
          return MaterialApp(
              debugShowCheckedModeBanner: false,
              title: '',
              theme: ThemeData(
                primarySwatch: Colors.blue,
              ),
              locale: controller.currentLanguage != ''
                  ? Locale(controller.currentLanguage, '')
                  : null,
              localeResolutionCallback: LocalizationService
                  .localeResolutionCallBack,
              supportedLocales: LocalizationService.supportedLocales,
              localizationsDelegates: LocalizationService.localizationsDelegate,
              home: ValueListenableBuilder(
                valueListenable: Hive.box(SETTINGS_BOX).listenable(),
                builder: (context, box, child) =>
                box.get('welcome_shown', defaultValue: false)
                    ? MyApps()
                    : WelcomePage(),
              ));
        }
    );
  }
}


// This page is shown once when the app is build  (My Onboarding Screen)
class WelcomePage extends StatefulWidget {
  _WelcomePage createState() => _WelcomePage();
}

class _WelcomePage extends State<WelcomePage> {
  WebViewController _controller;
  bool privacy = false;
  bool license = false;
  bool home = false;
  bool vis = false;
  String text = '';


  @override
  Widget build(BuildContext context) {
    if (privacy == false && license == false) {
      setState(() {
        text = TKeys.termstitle.translate(context);
      });
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(text),
      ),
      body: Container(
        child: p.StreamProvider<NetworkStatus>(
          create: (context) =>
          NetworkStatusService().networkStatusController.stream,
          child: NetworkAwareWidget(
            onlineChild: Container(
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                      child: WebView(
                        initialUrl: 'https://www.dynamic-project.de/terms/',
                        javascriptMode: JavascriptMode.unrestricted,
                        onWebViewCreated: (
                            WebViewController webViewController) {
                          // Get reference to WebView controller to access it globally
                          _controller = webViewController;
                        },
                        javascriptChannels: <JavascriptChannel>[
                          // Set Javascript Channel to WebView
                          _extractDataJSChannel(context),
                        ].toSet(),
                        onPageStarted: (String url) {
                          print('Page started loading: $url');
                        },
                        onPageFinished: (String url) {
                          print('Page finished loading: $url');
                          // In the final result page we check the url to make sure  it is the last page.
                          if (url.contains('/finalresponse.html')) {
                            // ignore: deprecated_member_use
                            _controller.evaluateJavascript(
                                "(function(){Flutter.postMessage(window.document.body.outerHTML)})();");
                          }
                        },
                      ),
                    ),
                    FloatingActionButton(
                      onPressed: () async {
                        print("helloss" + NetworkStatusService()
                            .networkStatusController.stream.toString());
                        if (home == true) {
                          var box = Hive.box(SETTINGS_BOX);
                          box.put("welcome_shown", true);
                        }

                        if (license == true) {
                          setState(() {
                            _controller.loadUrl(
                                'https://www.dynamic-project.de/licenses/');
                            text = TKeys.Licenses.translate(context);
                            license = false;
                            home = true;
                          });
                        }

                        if (privacy == false) {
                          setState(() {
                            _controller.loadUrl(
                                'https://www.dynamic-project.de/privacy/');
                            privacy = true;
                            license = true;
                            text = TKeys.Privacy.translate(context);
                          });
                        }
                      },
                      child: Icon(Icons.check),
                    )
                  ],
                ),
              ),
            ),
            offlineChild: Container(
              child: Center(
                child: Text(
                  TKeys.Internet.translate(context),
                  style: TextStyle(
                      color: Colors.grey[400],
                      fontWeight: FontWeight.w600,
                      fontSize: 20.0),
                ),
              ),
            ),
          ),
        ),
      ),

    );
  }

  JavascriptChannel _extractDataJSChannel(BuildContext context) {
    return JavascriptChannel(
      name: 'Flutter',
      onMessageReceived: (JavascriptMessage message) {
        print(message);
        String pageBody = message.message;
        print('page body: $pageBody');
      },
    );
  }
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => MyApps()));


    print("The user json file:   " + exists.toString());
    return Scaffold(
      appBar: AppBar(
        title: Text('Home page'),
      ),
      body: ListView(
        padding: const EdgeInsets.all(16.0),
        children: <Widget>[
          Text("This is a home page"),
          ElevatedButton(
            onPressed: () {
              Hive.box(SETTINGS_BOX).put('welcome_shown', false);
            },
            child: Text("Clear"),
          ),
          ElevatedButton(
            onPressed: () {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (context) => MyApps()));
            },
            child: Text("Connect to Database"),
          )
        ],
      ),
    );
  }
}

// conecting to Database
class MyApps extends StatefulWidget {

  MyApps();

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApps> {
  DietPlanRepository dietPlanRepo;
  UserRepository userRepo;
  Box box;
  Box contactBox;

  String text = '';

  @override
  void initState() {
    box = Hive.box(questionBox);
    contactBox = Boxes.getQuestionnaire();

    super.initState();
    initData();
  }


  @override
  Widget build(BuildContext context) {
    print(box.get('person'));


    return new WillPopScope(
        onWillPop: () async => false,
        child: ValueListenableBuilder(
          valueListenable: Hive.box(questionBox).listenable(),
          builder: (context, box, child) =>
          box.get('question_shown', defaultValue: true)
              ? DecisionPage() :
          //InfoScreen(contactBox.getAt(contactBox.length-1).questions)
          QuestScreen(
              index: contactBox.length - 1,
              person: contactBox
                  .getAt(contactBox.length - 1)
                  .questions,
              personData: contactBox.getAt(contactBox.length - 1),
              update: false

          )
          ,
        ));
  }


  Future<void> initData() async {
    // Initialize repository
    await initRepository();
    await initCoreStore();

    // Initialize parse
    await Parse().initialize(keyParseApplicationId, keyParseServerUrl,
        clientKey: keyParseClientKey,
        debug: true,
        coreStore: await CoreStoreSharedPrefsImp.getInstance());

    //parse serve with secure store and desktop support

    //    Parse().initialize(keyParseApplicationId, keyParseServerUrl,
    //        clientKey: keyParseClientKey,
    //        debug: true,
    //        coreStore: CoreStoreSharedPrefsImp.getInstance());

    // Check server is healthy and live - Debug is on in this instance so check logs for result


    var nbmer = 0;
    var channel;
    Timer timer;


    connect() async {
      Box contactBox = Hive.box<QuestionnaireModel>('question_model');


      final ParseCloudFunction function = ParseCloudFunction('sumNumbers');
      final Map<String, dynamic> params = <String, dynamic>{
        'title': 'Task 1',
        'done': false
      };
      final ParseResponse parseResponse =
      await function.executeObjectFunction<ParseObject>(parameters: params);
      if (parseResponse.success && parseResponse.result != null) {
        box.put("online", parseResponse.success);

        if (parseResponse.result['result'] is ParseObject) {
          //Transforms the return into a ParseObject
          final ParseObject parseObject = parseResponse.result['result'];
          print(parseObject.objectId);
        }
      }
      final ParseResponse response = await Parse().healthCheck();
      if (response.success) {
        Box contactBox = Hive.box<QuestionnaireModel>('question_model');

        //  print("Boxlength" + contactBox.length.toString());
        final ParseUser currentUser = await ParseUser.currentUser();
        if (currentUser == null) {
          //_redirectToPage(context, HomePage(DietPlanProviderApi()));

          Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => DecisionPage()));
        }


        QueryBuilder<ParseObject> queryPlayers =
        QueryBuilder<ParseObject>(ParseObject('Data'))
          ..whereEqualTo('username', currentUser.username);
        var apiResponse = await queryPlayers.count();
        if (apiResponse.success) {
          int countGames = apiResponse.count;
          //   print("mycounter" + countGames.toString());
          if (contactBox.length > countGames) {
            while (contactBox.length > countGames && contactBox
                .getAt(contactBox.length - countGames - 1)
                .answeredQuestions == true) {
              int i = contactBox.length - countGames - 1;

              var parseObject = ParseObject("Data")
                ..set("questionnaireType",
                    contactBox
                        .getAt(i)
                        .questionnaireType)..set
                  ("questionnaireName",
                    contactBox
                        .getAt(i)
                        .questionnaireName)..set(
                    "questionnaireTitle",
                    contactBox
                        .getAt(i)
                        .questionnaireTitle)..set(
                    "questionnaireId", contactBox
                    .getAt(i)
                    .questionnaireId)..set(
                    "questionnaireDescription",
                    contactBox
                        .getAt(i)
                        .questionnaireDescription)..set(
                    "questionnaireDate",
                    contactBox
                        .getAt(i)
                        .questionnaireDate)..set(
                    "questionnaireVersion",
                    contactBox
                        .getAt(i)
                        .questionnaireVersion)..set(
                    "questions", contactBox
                    .getAt(i)
                    .questions)..set(
                    "username", currentUser.username);

              final ParseResponse parseResponse = await parseObject.save();

              var dietPlan = ParseObject('_User')
                ..objectId = currentUser.objectId
                ..set(
                    'questionnaire',
                    (ParseObject('Data')
                      ..objectId = parseObject.objectId)
                        .toPointer());
              await dietPlan.save();


              if (parseResponse.success) {
                currentUser.objectId =
                    (parseResponse.results.first as ParseObject).objectId;
                countGames++;
                // print('Object created: $currentUser.objectId');
              } else {
                new WillPopScope(
                    onWillPop: () async => false,
                    child: ValueListenableBuilder(
                      valueListenable: Hive.box(questionBox).listenable(),
                      builder: (context, box, child) =>
                      box.get('question_shown', defaultValue: true)
                          ? DecisionPage() :
                      //InfoScreen(contactBox.getAt(contactBox.length-1).questions)
                      QuestScreen(
                          index: contactBox.length - 1,
                          person: contactBox
                              .getAt(contactBox.length - 1)
                              .questions,
                          personData: contactBox.getAt(contactBox.length - 1),
                          update: false

                      )
                      ,
                    ));

                //      print('Object created with failed: ${parseResponse.error
                //  .toString()}');
              }
            }
          }
        }

        if (apiResponse.success && apiResponse.results != null) {
          return DecisionPage();
        } else {
          return [];
        }
      }


      if (response.success) {
        await runTestQueries();
        text += 'runTestQueries\n';
        // print("online");
      }
      else if (!response.success) {
        //  print("offline");
      }
    }

    try {
      ValueListenableBuilder(
        valueListenable: Hive.box(SETTINGS_BOX).listenable(),
        builder: (context, box, child) =>
        box.get('online', defaultValue: false)
            ? timer = Timer.periodic(Duration(seconds: 80), (Timer t) => connect())
            : DecisionPage(),
      );

    } catch (e) {
      // print(e.toString());
    }


    void doSaveData() async {

    }

    void doReadData() async {

    }

    void doUpdateData() async {

    }


/*
    if (response.success) {
      await runTestQueries();
      text += 'runTestQueries\n';
      print(text);
    } else  if (!response.success) {
      while (!response.success) {
        Timer.periodic(new Duration(seconds: 5), (timer) async {
          await Parse().healthCheck();

          debugPrint(timer.tick.toString());
        });
      }
      return;
    }

 */

  }

  Future<void> runTestQueries() async {
    // Basic repository example
    //await repositoryAddUser();
    // await repositoryAddItems();
    await repositoryGetAllItems();

    //Basic usage
    //await createItem();
    //await getAllItems();
    //await getAllItemsByName();
    //await getSingleItem();
    //await getConfigs();
    //await query();
    //await initUser();
    //await initInstallation();
    //await function();
    //await functionWithParameters();
    //await test();
  }

  Future<void> initInstallation() async {
    final ParseInstallation installation =
    await ParseInstallation.currentInstallation();
    final ParseResponse response = await installation.create();
    print(response);
  }

  Future<void> test() async {
    User user = User('test_user', 'test_password', 'test@gmail.com');
    final ParseResponse signUpResponse = await user.signUp();

    if (signUpResponse.success) {
      user = signUpResponse.result;
    } else {
      final ParseResponse loginResponse = await user.login();

      if (loginResponse.success) {
        user = loginResponse.result;
      }
    }

    final QueryBuilder<DietPlan> query = QueryBuilder<DietPlan>(DietPlan())
      ..whereEqualTo(keyProtein, 30);
    final ParseResponse item = await query.query();
    print(item.toString());
  }

  Future<void> createItem() async {
    final ParseObject newObject = ParseObject('TestObjectForApi');
    newObject.set<String>('name', 'testItem');
    newObject.set<int>('age', 26);

    final ParseResponse apiResponse = await newObject.create();

    if (apiResponse.success && apiResponse.count > 0) {
      print(keyAppName + ': ' + apiResponse.result.toString());
    }
  }

  Future<void> getAllItemsByName() async {
    final ParseResponse apiResponse =
    await ParseObject('TestObjectForApi').getAll();

    if (apiResponse.success && apiResponse.count > 0) {
      for (final ParseObject testObject in apiResponse.results) {
        print(keyAppName + ': ' + testObject.toString());
      }
    }
  }

  Future<void> getAllItems() async {
    final ParseResponse apiResponse = await DietPlan().getAll();

    if (apiResponse.success && apiResponse.count > 0) {
      for (final DietPlan plan in apiResponse.results) {
        print(keyAppName + ': ' + plan.name);
      }
    } else {
      print(keyAppName + ': ' + apiResponse.error.message);
    }
  }

  Future<void> getSingleItem() async {
    final ParseResponse apiResponse = await DietPlan().getObject('B0xtU0Ekqi');

    if (apiResponse.success && apiResponse.count > 0) {
      final DietPlan dietPlan = apiResponse.result;

      // Shows example of storing values in their proper type and retrieving them
      dietPlan.set<int>('RandomInt', 8);
      final int randomInt = dietPlan.get<int>('RandomInt');

      if (randomInt is int) {
        print('Saving generic value worked!');
      }

      // Shows example of pinning an item
      await dietPlan.pin();

      // shows example of retrieving a pin
      final DietPlan newDietPlanFromPin =
      await DietPlan().fromPin('R5EonpUDWy');
      if (newDietPlanFromPin != null) {
        print('Retreiving from pin worked!');
      }
    } else {
      print(keyAppName + ': ' + apiResponse.error.message);
    }
  }

  Future<void> query() async {
    final QueryBuilder<ParseObject> queryBuilder =
    QueryBuilder<ParseObject>(ParseObject('TestObjectForApi'))
      ..whereLessThan(keyVarCreatedAt, DateTime.now());

    final ParseResponse apiResponse = await queryBuilder.query();

    if (apiResponse.success && apiResponse.count > 0) {
      final List<ParseObject> listFromApi = apiResponse.result;
      final ParseObject parseObject = listFromApi?.first;
      print('Result: ${parseObject.toString()}');
    } else {
      print('Result: ${apiResponse.error.message}');
    }
  }

  Future<void> initUser() async {
    // All return type ParseUser except all
    ParseUser user =
    ParseUser('RE9fU360lishjFKC5dLZS4Zwm', 'password', 'test@facebook.com');

    /// Sign-up
    /*ParseResponse response = await user.signUp();
    if (response.success) {
      user = response.result;
    }*/

    final ParseUser user1 = await ParseUser.currentUser();
    user1.authData;

    /// Login
    ParseResponse response = await user.login();
    if (response.success) {
      user = response.result;
    }

    /// Reset password
    response = await user.requestPasswordReset();
    if (response.success) {
      user = response.result;
    }

    /// Verify email
    response = await user.verificationEmailRequest();
    if (response.success) {
      user = response.result;
    }

    // Best practice for starting the app. This will check for a valid user from a previous session from a local storage
    user = await ParseUser.currentUser();

    /// Update current user from server - Best done to verify user is still a valid user
    response = await ParseUser.getCurrentUserFromServer(
        user?.get<String>(keyHeaderSessionToken));
    if (response?.success ?? false) {
      user = response.result;
    }

    /// log user out
    response = await user?.logout();
    if (response?.success ?? false) {
      user = response.result;
    }

    user = await ParseUser.currentUser();

    user =
        ParseUser('TestFlutter', 'TestPassword123', 'phill.wiggins@gmail.com');
    response = await user.login();
    if (response.success) {
      user = response.result;
    }

    response = await user.save();
    if (response.success) {
      user = response.result;
    }

    // Returns type ParseResponse as its a query, not a single result
    response = await ParseUser.all();
    if (response.success) {
      // We have a list of all users (LIMIT SET VIA SDK)
      print(response.results);
    }

    final QueryBuilder<ParseUser> queryBuilder =
    QueryBuilder<ParseUser>(ParseUser.forQuery())
      ..whereStartsWith(ParseUser.keyUsername, 'phillw');

    final ParseResponse apiResponse = await queryBuilder.query();
    if (apiResponse.success && apiResponse.count > 0) {
      final List<ParseUser> users = response.result;
      for (final ParseUser user in users) {
        print(keyAppName + ': ' + user.toString());
      }
    }
  }

  Future<void> function() async {
    final ParseCloudFunction function = ParseCloudFunction('hello');
    final ParseResponse result =
    await function.executeObjectFunction<ParseObject>();
    if (result.success) {
      if (result.result is ParseObject) {
        final ParseObject parseObject = result.result;
        print(parseObject.parseClassName);
      }
    }
  }

  Future<void> functionWithParameters() async {
    final ParseCloudFunction function = ParseCloudFunction('hello');
    final Map<String, String> params = <String, String>{'plan': 'paid'};
    function.execute(parameters: params);
  }

  Future<void> getConfigs() async {
    final ParseConfig config = ParseConfig();
    final ParseResponse addResponse =
    await config.addConfig('TestConfig', 'testing');

    if (addResponse.success) {
      print('Added a config');
    }

    final ParseResponse getResponse = await config.getConfigs();

    if (getResponse.success) {
      print('We have our configs.');
    }
  }

  Future<void> repositoryAddUser() async {
    final User user = User('test_username', 'password', 'test@gmail.com');

    final ApiResponse response = await userRepo.save(user);

    if (!response.success) {
      await userRepo.login(user);
    }

    final User currentUser =
    await ParseUser.currentUser(customUserObject: User.clone());
    print(currentUser);
  }

  Future<String> getJson() {
    return rootBundle.loadString('emprecord.json');
  }

  Future<void> repositoryAddItems() async {
    var dietPlan = json.decode(await getJson());

    final List<DietPlan> dietPlans = <DietPlan>[];
    dietPlans.add(dietPlan);
    await initRepository();
    final ApiResponse response = await dietPlanRepo.addAll(dietPlans);
    if (response.success) {
      print(response.result);
    }
  }

  Future<void> repositoryGetAllItems() async {
    final ApiResponse response = await dietPlanRepo.getAll();
    if (response.success) {
      print(response.result);
    }
  }

  Future<void> initRepository() async {
    dietPlanRepo ??= DietPlanRepository.init(await getDB());
    userRepo ??= UserRepository.init(await getDB());
  }

  /// Available options:
  /// SharedPreferences - Not secure but will work with older versions of SDK - CoreStoreSharedPrefsImpl
  /// Sembast - NoSQL DB - Has security - CoreStoreSembastImpl
  Future<CoreStore> initCoreStore() async {
    //return CoreStoreSembastImp.getInstance();
    return CoreStoreSharedPrefsImp.getInstance();
  }
}

final quizQuestionsProvider = FutureProvider.autoDispose<List<Question>>(
      (ref) =>
      ref.watch(quizRepositoryProvider).getQuestions(
        numQuestions: 20,
        categoryId: Random().nextInt(24) + 9,
        difficulty: Difficulty.any,
      ),
);

class QuizScreen extends HookWidget {
  @override
  Widget build(BuildContext context) {
    final quizQuestions = useProvider(quizQuestionsProvider);
    final pageController = usePageController();
    final TextEditingController _controller = TextEditingController();
    final bool questanswered = false;
    final double slider_value = 5;
    final box = Boxes.getQuestionnaire();
    return new WillPopScope(
      onWillPop: () async => false,
      child: Container(
        height: MediaQuery
            .of(context)
            .size
            .height,
        width: MediaQuery
            .of(context)
            .size
            .width,
        decoration: BoxDecoration(color: AppColor.themeColor),
        child: Scaffold(
          backgroundColor: Colors.transparent,
          body: quizQuestions.when(
            data: (questions) =>
                QuestScreen(
                    index: box.length - 1,
                    person: questions,
                    personData: box.getAt(box.length - 1),
                    update: false

                ),
            loading: () => const Center(child: CircularProgressIndicator()),
            error: (error, _) =>
                QuizError(
                  message: error is Failure
                      ? error.message
                      : 'Something went wrong!',
                ),
          ),
          bottomSheet: quizQuestions.maybeWhen(
            data: (questions) {
              final quizState = useProvider(quizControllerProvider.state);
              if ((!quizState.answered) || !pageController.hasClients)
                return const SizedBox.shrink();
              return CustomButton(
                title: pageController.page.toInt() + 1 < questions.length
                    ? 'Next '
                    : 'See Results',
                onTap: () async {
                  if (_controller.text.isEmpty) {
                    showDialog(
                      context: context,
                      builder: (context) {
                        return CustomDialogOne(
                          title: "Alert",
                          content: "Please answer the Question.",
                          negativeBtnText: "Done",
                        );
                      },
                    );
                  }

                  if (true) {
                    BuildContextX(context)
                        .read(quizControllerProvider)
                        .nextQuestion(questions, pageController.page.toInt());
                    if (pageController.page.toInt() + 1 < questions.length) {
                      pageController.nextPage(
                        duration: const Duration(milliseconds: 250),
                        curve: Curves.linear,
                      );
                    }
                  }

                  BuildContextX(context)

                      .read(quizControllerProvider)
                      .nextQuestion(questions, pageController.page.toInt());
                  if (pageController.page.toInt() + 1 < questions.length) {
                    pageController.nextPage(
                      duration: const Duration(milliseconds: 250),
                      curve: Curves.linear,
                    );
                  }
                },
              );
            },
            orElse: () => const SizedBox.shrink(),
          ),
        ),
      ),
    );
  }


}


Future addQuestionnaire(String questionnaireType,
    String questionnaireName,
    String questionnaireTitle,
    String questionnaireId,
    String questionnaireDescription,
    String questionnaireDate,
    String questionnaireVersion,
    List<Question> questions) async {
  /*
  final questionnaire = QuestionnaireModel()
    ..questionnaireType = questionnaireType
    ..questionnaireDate = DateTime.now().toString()
    ..questionnaireName = questionnaireName
    ..questionnaireTitle = questionnaireTitle
    ..questionnaireId = questionnaireId
    ..questionnaireDescription = questionnaireDescription
    ..questionnaireVersion = questionnaireVersion
    ..questions = questions;

  */

  //box.put('mykey', transaction);

  // final mybox = Boxes.getTransactions();
  // final myTransaction = mybox.get('key');
  // mybox.values;
  // mybox.keys;
}

class QuizError extends StatelessWidget {
  final String message;

  const QuizError({
    Key key,
    @required this.message,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            message,
            style: const TextStyle(
              color: Colors.white,
              fontSize: 20.0,
            ),
          ),
          const SizedBox(height: 20.0),
          CustomButton(
            title: 'Retry',
            onTap: () => context.refresh(quizRepositoryProvider),
          ),
        ],
      ),
    );
  }
}

final List<BoxShadow> boxShadow = const [
  BoxShadow(
    color: Colors.black26,
    offset: Offset(0, 2),
    blurRadius: 4.0,
  ),
];

class CustomButton extends StatelessWidget {
  final String title;
  final VoidCallback onTap;

  const CustomButton({
    Key key,
    @required this.title,
    @required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        margin: const EdgeInsets.all(20.0),
        height: 50.0,
        width: double.infinity,
        decoration: BoxDecoration(
          color: Colors.purple,
          boxShadow: boxShadow,
          borderRadius: BorderRadius.circular(25.0),
        ),
        alignment: Alignment.center,
        child: Text(
          title,
          style: const TextStyle(
            fontSize: 18.0,
            color: Colors.white,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
    );
  }
}

class QuizResults extends StatelessWidget {
  final QuizState state;
  final List<Question> questions;

  const QuizResults({
    Key key,
    @required this.state,
    @required this.questions,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Text(
          '${"All Done"} ',
          style: const TextStyle(
            color: Colors.white,
            fontSize: 60.0,
            fontWeight: FontWeight.w600,
          ),
          textAlign: TextAlign.center,
        ),
        const Text(
          '',
          style: TextStyle(
            color: Colors.white,
            fontSize: 48.0,
            fontWeight: FontWeight.bold,
          ),
          textAlign: TextAlign.center,
        ),
        const SizedBox(height: 40.0),
        CustomButton(
          title: 'Done',
          onTap: () {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(
                    builder: (context) => TabBarScreen()),
                ModalRoute.withName('/TabBarScreen'));
          },
        ),
      ],
    );
  }
}

bool text = false;


class CircularIcon extends StatelessWidget {
  final IconData icon;
  final Color color;

  const CircularIcon({
    Key key,
    @required this.icon,
    @required this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 24.0,
      width: 24.0,
      decoration: BoxDecoration(
        color: color,
        shape: BoxShape.circle,
        boxShadow: boxShadow,
      ),
      child: Icon(
        icon,
        color: Colors.white,
        size: 16.0,
      ),
    );
  }
}


bool showWidget = false;

class Expansionpanel extends StatefulWidget {
  final List<AnswerOption> answerOptions;
  final AnswerOption option;
  TextEditingController nameController;
  String Function(String value) fieldValidator;
  final Question person;
  final QuestionnaireModel personData;

  Expansionpanel(this.answerOptions, this.option, this.nameController,
      this.fieldValidator, this.person, this.personData);

  @override
  _MyWidgetState createState() => _MyWidgetState();
}

class _MyWidgetState extends State<Expansionpanel> {

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: [
        showWidget
            ? widget.option.question.questionType == "free_text" ? Container(
          width: MediaQuery
              .of(context)
              .size
              .width / 1.5,
          height: 100,
          color: Colors.white,
          child: TextFormField(
            keyboardType: TextInputType.multiline,
            maxLines: null,
            validator: widget.fieldValidator,
            controller: widget.nameController,
          ),
        )
            : widget.option.question.questionType == "slider" ? Container(
          color: Colors.white,
          width: MediaQuery
              .of(context)
              .size
              .width / 1.2,

          child: SfSlider(
            min: 1.0,
            max: 10.0,
            value: widget.option.question.answerOptions.first.value.toDouble(),
            showLabels: true,
            interval: 9,
            labelFormatterCallback:
                (dynamic actualValue, String formattedText) {
              return actualValue == 10 ?
              LocalizationService.currentLocale.languageCode == 'de' ? widget
                  .person
                  .answerOptions
                  .first
                  .question
                  .answerOptions
                  .first
                  .answerText
                  .DE2 : widget
                  .person
                  .answerOptions
                  .first
                  .question
                  .answerOptions
                  .first
                  .answerText
                  .EN2 : LocalizationService.currentLocale.languageCode == 'de' ?
              widget
                  .person
                  .answerOptions
                  .first
                  .question
                  .answerOptions
                  .first
                  .answerText
                  .DE : widget
                  .person
                  .answerOptions
                  .first
                  .question
                  .answerOptions
                  .first
                  .answerText
                  .EN;
            },
            onChanged: (dynamic newValue) {
              print(newValue.round());

              setState(() {
                widget.nameController.text =
                    newValue.round().toString();
                widget.option.question.answerOptions.first.value =
                    newValue.round();

                widget.person.answer = [newValue.round().toString()];
                widget.personData.save();
              });
            },
          ),
        ) : Container() : Container()
      ],
    );
    ;
  }
}

class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  TextEditingController _controller;

  @override
  void initState() {
    super.initState();
    _controller = TextEditingController();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: TextField(
          controller: _controller,
          onSubmitted: (String value) async {
            await showDialog<void>(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: const Text('Thanks!'),
                  content: Text(
                      'You typed "$value", which has length ${value.characters
                          .length}.'),
                  actions: <Widget>[
                    TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: const Text('OK'),
                    ),
                  ],
                );
              },
            );
          },
        ),
      ),
    );
  }
}

class RadioListBuilder extends StatefulWidget {
  final int num;
  final List<AnswerText> answer;
  final Question question;

  const RadioListBuilder({Key key, this.num, this.answer, this.question})
      : super(key: key);

  @override
  RadioListBuilderState createState() {
    return RadioListBuilderState();
  }
}

class RadioListBuilderState extends State<RadioListBuilder> {
  int value;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: (context, index) {
        BuildContextX(context)
            .read(quizControllerProvider)
            .submitAnswer(widget.question, widget.answer[index].DE);
        return Card(
          child: RadioListTile(
            tileColor: Colors.white,
            value: index,
            groupValue: value,
            //onChanged: (ind) => setState(() => value = ind),
            onChanged: (dynamic ind) {
              setState(() {
                value = ind;
              });
            },

            title: Text(widget.answer[index].DE),
          ),
        );
      },
      itemCount: widget.num,
    );
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

class AddScreen extends StatefulWidget {
  final List<Question> questions;

  AddScreen({this.questions});

  @override
  _AddScreenState createState() => _AddScreenState();
}

class _AddScreenState extends State<AddScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text('Add Info'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: AddPersonForm(questions: widget.questions),
      ),
    );
  }
}

class InfoScreen extends StatefulWidget {


  @override
  _InfoScreenState createState() => _InfoScreenState();
}

class _InfoScreenState extends State<InfoScreen> {
  Box contactBox;
  final GlobalKey<ScaffoldState> _scaffoldkey = new GlobalKey<ScaffoldState>();

  // Delete info from people box
  _deleteInfo(int index) {
    contactBox.deleteAt(index);

    print('Item deleted from box at index: $index');
  }

  @override
  void initState() {
    super.initState();
    // Get reference to an already opened box
    contactBox = Hive.box<QuestionnaireModel>('question_model');
  }

  bool isButtonClickable = true;

  DietPlanProviderContract dietPlanRepo;

  final todoController = TextEditingController();
  bool update = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldkey,
      appBar: new AppBar(
        centerTitle: true,
        // title: setHeaderTitle(AppTranslations.of(context).text(AppTitle.appTitle),Colors.white),
        title: setHeaderTitle("Journal", Colors.white),
        backgroundColor: AppColor.themeColor,
        elevation: 1.0,
        actions: setCommonCartNitificationView(context),
      ),

      body: Column(
        children: <Widget>[
          Container(
              padding: EdgeInsets.fromLTRB(17.0, 1.0, 7.0, 1.0),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: TextField(
                      autocorrect: true,
                      textCapitalization: TextCapitalization.sentences,
                      controller: todoController,

                    ),
                  ),

                ],
              )),
          Expanded(
              child: FutureBuilder<List<ParseObject>>(
                  future: getTodo(),
                  builder: (context, snapshot) {
                    switch (snapshot.connectionState) {
                      case ConnectionState.none:
                      case ConnectionState.waiting:
                        return Center(
                          child: Container(
                              width: 100,
                              height: 100,
                              child: CircularProgressIndicator()),
                        );
                      default:
                        if (snapshot.hasError) {
                          return Center(
                            child: Text("Error..."),
                          );
                        }
                        if (!snapshot.hasData) {
                          return Center(
                            child: Text("No Data..."),
                          );
                        } else {
                          return ListView.builder(
                              padding: EdgeInsets.only(top: 10.0),
                              itemCount: snapshot.data.length,
                              itemBuilder: (context, index) {
                                //*************************************
                                //Get Parse Object Values
                                final varTodo = snapshot.data[index];
                                final questionsmodel = QuestionnaireModel
                                    .fromMap(varTodo.toJson());
                                print(questionsmodel.questionnaireName);
                                final varTitle = varTodo.get<String>(
                                    'questionnaireDate');
                                final varDone = false;

                                final objectId = varTodo.get<String>(
                                    'objectId');
                                print(contactBox.get(
                                    "2022-03-25 20:20:41.521257"));
                                print(contactBox
                                    .getAt(0)
                                    .questionnaireDate);
                                print(contactBox
                                    .getAt(0)
                                    .objectId);


                                // final questions = results4.map((e) => Question.fromMap(e)).toList();

                                /*
                                final questionsmodel = new QuestionnaireModel(questionnaireType:questionnaireType,questionnaireName:questionnaireName,questionnaireTitle:questionnaireTitle,
                                questionnaireId:questionnaireId,questionnaireDescription:questionnaireDescription,questionnaireDate:varTitle,questionnaireVersion:questionnaireVersion,questions:questions,user:user,objectId:objectId,
                                answeredQuestions:answeredQuestions);

                                 */
                                //*************************************

                                return ListTile(
                                  title: Text(varTitle),
                                  leading: CircleAvatar(
                                    child: Icon(
                                        varDone ? Icons.check : Icons
                                            .book_outlined),
                                    backgroundColor:
                                    varDone ? Colors.green : Colors.blue,
                                    foregroundColor: Colors.white,
                                  ),
                                  trailing: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      IconButton(
                                          icon: Icon(
                                            Icons.edit,
                                            color: Colors.blue,
                                          ),
                                          onPressed: () async {
                                            Navigator.of(context).push(
                                                MaterialPageRoute(
                                                  builder: (context) =>
                                                      QuestScreen(
                                                          index: index,
                                                          person: contactBox
                                                              .get(varTitle)
                                                              .questions,
                                                          personData: contactBox
                                                              .get(varTitle),
                                                          update: true

                                                      ),
                                                ));
                                            setState(() {
                                              //Refresh UI
                                            });
                                          }),
                                      IconButton(
                                        icon: Icon(
                                          Icons.delete,
                                          color: Colors.blue,
                                        ),
                                        onPressed: () async {
                                          await deleteTodo(
                                              varTodo.objectId, varTitle,
                                              contactBox);
                                          setState(() {
                                            final snackBar = SnackBar(
                                              content: Text("Todo deleted!"),
                                              duration: Duration(seconds: 2),
                                            );
                                            ScaffoldMessenger.of(context)
                                              ..removeCurrentSnackBar()
                                              ..showSnackBar(snackBar);
                                          });
                                        },
                                      )
                                    ],
                                  ),
                                );
                              });
                        }
                    }
                  }))
        ],
      ),
    );
  }
}


class InfoScreen2 extends StatefulWidget {


  @override
  _InfoScreenState2 createState() => _InfoScreenState2();
}

class _InfoScreenState2 extends State<InfoScreen2> {
  Box contactBox;

  // Delete info from people box
  _deleteInfo(int index) {
    contactBox.deleteAt(index);

    print('Item deleted from box at index: $index');
  }

  @override
  void initState() {
    super.initState();
    // Get reference to an already opened box
    contactBox = Hive.box<QuestionnaireModel>('question_model');
  }
  bool isButtonClickable = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        centerTitle: true,
        // title: setHeaderTitle(AppTranslations.of(context).text(AppTitle.appTitle),Colors.white),
        title: setHeaderTitle("Journal",Colors.white),
        backgroundColor: AppColor.themeColor,
        elevation: 1.0,
        actions: setCommonCartNitificationView(context),
      ),

      /*
      floatingActionButton: FloatingActionButton(
        onPressed: () => Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => AddScreen(questions:widget.questions),
          ),
        ),
        child: Icon(Icons.add),
      ),

       */

      body: ValueListenableBuilder(
        valueListenable: contactBox.listenable(),
        builder: (context, Box box, widget) {



          if (box.isEmpty) {
            return Center(
              child: Text('Empty'),
            );
          } else {
            return ListView.builder(
              itemCount: box.length,
              itemBuilder: (context, index) {

                var currentBox = box;
                var personData = currentBox.getAt(index);

                bool _thirdMenu = true;
                int today = DateTime.now().day;

                int oldDay = DateTime.parse(personData.questionnaireDate).day;
                WidgetsBinding.instance.addPostFrameCallback((_){

                  if(today - oldDay >= 1) {
                    setState(() {
                      _thirdMenu = false;
                    });
                  } else {
                    setState(() {
                      _thirdMenu = true;
                    });
                  }
                });




                return SizedBox(
                    height: 200.0,
                    child: InkWell(
                        onTap: () => Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => QuestScreen(
                              index: index,
                              person: personData.questions,
                              personData: personData,
                            ),
                          ),
                        ),
                        child: ListTile(
                          title: Text(personData.questionnaireName.toString()),
                          subtitle: Text(personData.questionnaireDate),
                          trailing: IconButton(
                            onPressed: () => _deleteInfo(index),
                            icon: Icon(
                              Icons.delete,
                              color: Colors.red,
                            ),
                          ),
                          enabled:_thirdMenu ,
                        )));
              },
            );
          }
        },
      ),
    );
  }
}




Future<void> saveTodo(String title) async {
  await Future.delayed(Duration(seconds: 1), () {});
}

Future<List<ParseObject>> getTodo() async {
  QueryBuilder<ParseObject> queryTodo =
  QueryBuilder<ParseObject>(ParseObject('Data'));
  final ParseResponse apiResponse = await queryTodo.query();

  if (apiResponse.success && apiResponse.results != null) {
    return apiResponse.results as List<ParseObject>;
  } else {
    return [];
  }
}

Future<ParseResponse> updateTodo(String id, QuestionnaireModel quest) async {
  var dietPlan = ParseObject('Data')
    ..objectId = id
    ..set('questions', quest.questions);
  await dietPlan.save();
}

Future<void> deleteTodo(String id, String index, Box contactBox) async {
  var todo = ParseObject('Data')
    ..objectId = id;
  print(contactBox.length);
  var response = await todo.delete();
  if (response.success) {
    contactBox.delete(index);
    print(contactBox.length);
  }
}

Future <List<QuestionnaireModel>> removeTodoList(DietPlan item) async {
  List<QuestionnaireModel> todoList = [];

  ApiResponse response = await TodoUtils.removeItem(item);
  print("Code is ${response.statusCode}");
  print("Response is ${response.results}");

  if (response.statusCode == 200) {
    print("success");
  } else {
    print(response.statusCode);
    //Handle error
  }

  return todoList;
}


class QuestScreen extends StatefulWidget {
  final int index;
  final List<Question> person;
  final QuestionnaireModel personData;
  final bool update;

  const QuestScreen({
    this.index,
    this.person,
    this.personData, this.update,
  });

  @override
  _QuestScreenState createState() => _QuestScreenState();
}

class _QuestScreenState extends State<QuestScreen> {
  Box contactBox;
  final _controller = new PageController();

  // Delete info from people box
  _deleteInfo(int index) {
    contactBox.deleteAt(index);

    print('Item deleted from box at index: $index');
  }

  @override
  void initState() {
    super.initState();
    // Get reference to an already opened box
    contactBox = Hive.box<QuestionnaireModel>('question_model');
  }

  @override
  Widget build(BuildContext context) {
    print(widget.person.length);
    print(widget.personData);

    return Scaffold(
      appBar: new AppBar(
        centerTitle: true,
        // title: setHeaderTitle(AppTranslations.of(context).text(AppTitle.appTitle),Colors.white),
        title: setHeaderTitle(
            TKeys.QuestBlogs.translate(context), Colors.white),
        backgroundColor: AppColor.themeColor,
        elevation: 1.0,
        leading: new IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.white),
          onPressed: () {
            if (_controller.page > 0) {
              _controller.jumpToPage(_controller.page.round() - 1);
            }
          },
        ),
      ),
      body: Container(
        child: PageView.builder(
          controller: _controller,
          physics: NeverScrollableScrollPhysics(),
          itemCount: widget.person.length,
          itemBuilder: (context, index) {
            var personData = widget.person[index];
            return SizedBox(
                height: 200.0,
                child: UpdatePersonForm(
                    index: index,
                    person: personData,
                    personData: widget.personData,
                    controler: _controller,
                    questionsLength: widget.person.length,
                    update: widget.update
                ));
          },
        ),
      ),
    );
  }
}

class UpdateScreen extends StatefulWidget {
  final int index;
  final Question person;
  final QuestionnaireModel personData;

  const UpdateScreen({
    this.index,
    this.person,
    this.personData,
  });

  @override
  _UpdateScreenState createState() => _UpdateScreenState();
}

class _UpdateScreenState extends State<UpdateScreen> {

  final localizationController = Get.put(LocalizationController());


  @override
  Widget build(BuildContext context) {
    return GetBuilder<LocalizationController>(
        init: localizationController,
        builder: (LocalizationController controller) {
          return MaterialApp(

            home: Padding(
              padding: const EdgeInsets.all(16.0),
              child: UpdatePersonForm(
                index: widget.index,
                person: widget.person,
                personData: widget.personData,
              ),
            ),
            locale: controller.currentLanguage != ''
                ? Locale(controller.currentLanguage, '')
                : null,
            localeResolutionCallback: LocalizationService
                .localeResolutionCallBack,
            supportedLocales: LocalizationService.supportedLocales,
            localizationsDelegates: LocalizationService.localizationsDelegate,
          );
        }
    );
  }
}

class AddPersonForm extends StatefulWidget {
  final List<Question> questions;

  const AddPersonForm({this.questions});

  @override
  _AddPersonFormState createState() => _AddPersonFormState();
}

class _AddPersonFormState extends State<AddPersonForm> {
  final _nameController = TextEditingController();
  final _countryController = TextEditingController();
  final _personFormKey = GlobalKey<FormState>();

  Box box;
  Box question;

  String _fieldValidator(String value) {
    if (value == null || value.isEmpty) {
      return 'Field can\'t be empty';
    }
    return null;
  }

  // Add info to people box
  _addInfo() async {
    QuestionnaireModel newPerson = QuestionnaireModel(
      questions: widget.questions,
      questionnaireId: _countryController.text,
    );
    box.add(newPerson);
    print('Info added to box!');
  }

  @override
  void initState() {
    super.initState();
    // Get reference to an already opened box
    box = Hive.box<QuestionnaireModel>('question_model');
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _personFormKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('Name'),
          TextFormField(
            controller: _nameController,
            validator: _fieldValidator,
          ),
          SizedBox(height: 24.0),
          Text('Home Country'),
          TextFormField(
            controller: _countryController,
            validator: _fieldValidator,
          ),
          Spacer(),
          Padding(
            padding: const EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 24.0),
            child: Container(
              width: double.maxFinite,
              height: 50,
              child: ElevatedButton(
                onPressed: () {
                  if (_personFormKey.currentState.validate()) {
                    _addInfo();
                    Navigator.of(context).pop();
                  }
                },
                child: Text('Add'),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class UpdatePersonForm extends StatefulWidget {
  final int index;
  final Question person;
  final QuestionnaireModel personData;
  final PageController controler;
  final int questionsLength;
  final bool update;

  const UpdatePersonForm({
    this.index,
    this.person,
    this.personData,
    this.controler,
    this.questionsLength, this.update,
  });

  @override
  _UpdatePersonFormState createState() => _UpdatePersonFormState();
}

class _UpdatePersonFormState extends State<UpdatePersonForm> {
  final _personFormKey = GlobalKey<FormState>();


  TextEditingController _nameController;
  TextEditingController _countryController;
  Box box;
  final _sliderValue = 50;
  double _value = 5;
  int value;


  String _fieldValidator(String value) {
    if (value == null || value.isEmpty) {
      return 'Field can\'t be empty';
    }
    return null;
  }

  changeState(item) {
    print(item.isActive);

    setState(() {
      if (item.isActive == false) {
        item.isActive = true;
      } else {
        item.isActive = false;
      }
    });
  }

  customBoxDecoration(isActive) {
    return BoxDecoration(
      color: isActive ? Color(0xff1763DD) : Colors.white,
      border: Border(
          left: BorderSide(color: Colors.black12, width: 1.0),
          bottom: BorderSide(color: Colors.black12, width: 1.0),
          top: BorderSide(color: Colors.black12, width: 1.0),
          right: BorderSide(color: Colors.black12, width: 1.0)),
      borderRadius: const BorderRadius.all(
        Radius.circular(5.0),
      ),
    );
  }

  // Update info of people box

  /*
  _updateInfo() {
    var persos = widget.personData;

    Question newPerson = Question(
      questionTime: "",
      questionId: widget.person.questionId,
      answer: [_nameController.value.text],
      questionType: widget.person.questionType,
      questionText: widget.person.questionText,
      answerHint: widget.person.answerHint,
      answerOptions: widget.person.answerOptions,
    );

    persos.questions[widget.index] = newPerson;

    print("The answer is :" + newPerson.answer.toString());

    print("my index " + widget.index.toString());

// Dave - 22

    box.putAt(0, persos);

    print('Info updated in box!');
  }

   */

  @override
  void initState() {
    super.initState();
    // Get reference to an already opened box
    box = Boxes.getQuestionnaire();


    if (widget.personData.questions[widget.index].answer.isEmpty) {
      _nameController = TextEditingController(text: "");
    } else {
      _nameController = TextEditingController(
          text: widget.personData.questions[widget.index].answer[0]);
    }
  }

  _getData() {
    print("My Data" + widget.person.questionText.EN);
    // final results3 =   widget.person.answers.map<AnswerText>((e) => AnswerText()).toList();
    // print("My Data2" + results3.toString());
  }

  bool toggle = false;
  final localizationController = Get.find<LocalizationController>();

  @override
  Widget build(BuildContext context) {
    final filled = List.filled(widget.person.answerOptions.length, false);

    print("myanssswe" +
        widget.personData.questions[widget.index].answer.toString());
    widget.personData.save();
    //  if(widget.person.answer==null)


    List mylist = [];

    print(mylist);

    return Form(
      key: _personFormKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          setCommonText(
              TKeys.Question.translate(context) + ' ${widget.index + 1} ' +
                  TKeys.of.translate(context) + ' ${widget.questionsLength}',
              Colors.black54,
              18.0,
              FontWeight.w700,
              2),

          Padding(
            padding: const EdgeInsets.fromLTRB(20.0, 16.0, 20.0, 12.0),
            child: setCommonText(
                LocalizationService.currentLocale.languageCode == 'de' ? widget
                    .person.questionText.DE : widget.person.questionText.EN,
                Colors.black54,
                28.0, FontWeight.w700, 2),
          ),

          Divider(
            color: Colors.grey[200],
            height: 32.0,
            thickness: 2.0,
            indent: 20.0,
            endIndent: 20.0,
          ),
          //   TextFormField(controller: _nameController,
          //   validator: _fieldValidator),

          Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  widget.person.questionType == "open"
                      ? TextFormField(
                    controller: _nameController,
                    validator: _fieldValidator,
                    keyboardType: TextInputType.number,
                    onSaved: (String value) {
                      widget.personData.questions[widget.index].answer.clear();
                      widget.personData.questions[widget.index].answer.add(
                          _nameController.text);


                      widget.personData.save();
                      setState(() {});
                    },
                    inputFormatters: <TextInputFormatter>[
                      FilteringTextInputFormatter.digitsOnly
                    ],
                    maxLength: 3,
                    // Only numbers can be entered
                  )
                      : widget.person.questionType == "slider"
                      ? SfSlider(
                    min: 1.0,
                    max: 10.0,
                    value: widget.person.answerOptions.first.value.toDouble(),
                    showLabels: true,
                    interval: 9,
                    labelFormatterCallback:
                        (dynamic actualValue, String formattedText) {
                      return actualValue == 10
                          ? LocalizationService.currentLocale.languageCode ==
                          'de'
                          ? widget.person.answerOptions.first.answerText.DE2
                          : widget.person.answerOptions.first.answerText.EN2
                          : LocalizationService.currentLocale.languageCode ==
                          'de' ?
                      widget.person.answerOptions.first.answerText.DE : widget
                          .person.answerOptions.first.answerText.EN;
                    },
                    onChanged: (dynamic newValue) {
                      print(newValue.round());

                      setState(() {
                        _nameController.text =
                            newValue.round().toString();
                        widget.person.answerOptions.first.value =
                            newValue.round();
                      });
                    },

                  )
                      : widget.person.questionType == "free_text"
                      ? TextFormField(
                    keyboardType: TextInputType.multiline,
                    maxLines: null,
                    validator: _fieldValidator,
                    controller: _nameController,
                    onSaved: (String value) {
                      widget.personData.questions[widget.index].answer =
                      [_nameController.text];
                      widget.personData.save();
                    },
                  )
                      : widget.person.questionType == "text_selection"
                      ? Expanded(
                    child: ListView.builder(
                        shrinkWrap: true,
                        itemCount:
                        widget.person.answerOptions.length,
                        itemBuilder: (context, index) {
                          var myanswers = widget.person
                              .answerOptions[index].question.answerOptions;

                          return Column(children: <Widget>[

                            Divider(
                              color: Colors.grey[200],
                              height: 10.0,
                              thickness: 2.0,
                              indent: 20.0,
                              endIndent: 20.0,
                            ),
                            setCommonText(LocalizationService.currentLocale
                                .languageCode == 'de' ?

                            ' ${widget.person.answerOptions[index].answerText
                                .DE} ' : ' ${widget.person.answerOptions[index]
                                .answerText.EN} ',
                                Colors.black54,
                                18.0,
                                FontWeight.w700,
                                2),
                            Divider(
                              color: Colors.grey[200],
                              height: 32.0,
                              thickness: 2.0,
                              indent: 20.0,
                              endIndent: 20.0,
                            ),
                            Wrap(
                                spacing: 10.0,
                                runSpacing: 20.0,
                                children: widget.person
                                    .answerOptions[index].question.answerOptions
                                    .map((option) =>
                                new Container(
// margin: EdgeInsets.all(5),
                                    decoration:
                                    customBoxDecoration(
                                        option
                                            .isActive),
                                    child: InkWell(
                                        onTap: () {
                                          setState(() {
                                            widget.personData.questions[widget
                                                .index].answer = [
                                              LocalizationService.currentLocale
                                                  .languageCode == 'de' ? option
                                                  .answerText.DE : option
                                                  .answerText.EN
                                            ];


                                            widget.person.answerOptions
                                                .forEach((element) {
                                              element.isActive = false;
                                            });

                                            option.isActive =
                                            !option.isActive;

                                            print(option.isActive);
                                            print(widget.controler.page);
                                          });


                                          //         widget.personData.questions[widget.index].answer.clear();


                                          myanswers
                                              .forEach((element) {
                                            if (element.isActive == true) {
                                              if (!widget.personData
                                                  .questions[widget.index]
                                                  .answer.contains(
                                                  (LocalizationService
                                                      .currentLocale
                                                      .languageCode == 'de'
                                                      ? element.answerText.DE
                                                      : element.answerText
                                                      .EN))) {
                                                widget.personData
                                                    .questions[widget.index]
                                                    .answer.add(
                                                    LocalizationService
                                                        .currentLocale
                                                        .languageCode == 'de'
                                                        ? element.answerText.DE
                                                        : element.answerText
                                                        .EN);
                                                widget.personData.save();
                                              }

                                              //    print("myList2" + widget.person.answer.toString());
                                            }

                                            if (element.isActive == false) {
                                              widget.personData.questions[widget
                                                  .index].answer.removeWhere((
                                                  item) =>
                                              item == (LocalizationService
                                                  .currentLocale.languageCode ==
                                                  'de'
                                                  ? element.answerText.DE
                                                  : element.answerText.EN));
                                              widget.personData.save();


                                              //    print("myList2" + widget.person.answer.toString());
                                            }
                                          });
                                          //     print("myList2" + widget.person.answer.toString());

                                          ///  _nameController.text = mylist.toString();


                                        },
                                        child: Container(
                                            padding:
                                            EdgeInsets
                                                .all(
                                                10),
                                            child: Text(
                                                '${LocalizationService
                                                    .currentLocale
                                                    .languageCode == 'de'
                                                    ? option.answerText.DE
                                                    : option.answerText.EN}',
                                                textAlign:
                                                TextAlign
                                                    .center,
                                                style: TextStyle(
                                                    fontWeight: FontWeight
                                                        .normal,
                                                    color: option.isActive
                                                        ? Colors.white
                                                        : Colors.black87))))))
                                    .toList())
                          ]);
                        }),
                  )
                      : widget.person.questionType == "image"
                      ? Wrap(
                      spacing: 5.0,
                      runSpacing: 20.0,
                      children: widget.person.answerOptions
                          .map((option) =>
                      new Container(
// margin: EdgeInsets.all(5),
                        // decoration: customBoxDecoration(option.isActive),
                          child: IconButton(
                            icon: option.isActive
                                ? Icon(
                              Icons.check,
                            )
                                : Image.asset('assets/' +
                                option.answerText.DE +
                                '.png'),
                            color: Colors.red,
                            iconSize: 50,
                            onPressed: () async {

                              print("answerId" + option.answerId.toString());

                              final prefs = await SharedPreferences.getInstance();
                              await prefs.setInt(DateFormat('EEEE').format(DateTime.now()), option.answerId);


                              setState(() {
                                widget.personData.questions[widget.index]
                                    .answer = [
                                  LocalizationService.currentLocale
                                      .languageCode == 'de' ? option.answerText
                                      .DE : option.answerText.EN
                                ];
                                widget.personData.save();

                                option.isActive =
                                !option.isActive;
                                widget.person.answerOptions
                                    .forEach((element) {
                                  element.isActive = false;
                                });
                                option.isActive =
                                !option.isActive;
                              });
                            },
                          )))
                          .toList())
                      : widget.person.questionType == "radio"
                      ? Expanded(
                    child: ListView.builder(
                      itemBuilder: (context, index) {
                        return RadioListTile(
                          tileColor: Colors.white,
                          value: index,
                          groupValue: widget.person.radioValue,
                          //onChanged: (ind) => setState(() => value = ind),
                          onChanged: (dynamic ind) {
                            setState(() {
                              print(ind);
                              widget.personData.questions[widget.index]
                                  .radioValue = ind;

                              widget.personData.questions[widget.index].answer
                                  .clear();
                              widget.personData.questions[widget.index].answer =
                              [
                                LocalizationService.currentLocale
                                    .languageCode == 'de' ? widget.person
                                    .answerOptions[ind].answerText.DE
                                    : widget.person.answerOptions[ind]
                                    .answerText.EN
                              ];
                              widget.personData.save();

                              widget.person.setRadioValue(ind);
                            });
                          },

                          title: Text(
                              LocalizationService.currentLocale.languageCode ==
                                  'de' ? widget.person
                                  .answerOptions[index].answerText.DE : widget
                                  .person
                                  .answerOptions[index].answerText.EN),
                        );
                      },
                      itemCount: widget
                          .person.answerOptions.length,
                    ),
                  )

                  /*
              Flexible(
    child: AnswerCard5(
    answer: widget.person.answerOptions,
    isSelected: true,
    isCorrect: true,
    isDisplayingAnswer: true,
    )
    )
         */

                      : widget.person.questionType == "multiple_choice"
                      ? Expanded(
                      child: Wrap(
                          direction: Axis.vertical,

                          spacing: 10.0,
                          runSpacing: 20.0,
                          children: widget.person.answerOptions
                              .map((option) =>
                          new Container(
// margin: EdgeInsets.all(5),
                              decoration:
                              customBoxDecoration(
                                  option
                                      .isActive),
                              child: InkWell(
                                  onTap: () {
                                    setState(() {
                                      if (option.isActive == false) {
                                        option.isActive = true;
                                      } else {
                                        option.isActive = false;
                                      }
                                    });
                                    option.setActive(option
                                        .isActive);

                                    widget.personData.questions[widget.index]
                                        .answer.clear();
                                    widget.person.answerOptions
                                        .forEach((element) {
                                      if (element.isActive == true) {
                                        if (!widget.personData.questions[widget
                                            .index].answer.contains(
                                            (LocalizationService.currentLocale
                                                .languageCode == 'de' ? element
                                                .answerText.DE : element
                                                .answerText.EN))) {
                                          widget.personData.questions[widget
                                              .index].answer.add(
                                              LocalizationService.currentLocale
                                                  .languageCode == 'de'
                                                  ? element.answerText.DE
                                                  : element.answerText.EN);
                                          widget.personData.save();
                                        }

                                        //    print("myList2" + widget.person.answer.toString());
                                      }

                                      if (element.isActive == false) {
                                        widget.personData.questions[widget
                                            .index].answer.removeWhere((item) =>
                                        item ==
                                            (LocalizationService.currentLocale
                                                .languageCode == 'de' ? element
                                                .answerText.DE : element
                                                .answerText.EN));
                                        widget.personData.save();


                                        //    print("myList2" + widget.person.answer.toString());
                                      }
                                    });
                                  },
                                  child: Container(


                                      padding:
                                      EdgeInsets
                                          .all(
                                          10),
                                      child: Text(
                                          '${LocalizationService.currentLocale
                                              .languageCode == 'de' ? option
                                              .answerText.DE : option.answerText
                                              .EN}',
                                          textAlign:
                                          TextAlign
                                              .center,
                                          style: TextStyle(
                                              fontWeight: FontWeight
                                                  .normal,
                                              color: option.isActive
                                                  ? Colors.white
                                                  : Colors.black87))))))
                              .toList())) :

                  widget.person.questionType == "follow-up"
                      ? Expanded(
                    child: Wrap(
                        direction: Axis.vertical,

                        spacing: 10.0,
                        runSpacing: 20.0,
                        children: widget.person.answerOptions
                            .map((option) =>
                        new Container(
// margin: EdgeInsets.all(5),
                            decoration:
                            customBoxDecoration(
                                option
                                    .isActive),
                            child: InkWell(
                                onTap: () {
                                  setState(() {
                                    option.isActive =
                                    !option.isActive;
                                    widget.person.answerOptions
                                        .forEach((element) {
                                      element.isActive = false;
                                    });
                                    option.isActive =
                                    !option.isActive;
                                    widget.personData.questions[widget.index]
                                        .answer = [
                                      LocalizationService.currentLocale
                                          .languageCode == 'de' ? option
                                          .answerText.DE : option.answerText.EN
                                    ];
                                    widget.personData.save();

                                    //_updateInfo();

                                    widget.controler.jumpToPage(
                                        option.answerText.Page - 1);
                                  });
                                },
                                child: Container(
                                    padding:
                                    EdgeInsets
                                        .all(
                                        10),
                                    child: Text(
                                        '${LocalizationService.currentLocale
                                            .languageCode == 'de' ? option
                                            .answerText.DE : option.answerText
                                            .EN}',
                                        textAlign:
                                        TextAlign
                                            .center,
                                        style: TextStyle(
                                            fontWeight: FontWeight
                                                .normal,
                                            color: option.isActive
                                                ? Colors.white
                                                : Colors.black87))))))
                            .toList()),
                  )
                      : widget.person.questionType == "choice"
                      ? Expanded(
                    child: Wrap(
                        direction: Axis.vertical,

                        spacing: 10.0,
                        runSpacing: 10.0,
                        children: widget.person.answerOptions
                            .map((option) =>
                        new Container(
// margin: EdgeInsets.all(5),
                            decoration: customBoxDecoration(
                                option
                                    .isActive),
                            child:
                            Row(
                                mainAxisAlignment: MainAxisAlignment
                                    .spaceEvenly,
                                children: [


                                  option.question != null ? Expansionpanel(
                                      widget.person.answerOptions, option,
                                      _nameController, _fieldValidator,
                                      widget.person, widget.personData) :


                                  Divider(
                                    color: Colors.grey[200],
                                    height: 32.0,
                                    thickness: 2.0,
                                    indent: 20.0,
                                    endIndent: 20.0,
                                  ),


/*
      option.question != null?
      Container(
        decoration:
        customBoxDecoration(
            option
                .isActive),
        width: 200.0,
       height:200,
       child: TextFormField(
         keyboardType: TextInputType.multiline,
         maxLines: null,
         validator: _fieldValidator,
         controller: _nameController,
       ),
     ):
*/
                                  InkWell(
                                      onTap: () {
                                        setState(() {
                                          widget.personData.questions[widget
                                              .index].answer = [
                                            LocalizationService.currentLocale
                                                .languageCode == 'de' ? option
                                                .answerText.DE : option
                                                .answerText.EN
                                          ];

                                          option.isActive =
                                          !option.isActive;
                                          widget.person.answerOptions
                                              .forEach((element) {
                                            element.isActive = false;
                                          });

                                          if (option.question != null) {
                                            showWidget = !showWidget;
                                          }

                                          if (option.question == null) {
                                            showWidget = false;
                                          }

                                          option.isActive =
                                          !option.isActive;

                                          print(option.answerText.Page);
                                          print(widget.controler.page);
                                        });
                                      },
                                      child: Container(
                                          padding:
                                          EdgeInsets
                                              .all(
                                              10),
                                          child: Text(
                                              '${LocalizationService
                                                  .currentLocale.languageCode ==
                                                  'de'
                                                  ? option.answerText.DE
                                                  : option.answerText.EN}',
                                              textAlign:
                                              TextAlign
                                                  .center,
                                              style: TextStyle(
                                                  fontWeight: FontWeight
                                                      .normal,
                                                  color: option.isActive
                                                      ? Colors.white
                                                      : Colors.black87)))),
                                ])))
                            .toList()),
                  ) :

                  TextFormField(controller: _nameController,
                    validator: _fieldValidator,
                    // Only numbers can be entered
                  )
                ],
              )),

          Padding(
            padding: const EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 24.0),
            child: Container(
              width: double.maxFinite,
              height: 50,
              child: new InkWell(
                onTap: () async {
                  FocusScope.of(context).unfocus();

                  if (_nameController.text.isEmpty &&
                      widget.personData.questions[widget.index].answer
                          .isNotEmpty) {
                    _nameController.text =
                        widget.personData.questions[widget.index].answer
                            .toString();
                  }


                  if (_personFormKey.currentState.validate() &&
                      _nameController.text.isNotEmpty) {
                    print(widget.personData.questions[widget.index].answer);

                    widget.personData.questions[widget.index].answer.clear();


                    widget.personData.questions[widget.index].answer.add(
                        _nameController.text);
                    widget.personData.save();
                    final prefs = await SharedPreferences.getInstance();


                    await prefs.setString('question' + widget.index.toString(), widget.personData.questions[widget.index].answer[0]);

                    if(prefs.getString('question' + widget.index.toString()) == null){
               await prefs.setString('question' + widget.index.toString(), widget.personData.questions[widget.index].answer.toString());
               print("in my Pref" + widget.personData.questions[widget.index].answerOptions[0].answerId.toString());

             }
                    print("in my Pref" + prefs.getString('question' + widget.index.toString()));

                    //  _updateInfo();
                    if (widget.controler.page.toInt() + 1 <
                        widget.questionsLength) {
                      Hive.box(questionBox).put("question_shown", false);


                      widget.controler.nextPage(
                        duration: const Duration(milliseconds: 250),
                        curve: Curves.linear,
                      );
                      print(_nameController.text);
                    }


                    if (!(widget.controler.page.toInt() + 1 <
                        widget.questionsLength)) {
                      Hive.box(questionBox).put("question_shown", true);

                      widget.personData.setAnswered(true);
                      print(widget.personData);


/*
                      final ParseUser currentUser = await ParseUser.currentUser();





                      var parseObject = ParseObject("Data")
                        ..set("questionnaireType",widget.personData.questionnaireType)
                        ..set("questionnaireName", widget.personData.questionnaireName)
                        ..set("questionnaireTitle", widget.personData.questionnaireTitle)
                        ..set("questionnaireId", widget.personData.questionnaireId)
                        ..set("questionnaireDescription", widget.personData.questionnaireDescription)
                        ..set("questionnaireDate", widget.personData.questionnaireDate)
                        ..set("questionnaireVersion",widget.personData.questionnaireVersion)
                        ..set("questions", widget.personData.questions)
                        ..set("username", currentUser.username);

                      final ParseResponse parseResponse = await parseObject.save();

 */


                      if (widget.update != null && widget.update != false) {
                        updateTodo(
                            widget.personData.objectId, widget.personData);
                      }

                      if (widget.update == false) {
                        TodoUtils.addTodo(widget.personData)
                            .then((res) {
                          //   ScaffoldMessenger.of(context).hideCurrentSnackBar();

                          ParseResponse response = res;
                          if (response.statusCode == 201) {
                            //Successful
                            // _taskController.text = "";
                            widget.personData.objectId =
                                (response.results.first as ParseObject)
                                    .objectId;
                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                              content: Text("Todo added!"),
                              duration: Duration(seconds: 1),));

                            setState(() {
                              //Update UI
                            });
                          }
                        });
                      }
                      /*


                      var dietPlan = ParseObject('_User')
                        ..objectId = currentUser.objectId
                        ..set(
                            'questionnaire',
                            (ParseObject('Data')..objectId = parseObject.objectId)
                                .toPointer());
                      await dietPlan.save();

                      if (parseResponse.success) {
                        currentUser.objectId = (parseResponse.results.first as ParseObject).objectId;
                        print('Object created: $currentUser.objectId');
                      } else {
                        print('Object created with failed: ${parseResponse.error.toString()}');
                      }

     */



                      final String quest = prefs.getString('question1');

                      if (quest!= null )
                      {
                        print("quest: " + quest);
                      }
                      final String quest2 = prefs.getString('question2');
                      if (quest2!= null )
                      {
                        print("quest2: " + quest);
                      }                      final String quest3 = prefs.getString('question3');
                      final String quest4 = prefs.getString('question4');
                      if (quest4!= null )
                      {
                        print("quest4: " + quest);

                      }


                      final String quest5 = prefs.getString('question5');



                      if (quest5!= null )
                      {
                        print("quest5: " + quest);
                      }
                      final String quest6 = prefs.getString('question6');
                      if (quest6!= null )
                      {
                        print("quest6: " + quest);
                      }
                      final String quest7 = prefs.getString('question7');
                      if (quest7!= null )
                      {
                        print("quest7: " + quest);
                      }
                      final String quest8 = prefs.getString('question8');
                      if (quest8!= null )
                      {
                        print("quest8: " + quest);
                      }
                      final String quest9 = prefs.getString('question9');
                      if (quest9!= null )
                      {
                        print("quest9: " + quest);
                      }
                      final String quest10 = prefs.getString('question10');
                      if (quest10!= null )
                      {
                        print("quest10: " + quest);
                      }
                      final int counter = prefs.getInt('points');

                      prefs.setInt("points", 5 + counter);
                      prefs.setString("QuestTime", DateTime.now().toString());

                      print(counter);


                      Navigator.of(context)
                          .push(
                          MaterialPageRoute(builder: (context) => Results()));
                    }
                  }


                  if (_nameController.text.isEmpty) {
                    showDialog(
                      context: context,
                      builder: (context) {
                        return CustomDialogOne(
                          title: TKeys.Alert.translate(context),
                          content: TKeys.AnswQues.translate(context),
                          negativeBtnText: TKeys.Done.translate(context),
                        );
                      },
                    );
                  }
                },
                child: new Container(
                  height: 45,
                  width: MediaQuery
                      .of(context)
                      .size
                      .width,
                  child: new Material(
                    color: AppColor.themeColor,
                    borderRadius: BorderRadius.circular(22.5),
                    elevation: 5.0,
                    child: new Center(
                      child: new Text(
                        widget.index + 1 < widget.questionsLength
                            ? TKeys.Next.translate(context)
                            : TKeys.Submit.translate(context),
                        textDirection: SharedManager.shared.direction,
                        style: new TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}


int _testValue;

Future<void> loadData() async {
  SharedPreferences pref = await SharedPreferences.getInstance();

  pref.setString("startTime", DateTime.now().toString());

  SharedPreferences.getInstance().then((SharedPreferences sp) {
    pref = sp;
    _testValue = pref.getInt("dailycigarattes");
    // will be null if never previously saved
    if (_testValue == null) {
      _testValue = 0;
      pref.setDouble("pricePerCigaratte", 0);
      pref.setInt("dailycigarattes", 0);
      pref.setString("startTime", DateTime.now().toString());
      pref.setInt("points", 0);

      print(_testValue);
      print(_testValue);
    }
  });
}


Future addQuestionnaie(String objectId,
    QuestionnaireModel questionnaireModel) async {
  final box = Boxes.getQuestionnaire();
  Hive.box(questionBox).put("question_shown", false);

  box.put(objectId, questionnaireModel);
  print('Questionnaire Info added to box!');
}

