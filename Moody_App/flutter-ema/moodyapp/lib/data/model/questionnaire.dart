class Questionnaire {

  final String version;
  final String name;
  final String title;
  final int id;
  final String description;
  final int date;

  Questionnaire(this.version, this.name,this.title,this.id,this.description,this.date);

  Questionnaire.fromJson(Map<String, dynamic> json)
      : version = json['version'],
        name = json['name'],
        title = json['title'],   id = json['id'],
        description = json['description'],   date = json['date']
  ;

  Map<String, dynamic> toJson() => {
    'version': version,
    'name': name,
    'title': title,
    'id': id,
    'description': description,
    'date': date,
  };
}


/*
Map<String, dynamic> userMap = jsonDecode(jsonString);
var user = User.fromJson(userMap);

print('Howdy, ${Questionnaire.name}!');
print('We sent the verification link to ${user.email}.');

 */