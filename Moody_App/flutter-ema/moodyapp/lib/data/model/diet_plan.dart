import 'dart:core';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:meta/meta.dart';

import 'package:parse_server_sdk/parse_server_sdk.dart';

import '../../QUESTIONNAIRE/models/question_model.dart';

const String keyDietPlan = 'Data';
const String keyName = 'questionnaireDate';
const String keyDescription = 'questionnaireVersion';
const String keyProtein = 'questionnaireTitle';
const String keyCarbs = 'questionnaireDescription';
const String keyFat = 'questionnaireType';
const String keyStatus = 'questionnaireId';
const String keyquestions = 'questions';


class DietPlan extends ParseObject implements ParseCloneable {
  DietPlan() : super(keyDietPlan);
  DietPlan.clone() : this();

  @override
  DietPlan clone(Map<String, dynamic> map) => DietPlan.clone()..fromJson(map);

  String get name => get<String>(keyName);
  set name(String name) => set<String>(keyName, name);

  num get description => get<num>(keyDescription);
  set description(num description) => set<num>(keyDescription, 5);

  String get protein => get<String>(keyProtein);
  set protein(String protein) => super.set<String>(keyProtein, protein);

  String get carbs => get<String>(keyCarbs);
  set carbs(String carbs) => set<String>(keyCarbs, carbs);

  String get fat => get<String>(keyFat);
  set fat(String fat) => set<String>(keyFat, fat);





}
