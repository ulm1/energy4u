import 'package:hive/hive.dart';

part 'userhive.g.dart';

@HiveType(typeId: 1)
class Person extends HiveObject {
  @HiveField(0)
  String username;
  @HiveField(1)
  String password;
  @HiveField(2)
  String email;
  @HiveField(3)
  bool anonymous;
  @HiveField(4)
  String objectId;
  @HiveField(5)
  bool gamification;

  Person({
    this.username,
    this.password,
    this.email,
    this.anonymous,
    this.objectId,
    this.gamification

  });
}