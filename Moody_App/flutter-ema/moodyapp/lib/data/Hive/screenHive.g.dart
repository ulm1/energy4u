// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'screenHive.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class ScreenTimeAdapter extends TypeAdapter<ScreenTime> {
  @override
  final int typeId = 0;

  @override
  ScreenTime read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return ScreenTime()
      ..screenName = fields[0] as String
      ..startEvent = fields[1] as String
      ..endEvent = fields[2] as String
      ..startTime = fields[3] as String
      ..endTime = fields[4] as String;
  }

  @override
  void write(BinaryWriter writer, ScreenTime obj) {
    writer
      ..writeByte(5)
      ..writeByte(0)
      ..write(obj.screenName)
      ..writeByte(1)
      ..write(obj.startEvent)
      ..writeByte(2)
      ..write(obj.endEvent)
      ..writeByte(3)
      ..write(obj.startTime)
      ..writeByte(4)
      ..write(obj.endTime);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ScreenTimeAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
