import 'package:hive/hive.dart';
part 'screenHive.g.dart';


@HiveType(typeId: 0)
class ScreenTime extends HiveObject {

  @HiveField(0)
  String screenName;

  @HiveField(1)
  String startEvent;

  @HiveField(2)
  String endEvent;

  @HiveField(3)
  String startTime;

  @HiveField(4)
  String endTime;

}