import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'main.dart';

class LifeCycleManager extends StatefulWidget {
  final Widget child;
  final String page;

  LifeCycleManager({Key key, this.child,this.page}) : super(key: key);
  _LifeCycleManagerState createState() => _LifeCycleManagerState();
}
class _LifeCycleManagerState extends State<LifeCycleManager>
    with WidgetsBindingObserver,RouteAware {
  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
    });

    super.initState();
  }


  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }




  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    print("DidChangeDependencies");
  }

  @override
  Future<void> didChangeAppLifecycleState(AppLifecycleState state) async {
    print('state = $state');

    final preference = await SharedPreferences.getInstance();



    if(state== AppLifecycleState.resumed){
      print(DateTime.now());
      print(widget.key);
      preference.setString('StartDate:', DateTime.now().toString());
    }

    if(state== AppLifecycleState.paused){
      print(DateTime.now().toString()+ " paused");
      preference.setString('EndDate:', DateTime.now().toString());

    }
    if(state== AppLifecycleState.inactive){
      preference.setString('EndDate:', DateTime.now().toString());
    }
    if(state== AppLifecycleState.detached){
      preference.setString('EndDate:', DateTime.now().toString());
    }

  }
  @override
  Widget build(BuildContext context) {
    return Container(
      child: widget.child,
    );
  }
}

abstract class StoppableService {
  bool _serviceStoped = false;
  bool get serviceStopped => _serviceStoped;
  @mustCallSuper
  void stop() {
    _serviceStoped = true;
  }
  @mustCallSuper
  void start() {
    _serviceStoped = false;
  }
}

class LocationService extends StoppableService {
  @override
  void start() {
    super.start();
    // start subscription again
  }
  @override
  void stop() {
    super.stop();
    // cancel stream subscription
  }
}