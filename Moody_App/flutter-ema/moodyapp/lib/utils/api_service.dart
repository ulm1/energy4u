import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:parse_server_sdk_flutter/parse_server_sdk.dart';

import '../QUESTIONNAIRE/models/question_model.dart';
import '../data/base/api_response.dart';
import '../data/model/diet_plan.dart';
import '../domain/constants/application_constants.dart';

class TodoUtils {

  static final String _baseUrl = 'http://192.168.178.20:1337/parse/';


  static Future<ParseResponse> addTodo(QuestionnaireModel todo) async{

    final ParseUser currentUser = await ParseUser.currentUser();

    print("hello" + todo.questions.map((e) => e.answer).toString());
    todo.questions.map((e) => e.toJson());



    final ParseObject newObject = ParseObject('Data')
    ..set("questionnaireType",todo.questionnaireType)
    ..set("questionnaireName", todo.questionnaireName)
    ..set("questionnaireTitle", todo.questionnaireTitle)
    ..set("questionnaireId", todo.questionnaireId)
    ..set("questionnaireDescription", todo.questionnaireDescription)
    ..set("questionnaireDate", todo.questionnaireDate)
    ..set("questionnaireVersion",todo.questionnaireVersion)
    ..set("questions",todo.questions)
    ..set("username", currentUser.username);



    final ParseResponse apiResponse = await newObject.create();

      if (apiResponse.success && apiResponse.count > 0) {
        print(keyAppName + ': ' + apiResponse.result.toString());
      }
      else {
        print(keyAppName + ': ' + apiResponse.error.message);
      }

    return apiResponse;
  }


  Future<List<ParseObject>> getTodo() async {
    QueryBuilder<ParseObject> queryTodo =
    QueryBuilder<ParseObject>(ParseObject('Todo'));
    final ParseResponse apiResponse = await queryTodo.query();

    if (apiResponse.success && apiResponse.results != null) {
      return apiResponse.results as List<ParseObject>;
    } else {
      return [];
    }
  }


  @override
  static Future<ApiResponse> removeItem(DietPlan item) async {
    return getApiResponse<DietPlan>(await item.delete());
  }



  static Future updateTodo(QuestionnaireModel todo) async{




    String apiUrl = _baseUrl + "Data/${todo.objectId}";

    Response response = await put(Uri.parse(apiUrl),
        headers: {
          'X-Parse-Application-Id' : keyParseApplicationId,
          'X-Parse-REST-API-Key' : keyParseServerUrl,
          'Content-Type' : 'application/json'
    },
        body: json.encode(todo)
    );

    return response;
  }






}






