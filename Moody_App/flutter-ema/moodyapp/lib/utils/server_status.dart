import 'dart:async';

import 'package:connectivity/connectivity.dart';

enum ServerStatus { Online, Offline }

class NetworkStatusService {
  StreamController<ServerStatus> networkStatusController =
  StreamController<ServerStatus>();

  NetworkStatusService() {
    Connectivity().onConnectivityChanged.listen((status){
      networkStatusController.add(_getNetworkStatus(status));
    });
  }

  ServerStatus _getNetworkStatus(ConnectivityResult status) {
    return status == ConnectivityResult.mobile || status == ConnectivityResult.wifi ? NetworkStatus.Online : NetworkStatus.Offline;
  }
}
