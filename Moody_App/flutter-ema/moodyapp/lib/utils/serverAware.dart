import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:moodyapp/widgets/network_status_service.dart';
import 'package:provider/provider.dart';
import 'package:webview_flutter/src/webview.dart';

class ServerAwareWidget extends StatelessWidget {
  final Widget onlineChild;
  final Widget offlineChild;

  const ServerAwareWidget({Key key,  this.onlineChild,  this.offlineChild})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    NetworkStatus networkStatus = Provider.of<NetworkStatus>(context);
    if (networkStatus == NetworkStatus.Online) {
      _showToastMessage("Online");

      return onlineChild;
    } else {

      _showToastMessage("Offline");
      return offlineChild;
    }
  }

  void _showToastMessage(String message){
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1
    );
  }
}

bool online = false;

setStatue(bool stat){

  online = stat;
  return online;

}
getStatue(){
  return online;
}
