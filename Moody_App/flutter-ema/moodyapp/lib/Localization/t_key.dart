import 'localization_service.dart';

enum TKeys{
  hello,
  info,
  changeLanguage,
  termstitle,
  Licenses,
  Privacy,
  Internet,
 dashbHello,
  appTitle,
  dashbTitleNote,
  dashbQuest,
  QuestAvaliability,
  dashbAppointment,
  appointmentAvaliability,
  dashbAchievments,
  AchievmentsAvaliability,
  drawerHome,
  drawerCalender,
  drawerEntries,
  drawerJournal,
  drawerProfile,
  drawerBlogs,
  Settings,
  Notifications,
  MyMoodDiary,
  ChooseQuest,
  QuestionnaireInformation,
  Information,
  overallExamition,
  Questions,
  Length,
  NoQuest,
  QuestBlogs,
  Total,
  Today,
  Worst,
  Happiest,
  signIN,
  loginUserName,
  Pleaseuser,
  signupInfo,
  done,
  loginDontHaveAccount,
  SignUp,
  WrongUsername,
  reEnterUsername,
  allInfo,
  Email,
password,
  signUpNote,
  Question,
  of,
  Next,
  Submit,
  AnswQues,
  Alert,
  Done,
  EmailExists,
  NoConnection,
  EmailNotValid,
SetTime,
  On,
  Off,
Feedback,
  feedbacktype,
  loginProb,
  QuestProb,
  UserProb,
other,
  Suggestions,
  explain,
  Login







}

//TKeys.hello
extension TKeysExtention on TKeys {
  String get _string => toString().split('.')[1];

  String translate(context) {
    return LocalizationService.of(context).translate(_string) ?? '';
  }
}

