import 'package:hive/hive.dart';
import 'package:moodyapp/data/Hive/userhive.dart';

class Boxes {
  static Box<Person> getTransactions() =>
      Hive.box<Person>('userhive');
}