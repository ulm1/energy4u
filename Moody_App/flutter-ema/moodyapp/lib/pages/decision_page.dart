import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:moodyapp/Localization/localization_service.dart';
import 'package:moodyapp/Screens/Notifications/store/AppState.dart';
import 'package:moodyapp/Screens/TabBarScreens/Tabbar/Tabbar.dart';
import 'package:moodyapp/data/repositories/diet_plan/provider_api_diet_plan.dart';
import 'package:moodyapp/domain/constants/application_constants.dart';
import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:redux/src/store.dart';

import 'home_page.dart';
import 'login_page.dart';

class DecisionPage extends StatefulWidget {
  DecisionPage();

  @override
  _DecisionPageState createState() => _DecisionPageState();
}

class _DecisionPageState extends State<DecisionPage> {
  String _parseServerState = 'Checking Parse Server...';
  final localizationController = Get.put(LocalizationController());

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      _initParse();
    });
  }





  @override
  Widget build(BuildContext context) {
    return GetBuilder<LocalizationController>(
        init: localizationController,
        builder: (LocalizationController controller){
        return MaterialApp(
          debugShowCheckedModeBanner:false,

          home: TabBarScreen(),
          locale: controller.currentLanguage != ''
              ? Locale(controller.currentLanguage, '')
              : null,
          localeResolutionCallback: LocalizationService.localeResolutionCallBack,
          supportedLocales: LocalizationService.supportedLocales,
          localizationsDelegates: LocalizationService.localizationsDelegate,
        );

      }
    );
  }

  Widget _showLogo() {
    return Hero(
      tag: 'hero',
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0.0, 70.0, 0.0, 0.0),
        child: CircleAvatar(
          backgroundColor: Colors.transparent,
          radius: 48.0,
         // child: Image.asset('assets/parse.png'),
        ),
      ),
    );
  }


  Future<void> _initParse() async {
    try {
      await Parse().initialize(keyParseApplicationId, keyParseServerUrl,
          clientKey: keyParseClientKey, debug: true);
      final ParseResponse response = await Parse().healthCheck();
      if (response.success) {
        final ParseUser user = await ParseUser.currentUser();
        if (user != null) {
          _redirectToPage(context, TabBarScreen());
        } else {
          _redirectToPage(context, LoginScreen());
        }
      } else {
        setState(() {
          _parseServerState =
          'Parse Server Not avaiable\n due to ${response.error.toString()}';
        });
      }
    } catch (e) {
      setState(() {
        _parseServerState = e.toString();
      });
    }
  }

  Future<void> _initParse2() async {
    try {
      await Parse().initialize(keyParseApplicationId, keyParseServerUrl,
          clientKey: keyParseClientKey, debug: true);
      final ParseResponse response = await Parse().healthCheck();
      if (response.success) {
        final ParseUser user = await ParseUser.currentUser();

        if (user != null) {
         //_redirectToPage(context, HomePage(DietPlanProviderApi()));

          _redirectToPage(context, TabBarScreen());


        } else {
          _redirectToPage(context, LoginScreen());
        }
      } else {




        _redirectToPage(context, TabBarScreen());
      }
        /*
        setState(() {
          _parseServerState =
              'Parse Server Not avaiable\n due to ${response.error.toString()}';
        });
      }

         */
    } catch (e) {

      print(e.toString());

      setState(() {
        _parseServerState = e.toString();
      });
    }
  }

  Future<void> _redirectToPage(BuildContext context, Widget page) async {
    final MaterialPageRoute<bool> newRoute =
        MaterialPageRoute<bool>(builder: (BuildContext context) => page);

    final bool nav = await Navigator.of(context)
        .pushAndRemoveUntil<bool>(newRoute, ModalRoute.withName('/'));
    if (nav == true) {
      _initParse();
    }
  }
}
