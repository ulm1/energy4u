import 'dart:convert';

import 'package:moodyapp/Helper/Constant.dart';
import 'package:moodyapp/Screens/MyCustomDialogOne.dart';

import 'package:moodyapp/Screens/contanst/contanst.dart';
import 'package:moodyapp/models/question_model.dart';
import 'package:moodyapp/services/constants.dart';
import 'package:moodyapp/services/database.dart';
import 'package:moodyapp/widgets/questionnaire_play_widgets.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:parse_server_sdk_flutter/parse_server_sdk.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../globals.dart';


var _correct =0;
var _incorrect =0;
var _notAttempted = 0;



class Questionnaire extends StatefulWidget {
  final String quizId,objectId;
  SharedPreferences sharedPreferences;

  Questionnaire(this.quizId, this. objectId);

  @override
  _QuestionnaireState createState() => _QuestionnaireState();
}




/// Stream
Stream infoStream;

class _QuestionnaireState extends State<Questionnaire> {


  bool isLoading = true;

  @override
  void initState() {


    if (infoStream == null) {
      infoStream = Stream<List<int>>.periodic(Duration(milliseconds: 100), (x) {
        return [_correct, _incorrect];
      });
    }

    super.initState();
  }

  Future<QuestionModel> getQuestionModelFromDatasnapshot(
      questionSnapshot) async {
    QuestionModel questionModel = new QuestionModel();

    String rawJson = '{"name":"I am someone who is sometimes shy, introverted.","age":30}';
    Map<String, dynamic> map = jsonDecode(rawJson); // import 'dart:convert';

    String name = map['name'];
    int age = map['age'];

    QueryBuilder<ParseObject> queryUsers =
    QueryBuilder<ParseObject>(ParseObject('DataTypes'))
      ..whereEqualTo('objectId', widget.objectId);
    final ParseResponse parseResponse = await queryUsers.query();
    if (parseResponse.success && parseResponse.results != null) {
      final object = (!parseResponse.results.first) as ParseObject;
      print('stringField: ${object.get<String>('stringField')}');
      print('stringField: ${object.get<String>('stringField')}');
      print('doubleField: ${object.get<double>('doubleField')}');
      print('intField: ${object.get<int>('intField')}');
      print('boolField: ${object.get<bool>('boolField')}');
      print('dateField: ${object.get<DateTime>('dateField')}');
      print('jsonField: ${object.get<Map<String, dynamic>>('jsonField')}');
      print('listStringField: ${object.get<List>('listStringField')}');
      print('listNumberField: ${object.get<List>('listNumberField')}');
      print('listIntField: ${object.get<List>('listIntField')}');
      print('listBoolField: ${object.get<List>('listBoolField')}');
      print('listJsonField: ${object.get<List>('listJsonField')}');

      questionModel.question = name;
      questionModel.id =  object.get<String>('stringField');
      questionModel.quizId = object.get<String>('stringField');
      questionModel.quizWeekday = object.get<String>('stringField');


    }





    /// shuffling the options
    List<String> options = [
      questionSnapshot.get("option1"),
      questionSnapshot.get("option2"),
      questionSnapshot.get("option3"),
      questionSnapshot.get("option4"),
      questionSnapshot.get("option6"),
      questionSnapshot.get("option7"),
      questionSnapshot.get("option8"),
    ];

    questionModel.option1 = options[0];
    questionModel.option2 = options[1];
    questionModel.option3 = options[2];
    questionModel.option4 = options[3];
    questionModel.option6 = options[4];
    questionModel.option7 = options[5];
    questionModel.option8 = options[6];

    questionModel.correctOption = questionSnapshot.get("option1");
    ;
    questionModel.answered = false;

    return questionModel;
  }


  @override
  void dispose() {
    infoStream = null;
    super.dispose();
  }

  final _fbKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: isLoading
          ? Container(
              child: Center(child: CircularProgressIndicator()),
            )
          : Column(
              children: <Widget>[
                Container(
                  child: InfoHeader(
                    length: 5,
                  ),
                ),
                Expanded(
                  child: SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          height: 10,
                        ),
                           ListView.builder(
                                itemCount: 5,
                                shrinkWrap: true,
                                physics: ClampingScrollPhysics(),
                                itemBuilder: (context, index) {

                                }),
                      ],
                    ),
                  ),
                ),
              ],
            ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.check),
        backgroundColor: AppColor.themeColor,
        onPressed: () {
          if (_notAttempted > 0) {
            showDialog(
              context: context,
              builder: (context) {
                return CustomDialogOne(
                  title: "Alert",
                  content: "Please answer all the Questions.",
                  negativeBtnText: "Done",
                );
              },
            );
          }
        },
      ),
    );
  }
}

class InfoHeader extends StatefulWidget {
  final int length;

  InfoHeader({@required this.length});

  @override
  _InfoHeaderState createState() => _InfoHeaderState();
}

class _InfoHeaderState extends State<InfoHeader> {
  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.white,
      elevation: 0.0,
      child: StreamBuilder(
          stream: infoStream,
          builder: (context, snapshot) {
            return snapshot.hasData
                ? Container(
                    height: 40,
                    margin: EdgeInsets.only(left: 14),
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      shrinkWrap: true,
                      children: <Widget>[
                        NoOfQuestionTile(
                          text: "Total",
                          number: widget.length,
                        ),
                        NoOfQuestionTile(
                          text: "Not Attempted",
                          number: _notAttempted,
                        ),
                      ],
                    ),
                  )
                : Container();
          }),
    );
  }
}

Map myMap = new Map();
int counter2 = 1;

class QuizPlayTile extends StatefulWidget {
  final QuestionModel questionModel;
  final int index;

  static Map myMap;

  QuizPlayTile({@required this.questionModel, @required this.index});

  @override
  _QuizPlayTileState createState() => _QuizPlayTileState();
}

class _QuizPlayTileState extends State<QuizPlayTile> {
  String optionSelected = "";
  bool attempted = false;


  @override
  Widget build(BuildContext context) {
    RouterName.DateOfQuiz = DateTime.now();

    int counter = 1;

    // create our map

// populate it
    List<QuestionModel> people = new List<QuestionModel>();
    people.add(new QuestionModel(
        question: widget.questionModel.question,
        answer: optionSelected,
        quizId: widget.questionModel.id));
    ;


    return Container(
      child: Card(
        color: Colors.white,
        elevation: 0.0,
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.symmetric(horizontal: 5.0),
                child: Text(
                  "${widget.index + 1} ${")"} ${widget.questionModel.question}",
                  style: TextStyle(
                      fontSize: 18, color: Colors.black.withOpacity(0.8)),
                  //    setCommonText(AppTranslations.of(context).text(AppTitle.dashbTitleNote),Colors.black54,22.0, FontWeight.w700,2),
                ),
              ),
              SizedBox(
                height: 12,
              ),
              InkWell(
                child: GestureDetector(
                  onTap: () {
                    if (!widget.questionModel.answered) {
                      ///correct
                      if (widget.questionModel.option1 ==
                          widget.questionModel.correctOption) {
                        setState(() {
                          optionSelected = widget.questionModel.option1;
                          widget.questionModel.answered = false;
                        });
                      } else {
                        setState(() {
                          optionSelected = widget.questionModel.option1;
                          widget.questionModel.answered = false;
                        });
                      }
                    }
                    if (attempted == false) {
                      _notAttempted = _notAttempted - 1;
                      attempted = true;
                    }
                  },
                  child: OptionTile(
                    option: "1",
                    description: "${widget.questionModel.option1}",
                    correctAnswer: optionSelected,
                    optionSelected: optionSelected,
                  ),
                ),
                onTap: () {
                  setState(() {
                    optionSelected = widget.questionModel.option1;
                    widget.questionModel.answered = false;
                  });

                  if (attempted == false) {
                    _notAttempted = _notAttempted - 1;
                    attempted = true;
                  }
                },
              ),
              SizedBox(
                height: 4,
              ),
              InkWell(
                child: GestureDetector(
                  onTap: () {
                    if (!widget.questionModel.answered) {
                      ///correct
                      if (widget.questionModel.option2 ==
                          widget.questionModel.correctOption) {
                        setState(() {
                          optionSelected = widget.questionModel.option2;
                          widget.questionModel.answered = false;
                        });
                      } else {
                        setState(() {
                          optionSelected = widget.questionModel.option2;
                          widget.questionModel.answered = false;
                        });
                      }
                    }
                    if (attempted == false) {
                      _notAttempted = _notAttempted - 1;
                      attempted = true;
                    }
                  },
                  child: OptionTile(
                    option: "2",
                    description: "${widget.questionModel.option2}",
                    correctAnswer: optionSelected,
                    optionSelected: optionSelected,
                  ),
                ),
                onTap: () {
                  setState(() {
                    optionSelected = widget.questionModel.option2;
                    widget.questionModel.answered = false;
                  });

                  if (attempted == false) {
                    _notAttempted = _notAttempted - 1;
                    attempted = true;
                  }
                },
              ),
              SizedBox(
                height: 4,
              ),
              InkWell(
                child: GestureDetector(
                  onTap: () {
                    if (!widget.questionModel.answered) {
                      ///correct
                      if (widget.questionModel.option3 ==
                          widget.questionModel.correctOption) {
                        setState(() {
                          optionSelected = widget.questionModel.option3;
                          widget.questionModel.answered = false;
                        });
                      } else {
                        setState(() {
                          optionSelected = widget.questionModel.option3;
                          widget.questionModel.answered = false;
                        });
                      }
                    }
                    if (attempted == false) {
                      _notAttempted = _notAttempted - 1;
                      attempted = true;
                    }
                  },
                  child: OptionTile(
                    option: "3",
                    description: "${widget.questionModel.option3}",
                    correctAnswer: optionSelected,
                    optionSelected: optionSelected,
                  ),
                ),
                onTap: () {
                  setState(() {
                    optionSelected = widget.questionModel.option3;
                    widget.questionModel.answered = false;
                  });

                  if (attempted == false) {
                    _notAttempted = _notAttempted - 1;
                    attempted = true;
                  }
                },
              ),
              SizedBox(
                height: 4,
              ),
              InkWell(
                child: GestureDetector(
                  onTap: () {
                    if (!widget.questionModel.answered) {
                      ///correct
                      if (widget.questionModel.option4 ==
                          widget.questionModel.correctOption) {
                        setState(() {
                          optionSelected = widget.questionModel.option4;
                          widget.questionModel.answered = false;
                        });
                      } else {
                        setState(() {
                          optionSelected = widget.questionModel.option4;
                          widget.questionModel.answered = false;
                        });
                      }
                    }
                    if (attempted == false) {
                      _notAttempted = _notAttempted - 1;
                      attempted = true;
                    }
                  },
                  child: OptionTile(
                    option: "4",
                    description: "${widget.questionModel.option4}",
                    correctAnswer: optionSelected,
                    optionSelected: optionSelected,
                  ),
                ),
                onTap: () {
                  setState(() {
                    optionSelected = widget.questionModel.option4;
                    widget.questionModel.answered = false;
                  });

                  if (attempted == false) {
                    _notAttempted = _notAttempted - 1;
                    attempted = true;
                  }
                },
              ),
              SizedBox(
                height: 4,
              ),
              InkWell(
                child: GestureDetector(
                  onTap: () {
                    if (!widget.questionModel.answered) {
                      ///correct
                      if (widget.questionModel.option6 ==
                          widget.questionModel.correctOption) {
                        setState(() {
                          optionSelected = widget.questionModel.option6;
                          widget.questionModel.answered = false;
                        });
                      } else {
                        setState(() {
                          optionSelected = widget.questionModel.option6;
                          widget.questionModel.answered = false;
                        });
                      }
                    }
                    if (attempted == false) {
                      _notAttempted = _notAttempted - 1;
                      attempted = true;
                    }
                  },
                  child: OptionTile(
                    option: "5",
                    description: "${widget.questionModel.option6}",
                    correctAnswer: optionSelected,
                    optionSelected: optionSelected,
                  ),
                ),
                onTap: () {
                  setState(() {
                    optionSelected = widget.questionModel.option6;
                    widget.questionModel.answered = false;
                  });

                  if (attempted == false) {
                    _notAttempted = _notAttempted - 1;
                    attempted = true;
                  }
                },
              ),
              SizedBox(
                height: 4,
              ),
              InkWell(
                child: GestureDetector(
                  onTap: () {
                    if (!widget.questionModel.answered) {
                      ///correct
                      if (widget.questionModel.option7 ==
                          widget.questionModel.correctOption) {
                        setState(() {
                          optionSelected = widget.questionModel.option7;
                          widget.questionModel.answered = false;
                        });
                      } else {
                        setState(() {
                          optionSelected = widget.questionModel.option7;
                          widget.questionModel.answered = false;
                        });
                      }
                    }
                    if (attempted == false) {
                      _notAttempted = _notAttempted - 1;
                      attempted = true;
                    }
                  },
                  child: OptionTile(
                    option: "6",
                    description: "${widget.questionModel.option7}",
                    correctAnswer: optionSelected,
                    optionSelected: optionSelected,
                  ),
                ),
                onTap: () {
                  setState(() {
                    optionSelected = widget.questionModel.option7;
                    widget.questionModel.answered = false;
                  });

                  if (attempted == false) {
                    _notAttempted = _notAttempted - 1;
                    attempted = true;
                  }
                },
              ),
              SizedBox(
                height: 4,
              ),
              InkWell(
                child: GestureDetector(
                  onTap: () {
                    if (!widget.questionModel.answered) {
                      ///correct
                      if (widget.questionModel.option8 ==
                          widget.questionModel.correctOption) {
                        setState(() {
                          optionSelected = widget.questionModel.option8;
                          widget.questionModel.answered = false;
                        });
                      } else {
                        setState(() {
                          optionSelected = widget.questionModel.option8;
                          widget.questionModel.answered = false;
                        });
                      }
                    }
                    if (attempted == false) {
                      _notAttempted = _notAttempted - 1;
                      attempted = true;
                    }
                  },
                  child: OptionTile(
                    option: "7",
                    description: "${widget.questionModel.option8}",
                    correctAnswer: optionSelected,
                    optionSelected: optionSelected,
                  ),
                ),
                onTap: () {
                  setState(() {
                    optionSelected = widget.questionModel.option8;
                    widget.questionModel.answered = false;
                  });

                  if (attempted == false) {
                    _notAttempted = _notAttempted - 1;
                    attempted = true;
                  }
                },
              ),
              SizedBox(
                height: 4,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
