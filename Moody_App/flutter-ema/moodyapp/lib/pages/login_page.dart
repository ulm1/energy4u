import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:moodyapp/Constant/sharedpreference_page.dart';
import 'package:moodyapp/Screens/TabBarScreens/Tabbar/Tabbar.dart';
import 'package:moodyapp/data/Hive/userhive.dart';
import 'package:moodyapp/data/model/user.dart';
import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:random_string/random_string.dart';

import '../globals.dart';
import 'boxes.dart';
import 'decision_page.dart';


enum FormMode { LOGIN, SIGNUP }
const String SETTINGS_BOX = "settings";
const String SignUp_BOX = "signUp";



class LoginScreen extends StatefulWidget {



  @override
  _LoginScreen createState() => _LoginScreen();
}

class _LoginScreen extends State<LoginScreen> {
  Box box;


  @override
  void initState() {
    super.initState();
    // Get reference to an already opened box
    box = Hive.box(SignUp_BOX);

  }




  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: box.listenable(),
      builder: (context, box, child) =>
      box.get('registration_shown', defaultValue: false)
          ? LoginPage(true)
          : LoginPage(false),
    );
  }
}


class LoginPage extends StatefulWidget {
  final bool statues;

  LoginPage(this.statues);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final controllerUsername = TextEditingController();
  final controllerPassword = TextEditingController();
  final controllerEmail = TextEditingController();

  @override
  void initState() {
    super.initState();



    if(widget.statues == true){
      Login();
    }

    if(widget.statues == false){
      doUserRegistration();
    }

  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(

        body: Center(
          child: SingleChildScrollView(
            padding: const EdgeInsets.all(8),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [

              ],
            ),
          ),
        ));



}

  void showSuccess() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text("Success!"),
          content: const Text("User was successfully created!"),
          actions: <Widget>[
            new FlatButton(
              child: const Text("OK"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void showError(String errorMessage) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text("Error!"),
          content: Text(errorMessage),
          actions: <Widget>[
            new FlatButton(
              child: const Text("OK"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void showSuccesss(String message) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text("Success!"),
          content: Text(message),
          actions: <Widget>[
            new TextButton(
              child: const Text("OK"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }


  // creating User to Hive
  Future addPerson(String username,String password, String email, bool anonymous ) async {


    print("Hive user" + username.toString() );


  }

//register the user anonomously and save it to the parse server
  void doUserRegistration() async {
    final box = Boxes.getTransactions();

    print("my user at login_page  " + box.get(0).toString());

    final user = ParseUser.createUser(box.values.first.username, box.values.first.password, box.values.first.email);
    var response = await user.signUp();
    if (response.success) {

      /*
      var dietPlan = ParseObject('_User')
        ..objectId = user.objectId
        ..set('username', box.values.first.username)
        ..set('password', box.values.first.password)
        ..set('email', box.values.first.email)
        ..set('anonymous', true);

    await dietPlan.save();

       */



      Person person = box.get(0);
      person.anonymous= true;
      person.save();
      print("Added to Box" + box.getAt(0).toString());


      Hive.box(SignUp_BOX).put('registration_shown',true);
     // addPerson(username,password, email, true);
      doUserLogin(box.values.first.username,box.values.first.password,box.values.first.email);
      //showSuccess();
      } else {
      Hive.box(SignUp_BOX).put('registration_shown',false);
      doUserRegistration();

    }
  }


  void doUserLogin(String usernam,String passwor,String email) async {

    final username = usernam;
    final password = passwor;

    final user = ParseUser(username, password, email);

    var response = await user.login();

    if (response.success) {
      //showSuccesss("User was successfully login!");


      Navigator.of(context).push(MaterialPageRoute(builder: (context)=>TabBarScreen()));


      // DecisionPage();

      setState(() {
       // isLoggedIn = true;
      });
    } else {
      showError(response.error.message);
    }
  }



  void Login() async {


    final box = Boxes.getTransactions();


    final username = box.values.first.username;
    final password =  box.values.first.password;
    final email =  box.values.first.email;

    print("User exist loggin User");

    final user = ParseUser(username, password, email);

    var response = await user.login();

    if (response.success) {
      //showSuccesss("User was successfully login!");


      Navigator.of(context).push(MaterialPageRoute(builder: (context)=>TabBarScreen()));
print("login User");

      // DecisionPage();

      setState(() {
        // isLoggedIn = true;
      });
    } else {
      showError(response.error.message);
    }
  }


//


}

