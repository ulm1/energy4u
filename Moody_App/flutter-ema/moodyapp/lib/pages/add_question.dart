import 'package:moodyapp/Helper/CommonWidgets/CommonWidgets.dart';
import 'package:moodyapp/Helper/Constant.dart';
import 'package:moodyapp/Localization/app_translations.dart';
import 'package:moodyapp/Screens/contanst/contanst.dart';
import 'package:moodyapp/data/repositories/diet_plan/provider_api_diet_plan.dart';
import 'package:moodyapp/pages/questionnaire.dart';
import 'package:moodyapp/services/database.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:parse_server_sdk_flutter/parse_server_sdk.dart';
import 'package:progress_indicators/progress_indicators.dart';
import 'home_page.dart';


import '../globals.dart';

class AddQuestion extends StatefulWidget {
  final String quizId;

  AddQuestion(this.quizId);

  @override
  _AddQuestionState createState() => _AddQuestionState();
}




class _AddQuestionState extends State<AddQuestion> {
  final _formKey = GlobalKey<FormState>();
  bool _isLoading = true;
  String question = "",
      option1 = "",
      option2 = "",
      option3 = "",
      option4 = "",
      answer = "";
    String objectId = '';


  uploadQuestionData() async {

    void showMessage(String message) {
      ScaffoldMessenger.of(context)
        ..removeCurrentSnackBar()
        ..showSnackBar(SnackBar(content: Text(message)));
    }

    var parseObject = ParseObject("DataTypes")
      ..set("question", "How")
      ..set("option1", "String")
      ..set("option2", "String")
      ..set("option3", "String")
      ..set("option4", "String")
      ..set("option5", "String")
      ..set("option6", "String")
      ..set("option6", "String")
      ..set("option8", "String")
      ..set("answer", "")
      ..set("quizWeekday", "")
      ..set("id", "")

      ..set("doubleField", 1.5)
      ..set("intField", 2)
      ..set("boolField", true)
      ..set("dateField", DateTime.now())
      ..set("jsonField", {"field1": "value1", "field2": "value2"})
      ..set("listStringField", ["a", "b", "c", "d"])
      ..set("listIntField", [0, 1, 2, 3, 4])
      ..set("listBoolField", [false, true, false])
      ..set("listJsonField", [
        {"field1": "value1", "field2": "value2"},
        {"field1": "value1", "field2": "value2"}
      ]);

    final ParseResponse parseResponse = await parseObject.save();

    if (parseResponse.success) {
      objectId = (parseResponse.results.first as ParseObject).objectId;
      showMessage('Object created: $objectId');
    } else {
      showMessage('Object created with failed: ${parseResponse.error.toString()}');
    }
  }

  @override
  Future<void> initState()  {
    TimeOfDay roomBooked2 = TimeOfDay.fromDateTime(
        DateTime.now().add(Duration(hours: 20, minutes: 0))); // 4:30pm
    //databaseService.addUser(uid, RouterName.usern, RouterName.dob, RouterName.gender);#
    String objectId = '';

    uploadQuestionData();








    setState(() {
      _isLoading = false;
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        centerTitle: true,
        // title: setHeaderTitle(AppTranslations.of(context).text(AppTitle.appTitle),Colors.white),
        title: setHeaderTitle(
            AppTranslations.of(context).text(AppTitle.dashbFindDoctor),
            Colors.white),
        backgroundColor: AppColor.themeColor,
        elevation: 1.0,
      ),
      // drawer: DrawerOnly(),
      body: _isLoading
          ? ListView(
              children: <Widget>[
                Container(
                  child: Column(
                    children: <Widget>[
                      // Linear Loader with AppBar
                      LinearProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(appColor),
                      ),
                      // Jumping Dots Loader
                      SizedBox(height: 40.0),
                      SizedBox(height: 40.0),
                      SizedBox(height: 40.0),
                      SizedBox(height: 40.0),
                      SizedBox(height: 40.0),
                      SizedBox(height: 40.0),
                      SizedBox(height: 40.0),
                      SizedBox(height: 40.0),
                      SizedBox(height: 40.0),
                      SizedBox(height: 40.0),
                      SizedBox(height: 40.0),
                      SizedBox(height: 40.0),
                      SizedBox(height: 40.0),
                      SizedBox(height: 40.0),
                      SizedBox(height: 40.0),
                      SizedBox(height: 40.0),
                      SizedBox(height: 40.0),
                      SizedBox(height: 40.0),
                      SizedBox(height: 40.0),
                      SizedBox(height: 40.0),
                      // Jumping icon Loader
                      CollectionSlideTransition(
                        children: <Widget>[
                          Icon(Icons.android),
                          Icon(Icons.phone_iphone),
                          Icon(Icons.apps),
                        ],
                      ),
                      // Scalling Icon with jump Loader
                      SizedBox(height: 40.0),
                    ],
                  ),
                ),
              ],
            )
          :HomePage(DietPlanProviderApi()),
    );
  }
}
