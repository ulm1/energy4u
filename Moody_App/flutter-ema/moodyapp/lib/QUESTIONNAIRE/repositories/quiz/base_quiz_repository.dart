
import 'package:moodyapp/QUESTIONNAIRE/enums/difficulty.dart';
import 'package:moodyapp/QUESTIONNAIRE/models/question_model.dart';

abstract class BaseQuizRepository {
  Future<List<Question>> getQuestions({
    int numQuestions,
    int categoryId,
    Difficulty difficulty,
  });
}
