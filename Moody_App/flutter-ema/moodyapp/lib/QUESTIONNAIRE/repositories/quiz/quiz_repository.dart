import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:dio/dio.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hive/hive.dart';

import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:meta/meta.dart';
import 'package:moodyapp/QUESTIONNAIRE/enums/difficulty.dart';
import 'package:moodyapp/QUESTIONNAIRE/models/failure_model.dart';
import 'package:moodyapp/QUESTIONNAIRE/models/question_model.dart';
import 'package:moodyapp/Screens/contanst/contanst.dart';
import 'package:moodyapp/pages/boxes.dart';
//import 'package:moodyapp/boxes.dart';
import 'package:parse_server_sdk_flutter/parse_server_sdk.dart';
import 'package:objectid/objectid.dart';



import '../../../main.dart';
import 'base_quiz_repository.dart';

final dioProvider = Provider<Dio>((ref) => Dio());

final quizRepositoryProvider =
    Provider<QuizRepository>((ref) => QuizRepository(ref.read));


class QuizRepository extends BaseQuizRepository {
  final Reader _read;
Box box;





  QuizRepository(this._read);
  @override





  Future<List<Question>> getQuestions({
    @required int numQuestions,
    @required int categoryId,
    @required Difficulty difficulty,
  }) async {
    try {
      final queryParameters = {
        'type': 'multiple',
        'amount': numQuestions,
        'category': categoryId,
      };

/*
      uploadQuestionData() async {



        var parseObject = ParseObject("DataTypes")
          ..set("question", "How")
          ..set("option1", "String")
          ..set("option2", "String")
          ..set("option3", "String")
          ..set("option4", "String")
          ..set("option5", "String")
          ..set("option6", "String")
          ..set("option6", "String")
          ..set("option8", "String")
          ..set("answer", "")
          ..set("quizWeekday", "")
          ..set("id", "")

          ..set("doubleField", 1.5)
          ..set("intField", 2)
          ..set("boolField", true)
          ..set("dateField", DateTime.now())
          ..set("jsonField", {"field1": "value1", "field2": "value2"})
          ..set("listStringField", ["a", "b", "c", "d"])
          ..set("listIntField", [0, 1, 2, 3, 4])
          ..set("listBoolField", [false, true, false])
          ..set("listJsonField", [
            {"field1": "value1", "field2": "value2"},
            {"field1": "value1", "field2": "value2"}
          ]);

        final ParseResponse parseResponse = await parseObject.save();

        if (parseResponse.success) {
          objectId = (parseResponse.results.first as ParseObject).objectId;
          print('Object created: $objectId');
        } else {
          print('Object created with failed: ${parseResponse.error.toString()}');
        }
      }
*/

      if (difficulty != Difficulty.any) {
        queryParameters.addAll(
          {'difficulty': EnumToString.convertToString(difficulty)},
        );
      }




         String response2 = "";

        if (RouterName.id == "bfi2_6") {
         response2 = await rootBundle.loadString('assets/bfi2_6.json');

        }
        if (RouterName.id == "general_7") {
        response2 = await rootBundle.loadString('assets/general_7.json');

        }

        if (RouterName.id == "questionnaire_v01") {
          response2 = await rootBundle.loadString('assets/questionnaire_v01.json');
          final results2 = await json.decode(response2);
          final results3 = List<Map<String, dynamic>>.from(results2['questions'] ?? []);

          print(results2['questionnaireType']);








/*
          var parseObject = ParseObject("Data")
            ..set("questionnaireType",results2['questionnaireType'])
            ..set("questionnaireName", results2['questionnaireName'])
            ..set("questionnaireTitle", results2['questionnaireTitle'])
            ..set("questionnaireId", results2['questionnaireId'])
            ..set("questionnaireDescription", results2['questionnaireDescription'])
            ..set("questionnaireDate", results2['questionnaireDate'])
            ..set("questionnaireVersion",results2['questionnaireVersion'])

            ..set("questions", results3)
            ..set("option8", "String")
            ..set("answer", "")
            ..set("quizWeekday", "")
            ..set("id", "")

            ..set("doubleField", 1.5)
            ..set("intField", 2)
            ..set("boolField", true)
            ..set("dateField", DateTime.now())
            ..set("jsonField", {"field1": "value1", "field2": "value2"})
            ..set("listStringField", ["a", "b", "c", "d"])
            ..set("listIntField", [0, 1, 2, 3, 4])
            ..set("listBoolField", [false, true, false])
            ..set("listJsonField", [
              {"field1": "value1", "field2": "value2"},
              {"field1": "value1", "field2": "value2"}
            ]);

          final ParseResponse parseResponse = await parseObject.save();

          if (parseResponse.success) {
            objectId = (parseResponse.results.first as ParseObject).objectId;
            print('Object created: $objectId');
          } else {
            print('Object created with failed: ${parseResponse.error.toString()}');
          }
 */
        }





       // final String response2 = await rootBundle.loadString('assets/general_7.json');

        final results2 = await json.decode(response2);
        final results3 = List<Map<String, dynamic>>.from(results2['questions'] ?? []);
        print(results3);



        _addInfo() async {
          final ParseUser user = await ParseUser.currentUser();


          final box = Boxes.getTransactions();

  print(box.values.first.username);


            QuestionnaireModel newPerson = QuestionnaireModel(
            questionnaireName: results2['questionnaireName'],
            questionnaireType: results2['questionnaireType'],
            questionnaireTitle: results2['questionnaireTitle'],
            questionnaireId: results2['questionnaireId'],
            questionnaireDescription: results2['questionnaireDescription'],
            questionnaireVersion: results2['questionnaireVersion'],
            questionnaireDate: DateTime.now().toString(),
            questions: results3.map((e) => Question.fromMap(e)).toList(),

            );
          addQuestionnaie(newPerson.questionnaireDate,newPerson);
          print('Questionnaire Info');
          print(results3);
          print(results3.map((e) => Question.fromMap(e)).toList());

        }

        if (results2.isNotEmpty) {
          _addInfo();
          return results3.map((e) => Question.fromMap(e)).toList();
        }

      return [];
    } on DioError catch (err) {
      print(err.message);
      throw Failure(
        message: err.response?.statusMessage ?? 'Something went wrong!',
      );
    } on SocketException catch (err) {
      print(err);
      throw const Failure(message: 'Please check your connection.');
    }
  }
}
