import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:flutter_svg/avd.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:meta/meta.dart';
import 'package:hive/hive.dart';
import 'package:moodyapp/Localization/localization_service.dart';


part 'question_model.g.dart';



@HiveType(typeId: 7)

class QuestionnaireModel extends HiveObject{
  QuestionnaireModel({
    this.questionnaireType,
    this.questionnaireName,
    this.questionnaireTitle,
    this.questionnaireId,
    this.questionnaireDescription,
    this.questionnaireDate,
    this.questionnaireVersion,
    this.answeredQuestions,
    this.objectId,
    this.user,
    this.questions,
  });

  @HiveField(0)
  final String questionnaireType;
  @HiveField(1)
  final String questionnaireName;
  @HiveField(2)
  final String questionnaireTitle;
  @HiveField(3)
  final String questionnaireId;
  @HiveField(4)
  final String questionnaireDescription;
  @HiveField(5)
   String questionnaireDate;
  @HiveField(6)
  final int questionnaireVersion;
  @HiveField(7)
   bool answeredQuestions;
  @HiveField(8)
   String objectId;
  @HiveField(9)
  final List<Question> questions;
  @HiveField(10)
  final String user;

  factory QuestionnaireModel.fromJson(String str) => QuestionnaireModel.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory QuestionnaireModel.fromMap(Map<String, dynamic> json) => QuestionnaireModel(
    questionnaireType: json["questionnaireType"],
    questionnaireName: json["questionnaireName"],
    questionnaireTitle: json["questionnaireTitle"],
    questionnaireId: json["questionnaireId"],
    questionnaireDescription: json["questionnaireDescription"],
    questionnaireDate: json["questionnaireDate"],
    questionnaireVersion: json["questionnaireVersion"],
    answeredQuestions: json["answeredQuestions"],
    objectId: json["objectId"],
    user: json["user"],
    questions: List<Question>.from(json["questions"].map((x) => Question.fromMap(x))),
  );

  Map<String, dynamic> toMap() => {
    "questionnaireType": questionnaireType,
    "questionnaireName": questionnaireName,
    "questionnaireTitle": questionnaireTitle,
    "questionnaireId": questionnaireId,
    "questionnaireDescription": questionnaireDescription,
    "questionnaireDate": questionnaireDate,
    "questionnaireVersion": questionnaireVersion,
    "answeredQuestions": answeredQuestions,
    "objectId": objectId,
    "user": user,

    "questions": List<dynamic>.from(questions.map((x) => x.toMap())),
  };

  void setAnswered(bool currentYear) {

    print("radiovalue is set" + currentYear.toString() );
    answeredQuestions = currentYear;
  }}

@HiveType(typeId: 8)

class Question extends HiveObject{
  Question({
    this.questionId,
    this.questionTime,
    this.answer,
    this.questionType,
    this.questionText,
    this.answerHint,
    this.answerOptions,
    this.radioValue,
  });

  @HiveField(0)
  final int questionId;
  @HiveField(1)
   String questionTime;
  @HiveField(2)
   List<String> answer;
  @HiveField(3)
  final String questionType;
  @HiveField(4)
  final QuestionText questionText;
  @HiveField(5)
  final QuestionText answerHint;
  @HiveField(6)
  final List<AnswerOption> answerOptions;
  @HiveField(7)
   int radioValue;

  factory Question.fromJson(String str) => Question.fromMap(json.decode(str));

 // String toJson() => json.encode(toMap());

  factory Question.fromMap(Map<String, dynamic> json) => Question(
    questionId: json["questionId"],
    questionTime: json["questionTime"],
    answer: List<String>.from(json["answer"].map((x) => x)),
    questionType: json["questionType"],
    questionText: QuestionText.fromMap(json["questionText"]),
    answerHint: json["answerHint"] == null ? null : QuestionText.fromMap(json["answerHint"]),
    answerOptions: List<AnswerOption>.from(json["answerOptions"].map((x) => AnswerOption.fromMap(x))),
    radioValue: json["radioValue"] == null ? null : json["radioValue"],
  );

  Map<String, dynamic> toMap() => {
    "questionId": questionId,
    "questionTime": questionTime,
    "answer": List<dynamic>.from(answer.map((x) => x)),
    "questionType": questionType,
    "questionText": questionText.toMap(),
    "answerHint": answerHint == null ? null : answerHint.toMap(),
    "answerOptions": List<dynamic>.from(answerOptions.map((x) => x.toMap())),
    "radioValue": radioValue == null ? null : radioValue,
  };

  Map toJson() => {
    'questionId':questionId,
    'questionTime':questionTime,
    'questionType': questionType,
    "questionText": questionText.toMap(),
    "answerHint": answerHint == null ? null : answerHint.toMap(),
    'answerOptions': answerOptions,
    "answerOptions": List<dynamic>.from(answerOptions.map((x) => x.toMap())),
    "radioValue": radioValue == null ? null : radioValue,
    "answer": answer,


  };


  void setRadioValue(int currentYear) {

    print("radiovalue is set" + currentYear.toString() );
    radioValue = currentYear;
  }}

@HiveType(typeId: 9)
class QuestionText extends HiveObject {
  QuestionText({
    this.DE,
    this.EN,
  });
  @HiveField(0)
  final String DE;

  @HiveField(1)
  final String EN;

  factory QuestionText.fromJson(String str) => QuestionText.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory QuestionText.fromMap(Map<String, dynamic> json) => QuestionText(
    DE: json["DE"],
    EN: json["EN"],
  );

  Map<String, dynamic> toMap() => {
    "DE": DE,
    "EN": EN,
  };
}
@HiveType(typeId: 10)

class AnswerOption extends HiveObject{
  AnswerOption({
    this.answerId,
    this.value,
    this.answerText,
    this.question,
    this.isActive,
  });

  @HiveField(1)
  final int answerId;
  @HiveField(2)
   int value;
  @HiveField(3)
  final AnswerText answerText;
  @HiveField(4)
  final AnswerOptionQuestion question;
  @HiveField(5)
   bool isActive;

  factory AnswerOption.fromJson(String str) => AnswerOption.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory AnswerOption.fromMap(Map<String, dynamic> json) => AnswerOption(
    answerId: json["answerId"],
    value: json["value"] == null ? null : json["value"],
    answerText: AnswerText.fromMap(json["answerText"]),
    question: json["question"] == null ? null : AnswerOptionQuestion.fromMap(json["question"]),
    isActive: json["isActive"] == null ? null : json["isActive"],
  );

  Map<String, dynamic> toMap() => {
    "answerId": answerId,
    "value": value == null ? null : value,
    "answerText": answerText.toMap(),
    "question": question == null ? null : question.toMap(),
    "isActive": isActive == null ? null : isActive,
  };

   setActive(bool currentYear) {

    print("object" + currentYear.toString() );
    isActive = currentYear;
  }}
@HiveType(typeId: 11)


class AnswerText extends HiveObject{
  AnswerText({
    this.DE,
    this.EN,
    this.DE2,
    this.EN2,
    this.Page,
  });

  @HiveField(0)
  final String DE;
  @HiveField(1)
  final String EN;
  @HiveField(2)
  final String DE2;
  @HiveField(3)
  final String EN2;
  @HiveField(4)
   int Page;

  factory AnswerText.fromJson(String str) => AnswerText.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory AnswerText.fromMap(Map<String, dynamic> json) => AnswerText(
    DE: json["DE"],
    EN: json["EN"],
    DE2: json["DE2"] == null ? null : json["DE2"],
    EN2: json["EN2"] == null ? null : json["EN2"],
    Page: json["Page"] == null ? null : json["Page"],
  );

  Map<String, dynamic> toMap() => {
    "DE": DE,
    "EN": EN,
    "DE2": DE2 == null ? null : DE2,
    "EN2": EN2 == null ? null : EN2,
    "Page": Page == null ? null : Page,
  };
}

@HiveType(typeId: 12)

class AnswerOptionQuestion extends HiveObject{
  AnswerOptionQuestion({
    this.questionType,
    this.answerOptions,
  });

  @HiveField(1)
  final String questionType;
  @HiveField(2)
  final List<FluffyAnswerOption> answerOptions;

  factory AnswerOptionQuestion.fromJson(String str) => AnswerOptionQuestion.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory AnswerOptionQuestion.fromMap(Map<String, dynamic> json) => AnswerOptionQuestion(
    questionType: json["questionType"],
    answerOptions: List<FluffyAnswerOption>.from(json["answerOptions"].map((x) => FluffyAnswerOption.fromMap(x))),
  );

  Map<String, dynamic> toMap() => {
    "questionType": questionType,
    "answerOptions": List<dynamic>.from(answerOptions.map((x) => x.toMap())),
  };
}
@HiveType(typeId: 13)

class FluffyAnswerOption extends HiveObject{
  FluffyAnswerOption({
    this.answerId,
    this.isActive,
    this.answerText,
    this.value,
  });

  @HiveField(0)
  final int answerId;
  @HiveField(1)
   bool isActive;
  @HiveField(2)
  final AnswerText answerText;
  @HiveField(3)
   int value;

  factory FluffyAnswerOption.fromJson(String str) => FluffyAnswerOption.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory FluffyAnswerOption.fromMap(Map<String, dynamic> json) => FluffyAnswerOption(
    answerId: json["answerId"],
    isActive: json["isActive"] == null ? null : json["isActive"],
    answerText: AnswerText.fromMap(json["answerText"]),
    value: json["value"] == null ? null : json["value"],
  );

  Map<String, dynamic> toMap() => {
    "answerId": answerId,
    "isActive": isActive == null ? null : isActive,
    "answerText": answerText.toMap(),
    "value": value == null ? null : value,
  };

  void setActive(bool currentYear) {

    print("object" + currentYear.toString() );
    this.isActive = currentYear;
  }

  void setValue(int currentYear) {
    value = currentYear;
  }}
