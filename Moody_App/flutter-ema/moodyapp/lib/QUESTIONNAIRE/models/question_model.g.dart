// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'question_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class QuestionnaireModelAdapter extends TypeAdapter<QuestionnaireModel> {
  @override
  final int typeId = 7;

  @override
  QuestionnaireModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return QuestionnaireModel(
      questionnaireType: fields[0] as String,
      questionnaireName: fields[1] as String,
      questionnaireTitle: fields[2] as String,
      questionnaireId: fields[3] as String,
      questionnaireDescription: fields[4] as String,
      questionnaireDate: fields[5] as String,
      questionnaireVersion: fields[6] as int,
      answeredQuestions: fields[7] as bool,
      objectId: fields[8] as String,
      user: fields[10] as String,
      questions: (fields[9] as List)?.cast<Question>(),
    );
  }

  @override
  void write(BinaryWriter writer, QuestionnaireModel obj) {
    writer
      ..writeByte(11)
      ..writeByte(0)
      ..write(obj.questionnaireType)
      ..writeByte(1)
      ..write(obj.questionnaireName)
      ..writeByte(2)
      ..write(obj.questionnaireTitle)
      ..writeByte(3)
      ..write(obj.questionnaireId)
      ..writeByte(4)
      ..write(obj.questionnaireDescription)
      ..writeByte(5)
      ..write(obj.questionnaireDate)
      ..writeByte(6)
      ..write(obj.questionnaireVersion)
      ..writeByte(7)
      ..write(obj.answeredQuestions)
      ..writeByte(8)
      ..write(obj.objectId)
      ..writeByte(9)
      ..write(obj.questions)
      ..writeByte(10)
      ..write(obj.user);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is QuestionnaireModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class QuestionAdapter extends TypeAdapter<Question> {
  @override
  final int typeId = 8;

  @override
  Question read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Question(
      questionId: fields[0] as int,
      questionTime: fields[1] as String,
      answer: (fields[2] as List)?.cast<String>(),
      questionType: fields[3] as String,
      questionText: fields[4] as QuestionText,
      answerHint: fields[5] as QuestionText,
      answerOptions: (fields[6] as List)?.cast<AnswerOption>(),
      radioValue: fields[7] as int,
    );
  }

  @override
  void write(BinaryWriter writer, Question obj) {
    writer
      ..writeByte(8)
      ..writeByte(0)
      ..write(obj.questionId)
      ..writeByte(1)
      ..write(obj.questionTime)
      ..writeByte(2)
      ..write(obj.answer)
      ..writeByte(3)
      ..write(obj.questionType)
      ..writeByte(4)
      ..write(obj.questionText)
      ..writeByte(5)
      ..write(obj.answerHint)
      ..writeByte(6)
      ..write(obj.answerOptions)
      ..writeByte(7)
      ..write(obj.radioValue);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is QuestionAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class QuestionTextAdapter extends TypeAdapter<QuestionText> {
  @override
  final int typeId = 9;

  @override
  QuestionText read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return QuestionText(
      DE: fields[0] as String,
      EN: fields[1] as String,
    );
  }

  @override
  void write(BinaryWriter writer, QuestionText obj) {
    writer
      ..writeByte(2)
      ..writeByte(0)
      ..write(obj.DE)
      ..writeByte(1)
      ..write(obj.EN);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is QuestionTextAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class AnswerOptionAdapter extends TypeAdapter<AnswerOption> {
  @override
  final int typeId = 10;

  @override
  AnswerOption read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return AnswerOption(
      answerId: fields[1] as int,
      value: fields[2] as int,
      answerText: fields[3] as AnswerText,
      question: fields[4] as AnswerOptionQuestion,
      isActive: fields[5] as bool,
    );
  }

  @override
  void write(BinaryWriter writer, AnswerOption obj) {
    writer
      ..writeByte(5)
      ..writeByte(1)
      ..write(obj.answerId)
      ..writeByte(2)
      ..write(obj.value)
      ..writeByte(3)
      ..write(obj.answerText)
      ..writeByte(4)
      ..write(obj.question)
      ..writeByte(5)
      ..write(obj.isActive);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AnswerOptionAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class AnswerTextAdapter extends TypeAdapter<AnswerText> {
  @override
  final int typeId = 11;

  @override
  AnswerText read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return AnswerText(
      DE: fields[0] as String,
      EN: fields[1] as String,
      DE2: fields[2] as String,
      EN2: fields[3] as String,
      Page: fields[4] as int,
    );
  }

  @override
  void write(BinaryWriter writer, AnswerText obj) {
    writer
      ..writeByte(5)
      ..writeByte(0)
      ..write(obj.DE)
      ..writeByte(1)
      ..write(obj.EN)
      ..writeByte(2)
      ..write(obj.DE2)
      ..writeByte(3)
      ..write(obj.EN2)
      ..writeByte(4)
      ..write(obj.Page);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AnswerTextAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class AnswerOptionQuestionAdapter extends TypeAdapter<AnswerOptionQuestion> {
  @override
  final int typeId = 12;

  @override
  AnswerOptionQuestion read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return AnswerOptionQuestion(
      questionType: fields[1] as String,
      answerOptions: (fields[2] as List)?.cast<FluffyAnswerOption>(),
    );
  }

  @override
  void write(BinaryWriter writer, AnswerOptionQuestion obj) {
    writer
      ..writeByte(2)
      ..writeByte(1)
      ..write(obj.questionType)
      ..writeByte(2)
      ..write(obj.answerOptions);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AnswerOptionQuestionAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class FluffyAnswerOptionAdapter extends TypeAdapter<FluffyAnswerOption> {
  @override
  final int typeId = 13;

  @override
  FluffyAnswerOption read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return FluffyAnswerOption(
      answerId: fields[0] as int,
      isActive: fields[1] as bool,
      answerText: fields[2] as AnswerText,
      value: fields[3] as int,
    );
  }

  @override
  void write(BinaryWriter writer, FluffyAnswerOption obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.answerId)
      ..writeByte(1)
      ..write(obj.isActive)
      ..writeByte(2)
      ..write(obj.answerText)
      ..writeByte(3)
      ..write(obj.value);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is FluffyAnswerOptionAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
