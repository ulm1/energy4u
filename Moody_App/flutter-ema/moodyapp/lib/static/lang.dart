const Map langs = {
  "de": {
    "tipsandfacts": [
      "Denken Sie daran, dass die Zeit auf Ihrer Seite ist",
      "Das Beste kommt noch",
      "Gehen Sie spazieren",
      "Sie machen das sehr gut",
      "Trinken Sie viel Wasser",
      "Eine positive Einstellung bringt positive Dinge hervor",
      "Schau weiter nach oben"
    ],
    "progressDescription": {
      "1": "Fülle deinen ersten Fragebogen aus",
      "2": "Füllen Sie 2 Fragebögen aus",
      "3": "Füllen Sie 10 Fragebögen aus",
      "4": "15 Fragebögen ausfüllen",
      "5": "20 Fragebögen ausfüllen",
      "6": "30 Fragebögen ausfüllen",
      "7": "35 Fragebögen ausfüllen",
      "8": "40 Fragebögen ausfüllen",
      "9": "45 Fragebögen ausfüllen",
      "10": "50 Fragebögen ausfüllen",
      "11": "55 Fragebögen ausfüllen",
      "12": "60 Fragebögen ausfüllen",
      "13": "65 Fragebögen ausfüllen",
      "14": "80 Fragebögen ausfüllen",
      "15": "100 Fragebögen ausfüllen"
    },
    "guide": {
      "whysohard": {
        "image": "assets/images/stress.svg",
        "title": "Warum ist das Aufhören so schwer?",
        "content": [
          {
            "title": "Aufhören kann wirklich schwer sein",
            "text":
                "Wir alle kennen die gesundheitlichen Risiken des Rauchens, aber das macht es nicht einfacher, mit dem Rauchen aufzuhören. Egal, ob man als Teenager nur gelegentlich raucht oder ein Leben lang eine Packung am Tag raucht, das Aufhören kann wirklich schwierig sein."
          },
          {
            "title": "Warum?",
            "text": [
              "Das Rauchen von Tabak ist sowohl eine körperliche als auch eine psychische Sucht. Das Nikotin in Zigaretten sorgt für ein vorübergehendes - und süchtig machendes - Hochgefühl. Wird die regelmäßige Zufuhr von Nikotin unterbrochen, treten körperliche Entzugserscheinungen und Verlangen auf. ",
              "Rauchen ist auch als tägliches Ritual verankert. Vielleicht rauchen Sie automatisch eine Zigarette zu Ihrem Morgenkaffee, in einer Pause bei der Arbeit oder in der Schule oder auf dem Heimweg am Ende eines hektischen Tages. Oder vielleicht rauchen Ihre Freunde, Ihre Familie oder Ihre Kollegen, und es ist Teil der Art und Weise geworden, wie Sie mit ihnen umgehen"
            ]
          },
          {
            "Titel": "Erfolgreich mit dem Rauchen aufhören",
            "text":
                "Um erfolgreich mit dem Rauchen aufzuhören, müssen Sie sich sowohl mit der Sucht als auch mit den dazugehörigen Gewohnheiten und Routinen auseinandersetzen. Aber es ist machbar. Mit der richtigen Unterstützung und einem Plan zur Raucherentwöhnung kann jeder Raucher der Sucht den Rücken kehren - selbst wenn Sie es schon mehrfach versucht haben und gescheitert sind."
          }
        ]
      },
      "startplan": {
        "image": "assets/images/support-notes-colour.svg",
        "title": "Ihr Plan zur Raucherentwöhnung",
        "content": [
          {
            "title": "Fragen, die Sie sich stellen sollten",
            "text":
                "Nehmen Sie sich die Zeit, darüber nachzudenken, welche Art von Raucher Sie sind, welche Momente Ihres Lebens nach einer Zigarette verlangen und warum. Dies wird Ihnen helfen, herauszufinden, welche Tipps, Techniken oder Therapien für Sie am besten geeignet sind."
          },
          {
            "title":
                "Teilen Sie Familie, Freunden und Kollegen mit, dass Sie vorhaben, mit dem Rauchen aufzuhören.",
            "text":
                "Informieren Sie Ihre Freunde und Familie über Ihren Plan, mit dem Rauchen aufzuhören, und sagen Sie ihnen, dass Sie ihre Unterstützung und Ermutigung brauchen, um aufzuhören. Suchen Sie sich einen Freund, der ebenfalls mit dem Rauchen aufhören möchte. Sie können sich gegenseitig helfen, die schweren Zeiten zu überstehen."
          },
          {
            "title":
                "Antizipieren und planen Sie die Herausforderungen, denen Sie beim Aufhören begegnen werden.",
            "text":
                "Die meisten Menschen, die wieder mit dem Rauchen beginnen, tun dies innerhalb der ersten drei Monate. Sie können es schaffen, indem Sie sich auf die üblichen Herausforderungen wie Nikotinentzug und Zigarettenhunger vorbereiten."
          },
          {
            "Titel": "Erkennen Sie Ihre Auslöser für das Rauchen",
            "text":
                "Eines der besten Dinge, die Sie tun können, um mit dem Rauchen aufzuhören, ist, die Dinge zu identifizieren, die Sie zum Rauchen verleiten, einschließlich bestimmter Situationen, Aktivitäten, Gefühle und Menschen."
          },
          {
            "title":
                "Entfernen Sie Zigaretten und andere Tabakprodukte aus Ihrer Wohnung, Ihrem Auto und Ihrem Arbeitsplatz.",
            "text":
                "Werfen Sie alle Ihre Zigaretten, Feuerzeuge, Aschenbecher und Streichhölzer weg. Waschen Sie Ihre Kleidung und machen Sie alles frisch, was nach Rauch riecht. Shampoonieren Sie Ihr Auto, reinigen Sie Ihre Vorhänge und Ihren Teppich, und dämpfen Sie Ihre Möbel."
          },
          {
            "title": "Sprechen Sie mit Ihrem Arzt über Hilfe beim Aufhören.",
            "text":
                "Ihr Arzt kann Ihnen Medikamente verschreiben, die Ihnen bei den Entzugserscheinungen helfen. Wenn Sie keinen Arzt aufsuchen können, erhalten Sie viele Produkte rezeptfrei in Ihrer Apotheke, darunter Nikotinpflaster, Lutschtabletten und Kaugummi."
          }
        ]
      },
      "unpleasent": {
        "image": "assets/images/sad.svg",
        "title": "Rauchen Sie, um unangenehme Gefühle zu lindern?",
        "content": [
          {
            "title": "Unangenehme Gefühle",
            "text":
                "Viele von uns rauchen, um unangenehme Gefühle wie Stress, Depression, Einsamkeit und Angst zu bewältigen. Wenn man einen schlechten Tag hat, kann es einem so vorkommen, als seien Zigaretten der einzige Freund. So viel Trost Zigaretten auch spenden mögen, es ist wichtig, sich daran zu erinnern, dass es gesündere und effektivere Wege gibt, um unangenehme Gefühle in Schach zu halten. Dazu können Sport, Meditation, Entspannungsstrategien oder einfache Atemübungen gehören."
          },
          {
            "Titel": "Umgang mit stressigen Situationen",
            "text":
                "Für viele Menschen besteht ein wichtiger Aspekt der Raucherentwöhnung darin, alternative Wege zu finden, um mit diesen schwierigen Gefühlen umzugehen, ohne zu Zigaretten zu greifen. Auch wenn Zigaretten nicht mehr Teil Ihres Lebens sind, bleiben die schmerzhaften und unangenehmen Gefühle, die Sie in der Vergangenheit zum Rauchen veranlasst haben, bestehen. Es lohnt sich also, darüber nachzudenken, wie Sie mit stressigen Situationen und alltäglichen Ärgernissen umgehen wollen, die Sie normalerweise zum Rauchen veranlassen würden."
          }
        ]
      },
      "avoidingcommon": {
        "image": "assets/images/coach_monochromatic.svg",
        "title": "Tipps zur Vermeidung von häufigen Auslösern",
        "content": [
          {
            "title": "Alkohol",
            "text": [
              "Viele Menschen rauchen, wenn sie trinken. Versuchen Sie, auf alkoholfreie Getränke umzusteigen oder trinken Sie nur an Orten, an denen das Rauchen verboten ist. Versuchen Sie alternativ, Nüsse zu knabbern, auf einem Cocktailstick zu kauen oder an einem Strohhalm zu lutschen."
            ]
          },
          {
            "title": "Andere Raucher",
            "text": [
              "Wenn Freunde, Familienmitglieder und Arbeitskollegen in Ihrer Nähe rauchen, kann es doppelt schwierig sein, aufzuhören oder einen Rückfall zu vermeiden. Sprechen Sie über Ihren Entschluss, mit dem Rauchen aufzuhören, damit die Leute wissen, dass sie nicht rauchen können, wenn Sie mit ihnen im Auto sitzen oder gemeinsam eine Kaffeepause machen. Finden Sie an Ihrem Arbeitsplatz Nichtraucher, mit denen Sie Ihre Pausen verbringen können, oder unternehmen Sie etwas anderes, z. B. einen Spaziergang."
            ]
          },
          {
            "title": "Ende einer Mahlzeit",
            "text": [
              "Für manche Raucher bedeutet das Ende einer Mahlzeit, sich eine Zigarette anzuzünden, und die Aussicht, damit aufzuhören, kann entmutigend wirken. Sie können jedoch versuchen, diesen Moment nach einer Mahlzeit durch etwas anderes zu ersetzen, z. B. durch ein Stück Obst, ein gesundes Dessert, ein Stück Schokolade oder einen Kaugummi."
            ]
          }
        ]
      },
      "firsttwoweeks": {
        "image": "assets/images/checklist.svg",
        "title": "Die ersten 2 Wochen",
        "content": [
          {
            "title":
                "Die ersten zwei Wochen sind entscheidend für Ihren Erfolg",
            "text": [
              "Wenn Sie die ersten zwei Wochen überstehen, sind Ihre Erfolgschancen wesentlich höher. Deshalb ist es wichtig, dass Sie sich in diesen kritischen Wochen die bestmögliche Chance geben.",
              "In den ersten zwei Wochen geht es vor allem darum, sich abzulenken, sich zu beschäftigen und gut zu sich selbst zu sein. Beschäftigen Sie sich mit Aktivitäten, die Spaß machen und wenig Stress verursachen, und vermeiden Sie solche, die viel Stress verursachen."
            ]
          },
          {
            "title": "Beschäftigt bleiben",
            "text": [
              "Halten Sie Ihre Hände beschäftigt. Manche Menschen benutzen gerne einen Stift, einen Strohhalm oder einen Kaffeerührer.",
              "Trinken Sie viel Wasser.",
              "Wenn Sie Ihrem Verlangen einfach nur dasitzen, geben Sie ihm Raum, um zu wachsen.",
              "Entspannen Sie sich und atmen Sie tief durch."
            ]
          },
          {
            "title": "Vermeiden Sie Situationen mit hohem Risiko",
            "text": [
              "Hänge nicht mit Rauchern herum. Das ist so, als würde ein Crack-Süchtiger mit Crack-Süchtigen herumhängen. Ganz gleich, wie freundlich und hilfsbereit Ihre rauchenden Freunde sind, sie stellen zumindest in den ersten Monaten ein hohes Risiko dar.",
              "Üben Sie zu sagen:Nein danke, ich rauche nicht mehr",
              "Machen Sie sich klar, dass Sie auf Situationen mit hohem Risiko stoßen werden, an die Sie nicht gedacht haben. Wenn Sie einen Auslöser bemerken, planen Sie, schnell aufzustehen und zu gehen.",
              "Ein Tapetenwechsel kann den ganzen Unterschied ausmachen."
            ]
          },
          {
            "title": "Sprich mit dir selbst",
            "text": [
              "Die meisten Heißhungerattacken dauern nur 10 bis 20 Minuten. Lenken Sie sich ab, und das Verlangen wird vergehen. Wenn Sie an den Konsum denken, reden Sie mit sich selbst und beschäftigen Sie sich."
            ]
          }
        ]
      },
      "copingwithsymptomps": {
        "image": "assets/images/stressli.svg",
        "title": "Bewältigung von Symptomen",
        "content": [
          {
            "title": "Symptome",
            "text":
                "Wenn Sie mit dem Rauchen aufhören, werden Sie wahrscheinlich eine Reihe von körperlichen Symptomen verspüren, während sich Ihr Körper vom Nikotin zurückzieht. Der Nikotinentzug beginnt schnell, in der Regel innerhalb einer Stunde nach der letzten Zigarette und erreicht seinen Höhepunkt zwei bis drei Tage später. Die Entzugserscheinungen können einige Tage bis mehrere Wochen andauern und sind von Mensch zu Mensch unterschiedlich."
          },
          {"title": "Häufige Symptome", "text": "Verlangen nach Zigaretten"},
          {
            "title": "Vorübergehend",
            "text":
                "So unangenehm diese Entzugserscheinungen auch sein mögen, sollten Sie nicht vergessen, dass sie nur vorübergehend sind. Sie werden sich in ein paar Wochen bessern, wenn die Giftstoffe aus Ihrem Körper gespült werden. Sagen Sie in der Zwischenzeit Ihren Freunden und Ihrer Familie, dass Sie nicht wie gewohnt sein werden, und bitten Sie sie um Verständnis."
          },
          {
            "title": "Wie lange dauern die Nikotinentzugssymptome an?",
            "text":
                "72 Stunden nachdem Sie mit dem Rauchen aufgehört haben, ist das Nikotin aus Ihrem Körper verschwunden. Die Nikotinentzugssymptome erreichen in der Regel 2 bis 3 Tage nach dem Rauchstopp ihren Höhepunkt und sind innerhalb von 1 bis 3 Monaten verschwunden. Es dauert mindestens 3 Monate, bis sich die Gehirnchemie nach der Raucherentwöhnung wieder normalisiert hat. Die letzten beiden Symptome, die verschwinden, sind in der Regel Reizbarkeit und Energielosigkeit. Jedes wirksame Programm zur Raucherentwöhnung muss diese lange Anpassungszeit berücksichtigen. Deshalb empfehlen einige Ärzte, das Nikotin mit einer Nikotinersatztherapie langsam abzusetzen. Zusammenfassend lässt sich sagen, dass sich die meisten Menschen bereits nach einer Woche besser fühlen und die Symptome in der Regel innerhalb von 3 Monaten verschwunden sind."
          }
        ]
      },
      "managecravings": {
        "image": "assets/images/sleep.svg",
        "title": "Zigarettenhunger bewältigen",
        "content": [
          {
            "title": "Wird bald vorbei sein",
            "text":
                "Wenn Sie die Auslöser für das Rauchen vermeiden, können Sie Ihren Drang zu rauchen zwar verringern, aber das Verlangen nach Zigaretten können Sie wahrscheinlich nicht ganz vermeiden. Glücklicherweise hält das Verlangen nicht lange an - in der Regel etwa 5 oder 10 Minuten. Wenn Sie in Versuchung geraten, sich eine Zigarette anzuzünden, erinnern Sie sich daran, dass das Verlangen bald vorübergehen wird, und versuchen Sie, es abzuwarten. Es ist hilfreich, sich im Voraus Strategien zurechtzulegen, um mit dem Verlangen umzugehen."
          },
          {
            "Titel": "Lenke dich ab",
            "text":
                "Machen Sie den Abwasch, schalten Sie den Fernseher ein, duschen Sie oder rufen Sie einen Freund an. Die Aktivität spielt keine Rolle, solange sie Sie vom Rauchen ablenkt."
          },
          {
            "title": "Erinnere dich daran, warum du aufgehört hast",
            "text":
                "Konzentrieren Sie sich auf die Gründe für den Ausstieg, einschließlich der gesundheitlichen Vorteile (z. B. Senkung des Risikos für Herzkrankheiten und Lungenkrebs), des besseren Aussehens, des gesparten Geldes und des gesteigerten Selbstwertgefühls."
          },
          {
            "title": "Raus aus einer verlockenden Situation",
            "text":
                "Der Ort, an dem Sie sich befinden, oder das, was Sie gerade tun, kann das Verlangen auslösen. Wenn das der Fall ist, kann ein Tapetenwechsel den Unterschied ausmachen."
          },
          {
            "title": "Belohne dich selbst",
            "text":
                "Bestärken Sie Ihre Siege. Jedes Mal, wenn Sie ein Verlangen besiegen, geben Sie sich selbst eine Belohnung, um motiviert zu bleiben."
          },
          {
            "title": "Finde einen oralen Ersatz",
            "text":
                "Halten Sie andere Dinge bereit, die Sie in den Mund stecken können, wenn der Heißhunger kommt. Versuchen Sie es mit Minzbonbons, Karotten- oder Selleriestangen, Kaugummi oder Sonnenblumenkernen. Oder lutschen Sie an einem Trinkhalm."
          },
          {
            "title": "Beschäftige deinen Geist",
            "text":
                "Lesen Sie ein Buch oder eine Zeitschrift, hören Sie Musik, die Sie lieben, lösen Sie ein Kreuzworträtsel oder Sudoku oder spielen Sie ein Online-Spiel."
          },
          {
            "title": "Halte deine Hände beschäftigt",
            "text":
                "Quetschbälle, Bleistifte oder Büroklammern sind ein guter Ersatz, um das Bedürfnis nach taktiler Stimulation zu befriedigen."
          },
          {
            "title": "Putzen Sie Ihre Zähne",
            "text":
                "Das frisch geputzte, saubere Gefühl kann helfen, das Verlangen nach Zigaretten zu vertreiben."
          },
          {
            "title": "Wasser trinken",
            "text":
                "Trinken Sie langsam ein großes Glas Wasser. Das hilft nicht nur, das Verlangen zu überwinden, sondern auch, die Symptome des Nikotinentzugs zu minimieren."
          },
          {
            "title": "Etwas anderes anzünden",
            "text":
                "Anstatt eine Zigarette anzuzünden, zünde eine Kerze oder etwas Weihrauch an."
          },
          {
            "title": "Werde aktiv",
            "text":
                "Gehen Sie spazieren, machen Sie ein paar Hampelmänner oder Liegestütze, probieren Sie ein paar Yoga-Dehnungen aus oder laufen Sie um den Block."
          },
          {
            "title": "Versuche, dich zu entspannen",
            "text":
                "Tun Sie etwas, das Sie beruhigt, zum Beispiel ein warmes Bad nehmen, meditieren, ein Buch lesen oder tiefe Atemübungen machen."
          },
          {
            "title": "Gehen Sie irgendwohin, wo das Rauchen nicht erlaubt ist",
            "text":
                "Betreten Sie zum Beispiel ein öffentliches Gebäude, ein Geschäft, ein Einkaufszentrum, ein Café oder ein Kino."
          }
        ]
      },
      "preventweightgain": {
        "image": "assets/images/eating.svg",
        "title": "Gewichtszunahme vorbeugen",
        "content": [
          {
            "title": "Gewichtszunahme",
            "text":
                "Da Rauchen den Appetit unterdrückt, ist die Gewichtszunahme für viele von uns eine häufige Sorge, wenn wir beschließen, mit dem Rauchen aufzuhören. Sie könnten dies sogar als Grund dafür anführen, nicht aufzuhören. Es stimmt zwar, dass viele Raucher innerhalb von sechs Monaten nach dem Rauchstopp an Gewicht zunehmen, aber die Zunahme ist in der Regel gering - im Durchschnitt etwa fünf Pfund - und nimmt mit der Zeit wieder ab. Es ist auch wichtig, daran zu denken, dass ein paar zusätzliche Pfunde für ein paar Monate Ihr Herz nicht so sehr schädigen, wie es das Rauchen tut. Eine Gewichtszunahme ist jedoch NICHT unvermeidlich, wenn Sie mit dem Rauchen aufhören."
          },
          {
            "title": "Gewichtszunahme ist NICHT unvermeidlich",
            "text":
                "Rauchen dämpft Ihren Geruchs- und Geschmackssinn, so dass Ihnen das Essen nach dem Rauchstopp oft attraktiver erscheint. Sie können auch an Gewicht zunehmen, wenn Sie die orale Befriedigung des Rauchens durch den Verzehr von ungesundem Komfortessen ersetzen. Daher ist es wichtig, andere, gesunde Wege zu finden, um mit unangenehmen Gefühlen wie Stress, Angst oder Langeweile umzugehen, anstatt gedankenlos und emotional zu essen."
          },
          {
            "Titel": "Nähren Sie sich selbst",
            "text":
                "Anstatt zu Zigaretten oder Essen zu greifen, wenn Sie sich gestresst, ängstlich oder deprimiert fühlen, lernen Sie neue Wege, sich schnell zu beruhigen. Hören Sie zum Beispiel aufmunternde Musik, spielen Sie mit einem Haustier oder trinken Sie eine Tasse heißen Tee."
          },
          {
            "title": "Essen Sie gesunde, abwechslungsreiche Mahlzeiten",
            "text":
                "Essen Sie viel Obst, Gemüse und gesunde Fette. Vermeiden Sie zuckerhaltige Lebensmittel, Limonaden, Frittiertes und Fertiggerichte."
          },
          {
            "title": "Lerne, achtsam zu essen",
            "text":
                "Emotionales Essen geschieht in der Regel automatisch und ist praktisch geistlos. Es ist leicht, einen Becher Eiscreme zu verdrücken, während man vor dem Fernseher sitzt oder auf sein Handy starrt. Wenn man jedoch die Ablenkungen beim Essen ausschaltet, kann man sich besser darauf konzentrieren, wie viel man isst, und sich auf seinen Körper und seine wirklichen Gefühle einstellen. Sind Sie wirklich noch hungrig oder essen Sie aus einem anderen Grund?"
          },
          {
            "title": "Trinke viel Wasser",
            "text":
                "Das Trinken von mindestens sechs bis acht Gläsern (8 oz.) hilft Ihnen, sich satt zu fühlen und hält Sie davon ab, zu essen, wenn Sie nicht hungrig sind. Wasser hilft außerdem, Giftstoffe aus dem Körper zu spülen."
          },
          {
            "title": "Geh spazieren",
            "text":
                "Es wird Ihnen nicht nur helfen, Kalorien zu verbrennen und das Gewicht zu halten, sondern auch Stress und Frustration abzubauen, die mit dem Rauchstopp einhergehen."
          },
          {
            "title": "Naschen Sie ohne Schuldgefühle",
            "text":
                "Eine gute Wahl sind zuckerfreie Kaugummis, Karotten- und Selleriestangen oder in Scheiben geschnittene Paprika oder Jicama."
          }
        ]
      },
      "Medikamente": {
        "image": "assets/images/Drcorona.svg",
        "title":
            "Medikamente und Therapie zur Unterstützung der Raucherentwöhnung",
        "content": [
          {
            "title": "Medikation hilft",
            "text":
                "Es gibt viele verschiedene Methoden, die Menschen erfolgreich geholfen haben, sich das Rauchen abzugewöhnen. Es kann sein, dass Sie mit der ersten Methode, die Sie ausprobieren, Erfolg haben. Wahrscheinlicher ist jedoch, dass Sie mehrere Methoden oder eine Kombination von Behandlungen ausprobieren müssen, um diejenige zu finden, die für Sie am besten funktioniert."
          },
          {
            "title": "Medikation",
            "text":
                "Medikamente zur Raucherentwöhnung können Entzugserscheinungen lindern und das Verlangen nach dem Rauchen verringern. Sie sind am wirksamsten, wenn sie im Rahmen eines umfassenden, von Ihrem Arzt überwachten Raucherentwöhnungsprogramms eingesetzt werden. Sprechen Sie mit Ihrem Arzt über Ihre Möglichkeiten und darüber, ob ein Medikament zur Raucherentwöhnung das Richtige für Sie ist. Die von der US-amerikanischen Food and Drug Administration (FDA) zugelassenen Medikamente sind:"
          },
          {
            "title": "Nicht-Nikotin-Medikamente",
            "text":
                "Diese Medikamente helfen Ihnen, mit dem Rauchen aufzuhören, indem sie das Verlangen und die Entzugssymptome ohne die Verwendung von Nikotin reduzieren. Medikamente wie Bupropion (Zyban) und Vareniclin (Chantix, Champix) sind nur für den kurzfristigen Gebrauch bestimmt."
          },
          {
            "Titel": "Alternative Therapien",
            "text":
                "Es gibt verschiedene Möglichkeiten, mit dem Rauchen aufzuhören, die nicht mit einer Nikotinersatztherapie, Vaping oder verschreibungspflichtigen Medikamenten zu tun haben. Dazu gehören:"
          },
          {
            "title": "Hypnose",
            "text":
                "Dies ist eine beliebte Option, die bei vielen Rauchern, die mit dem Rauchen aufhören wollen, gute Ergebnisse erzielt hat. Vergessen Sie alles, was Sie vielleicht von Bühnenhypnotiseuren gesehen haben. Hypnose funktioniert, indem sie Sie in einen tief entspannten Zustand versetzt, in dem Sie offen für Suggestionen sind, die Ihren Willen, mit dem Rauchen aufzuhören, stärken und Ihre negativen Gefühle gegenüber Zigaretten verstärken."
          },
          {
            "title": "Akupunktur",
            "text":
                "Als eine der ältesten bekannten medizinischen Techniken soll die Akupunktur die Freisetzung von Endorphinen (natürliche Schmerzmittel) auslösen, die dem Körper Entspannung verschaffen. Als Hilfsmittel zur Raucherentwöhnung kann Akupunktur bei der Bewältigung von Raucherentzugssymptomen hilfreich sein."
          },
          {
            "Titel": "Verhaltenstherapie",
            "text":
                "Die Nikotinsucht hängt mit den gewohnheitsmäßigen Verhaltensweisen oder Ritualen zusammen, die mit dem Rauchen verbunden sind. Die Verhaltenstherapie konzentriert sich auf das Erlernen neuer Bewältigungsstrategien und das Aufbrechen dieser Gewohnheiten."
          },
          {
            "Titel": "Motivationstherapien",
            "text":
                "In Selbsthilfebüchern und auf Websites finden Sie eine Reihe von Möglichkeiten, sich selbst zu motivieren, das Rauchen aufzugeben. Ein bekanntes Beispiel ist die Berechnung der finanziellen Einsparungen. Manche Menschen finden die Motivation, mit dem Rauchen aufzuhören, indem sie einfach ausrechnen, wie viel Geld sie sparen werden. Das kann für einen Sommerurlaub reichen."
          }
        ]
      },
      "ifyouslip": {
        "image": "assets/images/countingstars.svg",
        "title": "Was zu tun ist, wenn Sie abrutschen oder rückfällig werden",
        "content": [
          {
            "title": "Völlig normal",
            "text":
                "Die meisten Menschen versuchen mehrmals, mit dem Rauchen aufzuhören, bevor sie sich die Gewohnheit endgültig abgewöhnen, also machen Sie sich nicht fertig, wenn Sie einen Fehler machen und eine Zigarette rauchen. Machen Sie stattdessen aus dem Rückfall eine Erholung, indem Sie aus Ihrem Fehler lernen. Analysieren Sie, was passiert ist, bevor Sie wieder mit dem Rauchen angefangen haben, identifizieren Sie die Auslöser oder Problembereiche, in die Sie hineingeraten sind, und erstellen Sie einen neuen Plan für die Raucherentwöhnung, der diese beseitigt."
          },
          {
            "title": "Es ist nicht das Ende",
            "text":
                "Es ist auch wichtig, den Unterschied zwischen einem Ausrutscher und einem Rückfall zu betonen. Wenn Sie wieder mit dem Rauchen anfangen, heißt das nicht, dass Sie nicht wieder einsteigen können. Sie können entweder aus dem Ausrutscher lernen und sich dadurch motivieren lassen, sich mehr anzustrengen, oder Sie können ihn als Ausrede benutzen, um wieder mit dem Rauchen anzufangen. Aber die Entscheidung liegt bei Ihnen. Ein Ausrutscher muss sich nicht zu einem Rückfall auswachsen"
          },
          {
            "title": "Du bist kein Versager, wenn du einen Fehler machst",
            "text":
                "Das bedeutet nicht, dass du nicht für immer aufhören kannst."
          },
          {
            "title":
                "Lass einen Ausrutscher nicht zu einer Schlammlawine werden",
            "text":
                "Werfen Sie den Rest der Packung weg. Es ist wichtig, so schnell wie möglich wieder auf den Nichtraucherpfad zurückzukehren."
          },
          {
            "title":
                "Schauen Sie auf Ihr Raucherentwöhnungsprotokoll zurück und fühlen Sie sich gut",
            "text":
                "Schauen Sie auf Ihr Raucherentwöhnungsprotokoll zurück und freuen Sie sich über die Zeit, die Sie ohne Rauchen verbracht haben."
          },
          {
            "title": "Finde den Auslöser",
            "text":
                "Was genau war es, das Sie dazu gebracht hat, wieder zu rauchen? Entscheiden Sie, wie Sie beim nächsten Mal mit diesem Problem umgehen werden."
          },
          {
            "title": "Lernen Sie aus Ihrer Erfahrung",
            "text": "Was war besonders hilfreich? Was hat nicht funktioniert?"
          },
          {
            "title":
                "Verwenden Sie ein Medikament, das Ihnen beim Aufhören hilft?",
            "text":
                "Rufen Sie Ihren Arzt an, wenn Sie wieder anfangen zu rauchen. Einige Medikamente können nicht verwendet werden, wenn Sie gleichzeitig rauchen."
          }
        ]
      }
    },
    "guideps": {
      "guideto": "Ihr Leitfaden zur Raucherentwöhnung",
      "guides": "Leitfäden",
      "search": "Suche"
    },
    "home": {
      "wallet": "Brieftasche",
      "progress": "Fortschritt",
      "guide": "Anleitung",
      "rewards": "Belohnungen",
      "minafter": "MINUTEN NACH",
      "daysafter": "NACH X TAGEN",
      "timePassed": "Zeit, die vergangen ist, seit Sie den letzten Fragebogen ausgefüllt haben",
      "moneyEarned": "Insgesamt verdientes Geld",
      "cigarratesnotsmoked": "Nicht gerauchte Zigaretten",
      "day": "Level",
      "achievement": "Nächstes Ziel",
      "da": "Tage",

      "minute": "Minute",
      "second": "Sekunde",
      "hour": "Stunde",
      "resetall": "Gib nicht auf!",
      "resetallq": "Auf dem Weg zum Erfolg zu stolpern ist kein Misserfolg. Aufgeben ist ein Misserfolg.",
      "reset": "Zurücksetzen",
      "cancel": "Cancel",
      "settings": "Settings",
      "reason": "Reasons"
    },
    "process": {
      "daysleft": "Fragebögen übrig",
      "minleft": "verbleibende Minuten",
      "completed": "Abgeschlossen"
    },
    "wallet": {
      "Saldo": "Saldo",
      "newtransaction": "Neue Transaktion hinzufügen",
      "title": "Titel",
      "description": "Beschreibung",
      "Betrag": "Geldbetrag",
      "hinzufügen": "Hinzufügen",
      "täglich": "Täglich",
      "wöchentlich": "Wöchentlich",
      "monatlich": "Monatlich",
      "Jährlich": "Jährlich"
    },
    "reason": {
      "addnew": "Einen neuen Grund hinzufügen",
      "somegoodreasons": "Einige gute Gründe",
      "reason": "Schreiben Sie hier"
    },
    "misc": {
      "areusuredelete":
          "Sind Sie sicher, dass Sie diesen Eintrag löschen möchten?",
      "confirm": "Bestätigen",
      "delete": "Löschen",
      "Abbrechen": "Abbrechen"
    },
    "welcome": {
      "welcometext": "Sie treffen eine gute Entscheidung",
      "tellreason":
          "Bevor wir anfangen, sagen Sie uns, warum Sie mit dem Rauchen aufhören wollen",
      "reasonhint": "Ich möchte mit dem Rauchen aufhören, weil...",
      "weneedtoknow": "Ein paar Dinge, die wir wissen müssen",
      "howmanyperday": "Wie viele Zigarretten rauchen Sie am Tag",
      "howmuchpercigcost": "Wie viel kostet Sie eine Zigarette?",
      "choosecurrency": "Wählen Sie Ihre Währung",
      "start": "Jetzt starten!",
      "addtolist": "Zur Liste hinzufügen"
    },
    "settings":  {
      "save": "speichern",
      "youstopped": "Die Zeit, in der Sie mit dem Rauchen aufgehört haben",
      "change": "Veränderung"
    }
  },
  "en": {
    "tipsandfacts": [
      "Remember that time is on your side",
      "The best is yet to be",
      "Take walks",
      "You are doing pretty well",
      "Drink lots of water",
      "A positive mindset brings positive things",
      "Keep looking up"
    ],
    "progressDescription": {
      "1": "Complete your first questionnaire",
      "2": "Complete 2 questionnaires",
      "3": "Complete 10 questionnaires",
      "4": "Complete 15 questionnaires",
      "5": "Complete 20 questionnaires",
      "6": "Complete 30 questionnaires",
      "7": "Complete 35 questionnaires",
      "8": "Complete 40 questionnaires",
      "9": "Complete 45 questionnaires",
      "10": "Complete 50 questionnaires",
      "11": "Complete 55 questionnaires",
      "12": "Complete 60 questionnaires",
      "13": "Complete 65 questionnaires",
      "14": "Complete 80 questionnaires",
      "15": "Complete 100 questionnaires"
    },
    "guide": {
      "whysohard": {
        "image": "assets/images/stress.svg",
        "title": "Why is quitting so hard?",
        "content": [
          {
            "title": "Quitting can be really tough",
            "text":
                "We all know the health risks of smoking, but that doesn’t make it any easier to kick the habit. Whether you’re an occasional teen smoker or a lifetime pack-a-day smoker, quitting can be really tough."
          },
          {
            "title": "Why?",
            "text": [
              "Smoking tobacco is both a physical addiction and a psychological habit. The nicotine from cigarettes provides a temporary—and addictive—high. Eliminating that regular fix of nicotine causes your body to experience physical withdrawal symptoms and cravings. Because of nicotine’s “feel good” effect on the brain, you may turn to cigarettes as a quick and reliable way to boost your outlook, relieve stress, and unwind. Smoking can also be a way of coping with depression, anxiety, or even boredom. Quitting means finding different, healthier ways to cope with those feelings.",
              "Smoking is also ingrained as a daily ritual. It may be an automatic response for you to smoke a cigarette with your morning coffee, while taking a break at work or school, or on your commute home at the end of a hectic day. Or maybe your friends, family, or colleagues smoke, and it’s become part of the way you relate with them."
            ]
          },
          {
            "title": "Successfully stop smoking",
            "text":
                "To successfully stop smoking, you’ll need to address both the addiction and the habits and routines that go along with it. But it can be done. With the right support and quit plan, any smoker can kick the addiction—even if you’ve tried and failed multiple times before."
          }
        ]
      },
      "startplan": {
        "image": "assets/images/support-notes-colour.svg",
        "title": "Your Stop Smoking Plan",
        "content": [
          {
            "title": "Questions to ask yourself",
            "text":
                "Take the time to think of what kind of smoker you are, which moments of your life call for a cigarette, and why. This will help you to identify which tips, techniques, or therapies may be most beneficial for you."
          },
          {
            "title":
                "Tell family, friends, and co-workers that you plan to quit.",
            "text":
                "Let your friends and family in on your plan to quit smoking and tell them you need their support and encouragement to stop. Look for a quit buddy who wants to stop smoking as well. You can help each other get through the rough times."
          },
          {
            "title":
                "Anticipate and plan for the challenges you’ll face while quitting.",
            "text":
                "Most people who begin smoking again do so within the first three months. You can help yourself make it through by preparing ahead for common challenges, such as nicotine withdrawal and cigarette cravings."
          },
          {
            "title": "Identify your smoking triggers",
            "text":
                "One of the best things you can do to help yourself quit is to identify the things that make you want to smoke, including specific situations, activities, feelings, and people."
          },
          {
            "title":
                "Remove cigarettes and other tobacco products from your home, car, and work.",
            "text":
                "Throw away all of your cigarettes, lighters, ashtrays, and matches. Wash your clothes and freshen up anything that smells like smoke. Shampoo your car, clean your drapes and carpet, and steam your furniture."
          },
          {
            "title": "Talk to your doctor about getting help to quit.",
            "text":
                "Your doctor can prescribe medication to help with withdrawal symptoms. If you can’t see a doctor, you can get many products over the counter at your local pharmacy, including nicotine patches, lozenges, and gum."
          }
        ]
      },
      "unpleasent": {
        "image": "assets/images/sad.svg",
        "title": "Do you smoke to relieve unpleasant feelings?",
        "content": [
          {
            "title": "Unpleasant Feelings",
            "text":
                "Many of us smoke to manage unpleasant feelings such as stress, depression, loneliness, and anxiety. When you have a bad day, it can seem like cigarettes are your only friend. As much comfort as cigarettes provide, though, it’s important to remember that there are healthier and more effective ways to keep unpleasant feelings in check. These may include exercising, meditating, relaxation strategies, or simple breathing exercises."
          },
          {
            "title": "Dealing with stressful situations",
            "text":
                "For many people, an important aspect of giving up smoking is to find alternate ways to handle these difficult feelings without turning to cigarettes. Even when cigarettes are no longer a part of your life, the painful and unpleasant feelings that may have prompted you to smoke in the past will still remain. So it’s worth spending some time thinking about the different ways you intend to deal with stressful situations and the daily irritations that would normally have you lighting up."
          }
        ]
      },
      "avoidingcommon": {
        "image": "assets/images/coach_monochromatic.svg",
        "title": "Tips for avoiding common triggers",
        "content": [
          {
            "title": "Alcohol",
            "text": [
              "Many people smoke when they drink. Try switching to non-alcoholic drinks or drink only in places where smoking inside is prohibited. Alternatively, try snacking on nuts, chewing on a cocktail stick or sucking on a straw."
            ]
          },
          {
            "title": "Other smokers",
            "text": [
              "When friends, family, and co-workers smoke around you, it can be doubly difficult to give up or avoid relapse. Talk about your decision to quit so people know they won’t be able to smoke when you’re in the car with them or taking a coffee break together. In your workplace, find non-smokers to have your breaks with or find other things to do, such as taking a walk."
            ]
          },
          {
            "title": "End of a meal",
            "text": [
              "For some smokers, ending a meal means lighting up, and the prospect of giving that up may appear daunting. However, you can try replacing that moment after a meal with something else, such as a piece of fruit, a healthy dessert, a square of chocolate, or a stick of gum."
            ]
          }
        ]
      },
      "firsttwoweeks": {
        "image": "assets/images/checklist.svg",
        "title": "The First 2 Weeks",
        "content": [
          {
            "title": "The first two weeks are critical for your success",
            "text": [
              "If you can get though the first two weeks your chance of success is much higher. Therefore it is important to give yourself the best chance you can during these critical weeks.",
              "The first two weeks are all about distractions, keeping busy, and being good to yourself. Keep busy with fun, low stress activities and avoid high stress ones."
            ]
          },
          {
            "title": "Stay Busy",
            "text": [
              "Keep your hands busy. Some people like to use a pen, a straw or a coffee stirrer.",
              "Drink lots of water.",
              "If you just sit there with your cravings, you are giving them room to grow.",
              "Relax and breathe deeply."
            ]
          },
          {
            "title": "Avoid High Risk Situations",
            "text": [
              "Don’t hang out with smokers. That’s like a crack addict hanging out with crack addicts. No matter how friendly and supportive your smoking friends are, they are still a high risk environment for at least the first several months.",
              "Practice saying, “No thank you, I don’t smoke anymore.”",
              "Understand that you will encounter high risk situations that you haven’t thought of. If you find yourself triggered, plan to get up and leave quickly.",
              "A change of scenery can make all the difference."
            ]
          },
          {
            "title": "Talk to Yourself",
            "text": [
              "Most cravings only last 10 - 20 minutes. Distract yourself, and the cravings will pass. When you think about using, talk to yourself and keep yourself busy.",
              "“I refuse to believe that smoking is more powerful than me.”\n“I won’t give smoking any more power over my life.”\n“I chose to be a non-smoker.”"
            ]
          }
        ]
      },
      "copingwithsymptomps": {
        "image": "assets/images/stressli.svg",
        "title": "Coping with symptoms",
        "content": [
          {
            "title": "Symptoms",
            "text":
                "Once you stop smoking, you’ll likely experience a number of physical symptoms as your body withdraws from nicotine. Nicotine withdrawal begins quickly, usually starting within an hour of the last cigarette and peaking two to three days later. Withdrawal symptoms can last for a few days to several weeks and differ from person to person."
          },
          {"title": "Common symptoms", "text": "Cigarette cravings"},
          {
            "title": "Temporary",
            "text":
                "As unpleasant as these withdrawal symptoms may be, it’s important to remember that they are only temporary. They will get better in a few weeks as the toxins are flushed from your body. In the meantime, let your friends and family know that you won’t be your usual self and ask for their understanding."
          },
          {
            "title": "How Long Do Nicotine Withdrawal Symptoms Last?",
            "text":
                "Nicotine is out of your body 72 hours after you quit smoking. Nicotine withdrawal symptoms usually reach their peak 2 to 3 days after you quit, and are gone within 1 to 3 months. It takes at least 3 months for your brain chemistry to return to normal after you quit smoking. The last two symptoms to go usually are irritability and low energy. Any effective smoking cessation program has to take into account this long adjustment period. It is why some doctors recommend weaning off nicotine slowly with nicotine replacement therapy. In summary, most people start to feel better after 1 week, and the symptoms are usually gone within 3 months."
          }
        ]
      },
      "managecravings": {
        "image": "assets/images/sleep.svg",
        "title": "Manage cigarette cravings",
        "content": [
          {
            "title": "Will soon pass",
            "text":
                "While avoiding smoking triggers will help reduce your urge to smoke, you probably can’t avoid cigarette cravings entirely. Fortunately, cravings don’t last long—typically, about 5 or 10 minutes. If you’re tempted to light up, remind yourself that the craving will soon pass and try to wait it out. It helps to be prepared in advance by having strategies to cope with cravings."
          },
          {
            "title": "Distract yourself",
            "text":
                "Do the dishes, turn on the TV, take a shower, or call a friend. The activity doesn’t matter as long as it gets your mind off smoking."
          },
          {
            "title": "Remind yourself why you quit",
            "text":
                "Focus on your reasons for quitting, including the health benefits (lowering your risk for heart disease and lung cancer, for example), improved appearance, money you’re saving, and enhanced self-esteem."
          },
          {
            "title": "Get out of a tempting situation",
            "text":
                "Where you are or what you’re doing may be triggering the craving. If so, a change of scenery can make all the difference."
          },
          {
            "title": "Reward yourself",
            "text":
                "Reinforce your victories. Whenever you triumph over a craving, give yourself a reward to keep yourself motivated."
          },
          {
            "title": "Find an oral substitute",
            "text":
                "Keep other things around to pop in your mouth when cravings hit. Try mints, carrot or celery sticks, gum, or sunflower seeds. Or suck on a drinking straw."
          },
          {
            "title": "Keep your mind busy",
            "text":
                "Read a book or magazine, listen to some music you love, do a crossword or Sudoku puzzle, or play an online game."
          },
          {
            "title": "Keep your hands busy",
            "text":
                "Squeeze balls, pencils, or paper clips are good substitutes to satisfy that need for tactile stimulation."
          },
          {
            "title": "Brush your teeth",
            "text":
                "The just-brushed, clean feeling can help banish cigarette cravings."
          },
          {
            "title": "Drink water",
            "text":
                "Slowly drink a large glass of water. Not only will it help the craving pass, but staying hydrated helps minimize the symptoms of nicotine withdrawal."
          },
          {
            "title": "Light something else",
            "text":
                "Instead of lighting a cigarette, light a candle or some incense."
          },
          {
            "title": "Get active",
            "text":
                "Go for a walk, do some jumping jacks or pushups, try some yoga stretches, or run around the block."
          },
          {
            "title": "Try to relax",
            "text":
                "Do something that calms you down, such as taking a warm bath, meditating, reading a book, or practicing deep breathing exercises."
          },
          {
            "title": "Go somewhere smoking is not permitted",
            "text":
                "Step into a public building, store, mall, coffee shop, or movie theatre, for example."
          }
        ]
      },
      "preventweightgain": {
        "image": "assets/images/eating.svg",
        "title": "Preventing weight gain",
        "content": [
          {
            "title": "Gaining weight",
            "text":
                "Smoking acts as an appetite suppressant, so gaining weight is a common concern for many of us when we decide to give up cigarettes. You may even be using it as a reason not to quit. While it’s true that many smokers put on weight within six months of stopping smoking, the gain is usually small—about five pounds on average—and that initial gain decreases over time. It’s also important to remember that carrying a few extra pounds for a few months won’t hurt your heart as much as smoking does. However, gaining weight is NOT inevitable when you stop smoking."
          },
          {
            "title": "Gaining weight is NOT inevitable",
            "text":
                "Smoking dampens your sense of smell and taste, so after you quit food will often seem more appealing. You may also gain weight if you replace the oral gratification of smoking with eating unhealthy comfort foods. Therefore, it’s important to find other, healthy ways to deal with unpleasant feelings such as stress, anxiety, or boredom rather than mindless, emotional eating."
          },
          {
            "title": "Nurture yourself",
            "text":
                "Instead of turning to cigarettes or food when you feel stressed, anxious, or depressed, learn new ways to quickly soothe yourself. Listen to uplifting music, play with a pet, or sip a cup of hot tea, for example."
          },
          {
            "title": "Eat healthy, varied meals",
            "text":
                "Eat plenty of fruit, vegetables, and healthy fats. Avoid sugary food, sodas, fried, and convenience food."
          },
          {
            "title": "Learn to eat mindfully",
            "text":
                "Emotional eating tends to be automatic and virtually mindless. It’s easy to polish off a tub of ice cream while zoning out in front of the TV or staring at your phone. But by removing distractions when you eat, it’s easier to focus on how much you’re eating and tune into your body and how you’re really feeling. Are you really still hungry or eating for another reason?"
          },
          {
            "title": "Drink lots of water",
            "text":
                "Drinking at least six to eight 8 oz. glasses will help you feel full and keep you from eating when you’re not hungry. Water will also help flush toxins from your body."
          },
          {
            "title": "Take a walk",
            "text":
                "Not only will it help you burn calories and keep the weight off, but it will also help alleviate feelings of stress and frustration that accompany smoking withdrawal."
          },
          {
            "title": "Snack on guilt-free foods",
            "text":
                "Good choices include sugar-free gum, carrot and celery sticks, or sliced bell peppers or jicama."
          }
        ]
      },
      "medication": {
        "image": "assets/images/Drcorona.svg",
        "title": "Medication and therapy to help you quit",
        "content": [
          {
            "title": "Medicating helps",
            "text":
                "There are many different methods that have successfully helped people to kick the smoking habit. While you may be successful with the first method you try, more likely you’ll have to try a number of different methods or a combination of treatments to find the ones that work best for you."
          },
          {
            "title": "Medications",
            "text":
                "Smoking cessation medications can ease withdrawal symptoms and reduce cravings. They are most effective when used as part of a comprehensive stop smoking program monitored by your physician. Talk to your doctor about your options and whether an anti-smoking medication is right for you. The U.S. Food and Drug Administration (FDA) approved options are:"
          },
          {
            "title": "Nicotine replacement therapy",
            "text":
                "Nicotine replacement therapy involves “replacing” cigarettes with other nicotine substitutes, such as nicotine gum, patch, lozenge, inhaler, or nasal spray. It relieves some of the withdrawal symptoms by delivering small and steady doses of nicotine into your body without the tars and poisonous gases found in cigarettes. This type of treatment helps you focus on breaking your psychological addiction and makes it easier to concentrate on learning new behaviors and coping skills."
          },
          {
            "title": "Non-nicotine medication",
            "text":
                "These medications help you stop smoking by reducing cravings and withdrawal symptoms without the use of nicotine. Medications such as bupropion (Zyban) and varenicline (Chantix, Champix) are intended for short-term use only."
          },
          {
            "title": "Alternative therapies",
            "text":
                "There are several things you can do to stop smoking that don’t involve nicotine replacement therapy, vaping, or prescription medications. These include:"
          },
          {
            "title": "Hypnosis",
            "text":
                "This is a popular option that has produced good results for many smokers struggling to quit. Forget anything you may have seen from stage hypnotists, hypnosis works by getting you into a deeply relaxed state where you are open to suggestions that strengthen your resolve to stop smoking and increase your negative feelings toward cigarettes."
          },
          {
            "title": "Acupuncture",
            "text":
                "One of the oldest known medical techniques, acupuncture is believed to work by triggering the release of endorphins (natural pain relievers) that allow the body to relax. As a smoking cessation aid, acupuncture can be helpful in managing smoking withdrawal symptoms."
          },
          {
            "title": "Behavioral Therapy",
            "text":
                "Nicotine addiction is related to the habitual behaviors or rituals involved in smoking. Behavior therapy focuses on learning new coping skills and breaking those habits."
          },
          {
            "title": "Motivational Therapies",
            "text":
                "Self-help books and websites can provide a number of ways to motivate yourself to give up smoking. One well known example is calculating the monetary savings. Some people have been able to find the motivation to quit just by calculating how much money they will save. It may be enough to pay for a summer vacation."
          }
        ]
      },
      "ifyouslip": {
        "image": "assets/images/countingstars.svg",
        "title": "What to do if you slip or relapse",
        "content": [
          {
            "title": "Completely normal",
            "text":
                "Most people try to stop smoking several times before they kick the habit for good, so don’t beat yourself up if you slip up and smoke a cigarette. Instead, turn the relapse into a rebound by learning from your mistake. Analyze what happened right before you started smoking again, identify the triggers or trouble spots you ran into, and make a new stop-smoking plan that eliminates them."
          },
          {
            "title": "It's not the end",
            "text":
                "It’s also important to emphasize the difference between a slip and a relapse. If you start smoking again, it doesn’t mean that you can’t get back on the wagon. You can choose to learn from the slip and let it motivate you to try harder or you can use it as an excuse to go back to your smoking habit. But the choice is yours. A slip doesn’t have to turn into a full-blown relapse."
          },
          {
            "title": "You’re not a failure if you slip up",
            "text": "It doesn’t mean you can’t quit for good."
          },
          {
            "title": "Don’t let a slip become a mudslide",
            "text":
                "Throw out the rest of the pack. It’s important to get back on the non-smoking track as soon as possible."
          },
          {
            "title": "Look back at your quit log and feel good",
            "text":
                "Look back at your quit log and feel good about the time you went without smoking."
          },
          {
            "title": "Find the trigger",
            "text":
                "Exactly what was it that made you smoke again? Decide how you will cope with that issue the next time it comes up."
          },
          {
            "title": "Learn from your experience",
            "text": "What has been most helpful? What didn’t work?"
          },
          {
            "title": "Are you using a medicine to help you quit?",
            "text":
                "Call your doctor if you start smoking again. Some medicines cannot be used if you’re smoking at the same time."
          }
        ]
      }
    },
    "guideps": {
      "guideto": "Your guide to\nQuit Smoking",
      "guides": "Guides",
      "search": "Search"
    },
    "home": {
      "wallet": "Wallet",
      "progress": "Progress",
      "guide": "Guide",
      "rewards": "Rewards",
      "minafter": "MINUTES AFTER",
      "achievement": "Next Goal",
      "da": "Days",

      "daysafter": "AFTER X DAYS",
      "timePassed": "Time that passed since you took last Questionnaire",
      "moneyEarned": "Total money earned",
      "cigarratesnotsmoked": "Cigarattes not smoked",
      "day": "Level",
      "minute": "Minute",
      "second": "Second",
      "hour": "Hour",
      "resetall": "Do not give up!",
      "resetallq":
          "Stumbling on the road to success is not a failure. Giving up is a failure.",
      "reset": "Reset",
      "cancel": "Cancel",
      "settings": "Settings",
      "reason": "Reasons"
    },
    "process": {
      "daysleft": "Questionnaires left",
      "minleft": "minutes left",
      "completed": "Completed"
    },
    "wallet": {
      "balance": "Balance",
      "newtransaction": "Add New Transaction",
      "title": "Title",
      "description": "Description",
      "amount": "Amount of money",
      "add": "Add",
      "daily": "Daily",
      "weekly": "Weekly",
      "monthly": "Monthly",
      "yearly": "Yearly"
    },
    "reason": {
      "addnew": "Add a new reason",
      "somegoodreasons": "Some Good Reasons",
      "reason": "Write here"
    },
    "misc": {
      "areusuredelete": "Are you sure you wish to delete this item?",
      "confirm": "Confirm",
      "delete": "Delete",
      "cancel": "Cancel"
    },
    "welcome": {
      "welcometext": "You are making a great decision",
      "tellreason": "Before we start, tell us why do you want to quit smoking",
      "reasonhint": "I want to quit smoking because...",
      "weneedtoknow": "Few things we need to know",
      "howmanyperday": "How many cigarattes do you smoke in a day",
      "howmuchpercigcost": "How much does one cigarette cost you?",
      "choosecurrency": "Choose your currency",
      "start": "Start Now!",
      "addtolist": "Add to list"
    },
    "settings": {
      "save": "Save",
      "youstopped": "The time you stopped smoking",
      "change": "Change"
    }
  }
};
