
import 'dart:ui';

import 'package:moodyapp/Localization/app_translations.dart';
import 'package:moodyapp/Screens/TabBarScreens/Tabbar/Tabbar.dart';
import 'package:moodyapp/Screens/TabBarScreens/UserProfile/UserProfile.dart';
import 'package:flutter/material.dart';

import 'package:shared_preferences/shared_preferences.dart';

import '../main.dart';
import 'Constant.dart';

class SharedManager {
  static String firebaseId;

  static final SharedManager shared = SharedManager._internal(firebaseId);




  factory SharedManager() {
    return shared;
  }

  SharedManager._internal(firebaseId);

  // bool isRTL = true;
  bool isRTL = false;
  var direction = TextDirection.ltr;
  var count = 2;
  bool isOnboarding = false;
  int currentIndex = 0;
  var fontFamilyName = "Quicksand";
bool isOpenMessageScreen = false;
var ipAddress = "";

String name;
String mobile;
String specility;
bool isDoctor;

  var language =   Locale("en", "");
  // var language =   Locale("es", "");
  // var language =   Locale("ar", "");
  // var language =   Locale("fr", "");

setNavigation(BuildContext context, dynamic viewScreen){
  Navigator.of(context).push(MaterialPageRoute(builder: (context)=>viewScreen()));
}

  storeBoolValueLocally(bool value,String key)async{
    final prefs = await SharedPreferences.getInstance();
          await prefs.setBool(key, value);
  }

  dynamic retriveStoredValue(String key)async{
    final prefs = await SharedPreferences.getInstance();
    return prefs.get(key);
  }


String themeType = 'light';
ThemeData getThemeType(){
    return new ThemeData(
      brightness: _getBrightness(),
    );
}

Brightness _getBrightness(){
  if(themeType == "dark"){
     return Brightness.dark;
  }
  else{
    return Brightness.light;
  }
}

final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();



ValueNotifier<Locale> locale = new ValueNotifier(Locale('en', ''));

      String validateEmail(String value) {
          Pattern pattern =
              r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
          RegExp regex = new RegExp(pattern);
          if (!regex.hasMatch(value))
            return 'Enter Valid Email';
          else
            return null;
        }




void showAlertDialog(String message,BuildContext context) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Health Care"),
          content: new Text(message),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("OK"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

}