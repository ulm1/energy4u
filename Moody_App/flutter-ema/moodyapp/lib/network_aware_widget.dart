import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:moodyapp/widgets/network_status_service.dart';
import 'package:provider/provider.dart';
import 'package:webview_flutter/src/webview.dart';

import 'package:dcdg/dcdg.dart';


class NetworkAwareWidget extends StatelessWidget {
  final Widget onlineChild;
  final Widget offlineChild;

  const NetworkAwareWidget({Key key,  this.onlineChild,  this.offlineChild})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    NetworkStatus networkStatus = Provider.of<NetworkStatus>(context);
    if (networkStatus == NetworkStatus.Online) {
      setStatue(true);
      return onlineChild;
    } else {
      setStatue(false);

     // _showToastMessage("Offline");
      return offlineChild;
    }
  }

}

class NetworkAwareWidgetServer extends StatelessWidget {
  final Widget onlineChild;
  final Widget offlineChild;

  const NetworkAwareWidgetServer({Key key,  this.onlineChild,  this.offlineChild})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    NetworkStatus networkStatus = Provider.of<NetworkStatus>(context);
    if (networkStatus == NetworkStatus.Online) {
      setStatue(true);
      return onlineChild;
    } else {
      setStatue(false);

      // _showToastMessage("Offline");
      return offlineChild;
    }
  }

}



bool online = false;

setStatue(bool stat){

  online = stat;
  return online;

}
getStatue(){
  return online;
}
