# flutter-ema

Parse Server Prerequisites

NodeJS 16.13.2 (LTS) 

MongoDB
 
current version MongoDB Compass

Setup instructions:

    1.  First install MongoDB on your computer:
   For Windows
   https://docs.mongodb.com/manual/tutorial/install-mongodb-on-windows/
   
   For Mac
   https://docs.mongodb.com/manual/tutorial/install-mongodb-on-os-x/

    2.  Now install the newest stable version of MongoDB compass under: https://www.mongodb.com/try/download/compass
    3.  On the MongoDB compass installation setup screen make sure the service checkbox is selected
    4.  After installation launch the MongoDB Compass application.
    5.  Click on the New Connection tab and in the SRV text type: MongoDB : mongodb://localhost:27017 and then click on the connect button.
    6.  Click on the connect button
    7.  Proceed with the Parse Server Setup


Parse Server Setup:

    1.  Clone this repo.
    2.  Go into the flutter-ema/Parse-Server directory on the command prompt and install the sever by typing: npm install
    3.  Run MongoDB-communtiy on the Terminal in your local directory by typing: brew services start mongodb-community@'version_number' .
    4.  Now go into the flutter-ema/Parse-Server directory on the command prompt and run the server by typing: npm start
    5.  Proceed to the Parse Dashboard Setup.


Parse Dashboard Setup:

    1.  Run the command prompt and install the parse dashboard by typing : npm install -g parse-dashboard
    2.  Now go into the flutter-ema directory on the command prompt and load the config file for the Parse Dashboard by typing: parse-dashboard --config parse-dashboard-config.json --allowInsecureHTTP true
    3.  Open your browser and navigate to http://0.0.0.0:4040/.
    4.  Type "admin" for password and username on the Parse Dashboard Screen on your browser.
    5.  Open Android Studio and proceed with the Android Studio Setup instructions.

Android Studio Setup:

    1.  Run flutter pub get in the Terminal to download all packages
    2.  Setup the correct URL/IP in the directory: flutter-ema/moodyapp/lib/domain/constants/application_constants.dart.
    3.  Launch the App in the Android Studio application.

