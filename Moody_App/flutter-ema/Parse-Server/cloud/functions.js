

const fs = require("fs");




let jsonData = require('./questionnaire_v01.json');

console.log(jsonData);

Parse.Cloud.define('hello', req => {
  req.log.info(req);
  return 'Hi';
});

Parse.Cloud.define('asyncFunction', async req => {
  await new Promise(resolve => setTimeout(resolve, 1000));
  req.log.info(req);
  return 'Hi async';
});

Parse.Cloud.beforeSave('Test', () => {
  throw new Parse.Error(9001, 'Saving test objects is not available.');
});

Parse.Cloud.define("hellxo", async (request) => {
  console.log("Hello from Cloud Code!");
  return "Hello from Cloud Code!";
});

Parse.Cloud.define("sumNumbers", async (request) => {
  return (json);
});

Parse.Cloud.define("createToDo", async (request) => {
  const title = request.params.title;
  const done = request.params.done;
  try {
      //await jsonData.save();
      return jsonData;
    } catch (error) {
      console.log('ToDo create - Error - ' + error.code + ' ' + error.message);
    }
});

Parse.Cloud.define("getListToDo", async (request) => {
  let query = new Parse.Query("ToDo");
  query.descending("done");
  return await query.find();
});

Parse.Cloud.define("editUserProperty", async (request) => {
  const { objectId, email,password } = request.params;
  var User = Parse.Object.extend(Parse.User);
  var query = new Parse.Query(User);
  let result = await query.get(objectId, { useMasterKey: true });
  if (!result) new Error("No user found!");
result.set("email", email);
result.set("password", password);
result.set("anonymous", false);
result.set("gamification", Math.random() < 0.5);

  await result.save(null, { useMasterKey: true }).then(function(){
		return 'success';
	}, function(error){
		throw error;
	});
	
	return result;
});


Parse.Cloud.define("updateUser", async (request) => {
  const { objectId, username } = request.params;
  var User = Parse.Object.extend(Parse.User);
  var query = new Parse.Query(Parse.User);

  query.equalTo("objectId", objectId);
  query.first({
      success: function(object) 
      {
        object.set("username", username);
        object.save(null, { useMasterKey: true }).then(function(){
        return 'success';
      }, function(error){
        throw error;
      });

    }
  });
}); 