Parse.Cloud.define("hello", async (request) => {
    console.log("Hello from Cloud Code!");
    return "Hello from Cloud Code!";
});

Parse.Cloud.define("sumNumbers", async (request) => {
    return (request.params.number1 + request.params.number2);
});

Parse.Cloud.define("createToDo", async (request) => {
    const title = request.params.title;
    const done = request.params.done;

    const Todo = Parse.Object.extend('ToDo');
    const todo = new Todo();
    todo.set('title', title);
    todo.set('done', done);

    try {
        await todo.save();
        return todo;
      } catch (error) {
        console.log('ToDo create - Error - ' + error.code + ' ' + error.message);
      }
});

Parse.Cloud.define("getListToDo", async (request) => {
    let query = new Parse.Query("ToDo");
    query.descending("done");
    return await query.find();
});