
package client.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import client.application.Roboter;

/**
 *
 * @author ra7
 */
public class PlayerTest {
    
    public PlayerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setUpClass method, of class PlayerTest.
     */
    @Test
    public void testSetUpClass() {
        System.out.println("setUpClass");
        PlayerTest.setUpClass();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of tearDownClass method, of class PlayerTest.
     */
    @Test
    public void testTearDownClass() {
        System.out.println("tearDownClass");
        PlayerTest.tearDownClass();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setUp method, of class PlayerTest.
     */
    @Test
    public void testSetUp() {
        System.out.println("setUp");
        PlayerTest instance = new PlayerTest();
        instance.setUp();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of tearDown method, of class PlayerTest.
     */
    @Test
    public void testTearDown() {
        System.out.println("tearDown");
        PlayerTest instance = new PlayerTest();
        instance.tearDown();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
	public void playertest() {

		Player p = new Player("Jimmy", false, 5);
		String expectedname = "Jimmy";
		String actual = p.userName;
		assertEquals("The name should be Jimmy", expectedname, actual);
	}

	@Test
	public void isAitest() {

		Player p = new Player("Jimmy", false, 5);
		boolean expectedValue = false;
		boolean actual = p.isAI;
		assertEquals("The value should be false", expectedValue, actual);
	}

	@Test
	public void isUserId() {

		Player p = new Player("Jimmy", false, 5);
		int userId = 5;
		int actual = p.userID;
		assertEquals("The value should be 5", userId, actual);
	}

	@Test
	public void setColor() {
		Player p = new Player("Jimmy", false, 5);
		p.setColor("RED");
		String color = "RED";
		String actual = p.color;
		assertEquals("The value should be Red", color, actual);
	}

	@Test
	public void setRoboterX() {
		Player p = new Player("Jimmy", false, 5);

		Roboter roboter = new Roboter(5, 3, 1);
		p.setRoboter(roboter);
		int roboXpos = 5;
		int actual = p.roboter.xPos;
		assertEquals("The value should be 5", roboXpos, actual);
	}

	@Test
	public void setRoboterY() {
		Player p = new Player("Jimmy", false, 5);

		Roboter roboter = new Roboter(5, 3, 1);
		p.setRoboter(roboter);
		int roboYpos = 3;
		int actual = p.roboter.yPos;
		assertEquals("The value should be 3", roboYpos, actual);
	}

	@Test
	public void setRoboterID() {
		Player p = new Player("Jimmy", false, 5);

		Roboter roboter = new Roboter(5, 3, 1);
		p.setRoboter(roboter);
		int roboID = 1;
		int actual = p.roboter.RoboterID;
		assertEquals("The value should be 1", roboID, actual);
		p.getRoboter();
		Roboter expected = roboter;
		Roboter act = p.getRoboter();
		assertEquals("The roboter should be roboter ", expected, act);

	}

}