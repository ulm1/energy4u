package client.model;

import java.util.Observer;

/**
 * Abstract Object for every user connected to the server. Every Participant is
 * either a Player or a Watcher.
 * 
 * @author ali, fabian
 */
public abstract class Participant implements Observer {

	public String partName;
	public String role;
	public boolean isAI;
	public int userID;

	/**
	 * Constructor of the Object Participant.
	 * 
	 * @param name
	 *            The name of the participant
	 * @param role
	 *            The role of the participant (USER or SPECTATOR)
	 * @param ai
	 *            Boolean specifying whether the participant is an AI
	 * @param uid
	 *            The ID associated with the participant
	 */
	public Participant(String name, String role, boolean ai, int uid) {
		this.partName = name;
		this.role = role;
		this.isAI = ai;
		this.userID = uid;
	}

}