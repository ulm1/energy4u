package client.util;

import client.application.Roboter;

/**
 * Object for staging Roboters in the Animation Queue of the GameField.
 * AffectedNext indicates the Queue if the current Roboter Animation should be 
 * executed simultaneously with the next Roboter Animation in line. 
 * @author Ali Askari
 *
 */
public class AnimationQueueType {
	public Roboter robo;
	public boolean affectedNext;
	public boolean PAUSE_TOGGLE;
	
	/**
	 * Constructor of the object AnimationQueueType.
	 * 
	 * @param robo The Roboter affecting other Roboters
	 * @param affectedNext The next Roboter affected
	 */
	public AnimationQueueType(Roboter robo, boolean affectedNext) {
		this.robo = robo;
		this.affectedNext = affectedNext; 
	}
	
	
	public void AnimationQueueTypes( boolean affectedNext) {
	
		this.affectedNext = affectedNext; 
	}
	
	public AnimationQueueType(boolean pause) {
		this.PAUSE_TOGGLE = true;
	}
}
