package client.util;

import static org.junit.Assert.*;

import org.junit.Test;

public class CardsTest {

	@Test
	public void Cards() {
		Cards instance = new Cards("RIGHT", "Position1");		
	    String expectedCard = instance.cardType;
		assertEquals("The value should be 1",expectedCard,"RIGHT");
	}
	
	
	@Test
	public void test_method_Cards_1()
	{
		Cards instance = new Cards("RIGHT", 200);		
	    int expectedCode = instance.cardCode;
		assertEquals("The value should be 1",expectedCode,200);    
	}
	
	
	@Test
	public void test_method_Cards_2()
	{
		Cards instance = new Cards("RIGHT", 200, "UP");
		String expectedCard = instance.cardType;
	    int expectedCode = instance.cardCode;
	    assertEquals("The value should be 1",expectedCard,"RIGHT");
	    assertEquals("The value should be 1",expectedCode,200);    
		assertEquals("UP", instance.orientation);
	}

	
	 
	@Test
	public void test_method_getLocation()
	{
		Cards instance = new Cards("RIGHT", "Position1");		
		String actualPos = instance.getLocation();
		assertEquals("The value should be 1",actualPos,"Position1");	
	}
	
	
	
	@Test
	public void test_method_setLocation()
	{
		System.out.println("Now Testing Method:setLocation Branch:0");
		
		//Constructor
		Cards instance = new Cards("RIGHT", "Position_1");
		
		//Call Method
		instance.setLocation("Position_1");
		
		//Check Test Verification Points
		assertEquals("Position_1", instance.location);
		
	}

	/*
	 * Testing Conditon(s): Default
	 */
	@Test
	public void test_method_getCardType()
	{
		System.out.println("Now Testing Method:getCardType");
		
		//Constructor
		Cards instance = new Cards("RIGHT", "Position_1");
		
		//Get expected result and result
		String expResult ="RIGHT" ;
		String result = instance.getCardType();
		
		//Check Return value
		assertEquals(expResult, result);
		
		//Check Test Verification Points
	
		
	}

	/*
	 * Testing Conditon(s): Default
	 */
	@Test
	public void test_method_setCardType()
	{
		System.out.println("Now Testing Method:setCardType Branch:0");
		
		//Constructor
		Cards instance = new Cards("RIGHT", "Position_1");
		
		//Call Method
		instance.setCardType("RIGHT");
		
		//Check Test Verification Points
		assertEquals("RIGHT", instance.cardType);
		
	}

	
	
@Test
public void testgetCardCode() {
	Cards instance = new Cards("RIGHT", 200);		
    int expectedCode = instance.getCardCode();
    assertEquals("The value should be 1",expectedCode,200);  
}

@Test
public void testsetCardCode() {
	Cards instance = new Cards("RIGHT", 200);	
	instance.setCardCode(500);
	  int actual = instance.getCardCode();
	    assertEquals("The value should be 1",500,actual);  
	}
	


@Test
public void testgetOrientation() {
	
	Cards instance = new Cards("RIGHT", 200, "UP");
	String actual = instance.getOrientation();
	assertEquals("The value should be 1","UP",actual);  
}





@Test
public void testsetOrientation() {
	
	Cards instance = new Cards("RIGHT", 200, "DOWN");
	instance.setOrientation("UP");
	String actual = instance.getOrientation();
	assertEquals("The value should be 1","UP",actual);  
}
	
}	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	



