package client.util;

import static org.junit.Assert.*;

import org.junit.Test;

public class CoordinateTest {

	@Test
	public void test_method_Coordinate_0_branch_0()
	{
		System.out.println("Now Testing Method:Coordinates");
		
		//Constructor
		Coordinate instance = new Coordinate(5, 6);
		
		//Check Test Verification Points
		assertEquals(5, instance.x_Value);
		assertEquals(6, instance.y_Value);
		
	}
}