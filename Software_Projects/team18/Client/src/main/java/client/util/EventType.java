package client.util;

/**
 * Possible events occurring during a round for a specific user.
 * 
 * @author ali, fabian, holger
 */
public enum EventType {
	/**
	 * Robot moves one step forward in its current direction (orientation).
	 */
	MOVE,
	
	
	MOVE2,
	

	MOVE3,
	
	REDLASER_HIT_R,
	
	BLUELASER_HIT_R,
	
	REDLASER_HIT_DR,
	
	BLUELASER_HIT_DR,
	
	REDLASER_HIT_DL,
	
	BLUELASER_HIT_DL,
	
	REDLASER_HIT_D,
	
	BLUELASER_HIT_D,
	
	ROTATE180,
	/**
	 * Robot moves one step towards the opposite of its current direction
	 * (orientation).
	 */
	MOVE_BACKWARD,
	/**
	 * Robot collides with a wall while trying to move forward.
	 */
	COLLIDE_WITH_WALL,
	/**
	 * Robot collides with a wall while trying to move backward.
	 */
	COLLIDE_WITH_WALL_BACKWARD,
	/**
	 * Robot has arrived at a checkpoint.
	 */
	CHECKPOINT_REACHED,
	/**
	 * Robot has left the borders of the map.
	 */
	FALL_FROM_MAP,
	/**
	 * Robot rotates 90 degrees counter-clockwise.
	 */
	ROTATE_LEFT,
	/**
	 * Robot rotates 90 degrees clockwise.
	 */
	ROTATE_RIGHT,
	/**
	 * Robot reappears at its last checkpoint after having been destroyed.
	 */
	SPAWN,
	/**
	 * Robot is destroyed and taken from the map.
	 */
	DESTROY,
	/**
	 * Robot has reached all checkpoints in correct order and wins the game.
	 */
	WIN,
	/**
	 * Robot was destroyed too often and does no longer participate in the game.
	 */
	DISQUALIFIED, COLLIDE_WITH_Robo, HEALTH,
	/**
	 * Robot disappears after teleport.
	 */
	
	TELEPORT_DISAPPEAR,
	
	
	
	/**
	 * Robot reappears after teleport.
	 */
	TELEPORT_REAPPEAR,
	
	/**
	 * Robot is repaired.
	 */
	
	REPAIRED,
	/**
	 * Robot has reached an health field at the end of a round.
	 */
	HEALTH_FIELD_REACHED
	
}