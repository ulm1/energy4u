package client.util;

import static org.junit.Assert.*;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import org.junit.Test;

import com.jme3.app.Application;
import com.jme3.app.LostFocusBehavior;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioRenderer;
import com.jme3.audio.Listener;
import com.jme3.input.InputManager;
import com.jme3.profile.AppProfiler;
import com.jme3.renderer.Camera;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.Renderer;
import com.jme3.renderer.ViewPort;
import com.jme3.system.AppSettings;
import com.jme3.system.JmeContext;
import com.jme3.system.Timer;

public class NumberFactoryTest {

		@Test
		public void test() {

		NumberFactory f = new NumberFactory();
		Application app  = new Application() {
			
			@Override
			public void stop(boolean arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void stop() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void start(boolean arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void start() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void setTimer(Timer arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void setSettings(AppSettings arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void setPauseOnLostFocus(boolean arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void setLostFocusBehavior(LostFocusBehavior arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void setAppProfiler(AppProfiler arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void restart() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public boolean isPauseOnLostFocus() {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public ViewPort getViewPort() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Timer getTimer() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public AppStateManager getStateManager() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Renderer getRenderer() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public RenderManager getRenderManager() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public LostFocusBehavior getLostFocusBehavior() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Listener getListener() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public InputManager getInputManager() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public ViewPort getGuiViewPort() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public JmeContext getContext() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Camera getCamera() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public AudioRenderer getAudioRenderer() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public AssetManager getAssetManager() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public AppProfiler getAppProfiler() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public void enqueue(Runnable arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public <V> Future<V> enqueue(Callable<V> arg0) {
				// TODO Auto-generated method stub
				return null;
			}
		};
		NumberFactory.getNumber(5, app);
		assertEquals("The value should be 5",NumberFactory.numberID);  
		
			
			
		}

	}

