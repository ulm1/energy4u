//package client.controller;
//
//import static org.junit.Assert.*;
//
//import java.io.ByteArrayOutputStream;
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.io.InputStream;
//import java.util.ArrayList;
//
//import javax.json.Json;
//import javax.json.JsonObject;
//import javax.json.JsonReader;
//
//import org.junit.Test;
//
//import client.model.Map;
//import client.model.Participant;
//import client.model.Player;
//import client.util.BorderType;
//import client.util.Compass;
//import client.util.Event;
//import client.util.EventType;
//
//public class TestClientController {
//
//	public static final String PROTOCOL_VERSION = "1.0.0";
//
//	/**
//	 * Test the method encodeLoginRequest and getNumOfBytes in
//	 * NetworkController.
//	 * 
//	 * @author fabian
//	 */
//	@Test
//	public void TestEncodeLoginRequest() {
//		System.out.println("Testing getNumOfBytes and the JsonObject LOGIN");
//		JsonObject jsonOb = NetworkController.encodeLoginRequest("testName", "USER", true);
//		assertEquals("Type of the JsonObject must be LOGIN", "LOGIN", jsonOb.getJsonObject("head").getString("type"));
//		assertEquals("Protocol version must be 1.0.0", PROTOCOL_VERSION,
//				jsonOb.getJsonObject("head").getString("protocolVersion"));
//		assertEquals("Name must be testName", "testName", jsonOb.getJsonObject("body").getString("name"));
//		assertEquals("AI must be true", true, jsonOb.getJsonObject("body").getBoolean("ai"));
//		assertEquals("Role must be USER", "USER", jsonOb.getJsonObject("body").getString("role"));
//		int expected = jsonOb.toString().length();
//		int actual = NetworkController.getNumOfBytes(jsonOb);
//		assertEquals("Number of bytes must equal length of the string", expected, actual);
//
//		jsonOb = NetworkController.encodeLoginRequest("anotherName", "SPECTATOR", false);
//		assertEquals("Type of the JsonObject must be LOGIN", "LOGIN", jsonOb.getJsonObject("head").getString("type"));
//		assertEquals("Protocol version must be 1.0.0", PROTOCOL_VERSION,
//				jsonOb.getJsonObject("head").getString("protocolVersion"));
//		assertEquals("Name must be anotherName", "anotherName", jsonOb.getJsonObject("body").getString("name"));
//		assertEquals("AI must be false", false, jsonOb.getJsonObject("body").getBoolean("ai"));
//		assertEquals("Role must be SPECTATOR", "SPECTATOR", jsonOb.getJsonObject("body").getString("role"));
//		expected = jsonOb.toString().length();
//		actual = NetworkController.getNumOfBytes(jsonOb);
//		assertEquals("Number of bytes must equal length of the string", expected, actual);
//	}
//
//	/**
//	 * Test the method encodeMessage and getNumOfBytes in NetworkController.
//	 * 
//	 * @author fabian
//	 */
//	@Test
//	public void TestEncodeMessage() {
//		System.out.println("Testing the JsonObject MESSAGE");
//		JsonObject jsonOb = NetworkController.encodeMessage(3, "testMessage");
//		assertEquals("Type of the JsonObject must be MESSAGE", "MESSAGE",
//				jsonOb.getJsonObject("head").getString("type"));
//		assertEquals("Protocol version must be 1.0.0", PROTOCOL_VERSION,
//				jsonOb.getJsonObject("head").getString("protocolVersion"));
//		assertEquals("Content must be testMessage", "testMessage", jsonOb.getJsonObject("body").getString("content"));
//		assertEquals("ID must be 3", 3, jsonOb.getJsonObject("body").getInt("uid"));
//		int expected = jsonOb.toString().length();
//		int actual = NetworkController.getNumOfBytes(jsonOb);
//		assertEquals("Number of bytes must equal length of the string", expected, actual);
//
//		jsonOb = NetworkController.encodeMessage(10, "");
//		assertEquals("Type of the JsonObject must be MESSAGE", "MESSAGE",
//				jsonOb.getJsonObject("head").getString("type"));
//		assertEquals("Protocol version must be 1.0.0", PROTOCOL_VERSION,
//				jsonOb.getJsonObject("head").getString("protocolVersion"));
//		assertEquals("Content must be empty", "", jsonOb.getJsonObject("body").getString("content"));
//		assertEquals("ID must be 10", 10, jsonOb.getJsonObject("body").getInt("uid"));
//		expected = jsonOb.toString().length();
//		actual = NetworkController.getNumOfBytes(jsonOb);
//		assertEquals("Number of bytes must equal length of the string", expected, actual);
//	}
//
//	/**
//	 * Test the method decodeGameConfigMessage in NetworkController.
//	 * 
//	 * @throws FileNotFoundException
//	 *             Thrown if the JsonFile is not present.
//	 * @author fabian
//	 */
//	@Test
//	public void TestDecodeGameConfigMessage() throws FileNotFoundException {
//		System.out.println("Testing decodeGameConfigMessage");
//		File jsonInputFile = new File("JSON_Files/gameconfig.json");
//		InputStream is;
//		is = new FileInputStream(jsonInputFile);
//		JsonReader reader = Json.createReader(is);
//		JsonObject jsonOb = reader.readObject();
//		reader.close();
//		Map map = NetworkController.decodeGameConfigMessage(jsonOb);
//
//		assertEquals("Maximal number of spectators must be 10", 10, map.mapMaxSpectators);
//		assertEquals("Minimal number of players must be 2", 2, map.mapMinPlayers);
//		assertEquals("Maximum number of players must be 8", 8, map.mapMaxPlayers);
//		assertEquals("Card selection time must be 10000", 10000, map.mapChoosingTime);
//		assertEquals("Width of the field must be 10", 10, (int) map.dimensions.getWidth());
//		assertEquals("Height of the field must be 10", 10, (int) map.dimensions.getHeight());
//
//		int[] xMustBe = { 2, 2, 7, 6, 10 };
//		int[] yMustBe = { 8, 4, 7, 1, 7 };
//		for (int k = 0; k < map.checkpoints.size(); k++) {
//			assertEquals("X field must be " + xMustBe[k], xMustBe[k], map.checkpoints.get(k).x_Value);
//			assertEquals("Y field must be " + yMustBe[k], yMustBe[k], map.checkpoints.get(k).y_Value);
//		}
//
//		int[] xM = { 0, 0, 0, 0, 1, 1, 2, 2, 2, 3, 3, 4, 5, 5, 5, 5, 5, 5, 6, 6, 6, 7, 8, 9, 9, 9, 9, 10, 10, 10, 10,
//				10 };
//		int[] yM = { 1, 2, 9, 10, 1, 10, 1, 8, 10, 5, 8, 3, 1, 2, 3, 7, 8, 9, 7, 8, 9, 7, 3, 0, 1, 2, 3, 3, 7, 8, 9,
//				10 };
//		int[] wa = { 1, 1, 1, 1, 2, 2, 2, 1, 2, 3, 3, 1, 1, 1, 3, 1, 1, 1, 2, 2, 2, 3, 2, 2, 1, 1, 2, 2, 2, 1, 1, 1 };
//
//		int countWalls = 0;
//		for (int k = 0; k < map.cartography.length; k++) {
//			for (int j = 0; j < map.cartography[0].length; j++) {
//
//				for (int q = 0; q < xM.length; q++) {
//					if (xM[q] == k && yM[q] == j) {
//						BorderType control;
//
//						if (wa[q] == 1) {
//							control = BorderType.EAST;
//						} else if (wa[q] == 2) {
//							control = BorderType.SOUTH;
//						} else {
//							control = BorderType.SOUTH_EAST;
//						}
//						assertEquals("Border types must be equal", control, map.cartography[k][j].border);
//						countWalls++;
//						break;
//					}
//				}
//			}
//		}
//		assertEquals("Number of walls must be equal", xM.length, countWalls);
//	}
//
//	/**
//	 * Test the method decodeLobbyStatus in NetworkController.
//	 * 
//	 * @throws FileNotFoundException
//	 *             Thrown if the JsonFile is not present.
//	 * @author fabian
//	 */
//	@Test
//	public void TestDecodeLobbyStatus() throws FileNotFoundException {
//		System.out.println("Testing decodeLobbyStatus");
//		File jsonInputFile = new File("JSON_Files/lobby.json");
//		InputStream is;
//		is = new FileInputStream(jsonInputFile);
//		JsonReader reader = Json.createReader(is);
//		JsonObject jsonOb = reader.readObject();
//		reader.close();
//
//		int[] userIDs = { 1, 2, 3 };
//		boolean[] AIs = { false, false, false };
//		String[] names = { "player1", "spectator1", "testName" };
//		String[] isPlay = { "USER", "SPECTATOR", "SPECTATOR" };
//
//		ArrayList<Participant> partList = NetworkController.decodeLobbyStatus(jsonOb);
//
//		for (int k = 0; k < partList.size(); k++) {
//			assertEquals("IDs must coincide", userIDs[k], partList.get(k).userID);
//			assertEquals("AIs must coincide", AIs[k], partList.get(k).isAI);
//			assertEquals("Names must coincide", names[k], partList.get(k).partName);
//			assertEquals("Roles must coincide", isPlay[k], partList.get(k).role);
//		}
//	}
//
//	/**
//	 * Test the method decodeRoundResult in NetworkController.
//	 * 
//	 * @throws FileNotFoundException
//	 *             Thrown if the JsonFile is not present.
//	 * @author fabian
//	 */
//	@Test
//	public void TestDecodeRoundResult() throws FileNotFoundException {
//		System.out.println("Testing decodeRoundResult");
//		File jsonInputFile = new File("JSON_Files/roundresult.json");
//		InputStream is;
//		is = new FileInputStream(jsonInputFile);
//		JsonReader reader = Json.createReader(is);
//		JsonObject jsonOb = reader.readObject();
//		reader.close();
//
//		int[] uIDs = { 5, 5, 4, 4, 4, 1 };
//		int[][] affUid = { {}, {}, {}, {}, {}, {} };
//		EventType[] events = { EventType.MOVE_BACKWARD, EventType.CHECKPOINT_REACHED, EventType.MOVE, EventType.MOVE,
//				EventType.COLLIDE_WITH_WALL, EventType.ROTATE_LEFT };
//		int[] xP = { 7, 7, 3, 4, 4, 7 };
//		int[] yP = { 7, 7, 3, 3, 3, 4 };
//		Compass[] or = { Compass.NORTH, Compass.NORTH, Compass.EAST, Compass.EAST, Compass.EAST, Compass.WEST };
//
//		ArrayList<Event> eventList = NetworkController.decodeRoundResult(jsonOb);
//		for (int k = 0; k < eventList.size(); k++) {
//			assertEquals("ID must be equal", uIDs[k], eventList.get(k).userID);
//			assertEquals("Event must be equal", events[k], eventList.get(k).eventType);
//			assertEquals("Number of affected IDs must coincide", affUid[k].length, eventList.get(k).affectedUID.length);
//			assertEquals("X positions must coincide", xP[k], eventList.get(k).position.x_Value);
//			assertEquals("Y positions must coincide", yP[k], eventList.get(k).position.y_Value);
//			assertEquals("Orientations must coincide", or[k], eventList.get(k).orientation);
//		}
//	}
//
//	@Test
//	public void TestHandleWatcherErrors() {
//
//	}
//
//	/**
//	 * Test the method sendData in NetworkController.
//	 * 
//	 * @author fabian
//	 */
//	@Test
//	public void TestSendData() {
//		System.out.println("Testing sendData");
//		ByteArrayOutputStream baos = new ByteArrayOutputStream();
//
//		JsonObject jsonOb = NetworkController.encodeMessage(2, "testMessage");
//		NetworkController.sendData(baos, jsonOb);
//		assertEquals("JsonObject must not change when sent",
//				NetworkController.getNumOfBytes(jsonOb) + jsonOb.toString() + "\n", baos.toString());
//
//		baos = new ByteArrayOutputStream();
//		jsonOb = NetworkController.encodeLoginRequest("testName", "USER", false);
//		NetworkController.sendData(baos, jsonOb);
//		assertEquals("JsonObject must not change when sent",
//				NetworkController.getNumOfBytes(jsonOb) + jsonOb.toString() + "\n", baos.toString());
//	}
//
//	@Test
//	public void TestReceiveMessage() {
//
//	}
//
//	/**
//	 * Test the method decodeGameInit in NetworkController.
//	 * 
//	 * @throws FileNotFoundException
//	 *             Thrown if the file is not present.
//	 * @author fabian
//	 */
//	@Test
//	public void TestDecodeGameInit() throws FileNotFoundException {
//		System.out.println("Testing decodeGameInit");
//		File jsonInputFile = new File("JSON_Files/gameinit.json");
//		InputStream is = new FileInputStream(jsonInputFile);
//		JsonReader reader = Json.createReader(is);
//		JsonObject jsonOb = reader.readObject();
//		reader.close();
//
//		ArrayList<Player> playList = NetworkController.decodeGameInit(jsonOb);
//
//		int[] userIDs = { 1, 4, 5 };
//		int[] checkP = { 0, 0, 0 };
//		int[] numL = { 2, 2, 2 };
//		int[] xP = { 2, 2, 2 };
//		int[] yP = { 8, 8, 8 };
//		for (int k = 0; k < playList.size(); k++) {
//			Player temP = playList.get(k);
//			assertEquals("User IDs must be equal", userIDs[k], temP.userID);
//			assertEquals("Checkpoints must be equal", checkP[k], temP.getRoboter().lastCheckPoint);
//			assertEquals("Number of lives must be equal", numL[k], temP.getRoboter().lives);
//			assertEquals("X positions must be equal", xP[k], temP.getRoboter().xPos);
//			assertEquals("Y positions must be equal", yP[k], temP.getRoboter().yPos);
//		}
//	}
//
//	/**
//	 * Test the method getNumOfBytes in NetworkController.
//	 * 
//	 * @author fabian
//	 */
//	@Test
//	public void TestGetNumOfBytes() {
//		System.out.println("Testing getNumOfBytes");
//		JsonObject jsonOb = NetworkController.encodeLoginRequest("testName", "SPECTATOR", false);
//		int expected = jsonOb.toString().length();
//		int actual = NetworkController.getNumOfBytes(jsonOb);
//		assertEquals("Number of bytes must be equal to length of the string", expected, actual);
//
//		jsonOb = NetworkController.encodeMessage(3, "this is a test message");
//		expected = jsonOb.toString().length();
//		actual = NetworkController.getNumOfBytes(jsonOb);
//		assertEquals("Number of bytes must be equal to length of the string", expected, actual);
//	}
//
//	/**
//	 * Test the method 'wait' in NetworkController.
//	 * 
//	 * @author fabian
//	 */
//	@Test
//	public void TestWait() {
//		long currTime = System.currentTimeMillis();
//		NetworkController.wait(0);
//		long timeForZeroWait = System.currentTimeMillis() - currTime;
//
//		currTime = System.currentTimeMillis();
//		NetworkController.wait(500);
//		long waitingTime = System.currentTimeMillis() - currTime - timeForZeroWait;
//		assertTrue("The waiting time must be about 0.5 seconds", 495 <= waitingTime && waitingTime <= 505);
//
//		currTime = System.currentTimeMillis();
//		NetworkController.wait(1000);
//		waitingTime = System.currentTimeMillis() - currTime - timeForZeroWait;
//		assertTrue("The waiting time must be about 1 second", 995 <= waitingTime && waitingTime <= 1005);
//	}
//
//}
