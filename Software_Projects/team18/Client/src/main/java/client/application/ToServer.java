package client.application;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.util.ArrayList;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft;
import org.java_websocket.handshake.ServerHandshake;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import client.controller.NetworkController;
import client.model.Player;
import server.communication.data.in.ClientData;

/**
 * @author ga6
 *
 */
public  class ToServer extends WebSocketClient  {
	
	static String message = ""; 
	static boolean InLobby = false;
	



	public ToServer(URI serverUri, Draft draft) {
	
		super(serverUri, draft);

	}

	public ToServer(URI serverURI) {
		super(serverURI);
	}



	@Override
	public void onOpen(ServerHandshake handshakedata) {
		
		}


	@Override
	public void onClose(int code, String reason, boolean remote) {
		System.out.println("closed with exit code " + code + " additional info: " + reason);
	}

	@Override
	public void onMessage(String message) {

	
		}
	
		
		@Override
		public void send(String message) {
			this.message = message;
	     	send(message);
			return;

			
		}
		
		
		public  void run() {
	        while(true){
	              try {
	            	  
	            	
	            		 
	            	
	            		 System.out.println("sending now!!!");
	               
	            	//send(message);
	            	InLobby=false;
	            	  
	            } catch (Exception e) {              
	            	e.printStackTrace();
	            }
	        }
	           
	        }	
		
		
		
		
	

	@Override
	public void onMessage(ByteBuffer message) {
		System.out.println("received ByteBuffer");
	}

	@Override
	public void onError(Exception ex) {
		System.err.println("an error occurred:" + ex);
	}

	public static void giveMessage(String string) {
		 message = string;
		
		
		
	}

	public static boolean isInLobby() {
		return InLobby;
	}

	public static void setInLobby(boolean inLobby) {
		InLobby = inLobby;
	}

	public static void setReady(boolean b) {
	  setInLobby(b);
	  NetworkThread.ready();
	  
		return;
		
		
		
	}


	
}