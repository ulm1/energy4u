package client.application;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.jme3.app.Application;
import com.jme3.app.state.AppStateManager;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.screen.Screen;

/**
 *
 * @author ra7
 */
public class ButtonDialogTest {
    
    public ButtonDialogTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of initialize method, of class ButtonDialog.
     */
    @Test
    public void testInitialize() {
        System.out.println("initialize");
        
        ButtonDialog instance = new ButtonDialog();  
        AppStateManager stateManager = null;
        Application app = null;
      	assertEquals( true, true);
    }

    /**
     * Test of cleanup method, of class ButtonDialog.
     */
    @Test
    public void testCleanup() {
        System.out.println("cleanup");
        ButtonDialog instance = new ButtonDialog();
    	assertEquals( true, true);
    }

    /**
     * Test of setEnabled method, of class ButtonDialog.
     */
    @Test
    public void testSetEnabled() {
        System.out.println("setEnabled");
        boolean enabled = true;
    	assertEquals("The button should be enabled ",true, enabled);
    }

    /**
     * Test of update method, of class ButtonDialog.
     */
    @Test
    public void testUpdate() {
        System.out.println("update");
        float tpf = 2.0F;
        ButtonDialog instance = new ButtonDialog();
    	assertEquals(true,true);
    }

    /**
     * Test of bind method, of class ButtonDialog.
     */
    @Test
    public void testBind() {
        System.out.println("bind");
        Nifty nifty = null;
        Screen screen = null;
        ButtonDialog instance = new ButtonDialog();
        instance.bind(nifty, screen);
    	assertEquals(true,true);
    }

    /**
     * Test of onStartScreen method, of class ButtonDialog.
     */
    @Test
    public void testOnStartScreen() {
        System.out.println("onStartScreen");
        ButtonDialog instance = new ButtonDialog();
        instance.onStartScreen();
    	assertEquals(true,true);
    }

    /**
     * Test of onEndScreen method, of class ButtonDialog.
     */
    @Test
    public void testOnEndScreen() {
        System.out.println("onEndScreen");
        ButtonDialog instance = new ButtonDialog();
        instance.onEndScreen();
    	assertEquals(true,true);
    }

    /**
     * Test of popupErrorShows method, of class ButtonDialog.
     */
    @Test
    public void testPopupErrorShows() {
        System.out.println("popupErrorShows");
        boolean bool = false;
        ButtonDialog.popupErrorShows(bool);
    	assertEquals("The popup should not show",false,bool);
    }

    
}
