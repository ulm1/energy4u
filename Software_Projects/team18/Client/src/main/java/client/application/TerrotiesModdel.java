package client.application;

public class TerrotiesModdel {
	
	int id;
    String terrain;
	boolean castle;
	boolean tavern;
	int income;
	String label;
	String house;
	String allegiance;
	int existingSwords;
	int existingArchers;
	int existingKnights;
		
	public TerrotiesModdel(int id,String terrain, boolean castle, boolean tavern,int income, String label,String house,String allegiance,
	int existingSwords,int existingArchers,int existingKnights){
		this.id = id;
		this.terrain = terrain;
		this.castle = castle;
		this.tavern = tavern;
		this.income = income;
		this.label = label;
		this.house = house;
		this.allegiance = allegiance;
		this.existingSwords = existingSwords;
		this.existingArchers = existingArchers;
		this.existingKnights = existingKnights;
		
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTerrain() {
		return terrain;
	}

	public void setTerrain(String terrain) {
		this.terrain = terrain;
	}

	public boolean isCastle() {
		return castle;
	}

	public void setCastle(boolean castle) {
		this.castle = castle;
	}

	public boolean isTavern() {
		return tavern;
	}

	public void setTavern(boolean tavern) {
		this.tavern = tavern;
	}

	public int getIncome() {
		return income;
	}

	public void setIncome(int income) {
		this.income = income;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getHouse() {
		return house;
	}

	public void setHouse(String house) {
		this.house = house;
	}

	public String getAllegiance() {
		return allegiance;
	}

	public void setAllegiance(String allegiance) {
		this.allegiance = allegiance;
	}

	public int getExistingSwords() {
		return existingSwords;
	}

	public void setExistingSwords(int existingSwords) {
		this.existingSwords = existingSwords;
	}

	public int getExistingArchers() {
		return existingArchers;
	}

	public void setExistingArchers(int existingArchers) {
		this.existingArchers = existingArchers;
	}

	public int getExistingKnights() {
		return existingKnights;
	}

	public void setExistingKnights(int existingKnights) {
		this.existingKnights = existingKnights;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((allegiance == null) ? 0 : allegiance.hashCode());
		result = prime * result + (castle ? 1231 : 1237);
		result = prime * result + existingArchers;
		result = prime * result + existingKnights;
		result = prime * result + existingSwords;
		result = prime * result + ((house == null) ? 0 : house.hashCode());
		result = prime * result + id;
		result = prime * result + income;
		result = prime * result + ((label == null) ? 0 : label.hashCode());
		result = prime * result + (tavern ? 1231 : 1237);
		result = prime * result + ((terrain == null) ? 0 : terrain.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TerrotiesModdel other = (TerrotiesModdel) obj;
		if (allegiance == null) {
			if (other.allegiance != null)
				return false;
		} else if (!allegiance.equals(other.allegiance))
			return false;
		if (castle != other.castle)
			return false;
		if (existingArchers != other.existingArchers)
			return false;
		if (existingKnights != other.existingKnights)
			return false;
		if (existingSwords != other.existingSwords)
			return false;
		if (house == null) {
			if (other.house != null)
				return false;
		} else if (!house.equals(other.house))
			return false;
		if (id != other.id)
			return false;
		if (income != other.income)
			return false;
		if (label == null) {
			if (other.label != null)
				return false;
		} else if (!label.equals(other.label))
			return false;
		if (tavern != other.tavern)
			return false;
		if (terrain == null) {
			if (other.terrain != null)
				return false;
		} else if (!terrain.equals(other.terrain))
			return false;
		return true;
	}
	
	
	
}
