package client.application;

import static org.junit.Assert.*;

import org.junit.Test;

import client.controller.NetworkController;

public class HealthTest {

	@Test
	public void testHealthX() {
		
	    Health h = new Health(2,5);
		int expectedX = 2;	
		int actual = h.getTempX();
		assertEquals("The value should be 2", expectedX, actual);
	}
	
	@Test
	public void testHealthY() {
		
	    Health h = new Health(2,5);
		int expectedY = 5;	
		int actual = h.getTempY();
		assertEquals("The value should be 5", expectedY, actual);
	}
	
	@Test
	public void testsetHealthX() {
		
	    Health h = new Health(2,3);
	    h.setTempX(5);
	    int expectedX = 5;	
		int actual = h.getTempX();
		assertEquals("The value should be 5", expectedX, actual);
	}
	
	@Test
	public void testsetHealthY() {
		
	    Health h = new Health(2,3);
	    h.setTempY(5);
	    int expectedY = 5;	
		int actual = h.getTempY();
		assertEquals("The value should be 5 ", expectedY, actual);
	}
	
}
