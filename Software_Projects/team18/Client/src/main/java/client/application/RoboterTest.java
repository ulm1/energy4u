package client.application;

import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.jme3.app.Application;
import com.jme3.app.state.AppStateManager;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Geometry;

import client.util.Compass;
import client.util.Coordinate;

	/**
	 *
	 * @author ra7
	 */
	public class RoboterTest {
	    
	    public RoboterTest() {
	    }
	    
	    @BeforeClass
	    public static void setUpClass() {
	    }
	    
	    @AfterClass
	    public static void tearDownClass() {
	    }
	    
	    @Before
	    public void setUp() {
	    }
	    
	    @After
	    public void tearDown() {
	    }

	    /**
	     * Test of setLastCheckpoint method, of class Roboter.
	     */
	    @Test
	    public void testSetLastCheckpoint() {
	        System.out.println("setLastCheckpoint");
	        int lastCheckPoint = 0;
	        Roboter instance = null;
	        instance.setLastCheckpoint(lastCheckPoint);
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of setDestructionCount method, of class Roboter.
	     */
	    @Test
	    public void testSetDestructionCount() {
	        System.out.println("setDestructionCount");
	        int destructionCount = 0;
	        Roboter instance = null;
	        instance.setDestructionCount(destructionCount);
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of setXPos method, of class Roboter.
	     */
	    @Test
	    public void testSetXPos() {
	        System.out.println("setXPos");
	        int xPos = 0;
	        Roboter instance = null;
	        instance.setXPos(xPos);
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of setYPos method, of class Roboter.
	     */
	    @Test
	    public void testSetYPos() {
	        System.out.println("setYPos");
	        int yPos = 0;
	        Roboter instance = null;
	        instance.setYPos(yPos);
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of setLives method, of class Roboter.
	     */
	    @Test
	    public void testSetLives() {
	        System.out.println("setLives");
	        int lives = 0;
	        Roboter instance = null;
	        instance.setLives(lives);
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of setRemainingVariables method, of class Roboter.
	     */
	    @Test
	    public void testSetRemainingVariables() {
	        System.out.println("setRemainingVariables");
	        Compass orient = null;
	        Application app = null;
	        gameField gamefield = null;
	        ColorRGBA primaryColor = null;
	        ColorRGBA secondColor = null;
	        Roboter instance = null;
	        instance.setRemainingVariables(orient, app, gamefield, primaryColor, secondColor);
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of initialize method, of class Roboter.
	     */
	    @Test
	    public void testInitialize() {
	        System.out.println("initialize");
	        AppStateManager stateManager = null;
	        Application app = null;
	        Roboter instance = null;
	        instance.initialize(stateManager, app);
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of setEnabled method, of class Roboter.
	     */
	    @Test
	    public void testSetEnabled() {
	        System.out.println("setEnabled");
	        boolean enabled = false;
	        Roboter instance = null;
	        instance.setEnabled(enabled);
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of update method, of class Roboter.
	     */
	    @Test
	    public void testUpdate() {
	        System.out.println("update");
	        float tpf = 0.0F;
	        Roboter instance = null;
	        instance.update(tpf);
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of fireBlueLaser method, of class Roboter.
	     */
	    @Test
	    public void testFireBlueLaser() {
	        System.out.println("fireBlueLaser");
	        Roboter.fireBlueLaser();
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of fireREDLASERDL method, of class Roboter.
	     */
	    @Test
	    public void testFireREDLASERDL() {
	        System.out.println("fireREDLASERDL");
	        Roboter.fireREDLASERDL();
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of fireRedLaser method, of class Roboter.
	     */
	    @Test
	    public void testFireRedLaser() {
	        System.out.println("fireRedLaser");
	        Roboter.fireRedLaser();
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of fireBlueLaserDL method, of class Roboter.
	     */
	    @Test
	    public void testFireBlueLaserDL() {
	        System.out.println("fireBlueLaserDL");
	        Roboter.fireBlueLaserDL();
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of fireBlueLaserD method, of class Roboter.
	     */
	    @Test
	    public void testFireBlueLaserD() {
	        System.out.println("fireBlueLaserD");
	        Roboter.fireBlueLaserD();
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of fireRedLaserD method, of class Roboter.
	     */
	    @Test
	    public void testFireRedLaserD() {
	        System.out.println("fireRedLaserD");
	        Roboter.fireRedLaserD();
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of fireBlueLaserDR method, of class Roboter.
	     */
	    @Test
	    public void testFireBlueLaserDR() {
	        System.out.println("fireBlueLaserDR");
	        Roboter.fireBlueLaserDR();
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of fireRedLaserDR method, of class Roboter.
	     */
	    @Test
	    public void testFireRedLaserDR() {
	        System.out.println("fireRedLaserDR");
	        Roboter.fireRedLaserDR();
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of setPosition method, of class Roboter.
	     */
	    @Test
	    public void testSetPosition() {
	        System.out.println("setPosition");
	        Geometry box = null;
	        Roboter instance = null;
	        instance.setPosition(box);
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of finishMove method, of class Roboter.
	     */
	    @Test
	    public void testFinishMove() {
	        System.out.println("finishMove");
	        Roboter instance = null;
	        instance.finishMove();
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of move method, of class Roboter.
	     */
	    @Test
	    public void testMove_0args() {
	        System.out.println("move");
	        Roboter instance = null;
	        instance.move();
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of move2 method, of class Roboter.
	     */
	    @Test
	    public void testMove2() {
	        System.out.println("move2");
	        Roboter instance = null;
	        instance.move2();
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of move3 method, of class Roboter.
	     */
	    @Test
	    public void testMove3() {
	        System.out.println("move3");
	        Roboter instance = null;
	        instance.move3();
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of move method, of class Roboter.
	     */
	    @Test
	    public void testMove_ErrorType() {
	        System.out.println("move");
	        Compass affectedOrient = null;
	        Roboter instance = null;
	        instance.move(affectedOrient);
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of rotateRight method, of class Roboter.
	     */
	    @Test
	    public void testRotateRight() {
	        System.out.println("rotateRight");
	        Roboter instance = null;
	        instance.rotateRight();
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of rotate180 method, of class Roboter.
	     */
	    @Test
	    public void testRotate180() {
	        System.out.println("rotate180");
	        Roboter instance = null;
	        instance.rotate180();
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of rotateLeft method, of class Roboter.
	     */
	    @Test
	    public void testRotateLeft() {
	        System.out.println("rotateLeft");
	        Roboter instance = null;
	        instance.rotateLeft();
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of fallFromMap method, of class Roboter.
	     */
	    @Test
	    public void testFallFromMap() {
	        System.out.println("fallFromMap");
	        Roboter instance = null;
	        instance.fallFromMap();
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of Gethealth method, of class Roboter.
	     */
	    @Test
	    public void testGethealth() {
	        System.out.println("Gethealth");
	        Roboter instance = null;
	        instance.Gethealth();
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of spawn method, of class Roboter.
	     */
	    @Test
	    public void testSpawn() {
	        System.out.println("spawn");
	        Coordinate spawnPos = null;
	        Compass spawnOrient = null;
	        Roboter instance = null;
	        instance.spawn(spawnPos, spawnOrient);
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of moveBackward method, of class Roboter.
	     */
	    @Test
	    public void testMoveBackward_0args() {
	        System.out.println("moveBackward");
	        Roboter instance = null;
	        instance.moveBackward();
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of moveBackward method, of class Roboter.
	     */
	    @Test
	    public void testMoveBackward_ErrorType() {
	        System.out.println("moveBackward");
	        Compass affectedOrient = null;
	        Roboter instance = null;
	        instance.moveBackward(affectedOrient);
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of collide method, of class Roboter.
	     */
	    @Test
	    public void testCollide() {
	        System.out.println("collide");
	        Roboter instance = null;
	        instance.collide();
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of colliderobob method, of class Roboter.
	     */
	    @Test
	    public void testColliderobob() {
	        System.out.println("colliderobob");
	        Roboter instance = null;
	        instance.colliderobob();
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of collideBackward method, of class Roboter.
	     */
	    @Test
	    public void testCollideBackward() {
	        System.out.println("collideBackward");
	        Roboter instance = null;
	        instance.collideBackward();
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of reachCheckpoint method, of class Roboter.
	     */
	    @Test
	    public void testReachCheckpoint() {
	        System.out.println("reachCheckpoint");
	        Roboter instance = null;
	        instance.reachCheckpoint();
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of REDLASER_HIT_DL method, of class Roboter.
	     */
	    @Test
	    public void testREDLASER_HIT_DL() {
	        System.out.println("REDLASER_HIT_DL");
	        Roboter instance = null;
	        instance.REDLASER_HIT_DL();
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of REDLASER_HIT_DR method, of class Roboter.
	     */
	    @Test
	    public void testREDLASER_HIT_DR() {
	        System.out.println("REDLASER_HIT_DR");
	        Roboter instance = null;
	        instance.REDLASER_HIT_DR();
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of REDLASER_HIT_R method, of class Roboter.
	     */
	    @Test
	    public void testREDLASER_HIT_R() {
	        System.out.println("REDLASER_HIT_R");
	        Roboter instance = null;
	        instance.REDLASER_HIT_R();
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of REDLASER_HIT_D method, of class Roboter.
	     */
	    @Test
	    public void testREDLASER_HIT_D() {
	        System.out.println("REDLASER_HIT_D");
	        Roboter instance = null;
	        instance.REDLASER_HIT_D();
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of BLUELASER_HIT_R method, of class Roboter.
	     */
	    @Test
	    public void testBLUELASER_HIT_R() {
	        System.out.println("BLUELASER_HIT_R");
	        Roboter instance = null;
	        instance.BLUELASER_HIT_R();
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of BLUELASER_HIT_DR method, of class Roboter.
	     */
	    @Test
	    public void testBLUELASER_HIT_DR() {
	        System.out.println("BLUELASER_HIT_DR");
	        Roboter instance = null;
	        instance.BLUELASER_HIT_DR();
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of BLUELASER_HIT_DL method, of class Roboter.
	     */
	    @Test
	    public void testBLUELASER_HIT_DL() {
	        System.out.println("BLUELASER_HIT_DL");
	        Roboter instance = null;
	        instance.BLUELASER_HIT_DL();
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of BLUELASER_HIT_D method, of class Roboter.
	     */
	    @Test
	    public void testBLUELASER_HIT_D() {
	        System.out.println("BLUELASER_HIT_D");
	        Roboter instance = null;
	        instance.BLUELASER_HIT_D();
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of destroy method, of class Roboter.
	     */
	    @Test
	    public void testDestroy() {
	        System.out.println("destroy");
	        Roboter instance = null;
	        instance.destroy();
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of disqualify method, of class Roboter.
	     */
	    @Test
	    public void testDisqualify() {
	        System.out.println("disqualify");
	        Roboter instance = null;
	        instance.disqualify();
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of win method, of class Roboter.
	     */
	    @Test
	    public void testWin() {
	        System.out.println("win");
	        Roboter instance = null;
	        instance.win();
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of bluelaserpos method, of class Roboter.
	     */
	    @Test
	    public void testBluelaserpos() {
	        System.out.println("bluelaserpos");
	        int tempX = 0;
	        int tempY = 0;
	        Roboter.bluelaserpos(tempX, tempY);
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }

	    /**
	     * Test of redlaserpos method, of class Roboter.
	     */
	    @Test
	    public void testRedlaserpos() {
	        System.out.println("redlaserpos");
	        int tempX = 0;
	        int tempY = 0;
	        Roboter.redlaserpos(tempX, tempY);
	        // TODO review the generated test code and remove the default call to fail.
	        fail("The test case is a prototype.");
	    }
	    
	}
