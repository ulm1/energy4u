package client.application;

import client.controller.DragController;
import client.controller.LobbyScreenController;
import client.controller.NetworkController;
import client.controller.PhaseController;
import client.model.Map;
import client.model.Participant;
import client.model.Player;
import client.util.Cards;
import client.util.Compass;
import client.util.Coordinate;
import client.util.Event;
import client.util.EventType;
import server.communication.ClientDB;
import server.communication.ToClient;
import server.communication.WebSocketSRV;
import server.communication.data.in.ClientData;

//import server.ServerController;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;

import org.omg.CORBA.portable.OutputStream;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jme3.network.Server;
import com.jogamp.common.util.InterruptSource.Thread;
import com.sun.org.apache.xerces.internal.util.SynchronizedSymbolTable;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;

import org.java_websocket.WebSocket;
import org.java_websocket.WebSocketImpl;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft;

import org.java_websocket.exceptions.InvalidHandshakeException;
import org.java_websocket.framing.CloseFrame;
import org.java_websocket.framing.Framedata;
import org.java_websocket.framing.Framedata.Opcode;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.handshake.HandshakeImpl1Client;
import org.java_websocket.handshake.Handshakedata;
import org.java_websocket.handshake.ServerHandshake;
import org.java_websocket.server.WebSocketServer;

import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.security.acl.Owner;
import java.util.ArrayList;
import java.util.IllegalFormatException;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

import org.java_websocket.WebSocket;

public class NetworkThread extends WebSocketClient implements Runnable {

	static String[] packedStartScreenData;
	static GameofStones mainApp;
	static boolean laser = false;
	static ArrayList<Player> player_List = new ArrayList<Player>();
	static String[] orientations;
	public static boolean pause = false;
	static WebSocketClient client;
	Gson gson = null;
	static String name = "";
	static boolean readyf = false;
	ServerHandshake h;
	static boolean OnStart = true;
	static boolean InLobby = false;
	

	public boolean isReady() {
		return readyf;
	}

	public static void setReady(boolean ready) {
		readyf = ready;
		ready();
	}

	public NetworkThread(URI serverUri, Draft draft) {
		super(serverUri, draft);

	}

	public NetworkThread(URI serverURI) {
		super(serverURI);
	}

	@Override
	public void onOpen(ServerHandshake handshakedata) {
		
		
		if(OnStart==true){

		boolean isPlayer;
		if (packedStartScreenData[1].equals("watcher")) {
			isPlayer = false;
		} else {
			isPlayer = true;
		}

		send(NetworkController.encodeLoginRequest(packedStartScreenData[0], true, true).toString());

		System.out.println("new connection opened");
		
		OnStart = false;
		InLobby = true;
		
		
		}
		
	}
	
	

		



	@Override
	public void onClose(int code, String reason, boolean remote) {
		System.out.println("closed with exit code " + code + " additional info: " + reason);
	}

	@Override
	public void onMessage(String message) {

		System.out.println("received message: " + message);
		gson = new GsonBuilder().serializeNulls().create();

		ClientData cdat = null;
		cdat = gson.fromJson(message, ClientData.class);

		// handle incoming "login"
		if (cdat.getHeader().getMessageType().equals("loginResponse")) {

			name = cdat.getHeader().getName();

			GameofStones.switchAppState(GameofStones.startScreen, GameofStones.lobbyScreen);
		}

}
		
//		public  void InLobby(boolean message) {
//			
//			if(InLobby==true){
//				
//			ToServer.giveMessage(NetworkController.ready(name, true).toString(),InLobby);
//				InLobby=false;
//			}	
//			
//		}


	@Override
	public void onMessage(ByteBuffer message) {
		System.out.println("received ByteBuffer");
	}

	@Override
	public void onError(Exception ex) {
		System.err.println("an error occurred:" + ex);
	}

	public static void ConnectToServer(String[] strings, GameofStones gameofStones) {

		mainApp = gameofStones;
		packedStartScreenData = strings;

		boolean isPlayer = false;

		if (packedStartScreenData[1].equals("watcher")) {
			isPlayer = false;
		} else {
			isPlayer = true;
		}

		try {
			client = new NetworkThread(new URI("ws://localhost:8025"));
			System.out.println(packedStartScreenData[3]);

			client.connect();

		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void ready() {
		client.send(NetworkController.ready("", true).toString());
		
	}
}