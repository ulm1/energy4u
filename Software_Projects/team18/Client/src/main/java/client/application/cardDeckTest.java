package client.application;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.jme3.app.Application;
import com.jme3.app.state.AppStateManager;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.screen.Screen;

	/**
	 *
	 * @author ra7
	 */
	public class cardDeckTest {
	    
	    public cardDeckTest() {
	    }
	    
	    @BeforeClass
	    public static void setUpClass() {
	    }
	    
	    @AfterClass
	    public static void tearDownClass() {
	    }
	    
	    @Before
	    public void setUp() {
	    }
	    
	    @After
	    public void tearDown() {
	    }

	    /**
	     * Test of initialize method, of class cardDeck.
	     */
	    @Test
	    public void testInitialize() {
	        System.out.println("initialize");
	        AppStateManager stateManager = null;
	        Application app = null;
	        cardDeck instance = new cardDeck();
	        instance.initialize(stateManager, app);
	        assertEquals("The application always initializes .",true,true);
	    }

	    /**
	     * Test of cleanup method, of class cardDeck.
	     */
	    @Test
	    public void testCleanup() {
	        System.out.println("cleanup");
	        cardDeck instance = new cardDeck();
	        instance.cleanup();
	        assertEquals("The application always cleanups .",true,true);
	    }

	    /**
	     * Test of setEnabled method, of class cardDeck.
	     */
	    @Test
	    public void testSetEnabled() {
	        System.out.println("setEnabled");
	        boolean enabled = true;
	        cardDeck instance = new cardDeck();   
	        assertEquals("The application always launches the cardDeck .",true,enabled);
	    }

	    /**
	     * Test of popupErrorShows method, of class cardDeck.
	     */
	    @Test
	    public void testPopupErrorShows() {
	        System.out.println("popupErrorShows");
	        boolean bool = false;
	        cardDeck.popupErrorShows(bool);
	        assertEquals("The popup show successful .",true,true);
	    }

	    /**
	     * Test of bind method, of class cardDeck.
	     */
	    @Test
	    public void testBind() {
	        System.out.println("bind");
	        Nifty arg0 = null;
	        Screen arg1 = null;
	        cardDeck instance = new cardDeck();
	        instance.bind(arg0, arg1);
	        assertEquals("The bind method is always successful .",true,true);
	    }

	    /**
	     * Test of onEndScreen method, of class cardDeck.
	     */
	    @Test
	    public void testOnEndScreen() {
	        System.out.println("onEndScreen");
	        cardDeck instance = new cardDeck();
	        instance.onEndScreen();
	        assertEquals("The this is always the case .",true,true);
	    }

	    /**
	     * Test of onStartScreen method, of class cardDeck.
	     */
	    @Test
	    public void testOnStartScreen() {
	        System.out.println("onStartScreen");
	        cardDeck instance = new cardDeck();
	        instance.onStartScreen();
	        assertEquals("The this is always the case .",true,true);
	    }
	    
	}
