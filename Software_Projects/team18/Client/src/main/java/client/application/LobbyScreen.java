package client.application;


import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.niftygui.NiftyJmeDisplay;
import com.jme3.scene.Node;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;

/**
 */
public class LobbyScreen extends AbstractAppState implements ScreenController {
    /**
     * Initialize the SimpleApplication as well as the instance of the RoboRally.
     * In addition, the Nifty is integrated and thus the ScreenController can be used.
     */

    // initialize nifty library
    private SimpleApplication app;
    private GameofStones mainApp;
    private Nifty nifty;
    /**
     * Include the popupShows and the menuScreen as node.
     */
    Node lobbyScreen = new Node("lobbyScreen");
    NiftyJmeDisplay niftyDisplayLobby;

    /**
     * This method initialises the change of the individual AppState
     * and makes the MenuScreen accessible via the RoboRally.
     * @param stateManager
     * @param app
     */
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        this.app = (SimpleApplication) app;
		mainApp = GameofStones.getMyself();
        // initialize stuff that is independent of whether state is PAUSED or RUNNING
    }

    /**
     * Here, a previous screen is switched off and the register is reset
     */
    @Override
    public void cleanup() {
    	super.cleanup();
    	app.getGuiViewPort().removeProcessor(niftyDisplayLobby);
    	nifty.exit();
        // unregister all my listeners, detach all my nodes.
    }

    /**
     * Here the note is integrated and the key triggers activated.
     * In addition, the Nifty display render method is called with:
     * app.getAssetManager()
     * app.getInputManager()
     * app.getAudioRenderer()
     * app.getGuiViewPort())
     * Additionally, the XML is loaded
     * and added via this to the controller's XML.
     * The XML controls are:
     * startButton
     * helpButton
     * quitButton
     * Start
     * Enter
     * button_back
     * Help
     * button_back
     * Quit
     * client/util/menuScreen.xml
     * menuScreenStart
     * @param enabled
     */
    @Override
    public void setEnabled(boolean enabled) {
        // pause and unpause
        super.setEnabled(enabled);

        if(enabled){

            // custom key bindings : map input to named actions
            this.app.getInputManager().addMapping("Send",
                    new KeyTrigger(KeyInput.KEY_RETURN),
                    new KeyTrigger(KeyInput.KEY_NUMPADENTER));

            // add the names to the action listener
            this.app.getInputManager().addListener(actionListenerLobby, "Send");

            // initialize stuff that is in use while this state is RUNNING
            niftyDisplayLobby = NiftyJmeDisplay.newNiftyJmeDisplay(
                    app.getAssetManager(),
                    app.getInputManager(),
                    app.getAudioRenderer(),
                    app.getGuiViewPort());
            nifty = niftyDisplayLobby.getNifty();

            // initialize the lobbyScreen
            nifty.fromXml("client/util/lobbyScreen.xml", "startLobby", this);

            app.getGuiViewPort().addProcessor(niftyDisplayLobby);
            //app.getInputManager().setCursorVisible(true);

            app.getRootNode().attachChild(this.lobbyScreen);
        } else {

            app.getRootNode().detachChild(this.lobbyScreen);
        }
    }

    /**
     * Nifty GUI Node is integrated with the screen.
     * The first method gives you access to the main Nifty instance and the Screen class, the Java
     * representation of the active screen. Nifty will call this method when it initializes the screen. The
     * method is: bind(Nifty nifty, Screen screen).
     * @param nifty
     * @param screen
     */
    @Override
    public void bind(Nifty nifty, Screen screen) {

    }

    /**
     * There are two other simple methods in the ScreenController interface that are called in the screen
     * life cycle: onStartScreen() and onEndScreen().
     */
    @Override
    public void onStartScreen() {
        //nothing
    }

    /**
     * As with the onStartScreen method:
     * Interface that are called in the screen
     * life cycle: onEndScreen().
     * 
     */
    @Override
    public void onEndScreen() {
        //nothing
    }

    /**
     * This method implements an ActionListener
     * which responds to the seconds the node
     * is connected. This is to be implemented
     * with care, since otherwise it can
     * still be active in a following screen.
     * The ActionListener depends on the
     * MenuScreen ControllerClass
     * together because it can
     * intercept the ontrol elements.
     * The XML controls are:
     * send
     * These are all strings in the XML file menuScreen.xml.
     * @param bool
     */

    private ActionListener actionListenerLobby = new ActionListener() {
        public void onAction(String name, boolean keyPressed, float tpf) {

            if (keyPressed && name.equals("Send")) {
                nifty.getCurrentScreen().findElementById("btnSend").onClickAndReleasePrimaryMouseButton();
            }
        }
    };
}
