package client.application;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class MyImage {
	  public static void main(String args[])throws IOException{
		    BufferedImage image = null;
		    File f = null;

		    //read image file
		    try{
		      f = new File("./Assets/Wald.png");
		      image = ImageIO.read(f);
		    }catch(IOException e){
		      System.out.println("Error: "+e);
		    }

		    //write image
		    try{
		    	f = new File("./Assets/Wald.png");
		      ImageIO.write(image, "png", f);
		    }catch(IOException e){
		      System.out.println("Error: "+e);
		    }
		  }//main()ends here
		}//



