package client.application;

import java.awt.Polygon;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import javax.json.JsonArray;
import javax.json.JsonObject;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioRenderer;
import com.jme3.input.InputManager;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.MouseAxisTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.input.controls.Trigger;
import com.jme3.material.Material;
import com.jme3.material.RenderState.BlendMode;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.niftygui.NiftyJmeDisplay;
import com.jme3.renderer.ViewPort;
import com.jme3.renderer.queue.RenderQueue.Bucket;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Box;
import com.jme3.ui.Picture;

import client.controller.DragController;
import client.controller.StartScreenController;
import client.model.Map;
import client.model.Player;
import client.util.AnimationQueueType;
import client.util.BorderType;
import client.util.Compass;
import client.util.Coordinate;
import client.util.EventType;
import client.util.Field;
import client.util.NumberFactory;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.controls.Console;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;

public class gameField extends AbstractAppState implements ScreenController {

	private SimpleApplication app;
	private Field[][] cartography;
	private final static Trigger TRIGGER_EXIT = new MouseButtonTrigger(MouseInput.BUTTON_RIGHT);
	private final static String MAPPING_EXIT = "Exit";
	public ArrayList<Roboter> roboterList;
	private Nifty nifty;
	static Node vor;
	// private GamePopupController pop;
	StartScreenController pop;

	int X_START = 0;
	int Y_START = -2;
	int Z_START = -2;
	int CHECKPOINTS = 0;
	int PORTAL = 0;
	int HEALTH = 0;
	static int x;
	static int y;

	int FIELD_WIDTH;
	int FIELD_HEIGHT;
static ArrayList<MyPolgon> po;

	String id = "Box";
	String idB = "Border";
	String idO = "Obstacle";
	Geometry geo;
	Roboter robo, roboTwo, roboThree, roboFour;
	Voronoi v;

	HashMap<Integer, Roboter> roboterHash = new HashMap<>();
	public LinkedList<AnimationQueueType> animationQueue = new LinkedList<>();
	ArrayList<Player> playerList;
	ArrayList<ColorRGBA> colorList;

	int idNumber = 1;
	int borderNumber = 0;
	boolean toggle_rotation = true;
	Node field = new Node("field");
	static int oID = 1; // static IDs for referring to Spatials Obstalces
	static int nID = 1; // static IDs for referring to nodes
	int activeAnimations = 0;
	float cursorPosX;
	float cursorPosY;
	float colorV = 0.6f;
	float colorS = 0.9f;
	static ArrayList<Coordinate> portalsp;
	static ArrayList<Coordinate> healthp;
	static ArrayList<Coordinate> redLp;
	static ArrayList<Coordinate> blueLpp;

	private boolean GAME_IS_PAUSED = false;

	// Declaring the necessary Material and Shapes
	Material gameField, checkpointField, border, borderCorner, obstacle, healthField, portalField, lbField, lrField;
	Box box, obstacleBox;

	public gameField(Map map) {
		this.FIELD_WIDTH = map.cartography.length;
		this.FIELD_HEIGHT = map.cartography[0].length;
		this.cartography = new Field[FIELD_WIDTH + 1][FIELD_HEIGHT + 1];
		for (int v = 0; v <= FIELD_WIDTH; v++) {
			for (int h = 0; h <= FIELD_HEIGHT; h++) {
				if (v == FIELD_WIDTH || h == FIELD_HEIGHT) {
					this.cartography[v][h] = new Field(false, null);
				} else {
					this.cartography[v][h] = map.cartography[v][h];
				}
			}
		}
	}

	/**
	 * JME AppState Method for initializing the AppState. In this specific case,
	 * the method initializes all Shape and Material ClassVariables to there
	 * intended visualization. Change here the images if you want to.
	 */
	public void initialize(AppStateManager stateManager, Application app) {
		super.initialize(stateManager, app);
		this.app = (SimpleApplication) app;

		// Initializing the necessary shapes
		box = new Box(0.5f, 0.5f, 0);
		obstacleBox = new Box(0.5f * 0.1664f, 0.5f * 1.1664f, 0);

		// Initializing necessary materials
		gameField = new Material(app.getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md");
		gameField.setTexture("ColorMap", app.getAssetManager().loadTexture("RoboField-02-.png"));
		checkpointField = new Material(app.getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md");
		checkpointField.setTexture("ColorMap", app.getAssetManager().loadTexture("RoboFieldCheckpoint-02-01.png"));
		obstacle = new Material(app.getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md");
		obstacle.setTexture("ColorMap", app.getAssetManager().loadTexture("Obstacle-01.png"));
		border = new Material(app.getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md");
		border.setTexture("ColorMap", app.getAssetManager().loadTexture("Border-01.png"));
		borderCorner = new Material(app.getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md");
		borderCorner.setTexture("ColorMap", app.getAssetManager().loadTexture("Bordercorner-01.png"));
		healthField = new Material(app.getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md");
		healthField.setTexture("ColorMap", app.getAssetManager().loadTexture("health.png"));
		portalField = new Material(app.getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md");
		portalField.setTexture("ColorMap", app.getAssetManager().loadTexture("portal.png"));
		lbField = new Material(app.getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md");
		lbField.setTexture("ColorMap", app.getAssetManager().loadTexture("lasergunblue.PNG"));
		lrField = new Material(app.getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md");
		lrField.setTexture("ColorMap", app.getAssetManager().loadTexture("lasergunreddown.png"));

		// Setting Transparency for the materials
		obstacle.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
		gameField.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
		checkpointField.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
		healthField.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
		portalField.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
		lbField.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
		lrField.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);

		app.getInputManager().addMapping(MAPPING_EXIT, TRIGGER_EXIT);

		app.getInputManager().addMapping("CLICK", new MouseButtonTrigger(MouseInput.BUTTON_LEFT));
		app.getInputManager().addMapping("DRAG", new MouseButtonTrigger(MouseInput.BUTTON_LEFT));
		app.getInputManager().addMapping("ZOOM_IN", new MouseAxisTrigger(MouseInput.AXIS_WHEEL, true));
		app.getInputManager().addMapping("ZOOM_OUT", new MouseAxisTrigger(MouseInput.AXIS_WHEEL, false));

		app.getInputManager().addListener(actionListener, "CLICK");
	//	app.getInputManager().addListener(analogListener, "DRAG");
		app.getInputManager().addListener(actionListener, "ZOOM_IN");
		app.getInputManager().addListener(actionListener, "ZOOM_OUT");
		app.getInputManager().addListener(actionListener1, MAPPING_EXIT);

	}

	@Override
	public void cleanup() {
		super.cleanup();
		// unregister all my listeners, detach all my nodes, etc....
	}

	/**
	 * JME Method for enabling the AppState
	 */
	@Override
	public void setEnabled(boolean enabled) {
		// Pause and unpause
		super.setEnabled(enabled);
		if (enabled) {
			// init stuff that is in use while this state is RUNNING
			int closeWidth = GameofStones.SCREEN_WIDTH / 5;
			int closeHeight = closeWidth / 2;

			renderGameField();
			v = new Voronoi();
			app.getStateManager().attach(v);
			v.initialize(app.getStateManager(), app);
			app.getStateManager().getState(Voronoi.class).setEnabled(true);
			attachGuiElements();
			float leftCornerY = -2f;
			float leftCornerX = -3;
//            app.getGuiNode().getChild("voronoi").move(leftCornerX, leftCornerY, -1);
		
			field =vor;
			
			field.setLocalTranslation(leftCornerX, leftCornerY, -1);
			
			app.getRootNode().attachChild(field);

			app.getCamera().setLocation(new Vector3f(0, 0, 15));
			createColorSet(playerList.size());
			int colorIndex = 0;
			this.roboterList = new ArrayList<Roboter>();
			for (Player player : playerList) {
				Roboter tempRobo = player.roboter;
				tempRobo.username = player.userName;
				tempRobo.lastCheckPoint = 0;
				tempRobo.destructionCount = 0;
				tempRobo.isDestroyed = false;
				roboterList.add(tempRobo);
				tempRobo.setRemainingVariables(Compass.NORTH, app, this, colorList.get(colorIndex),
						colorList.get(playerList.size() - 1 - colorIndex));
				colorIndex++;
				addRoboter(tempRobo);
			}

			// test where button is created

			ButtonDialog buttonDialog = new ButtonDialog();
			app.getStateManager().attach(buttonDialog);
			buttonDialog.initialize(app.getStateManager(), app);
			app.getStateManager().getState(ButtonDialog.class).setEnabled(true);
			attachGuiElements();
			app.getGuiNode().getChild("buttonDialog").move(0, GameofStones.SCREEN_HEIGHT / 50 + closeHeight, 0);

			// test where button is created

			Phase c = new Phase();
			app.getStateManager().attach(c);
			c.initialize(app.getStateManager(), app);
			app.getStateManager().getState(Phase.class).setEnabled(true);
			attachGuiElements();
			app.getGuiNode().getChild("phase").move(0, GameofStones.SCREEN_HEIGHT / 50 + closeHeight, 0);

			// where RankingField is created basically attached
			RankingField rankingField = new RankingField();
			app.getStateManager().attach(rankingField);
			rankingField.initialize(app.getStateManager(), app);
			app.getStateManager().getState(RankingField.class).setEnabled(true);
			attachGuiElements();
			app.getGuiNode().getChild("rankingField").move(0,
					GameofStones.SCREEN_HEIGHT * 9 / 10
							- ((86 * (roboterList.size() + 1) + 179 + 182) * (float) (GameofStones.SCREEN_WIDTH) / 2250f),
					0);

		} else {
			// take away everything not needed while this state is PAUSED
			app.getRootNode().detachChild(field);

		}
	}

	// Note that update is only called while the state is both attached and
	// enabled.
	@Override
	public void update(float tpf) {
		// do the following while game is RUNNING
		if (!animationQueue.isEmpty()) {

			if (activeAnimations == 0) {
				AnimationQueueType element = animationQueue.poll();
				element.robo.myTurn = true;
				activeAnimations++;
				while (element.affectedNext) {
					element = animationQueue.poll();
					element.robo.myTurn = true;
					activeAnimations++;
				}
			}
		}

	}

	private void attachGuiElements() {
		// Picture closeButton = new Picture("closeButton");
		// closeButton.setImage(app.getAssetManager(), "InfoKeule_close-01.png",
		// true);
		// closeButton.move(0, RoboRally.SCREEN_HEIGHT / 10, 0);
		// int closeWidth = RoboRally.SCREEN_WIDTH / 5;
		// int closeHeight = closeWidth / 2;
		// closeButton.setWidth(closeWidth);
		// closeButton.setHeight(closeHeight);
		// //System.out.println("my pos" +closeButton.move(0,
		// RoboRally.SCREEN_HEIGHT / 10, 0));
		// System.out.println(closeButton.getLocalTranslation());
		// app.getGuiNode().attachChild(closeButton);
		//
		// Picture infoButton = new Picture("infoButton");
		// infoButton.setImage(app.getAssetManager(), "InfoKeule_info-01.png",
		// true);
		// //140 ... 60
		// infoButton.move(0, RoboRally.SCREEN_HEIGHT / 10 + closeHeight, 0);
		// int infoWidth = closeWidth;
		// int infoHeight = closeHeight;
		// infoButton.setWidth(infoWidth);
		// infoButton.setHeight(infoHeight);
		// app.getGuiNode().attachChild(infoButton);
		//
		// Picture straight = new Picture("staight");
		// straight.setImage(app.getAssetManager(), "Cards/straight.png", true);
		// straight.move(0, 0 , 0);
		// int straightWidth = RoboRally.SCREEN_WIDTH / 5;
		// int straightHeight = straightWidth / 2;
		// straight.setWidth(straightWidth);
		// straight.setHeight(straightHeight);
		// app.getGuiNode().attachChild(straight);

	}

	private void createColorSet(int size) {
		colorList = new ArrayList<ColorRGBA>();
		for (int i = 0; i < size; i++) {
			int h;
			if (i == 0) {
				h = 360;
			} else {
				h = 360 / (size * i);
			}

			int hi = (int) (Math.floor(h / 60));
			int f = (int) ((h / 60) - hi);
			float p = colorV * (1 - colorS);
			float q = colorV * (1 - colorS * f);
			float t = colorV * (1 - colorS * (1 - f));

			ColorRGBA color;

			switch (hi) {
			case 6:
			case 0:
				color = new ColorRGBA(colorV, t, p, 1);
				break;
			case 1:
				color = new ColorRGBA(q, colorV, p, 1);
				break;
			case 2:
				color = new ColorRGBA(p, colorV, t, 1);
				break;
			case 3:
				color = new ColorRGBA(p, q, colorV, 1);
				break;
			case 4:
				color = new ColorRGBA(t, p, colorV, 1);
				break;
			case 5:
				color = new ColorRGBA(colorV, p, q, 1);
				break;
			default:
				color = new ColorRGBA(1, 1, 0, 1);
			}

			colorList.add(color);
		}

	}

	public void addRoboter(int x, int y, Compass direction, int id) {
		Roboter robo = new Roboter(x, y, direction, id, app, this);
		roboterHash.put(id, robo);
		robo.initialize(app.getStateManager(), app);
		app.getStateManager().attach(robo);
		robo.roboGeo.scale(0);
		robo.setEnabled(true);
	}

	public void addRoboter(Roboter robo) {
		roboterHash.put(robo.RoboterID, robo);
		robo.initialize(app.getStateManager(), app);
		app.getStateManager().attach(robo);
		robo.roboGeo.scale(0);
		robo.setEnabled(true);
	}

	/**
	 * AnimateRoboter method for the gamefield. The Method stages an
	 * AnimationQueueType Object at the animationQueue of the gamefield. For
	 * simultaneous animations like animations which affects other Roboters use
	 * the method with other signature.
	 * 
	 * @param event
	 *            EventType which will be staged
	 * @param id
	 *            ID of the Roboter which the event will be staged for
	 */
	public void animateRoboter(EventType event, int id) {
		Roboter affectedRobo = roboterHash.get(id);

		if (affectedRobo != null) {
			switch (event) {
			case MOVE:
				affectedRobo.move();
				break;
			case MOVE2:
				affectedRobo.move2();
				break;
			case REDLASER_HIT_R:
				affectedRobo.BLUELASER_HIT_R();
				break;
			case BLUELASER_HIT_R:
				affectedRobo.BLUELASER_HIT_R();
				break;
			case REDLASER_HIT_DR:
				affectedRobo.REDLASER_HIT_DR();
				break;
			case REDLASER_HIT_DL:
				affectedRobo.REDLASER_HIT_DL();
				break;
			case BLUELASER_HIT_DL:
				affectedRobo.BLUELASER_HIT_DL();
				break;
			case REDLASER_HIT_D:
				affectedRobo.REDLASER_HIT_D();
				break;
			case BLUELASER_HIT_D:
				affectedRobo.BLUELASER_HIT_D();
				break;
			case BLUELASER_HIT_DR:
				affectedRobo.REDLASER_HIT_DR();
				break;
			case MOVE3:
				affectedRobo.move3();
				break;
			case MOVE_BACKWARD:
				affectedRobo.moveBackward();
				break;
			case COLLIDE_WITH_WALL:
				affectedRobo.collide();
				break;
			case HEALTH:
				affectedRobo.Gethealth();
				break;
			case COLLIDE_WITH_Robo:
				affectedRobo.colliderobob();
				break;
			case COLLIDE_WITH_WALL_BACKWARD:
				affectedRobo.collideBackward();
				break;
			case CHECKPOINT_REACHED:
				affectedRobo.reachCheckpoint();
				break;
			case FALL_FROM_MAP:
				affectedRobo.fallFromMap();
				break;
			case ROTATE_LEFT:
				affectedRobo.rotateLeft();
				break;
			case ROTATE_RIGHT:
				affectedRobo.rotateRight();
				break;
			case ROTATE180:
				affectedRobo.rotate180();
				break;
			case DESTROY:
				affectedRobo.destroy();
				break;
			case DISQUALIFIED:
				affectedRobo.disqualify();
				DragController.disqualifyWindow();
				break;
			case WIN:
				affectedRobo.win();
				DragController.wonWindow();
				break;
			default:
				break;
			}

			animationQueue.add(new AnimationQueueType(affectedRobo, false));
		} else {
			System.out.println("ERROR at animateRoboter - Roboter ID not found!");
		}

	}

	/**
	 * AnimateRoboter method for the gamefield. The method stages a set of
	 * AnimationQueueType Objects at the local animationQueue, so that this set
	 * of animations will later be executed simultaneously for all the Roboter
	 * IDs. CAUTION: Not all Events can be staged as an animation with affect on
	 * others
	 * 
	 * @param event
	 *            EventType which will be staged
	 * @param id
	 *            ID of the Roboter who triggered the event
	 * @param affectedUid
	 *            all IDs of the Roboter which are affected of the first IDs
	 *            Event. The Events staged for this IDs depends on the Event
	 *            from the first ID and the declared actions of the
	 *            Schnittstellenkomitee
	 */
	public void animateRoboter(EventType event, int id, int... affectedUid) {
		Roboter affectedRobo = roboterHash.get(id);
		if (affectedRobo != null) {
			switch (event) {
			case MOVE:
				affectedRobo.move();
				break;
			case MOVE_BACKWARD:
				affectedRobo.moveBackward();
				break;
			case COLLIDE_WITH_WALL:
				affectedRobo.collide();
				break;
			case COLLIDE_WITH_WALL_BACKWARD:
				affectedRobo.collideBackward();
				break;
			case FALL_FROM_MAP:
				affectedRobo.fallFromMap();
				break;
			case DISQUALIFIED:
				affectedRobo.disqualify();
				DragController.disqualifyWindow();
			default:
				System.out.println(
						"ERROR at animateRoboter - EventType does not support Affected UIds due to protocoll regulations");
				break;
			}
			animationQueue.add(new AnimationQueueType(affectedRobo, true));
		} else {
			System.out.println("ERROR at animateRoboter - Roboter ID not found!");
		}

		for (int i = 0; i < affectedUid.length - 1; i++) {
			Roboter affectedRobos = roboterHash.get(affectedUid[i]);
			if (affectedRobos != null) {
				switch (event) {
				case MOVE:
					affectedRobos.move(affectedRobo.queueOrientation);
					break;
				case MOVE_BACKWARD:
					affectedRobos.moveBackward();
					break;
				case COLLIDE_WITH_WALL:
					break;
				case COLLIDE_WITH_WALL_BACKWARD:
					break;
				case FALL_FROM_MAP:
					affectedRobo.fallFromMap();
					break;
				default:
					System.out.println(
							"ERROR at animateRoboter - EventType does not support Affected UIds due to protocoll regulations");
					break;
				}
				animationQueue.add(new AnimationQueueType(affectedRobos, true));
			} else {
				System.out.println("ERROR at animateRoboter - Roboter ID not found!");
			}

		}

		Roboter lastAffected = roboterHash.get(affectedUid[affectedUid.length - 1]);
		if (lastAffected != null) {
			switch (event) {
			case MOVE:
				lastAffected.move(affectedRobo.queueOrientation);
				break;
			case MOVE_BACKWARD:
				lastAffected.moveBackward();
				break;
			case COLLIDE_WITH_WALL:
				lastAffected.collide();
				break;
			case COLLIDE_WITH_WALL_BACKWARD:
				lastAffected.collideBackward();
				break;
			default:
				System.out.println(
						"ERROR at animateRoboter - EventType does not support Affected UIds due to protocoll regulations");
				break;
			}
			animationQueue.add(new AnimationQueueType(lastAffected, false));
		} else {
			System.out.println("ERROR at animateRoboter - Roboter ID not found!");
		}

	}

	public void spawnRoboter(int id, Coordinate position, Compass orientation, int affectedID) {
		Roboter robo = roboterHash.get(id);
		
		Coordinate p = new Coordinate(x,y);
		robo.spawn(position, orientation);
		if (affectedID == -1) {

			animationQueue.add(new AnimationQueueType(robo, false));
		} else {
			Roboter affectedRobo = roboterHash.get(affectedID);
			affectedRobo.destroy();
			animationQueue.add(new AnimationQueueType(robo, false));
			animationQueue.add(new AnimationQueueType(affectedRobo, false));

		}
	}

	public void nextMove() {
		activeAnimations--;
	}

	/**
	 * Creates one or two obstacles depending on the BorderType.
	 * 
	 * @param border
	 *            type of the obstacle. Check out BorderType-Enum for more
	 *            informations.
	 * @param geo
	 *            JME Geometry object. The obstacles translation will be
	 *            adjusted to the position of the Geometry
	 */
	public void setObstacle(BorderType border, Geometry geo) {
		ArrayList<Geometry> obsts = new ArrayList<>();
		Geometry obs1;
		Geometry obs2;

		switch (border) {
		case SOUTH:
			obs1 = new Geometry(idO + oID, obstacleBox);
			oID++;
			obs1.setMaterial(obstacle);
			obs1.rotate(0, 0, 90 * FastMath.DEG_TO_RAD);
			obs1.move(0, -0.5f, 0);
			obsts.add(obs1);
			break;
		case EAST:
			obs1 = new Geometry(idO + oID, obstacleBox);
			oID++;
			obs1.setMaterial(obstacle);
			obs1.move(0.5f, 0, 0);
			obsts.add(obs1);
			break;
		case SOUTH_EAST:
			obs1 = new Geometry(idO + oID, obstacleBox);
			oID++;
			obs1.setMaterial(obstacle);
			obs2 = new Geometry(idO + oID, obstacleBox);
			oID++;
			obs2.setMaterial(obstacle);
			obs2.rotate(0, 0, 90 * FastMath.DEG_TO_RAD);
			obs1.move(0.5f, 0, 0);
			obs2.move(0, -0.5f, 0);
			obsts.add(obs1);
			obsts.add(obs2);
			break;
		}

		Node obstacleNode = new Node("Node" + nID);
		nID++;
		Vector3f pos = geo.getLocalTranslation();
		obstacleNode.setLocalTranslation(pos.getX(), pos.getY(), pos.getZ());
		for (Geometry obs : obsts) {
			obs.setQueueBucket(Bucket.Translucent);
			obstacleNode.attachChild(obs);
		}
		field.attachChild(obstacleNode);

	}

//	public void animateRedLaser() {
//		System.out.println("red laser is "+ server.application.EchoThread.getRedLaser().get(0).orientation);
//		switch (server.application.EchoThread.getRedLaser().get(0).orientation) {
//		case "DOWN":
//			Roboter.fireRedLaserD();
//			break;
//		case "RIGHT":
//			Roboter.fireRedLaser();
//			break;
////		case "DiagDownL":
////			Roboter.fireREDLASERDL();
////			break;
////		case "DiagDownR":
////			Roboter.fireRedLaserDR();
////			break;
//		default:
//			System.out.println("ERROR at finding red laser type");
//			break;
//		}
//	}
//
//	public void animateBlueLaser() {
//		System.out.println("blue laser "+ server.application.EchoThread.getBlueLaser().get(0).orientation);
//		switch (server.application.EchoThread.getBlueLaser().get(0).orientation) {
//		
//		case "DOWN":
//			Roboter.fireBlueLaserD();
//			break;
//		case "RIGHT":
//			Roboter.fireBlueLaser();
//			break;
////		case "DiagDownL":
////			Roboter.fireBlueLaserDL();
////			break;
////		case "DiagDownR":
////			Roboter.fireBlueLaserDR();
////			break;
//		default:
//			System.out.println("ERROR at finding blue laser type");
//			System.out.println(server.application.EchoThread.getBlueLaser().get(0).orientation);
//			break;
//
//		}
//	}
//
//	/**
//	 * Method which renders the actual GameField to a given Map-Object after the
//	 * GameField-Instance was created with a Map.
//	 */
	public void renderGameField() {

		

		for (int v = 0; v <= FIELD_WIDTH; v++) {
			for (int h = 0; h <= FIELD_HEIGHT; h++) {

				geo = new Geometry(id + idNumber, box);

				// geo.setMaterial(this.gameField);
				if (v == FIELD_WIDTH | h == FIELD_HEIGHT) { // For-Loop reached
															// section not
															// contained in the
															// given
															// Map-Cartography.
					cartography[v][h].boxGeo = geo;
					// Check if Field is corner or border.
				

				
					geo.setQueueBucket(Bucket.Transparent);
					field.attachChild(geo);

				} else { // For-Loop coordinates are in the Maps-Cartography
		
					

					
					// 3

					
					// 4
					geo.setQueueBucket(Bucket.Transparent);

					// TODO: Handle rotations of field an borders

					// 5*******
					if (toggle_rotation && cartography[v][h].isValid) {
						geo.rotate(0, 0, 90 * FastMath.DEG_TO_RAD);
					}

					field.attachChild(geo);
				
				}
			

				toggle_rotation = !toggle_rotation;/// **
				idNumber++;// ***
			
			}
			if (FIELD_HEIGHT % 2 == 1) {
				toggle_rotation = !toggle_rotation;
			}
		

			}
	}

	/**
	 * This Method checks for a tupel of coordinates v and h if the coordinates
	 * represent the corner of the actual instance of GameField.
	 * 
	 * @param v
	 *            vertical coordinate
	 * @param h
	 *            horizontal coordinate
	 * @return -1 if the coordinates are not a corner. Else return 0 =
	 *         TopLeftCorner, 1 = TopRightCorner, 2 = BottomRightCorner, 3 =
	 *         BottomLeftCorner
	 */
	private int isCorner(int v, int h) {

		if (v == 0 && h == 0) {
			return 0;
		} else if (v == 0 && h == FIELD_HEIGHT) {
			return 3;
		} else if (v == FIELD_WIDTH && h == 0) {
			return 1;
		} else if ((v == FIELD_WIDTH && h == FIELD_HEIGHT)) {
			return 2;
		} else {
			return -1;
		}
	}

	/**
	 * Set a checkpoint to a given boxID If the boxID is unvalid, the method
	 * catch the Exception
	 * 
	 * @param boxID
	 *            JME Spatial ID of the Field Element which should be switched
	 *            to a checkpoint
	 */
	public void setCheckpoint(int boxID) {
		boolean rotate = false;
		Spatial child = null;

		try {
			child = field.getChild(id + boxID);
			if (FIELD_WIDTH % 2 != 0) {
				if (boxID % 2 == 0) {
					rotate = true;
				}
			} else {
				if (Math.ceil(boxID / FIELD_WIDTH) % 2 == 1) {
					if (boxID % 2 == 1) {
						rotate = true;
					}
				} else {
					if (boxID % 2 == 0) {
						rotate = true;
					}
				}
				;
			}
			child.setMaterial(checkpointField);
			if (rotate) {
				child.rotate(0, 0, 90 * FastMath.DEG_TO_RAD);
			}

		} catch (NullPointerException e) {
			e.printStackTrace();
		}

	}

	public Geometry getBox(int x, int y) {
		return cartography[x][y].boxGeo;
	}

	private ActionListener actionListener1 = new ActionListener() {
		public void onAction(String name, boolean isPressed, float tpf) {
			if (name.equals(MAPPING_EXIT)) {

				// geom.rotate(0, intensity * 10f, 0);
				System.out.println("You triggered: " + name);
			}
		}
	};

//	private AnalogListener analogListener = new AnalogListener() {
//		public void onAnalog(String name, float value, float tpf) {
//			if (name.equals("DRAG")) {
//				Vector2f screenPos = app.getInputManager().getCursorPosition();
//				float moveX = (cursorPosX - screenPos.x) / -100;
//				float moveY = (cursorPosY - screenPos.y) / -100;
//				Vector3f camPos = app.getCamera().getLocation();
//				app.getCamera().setLocation(new Vector3f(camPos.x - moveX, camPos.y - moveY, camPos.z));
//				cursorPosX = screenPos.x;
//				cursorPosY = screenPos.y;
//			}
//
//		}
//	};

	private ActionListener actionListener = new ActionListener() {
		@SuppressWarnings("null")
		public void onAction(String name, boolean keyPressed, float tpf) {
			if (name.equals("CLICK") && keyPressed) {
				
//				
//				for(int i=0; i<po.size();i++){
//					int id = po.get(i).id;
//				RankingField.setId(id);
//				}
//				//animateRoboter(EventType.MOVE,NetworkThread.myId());
				
				
				
				// CollisionResults results = new CollisionResults();
				// Vector2f click2d = app.getInputManager().getCursorPosition();
				// Vector3f click3d = app.getCamera().getWorldCoordinates(new
			    //  Vector2f(click2d.getX(), click2d.getY()), 0f);
				// Vector3f dir = app.getCamera().getWorldCoordinates(new
				// Vector2f(click2d.getX(), click2d.getY()),
				// 1f).subtractLocal(click3d);
				// Ray ray = new Ray(click3d,dir);
				// app.getRootNode().collideWith(ray, results);
				// CollisionResult target =
				// results.getClosestCollision().equals(obj);

				 cursorPosX = app.getInputManager().getCursorPosition().x;
				 cursorPosY = app.getInputManager().getCursorPosition().y;
				// System.out.println("my x pos" + cursorPosX);
				// System.out.println("my y pos" + cursorPosY);
				// if(((cursorPosX >= 89)&&(cursorPosX <= 149)) || ((cursorPosY
				// >= 104)&& (cursorPosY <= 127))){
				//
				// pop = new StartScreenController();
				// // stateManager.attach(pop);
				//
				// NiftyJmeDisplay niftyDisplay =
				// NiftyJmeDisplay.newNiftyJmeDisplay(app.getAssetManager(),
				// app.getInputManager(),
				// app.getAudioRenderer(), app.getGuiViewPort());
				//
				// nifty = niftyDisplay.getNifty();
				// nifty.fromXml("client/util/startScreen.xml", "startScreen");
				// // pop.showGameExitPopup();
				//
				//
				//
				// pop.showErrorPopup("Test", "there is a error");

				// }

			} else if (name.equals("ZOOM_IN")) {
				Vector3f camPos = app.getCamera().getLocation();
				app.getCamera().setLocation(new Vector3f(camPos.x, camPos.y, camPos.z - 1));
			} else if (name.equals("ZOOM_OUT")) {
				System.out.println("zooming");
				Vector3f camPos = app.getCamera().getLocation();
				app.getCamera().setLocation(new Vector3f(camPos.x, camPos.y, camPos.z + 1));
			}
		}
	};

	public String findNiftyControl(String console, Class<Console> consoleClass) {
		return console;
	}

	@Override
	public void bind(Nifty arg0, Screen arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onEndScreen() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStartScreen() {
		// TODO Auto-generated method stub

	}

	public static void getHealthPos(JsonObject jsonOb) {

		JsonArray healths = jsonOb.getJsonObject("body").getJsonObject("map").getJsonArray("healthFields");
		healthp = new ArrayList<Coordinate>();

		for (int j = 0; j < healths.size(); j++) {
			int tempX = healths.getJsonObject(j).getInt("fieldX");
			int tempY = healths.getJsonObject(j).getInt("fieldY");

			healthp.add(new Coordinate(tempX, tempY));
		}
	}

	public static void getRedLaserPos(JsonObject jsonOb) {

		JsonArray laser = jsonOb.getJsonObject("body").getJsonObject("map").getJsonArray("laser");
		redLp = new ArrayList<Coordinate>();
		blueLpp = new ArrayList<Coordinate>();
		for (int j = 0; j < laser.size(); j++) {

			if (laser.getJsonObject(j).getString("color").equals("RED")) {
				int tempX = laser.getJsonObject(j).getInt("fieldX");
				int tempY = laser.getJsonObject(j).getInt("fieldY");

				redLp.add(new Coordinate(tempX, tempY));
			} else {
				int tempX = laser.getJsonObject(j).getInt("fieldX");
				int tempY = laser.getJsonObject(j).getInt("fieldY");
				blueLpp.add(new Coordinate(tempX, tempY));
			}
		}
	}

	// public static void getBlueLaserPos(JsonObject jsonOb) {
	//
	// JsonArray blaser =
	// jsonOb.getJsonObject("body").getJsonObject("map").getJsonArray("laserBlue");
	// blueLpp = new ArrayList<Coordinate>();
	//
	// for (int j = 0; j < blaser.size(); j++) {
	// if(blaser.getJsonObject(j).getString("color").equals("BLUE")){
	//
	// int tempX = blaser.getJsonObject(j).getInt("fieldX");
	// int tempY = blaser.getJsonObject(j).getInt("fieldY");
	//
	// blueLpp.add(new Coordinate(tempX,tempY));
	// }
	// }
	// }
	public static void getPortalPos(JsonObject jsonOb) {
		JsonArray portalsr = jsonOb.getJsonObject("body").getJsonObject("map").getJsonArray("portal");
		portalsp = new ArrayList<Coordinate>();

		for (int j = 0; j < portalsr.size(); j++) {
			int tempX = portalsr.getJsonObject(j).getInt("startX");
			int tempY = portalsr.getJsonObject(j).getInt("startY");
			int etempX = portalsr.getJsonObject(j).getInt("endX");
			int etempY = portalsr.getJsonObject(j).getInt("endY");
			portalsp.add(new Coordinate(tempX, tempY));
			portalsp.add(new Coordinate(etempX, etempY));

		}
	}

	public static void setPosition(int i, int j) {
		x = i;
		y =j;
		
	}

	public static void Node(Node voronoi) {
		vor = voronoi;
		
	}

	public static void setPOL(ArrayList<MyPolgon> p2) {
		po = p2;
		
	}
}
