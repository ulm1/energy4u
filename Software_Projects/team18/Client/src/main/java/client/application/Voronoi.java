package client.application;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.TexturePaint;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.json.JsonArray;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Box;
import com.jme3.scene.shape.Quad;
import com.jme3.texture.Image;
import com.jme3.texture.Texture;
import com.jme3.texture.Texture2D;
import com.jme3.texture.plugins.AWTLoader;

public class Voronoi extends AbstractAppState {
	static Node voronoi = new Node("voronoi");
	Node voronoi2 = new Node("voronoi2");
	private GameofStones mainApp;
	private SimpleApplication app;
	static JsonArray terr = null;
	
	public boolean edges[][];
	public int territoryMap[][];
	
	public LinkedList<Vector2f> borders[];

	static double p = 3;
	static BufferedImage I;
	public static int px[], py[], color[], cells = 6, size = 1000;
	ArrayList<Polygon> pol = new ArrayList<Polygon>() ;
	ArrayList<MyPolgon> p2 = new ArrayList<MyPolgon>();
	
	Polygon newPolygon; 

	ImageIcon icon;
	Canvas canvas;
	Material material;
	static String Terraintype = "";
	static Node robo;

	@Override
	public void initialize(AppStateManager stateManager, Application app) {
		super.initialize(stateManager, app);
		this.app = (SimpleApplication) app;
		mainApp = GameofStones.getMyself();

	}

	Geometry geometry;
	Geometry geometry2;

	@Override
	public void setEnabled(boolean enabled) {
		// pause and unpause
		super.setEnabled(enabled);

		if (enabled) {
			
			
		

			app.getRootNode().attachChild(voronoi);
			app.getGuiNode().attachChild(this.voronoi2);

			int n = 0;
			Random rand = new Random();
			I = new BufferedImage(size, size, BufferedImage.TYPE_INT_RGB);
			px = new int[cells];
			py = new int[cells];
			color = new int[cells];
			
			edges = new boolean[cells][cells];
			territoryMap = new int[1000][1000];
			borders = new LinkedList[cells];
			for (int i = 0; i < cells; i++) {
				px[i] = rand.nextInt(size);
				py[i] = rand.nextInt(size);
				color[i] = rand.nextInt(16777215);
				
				edges[i][0] = i == 0 ? false : true;
				edges[0][i] = i == 0 ? false : true;
				
				Mesh mesh = new Quad(10, 10);
				geometry = new Geometry("quad", mesh);
				
				borders[i] = new LinkedList<Vector2f>();

			}
			
			int previousN = -1;
			for (int x = 0; x < size; x++) {
				previousN = - 1;
				for (int y = 0; y < size; y++) {
					n = 0;
					for (int i = 1; i < cells; i++) {
						if (distance(px[i], x, py[i], y) < distance(px[n], x, py[n], y)) {
							n = i;

						}
					}
					if(x == 0 )
					{
						borders[n].addLast(new Vector2f(x, y));
						I.setRGB(x, y, 0);
					}
					else if ( y == size - 1)
					{
						
						borders[n].addLast(new Vector2f(x, y));
						I.setRGB(x, y, 0);
					} 
					else if(y == 0)
					{
						borders[n].addFirst(new Vector2f(x, y));
						I.setRGB(x, y, 0);
					}
					else if (x == size - 1)
					{
						borders[n].addFirst(new Vector2f(x, y));
						I.setRGB(x, y, 0);
					}
					else if(previousN == n)
					{
							
							
//					
								I.setRGB(x, y, 0xFFFFFF);
					} 
					else //case Border was crossed horizontally
					{
						I.setRGB(x, y, 0);
						borders[n].addFirst(new Vector2f(x, y));
						borders[previousN].addLast(new Vector2f(x, y - 1));
					}
					
					previousN = n;
					
					territoryMap[x][y] = n;

				}
			}

			Graphics2D g = I.createGraphics();

			for (int i = 0; i < cells; i++) {
				g.setColor(Color.RED);
				g.fill(new Ellipse2D.Double(px[i] - 5, py[i] - 5, 10, 10));
				
				for(int j = i + 1; j < cells; j++)
				{
					if ( i == j) continue;
					
					if(edges[i][j])
					{
						System.out.println("Roadcheck");
						Graphics2D road = I.createGraphics();
						road.setColor(Color.BLACK);
						road.draw(new Line2D.Double(px[i], py[i], px[j], py[j]));
						
						
						edges[i][j] = false;
						edges[j][i] = false;
						
						I.flush();
						
					}
				}
				
//				AWTLoader loader = new AWTLoader();
//				Image load = loader.load(I, true);
//				Texture t = new Texture2D();
//
//				t.setImage(load);
//				material = new Material(app.getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md");
//				material.setTexture("ColorMap", t);
//				// material.setTextureParam("ColorMap", varType, new
//				// Texture2D(awtLoader.load(bufferedImage, true)));
//				geometry.setMaterial(material);
//				voronoi.attachChild(geometry);
				
				
				
				I.flush();

			}
			JFrame newFrame = new JFrame("polygonstuff");
			JPanel panel = new JPanel();
			  File f = null;
			  File d = null;
			  BufferedImage img = null;
			
			  
			  
			newFrame.add(panel);
			BufferedImage bfImg = new BufferedImage(size, size, BufferedImage.TYPE_INT_RGB);
			
			String terrain2 = "";
			
			for(int i = 0; i < cells; i++)
			{
				int sizeArray = borders[i].size();
				int[] xCords = new int[sizeArray];
				int[] yCords = new int[sizeArray];
				
				for(int j = 0; j < sizeArray; j++)
				{
					Vector2f cord = (Vector2f) borders[i].get(j);
					xCords[j] = (int) cord.x;
					yCords[j] = (int) cord.y;
				}
				
				
				 newPolygon = new Polygon(xCords, yCords, sizeArray);
				 
				 
			
				 try{
				      f = new File("./Assets/Wald.jpg");
				      img = ImageIO.read(f);
				    }catch(IOException e){
				      System.out.println("Error: "+e);
				    }
				 
				  BufferedImage imgw = null; 
				 try{
				      File w  = new File("./Assets/W�ste.png");
				       imgw = ImageIO.read(w);
				    }catch(IOException e){
				      System.out.println("Error: "+e);
				    }
				 
				 BufferedImage imgm = null; 
				 try{
				      File m  = new File("./Assets/Gebirge.jpg");
				       imgm = ImageIO.read(m);
				    }catch(IOException e){
				      System.out.println("Error: "+e);
				    }
				 
				 BufferedImage imgg = null; 
				 try{
				      File m  = new File("./Assets/Grasland.jpg");
				       imgg = ImageIO.read(m);
				    }catch(IOException e){
				      System.out.println("Error: "+e);
				    }
				 BufferedImage imgh = null; 
				 try{
				      File m  = new File("./Assets/Sumpf.jpg");
				       imgh = ImageIO.read(m);
				    }catch(IOException e){
				      System.out.println("Error: "+e);
				    }
				 
				 
				 
				 
				MyPolgon p = new MyPolgon(newPolygon,pol.size()); 
				pol.add(newPolygon);
				p2.add(p);
				
				//Roboter.setPosition(px[0] - 5, py[0] - 5);
				
				
				

			
				
				
				
				System.out.println("my size is " + pol.size());
				
			
				Graphics2D pic = bfImg.createGraphics();
				
				
				System.out.println(i);
				 terrain2 = terr.getJsonObject(i).getString("terrain");
				
			
				switch(terrain2){
				
				 case  "forest":
				 TexturePaint pd = new TexturePaint(img, new Rectangle(pol.get(i).getBounds().x, pol.get(i).getBounds().y, pol.get(i).getBounds().width, pol.get(i).getBounds().height));
					pic.setPaint(pd);
                 break;
                 case "desert":
                    pd = new TexturePaint(imgw, new Rectangle(pol.get(i).getBounds().x, pol.get(i).getBounds().y, pol.get(i).getBounds().width, pol.get(i).getBounds().height));
					pic.setPaint(pd);
                 break;
                 case "mountains":
                 pd = new TexturePaint(imgm, new Rectangle(pol.get(i).getBounds().x, pol.get(i).getBounds().y, pol.get(i).getBounds().width, pol.get(i).getBounds().height));
					pic.setPaint(pd);
                  break;
                 case "plain":
                     pd = new TexturePaint(imgg, new Rectangle(pol.get(i).getBounds().x, pol.get(i).getBounds().y, pol.get(i).getBounds().width, pol.get(i).getBounds().height));
    					pic.setPaint(pd);
                      break;
                 
                 default: pic.setColor(Color.green);
                 break;
				
				
				}
//				if(terrain2.equals("forest")){
//				TexturePaint pd = new TexturePaint(img, new Rectangle(pol.get(i).getBounds().x, pol.get(i).getBounds().y, pol.get(i).getBounds().width, pol.get(i).getBounds().height));
//				pic.setPaint(pd);
//				}
//				if(terrain2.equals("desert")){
//					TexturePaint pd = new TexturePaint(imgw, new Rectangle(pol.get(i).getBounds().x, pol.get(i).getBounds().y, pol.get(i).getBounds().width, pol.get(i).getBounds().height));
//					pic.setPaint(pd);
//					}
//				
//				
//				else if  {
//					
//					pic.setColor(Color.RED);
//				}
				
			
				
		       //forest 
		//		pic.drawImage(img, px[forestid] - 5, py[forestid] - 5, 80, 80, null);
				
				//pic.drawPolygon(newPolygon);
				pic.fillPolygon(newPolygon);
				gameField.setPOL(p2);

				panel.paintComponents(pic);
				
				
			}
			
			AWTLoader loader = new AWTLoader();
			Image load = loader.load(bfImg, true);
			Texture t = new Texture2D();

			t.setImage(load);
			material = new Material(app.getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md");
			material.setTexture("ColorMap", t);
			// material.setTextureParam("ColorMap", varType, new
			// Texture2D(awtLoader.load(bufferedImage, true)));
			geometry.setMaterial(material);
			
			
			Box box2 = new Box(1,1,1);
	        Geometry red = new Geometry("Box", box2);
	       //red.setLocalTranslation(new Vector3f(1,3,1));
	        Material mat2 = new Material(app.getAssetManager(),
	                "Common/MatDefs/Misc/Unshaded.j3md");
	        mat2.setColor("Color", ColorRGBA.Red);
	        red.setMaterial(mat2);
	    	//(voronoi.attachChild(red);
			voronoi.attachChild(geometry);
			gameField.Node(voronoi);
		
			
			
			
			panel.setSize(800, 600);
			newFrame.setResizable(true);
			
			panel.repaint();
			
			panel.setVisible(true);
			panel.setBackground(Color.RED);
			newFrame.pack();
			newFrame.setVisible(true);
			newFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			try {
				ImageIO.write(I, "png", new File("voronoi.png"));

			} catch (IOException e) {
			}

		}
	}

	static double distance(int x1, int x2, int y1, int y2) {
		double d;
	//	d = Math.abs(x1 -x2) + Math.abs(y1 - y2);// Manhatten Distance
	//d = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)); // Euclidian
	d = Math.pow(Math.pow(Math.abs(x1 - x2), p) + Math.pow(Math.abs(y1 - y2), p), (1 / p)); // Minkovski
		return d;
	}
	

	public static void setTerritories(JsonArray terrotories) {
		 terr = terrotories;
		
	}

	public static void setChild(Node roboGeo) {
	//	voronoi.attachChild(roboGeo);
		
	}
	
	





}