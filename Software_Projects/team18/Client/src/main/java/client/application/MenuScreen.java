package client.application;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.niftygui.NiftyJmeDisplay;
import com.jme3.scene.Node;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;

/**
 * @author aron, soeren
 */
public class MenuScreen extends AbstractAppState implements ScreenController {
    /**
     * Initialize the SimpleApplication as well as the instance of the RoboRally.
     * In addition, the Nifty is integrated and thus the ScreenController can be used.
     */
    private SimpleApplication app;
    private GameofStones mainApp;
    private Nifty nifty;
    NiftyJmeDisplay niftyDisplayMenu;

    /**
     * Include the popupShows and the menuScreen as node.
     */
    Node menuScreen = new Node("menuScreen");
    public static AppState myself;
    private static boolean popupShows;

    /**
     * This method initialises the change of the individual AppState
     * and makes the MenuScreen accessible via the RoboRally.
     * @param stateManager
     * @param app
     * @author aron, soeren
     */
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        this.app = (SimpleApplication) app;
        mainApp = GameofStones.getMyself();
        MenuScreen.myself = this;
        // initialize stuff that is independent of whether state is PAUSED or RUNNING
    }

    /**
     * Here, a previous screen is switched off and the register is reset.
     * @author aron, soeren
     */
    @Override
    public void cleanup() {
        super.cleanup();
        // unregister all my listeners, detach all my nodes.
        // unregister all my listeners, detach all my nodes, etc....
        System.out.println("Cleanup at menuScreen Is Called" );
        nifty.exit();
        app.getGuiViewPort().removeProcessor(niftyDisplayMenu);
        System.out.println("Cleanup at menuScreen is finished");
    }

    /**
     * Here the note is integrated and the key triggers activated.
     * In addition, the Nifty display render method is called with:
     * app.getAssetManager()
     * app.getInputManager()
     * app.getAudioRenderer()
     * app.getGuiViewPort())
     * Additionally, the XML is loaded
     * and added via this to the controller's XML.
     * The XML controls are:
     * startButton
     * helpButton
     * quitButton
     * Start
     * Enter
     * button_back
     * Help
     * button_back
     * Quit
     * client/util/menuScreen.xml
     * menuScreenStart
     * @param enabled
     * @author aron, soeren
     */
    @Override
    public void setEnabled(boolean enabled) {
        // pause and unpause
        super.setEnabled(enabled);

        if(enabled){
            popupShows = false;

            // custom key bindings : map input to named actions
            this.app.getInputManager().addMapping("Start",
                    new KeyTrigger(KeyInput.KEY_S));
            this.app.getInputManager().addMapping("Enter",
                    new KeyTrigger(KeyInput.KEY_RETURN),
                    new KeyTrigger(KeyInput.KEY_NUMPADENTER));
            this.app.getInputManager().addMapping("Help",
                    new KeyTrigger(KeyInput.KEY_H));
            this.app.getInputManager().addMapping("Quit",
                    new KeyTrigger(KeyInput.KEY_ESCAPE),
                    new KeyTrigger(KeyInput.KEY_Q));

            // add the names to the action listener
            this.app.getInputManager().addListener(actionListener, "Start");
            this.app.getInputManager().addListener(actionListener, "Enter");
            this.app.getInputManager().addListener(actionListener, "Help");
            this.app.getInputManager().addListener(actionListener, "Quit");

            // initialize stuff that is in use while this state is RUNNING
            niftyDisplayMenu = NiftyJmeDisplay.newNiftyJmeDisplay(
                    app.getAssetManager(),
                    app.getInputManager(),
                    app.getAudioRenderer(),
                    app.getGuiViewPort());
            nifty = niftyDisplayMenu.getNifty();
            nifty.fromXml("client/util/menuScreen.xml", "menuScreenStart", this);

            app.getGuiViewPort().addProcessor(niftyDisplayMenu);
            app.getInputManager().setCursorVisible(true);

            app.getRootNode().attachChild(this.menuScreen);
        } else {
            popupShows = false;
            app.getInputManager().clearMappings();
            app.getInputManager().removeListener(actionListener);
            // take away everything not needed while this state is PAUSED
            app.getRootNode().detachChild(this.menuScreen);
            //nifty.exit();
            app.getRootNode().updateModelBound();
            app.getRootNode().updateGeometricState();
            app.getRootNode().updateLogicalState(0);
           // app.getGuiViewPort().removeProcessor(niftyDisplayMenu);
        }
    }

    /**
     * Nifty GUI Node is integrated with the screen.
     * The first method gives you access to the main Nifty instance and the Screen class, the Java
     * representation of the active screen. Nifty will call this method when it initializes the screen. The
     * method is: bind(Nifty nifty, Screen screen).
     * @param nifty
     * @param screen
     * @author aron, soeren
     */
    @Override
    public void bind(Nifty nifty, Screen screen) {

    }

    /**
     * There are two other simple methods in the ScreenController interface that are called in the screen
     * life cycle: onStartScreen() and onEndScreen().
     * @author aron, soeren
     */
    @Override
    public void onStartScreen() {

    }

    /**
     * As with the onStartScreen method:
     * Interface that are called in the screen
     * life cycle: onEndScreen().
     * @author aron, soeren
     */
    @Override
    public void onEndScreen() {

    }

    /**
     * This method implements an ActionListener
     * which responds to the seconds the node
     * is connected. This is to be implemented
     * with care, since otherwise it can
     * still be active in a following screen.
     * The ActionListener depends on the
     * MenuScreen ControllerClass
     * together because it can
     * intercept the ontrol elements.
     * The XML controls are:
     * startButton
     * helpButton
     * quitButton
     * Start
     * Enter
     * button_back
     * Help
     * button_back
     * Quit
     * These are all strings in the XML file menuScreen.xml.
     * @param bool
     * @author aron, soeren
     */
    public static void popupHelpShows(boolean bool) {
        popupShows = bool;
    }

    private ActionListener actionListener = new ActionListener() {
        public void onAction(String name, boolean keyPressed, float tpf) {
            // get currently focused element as well as all buttons
            Element focEl = nifty.getCurrentScreen().getFocusHandler().getKeyboardFocusElement();
            Element start = nifty.getCurrentScreen().findElementById("startButton");
            Element help = nifty.getCurrentScreen().findElementById("helpButton");
            Element quit = nifty.getCurrentScreen().findElementById("quitButton");

            if (keyPressed && !popupShows && name.equals("Start")) {
                start.onClickAndReleasePrimaryMouseButton();
            }

            if (keyPressed && name.equals("Enter")) {
                if (popupShows) {
                    nifty.getCurrentScreen().getTopMostPopup().findElementById("button_back").onClickAndReleasePrimaryMouseButton();
                } else if (focEl.equals(help)) {
                    // when focus on button 'help', click that button
                    help.onClickAndReleasePrimaryMouseButton();
                } else if (focEl.equals(quit)) {
                    // when focus on button 'quit', click that button
                    quit.onClickAndReleasePrimaryMouseButton();
                } else {
                    // when focus not on any other button, click 'start'
                    start.onClickAndReleasePrimaryMouseButton();
                }
            }

            if (keyPressed && name.equals("Help")) {
                if (popupShows) {
                    nifty.getCurrentScreen().getTopMostPopup().findElementById("button_back").onClickAndReleasePrimaryMouseButton();
                } else {
                    help.onClickAndReleasePrimaryMouseButton();
                }
            }

            if (keyPressed && !popupShows && name.equals("Quit")) {
                quit.onClickAndReleasePrimaryMouseButton();
            }
        }
    };
}
