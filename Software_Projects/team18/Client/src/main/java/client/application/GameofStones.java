package client.application;

import java.awt.Color;
import java.util.ArrayList;

import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AppState;
import com.jme3.math.ColorRGBA;
import com.jme3.system.AppSettings;
import com.jme3.ui.Picture;

import client.model.Map;
import client.model.Player;


public class GameofStones extends SimpleApplication {

	int FIELD_WIDTH = 10;
	int FIELD_HEIGHT = 5;
	float X_START = -2;
	float Y_START = 0;
	float Z_START = -2;
	public final static String UNSHADED = "Common/MatDefs/Misc/Unshaded.j3md";
	public static int SCREEN_HEIGHT;
	public static int SCREEN_WIDTH;

	public static GameofStones myself;
	public static MenuScreen menuScreen;
	public static StartScreen startScreen;
	static LobbyScreen lobbyScreen;
	public static RankingField rankingField;
	gameField gameField;
	Map gameMap;
	ArrayList<Player> playerList;

	private static boolean changeState = false;
	private static AppState disableState;
	private static AppState enableState;

	Picture logo;

	public static void main(String[] args) {
		GameofStones robo = new GameofStones();
		myself = robo;
		AppSettings settings = new AppSettings(true);
		settings.setRenderer(AppSettings.LWJGL_OPENGL2);
		settings.setWidth(1000);
		settings.setHeight(1000);
		settings.useInput();
		
		robo.setPauseOnLostFocus(false);		
		robo.setSettings(settings);
		robo.setDisplayFps(false);
		robo.setShowSettings(false);
		robo.setDisplayStatView(false);
	
		robo.start();

	}

	public static GameofStones getMyself() {
		return myself;
	}

	@Override
	public void simpleInitApp() {

		flyCam.setEnabled(false);

		this.getViewPort().setBackgroundColor(ColorRGBA.Blue);
	
		menuScreen = new MenuScreen();
		startScreen = new StartScreen();
		lobbyScreen = new LobbyScreen();
		stateManager.attach(menuScreen);
		menuScreen.initialize(this.stateManager, this);
		menuScreen.setEnabled(true);
		SCREEN_HEIGHT = this.getCamera().getHeight();
		SCREEN_WIDTH = this.getCamera().getWidth();

	}

	@Override
	public void simpleUpdate(float tpf) {
		if (changeState) {

			disableState.setEnabled(false);
			disableState.cleanup();
			stateManager.detach(disableState);
			stateManager.attach(enableState);
			enableState.initialize(stateManager, this);
			enableState.setEnabled(true);
			changeState = false;
			rootNode.updateGeometricState();
			guiNode.updateGeometricState();

		}
	}

	public void startConnection(String[] packedStartScreenData) {
	

			NetworkThread.ConnectToServer(packedStartScreenData, this);


	}

	public static void switchAppState(AppState dis, AppState en) {
		disableState = dis;
		enableState = en;
		changeState = true;
	}
}
