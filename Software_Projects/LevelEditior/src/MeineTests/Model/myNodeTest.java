package MeineTests.Model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import Model.myNode;

public class myNodeTest {
	
	private myNode nodeUnderTest;
	private ArrayList<myNode> linkedNodesOfNode;
	
	private final int idOfNode = 42;
	private final int xOfNode  = 456;
	private final int yOfNode  = 655756;
	
	@Before
	public void init()
	{
		nodeUnderTest = new myNode(idOfNode, xOfNode, yOfNode);
		linkedNodesOfNode = new ArrayList<myNode>();
	}
	
	@Test
	public void nameTest()
	{
		assertEquals(idOfNode, nodeUnderTest.getName());
		
		final int alternativeId = 65465;
		
		nodeUnderTest.setName(alternativeId);
		assertEquals(alternativeId, nodeUnderTest.getName());
		
		nodeUnderTest.setName(idOfNode);
	}
	
	@Test
	public void xTest()
	{
		assertEquals(xOfNode, nodeUnderTest.getX());
		
		final int alternativeX = 84654;
		
		nodeUnderTest.setX(alternativeX);
		assertEquals(alternativeX, nodeUnderTest.getX());
		
		nodeUnderTest.setX(xOfNode);
	}
	
	@Test
	public void yTest()
	{
		assertEquals(yOfNode, nodeUnderTest.getY());
		
		final int alternativeY = 95262;
		
		nodeUnderTest.setY(alternativeY);
		assertEquals(alternativeY, nodeUnderTest.getY());
		
		nodeUnderTest.setY(yOfNode);
	}
	
	@Test
	public void linkedNodesTest()
	{
		assertNotNull(nodeUnderTest.getLinkedNode());
		
		nodeUnderTest.setLinkedNode(linkedNodesOfNode);
		
		assertEquals( linkedNodesOfNode, nodeUnderTest.getLinkedNode());
	}
	
}
