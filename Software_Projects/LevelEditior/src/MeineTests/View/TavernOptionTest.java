package MeineTests.View;

import static org.junit.Assert.assertEquals;

import javax.json.Json;
import javax.json.JsonObjectBuilder;

import org.junit.Before;
import org.junit.Test;

import View.TavernOption;

public class TavernOptionTest {
	
	private TavernOption optionUnderTest;
	
	@Before
	public void init()
	{
		optionUnderTest= new TavernOption();
	}
	
	@Test
	public void testSetVisible()
	{
		
		boolean setVisibleValue = true;
		boolean expected_VisibleValue = true;
		
		optionUnderTest.setVisible(setVisibleValue);
		
		assertEquals(expected_VisibleValue, optionUnderTest.isVisible());
		
		setVisibleValue = false;
		expected_VisibleValue = false;
		
		optionUnderTest.setVisible(setVisibleValue);
		
		assertEquals(expected_VisibleValue, optionUnderTest.isVisible());
	}
	
	@Test
	public void testGetAsJson()
	{
		final String expected_JsonContent_true = "{\"tavern\":true}";
		final String expected_JsonContent_false = "{\"tavern\":false}";
		JsonObjectBuilder objBuilder = Json.createBuilderFactory(null).createObjectBuilder();
		
		optionUnderTest.setTavern(true);
		
		optionUnderTest.getAsJson(objBuilder);
		assertEquals( expected_JsonContent_true, objBuilder.build().toString());
		
		optionUnderTest.setTavern(false);
		
		optionUnderTest.getAsJson(objBuilder);
		assertEquals( expected_JsonContent_false, objBuilder.build().toString());
	}
	
}
