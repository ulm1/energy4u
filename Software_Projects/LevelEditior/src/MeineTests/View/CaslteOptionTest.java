package MeineTests.View;

import static org.junit.Assert.assertEquals;

import javax.json.Json;
import javax.json.JsonObjectBuilder;

import org.junit.Before;
import org.junit.Test;

import View.CastleOption;

public class CaslteOptionTest {
	
	private CastleOption castleOptionUnderTest;
	
	@Before
	public void init()
	{
		castleOptionUnderTest = new CastleOption();
	}
	
	@Test
	public void setVisibleTest()
	{
		boolean setVisibility = true;
		boolean expectedVisibility = true;
		
		castleOptionUnderTest.setVisible(setVisibility);
		
		assertEquals(expectedVisibility, castleOptionUnderTest.isVisible());
		
		setVisibility = false;
		expectedVisibility = false;
		
		castleOptionUnderTest.setVisible(setVisibility);
		
		assertEquals(expectedVisibility, castleOptionUnderTest.isVisible());
	}
	
	@Test
	public void getAsJsonTest()
	{
		final String expectedJson_true = "{\"castle\":true}";
		final String expectedJson_false = "{\"castle\":false}";
		
		JsonObjectBuilder objBuilder = Json.createBuilderFactory(null).createObjectBuilder();
		
		castleOptionUnderTest.setCastle(true);
		castleOptionUnderTest.getAsJson(objBuilder);
		
		assertEquals(expectedJson_true, objBuilder.build().toString());
		
		castleOptionUnderTest.setCastle(false);
		castleOptionUnderTest.getAsJson(objBuilder);
		
		assertEquals(expectedJson_false, objBuilder.build().toString());
	}
}
