package Model;

import java.util.ArrayList;

public class myNode {
	int name;
	int x; 
	int y;
	private ArrayList<myNode> linkedNode = new ArrayList<>(); //TO DO 
	
	public myNode(int name, int x, int y){
		this.name = name;
		this.x = x;
		this.y = y;
		
	}

	public int getName() {
		return name;
	}

	public void setName(int name) {
		this.name = name;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public ArrayList<myNode> getLinkedNode() {
		return linkedNode;
	}

	public void setLinkedNode(ArrayList<myNode> linkedNode) {
		this.linkedNode = linkedNode;
	}
}
