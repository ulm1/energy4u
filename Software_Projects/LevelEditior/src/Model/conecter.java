package Model;

import myController.myController;

// erstellen der Verbindungen zweier Punkte
public class conecter {
	public myNode startPoint;
	public myNode endPoint;
	public myController c;
	
	
	public conecter (myNode startPoint, myNode endPoint, myController c){//to do wegen fehlerausgabe window
		this.startPoint = startPoint;
		this.endPoint = endPoint;
		this.c = c;
		}


	public myNode getStartPoint() {
		return startPoint;
	}


	public void setStartPoint(myNode startPoint) {
		this.startPoint = startPoint;
	}


	public myNode getEndPoint() {
		return endPoint;
	}


	public void setEndPoint(myNode endPoint) {
		this.endPoint = endPoint;
	}


	public myController getC() {
		return c;
	}


	public void setC(myController c) {
		this.c = c;
	}
	
	
	
}