package View;

import java.util.ArrayList;
import java.util.Arrays;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import javax.json.JsonValue.ValueType;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class ArmyOptionControl extends NodeOption {
	private static final long serialVersionUID = 1L;
	
	private JTabbedPane tabBar;
	protected ArrayList<HousesEnum> ListOfHouses;
	private ArrayList<ArmyOption> listOfArmyOptions;
	private JPanel lastEmptyPanel;
	private boolean duringUpdate = false;
	
	public ArmyOptionControl()
	{
		super();
		
		ListOfHouses = new ArrayList<HousesEnum>(Arrays.asList(HousesEnum.values()));
		
		tabBar = new JTabbedPane();
		add(tabBar);
		
		listOfArmyOptions = new ArrayList<ArmyOption>();
		
		ArmyOption newOptionTab = new ArmyOption(this);
		
		ListOfHouses.remove(newOptionTab.getSelectedItem());
		
		listOfArmyOptions.add(newOptionTab);
		
		lastEmptyPanel = new JPanel();
		
		tabBar.add("" + newOptionTab.getSelectedItem().name(), newOptionTab);
		tabBar.addTab("New Army", lastEmptyPanel);
		tabBar.addChangeListener(new ChangeListener() {
			@Override
		    public void stateChanged(ChangeEvent e) {
		        if(tabBar.getSelectedIndex() == (tabBar.getTabCount() - 1) && !ListOfHouses.isEmpty())
		        {
		        	triggerNewTab();
		        }
		        // Prints the string 3 times if there are 3 tabs etc
		    }
		});
		
		tabBar.setVisible(true);
		setVisible(true);
	}

	private void triggerNewTab()
	{
		if(duringUpdate == true)
		{
			return;
		}
		ArmyOption newArmyOption = new ArmyOption(this);
		lastEmptyPanel.add(newArmyOption);
		listOfArmyOptions.add(newArmyOption);
    	tabBar.setTitleAt(tabBar.getSelectedIndex(), "" + newArmyOption.getSelectedItem().name());
    	ListOfHouses.remove(newArmyOption.getSelectedItem());
    	if(!ListOfHouses.isEmpty())
    	{
    		lastEmptyPanel = new JPanel();
        	tabBar.addTab("New Army", lastEmptyPanel);
		}
		if (listOfArmyOptions.size() >= 2) 
		{
			for (ArmyOption option : listOfArmyOptions) 
			{
				option.updateDeletButton(true);

			}
		}
	}

	public void triggerChange() 
	{
		if(duringUpdate == true)
		{
			return;
		}
		duringUpdate = true;
		
		ArrayList<HousesEnum> newListOfHouses = new ArrayList<HousesEnum>(Arrays.asList(HousesEnum.values()));
		
		for(int i = 0; i < listOfArmyOptions.size(); i++)
		{
			ArmyOption optionUpdate = listOfArmyOptions.get(i);
			HousesEnum selectedHouse = optionUpdate.getSelectedItem();
			tabBar.setTitleAt(i, "" + selectedHouse.name());
			newListOfHouses.remove(selectedHouse);
		}
		
		ListOfHouses = newListOfHouses;
		
		listOfArmyOptions.forEach(optionUpdate -> optionUpdate.restartComboBox());
		
		duringUpdate = false;
	}
	
	public void triggerDeletion()
	{
		int selectedTab = tabBar.getSelectedIndex();
		ArmyOption selectedOption = listOfArmyOptions.get(selectedTab);
		
		ListOfHouses.add(selectedOption.getSelectedItem());
		
		if(selectedTab == tabBar.getTabCount() - 2 )
		{
			tabBar.setSelectedIndex(selectedTab - 1);	
		}
		tabBar.remove(selectedTab);
		listOfArmyOptions.remove(selectedTab);
		
		if(listOfArmyOptions.size() < 2)
    	{
			listOfArmyOptions.forEach(optionUpdate -> optionUpdate.updateDeletButton(false));
    	}
		
		triggerChange();
	}

	public void loadJson(JsonArray jsUnitsBuffer) 
	{	
		duringUpdate = true;
		if(jsUnitsBuffer == null)
		{
			System.err.println("There is a problem with the units settings");
			return;
		}
		tabBar.removeAll();
		
		ListOfHouses = new ArrayList<HousesEnum>(Arrays.asList(HousesEnum.values()));
		listOfArmyOptions.clear();
		
		for( JsonValue value : jsUnitsBuffer )
		{
			if(value.getValueType() != ValueType.OBJECT)
			{
				System.err.println("There is a problem with the units settings");
				break;
			}
			else
			{
				ArmyOption newOptionTab = new ArmyOption(this, (JsonObject) value);
				listOfArmyOptions.add(newOptionTab);
				HousesEnum selectedHouse = newOptionTab.getSelectedItem();
				if(!ListOfHouses.remove(selectedHouse))
				{
					System.err.println("Sanity check failed. JsonFile is corrupted.");
					return;
				}
				tabBar.add(  selectedHouse.name() , newOptionTab);
			}
		}
		
		if(listOfArmyOptions.size() < 2)
    	{
			listOfArmyOptions.forEach(optionUpdate -> optionUpdate.updateDeletButton(false));
    	}
		else
		{
			listOfArmyOptions.forEach(optionUpdate -> optionUpdate.updateDeletButton(true));
		}
		if(!ListOfHouses.isEmpty())
    	{
    		lastEmptyPanel = new JPanel();
        	tabBar.addTab("New Army", lastEmptyPanel);
		}
		duringUpdate = false;
	}
	
	@Override
	public void getAsJson(JsonObjectBuilder jsonObBuilder) {
		JsonArrayBuilder armyBuilder = Json.createBuilderFactory(null).createArrayBuilder();
		
		listOfArmyOptions.forEach( armyOption -> armyBuilder.add(armyOption.getAsJson()));
		
		jsonObBuilder.add("units", armyBuilder.build());
	}
	
}
