package View;

import java.awt.GridLayout;

import javax.json.JsonObjectBuilder;
import javax.swing.JComboBox;
import javax.swing.JLabel;

/**
 * @author berfin
 *	This class is designed to store and manage the information about one house in one territory.
 */
public class HouseOption extends NodeOption {

	private static final long serialVersionUID = 8220652590669423365L;
	private final JLabel s_label = new JLabel("House: "); 
	private JComboBox<HousesEnum> s_DropDown;
	
	public HouseOption() {
		GridLayout cv_masterLayout = new GridLayout(1,2);
		
		s_DropDown = new JComboBox<HousesEnum>(HousesEnum.values());
		
		setLayout(cv_masterLayout);
		
		add(s_label);
		add(s_DropDown);
	}

	/**
	 * sets the houses visible
	 */
	@Override
	public void setVisible(boolean value)
	{	
		super.setVisible(value);
		
		s_DropDown.setVisible(value);
		s_label.setVisible(value);
	}
	
	/**
	 * gives the selected house as Json
	 */
	@Override
	public void getAsJson(JsonObjectBuilder jsonObBuilder) {
		jsonObBuilder.add("house", ((HousesEnum) s_DropDown.getSelectedItem()).name());
		
	}
	
	/**
	 * shows in drop down menue which house is selected
	 * @param houseBuffer: sets the selected house
	 */
	public void setHouse(String houseBuffer) {
		HousesEnum hsEnum = HousesEnum.valueOf(houseBuffer);
		s_DropDown.setSelectedItem(hsEnum);
	}

}
