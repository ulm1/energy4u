package View;

import javax.json.JsonObjectBuilder;
import javax.swing.JPanel;

/**
 * @author berfin
 */
public abstract class NodeOption extends JPanel{
	private static final long serialVersionUID = 1L;

	public abstract void getAsJson(JsonObjectBuilder jsonObBuilder);
	
}
