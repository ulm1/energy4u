package View;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonBuilderFactory;
import javax.json.JsonObjectBuilder;

/**
 * model class for levelconfiguration
 * @author berfin
 *
 */
public class LevelConfiguration {
	public static final String PROTOCOL_VERSION = "1522460114";
	
	private int minplayer;
	private int maxPlayer;
	private boolean[][] edges;
	private int minUnits;
	private int maxUnits;
	private int maxUnitsNonCastle;
	private int swords;
	private int archers;
	private int knigths;
	private int swordsCost;
	private int archersCost;
	private int knigthCost;
	
	private double resourceDecay;
	private double starvingChance;
	private double eggChance;
	private double dragonKillChance;
	
	public LevelConfiguration()
	{
	}

	public double getDragonKillChance() {
		return dragonKillChance;
	}

	public void setDragonKillChance(double dragonKillChance) {
		this.dragonKillChance = dragonKillChance;
	}

	public double getEggChance() {
		return eggChance;
	}

	public void setEggChance(double eggChance) {
		this.eggChance = eggChance;
	}

	public double getStarvingChance() {
		return starvingChance;
	}

	public void setStarvingChance(double starvingChance) {
		this.starvingChance = starvingChance;
	}

	public double getResourceDecay() {
		return resourceDecay;
	}

	public void setResourceDecay(double resourceDecay) {
		this.resourceDecay = resourceDecay;
	}

	public int getKnigthCost() {
		return knigthCost;
	}

	public void setKnigthCost(int knigthCost) {
		this.knigthCost = knigthCost;
	}

	public int getArchersCost() {
		return archersCost;
	}

	public void setArchersCost(int archersCost) {
		this.archersCost = archersCost;
	}

	public int getSwordsCost() {
		return swordsCost;
	}

	public void setSwordsCost(int swordsCost) {
		this.swordsCost = swordsCost;
	}

	public int getKnigths() {
		return knigths;
	}

	public void setKnigths(int knigths) {
		this.knigths = knigths;
	}

	public int getArchers() {
		return archers;
	}

	public void setArchers(int archers) {
		this.archers = archers;
	}

	public int getSwords() {
		return swords;
	}

	public void setSwords(int swords) {
		this.swords = swords;
	}

	public int getMaxUnitsNonCastle() {
		return maxUnitsNonCastle;
	}

	public void setMaxUnitsNonCastle(int maxUnitsNonCastle) {
		this.maxUnitsNonCastle = maxUnitsNonCastle;
	}

	public int getMaxUnits() {
		return maxUnits;
	}

	public void setMaxUnits(int maxUnits) {
		this.maxUnits = maxUnits;
	}

	public int getMinUnits() {
		return minUnits;
	}

	public void setMinUnits(int minUnits) {
		this.minUnits = minUnits;
	}

	public int getMaxPlayer() {
		return maxPlayer;
	}

	public void setMaxPlayer(int maxPlayer) {
		this.maxPlayer = maxPlayer;
	}

	public int getMinplayer() {
		return minplayer;
	}

	public void setMinplayer(int minplayer) {
		this.minplayer = minplayer;
	}
	
	/**
	 * 
	 * @return returns as Json
	 */

	public JsonObjectBuilder getGameConfgJSON()
	{
		JsonBuilderFactory factory = Json.createBuilderFactory(null);
		
		JsonObjectBuilder jsonObBuilder = factory.createObjectBuilder();
		jsonObBuilder.add("minPlayers", minplayer);
		jsonObBuilder.add("maxPlayers", maxPlayer);
		
		JsonArrayBuilder edgesBuilder = factory.createArrayBuilder();
		
		for(int i = 0; i < edges.length; i++)
		{
			JsonArrayBuilder innerEdgesBuilder = factory.createArrayBuilder();
			for(int j = 0; j < edges.length; j++)
			{
				innerEdgesBuilder.add(edges[i][j]);
			}
			edgesBuilder.add(innerEdgesBuilder);
		}
		
		jsonObBuilder.add("edges", edgesBuilder);
		
		jsonObBuilder.add("minUnitsForCastleConquest",minUnits);
		jsonObBuilder.add("maxUnitsForCastleRecruit", maxUnits);
		jsonObBuilder.add("maxUnitsForNonCastleRecruit", maxUnitsNonCastle);
						
		JsonObjectBuilder jsonRecruitmentBuilder = factory.createObjectBuilder();
						
		jsonRecruitmentBuilder.add("swords",swords);
		jsonRecruitmentBuilder.add("archers",archers);
		jsonRecruitmentBuilder.add("knights",knigths);
						
		jsonObBuilder.add("recruitmentCost",jsonRecruitmentBuilder);
						
		JsonObjectBuilder jsonUpkeepBuilder = factory.createObjectBuilder();
		jsonUpkeepBuilder.add("swords",swordsCost);
		jsonUpkeepBuilder.add("archers",archersCost);
		jsonUpkeepBuilder.add("knights",knigthCost);
						
		jsonObBuilder.add("upkeepCost", jsonUpkeepBuilder);
		jsonObBuilder.add("starvingChance", starvingChance);
		jsonObBuilder.add("resourceDecay", resourceDecay);
		jsonObBuilder.add("eggChance", eggChance);
		jsonObBuilder.add("dragonKillChance", dragonKillChance);
		
		return jsonObBuilder;
	}
	
	/** 
	 * @param set edges
	 */

	public void setEdges(boolean[][] edges) {
		this.edges = edges;
	}
	
	
}