package View;

/**
 * Metadata of the data types,
 * is deciding if its a integer, double or boolean
 * JSON Object get Results
 * @author be7
 */

public enum DataTypeEnum {
	
	IntData,
	DoubleData,
	BooleanData;
	
}
