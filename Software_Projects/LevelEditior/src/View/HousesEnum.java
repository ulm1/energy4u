package View;

/**
 * Metadata of the houses, 
 * @be7
 */

public enum HousesEnum {
	Arynes,
	Baratheon,
	Graufreud,
	Lanister,
	Martel,
	Stark,
	Targaryen,
	Tyrell,
	Bolton,
}
