package View;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;

import java.util.ArrayList;
import java.util.LinkedList;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonValue.ValueType;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import Model.conecter;
import Model.myNode;
import myController.myActionListener;
import myController.myController;

public class layout extends JFrame {
	
	private static final long serialVersionUID = 1L;
	JButton next;
	JButton clear;
	JButton speichern;
	JButton back;
	JButton p_back;
	static JButton save;
	static JPanel right;
	
	private static SettingsTab setTabs;

	graphikFenster fenster;
	myActionListener Lp;
	myController c;
	JScrollPane scroll;
	JRadioButton[][] matrixButton;
	JPanel panel;
	JFrame start;
	JFrame partie1;
	JFrame configDatei;

	static JRadioButton rb1;
	JTextField Bebauung;
	public static boolean graphsaved = false;
	 static JPanel p;
		
	
	 /**
	  * This constructor is initializing a new Layout. 
	  * The Layout Object itself handles all underlying functions and objects and is the main controller 
	  * of the main Leveleditor view.
	  * @param c The here handled parameter c is the controller, 
	  * which is used throughout the whole layout.
	  * @author be7
	  */
	 
	public layout(myController c) {
		
		//create Levelkonfiguration page
				configDatei = new JFrame ("Levelkonfiguration");
				
				try {
					UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
						| UnsupportedLookAndFeelException e2) {
					e2.printStackTrace();
				}
				
				configDatei.setSize(1000,2000);
				configDatei.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				
				configDatei.setLayout(null);
				
				setTabs = new SettingsTab();
				add(setTabs);
				setTabs.setBounds(700, 1, 600, 1000);
				setTabs.setVisible(true);
				setTabs.setEditingMode(false);	
				
				
				
				//create partiekonfiguration page
				partie1 = new JFrame ("Partiekonfiguration");
				
			    partie1.setSize(1000,1000);
				partie1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				
				partie1.setLayout(null);

				JLabel runde = new JLabel ("Round");
				JLabel rundeZeit = new JLabel ("Time per Round");
				JLabel winter = new JLabel ("Winter loss Chance");
				JLabel winterNach = new JLabel ("Max seconds before Winter");
				

				
				partie1.add(runde);
				runde.setBounds(300, 250, 150, 25);
				JTextField runde1= new JTextField("");
				runde1.setBounds(600, 250, 150, 25);
				partie1.add(runde1);
				
				partie1.add(rundeZeit);
				rundeZeit.setBounds(300, 300, 150, 25);
				JTextField rundeZeit1= new JTextField("");
				rundeZeit1.setBounds(600, 300, 150, 25);
				partie1.add(rundeZeit1);
				
				
				partie1.add(winter);
				winter.setBounds(300, 350, 150, 25);
				JTextField winter1= new JTextField("");
				winter1.setBounds(600, 350, 150, 25);
				partie1.add(winter1);
				
				partie1.add(winterNach);
				winterNach.setBounds(300, 400, 170, 25);
				JTextField winterNach1= new JTextField("");
				winterNach1.setBounds(600, 400, 150, 25);
				partie1.add(winterNach1);
				partie1.setVisible(false);
				
				
				JButton speichern2 = new JButton("Save");
				speichern2.setBounds(300,700, 150, 40);
				speichern2.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						if (!Desktop.isDesktopSupported()) {
							return;
						}
						final JFileChooser fileChooser = new JFileChooser();
						FileFilter f = new FileNameExtensionFilter("json file","json");
						fileChooser.setFileFilter(f);
						int result = fileChooser.showSaveDialog(null);
						if (result == JFileChooser.APPROVE_OPTION) {
							File fe = fileChooser.getSelectedFile(); 
							JsonObject file = myController.Partiekonfiguration(Integer.parseInt(runde1.getText()),Integer.parseInt(rundeZeit1.getText()),Float.parseFloat(winter1.getText()),Integer.parseInt( winterNach1.getText()));
							partie1.setVisible(false);
							setVisible(true);
						
							try {
								FileWriter fw = new FileWriter(fe.getPath());
								fw.write(file.toString());
								fw.flush();
								fw.close();
							} catch (Exception e1) {

								System.out.println("error");
							}
						}
					}
				});
				
				
				/**
				* This button is located in the game configuration
				* which returns you to the main menu. 
				* @author be7
				*/
				
				JButton p_back = new JButton("Back");
				p_back.setBounds(500,700, 150, 40);
				p_back.addActionListener(new ActionListener() {
			
					@Override
					public void actionPerformed(ActionEvent e) {
						goBack();

					}

					private void goBack() {
						setVisible(true);
						partie1.setVisible(false);

					}
				});
			
				partie1.add(p_back);	
				partie1.add(speichern2);		

				
		right = new JPanel();
		right.setLayout(null);

		right.setBackground(Color.red);
		
		p = new JPanel();
		p.setLayout(null);

		p.setBackground(Color.blue);
		
	
		
		

		// create start page;
		start = new JFrame("Leveleditor");

		start.setSize(1000, 1000);
		start.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JButton open = new JButton("Open LevelEditor");
		JButton open2 = new JButton("Open Partiekonfiguration");
		JButton newL = new JButton("New");
		JButton Exit = new JButton("Exit");
		 save = new JButton("Save");
		start.setLayout(null);

		start.add(open);
		start.add(open2);
		start.add(newL);
		start.add(Exit);

		open.setBounds(400, 300, 150, 40);
		open2.setBounds(400, 400, 150, 40);
		newL.setBounds(400, 500, 150, 40);
		Exit.setBounds(400, 600, 150, 40);

		newL.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setVisibility();

			}

		});

		Exit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);

			}

		});
		
		
		open2.addActionListener(new ActionListener() {
			/**
			 * @author ronaldagee
			 */
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!Desktop.isDesktopSupported()) {
					return;
				}
				final JFileChooser fileChooser = new JFileChooser(new File ("c.\\"));
				FileFilter f = new FileNameExtensionFilter("json file","json");
				fileChooser.setFileFilter(f);
				int result = fileChooser.showOpenDialog(null);
				if (result == JFileChooser.APPROVE_OPTION) {
				
					
					
					File json2InputFile = fileChooser.getSelectedFile();
					
					InputStream iss = null;
					try {
						iss = new FileInputStream(json2InputFile);
					} catch (FileNotFoundException e1) {

						e1.printStackTrace();
					}
					JsonReader reader3 = Json.createReader(iss);
					JsonObject jsonOb1 = reader3.readObject();
					
				
					int json =jsonOb1.getJsonObject("matchcfg").getInt("numberOfRoundsBeforeWinter");
					Integer.toString(json);
					runde1.setText(Integer.toString(json));
					
					int json2 =jsonOb1.getJsonObject("matchcfg").getInt("timePerPhase");
					Integer.toString(json2);
					rundeZeit1.setText(Integer.toString(json2));
					
					int json3 =jsonOb1.getJsonObject("matchcfg").getInt("winterLossChance");
					Integer.toString(json3);
					winter1.setText(Integer.toString(json3));
					
					int json4 =jsonOb1.getJsonObject("matchcfg").getInt("maxSecondsBeforeWinter");
					Integer.toString(json4);
					winterNach1.setText(Integer.toString(json4));
							
					partie1.setVisible(true);
					
					System.out.println(jsonOb1);
						
			}
			}
			});

		open.addActionListener(new ActionListener() {
	
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!Desktop.isDesktopSupported()) {
					return;
				}
				final JFileChooser fileChooser = new JFileChooser(new File ("c.\\"));
				FileFilter f = new FileNameExtensionFilter("json file","json");
				fileChooser.setFileFilter(f);
				int result = fileChooser.showOpenDialog(null);
				if (result == JFileChooser.APPROVE_OPTION) {
					File json2InputFile = fileChooser.getSelectedFile();
					
					InputStream iss = null;
					try {
						iss = new FileInputStream(json2InputFile);
					} catch (FileNotFoundException e1) {

						e1.printStackTrace();
					}
					JsonReader reader3 = Json.createReader(iss);
					JsonObject jsonOb1 = reader3.readObject();
							
					System.out.println(jsonOb1);					
			}
			}
			});

		start.setVisible(true);

		this.setTitle("LevelEditor");
		this.setSize(1500, 1500);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLayout(null);
		this.c = c;

		JMenuBar menuBar = new JMenuBar();
		JMenu menu = new JMenu("File");
		JMenu shortCutMenu = new JMenu("Settings");
		menuBar.add(shortCutMenu);
		menuBar.add(menu);
		
		JMenuItem menuItem_open = new JMenuItem("Open");
		menuItem_open.addActionListener( new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JsonObject loadedData = loadTrigger();
				if(loadedData != null) {
					loadedData = loadedData.getJsonObject("levelcfg");
					
					if(loadedData == null) {
						System.err.println("The config file is corrupted. A levelcfg tag is missing.");
						return;
					}
					
					JsonArray jsEdges = setTabs.loadData(loadedData);
					fenster.controller.n = setTabs.getNodes();
					if(jsEdges == null) {
						System.err.println("The edges table is corrupted");
						return;
					}
					int i = 0;
					int j = 0;
					
					LinkedList<LinkedList<Boolean>> edges = new LinkedList<LinkedList<Boolean>>();
					
					for(JsonValue jsValAr : jsEdges) {
						if(jsValAr.getValueType() != ValueType.ARRAY) {
							System.err.println("The edges table is corrupted");
							return;
						}
						
						LinkedList<Boolean> innerEdges = new LinkedList<Boolean>();
						edges.add(innerEdges);
						
						JsonArray jsArray = (JsonArray) jsValAr;
						j = 0;
						for(JsonValue jsPosVal : jsArray) {

							if(jsPosVal.getValueType() == ValueType.FALSE ) {
								innerEdges.add(new Boolean(false));
							}
							else if(jsPosVal.getValueType() == ValueType.TRUE ) {
								innerEdges.add(new Boolean(true));
							}
							else {
								System.err.println("The edges table is corrupted");
								return;
							}
							j++;
						}
						i++;
					}
					
					matrixButton = new JRadioButton[edges.size()][edges.size()];
					for(i = 0; i < edges.size(); i++) {
						for(j = 0; j <edges.size(); j++) {
							matrixButton[i][j] = new JRadioButton();
							matrixButton[i][j].setSelected(edges.get(i).get(j));
							if(i >= j) {
								matrixButton[i][j].setEnabled(false);
							}
							else {
								matrixButton[i][j].setEnabled(true);
								matrixButton[i][j].addActionListener(Lp);
							}
						}
					}
					updateMatrix(edges.size(), false);
					Lp.setColorToGraph();
					fenster.repaint();
				}
			}
			
		});
		KeyStroke keyStrokeToOpen = KeyStroke.getKeyStroke(KeyEvent.VK_O, KeyEvent.CTRL_DOWN_MASK);
		menuItem_open.setAccelerator(keyStrokeToOpen);
		shortCutMenu.add(menuItem_open);
		
		JMenuItem menuItem_save = new JMenuItem("Save");
		menuItem_save.addActionListener( new ActionListener() {
				
					@Override
					public void actionPerformed(ActionEvent e) {
						triggerSave();	
					}
				}
			);
		KeyStroke keyStrokeToSave = KeyStroke.getKeyStroke(KeyEvent.VK_S, KeyEvent.CTRL_DOWN_MASK);
		menuItem_save.setAccelerator(keyStrokeToSave);
		shortCutMenu.add(menuItem_save);
		
		JMenuItem menuItem_clear = new JMenuItem("Clear");
		menuItem_clear.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				triggerClear();
			}
			
		});
		KeyStroke keyStrokeToClear = KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, KeyEvent.CTRL_DOWN_MASK);
		menuItem_clear.setAccelerator(keyStrokeToClear);
		shortCutMenu.add(menuItem_clear);
		
		JMenuItem menuItem_exit = new JMenuItem("Exit");
		menuItem_exit.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
			
		});
		KeyStroke keyStrokeToExit = KeyStroke.getKeyStroke(KeyEvent.VK_END, KeyEvent.CTRL_DOWN_MASK);
		menuItem_exit.setAccelerator(keyStrokeToExit);
		shortCutMenu.add(menuItem_exit);
		
		// a group of JMenuItems
	
		JMenuItem menuItem2 = new JMenuItem("Partiekonfiguration");
		menu.add(menuItem2);

		this.setJMenuBar(menuBar);

		// function = new myActionListener(this);

		// get my current functions from the graphiFenster class

		fenster = new graphikFenster(c);
		c.setGraphikFenster(fenster);

		// declare the buttons
		back = new JButton("BACK");
		back.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				goBack();
			}

			private void goBack() {
				setVisible(false);
				start.setVisible(true);
			}
		});

		next = new JButton("EXIT");
		next.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);

			}

		});
		clear = new JButton("CLEAR");
		Lp = new myActionListener(this, c.getGraphikFenster());

		// functions of the buttons

		clear.addActionListener(Lp);
		 clear.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					triggerClear();
				}
		 });


		// add it to the JFrame
		add(save);
		save.setBounds(1100, 1100, 150, 40);
		add(next);
		next.setBounds(700, 1100, 150, 40);
		add(back);
		back.setBounds(900, 1100, 150, 40);
		add(clear);
		clear.setBounds(40, 450, 150, 40);
		fenster.setBounds(40, 20, 600, 400);

		Bebauung = new JTextField ("");
		Bebauung.setBounds(780, 215, 100, 25);
		Bebauung.setVisible(false);
		add(Bebauung);

		add(fenster);
		addMatix(0);
		
			menuItem2.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					
					setVisible(false);
					partie1.setVisible(true);
				}
			 }
			 );
		this.setVisible(false);

	}
	
	protected void setVisibility() {
		start.setVisible(false);
		this.setVisible(true);

	}

	// Creates my Matrix
	public boolean[][] setMatix() {

		// toDO
		int coulumns = c.getN().size();
		boolean[][] mybool = new boolean[coulumns][coulumns];
		for (int i = 0; i < coulumns; i++) {
			for (int j = 0; j < coulumns; j++) {
				if (matrixButton != null) {
					if (matrixButton[i][j].isSelected()) {
						mybool[i][j] = true;
					} else {
						mybool[i][j] = false;
					}
				}
			}
		}

		return mybool;
	}

	public void addMatix(int e) {

		if (scroll != null) {
			this.remove(this.scroll);
		}

		panel = new JPanel();

		// setzte die farbe f�r mein unteres Panel

		panel.setBackground(Color.white);

		// damit die buttons richtig geordnet sind

		panel.setLayout(null);
		panel.setPreferredSize(new Dimension(20 + MatrixSize(), 20 + MatrixSize()));
		this.matrixButton = new JRadioButton[e][e];
		for (int i = 0; i < e; i++) {
			for (int j = 0; j < e; j++) {

				// Nummer im oberen Bereich aufz�hlen
				if (j == 0) {
					JLabel headNumbers = new JLabel((i + 1) + "");
					panel.add(headNumbers);

					headNumbers.setBounds(35 + getCenter() + 40 * i, getCenter() + 40 * j, 40, 40);

				}
				// horizontale Aufz�hlung

				if (i == 0) {
					JLabel horizontalNumbers = new JLabel((j + 1) + "");
					this.panel.add(horizontalNumbers);
					horizontalNumbers.setBounds(getCenter() + 40 * i, getCenter() + 40 + 40 * j, 60, 60);
				}
				// kleine Panel rechts f�r Button setzen
				this.matrixButton[i][j] = new JRadioButton();
				panel.add(this.matrixButton[i][j]);

				this.matrixButton[i][j].setBounds(getCenter() + 20 + 40 * i, getCenter() + 40 + 40 * j, 60, 60);
				this.matrixButton[i][j].setBackground(Color.yellow);
				this.matrixButton[i][j].setHorizontalAlignment(SwingConstants.CENTER);

				if (j >= i) {
					this.matrixButton[i][j].setEnabled(false);

				} else {
					matrixButton[i][j].addActionListener(Lp);
				}
			}
		}
		scroll = new JScrollPane(panel);
		add(scroll, 0);
		// unteresFenster Position
		scroll.setBounds(40, 500, 600, 400);
		scroll.updateUI();

	}

	private int getCenter() {
		int numVertices = this.c.getN().size();
		if (400 - (numVertices * 40) <= 0) {
			return 0;
		}
		return ((400 - (numVertices * 40)) / 2);
	}

	// the Size of the Matrix

	int MatrixSize() {
		int n = this.c.getN().size();
		if ((n * 40) <= 480) {
			return 480;
		}
		return ((n * 40));
	}

	public JRadioButton[][] getMatrixButton() {
		return matrixButton;
	}

	public JButton erase() {
		graphsaved = false;
		return clear;
	}

	public myController getC() {
		return c;
	}

	public graphikFenster getFenster() {
		return fenster;
	}
	
	/**
	 * generates a new tab
	 * @author be7
	 * @param myNode : new node which is paint
	 */
	public void fastCreation(myNode myNode) {
		
		setTabs.setEditingMode(true);
		setTabs.generateTabs(myNode);
		
		save.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) 
			{	
				triggerSave();
			}
		});
		
		repaint();
	}

	public void updateMatrix(int size)
	{
		updateMatrix(size, true);
	}
	
	/**
	 *generates JButtons
	 * @param size
	 * @param addNewRows
	 * @author ronald agee
	 */
	
	public void updateMatrix(int size, boolean addNewRows) {
		if (scroll != null) {
			this.remove(this.scroll);
		}

		panel = new JPanel();

		// setzte die farbe f�r mein unteres Panel

		panel.setBackground(Color.white);

		// damit die buttons richtig geordnet sind

		panel.setLayout(null);
		panel.setPreferredSize(new Dimension(20 + MatrixSize(), 20 + MatrixSize()));
		JRadioButton oldButtons[][] = matrixButton;
		this.matrixButton = new JRadioButton[size][size];
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {

				// Nummer im oberen Bereich aufz�hlen
				if (j == 0) {
					JLabel headNumbers = new JLabel((i + 1) + "");
					panel.add(headNumbers);

					headNumbers.setBounds(35 + getCenter() + 40 * i, getCenter() + 40 * j, 40, 40);

				}
				// horizontale Aufz�hlung

				if (i == 0) {
					JLabel horizontalNumbers = new JLabel((j + 1) + "");
					this.panel.add(horizontalNumbers);
					horizontalNumbers.setBounds(getCenter() + 40 * i, getCenter() + 40 + 40 * j, 60, 60);
				}
				// kleine Panel rechts f�r Button setzen
				this.matrixButton[i][j] = new JRadioButton();
				panel.add(this.matrixButton[i][j]);

				this.matrixButton[i][j].setBounds(getCenter() + 20 + 40 * i, getCenter() + 40 + 40 * j, 60, 60);
				this.matrixButton[i][j].setBackground(Color.yellow);
				this.matrixButton[i][j].setHorizontalAlignment(SwingConstants.CENTER);
				if((i < size - 1 && j < size - 1)  || !addNewRows)
				{
					this.matrixButton[i][j].setSelected(oldButtons[i][j].isSelected());
				}
				if (j >= i) {
					this.matrixButton[i][j].setEnabled(false);

				} else {
					matrixButton[i][j].addActionListener(Lp);
				}
			}
		}
		scroll = new JScrollPane(panel);
		add(scroll, 0);
		// unteresFenster Position
		scroll.setBounds(40, 500, 600, 400);
		scroll.updateUI();
	}

	/**
	 * Clear Button
	 * Clears the entered data
	 * @author be7
	 */
	public void triggerClear() {
		c.setN(new ArrayList<myNode>());
		c.setConect(new ArrayList<conecter>());
		c.setSection(new ArrayList<myNode>());
		getFenster().repaint();
		addMatix(0);
		
		Lp.gf.getCheck().getN().clear();
		Lp.gf.getCheck().getConect().clear();
		Lp.gf.getCheck().getPolygonSection().clear();
		Lp.gf.getCheck().getSection().clear();
		Lp.gf.getCheck().getPolygonSection().clear();
		Lp.gf.getCheck().getMatrix().clear();
		
		setTabs.setDefault();
	}
	
	/**
	 * Save Button
	 * Function for saving a created Level
	 * @author be7
	 */
	
	private void triggerSave() {
	
		if (!Desktop.isDesktopSupported()) {
			return;
		}
		final JFileChooser fileChooser = new JFileChooser();
		FileFilter f = new FileNameExtensionFilter("json file","json");
		fileChooser.setFileFilter(f);
		int result = fileChooser.showSaveDialog(null);
		// File file = new File("team20");
		if (result == JFileChooser.APPROVE_OPTION) {
			File fe = fileChooser.getSelectedFile(); 
			
			boolean[][] edges = new boolean[matrixButton.length][matrixButton.length]; //second dimension is correct this way, because the matrix is allways a square
			for(int i = 0; i < matrixButton.length ; i++) {
			
				for(int j = 0; j < matrixButton.length; j++) {
				
					edges[i][j] = matrixButton[i][j].isSelected();
				}
			}
			
			JsonObject file = setTabs.getResults(edges);
			System.out.println(file.toString());
			 try {
				String ending = "";
				if(f.accept(fe))
					ending = "";
				else
					ending = ".json";
				FileWriter fw = new FileWriter(fe.getPath() + ending);
				fw.write(file.toString());
			 	fw.flush();
			 	fw.close();
			 } 
			 catch (Exception e1) {
				 System.out.println("error");
			 }
		}
	}
	
	/** 
	 * loads the saved data or level with a FileChooser
	 * @author be7
	 * @return returns the file which should be loaded
	 */
	
	public JsonObject loadTrigger() {
		JFileChooser fileChooser = new JFileChooser();
		JsonObject loadedData = null;
		FileFilter f = new FileNameExtensionFilter("json file","json");
		fileChooser.setFileFilter(f);
		int result = fileChooser.showOpenDialog(null);
		if(result == JFileChooser.APPROVE_OPTION){
			File fe = fileChooser.getSelectedFile();
			if(!f.accept(fe)) {
				return null;
			}
			FileInputStream fileReader;
			JsonReader jsonReader;
			try {
				fileReader = new FileInputStream(fe);
				jsonReader = Json.createReader(fileReader);

				loadedData = jsonReader.readObject();
				
				jsonReader.close();
			}
			catch (IOException e) {		
				e.printStackTrace();
			}
		}
		return loadedData;
	}

}
