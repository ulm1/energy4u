package View;

import java.awt.GridLayout;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonNumber;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import javax.json.JsonValue.ValueType;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.text.NumberFormatter;

/**
 * manages one terrain option
 * @author berfin
 */
public class TerrainOptionTab extends JPanel {
	
	
	JFormattedTextField [][] Tabelle= new JFormattedTextField [3][3];  //3 dimensional 
	private TerrainEnum trEnum;
	final private JLabel[] soldierLabels = {new JLabel("Swordman"), new JLabel("Archer"), new JLabel("Knight")};
	final private JLabel[] soldierLabels2 = {new JLabel("Swordman"), new JLabel("Archer"), new JLabel("Knight")};
	
	
/**
 * constructs the tabs for the terrain
 * @param trEnum : list of all existing terrains
 */
	public TerrainOptionTab(TerrainEnum trEnum) 
	{
		this.trEnum = trEnum;
		
		NumberFormat formatterInt = new DecimalFormat("#");
		NumberFormatter returnFormat = new NumberFormatter(formatterInt);
		returnFormat.setAllowsInvalid(false);
		
		GridLayout mainLayout = new GridLayout(4, 4);
		
		setLayout(mainLayout);
		JPanel emptyPanel = new JPanel();
		add(emptyPanel);
		for(JLabel label : soldierLabels)
		{
			add(label);
		}
		
		// Tabelle erstellen
		for(int iterator = 0; iterator < 3; iterator++) 
		{
			add(soldierLabels2[iterator]);
			for(int iterator2= 0; iterator2 <3; iterator2++)
			{
			Tabelle[iterator][iterator2] = new JFormattedTextField(returnFormat);
			Tabelle[iterator][iterator2].setValue(0);
			
			add(Tabelle[iterator][iterator2]);
			}
			
		} 
		
	}
	/**
	 * 
	 * @param trEnum
	 * @param jsArray
	 */
	public TerrainOptionTab(TerrainEnum trEnum, JsonArray jsArray) {
		this(trEnum);
		
		int iterator = 0;
		int iterator2 = 0;
		
		
		for( JsonValue value : jsArray) 
		{
			if(value.getValueType() != ValueType.ARRAY)
			{
				System.err.println("The terrain " + trEnum.name() + " seems to be corrupted.");
				return;
			}
			JsonArray jsInnerArray = (JsonArray) value;
			for(JsonValue value2 : jsInnerArray)
			{
				if(value2.getValueType() != ValueType.NUMBER)
				{
					System.err.println("The terrain " + trEnum.name() + " seems to be corrupted.");
					return;
				}
				double doubleBuffer = ((JsonNumber) value2).doubleValue();
				Tabelle[iterator][iterator2].setValue(doubleBuffer);
			}
			
		} 
	}

	/**
	 *deletes all inputs
	 */
	public void setDefault() 
	{
		for(JFormattedTextField[] a_jft : Tabelle)
		{
			for(JFormattedTextField field : a_jft)
			{
				field.setValue(0);
			}
		}
		setEditingMode(false);
	}

	/**
	 * 
	 * @param b_value
	 */
	public void setEditingMode(boolean b_value) 
	{
		for(JFormattedTextField[] a_jft : Tabelle)
		{
			for(JFormattedTextField field : a_jft)
			{
				field.setEditable(b_value);
			}
		}
	}

	public String getTitle() 
	{
		
		return trEnum.name();
		
	}

	/**
	 * 
	 * @return
	 */

	public JsonObjectBuilder getAsJson() {

		JsonObjectBuilder terBuilder = Json.createBuilderFactory(null).createObjectBuilder();
		JsonArrayBuilder tableBuilder = Json.createBuilderFactory(null).createArrayBuilder();
		
		for(int i = 0; i < 3; i++)
		{
			JsonArrayBuilder rowBuilder = Json.createBuilderFactory(null).createArrayBuilder();
			for(int j = 0; j < 3; j++)
			{
				Number percentage = (Number)Tabelle[i][j].getValue();
				rowBuilder.add(percentage.doubleValue());
			}
			tableBuilder.add(rowBuilder);
		}
		
		terBuilder.add("terrain", trEnum.name());
		terBuilder.add("percentages", tableBuilder);
		
		return terBuilder;
	}

}
