package View;

import java.awt.GridLayout;
import java.util.ArrayList;

import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import javax.json.JsonValue.ValueType;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonBuilderFactory;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import Model.myNode;

public class SettingsTab extends JPanel 
{
	private static final long serialVersionUID = 1L;
	
	
	private ArrayList<NodeConfig> a_NodeConfigurations;
	
	private String mapName = "Map name goes here";
	private String creator = "Name goes here";
	
	private GlobalSettings glSettings;
	private TerrainSettings trSettings;

	private JTabbedPane tabCtrl;
	private JTabbedPane globCtrl;

	/**
	 * Constructor, divides the components. Get Superclass and inhertis from JPanel. 
	 * @author be7
	 */
	
	public SettingsTab()
	{
		super();
		
		a_NodeConfigurations = new ArrayList<NodeConfig>(); 
		glSettings = new GlobalSettings();
		trSettings = new TerrainSettings();
		
		GridLayout cv_masterLayout = new GridLayout(2, 1);
		
		tabCtrl = new JTabbedPane(JTabbedPane.TOP,JTabbedPane.SCROLL_TAB_LAYOUT);
		globCtrl = new JTabbedPane(JTabbedPane.TOP,JTabbedPane.SCROLL_TAB_LAYOUT);
		add(tabCtrl);
		
		cv_masterLayout.addLayoutComponent("Settings", globCtrl);
		
		globCtrl.addTab("Global Settings", glSettings);
		globCtrl.addTab("Terrain Settings", trSettings);
		
		add(globCtrl);
		
		setLayout(cv_masterLayout);
	}
	
	/**
	 * sets tabs visible
	 * @param value: this parameter sets the visibility true or false
	 */
	@Override
	public void setVisible(boolean value)
	{
		tabCtrl.setVisible(value);
		globCtrl.setVisible(value);
		
		super.setVisible(value);
	}
	
	/**
	 * gets x and y coordinates
	 * @author be7
	 * @return returns nodes
	 */
	
	public ArrayList<myNode> getNodes()
	{
		ArrayList<myNode> myNodes = new ArrayList<myNode>();
		
		a_NodeConfigurations.forEach( confg -> myNodes.add(new myNode(confg.getId(), confg.getXCordinate(), confg.getYCordinate())));
		
		return myNodes;
	}
	
	public JsonObject getResults(boolean[][] edges)
	{
		JsonBuilderFactory factory = Json.createBuilderFactory(null);
		
		JsonObjectBuilder metaBuilder = factory.createObjectBuilder();
		metaBuilder.add("name", mapName);
		metaBuilder.add("creationDate", System.currentTimeMillis());
		metaBuilder.add("creator", creator);
		
		JsonArrayBuilder territoriesBuilder = factory.createArrayBuilder();
		int nullcounter = 0;
		for(NodeConfig s_Node : a_NodeConfigurations)
		{
			if (s_Node == null)
			{
				System.out.println("null");
				nullcounter++;
				continue;
			}
			territoriesBuilder.add(s_Node.getAsJson());
			
		}
		System.out.println(nullcounter);
		JsonObjectBuilder lvlConfigBuilder = factory.createObjectBuilder();
		lvlConfigBuilder.add("meta", metaBuilder);
		lvlConfigBuilder.add("game", glSettings.getAsJson(edges));
		lvlConfigBuilder.add("territories", territoriesBuilder);
		lvlConfigBuilder.addNull("additions");
		lvlConfigBuilder.add("table", trSettings.getAsJson());
		
		
		JsonObjectBuilder resultBuilder =  factory.createObjectBuilder();
		
		resultBuilder.add("levelcfg", lvlConfigBuilder);
		
		return resultBuilder.build();
	}
	/**
	 * activates and deactivates the global and terrain settings
	 * @param b_value
	 */

	public void setEditingMode(boolean b_value) {
		glSettings.setEditingMode(b_value);
		trSettings.setEditingMode(b_value);
	}
	
	public void generateTabs(myNode myNode)
	{
			NodeConfig newConfigTab = new NodeConfig(myNode);
			
			a_NodeConfigurations.add(newConfigTab);
			
			tabCtrl.add(""+(myNode.getName()), newConfigTab);
			newConfigTab.setVisible(true);
	}
	
	/**
	 * resets everything on default values
	 */

	public void setDefault() {
		
		glSettings.setDefault();
		trSettings.setDefault();
		
		tabCtrl.removeAll();
	}
	
	/**
	 * loads a Json File and divides it in logical segments, pushing them towards to destination
	 * @param loadedData
	 * @return
	 */
	
	public JsonArray loadData(JsonObject loadedData) {
		
		if(loadedData == null)
		{
			throw new IllegalArgumentException("No game Section existing.");
		}
		System.out.println(loadedData.toString());
		JsonObject jsonGameCfg = loadedData.getJsonObject("game");
		JsonArray edges = glSettings.loadFields(jsonGameCfg);
		
		tabCtrl.removeAll();
		a_NodeConfigurations.clear();
		JsonArray jsonTerritoriesCfg = loadedData.getJsonArray("territories");
		for(JsonValue jsValue : jsonTerritoriesCfg)
		{
			if(jsValue.getValueType() == ValueType.OBJECT)
			{
				JsonObject jsObject = (JsonObject) jsValue;
				NodeConfig newConfigTab = new NodeConfig(jsObject);
				tabCtrl.addTab(""+newConfigTab.getId(), newConfigTab);
				a_NodeConfigurations.add(newConfigTab);
			}
			else
			{
				throw new IllegalArgumentException("The territory array is corrupted.");
			}
		}
		
		
		JsonArray jsonTableCfg = loadedData.getJsonArray("table");
		trSettings.loadData(jsonTableCfg);
		return edges;
	}
}
