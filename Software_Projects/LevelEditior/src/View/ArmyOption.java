package View;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.text.NumberFormatter;

/**
 * @author berfin
 *
 *	This class is designed to store and manage the information about one army in one territory.
 *	This class is managed by the Army Option Control.
 */
public class ArmyOption extends JPanel {
	private static final long serialVersionUID = -5355670208318281992L;
	
	private ArmyOptionControl storageClass;
	private boolean initCompleted = false;
	
	private JFormattedTextField[] a_TextFields;
	private JComboBox<HousesEnum> s_DropDown;
	private JButton deletionButton;
	
	private final JLabel a_labels[] = 
		{
				new JLabel("Allegiance: "),
				new JLabel("Swordmen: "),
				new JLabel("Archers: "),
				new JLabel("Knigths: ")
		};
	
	/**
	 * This is a constructor of an Army Option storage class.
	 * @param armyOptionControl: This is the reference to the Controller, which regulates all the Army Options used in this one Tab.
	 * @param value: This JsonObject contains information, which are used to initialize the Army Option with. 
	 */	
	public ArmyOption(ArmyOptionControl armyOptionControl, JsonObject value) {
		this(armyOptionControl);
		
		String jsHouse = value.getString("allegiance");
		if(jsHouse == null)
		{
			System.err.println("The units tab of a territory seems corrupted.");
			return;
		}
		HousesEnum selectedHouse = HousesEnum.valueOf(jsHouse);
		s_DropDown.setSelectedItem(selectedHouse);
		
		int swordBuffer = value.getJsonNumber("existingSwords").intValue();
		a_TextFields[0].setValue(swordBuffer);
		int archerBuffer = value.getJsonNumber("existingArchers").intValue();
		a_TextFields[1].setValue(archerBuffer);
		int knightBuffer = value.getJsonNumber("existingKnights").intValue();
		a_TextFields[2].setValue(knightBuffer);
	}

	/**
	 * This is a constructor of an Army Option storage class.
	 * @param storageClass: This is the reference to the Controller, which regulates all the Army Options used in this one Tab. 
	 */
	public ArmyOption(ArmyOptionControl storageClass) {
		
		this.storageClass = storageClass;
		GridLayout cv_masterLayout = new GridLayout(2, 1);
		
		setLayout(cv_masterLayout);
		
		JPanel upperPanel = new JPanel();
		GridLayout upperLayout = new GridLayout(4,2);
		upperPanel.setLayout(upperLayout);
		add(upperPanel);
		
		s_DropDown = new JComboBox<HousesEnum>();
		restartComboBox();
		s_DropDown.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent event) {
				if (event.getStateChange() == ItemEvent.SELECTED) {
					
					storageClass.triggerChange();
					
				}
			}
		});
		
		upperPanel.add(a_labels[0]);
		upperPanel.add(s_DropDown);
		
		NumberFormat formatterInt = new DecimalFormat("#");
		NumberFormatter returnFormat = new NumberFormatter(formatterInt);
		returnFormat.setAllowsInvalid(false);
		
		a_TextFields = new JFormattedTextField[3];
		
		for(int iterator = 0; iterator < 3; iterator++) 
		{
			a_TextFields[iterator] = new JFormattedTextField(returnFormat);
			a_TextFields[iterator].setValue(0);
			
			upperPanel.add(a_labels[iterator + 1]);
			upperPanel.add(a_TextFields[iterator]);
		}
		
		deletionButton = new JButton("Delete");
		deletionButton.setEnabled(false);
		deletionButton.setSize(new Dimension(2, 2));
		deletionButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				storageClass.triggerDeletion();
			}
		});
		
		JPanel lowerPanel = new JPanel();
		FlowLayout  lowerLayout = new FlowLayout ();
		
		lowerPanel.setLayout(lowerLayout );
		lowerPanel.add(deletionButton);
		add(lowerPanel);
		
		initCompleted = true;
		
	}
	
	/**
	 * returns the selected house from drop down list 
	 * @return returns the selected Item from the drop down Menu (HousesEnum)
	 */
	public HousesEnum getSelectedItem()
	{
		return (HousesEnum) s_DropDown.getSelectedItem();
	}
	
	/**
	 * This method restarts the ComboBox and fills it with the new houses, which are allowed to be set.
	 */
	public void restartComboBox() {
		ArrayList<HousesEnum> localListOfHouses = new ArrayList<HousesEnum>(storageClass.ListOfHouses);
		HousesEnum rememberMe = null;
		if(initCompleted)
		{
			rememberMe = getSelectedItem();
			localListOfHouses.add(rememberMe);
		}
		
		s_DropDown.removeAllItems();
		localListOfHouses.forEach(house -> s_DropDown.addItem(house));
		
		if(initCompleted)
		{
			s_DropDown.setSelectedItem(rememberMe);	
		}
	}
	
	/**
	 * sets the Army labels and their Textfields visible
	 */
	@Override
	public void setVisible(boolean value)
	{
		super.setVisible(value);

		for(int iterator = 0; iterator < 3; iterator++) 
		{
			a_labels[iterator].setVisible(value);;
			a_TextFields[iterator].setVisible(value);
		}
	}

	
	/**
	 * This Method returns the information stored in this object in a Json Format.
	 * @return returns the values in a JsonObjectBuilder.
	 */
	public JsonObjectBuilder getAsJson() {
		
		JsonObjectBuilder jsonArmyBuilder = Json.createBuilderFactory(null).createObjectBuilder();
		
		HousesEnum selectedHouse = (HousesEnum) s_DropDown.getSelectedItem();
		jsonArmyBuilder.add("allegiance", selectedHouse.name() );
		
		Number swordmen = (Number)a_TextFields[0].getValue();
		jsonArmyBuilder.add("existingSwords", swordmen.intValue());
		Number archers = (Number)a_TextFields[1].getValue();
		jsonArmyBuilder.add("existingArchers", archers.intValue());
		Number knights = (Number)a_TextFields[2].getValue();
		jsonArmyBuilder.add("existingKnights", knights.intValue());
		
		
		return jsonArmyBuilder;
	}

	
	/**
	 * This function activates and deactivates the delete option for this tab.
	 * 
	 * @param b: 	This parameter decides if the deletion button is enabled(true) 
	 * 				or disabled(false) for this tab.
	 */
	public void updateDeletButton(boolean b) {
		deletionButton.setEnabled(b);
	}

}
