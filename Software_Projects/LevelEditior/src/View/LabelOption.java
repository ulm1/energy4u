package View;

import javax.json.JsonObjectBuilder;

/**
 * deprecated
 * @author berfin
 *
 */
public class LabelOption extends NodeOption {
	/**
	 * gets the labels as Json File and extends the class NodeOption
	 */
	@Override
	public void getAsJson(JsonObjectBuilder jsonObBuilder) {
		 jsonObBuilder.addNull("label");
		
	}
}
