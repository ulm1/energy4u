package View;

import java.awt.GridLayout;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.text.NumberFormatter;

/**
 * is the view container for the levelconfiguration
 * @author berfin
 */

public class GlobalSettings extends JPanel {
	
	private JFormattedTextField[] a_inputFields;
	private JLabel[] a_DescriptionLabels;
	/**
	 * is the constructor 
	 */
	public GlobalSettings()
	{
		super();
		int i_upperBound = LevelConfigurationTypes.values().length;
		
		a_inputFields = new JFormattedTextField[i_upperBound];
		a_DescriptionLabels = new JLabel[i_upperBound];
		GridLayout cv_lowerLayout = new GridLayout( i_upperBound, 2);
		
		setLayout(cv_lowerLayout);
		
		LevelConfigurationTypes[] listOfEnums = LevelConfigurationTypes.values();
		
		for( LevelConfigurationTypes e_iteratingEnum : listOfEnums)
		{
			int i_iterator =  e_iteratingEnum.getIndex();
			a_inputFields[i_iterator] = new JFormattedTextField(getFormat(e_iteratingEnum));
			a_DescriptionLabels[i_iterator] = new JLabel(e_iteratingEnum.getDescribtion());
			a_inputFields[i_iterator].setValue(0);
			add(a_DescriptionLabels[i_iterator]);
			add(a_inputFields[i_iterator]);
			
		}
	}
	/**
	 * returns the format which is related to the given enum
	 * @param enumToMap: stores the information which formatter is required
	 * @return returns the correct formatter
	 */
	
	private NumberFormatter getFormat(LevelConfigurationTypes enumToMap)
	{
		NumberFormatter returnFormat;
		switch(enumToMap.getDataType())
		{
			case IntData:
				NumberFormat formatterInt = new DecimalFormat("#");
				returnFormat = new NumberFormatter(formatterInt);
				returnFormat.setAllowsInvalid(false);
				break;
			case DoubleData:
				NumberFormat formatterDouble = new DecimalFormat("#0.0####");
				returnFormat = new NumberFormatter(formatterDouble);
				returnFormat.setAllowsInvalid(false);
				break;
			case BooleanData:
				NumberFormat formatterBool = new DecimalFormat("#,##0.00;(#,##0.00)");
				returnFormat = new NumberFormatter(formatterBool);
				returnFormat.setAllowsInvalid(false);
				break;
			default:
				returnFormat = null;
				break;
		}
		
		return returnFormat;
		
	}
	
	/**
	 * sets textFields and labels visible
	 * @param value: this parameter sets the visibility true or false
	 */
	@Override
	public void setVisible(boolean value) {
		int i_upperBound = LevelConfigurationTypes.values().length;
		
		for(int i_iterator = 0; i_iterator < i_upperBound; i_iterator++)
		{
			a_inputFields[i_iterator].setVisible(value);
			a_DescriptionLabels[i_iterator].setVisible(value);
		}
		super.setVisible(value);
	}
	
	/**
	 * getting Information as JSon format
	 * @param edges: describes the edges between the nodes which represents the territories
	 * @return returns the global settings as Json
	 */
	public JsonObjectBuilder getAsJson(boolean[][] edges)
	{
		LevelConfiguration lvlConfg = new LevelConfiguration();
		lvlConfg.setEdges(edges);
		for(LevelConfigurationTypes iterationsElement : LevelConfigurationTypes.values())
		{
			Number value = (Number) a_inputFields[iterationsElement.getIndex()].getValue();
			
			switch(iterationsElement)
			{
			case MinPlayer:
				lvlConfg.setMinplayer(value.intValue());
				break;
			case MaxPlayer:
				lvlConfg.setMaxPlayer(value.intValue());
				break;
			case MinUnitsCastleConquest:
				lvlConfg.setMinUnits(value.intValue());
				break;
			case MaxUnitsCastleConquest:
				lvlConfg.setMaxUnits(value.intValue());
				break;
			case MaxUnitsNonCastle:
				lvlConfg.setMaxUnitsNonCastle(value.intValue());
				break;
			case SwordsRec:
				lvlConfg.setSwords(value.intValue());
				break;
			case ArchersRec:
				lvlConfg.setArchers(value.intValue());
				break;
			case KnigthsRec:
				lvlConfg.setKnigths(value.intValue());
				break;
			case SwordsUpKeep:
				lvlConfg.setSwordsCost(value.intValue());
				break;
			case ArchersUpKeep:
				lvlConfg.setArchersCost(value.intValue());
				break;
			case KnightUpKeep:
				lvlConfg.setKnigthCost(value.intValue());
				break;
			case ResourceDecay:
				lvlConfg.setResourceDecay(value.doubleValue());
				break;
			case StarvingChance:
				lvlConfg.setStarvingChance(value.doubleValue());
				break;
			case EggChance:
				lvlConfg.setEggChance(value.doubleValue());
				break;
			case DragonKillChance:
				lvlConfg.setDragonKillChance(value.doubleValue());
				break;
			}
		}
	
		return lvlConfg.getGameConfgJSON();
	}
	
	/**
	 * activates or deactivates the textfields
	 * @param b_value: activate and deactivates the textfields
	 */
	
	public void setEditingMode(boolean b_value) {
		for( JFormattedTextField inputField: a_inputFields)
			inputField.setEditable(b_value);	
	}
	
	/**
	 * resets everything on default values
	 */
	
	public void setDefault() {
		for(JFormattedTextField inputField : a_inputFields)
		{
			inputField.setValue(0);
			inputField.setEditable(false);
		}
	}
	
	/**
	 * gets the Information as JSOn file and sets the values accordingly
	 * @param jsonGameCfg: gets Json file with information
	 * @return return Edges in Json format
	 */
	public JsonArray loadFields(JsonObject jsonGameCfg) {
		LevelConfigurationTypes[] listOfEnums = LevelConfigurationTypes.values();
		for( LevelConfigurationTypes e_iteratingEnum : listOfEnums)
		{
			int i_iterator =  e_iteratingEnum.getIndex();
			if(
					e_iteratingEnum == LevelConfigurationTypes.SwordsUpKeep ||
					e_iteratingEnum == LevelConfigurationTypes.KnightUpKeep ||
					e_iteratingEnum == LevelConfigurationTypes.ArchersUpKeep ||
					e_iteratingEnum == LevelConfigurationTypes.SwordsRec ||
					e_iteratingEnum == LevelConfigurationTypes.KnigthsRec ||
					e_iteratingEnum == LevelConfigurationTypes.ArchersRec
				){
				continue;
			}
				
			else if (e_iteratingEnum.getDataType() == DataTypeEnum.IntData)
			{
				
				int intBuffer = jsonGameCfg.getJsonNumber(e_iteratingEnum.getJsonName()).intValue();
				a_inputFields[i_iterator].setValue(intBuffer);
			} 
			else if(e_iteratingEnum.getDataType() == DataTypeEnum.DoubleData)
			{
				double doubleBuffer = jsonGameCfg.getJsonNumber(e_iteratingEnum.getJsonName()).doubleValue();
				a_inputFields[i_iterator].setValue(doubleBuffer);
			}
			else
			{
				System.err.println("This Point should never be reached.");
				continue;
			}
		}
		
		JsonObject upKeepCost = jsonGameCfg.getJsonObject("upkeepCost");
		if(upKeepCost == null)
		{
			System.err.println("upkeepCost costs are corrupted or not properlly defined. Pls fix this.");
			return null;
		}
		int swordUpKeep = upKeepCost.getJsonNumber("swords").intValue();
		int KnightUpKeep = upKeepCost.getJsonNumber("knights").intValue();
		int archerUpKeep = upKeepCost.getJsonNumber("archers").intValue();
		
		a_inputFields[LevelConfigurationTypes.ArchersUpKeep.getIndex()].setValue(archerUpKeep);
		a_inputFields[LevelConfigurationTypes.KnightUpKeep.getIndex()].setValue(KnightUpKeep);
		a_inputFields[LevelConfigurationTypes.SwordsUpKeep.getIndex()].setValue(swordUpKeep);
		
		JsonObject recruitmentCost = jsonGameCfg.getJsonObject("recruitmentCost");
		if(recruitmentCost == null)
		{
			System.err.println("Recruitment costs are corrupted or not properlly defined. Pls fix this.");
			return null;
		}
		
		int swordRecruitment = 	recruitmentCost.getJsonNumber("swords").intValue();
		int KnightRecruitment = recruitmentCost.getJsonNumber("knights").intValue();
		int archerRecruitment = recruitmentCost.getJsonNumber("archers").intValue();
		
		a_inputFields[LevelConfigurationTypes.SwordsRec.getIndex()].setValue(swordRecruitment);
		a_inputFields[LevelConfigurationTypes.KnigthsRec.getIndex()].setValue(KnightRecruitment);
		a_inputFields[LevelConfigurationTypes.ArchersRec.getIndex()].setValue(archerRecruitment);
		
		JsonArray jsEdges = jsonGameCfg.getJsonArray("edges");
		return jsEdges;
	}
}
