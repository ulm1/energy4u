package View;

import java.awt.GridLayout;
import javax.json.JsonObjectBuilder;
import javax.swing.JComboBox;
import javax.swing.JLabel;

/**
 * This class is designed to store and manage the information about one terrain in a territory.
 * @author berfin
 *
 */
public class TerrainOption extends NodeOption {
	private static final long serialVersionUID = -4141753101597036166L;

	private GridLayout cv_masterLayout;
	
	private final JLabel s_label = new JLabel("Terrain: "); 
	private JComboBox<TerrainEnum> s_DropDown;
	
	/**
	 * creates labels and drop down for a terrain
	 */
	public TerrainOption() 
	{
		cv_masterLayout = new GridLayout(1,2);
		
		setLayout(cv_masterLayout);
		
		s_DropDown = new JComboBox<TerrainEnum>(TerrainEnum.values());
		
		add(s_label);
		add(s_DropDown);
	}
	/**
	 * sets the drop down menu and the labels visible
	 */
	@Override
	public void setVisible(boolean value)
	{
		super.setVisible(value);
		
		s_DropDown.setVisible(value);
		s_label.setVisible(value);
	}
	
	/**
	 * adds the stored information to the JSON builder
	 * @param jsonObBuilder: stores temporary information for processing 
	 */
	@Override
	public void getAsJson(JsonObjectBuilder jsonObBuilder) 
	{
		jsonObBuilder.add("terrain", ((TerrainEnum) s_DropDown.getSelectedItem()).name());
	}
	
	/**
	 * activate or deactivates the dropdown selection for a terrain
	 * @param terrainBuffer : selected terrain
	 */
	public void setTerrain(String terrainBuffer) 
	{
		TerrainEnum selectedEnum = TerrainEnum.valueOf(terrainBuffer);
		s_DropDown.setSelectedItem(selectedEnum);
	}
}
