package View;

import java.util.LinkedList;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.json.JsonValue.ValueType;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

/**
 * @author berfin
 * 
 */
public class TerrainSettings extends JPanel {
	
	private JTabbedPane enumBar; 
	private LinkedList<TerrainOptionTab> TerrainOptions_Ls;
	
	public TerrainSettings()
	{
		enumBar = new JTabbedPane();
		TerrainOptions_Ls = new LinkedList<TerrainOptionTab>();
		
		TerrainEnum[] trEnumLs = TerrainEnum.values();
		for (TerrainEnum trEnum : trEnumLs)
		{
			TerrainOptionTab newTab = new TerrainOptionTab(trEnum);
			TerrainOptions_Ls.add(newTab);
			enumBar.add(newTab.getTitle(), newTab);
		}
		
		add(enumBar);		
	}
	/**
	 * deletes all inputs
	 */
	
	public void setDefault() {
		
		TerrainOptions_Ls.forEach(trEnumOption -> trEnumOption.setDefault());
		
	}
	/**
	 * activates all fields for input
	 * @param b_value : if its true its editable and if its false its not editable
	 */

	public void setEditingMode(boolean b_value) {

		TerrainOptions_Ls.forEach(trEnumOption -> trEnumOption.setEditingMode(b_value));
		
	}
	
	/**
	 * 
	 * @return
	 */

	public JsonArrayBuilder getAsJson() {
		
		JsonArrayBuilder terArrayBuilder = Json.createBuilderFactory(null).createArrayBuilder();
		
		TerrainOptions_Ls.forEach(terrainOption -> terArrayBuilder.add(terrainOption.getAsJson()));
		
		return terArrayBuilder;
	}
	
	/**
	 * 
	 * @param jsonTerritoriesCfg: contains all lists of loss probability
	 */
	public void loadData(JsonArray jsonTerritoriesCfg) {
		enumBar.removeAll();
		TerrainOptions_Ls.clear();
		
		for(JsonValue jsValue : jsonTerritoriesCfg)
		{
			if(jsValue.getValueType() != ValueType.OBJECT)
			{
				System.err.println("The Terrain Data is corrupted, unexpected Data Type.");
				return;
			}
			JsonObject jsObject = (JsonObject) jsValue;
			
			String terrainBuffer = jsObject.getString("terrain");
			if(terrainBuffer == null)
			{
				System.err.println("The Terrain Data is corrupted, incorrect Terrain settings.");
				return;
			}
			TerrainEnum trEnum = TerrainEnum.valueOf(terrainBuffer);
			
			JsonArray jsArray = jsObject.getJsonArray("percentages");
			if(jsArray == null)
			{
				System.err.println("The Terrain Data is corrupted, incorret Percentages.");
				return;
			}
			
			TerrainOptionTab newTab = new TerrainOptionTab(trEnum, jsArray);
			TerrainOptions_Ls.add(newTab);
			enumBar.add(newTab.getTitle(), newTab);
			
		}
		
	}
	
}
