package View;

import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import javax.swing.JCheckBox;

/**
 * This class is designed to store and manage the information about one tavern in a territory.
 * @author berfin
 */
public class TavernOption extends NodeOption {

	private JCheckBox checkBox;
	
	/**
	 * 
	 */
	public TavernOption()
	{
		checkBox = new JCheckBox("Tavern");
		
		add(checkBox);
	}
	/**
	 * sets the checkbox for a tavern visible
	 */
	public void setVisible(boolean value)
	{
		super.setVisible(value);

		checkBox.setVisible(value);
	}
	/**
	 * activate or deactivates the checkbox for a tavern in a territory
	 * @param tavernBuffer : activate or deactivates the checkbox
	 */
	
	public void setTavern(boolean tavernBuffer) {
		
		checkBox.setSelected(tavernBuffer);
		
	}
	
	/**
	 * adds the stored information to the JSON builder
	 * @param jsonObBuilder: stores temporary information for processing 
	 */
	@Override
	public void getAsJson(JsonObjectBuilder jsonObBuilder) {
		
		jsonObBuilder.add("tavern", (JsonValue) (checkBox.isSelected() ? JsonValue.TRUE : JsonValue.FALSE ));

		
	}
}
