package View;

/**
 * Metadata of the terrain, 
 * @be7
 */

enum TerrainEnum {
	Desert,
	Plain,
	Hills,
	Swamp,
	Forest
}
