package View;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.JComponent;
import Model.conecter;
import Model.myNode;
import Model.shape;
import myController.myController;

/**
 * @author ronald agee
 */

public class graphikFenster extends JComponent implements MouseListener {
	public myController controller;
	private layout myMainWindow;
	
	public myController getCheck() {
		return controller;
	}
	
	Graphics draw;

	public graphikFenster(myController check) {
		this.controller = check;
		check.setGraphikFenster(this);
		this.addMouseListener(this);
	}

	// F�r die Zahlen auf der oberen Fl�che die wir mit der maus setzten

	@Override
	public void paint(Graphics draw) {
		this.draw = draw;

		draw.setColor(Color.white);
		draw.fillRect(0, 0, this.getWidth(), this.getHeight());

		if (controller.getSection().size() == 0) {
			
			
			Fl�che(draw);
			draw.setColor(Color.red);

		}
		draw.setColor(Color.black);
		
		if(!layout.graphsaved==true){
		plotKnoten(draw);
		}
		draw.setColor(Color.black);
		plotBeziehung(draw);

	}



	private void plotBeziehung(Graphics paint) {
		ArrayList<conecter> lines = controller.getConect();
		for (int i = 0; i < lines.size(); i++) {
			myNode anfang = lines.get(i).getStartPoint();
			myNode ende = lines.get(i).getEndPoint();
			paint.drawLine((int) anfang.getX(), (int) anfang.getY(), (int) ende.getX(), (int) ende.getY());
		}
	}
	

	private void plotKnoten(Graphics paint) {
		ArrayList<myNode> node = controller.getN();
		int i = 0;
		for ( i = 0; i < node.size(); i++) {
			myNode zeigen = node.get(i);
				paint.drawString(Integer.toString(zeigen.getName()), (int) zeigen.getX(), (int) zeigen.getY());

		}

	}

	public void Fl�che(Graphics paint) {
		if(layout.graphsaved != true){

		ArrayList<shape> s = controller.getPolygonSection();
		for (int i = 0; i < s.size(); i++) {
			Random one = new Random();
			int red = (int) (Math.random() * 256);
			int green = (int) (Math.random() * 256);
			int black = (int) (Math.random() * 256);

			// Abf�rbung der Fl�che

			Color myColor = new Color(red, green, black, 170);
			//setzt die farbe f�r jedes section
			paint.setColor(myColor);
			Integer[] pi = s.get(i).getMyShape();
			int color1 = pi.length;
			int v[] = new int[color1];
			int z[] = new int[color1];
			for (int j = 0; j < color1; j++) {
				myNode n = controller.getname(pi[j]);
				if (n != null) {
					v[j] = (int) n.getX();
					z[j] = (int) n.getY();	
				}
			}
			paint.fillPolygon(v, z, color1);
		}
		}
	}

	@Override
	public void mouseClicked(MouseEvent a) {
		ArrayList<myNode> ListOfNodes = controller.getN();
		ListOfNodes.add(new myNode(ListOfNodes.size() + 1, a.getX(), a.getY()));
		myMainWindow.updateMatrix( ListOfNodes.size());
		
		myMainWindow.fastCreation(ListOfNodes.get(ListOfNodes.size() - 1));
		
		repaint();
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void setMyLayout(layout MainWindow) {
		this.myMainWindow = MainWindow;
		
	}
}
