package View;

 /**
  *  Datatype for Management of the different values of the Levelconfiguration
  *  Metadata of LevelkonfigTypes
  *  @author be7
  */

public enum LevelConfigurationTypes {
	MinPlayer("MinPlayer", 0, DataTypeEnum.IntData, "minPlayers"),
	MaxPlayer("MaxPlayer", 1, DataTypeEnum.IntData, "maxPlayers"),
	MinUnitsCastleConquest("MinUnitsForCastleConquest", 2, DataTypeEnum.IntData, "minUnitsForCastleConquest"),
	MaxUnitsCastleConquest("MaxUnitsForCastleConquest", 3, DataTypeEnum.IntData, "maxUnitsForCastleRecruit"),
	MaxUnitsNonCastle("MaxUnitsNonCastle", 4, DataTypeEnum.IntData, "maxUnitsForNonCastleRecruit"),
	SwordsRec("SwordsCostRec", 5, DataTypeEnum.IntData, "swordsRec"),
	ArchersRec("ArchersCostRec", 6, DataTypeEnum.IntData, "archersRec"),
	KnigthsRec("KnigthsCostRec", 7, DataTypeEnum.IntData, "knightsRec"),
	SwordsUpKeep("SwordsCostUpKeep", 8, DataTypeEnum.IntData, "swordsUpKeep"),
	ArchersUpKeep("ArchersCostUpKeep", 9, DataTypeEnum.IntData, "archersUpKeep"),
	KnightUpKeep("KnightsCostUpkeep",10, DataTypeEnum.IntData, "knightsUpKeep"),
	ResourceDecay("ResourceDecay", 11, DataTypeEnum.DoubleData, "resourceDecay"),
	StarvingChance("StarvingChance", 12, DataTypeEnum.DoubleData, "starvingChance"),
	EggChance("EggChance", 13, DataTypeEnum.DoubleData, "eggChance"),
	DragonKillChance("DragonKillChance", 14, DataTypeEnum.DoubleData, "dragonKillChance");
	
	private int i_index;
	private String S_describtion;
	private DataTypeEnum e_dataType;
	private String jsonName;
	
	LevelConfigurationTypes(String desc, int index, DataTypeEnum dataType, String jsonName)
	{
		this.i_index = index;
		this.S_describtion = desc;
		this.e_dataType = dataType;
		this.jsonName = jsonName;
	}

	public int getIndex() 
	{
		return i_index;
	}

	public String getDescribtion() 
	{
		return S_describtion;
	}
	
	public DataTypeEnum getDataType()
	{
		return e_dataType;
	}
	
	public String getJsonName()
	{
		return jsonName;
	}
}