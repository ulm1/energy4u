package View;

import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import javax.swing.JCheckBox;
/**
 * This class is designed to store and manage the information about one castle in one territory.
 * This class is managed by NodeConfig
 * @author berfin
 */
public class CastleOption extends NodeOption {
	private static final long serialVersionUID = 1L;
	
	JCheckBox checkBox;
	/**
	 * adds the checkBox which can be marked if a terrain should have a castle
	 */
	public CastleOption()
	{
		checkBox = new JCheckBox("Castle");
		
		add(checkBox);
	}

	/**
	 * makes the checkBox visible or invisible
	 */
	@Override
	public void setVisible(boolean value)
	{
		super.setVisible(value);

		checkBox.setVisible(value);
	}
	
	/**
	 * activate or deactivates the checkbox for a castle in a territory
	 * @param castleBuffer: activate or deactivates the checkbox
	 */
	public void setCastle(boolean castleBuffer) 
	{

		checkBox.setSelected(castleBuffer);

	}
	
	/**
	 * adds the stored information to the JSON builder
	 * @param jsonObBuilder: stores temporary information for processing 
	 */

	@Override
	public void getAsJson(JsonObjectBuilder jsonObBuilder) 
	{

		jsonObBuilder.add("castle",(JsonValue) (checkBox.isSelected() ? JsonValue.TRUE : JsonValue.FALSE ));
	}
}
