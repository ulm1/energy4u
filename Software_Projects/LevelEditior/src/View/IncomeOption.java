package View;

import java.awt.GridLayout;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.json.JsonObjectBuilder;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.text.NumberFormatter;

/**
 * This class is designed to store and manage the information about teh income in one territory.
 *	This class is managed by the NodeConfig.
 * @author berfin
 *
 */
public class IncomeOption extends NodeOption {

	private final JLabel s_label = new JLabel("Income: "); 
	private JFormattedTextField s_TextField;
	
	/**
	 * creates the necessary textfields and labels
	 */
	public IncomeOption()
	{
		GridLayout cv_masterLayout = new GridLayout(1, 2);
		
		NumberFormat formatterInt = new DecimalFormat("#");
		NumberFormatter returnFormat = new NumberFormatter(formatterInt);
		returnFormat.setAllowsInvalid(false);
		s_TextField = new JFormattedTextField(returnFormat);
		s_TextField.setValue(0);
		
		setLayout(cv_masterLayout);
		
		add(s_label);
		add(s_TextField);
		
	}
	
	/**
	 * sets the labels and textfields visible
	 */
	@Override
	public void setVisible(boolean value)
	{
		super.setVisible(value);

		s_label.setVisible(value);
		s_TextField.setVisible(value);
		
	}
	
	/**
	 * adds it to the JsonObjectBuilder
	 */
	
	@Override
	public void getAsJson(JsonObjectBuilder jsonObBuilder) {
		Number number = (Number) s_TextField.getValue();
		int value = number.intValue();
		jsonObBuilder.add("income", value);
	}
	
	/**
	 * sets the value of Income
	 * @param incomeBuffer: sets value of Income
	 */
	public void getIncome(int incomeBuffer) {
		
		s_TextField.setValue(incomeBuffer);
		
	}

}
