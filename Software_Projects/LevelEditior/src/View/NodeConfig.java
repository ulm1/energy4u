package View;

import java.awt.GridLayout;
import java.util.Arrays;
import java.util.LinkedList;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonBuilderFactory;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.swing.JPanel;
import Model.myNode;

/**
 * saves the options a node can have
 * @author berfin
 *
 */
public class NodeConfig extends JPanel {
	
	private static final long serialVersionUID = -2552831043195349586L;
	private final int i_amountOfOptions = 7;
	
	private int x;
	private int y;
	private int id;
	
	protected LinkedList<HousesEnum> ListOfHouses;
	
	private NodeOption[] a_Options;
	
	/**
	 * creates a nodeconfiguration
	 * @param myNode
	 */
	public NodeConfig(myNode myNode) 
	{
		this();
		
		x = myNode.getX();
		y = myNode.getY();
		id = myNode.getName();
	}
	/**
	 * has all the possible Options a terrain can have
	 */
	public NodeConfig()
	{
		super();
		
		ListOfHouses = new LinkedList<HousesEnum>(Arrays.asList(HousesEnum.values()));
		
		GridLayout cv_masterLayout = new GridLayout(1,2);
		
		JPanel leftPanel = new JPanel();
		GridLayout cv_leftLayout = new GridLayout(4,1);
		leftPanel.setLayout(cv_leftLayout);
		
		JPanel rightPanel = new JPanel();
		GridLayout cv_rightLayout = new GridLayout(1,1);
		rightPanel.setLayout(cv_rightLayout);
		
		add(leftPanel);
		add(rightPanel);
		
		setLayout(cv_masterLayout);
		
		a_Options = new NodeOption[i_amountOfOptions];
		
		a_Options[0] = new TerrainOption();
		leftPanel.add(a_Options[0]);
		
		a_Options[1] = new CastleOption();
		a_Options[2] = new TavernOption();
		JPanel checkBoxPanel = new JPanel();
		GridLayout checkBoxLayout = new GridLayout(1,2);
		checkBoxPanel.setLayout(checkBoxLayout);
		checkBoxPanel.add(a_Options[1]);
		checkBoxPanel.add(a_Options[2]);
		leftPanel.add(checkBoxPanel);
		
		a_Options[3] = new IncomeOption();
		leftPanel.add(a_Options[3]);
		
		a_Options[4] = new LabelOption();
		a_Options[5] = new HouseOption();
		leftPanel.add(a_Options[5]);
		
		a_Options[6] = new ArmyOptionControl();
		rightPanel.add(a_Options[6]);
		
	}
	/**
	 * creates from a Json Object a nodeconfiguration
	 * @param jsObject
	 */
	public NodeConfig(JsonObject jsObject) 
	{
		this();
		
		if(jsObject == null)
		{
			throw new IllegalArgumentException("This territory seems to be corrupted.");
		}
		
		this.id = jsObject.getInt("id");
		this.x = jsObject.getInt("x");
		this.y = jsObject.getInt("y");
		
		String terrainBuffer = jsObject.getString("terrain");
		((TerrainOption) a_Options[0]).setTerrain(terrainBuffer);
		
		boolean castleBuffer = jsObject.getBoolean("castle");
		((CastleOption) a_Options[1]).setCastle(castleBuffer);
		
		boolean tavernBuffer = jsObject.getBoolean("tavern");
		((TavernOption) a_Options[2]).setTavern(tavernBuffer);
		
		int incomeBuffer = jsObject.getJsonNumber("income").intValue();
		((IncomeOption) a_Options[3]).getIncome(incomeBuffer);
		
		String houseBuffer = jsObject.getString("house");
		((HouseOption) a_Options[5]).setHouse(houseBuffer);
		
		JsonArray JsUnitsBuffer = jsObject.getJsonArray("units");
		((ArmyOptionControl)a_Options[6]).loadJson(JsUnitsBuffer);
		
	}
	/**
	 * sets nodes visible
	 */
	@Override
	public void setVisible(boolean value)
	{
		super.setVisible(value);
		
		for(NodeOption option : a_Options)
		{
			if (option instanceof LabelOption) continue;
			option.setVisible(value);
		}
	}
	
	/**
	 * gets coordinates and id as Json 
	 * @return returns as Json
	 */
	public JsonObjectBuilder getAsJson()
	{
		JsonBuilderFactory factory = Json.createBuilderFactory(null);
		
		JsonObjectBuilder jsonObBuilder = factory.createObjectBuilder();
		
		jsonObBuilder.add("id", id);
		jsonObBuilder.add("x", x);
		jsonObBuilder.add("y", y);
		
		for(NodeOption s_Option : a_Options)
		{
			s_Option.getAsJson(jsonObBuilder);
		}
		
		return jsonObBuilder;
	}

	public int getId()
	{
		return this.id;
	}
	
	public int getXCordinate()
	{
		return this.x;
	}
	
	public int getYCordinate()
	{
		return this.y;
	}
	
	public void triggerChange(ArmyOption selfReference) {
	
	}
	
}
