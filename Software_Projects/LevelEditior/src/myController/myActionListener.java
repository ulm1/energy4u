package myController;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import Model.conecter;
import Model.myNode;
import View.graphikFenster;
import View.layout;

public class myActionListener implements ActionListener {

	layout l;
	myController c;
	public graphikFenster gf;

	public myActionListener(layout l, graphikFenster gf) {
		this.l = l;
		this.c = l.getC();
		this.gf = gf;
		gf.setMyLayout(l);
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == l.erase()) {

			l.triggerClear();
			
		}
			else {

			int c = l.getC().getN().size();
			for (int i = 0; i < c; i++) {
				for (int j = 0; j < c; j++) {

					if (j != i) {
						if (e.getSource().equals(l.getMatrixButton()[i][j])) {
							if (l.getMatrixButton()[i][j].isSelected()) {
								this.l.getMatrixButton()[j][i].setSelected(true);
							} else {
								this.l.getMatrixButton()[j][i].setSelected(false);
							}

						}

					}
				}

			}
			setColorToGraph();
		}
	}

	public void setColorToGraph() {
		int edges = c.getN().size();
		c.setSection(new ArrayList<myNode>());
		boolean[][] m = l.setMatix();
		myNode start;
		myNode end;
		c.setConect(new ArrayList<conecter>());
		ArrayList<conecter> r = new ArrayList<>();
		boolean collison = false;
		for (int i = 0; i < edges; i++) {
			start = c.getname(i);
			ArrayList<myNode> ccc = new ArrayList<>();
			for (int j = 0; j <= i; j++) {
				if (m[i][j]) {
					end = c.getname(j);
					ccc.add(end);
					start.setLinkedNode(ccc);
					r.add(new conecter(start, end, l.getC()));

				}
			}
			c.setConect(r);

		}
		;

		if (!collison) {
			c.setSection(new ArrayList<myNode>());
			c.getShape();

		}
		l.getFenster().repaint();
	}

}
