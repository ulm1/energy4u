package myController;

import java.awt.Window;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import Model.conecter;
import Model.myNode;
import Model.shape;
import View.graphikFenster;
import View.layout;
import Zyklus.Zyklus2;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonBuilderFactory;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;

public class myController {

	public ArrayList<myNode> n ;
	static ArrayList<conecter> conect;
	static ArrayList<myNode> section;
	ArrayList<shape> shape;
	static HashMap matrix;
	
	layout l;
	graphikFenster graphikFenster;
	
	public void setGraphikFenster(graphikFenster graphikFenster) {
		this.graphikFenster = graphikFenster;
	}
	
	public graphikFenster getGraphikFenster() {
		return graphikFenster;
	}
	
	
	
    public myController() {
        n = new ArrayList<>();
        conect = new ArrayList<>();
        shape = new ArrayList<>();
        section = new ArrayList<>();
        matrix = new HashMap();
     
        l = new layout(this);
        
    }
	

	// gibt den knoten mit dem dazugehörigen namen an
	
	public myNode getname(int name) {
		name++;
		myNode node = null;
		for (int i = 0; i < n.size(); i++) {
			if (n.get(i).getName() == name) {

				node = n.get(i);
			}
		}
		return node;
	}
	
	
	public ArrayList<shape> getShape() {
		ArrayList<myNode> n = 	l.getC().getN();
		for(int i = 0; i < n.size(); i++){
			myNode n2 = n.get(i);
			ArrayList<myNode> a = n2.getLinkedNode();
			
		} 
	
	shape = new ArrayList<>();
    ArrayList<myNode> l = new ArrayList<>();
	displayShapes(n.get(0),l);	
	
	return shape;
}

	public void displayShapes(myNode i, ArrayList<myNode> r){
		
		getList(l.setMatix(), getMyNodes());
		
	}
	
	
	public void getList(boolean amatrix[][],String Nodes[]){
		ArrayList<Integer[]> e = new ArrayList<>();
		matrix = new HashMap();
		Zyklus2 e2 = new Zyklus2(amatrix,Nodes);
		List c = e2.get_basics();
		for(int i = 0;i<c.size();i++){
			List c2 = (List) c.get(i);
			Integer[] array = new Integer[c2.size()];
			for(int j=0;j<c2.size();j++){
				String n = (String) c2.get(j);
				if(c2.size()>2){
					array[j]=Integer.parseInt(n);
				}
			}
			if(array.length>2){
				String aa = "";
				for(int j=0;j<array.length;j++){
					aa+=array[j] + "-";
				}
				matrix.put(aa, array);
			}
		}
		
	}
	
	
	
	
	
	public String[] getMyNodes(){
		
		String n[] = new String[this.n.size()];
		for(int i = 0; i < this.n.size();i++){
			n[i] = i + "";
			
		
			
		}
		return n;
	}
	

	public ArrayList<shape> getPolygonSection() {
		// TODO Auto-generated method stub
		ArrayList<shape> s = new ArrayList<>();
		Set s1 = matrix.entrySet();
		Iterator i = s1.iterator();
		
		while(i.hasNext()){
			Map.Entry x = (Map.Entry)i.next();
			shape w = new shape((Integer[]) x.getValue());
			s.add(w);
		}
		
		return s;
	}
	
	
	public ArrayList<myNode> getN() {
		return n;
	}


	public void setN(ArrayList<myNode> n) {
		this.n = n;
	}


	public  ArrayList<conecter> getConect() {
		return conect;
	}


	public  void setConect(ArrayList<conecter> conect) {
		this.conect = conect;
	}

	


	public  ArrayList<myNode> getSection() {
		return section;
	}


	public  void setSection(ArrayList<myNode> section) {
		this.section = section;
	}


	public  HashMap getMatrix() {
		return matrix;
	}


	public  void setMatrix(HashMap matrix) {
		myController.matrix = matrix;
	}


	public void setShape(ArrayList<shape> shape) {
		this.shape = shape;
	}
	
	
	public static final String PROTOCOL_VERSION = "1522460114";
	public static JsonBuilderFactory factory = Json.createBuilderFactory(null);
	public static JsonObject Levelkonfiguration(int minplayers,int maxPlayers,boolean edge,int minUnitsForCastleConquest,int maxUnitsForCastleRecruit,int maxUnitsForNonCastleRecruit ,int swords, int  archers, int knights,int swords1, int  archers1, int knights1,double starvingChance,double resourceDecay, double eggChance, double dragonKillChance) 
	{
		long currentTime = System.currentTimeMillis(); 
		JsonObject jsonOb = factory.createObjectBuilder()
				.add("levelcfg", factory.createObjectBuilder()
						.add("meta",factory.createObjectBuilder()
								.add("name", "Testbattle")
								.add("creationDate", PROTOCOL_VERSION)
								.add("Creator", "Ronnie"))
						.add("game", factory.createObjectBuilder()
						.add("minPlayers", minplayers)
						.add("maxPlayers", maxPlayers)
						.add("edges", factory.createArrayBuilder())
					.add("minUnitsForCastleConquest",minUnitsForCastleConquest)
					.add("maxUnitsForCastleRecruit", maxUnitsForCastleRecruit)
					.add("maxUnitsForNonCastleRecruit", maxUnitsForNonCastleRecruit)
					.add("recruitmentCost",factory.createObjectBuilder()
						.add("swords",swords)
						.add("archers",archers)
						.add("knights",knights))
					.add("upkeepCost",factory.createObjectBuilder()
							.add("swords",swords1)
							.add("archers",archers1)
							.add("knights",knights1))
						.add("starvingChance", starvingChance)
						.add("resourceDecay", resourceDecay)
						.add("eggChance", eggChance)
						.add("dragonKillChance", dragonKillChance))
					.add("territories", factory.createArrayBuilder())
				.add("additions",  factory.createArrayBuilder())
			.add("table", factory.createArrayBuilder())).build();
		System.out.println(jsonOb);
		return jsonOb;

	}

	public static JsonBuilderFactory factory1 = Json.createBuilderFactory(null);
	public static JsonObject Versorgungsphase() {
		long currentTime = System.currentTimeMillis();
		JsonObject jsonOb2 = factory1.createObjectBuilder()
				.add("header",
						factory1.createObjectBuilder().add("UID", "null").add("name", "null")
								.add("timestamp", currentTime).add("messageType", "supplyPhase"))

				// JsonObjectBuilder jsonObb = Json.createObjectBuilder();
				.add("supplyResults", factory1.createArrayBuilder()
								
								).build();
		System.out.println(jsonOb2);
		return jsonOb2;
	
	}
	
	public static JsonBuilderFactory factory2 = Json.createBuilderFactory(null);
	public static JsonObject Partiekonfiguration(int numberOfRoundsBeforeWinter,int timePerPhase,double winterLossChance,int maxSecondsBeforeWinter) {
		long currentTime = System.currentTimeMillis();
		JsonObject jsonOb2 = factory2.createObjectBuilder()
				.add("matchcfg",
						factory2.createObjectBuilder().add("numberOfRoundsBeforeWinter",numberOfRoundsBeforeWinter ).add("timePerPhase", timePerPhase)
								.add("winterLossChance", winterLossChance).add("maxSecondsBeforeWinter", maxSecondsBeforeWinter)).build();
		System.out.println(jsonOb2);
		return jsonOb2;
	
	}

}
