package View;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import Model.myNode;
import myController.myActionListener;
import myController.myController;

public class layout extends JFrame {
	// creates the buttons
	JButton next;
	JButton clear;
	JButton Matrix;
	JButton speichern;
	JButton back;
	JButton save;
	JButton SaveGraph;
	static JPanel right;
	  
	int minplayers;
	int maxPlayers;
	boolean edge;
	int maxUnitsForCastle;
	int maxUnitsForNonCastle;
	int swords;
	int archers;
	int knights;
	int swordsu;
	int archersu;
	int knightsu;
	int starvingChance;
	int resourceDecay; 
	ArrayList terrotitores;
	int numberOfRoundsBeforeWinter;
	int timePerPhase;
	double winterLossChance;
	int maxSecondsBeforeWinter;
	int minUnitsForCastleConquest;
	
	boolean castle;
	int income;
	String lable;
	String house;
	String allegiance;
	int existingSwords;
	int existingArchers;
	int existingKnights;
	String terrain;
	double percentages;
	

	graphikFenster fenster;
	myActionListener Lp;
	myController c;
	JScrollPane scroll;
	JRadioButton[][] matrixButton;
	// myActionListener function;
	JPanel panel;
	JFrame start;
	JFrame partie1;
	JFrame configDatei;

	static JRadioButton rb1;
	JTextField Bebauung;
	public static boolean graphsaved = false;
	 static JTabbedPane tab;
	 static JPanel p;
		
	

	public layout(myController c) {
		
		//create Levelkonfiguration page
				configDatei = new JFrame ("Levelkonfiguration");
				
				configDatei.setSize(1000,1000);
				configDatei.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				
				configDatei.setLayout(null);
				
				JButton speichern = new JButton("Save");
				speichern.setBounds(300,900, 150, 40);
				configDatei.add(speichern);
				
				
				JLabel configKo = new JLabel("Levelkonfiguration");
				JLabel min = new JLabel ("minPlayer");
				JLabel max = new JLabel ("maxPlayer");
				JLabel edges = new JLabel ("edges");
				JLabel minUnits = new JLabel ("minUnitsForCastleConquest");
				
				JLabel maxUnits = new JLabel("maxUnitsForCastleConquest");
				JLabel NonCastle = new JLabel ("maxUnitsNonCastle");
				JLabel mentCost = new JLabel ("mentCost");
				JLabel sw = new JLabel ("swords");
				JLabel ar = new JLabel ("archers");
				JLabel kn = new JLabel ("knights");
				JLabel upkeep = new JLabel ("upkeepCost");
				JLabel us = new JLabel ("swords");
				JLabel ua = new JLabel ("archers");
				JLabel uk = new JLabel ("knights");
				JLabel sc = new JLabel ("starvingChance");
			
				JLabel resourseD = new JLabel ("resourceDecay");
				
				
				configDatei.add(min);
				min.setBounds(300, 100, 150, 25);
				JTextField min1= new JTextField("");
				min1.setBounds(600, 100, 150, 25);
				configDatei.add(min1);
				
				configDatei.add(max);
				max.setBounds(300, 150, 150, 25);
				JTextField max1= new JTextField("");
			
				
				
				max1.setBounds(600, 150, 150, 25);
				configDatei.add(max1);
				
				configDatei.add(edges);
				edges.setBounds(300, 200, 150, 25);
				JTextField edges1= new JTextField("");
				edges1.setBounds(600, 200, 150, 25);
				configDatei.add(edges1);
				
				configDatei.add(minUnits);
				minUnits.setBounds(300, 250, 170, 25);
				JTextField minUnits1= new JTextField("");
				minUnits1.setBounds(600, 250, 150, 25);
				configDatei.add(minUnits1);
				
				configDatei.add(maxUnits);
				maxUnits.setBounds(300, 300, 170, 25);
				JTextField maxUnits1= new JTextField("");
				maxUnits1.setBounds(600, 300, 150, 25);
				configDatei.add(maxUnits1);
				
				
				configDatei.add(NonCastle);
				NonCastle.setBounds(300, 350, 170, 25);
				JTextField NonCastle1= new JTextField("");
				NonCastle1.setBounds(600, 350, 150, 25);
				configDatei.add(NonCastle1);
				
				configDatei.add(mentCost);
				mentCost.setBounds(300, 400, 170, 25);
				JTextField mentCost1= new JTextField("");
				mentCost1.setBounds(600, 400, 150, 25);
				configDatei.add(mentCost1);
				
				configDatei.add(sw);
				sw.setBounds(300, 450, 170, 25);
				JTextField sw1= new JTextField("");
				sw1.setBounds(600, 450, 150, 25);
				configDatei.add(sw1);
				
				configDatei.add(ar);
				ar.setBounds(300, 500, 170, 25);
				JTextField ar1= new JTextField("");
				ar1.setBounds(600, 500, 150, 25);
				configDatei.add(ar1);
				
				configDatei.add(kn);
				kn.setBounds(300, 550, 170, 25);
				JTextField kn1= new JTextField("");
				kn1.setBounds(600, 550, 150, 25);
				configDatei.add(kn1);
				
				
				
				configDatei.add(upkeep);
				upkeep.setBounds(300, 600, 150, 25);
			
				
				configDatei.add(us);
				us.setBounds(300, 650, 170, 25);
				JTextField us1= new JTextField("");
				us1.setBounds(600, 650, 150, 25);
				configDatei.add(us1);
				
				configDatei.add(uk);
				uk.setBounds(300, 700, 170, 25);
				JTextField uk1= new JTextField("");
				uk1.setBounds(600, 700, 150, 25);
				configDatei.add(uk1);
				
				
				configDatei.add(ua);
				ua.setBounds(300, 750, 170, 25);
				JTextField ua1= new JTextField("");
				ua1.setBounds(600, 750, 150, 25);
				configDatei.add(ua1);
				
				

				configDatei.add(resourseD);
				resourseD.setBounds(300, 800, 150, 25);
				JTextField resourseD1= new JTextField("");
				resourseD1.setBounds(600, 800, 150, 25);
				configDatei.add(resourseD1);
				
				configDatei.add(sc);
				sc.setBounds(300, 850, 150, 25);
				JTextField sc1= new JTextField("");
				sc1.setBounds(600, 850, 150, 25);
				configDatei.add(sc1);
				
				speichern.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						maxPlayers =Integer.parseInt(max1.getText());
						minplayers = Integer.parseInt(min1.getText());
						//edge = edges1.get;#
						minUnitsForCastleConquest =Integer.parseInt(minUnits1.getText());
						maxUnitsForCastle = Integer.parseInt(maxUnits1.getText());
						maxUnitsForNonCastle = Integer.parseInt(NonCastle1.getText());
						edge = Boolean.parseBoolean(edges1.getText());
						swords =Integer.parseInt(sw1.getText());
						archers = Integer.parseInt(ar1.getText());
						knights = Integer.parseInt(kn1.getText());
						swordsu =Integer.parseInt(us1.getText());
						archersu = Integer.parseInt(ua1.getText());
						knightsu = Integer.parseInt(uk1.getText());
						starvingChance = Integer.parseInt(sc1.getText());
						resourceDecay = Integer.parseInt(resourseD1.getText());
						
						
						
						partie1.setVisible(false);
						setVisible(true);

					}
				});
					
				
				//create partiekonfiguration page
				partie1 = new JFrame ("Partiekonfiguration");
				
			    partie1.setSize(1000,1000);
				partie1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				
				partie1.setLayout(null);
				
		

				
				
			

				JLabel runde = new JLabel ("Round");
				JLabel rundeZeit = new JLabel ("Time per Round");
				JLabel winter = new JLabel ("Winter loss Chance");
				JLabel winterNach = new JLabel ("Max seconds before Winter");
				

				
				partie1.add(runde);
				runde.setBounds(300, 250, 150, 25);
				JTextField runde1= new JTextField("");
				runde1.setBounds(600, 250, 150, 25);
				partie1.add(runde1);
				
				partie1.add(rundeZeit);
				rundeZeit.setBounds(300, 300, 150, 25);
				JTextField rundeZeit1= new JTextField("");
				rundeZeit1.setBounds(600, 300, 150, 25);
				partie1.add(rundeZeit1);
				
				
				partie1.add(winter);
				winter.setBounds(300, 350, 150, 25);
				JTextField winter1= new JTextField("");
				winter1.setBounds(600, 350, 150, 25);
				partie1.add(winter1);
				
				partie1.add(winterNach);
				winterNach.setBounds(300, 400, 170, 25);
				JTextField winterNach1= new JTextField("");
				winterNach1.setBounds(600, 400, 150, 25);
				partie1.add(winterNach1);
				partie1.setVisible(false);
				
				
				JButton speichern2 = new JButton("Save");
				speichern2.setBounds(300,700, 150, 40);
				speichern2.addActionListener(new ActionListener() {

		
					
					@Override
					public void actionPerformed(ActionEvent e) {
						if (!Desktop.isDesktopSupported()) {
							return;
						}
						final JFileChooser fileChooser = new JFileChooser(new File ("c.\\"));
						FileFilter f = new FileNameExtensionFilter("json file","json");
						fileChooser.setFileFilter(f);
						int result = fileChooser.showSaveDialog(null);
						// File file = new File("team20");
						if (result == JFileChooser.APPROVE_OPTION) {
							File fe = fileChooser.getSelectedFile();
						JsonObject file = myController.Partiekonfiguration(Integer.parseInt(runde1.getText()),Integer.parseInt(rundeZeit1.getText()),Float.parseFloat(winter1.getText()),Integer.parseInt( winterNach1.getText()));
						partie1.setVisible(false);
						setVisible(true);
						
					try {
						FileWriter fw = new FileWriter(fe.getPath());
						fw.write(file.toString());
						fw.flush();
						fw.close();
					}
					catch(Exception e1){
						
						System.out.println("error");
					}
					}
					}
				});
					
				partie1.add(speichern2);		

				
		right = new JPanel();
		right.setLayout(null);

		right.setBackground(Color.red);
		
		p = new JPanel();
		p.setLayout(null);

		p.setBackground(Color.blue);
	    tab = new JTabbedPane (JTabbedPane.TOP,JTabbedPane.SCROLL_TAB_LAYOUT ); 
		
	
		
		

		// create start page;
		start = new JFrame("Leveleditor");

		start.setSize(1000, 1000);
		start.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JButton open = new JButton("Open LevelEditor");
		JButton open2 = new JButton("Open Partiekonfiguration");
		JButton newL = new JButton("New");
		JButton Exit = new JButton("Exit");
		 save = new JButton("Save");
		start.setLayout(null);

		start.add(open);
		start.add(open2);
		start.add(newL);
		start.add(Exit);

		open.setBounds(400, 300, 150, 40);
		open2.setBounds(400, 400, 150, 40);
		newL.setBounds(400, 500, 150, 40);
		Exit.setBounds(400, 600, 150, 40);

		newL.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setVisibility();

			}

		});

		Exit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);

			}

		});
		
		
		open2.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!Desktop.isDesktopSupported()) {
					return;
				}
				final JFileChooser fileChooser = new JFileChooser(new File ("c.\\"));
				FileFilter f = new FileNameExtensionFilter("json file","json");
				fileChooser.setFileFilter(f);
				int result = fileChooser.showOpenDialog(null);
				if (result == JFileChooser.APPROVE_OPTION) {
				
					
					
					File json2InputFile = fileChooser.getSelectedFile();
					
					InputStream iss = null;
					try {
						iss = new FileInputStream(json2InputFile);
					} catch (FileNotFoundException e1) {

						e1.printStackTrace();
					}
					JsonReader reader3 = Json.createReader(iss);
					JsonObject jsonOb1 = reader3.readObject();
					
				
					int json =jsonOb1.getJsonObject("matchcfg").getInt("numberOfRoundsBeforeWinter");
					Integer.toString(json);
					runde1.setText(Integer.toString(json));
					
					int json2 =jsonOb1.getJsonObject("matchcfg").getInt("timePerPhase");
					Integer.toString(json2);
					rundeZeit1.setText(Integer.toString(json2));
					
					int json3 =jsonOb1.getJsonObject("matchcfg").getInt("winterLossChance");
					Integer.toString(json3);
					winter1.setText(Integer.toString(json3));
					
					int json4 =jsonOb1.getJsonObject("matchcfg").getInt("maxSecondsBeforeWinter");
					Integer.toString(json4);
					winterNach1.setText(Integer.toString(json4));
							
					partie1.setVisible(true);
					
					System.out.println(jsonOb1);
						
			}
			}
			});
		
		
		
		
		
		

		open.addActionListener(new ActionListener() {
	
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!Desktop.isDesktopSupported()) {
					return;
				}
				final JFileChooser fileChooser = new JFileChooser(new File ("c.\\"));
				FileFilter f = new FileNameExtensionFilter("json file","json");
				fileChooser.setFileFilter(f);
				int result = fileChooser.showOpenDialog(null);
				if (result == JFileChooser.APPROVE_OPTION) {
				
					
					
					File json2InputFile = fileChooser.getSelectedFile();
					
					InputStream iss = null;
					try {
						iss = new FileInputStream(json2InputFile);
					} catch (FileNotFoundException e1) {

						e1.printStackTrace();
					}
					JsonReader reader3 = Json.createReader(iss);
					JsonObject jsonOb1 = reader3.readObject();
							
					System.out.println(jsonOb1);					
			}
			}
			});
		
		
		save.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!Desktop.isDesktopSupported()) {
					return;
				}
				final JFileChooser fileChooser = new JFileChooser(new File ("c.\\"));
				FileFilter f = new FileNameExtensionFilter("json file","json");
				fileChooser.setFileFilter(f);
				int result = fileChooser.showSaveDialog(null);
				// File file = new File("team20");
				if (result == JFileChooser.APPROVE_OPTION) {
					File fe = fileChooser.getSelectedFile();
				JsonObject file = myController.Levelkonfiguration(minplayers,maxPlayers,edge,minUnitsForCastleConquest,
						maxUnitsForCastle,maxUnitsForNonCastle,swords,archers,knights,swordsu,archersu,knightsu,starvingChance,
						resourceDecay);

				
				
			try {
				FileWriter fw = new FileWriter(fe.getPath());
				fw.write(file.toString());
				fw.flush();
				fw.close();
			}
			catch(Exception e1){
				
				System.out.println("error");
			}
			}
			}
		});
		
		
		

		start.setVisible(true);

		this.setTitle("LevelEditor");
		this.setSize(1500, 1500);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLayout(null);
		this.c = c;

		JMenuBar menuBar = new JMenuBar();
		JMenu menu = new JMenu("File");
		menuBar.add(menu);

		// a group of JMenuItems
		JMenuItem menuItem = new JMenuItem("Open");
		JMenuItem menuItem3 = new JMenuItem("Levelkonfiguration");
		JMenuItem menuItem2 = new JMenuItem("Partiekonfiguration");
		menu.add(menuItem2);
		menu.add(menuItem3);

		this.setJMenuBar(menuBar);

		// function = new myActionListener(this);

		// get my current functions from the graphiFenster class

		fenster = new graphikFenster(c);
		c.setGraphikFenster(fenster);

	

		// declare the buttons
		back = new JButton("BACK");
		back.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				goBack();

			}

			private void goBack() {
				setVisible(false);
				start.setVisible(true);

			}
		});

		next = new JButton("EXIT");
		next.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);

			}

		});
		Matrix = new JButton("MATRIX");
		clear = new JButton("CLEAR");
		SaveGraph = new JButton("SAVE GRAPH");
		Lp = new myActionListener(this, c.getGraphikFenster());

		// functions of the buttons

		Matrix.addActionListener(Lp);
		clear.addActionListener(Lp);
		 clear.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					 tab.setVisible(false);
				}
		 });


		// add it to the JFrame
		add(save);
		save.setBounds(1100, 900, 150, 40);
		add(next);
		next.setBounds(700, 900, 150, 40);
		add(back);
		back.setBounds(900, 900, 150, 40);
		add(Matrix);
		Matrix.setBounds(490, 450, 150, 40);
		add(clear);
		clear.setBounds(40, 450, 150, 40);
		fenster.setBounds(40, 20, 600, 400);
		SaveGraph.setBounds(260, 450, 150, 40);
		add(SaveGraph);
		
		
		Bebauung = new JTextField ("");
		Bebauung.setBounds(780, 215, 100, 25);
		Bebauung.setVisible(false);
		add(Bebauung);
		
		
		
		add(fenster);
		addMatix(0);
		add(tab);
		
		
			SaveGraph.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				graphsaved = true;
				 tab.setVisible(true);

				}
		
			
		});
		
			menuItem2.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					
					setVisible(false);
					partie1.setVisible(true);
				}
				
			
			
			 }
			 );
			

			
			menuItem3.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						
						setVisible(false);
						configDatei.setVisible(true);
					}
					
				
				
				 }
				 );
		
		
	
		

		this.setVisible(false);

	}
	
	
	
	
	
	
	
	
	public void myButtonsandText(){
		
		

		
		
	}

	protected void setVisibility() {
		start.setVisible(false);
		this.setVisible(true);

	}

	// Creates my Matrix
	public boolean[][] setMatix() {

		// toDO
		int coulumns = c.getN().size();
		boolean[][] mybool = new boolean[coulumns][coulumns];
		for (int i = 0; i < coulumns; i++) {
			for (int j = 0; j < coulumns; j++) {
				if (matrixButton != null) {
					if (matrixButton[i][j].isSelected()) {
						mybool[i][j] = true;
					} else {
						mybool[i][j] = false;
					}
				}
			}
		}

		return mybool;
	}

	public void addMatix(int e) {

		if (scroll != null) {
			this.remove(this.scroll);
		}

		panel = new JPanel();

		// setzte die farbe f�r mein unteres Panel

		panel.setBackground(Color.white);

		// damit die buttons richtig geordnet sind

		panel.setLayout(null);
		panel.setPreferredSize(new Dimension(20 + MatrixSize(), 20 + MatrixSize()));
		this.matrixButton = new JRadioButton[e][e];
		for (int i = 0; i < e; i++) {
			for (int j = 0; j < e; j++) {

				// Nummer im oberen Bereich aufz�hlen
				if (j == 0) {
					JLabel headNumbers = new JLabel((i + 1) + "");
					panel.add(headNumbers);

					headNumbers.setBounds(35 + getCenter() + 40 * i, getCenter() + 40 * j, 40, 40);

				}
				// horizontale Aufz�hlung

				if (i == 0) {
					JLabel horizontalNumbers = new JLabel((j + 1) + "");
					this.panel.add(horizontalNumbers);
					horizontalNumbers.setBounds(getCenter() + 40 * i, getCenter() + 40 + 40 * j, 60, 60);
				}
				// kleine Panel rechts f�r Button setzen
				this.matrixButton[i][j] = new JRadioButton();
				panel.add(this.matrixButton[i][j]);

				this.matrixButton[i][j].setBounds(getCenter() + 20 + 40 * i, getCenter() + 40 + 40 * j, 60, 60);
				this.matrixButton[i][j].setBackground(Color.yellow);
				this.matrixButton[i][j].setHorizontalAlignment(SwingConstants.CENTER);

				if (j >= i) {
					this.matrixButton[i][j].setEnabled(false);

				} else {
					matrixButton[i][j].addActionListener(Lp);
				}
			}
		}
		scroll = new JScrollPane(panel);
		add(scroll, 0);
		// unteresFenster Position
		scroll.setBounds(40, 500, 600, 400);
		scroll.updateUI();

	}

	private int getCenter() {
		int numVertices = this.c.getN().size();
		if (400 - (numVertices * 40) <= 0) {
			return 0;
		}
		return ((400 - (numVertices * 40)) / 2);
	}

	// the Size of the Matrix

	int MatrixSize() {
		int n = this.c.getN().size();
		if ((n * 40) <= 480) {
			return 480;
		}
		return ((n * 40));
	}

	public JButton getAMatrix() {
		return Matrix;
	}

	public JRadioButton[][] getMatrixButton() {
		return matrixButton;
	}

	public JButton erase() {
		graphsaved = false;
		return clear;
	}

	public myController getC() {
		return c;
	}

	public graphikFenster getFenster() {
		return fenster;
	}
	

	public JButton getSaveGraph() {
		return SaveGraph;
	}





static ArrayList<JPanel> jp = new ArrayList<>();


	public static  void createNodeB(int i) {
		
		
		JLabel label = new JLabel("Houses");
		
		JLabel label2 = new JLabel("Terrains");
		JLabel label3 = new JLabel("�allegiance�");
		JLabel label4 = new JLabel ("Income");
		JLabel label5 = new JLabel ("Swordsman");
		JLabel label6 = new JLabel ("Knight");
		JLabel label7 = new JLabel ("Archer");
		

		String[] houses = {"Lanister","Stark","Baratheon","Targaryen","Tyrell","Graufreud","Arryns","Martell","Bolton"};
		String[] terraints = {"Desert","Grasslands","Forest","Swamp","Mountain"};

		JComboBox HList = new JComboBox(houses);
		HList.setSelectedIndex(4);
		
		
		JComboBox HListA = new JComboBox(houses);
		HListA.setSelectedIndex(4);
		
		
		
		HList.setBounds(700, 70, 100, 20);
		
		JComboBox tList = new JComboBox(terraints);
		tList.setSelectedIndex(4);
	

		tList.setBounds(700, 130, 100, 20);

	
		

		//add(label);
		label.setBounds(700, 1, 100, 100);
	
		label2.setBounds(700, 550, 50, 50);
		
		
	
		label3.setBounds(700, 200, 50, 50);
		
	
		label4.setBounds(700, 250, 50, 50);
		JTextField Einkommen = new JTextField ("");
		Einkommen.setMaximumSize(new Dimension(100,Einkommen.getMinimumSize().height));
	
		
	
		label5.setBounds(700, 300, 80, 50);
		JTextField Schwertk�mpfer= new JTextField("");
		Schwertk�mpfer.setMaximumSize(new Dimension(100,Schwertk�mpfer.getMinimumSize().height));

		
	
		label6.setBounds(700, 350, 50, 50);
		JTextField Ritter= new JTextField("");
		Ritter.setMaximumSize(new Dimension(100,Ritter.getMinimumSize().height));

		
		
		label7.setBounds(700, 400, 50, 50);
		JTextField Bogensch�tze= new JTextField("");
		Bogensch�tze.setMaximumSize(new Dimension(100,Bogensch�tze.getMinimumSize().height));
		
		JTextField Bebauung= new JTextField("");
		Bebauung.setMaximumSize(new Dimension(100,Bebauung.getMinimumSize().height));
		

        //System.out.print(i);
		if( i >0){
	     jp.add(new JPanel());
		 tab.addTab("" + i, jp.get(i-1));
		 jp.get(i-1).setBackground(Color.lightGray);
		// jp.get(i-1).setSize(500,500);
		 jp.get(i-1).setLayout(new BoxLayout( jp.get(i-1), BoxLayout.PAGE_AXIS));
	
		jp.get(i-1).add(label);
		label.setBounds(700, 1, 100, 100);
		 
		HList.setBounds(700, 70, 50, 50);
		 jp.get(i-1).add(HList);
		 
		 jp.get(i-1).add(label2);
		 
		 jp.get(i-1).add(tList);
		 tList.setBounds(700, 300, 50, 50);
		 jp.get(i-1).add(label3);
		 jp.get(i-1).add(HListA);
		 HListA.setBounds(700, 300, 50, 50);
		 
		
		 jp.get(i-1).add(label4);
		 jp.get(i-1).add(Einkommen);
		 jp.get(i-1).add(label5);
		 jp.get(i-1).add(Schwertk�mpfer);
		 jp.get(i-1).add(label6);
		 jp.get(i-1).add(Ritter);
		 jp.get(i-1).add(label7);
		 jp.get(i-1).add(Bogensch�tze);
		
		 
		 
		
		 tab.setVisible(false);
	
		 tab.setBounds(700, 1, 600, 600);
		

        
		
	}
	}

}
