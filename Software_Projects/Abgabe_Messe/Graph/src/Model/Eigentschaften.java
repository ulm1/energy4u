package Model;

public class Eigentschaften {
	
	
	String House;
	String terraints;
	String alliance;
	int Income;
	int swordsman;
	int archer;
	int knight;
	
	
	public Eigentschaften(String House,String t,String al,int i, int s, int k, int  a){
		this.House= House;
		this.terraints = t;
		this.alliance = al;
		this.Income =i;
		this.swordsman = s;
		this.knight = k;
		this.archer = a;
		
	}


	public String getHouse() {
		return House;
	}


	public void setHouse(String house) {
		House = house;
	}


	public String getTerraints() {
		return terraints;
	}


	public void setTerraints(String terraints) {
		this.terraints = terraints;
	}


	public String getAlliance() {
		return alliance;
	}


	public void setAlliance(String alliance) {
		this.alliance = alliance;
	}


	public int getIncome() {
		return Income;
	}


	public void setIncome(int income) {
		Income = income;
	}


	public int getSwordsman() {
		return swordsman;
	}


	public void setSwordsman(int swordsman) {
		this.swordsman = swordsman;
	}


	public int getArcher() {
		return archer;
	}


	public void setArcher(int archer) {
		this.archer = archer;
	}


	public int getKnight() {
		return knight;
	}


	public void setKnight(int knight) {
		this.knight = knight;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((House == null) ? 0 : House.hashCode());
		result = prime * result + Income;
		result = prime * result + ((alliance == null) ? 0 : alliance.hashCode());
		result = prime * result + archer;
		result = prime * result + knight;
		result = prime * result + swordsman;
		result = prime * result + ((terraints == null) ? 0 : terraints.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Eigentschaften other = (Eigentschaften) obj;
		if (House == null) {
			if (other.House != null)
				return false;
		} else if (!House.equals(other.House))
			return false;
		if (Income != other.Income)
			return false;
		if (alliance == null) {
			if (other.alliance != null)
				return false;
		} else if (!alliance.equals(other.alliance))
			return false;
		if (archer != other.archer)
			return false;
		if (knight != other.knight)
			return false;
		if (swordsman != other.swordsman)
			return false;
		if (terraints == null) {
			if (other.terraints != null)
				return false;
		} else if (!terraints.equals(other.terraints))
			return false;
		return true;
	}
	
	

}
