package myController;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import Model.conecter;
import Model.myNode;
import View.graphikFenster;
import View.layout;

public class myActionListener implements ActionListener {

	layout l;
	myController c;
	graphikFenster gf;

	public myActionListener(layout l, graphikFenster gf) {
		this.l = l;
		this.c = l.getC();
		this.gf = gf;
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == l.erase()) {

			c.setN(new ArrayList<myNode>());
			c.setConect(new ArrayList<conecter>());
			c.setSection(new ArrayList<myNode>());
			l.getFenster().repaint();
			l.addMatix(0);
			
			gf.getCheck().getN().clear();
			gf.getCheck().getConect().clear();
			gf.getCheck().getPolygonSection().clear();
			gf.getCheck().getSection().clear();
			gf.getCheck().getPolygonSection().clear();
			gf.getCheck().getMatrix().clear();
		} else if (e.getSource() == l.getAMatrix()) {
			l.addMatix(l.getC().getN().size());

		} else {

			int c = l.getC().getN().size();
			for (int i = 0; i < c; i++) {
				for (int j = 0; j < c; j++) {

					if (j != i) {
						if (e.getSource().equals(l.getMatrixButton()[i][j])) {
							if (l.getMatrixButton()[i][j].isSelected()) {
								this.l.getMatrixButton()[j][i].setSelected(true);
							} else {
								this.l.getMatrixButton()[i][j].setSelected(false);
							}

						}

					}
				}

			}
			setColorToGraph();
		}
	}

	public void setColorToGraph() {
		int edges = c.getN().size();
		c.setSection(new ArrayList<myNode>());
		boolean[][] m = l.setMatix();
		myNode start;
		myNode end;
		c.setConect(new ArrayList<conecter>());
		ArrayList<conecter> r = new ArrayList<>();
		boolean collison = false;
		for (int i = 0; i < edges; i++) {
			start = c.getname(i);
			ArrayList<myNode> ccc = new ArrayList<>();
			for (int j = 0; j < edges; j++) {
				if (m[i][j] && i < j) {
					end = c.getname(j);
					ccc.add(end);
					start.setLinkedNode(ccc);
					r.add(new conecter(start, end, l.getC()));

				}
			}
			c.setConect(r);

		}
		;

		if (!collison) {
			c.setSection(new ArrayList<myNode>());
			c.getShape();

		}
		l.getFenster().repaint();
	}

}
