package Zyklus;

import java.util.Vector;

public class Adjazenzliste {
	
	public static int [] [] get_Adjazenzliste(boolean[][] adjazenzmatrix) {
		int[][] adliste= new int [adjazenzmatrix.length] [];
		for (int i=0; i<adjazenzmatrix.length;i++) {
			Vector eins= new Vector();
			for(int j=0;j< adjazenzmatrix[i].length;j++){
				if(adjazenzmatrix[i][j]) {
					eins.add(new Integer(j));
				}
			}
			
			adliste[i]= new int [eins.size()];
			for(int j=0;j<eins.size(); j++) {
				Integer zwei =(Integer) eins.get(j);
				adliste[i][j]= zwei.intValue();
			}
		}
		return adliste;
	}

}
