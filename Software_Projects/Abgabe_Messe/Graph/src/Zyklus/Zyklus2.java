package Zyklus;

import java.util.List;
import java.util.Vector;

public class Zyklus2 {
	private List zyklus = null;
	private int [][] zyk = null;
	private Object [] zyk2= null;
	private boolean [] over = null;
	private Vector [] V = null;
	private Vector R = null;
	
	public Zyklus2 (boolean[][] b , Object [] Punkte  ) {
		this.zyk2 = Punkte;
		this.zyk= Adjazenzliste.get_Adjazenzliste(b);
		
	}
	
	public List get_basics() {  //get basics
		this.zyklus= new Vector();
		this.over = new boolean[this.zyk.length];
		this.V = new Vector [this.zyk.length];
		this.R = new Vector();
		 Zyklus3 sccs = new  Zyklus3 (this.zyk);
		int neu=0;
		
		while(true) {
			Zyklus1 Ergebnis= sccs.getAdjazenzliste(neu);
			if(Ergebnis!=null && Ergebnis.get_Matrix()!=null) {
				Vector[] t= Ergebnis.get_Matrix();
				neu= Ergebnis.get_adjazenz2();
				for(int j=0;j<t.length;j++) {
					if((t[j]!=null) && (t[j].size()>0 )) {
						this.over[j]= false;
						this.V[j]= new Vector();
					}
				}
				
				this.getZyklus(neu, neu, t);
				neu++;
			} else {
				break;
			}
		}
		return this.zyklus;
				
	}
	
	
	  private boolean getZyklus(int v, int s, Vector[] adjList) {
	        boolean f = false;
	        this.R.add(new Integer(v));
	        this.over[v] = true;

	        for (int i = 0; i < adjList[v].size(); i++) {
	            int w = ((Integer) adjList[v].get(i)).intValue();
	            // found cycle
	            if (w == s) {
	                Vector cycle = new Vector();
	                for (int j = 0; j < this.R.size(); j++) {
	                    int index = ((Integer) this.R.get(j)).intValue();
	                    cycle.add(this.zyk2[index]);
	                }
	                this.zyklus.add(cycle);
	                f = true;
	            } else if (!this.over[w]) {
	                if (this.getZyklus(w, s, adjList)) {
	                    f = true;
	                }
	            }
	        }

	        if (f) {
	            this.verhinderung(v);
	        } else {
	            for (int i = 0; i < adjList[v].size(); i++) {
	                int w = ((Integer) adjList[v].get(i)).intValue();
	                if (!this.V[w].contains(new Integer(v))) {
	                    this.V[w].add(new Integer(v));
	                }
	            }
	        }

	        this.R.remove(new Integer(v));
	        return f;
	    }
	
	

	private void verhinderung(int n) {
		this.over[n] = false;
		Vector one= this.V[n];
		while(one.size()>0) {
			Integer c = (Integer) one.get(0);
			one.remove(0);
			if(this.over[c.intValue()]) {
				this.verhinderung(c.intValue());
			}
		}
	}
	
	
}
