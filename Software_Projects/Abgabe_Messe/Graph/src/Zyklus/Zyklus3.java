package Zyklus;

import java.util.Vector;

public class Zyklus3 {
	private int [][] ersteListe= null;
	private int [][] Liste= null;
	private boolean [] b= null;
	private Vector v= null;
	private int [] c= null;
	private int []anzahl = null;
	private int s = 0;
	private Vector abaktuell= null;
	
	public Zyklus3(int [][] Liste) {
		this.ersteListe= Liste;
		
	}

	public Zyklus1 getAdjazenzliste(int n) {
		this.b= new boolean [this.ersteListe.length];
		this.c= new int [this.ersteListe.length];
		this.anzahl= new int [this.ersteListe.length];
		this.b= new boolean[this.ersteListe.length];
		this.v= new Vector();
		this.abaktuell= new Vector();
		
		this.grapherstellen(n);
		
		for(int i= n;i < ersteListe.length; i++) {
			if(!this.b[i]) {
				this.getConnect(i);
				Vector n2= this.getFirstId();
				if(n2!= null && !n2.contains(new Integer(n))&& !n2.contains(new Integer(n + 1))) {
					return this.getAdjazenzliste(n+1);
					
				}else {
					Vector [] adjazenzliste =this.getList(n2);
					if(adjazenzliste!=null) {
						for(int j=0; j<this.ersteListe.length;j++){
							if (adjazenzliste[j].size()>0) {
								return new Zyklus1(adjazenzliste,j);
							}
						}
					}
				}
			}
		}
		return null;
	}
	
	private void  grapherstellen (int n) {
		this.Liste = new int[this.ersteListe.length][0];
		
		for (int i=n;i <this.Liste.length;i++) {
			Vector neu=new Vector();
			for (int j =0; j<this.ersteListe[i].length;j++) {
				if(this.ersteListe[i][j]>=n) {
					neu.add(new Integer(this.ersteListe[i][j]));
					
				}
			} if (neu.size()>0) {
				this.Liste[i] = new int [neu.size()];
				for (int j=0 ; j < neu.size();j++) {
					Integer w = (Integer) neu.get(j);
					this.Liste[i][j] =w.intValue();
					
				}
			}
				
		}
	}
				private Vector getFirstId(){
					int min = this.Liste.length;
					Vector c = null;
					
					for(int i =0;i<this.abaktuell.size();i++){
						Vector s =(Vector) this.abaktuell.get(i);
						for(int j=0; j < s.size();j++){
							Integer node = (Integer)s.get(j); 
							if(node.intValue() < min){
								c = s;
								min = node.intValue();
							}
					}
					
				}
				return c;
			}
				
			public	Vector[] getList(Vector nodes){
					Vector[] l = null;
					if(nodes!=null){
						l = new Vector[this.Liste.length];
						for(int i = 0; i < l.length;i++){
							l[i]= new Vector();
						}
						for(int i = 0; i< nodes.size();i++){
							int n =((Integer)nodes.get(i)).intValue();
							for(int j=0;j<this.Liste[n].length;j++){
								int s =this.Liste[n][j];
								if(nodes.contains(new Integer(s))){
									l[n].add(new Integer(s));
								}
							}
						}
		}
					return l;
	}
	
	
	
	private void getConnect(int r){
		this.s++;
		this.c[r]= this.s;
		this.anzahl[r]= this.s;
		this.b[r]= true;
		this.v.add(new Integer(r));
		
		for(int i = 0;i<this.Liste[r].length;i++){
			int w =this.Liste[r][i];
			if(!this.b[w]){
				this.getConnect(w);
				this.c[r]= Math.min(c[r], c[w]);
				
			}else if(this.anzahl[w]< this.anzahl[r]){
				if(this.v.contains(new Integer(w))){
					c[r]= Math.min(this.c[r], this.anzahl[w]);
				}
			}
		}
		
		
		if((c[r]== anzahl[r])&&(v.size()>0)){
			int next=-1;
			Vector s= new Vector();
			do{
				next=((Integer)this.v.get(v.size()-1)).intValue();
				this.v.remove(v.size()-1);
				s.add(new Integer(next));
				
			}while(this.anzahl[next]> this.anzahl[r]);
			if(s.size()>1){
				this.abaktuell.add(s);
			}
		}
	}
}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
