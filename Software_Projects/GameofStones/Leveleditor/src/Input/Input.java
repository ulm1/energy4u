//package Input;
//
//import java.util.BitSet;
//
//import Game.GameMain;
//import GameConditions.GameCondition;
//import GameModel.RaceCar;
//import javafx.event.EventHandler;
//import javafx.scene.Scene;
//import javafx.scene.input.KeyCode;
//import javafx.scene.input.KeyEvent;
//import javafx.scene.input.MouseEvent;
//
//public class Input {
//
//	/**
//	 * Bitset which registers if any {@link KeyCode} keeps being pressed or if it is
//	 * released.
//	 */
//	private BitSet keyboardBitSet = new BitSet();
//
//	// -------------------------------------------------
//	// default key codes
//	// will vary when you let the user customize the key codes or when you add
//	// support for a 2nd raceCar
//	// -------------------------------------------------
//
//	private KeyCode upKey = KeyCode.UP;
//	private KeyCode downKey = KeyCode.DOWN;
//	private KeyCode leftKey = KeyCode.LEFT;
//	private KeyCode rightKey = KeyCode.RIGHT;
//
//	Scene scene;
//
//	public Input(Scene scene) {
//		this.scene = scene;
//		gameRunning();
//		// setUpInputHandler();
//
//	}
//
//	public void addListeners() {
//
//		scene.addEventFilter(KeyEvent.KEY_PRESSED, keyPressedEventHandler);
//		scene.addEventFilter(KeyEvent.KEY_RELEASED, keyReleasedEventHandler);
//		setUpInputHandler();
//		gameRunning();
//
//	}
//
//	public void removeListeners() {
//
//		scene.removeEventFilter(KeyEvent.KEY_PRESSED, keyPressedEventHandler);
//		scene.removeEventFilter(KeyEvent.KEY_RELEASED, keyReleasedEventHandler);
//
//	}
//
//	/**
//	 * TO stear the RaceCar in the GameMain
//	 * 
//	 */
//	
//	public void gameRunning() {
//		scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
//			@Override
//			public void handle(KeyEvent event) {
//				String c = event.getCode().toString();
//
//				switch (c) {
//				case "UP":
//					RaceCar.changeSpeedLevel(+1);
//					GameMain.realeased = false;
//					GameMain.CarMovements();
//					
//					break;
//				case "DOWN":
//					RaceCar.changeSpeedLevel(-1);
//					GameMain.realeased = false;
//					GameMain.CarMovementsBB();
//					break;
//				case "P":
//					GameMain.pauseGame();
//					break;
//				case "R":
//					if (!(GameMain.gameCondition == GameCondition.StartMenu)) {
//						GameMain.setGameCondition(GameCondition.Restart);
//						GameMain.restartGame();
//					}
//					break;
//				default:
//					break;
//				}
//
//			}
//		}
//		);
//		
//		scene.setOnKeyReleased(new EventHandler<KeyEvent>() {
//			@Override
//			public void handle(KeyEvent event) {
//				String c = event.getCode().toString();
//
//				switch (c) {
//				case "UP":
//					GameMain.realeased = true;
//					GameMain.CarMovementsF();
//					
//					break;
//				case "DOWN":
//					GameMain.realeased = true;
//					GameMain.CarMovementsB();
//					break;
//				case "P":
//					GameMain.pauseGame();
//					break;
//				default:
//					break;
//				}
//
//			}
//		}
//		);
//		
//		
//
//	}
//	
//	/**
//	 * For the Buttons on the different Menus eg StartMenu,PauseMenu
//	 */
//
//	private void setUpInputHandler() {
//
//		GameMain.startB.setOnMousePressed(new EventHandler<MouseEvent>() {
//			public void handle(MouseEvent event) {
//				GameMain.startGame();
//			}
//		});
//
//		GameMain.pauseB.setOnMousePressed(new EventHandler<MouseEvent>() {
//			public void handle(MouseEvent event) {
//				GameMain.resumeGame();
//			}
//		});
//
//		GameMain.goB.setOnMousePressed(new EventHandler<MouseEvent>() {
//			public void handle(MouseEvent event) {
//				GameMain.restartGame();
//			}
//		});
//
//		GameMain.wb.setOnMousePressed(new EventHandler<MouseEvent>() {
//			public void handle(MouseEvent event) {
//				GameMain.restartGame();
//			}
//		});
//
//	}
//
//	/**
//	 * "Key Pressed" handler for all input events: register pressed key in the
//	 * bitset
//	 */
//	private EventHandler<KeyEvent> keyPressedEventHandler = new EventHandler<KeyEvent>() {
//		@Override
//		public void handle(KeyEvent event) {
//
//			// register key down
//			keyboardBitSet.set(event.getCode().ordinal(), true);
//
//		}
//	};
//
//	/**
//	 * "Key Released" handler for all input events: unregister released key in the
//	 * bitset
//	 */
//	private EventHandler<KeyEvent> keyReleasedEventHandler = new EventHandler<KeyEvent>() {
//		@Override
//		public void handle(KeyEvent event) {
//
//			// register key up
//			keyboardBitSet.set(event.getCode().ordinal(), false);
//
//		}
//	};
//
//	// -------------------------------------------------
//	// Evaluate bitset of pressed keys and return the raceCar input.
//	// If direction and its opposite direction are pressed simultaneously, then the
//	// direction isn't handled.
//	// -------------------------------------------------
//
//	public boolean isMoveUp() {
//		return keyboardBitSet.get(upKey.ordinal()) && !keyboardBitSet.get(downKey.ordinal());
//	}
//
//	public boolean isMoveDown() {
//		return keyboardBitSet.get(downKey.ordinal()) && !keyboardBitSet.get(upKey.ordinal());
//	}
//
//	public boolean isMoveLeft() {
//		return keyboardBitSet.get(leftKey.ordinal()) && !keyboardBitSet.get(rightKey.ordinal());
//	}
//
//	public boolean isMoveRight() {
//		return keyboardBitSet.get(rightKey.ordinal()) && !keyboardBitSet.get(leftKey.ordinal());
//	}
//
//}
