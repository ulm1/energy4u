package Game;

import java.awt.Desktop;

import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;


public class LevelEditor extends Application {
	
	Button btnscene1, btnscene2;
    Label lblscene1;
    BorderPane pane1;
    FlowPane pane2;
    Scene scene1, scene2;
    Stage thestage;
	static double p = 3;
	static BufferedImage I;
	static int px[], py[], color[], cells = 100, size = 1000;

	
private Desktop desktop = Desktop.getDesktop();
	
	
	   Pane leveleditor = new Pane(); 
       Group root = new Group();  
	
    
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("GameOfStones");
        
        thestage=primaryStage;
        //can now use the stage in other methods
       
        //make things to put on panes
        btnscene1=new Button("Neues Level");
        btnscene2=new Button("Zur�ck");
        btnscene2.setLayoutY(30);   

        btnscene1.setOnAction(e-> ButtonClicked(e));
        btnscene2.setOnAction(e-> ButtonClicked(e));
        lblscene1=new Label("LevelEditor");
        
      //make 2 Panes
        pane1=new BorderPane();
        pane2= new FlowPane();
        
     
      
     
        //set background color of each Pane
        pane1.setStyle("-fx-background-color: grey;-fx-padding: 10px;");
        
        
        pane2.getChildren().addAll(btnscene1);
        pane1.setTop(lblscene1);
        pane1.setCenter(pane2);
          
        
      //make 2 scenes from 2 panes
        

        scene1 = new Scene(pane1, 1300, 1000);
        scene2 = new Scene(root, 1300, 1000);
        

        
        
        //create Menu
        MenuBar menuBar = new MenuBar();
        menuBar.prefWidthProperty().bind(primaryStage.widthProperty());
        
        // File menu - new, save, exit
        MenuItem openMenuItem = new MenuItem("Open");
        MenuItem exitMenuItem = new MenuItem("Exit");
        exitMenuItem.setOnAction(actionEvent -> Platform.exit());
        final FileChooser fileChooser = new FileChooser();
        
        
       
        
        //saveMenu actions
        
        openMenuItem.setOnAction(new EventHandler<ActionEvent>() {
        	  @Override
            public void handle(final ActionEvent e) {
        		  File file = fileChooser.showOpenDialog(primaryStage);
                  if (file != null) {
                      openFile(file);
                  }
            }
        }
    );
		
		
		int n = 0;
		Random rand = new Random();
		I = new BufferedImage(size, size, BufferedImage.TYPE_INT_RGB);
		px = new int[cells];
		py = new int[cells];
		color = new int[cells];
		for (int i = 0; i < cells; i++) {
			px[i] = rand.nextInt(size);
			py[i] = rand.nextInt(size);
			color[i] = rand.nextInt(16777215);
			Image image = SwingFXUtils.toFXImage(I, null);
			Label l = new Label();
			l.setGraphic(new ImageView(image));
			leveleditor.getChildren().add(l);
			
			
		
			
 
		}
		for (int x = 0; x < size; x++) {
			for (int y = 0; y < size; y++) {
				n = 0;
				for (byte i = 0; i < cells; i++) {
					if (distance(px[i], x, py[i], y) < distance(px[n], x, py[n], y)) {
						n = i;
 
					}
				}
				I.setRGB(x, y, color[n]);
				
			
				
 
			}
		}
 
//		GraphicsContext gc = I.getGraphicsContext2D();
//		Graphics2D g = I.createGraphics();
//		Color c = Color.BLACK;
//		g.setFill(c);
//		for (int i = 0; i < cells; i++) {
//			g.fill(new Ellipse2D .Double(px[i] - 2.5, py[i] - 2.5, 5, 5));
		Circle circle = new Circle(); 
		circle.setCenterX(300.0f); 
	    circle.setCenterY(180.0f); 
	    circle.setRadius(90.0f); 
	    
	    circle.setFill(Color.DARKRED);    
	      
	      //Setting the stroke width 
	    circle.setStrokeWidth(3); 
	      
	      //Setting color to the stroke  
	    circle.setStroke(Color.DARKSLATEBLUE);
		
		

		try {
			ImageIO.write(I, "png", new File("voronoi.png"));
		
		   
		} catch (IOException e) {
 
		}
		


		 root.getChildren().add(leveleditor);
			

        primaryStage.setScene(scene1);
        primaryStage.show();
    }
    
    
    private void openFile(File file) {
        try {
            desktop.open(file);
        } catch (IOException ex) {
            Logger.getLogger(
                FileChooser.class.getName()).log(
                    Level.SEVERE, null, ex
                );
        }
    }
    
    public void ButtonClicked(ActionEvent e)
    {
        if (e.getSource()==btnscene1)
            thestage.setScene(scene2);
        else
            thestage.setScene(scene1);
    }
    

	static double distance(int x1, int x2, int y1, int y2) {
		double d;
	    d = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)); // Euclidian
	  	return d;
	}
    
    
    
}
	





	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
