package client.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonBuilderFactory;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.JsonValue;

import com.google.common.collect.Multiset.Entry;
import com.jme3.app.state.AbstractAppState;

import JsonFromServer.Territory;
import client.application.ButtonDialog;
import client.application.GameofStones;
import client.application.NetworkThread;
import client.application.Phase;
import client.application.RankingField;

import client.model.Recruitment;
import client.util.PhaseType;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.controls.DropDown;
import de.lessvoid.nifty.controls.Label;
import de.lessvoid.nifty.controls.TextField;
import de.lessvoid.nifty.controls.Window;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.elements.render.TextRenderer;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;

public class PhaseController extends AbstractAppState implements ScreenController {

	private Nifty nifty;
	private static Screen screen;
	private GameofStones mainApp;
	private Element niftyElement;
	static JsonObject j;
	static ArrayList<myterroritories> up;
	static Long st = 0L;
	static Long in = 0L;
	static Long ter = 0L;
	static PhaseType phase;

	static String recuring;

	
	static ArrayList<Integer> hello;
	



	@Override
	public void bind(Nifty nifty, Screen screen) {
		this.nifty = nifty;
		this.screen = screen;
		this.mainApp = GameofStones.getMyself();

		// Label mainLabel = screen.findNiftyControl("mainLabel", Label.class);
		// mainLabel.setText("hallooo");

	}

	static boolean text = false;

	// Note that update is only called while the state is both attached and enabled.

	static DropDown<Integer> recId;
	static DropDown<Integer> recArcher;
	static DropDown<Integer> recKnight;
	static DropDown<Integer> recSword;
	static Long m;

	static DropDown<Integer> movDropArcher;
	static DropDown<Integer> movDropKnight;
	static DropDown<Integer> movDropSword;
	static DropDown<Integer> movDropIdFrom;
	static DropDown<Integer> movDropIdTo;

	static RankingField g = new RankingField();
	
	private static int roundTer = -1;

	public static void updateText() {
		// do the following while game is RUNNING

		Long terr;
		ArrayList<String> array = new ArrayList<String>();
		
		if (PhaseType.SUPPLY.equals(phase)) {

			// HashMap<Integer, Recruitment> curRecFromAlligence =
			// houseToRecruitment.get(units.getAllegiance())

			// Object bla = myinfo.values().toArray()[0];

			// if my housename matches players house then give me the id

			DropDown<String> mainTextField = screen.findNiftyControl("mainTextField", DropDown.class);
			
			for (myterroritories myTerr : up) {
				
				roundTer = myTerr.getTerritorId().intValue();
				
				mainTextField.addItem("" + myTerr.getTerritorId());
				mainTextField.selectItem("" + myTerr.getTerritorId());

				TextField mainTextField3 = screen.findNiftyControl("mainTextField3", TextField.class);
				mainTextField3.setText(myTerr.upkeep.toString());
				mainTextField3.disable();

				TextField mainTextField2 = screen.findNiftyControl("mainTextField2", TextField.class);
				mainTextField2.setText(myTerr.stock.toString());
				mainTextField2.disable();
				
				TextField mainTextField1 = screen.findNiftyControl("mainTextField1", TextField.class);
				mainTextField1.setText(myTerr.income.toString());
				mainTextField1.disable();

				TextField fahn = screen.findNiftyControl("mainTextField4", TextField.class);
				phase = PhaseType.None;

				if (myTerr.upkeep.toString().equals("0") && myTerr.stock.toString().equals("0")) {

					fahn.setText("YES");
					fahn.disable();

					phase = PhaseType.None;
				}

				else {

					fahn.disable();
					phase = PhaseType.None;

				}

			}
		}

		else if (PhaseType.RECRUITMENT.equals(phase)) {

			Map<Integer, String> myinfo = NetworkThread.playainfo;
			Map<String, HashMap<Integer, Recruitment>> people = g.houseToRecruitment;
			
			recId = screen.findNiftyControl("terTextField", DropDown.class);
			recArcher = screen.findNiftyControl("terTextField2", DropDown.class);
			recKnight = screen.findNiftyControl("terTextField3", DropDown.class);
			recSword = screen.findNiftyControl("terTextField4", DropDown.class);

			List<Territory> territories = g.terr;
			Territory bufferTer = territories.get(roundTer);
			String house = bufferTer.getHouse();
			
			HashMap<Integer, Recruitment> houseMap= people.get(house);
			Set<Integer> keys = houseMap.keySet();
			HashMap<Integer, Recruitment> noMansLand = people.get("nothing");
			if(noMansLand != null)
				keys.addAll(noMansLand.keySet());
			
			for(Integer key : keys)
			{
				recId.addItem(key);
				recId.selectItem(key);
			}
			
			phase = PhaseType.None;
			
			for(int i = 0; i < 6; i++)
			{
				recArcher.addItem(i);
				recKnight.addItem(i);
				recSword.addItem(i);
			}
			
			recArcher.selectItem(0);
			recKnight.selectItem(0);
			recSword.selectItem(0);
		}

		if (PhaseType.MOVEMENT.equals(phase)) {
			
			phase = PhaseType.None;
			
			movDropArcher   =   screen.findNiftyControl("beTextField", DropDown.class);
			movDropKnight   =   screen.findNiftyControl("beTextField2", DropDown.class);
			movDropSword    =   screen.findNiftyControl("beTextField3", DropDown.class);
			movDropIdFrom   =   screen.findNiftyControl("beTextField4", DropDown.class);
		    movDropIdTo     =   screen.findNiftyControl("beTextField5", DropDown.class);
		    
		    movDropArcher.enable();
		    movDropKnight.enable();
		    movDropSword .enable();
		    movDropIdFrom.enable();
		    movDropIdTo.enable();
  
		    for(int i = 0; i < 6; i++)
		    {
		    	movDropArcher.addItem(i);
		    	movDropKnight.addItem(i);
		    	movDropSword.addItem(i);
		    }
		    movDropArcher.selectItem(0);
		    movDropKnight.selectItem(0);
		    movDropSword.selectItem(0);
		    
			List<Territory> territories = g.terr;
			Territory bufferTer = territories.get(roundTer);
			String house = bufferTer.getHouse();
		    
		    HashMap<Integer, Recruitment> houseMap= g.houseToRecruitment.get(house);
			Set<Integer> keys = houseMap.keySet();
			
			for(Integer key : keys)
			{
				movDropIdFrom.addItem(key);
				movDropIdFrom.selectItem(key);
			}
			
			for(int i = 0; i < territories.size(); i++)
			{
				movDropIdTo.addItem(i);
			}
			movDropIdTo.selectItem(0);
		}

	}

	public void pause() {

	}

	@Override
	public void onStartScreen() {
		// nothing
	}

	@Override
	public void onEndScreen() {
		// nothing
	}

	public void Ok(String nextScreen) {
		phase = PhaseType.RECRUITMENT;

		nifty.gotoScreen(nextScreen);

	}

	public void Rekrutieren(String nextScreen) {
		phase = PhaseType.MOVEMENT;
		
		encodeRecuring( recId.getSelection() , recArcher.getSelection(), recKnight.getSelection(), recSword.getSelection());

		nifty.gotoScreen(nextScreen);

	}

	public void Bewegen(String nextScreen) {
		
		//encodeMovement();
		
		nifty.gotoScreen(nextScreen);

	}

	public void QuitGame(String nextScreen) {

		nifty.gotoScreen(nextScreen);

	}

	public static JsonObject encodeRecuring(int id, int sword, int bow, int knight) {
		JsonBuilderFactory factory = Json.createBuilderFactory(null);

		long currentTime = System.currentTimeMillis();

		JsonObject jsonOb = factory.createObjectBuilder()
				.add("header",
						factory.createObjectBuilder().add("UUID", NetworkThread.uid).add("name", NetworkThread.name)
								.add("timestamp", currentTime).add("messageType", "recruitmentPhase"))

				.add("body",
						factory.createObjectBuilder().add("recruitments",
								factory.createArrayBuilder()
										.add(factory.createObjectBuilder().add("territoryId", id)
												.add("recruitedSword", sword).add("recruitedBow", bow)
												.add("recruitedKnight:", knight))))
				.build();
		System.out.println(jsonOb);

		network.onWebsocketMessage(network.getConnection(), jsonOb.toString());

		return jsonOb;

	}
	
	public static JsonObject encodeMovement()
	{
		long currentTime = System.currentTimeMillis();
		JsonBuilderFactory factory = Json.createBuilderFactory(null);
		JsonObjectBuilder objFac = factory.createObjectBuilder();
		
		JsonObjectBuilder headerBuilder = factory.createObjectBuilder();                                                  
		headerBuilder.add("UUID", NetworkThread.uid);                                                                     
		headerBuilder.add("name", NetworkThread.name);                                                                     
		headerBuilder.add("timestamp", currentTime);                                                                      
		headerBuilder.add("messageType", "movementPhase");                                                                  

		JsonObjectBuilder movBuilder = factory.createObjectBuilder();
		movBuilder.add("amountSword", movDropSword.getSelection());
		movBuilder.add("amountBow", movDropArcher.getSelection());
		movBuilder.add("amountKnight", movDropKnight.getSelection());
		movBuilder.add("territoryIdFrom", movDropIdFrom.getSelection());
		movBuilder.add("territoryIdTo", movDropIdTo.getSelection());
		
		JsonArrayBuilder bodyArr = factory.createArrayBuilder();
		bodyArr.add(movBuilder.build());
		
		JsonObjectBuilder bodyBuilder = factory.createObjectBuilder();
		bodyBuilder.add("movements", bodyArr.build());
		
		objFac.add("header", headerBuilder.build());
		objFac.add("body", bodyBuilder.build());
		
		JsonObject jsonOb = objFac.build();
		System.out.println(jsonOb);
		
		network.onWebsocketMessage(network.getConnection(), jsonOb.toString());
		
		return jsonOb;
	}
	
	public static void supplyResults(ArrayList<myterroritories> myValues, PhaseType supply) {
		phase = supply;
		up = myValues;

	}

	static NetworkThread network;

	public static void setNetwork(NetworkThread networkThread) {
		network = networkThread;

	}


	public static void setMyTerr(ArrayList<Integer> hi) {
		hello = hi;

	}
}
