package client.model;

public class Movement {
	
	private int moveArchers;
	private int moveSwords;
	private int moveKnights;
	
	public Movement (int archer, int knights, int swordsman){
		super();
		this.moveArchers=archer;
		this.moveSwords =swordsman;
		this.moveKnights =knights;
		
	}

	public int getMoveArchers() {
		return moveArchers;
	}

	public void setMoveArchers(int moveArchers) {
		this.moveArchers = moveArchers;
	}

	public int getMoveSwords() {
		return moveSwords;
	}

	public void setMoveSwords(int moveSwords) {
		this.moveSwords = moveSwords;
	}

	public int getMoveKnights() {
		return moveKnights;
	}

	public void setMoveKnights(int moveKnights) {
		this.moveKnights = moveKnights;
	}

}
