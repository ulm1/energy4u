package client.model;

import java.util.HashMap;
 
public class Recruitment {
	
	private int archer;
	private int knights;
	private int swordsman;
	
	public Recruitment(int archer, int knights, int swordsman) {
		super();
		this.archer = archer;
		this.knights = knights;
		this.swordsman = swordsman;
	}

	public int getArcher() {
		return archer;
	}

	public void setArcher(int archer) {
		this.archer = archer;
	}

	public int getKnights() {
		return knights;
	}

	public void setKnights(int knights) {
		this.knights = knights;
	}

	public int getSwordsman() {
		return swordsman;
	}

	public void setSwordsman(int swordsman) {
		this.swordsman = swordsman;
	}
	
	public void combineRecruitment(Recruitment rec) {
		this.archer += rec.archer;
		this.knights += rec.knights;
		this.swordsman += rec.swordsman;
	}
	
}
