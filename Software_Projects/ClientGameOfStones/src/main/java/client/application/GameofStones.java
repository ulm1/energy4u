package client.application;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

import org.java_websocket.drafts.Draft_10;

import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AppState;
import com.jme3.math.ColorRGBA;
import com.jme3.system.AppSettings;
import com.jme3.ui.Picture;

import client.model.Map;
import client.model.Player;
import client.util.GameState;


public class GameofStones extends SimpleApplication {

	int FIELD_WIDTH = 10;
	int FIELD_HEIGHT = 5;
	float X_START = -2;
	float Y_START = 0;
	float Z_START = -2;
	public final static String UNSHADED = "Common/MatDefs/Misc/Unshaded.j3md";
	public static int SCREEN_HEIGHT;
	public static int SCREEN_WIDTH;

	public static GameofStones myself;
	public static MenuScreen menuScreen;
	public static StartScreen startScreen;
	static LobbyScreen lobbyScreen;
	public static RankingField rankingField;
	public static Phase phase;
	static gameField gameField;
	Map gameMap;
	ArrayList<Player> playerList;

	private static boolean changeState = false;
	private static AppState disableState;
	private static AppState enableState;

	Picture logo;

	public static void main(String[] args) {
		GameofStones stones = new GameofStones();
		myself = stones;
		AppSettings settings = new AppSettings(true);
		settings.setRenderer(AppSettings.LWJGL_OPENGL2);
		settings.setWidth(1000);
		settings.setHeight(1000);
		settings.useInput();
		stones.setPauseOnLostFocus(false);		
		stones.setSettings(settings);
		stones.setDisplayFps(false);
		stones.setShowSettings(false);
		stones.setDisplayStatView(false);
		stones.start();

	}

	public static GameofStones getMyself() {
		return myself;
	}

	@Override
	public void simpleInitApp() {

		flyCam.setEnabled(false);

		this.getViewPort().setBackgroundColor(ColorRGBA.Gray);
	
		menuScreen = new MenuScreen();
		startScreen = new StartScreen();
		lobbyScreen = new LobbyScreen();
		gameField = new gameField();
	
		stateManager.attach(menuScreen);
		menuScreen.initialize(this.stateManager, this);
		menuScreen.setEnabled(true);
		SCREEN_HEIGHT = this.getCamera().getHeight();
		SCREEN_WIDTH = this.getCamera().getWidth();

	}

	@Override
	public void simpleUpdate(float tpf) {
		
		
		
		if((NetworkThread.game == GameState.Lobby) ||(NetworkThread.game == GameState.Game )){
		
		if(myself.getTimer().getTimeInSeconds()>5) {
			Timers.main(null);
			
			myself.getTimer().reset();
			
		}
		}
		
		
		if (changeState) {

			disableState.setEnabled(false);
			disableState.cleanup();
			stateManager.detach(disableState);
			stateManager.attach(enableState);
			enableState.initialize(stateManager, this);
			enableState.setEnabled(true);
			changeState = false;
			rootNode.updateGeometricState();
			guiNode.updateGeometricState();

		}
	}


	
	
	public  void startConnection(String[] packedStartScreenData,String text) {		
		
		NetworkThread client = null;
		
		if(text.equals("start")) {
	
		try {
		client = new NetworkThread(new URI("ws://localhost:8025"),new Draft_10());
			 
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}


    	client.ConnectToServer(packedStartScreenData,this);
		}
		
		if(text.equals("text")) {
		
			client.setReady(true);

		
	}
	}	

	public static void switchAppState(AppState dis, AppState en) {
		disableState = dis;
		enableState = en;
		changeState = true;
	}
}
