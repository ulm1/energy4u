package client.application;

import client.controller.DragController;
import client.controller.LobbyScreenController;
import client.controller.NetworkController;
import client.controller.PhaseController;
import client.controller.myterroritories;
import client.model.Player;
import client.util.AnimationFactory;
import client.util.GameState;
import client.util.PhaseType;

import JsonFromServer.*;

//import server.ServerController;

import java.lang.Thread;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jme3.effect.ParticleEmitter;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft;
import org.java_websocket.drafts.Draft_10;
import org.java_websocket.drafts.Draft_17;

import org.java_websocket.handshake.ServerHandshake;

import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.json.JsonObject;

public class NetworkThread extends WebSocketClient {

	static String[] packedStartScreenData;
	static GameofStones mainApp;
	static boolean laser = false;
	static ArrayList<Player> player_List = new ArrayList<Player>();
	static String[] orientations;
	public static boolean pause = false;

	Gson gson = null;
	Gson g = null;
	public static String name = "";
	static boolean readyf = false;

	static boolean OnStart = true;
	static boolean InLobby = false;
	public static String uid = "";
	static Runnable rn;
	static JsonObject j;
	static String myMessage;
	Socket socket = null;
	ObjectOutputStream oos = null;
	ObjectInputStream ois = null;
	boolean reconnect = true;
	public static String meinHause;
	public static  HashMap<Integer, String> playainfo;
	
	static GameState game;

	public boolean isReady() {

		return readyf;
	}

	public void setReady(boolean ready) {
		onWebsocketMessage(getConnection(), NetworkController.ready(name, ready, uid).toString());

	}

	public NetworkThread(URI serverUri, Draft draft) {
		super(serverUri, new Draft_17());

	}

	public NetworkThread(URI serverURI) {
		super(serverURI);
	}

	public static void setMyMessage(String myMessage) {
		NetworkThread.myMessage = myMessage;
	}

	@Override
	public void onOpen(ServerHandshake handshakedata) {

		System.out.println("new connection opened");
		if (OnStart == true) {

			LobbyScreenController.setNetwork(this);

			boolean isPlayer;
			if (packedStartScreenData[1].equals("watcher")) {
				isPlayer = false;
			} else {
				isPlayer = true;
			}

			send(NetworkController.encodeLoginRequest(packedStartScreenData[0], isPlayer, true).toString());

			Timers.myTimer(packedStartScreenData, uid, this);
			PhaseController.setNetwork(this);

		}

	}

	@Override
	public void onClose(int code, String reason, boolean remote) {

		System.out.println("closed with exit code " + code + " additional info: " + reason);

		try {

			new NetworkThread(new URI("ws://localhost:8025"), new Draft_10());
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void onMessage(String message) {

		this.myMessage = message;
		System.out.println("received message: " + message);
		gson = new GsonBuilder().serializeNulls().create();

		ClientData cdat = null;
		cdat = gson.fromJson(message, ClientData.class);

		// handle incoming "login"
		if (cdat.getHeader().getMessageType().equals("loginResponse")) {

			if (cdat.getBody().isAccepted() == true) {
			
				uid = cdat.getHeader().getUUID();
				
			
				GameofStones.switchAppState(GameofStones.startScreen, GameofStones.lobbyScreen);
				game = GameState.Lobby;

				return;

			}

			else {
				System.out.println("Not accepted!!!");

			}

		}

		if (cdat.getHeader().getMessageType().equals("playerList")) {
			
			
			//create array to go through list
			for(int i = 0; i<cdat.getBody().getPlayers().length;i++) {
				//get my name out of the list
				if(cdat.getBody().getPlayers()[i].getName().equals(packedStartScreenData[0])) {

					name = cdat.getBody().getPlayers()[i].getName();
					meinHause =cdat.getBody().getPlayers()[i].getHouse();
					
			 playainfo = new HashMap<Integer, String>();
					 playainfo.put(1,meinHause);
					
					
					
					LobbyScreenController.setChangedHouse(false);
					LobbyScreenController.setHouse(meinHause);
					
				}
				
			}

		}
		gameField newGameField = new gameField();

		if (cdat.getHeader().getMessageType().equals("configData")) {

			List<Territory> house = cdat.getBody().getLevelcfg().getTerritories();
			
			
			LobbyScreenController.setTerr(house);
			RankingField.setTerritories(house);
			newGameField.setTerritories(house);
			newGameField.setEdges(cdat.getBody().getLevelcfg().getGame().getEdges());
			


		}

		if (cdat.getHeader().getMessageType().equals("supplyPhase")) {
		
			ArrayList<myterroritories> myValues = new ArrayList<myterroritories>();
			for(int i= 0; i <cdat.getBody().getSupplyResults().length; i++ ) {
				
				
				
				Long upkeep = cdat.getBody().getSupplyResults()[i].getUpkeep();
				String house = cdat.getBody().getSupplyResults()[i].getHouse();
				Long stock = cdat.getBody().getSupplyResults()[i].getStock();
				Long income = cdat.getBody().getSupplyResults()[i].getIncomeTerritories().get(i).getIncome();
				Long territoryId = cdat.getBody().getSupplyResults()[i].getIncomeTerritories().get(i).getTerritoryId();
				
				myterroritories terr= new myterroritories(house, territoryId, income, upkeep, stock);
				System.out.println(territoryId);
				myValues.add(terr);
				
				
			}
			

			PhaseController.supplyResults(myValues, PhaseType.SUPPLY);

		}
		if (cdat.getHeader().getMessageType().equals("gameStart")) {
			GameofStones.switchAppState(GameofStones.lobbyScreen, GameofStones.gameField);
			game = GameState.Game;
			
	

		}

		if (getMyMessage().equals("pauseRequest")) {
			send(NetworkController.pauseRequest(packedStartScreenData[0], uid).toString());

		}

		if (cdat.getHeader().getMessageType().equals("ready")) {

			String message1 = NetworkController.ready(packedStartScreenData[0], true, uid).toString();
			send(message1);

			send(NetworkController.ping(packedStartScreenData[0], true, uid).toString());

		}

		if (cdat.getHeader().getMessageType().equals("recruitmentPhase")) {
			send(message);

		}
		
		if(cdat.getHeader().getMessageType().equals("movementPhase")) {
			send(message);
		}
		
		if (cdat.getHeader().getMessageType().equals("houseRequest")) {
			send(message);

		}
		
		if (cdat.getHeader().getMessageType().equals("logout")) {
			
			
	
			
	
				ParticleEmitter checkpointAnim, destroyAnim;
				checkpointAnim = AnimationFactory.getShockwave(mainApp);
				
				newGameField.voronoi.attachChild(checkpointAnim);
				checkpointAnim.setParticlesPerSec(0);
				checkpointAnim.setNumParticles(1);
				checkpointAnim.emitAllParticles();

				ParticleEmitter toast = AnimationFactory.getToast(mainApp, AnimationFactory.CP_REACHED);
				toast.setLocalTranslation(6, 5, -1);
				newGameField.voronoi.attachChild(toast);
				toast.setParticlesPerSec(6);
				toast.setNumParticles(1);
				toast.emitAllParticles();
 
				
			
				}
	}
//			send(message);
//			
//			if(game == GameState.Lobby) {
//			GameofStones.switchAppState(GameofStones.lobbyScreen,GameofStones.startScreen);
//			}
//			
//			if(game == GameState.Game){
//			GameofStones.switchAppState(GameofStones.gameField,GameofStones.startScreen);
//			}
//			
//			game = GameState.Start;
//		}
//		
//
//	}

	public static String getMyMessage() {
		return myMessage;
	}

	public void send() {
		send(NetworkController.ready(packedStartScreenData[0], readyf, uid).toString());
		System.out.println("start Game");
	}

	@Override
	public void onMessage(ByteBuffer message) {
		System.out.println("received ByteBuffer");
	}

	@Override
	public void onError(Exception ex) {
		System.err.println("an error occurred:" + ex);
	}

	public void ConnectToServer(String[] strings, GameofStones gameofStones) {

		mainApp = gameofStones;
		packedStartScreenData = strings;
		this.connect();

	}

}