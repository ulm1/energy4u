package client.model;

import java.awt.geom.Dimension2D;
import java.util.ArrayList;

import client.util.Coordinate;
import client.util.Field;
import client.util.BorderType;

/**
 * Class for defining the object Map containing all the information about a game
 * (grid, number of checkpoints,..).
 * 
 * @author ali, fabian
 */
public class Map {
	public static final int MIN_PLAYERS = 2;
	public static final int MAX_PLAYERS = 8;
	public static final int MIN_CHECKPOINTS = 2;

	public int mapMaxSpectators = 10;
	public int mapMinPlayers;
	public int mapMaxPlayers;
	public int mapChoosingTime;
	public Field[][] cartography;
	public ArrayList<Coordinate> checkpoints;
	public Dimension2D dimensions;


	/**
	 * Constructor for the object map.
	 * 
	 * @param minPlay
	 *            Minimal number of players for the map.
	 * @param maxPlay
	 *            Maximal number of players for the map.
	 * @param chooseTime
	 *            Maximal time allowed for choosing the cards.
	 * @param maxSpect
	 *            Maximal number of spectators for the map.
	 * @param dim
	 *            Width and height of the map.
	 */
	public Map(int minPlay, int maxPlay, int chooseTime, int maxSpect, Dimension2D dim) {

		this.mapMinPlayers = minPlay;
		this.mapMaxPlayers = maxPlay;
		this.mapChoosingTime = chooseTime;
		this.mapMaxSpectators = maxSpect;
		this.dimensions = dim;
	

		this.cartography = new Field[8][8];

		/**
		 * Transform RIGHT/DOWN/RIGHT_DOWN into BorderTypes
		 */
		for (int j = 0; j < (int) dim.getWidth() + 1; j++) {
			for (int k = 0; k < (int) dim.getHeight() + 1; k++) {

				boolean isValid = true;

				if (j == 0 || k == 0) {
					isValid = false;
				}

				BorderType bt = null;

			

			

				this.cartography[j][k] = new Field(isValid, bt);
			}
		}
	}

	/**
	 * Method for checking if a map is valid, i.e. if all checkpoints are
	 * reachable.
	 * 
	 * @return Boolean telling whether the map is valid or not.
	 */
	public boolean checkValidity() {

		if (this.mapMinPlayers < MIN_PLAYERS || this.mapMaxPlayers > MAX_PLAYERS) {
			return false;
		}

		if (this.checkpoints.size() < MIN_CHECKPOINTS) {
			return false;
		}

		return true;
	}
}