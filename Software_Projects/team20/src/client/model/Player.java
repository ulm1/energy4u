package client.model;

import java.util.Observable;

import client.application.Roboter;

/**
 * Object for every participant actively taking part in the game.
 * 
 * @author fabian
 */
public class Player extends Participant {

	public String userName;
	public String color;
	public int userID;
	public boolean isAI;
	public Roboter roboter;

	/**
	 * Constructor of Player.
	 * 
	 * @param userName
	 *            Name of the player
	 * @param isAI
	 *            Boolean specifying whether a player is an AI
	 * @param userID
	 *            The user ID of the player
	 */
	public Player(String userName, boolean isAI, int userID) {
		super(userName, "USER", isAI, userID);
		this.userName = userName;
		this.isAI = isAI;
		this.userID = userID;
	}

	/**
	 * Setter method for the color in which the Roboter of the player is shown.
	 * 
	 * @param color
	 *            The color of the Roboter
	 */
	public void setColor(String color) {
		this.color = color;
	}

	/**
	 * Setter method for the Roboter associated with the player.
	 * 
	 * @param roboter
	 *            The Roboter of the player
	 */
	public void setRoboter(Roboter roboter) {
		this.roboter = roboter;
	}

	/**
	 * Getter method for the Roboter associated with the player.
	 * 
	 * @return The Roboter of the player.
	 */
	public Roboter getRoboter() {
		return this.roboter;
	}

	/**
	 * Automatically generated update-method for the Player.
	 */
	@Override
	public void update(Observable o, Object arg) {

	}

}