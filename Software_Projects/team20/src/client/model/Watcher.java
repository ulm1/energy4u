package client.model;

import java.util.Observable;

/**
 * Object for every user passively taken part in the game. Every Watcher is also
 * a Participant.
 * 
 * @author fabian
 */
public class Watcher extends Participant {

	/**
	 * Constructor of Watcher.
	 * 
	 * @param userName
	 *            The name of the watcher.
	 * @param isAI
	 *            Boolean specifying whether the Watcher is an AI
	 * @param userID
	 *            The ID associated with the Watcher
	 */
	public Watcher(String userName, boolean isAI, int userID) {
		super(userName, "SPECTATOR", isAI, userID);
	}

	/**
	 * Automatically generate update method for the Watcher.
	 */
	@Override
	public void update(Observable o, Object arg) {

	}
}