package client.util;

/**
 * Possible wall positions for a field on the grid.
 * 
 * @author ali, fabian
 */
public enum BorderType {
	/**
	 * Wall on the lower border of the field.
	 */
	SOUTH,
	/**
	 * Wall on the right border of the field.
	 */
	EAST,
	/**
	 * Wall on the right and on the lower border of the field.
	 */
	SOUTH_EAST
}