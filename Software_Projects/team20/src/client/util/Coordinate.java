package client.util;

/**
 * Coordinate object specifying the x-y position on the game grid.
 * 
 * @author ali, fabian
 */
public class Coordinate {
	public int x_Value;
	public int y_Value;

	/**
	 * 
	 * @param x
	 *            x-component of the position on the grid.
	 * @param y
	 *            y-component of the position on the grid.
	 */
	public Coordinate(int x, int y) {
		this.x_Value = x;
		this.y_Value = y;
	}
}