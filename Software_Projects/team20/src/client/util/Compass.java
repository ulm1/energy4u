package client.util;

/**
 * The possible orientations a robot can have.
 * 
 * @author ali, fabian
 */
public enum Compass {
	/**
	 * Northern direction (up).
	 */
	NORTH,
	/**
	 * Eastern direction (right).
	 */
	EAST,
	/**
	 * Southern direction (down).
	 */
	SOUTH,
	/**
	 * Western direction (left).
	 */
	WEST
}