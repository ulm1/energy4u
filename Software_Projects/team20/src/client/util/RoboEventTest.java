package client.util;

import static org.junit.Assert.*;

import org.junit.Test;

public class RoboEventTest {

	@SuppressWarnings("deprecation")
	@Test
	public void test() {
	float goal = 2.0f;
	boolean movedByaffected = false;
	RoboEvent rob = new RoboEvent(EventType.MOVE,goal,movedByaffected,Compass.EAST);
	assertEquals("EventType must be equal to MOVE", EventType.MOVE,rob.event);
	assertEquals("Moveaffected must be false", false, rob.flag);
	assertEquals("Compass must be EAST", Compass.EAST, rob.orientation);
		
	}

}
