package client.util;

import com.jme3.app.Application;
import com.jme3.material.Material;
import com.jme3.material.RenderState.BlendMode;
import com.jme3.renderer.queue.RenderQueue.Bucket;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Box;

import client.application.GameofStones;

public class NumberFactory {

	public static int numberID;

	public static Node getNumber(int id, Application app) {
		Box box = new Box(0.4f, 0.4f, 0);
		Node node = new Node("Number" + numberID);
		Geometry geo = new Geometry("NumberGeo" + numberID, box);

		Material mat = new Material(app.getAssetManager(), GameofStones.UNSHADED);
		mat.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);

		if (id < 10) {
			mat.setTexture("ColorMap", app.getAssetManager().loadTexture("CP" + id + "-01.png"));
			geo.setMaterial(mat);
			geo.setQueueBucket(Bucket.Translucent);
			node.attachChild(geo);
			numberID++;
			return node;
		} else {
			int lastDigit = id % 10;
			int firstDigit = (id - lastDigit) / 10;

			Material firstDigitMat = new Material(app.getAssetManager(), GameofStones.UNSHADED);
			firstDigitMat.setTexture("ColorMap", app.getAssetManager().loadTexture("CP" + firstDigit + "-01.png"));

			Material lastDigitMat = new Material(app.getAssetManager(), GameofStones.UNSHADED);
			lastDigitMat.setTexture("ColorMap", app.getAssetManager().loadTexture("CP" + lastDigit + "-01.png"));

			firstDigitMat.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
			lastDigitMat.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);

			Geometry geoFirstDigit = new Geometry("NumberGeo" + numberID + "_1", box);
			Geometry geoLastDigit = new Geometry("NumberGeo" + numberID + "_2", box);

			geoFirstDigit.setMaterial(firstDigitMat);
			geoLastDigit.setMaterial(lastDigitMat);

			geoFirstDigit.setQueueBucket(Bucket.Translucent);
			geoLastDigit.setQueueBucket(Bucket.Translucent);

			geoFirstDigit.move(-0.1f, 0, 0);
			geoLastDigit.move(0.1f, 0, 0);

			node.attachChild(geoFirstDigit);
			node.attachChild(geoLastDigit);

			numberID++;
			return node;

		}

	}
}
