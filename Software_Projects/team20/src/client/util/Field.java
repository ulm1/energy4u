package client.util;

import com.jme3.scene.Geometry;

/**
 * Field object which represents one single field of the game grid.
 * 
 * @author fabian
 */
public class Field {

	public boolean isValid;
	public BorderType border;
	public int checkpoint;
	public int portal;
	public int health;
	public Geometry boxGeo;

	/**
	 * Constructor for the Object Field.
	 * 
	 * @param isValid
	 *            Boolean which is true for valid and false for invalid fields.
	 * @param border
	 *            BorderType specifying whether a wall on the field exists.
	 */
	public Field(boolean isValid, BorderType border) {
		this.border = border;
		this.isValid = isValid;
	}
	
	
	
	
	
}