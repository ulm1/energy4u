package client.util;

import com.jme3.app.Application;
import com.jme3.effect.ParticleEmitter;
import com.jme3.effect.ParticleMesh;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;



public class AnimationFactory {

	public static final String LIVE_INC = "LVI";
	public static final String LIVE_DEC = "LVD";
	public static final String CP_REACHED = "CP";
	public static final String BLUELASER_HIT_R = "BL";
	public static final String REDLASER_HIT_DR = "RD";
	public static final String REDLASER_HIT_DL = "RHD";
	public static final String REDLASER_HIT_R = "RDR";
	public static final String BLUELASER_HIT_DR = "BlR";
	public static final String REDLASER_HIT_D = "RDLL";
	public static final String BLUELASER_HIT_DL = "HJ";
	public static final String BLUELASER_HIT_D = "d";
	public static final String LIVE_DEC3 = "LVD3";
	private static final String LIVE_INC2 = "LVI2";



	

	public static ParticleEmitter getShockwave(Application app) {
		ParticleEmitter debris = new ParticleEmitter("Debris", ParticleMesh.Type.Triangle, 1);
		Material debris_mat = new Material(app.getAssetManager(), "Common/MatDefs/Misc/Particle.j3md");
		debris_mat.setTexture("Texture", app.getAssetManager().loadTexture("Effects/Explosion/shockwave.png"));
		debris.setMaterial(debris_mat);
		debris.setImagesX(1);
		debris.setImagesY(1);
		debris.setStartSize(0);
		debris.setEndSize(2);
		debris.setLowLife(0.8f);
		debris.setHighLife(0.8f);
		debris.setParticlesPerSec(0);
		debris.setNumParticles(1);

		return debris;
	}

	public static ParticleEmitter getExplosion(Application app) {
		ParticleEmitter debris = new ParticleEmitter("Debris", ParticleMesh.Type.Triangle, 6);
		Material debris_mat = new Material(app.getAssetManager(), "Common/MatDefs/Misc/Particle.j3md");
		debris_mat.setTexture("Texture", app.getAssetManager().loadTexture("Effects/Explosion/destroyRobo.png"));
		debris.setMaterial(debris_mat);
		debris.setImagesX(3);
		debris.setImagesY(3);
		debris.setStartSize(0.1f);
		debris.setRotateSpeed(4);
		debris.setSelectRandomImage(true);
		debris.setStartColor(ColorRGBA.DarkGray);
		debris.setEndColor(ColorRGBA.DarkGray);
		debris.getParticleInfluencer().setInitialVelocity(new Vector3f(0, 8, 0));
		debris.getParticleInfluencer().setVelocityVariation(1);
		debris.setEndSize(0.3f);
		debris.setLowLife(0.6f);
		debris.setHighLife(0.6f);

		return debris;
	}

	public static ParticleEmitter getGameOver(Application app) {
		ParticleEmitter debris = new ParticleEmitter("Debris", ParticleMesh.Type.Triangle, 6);
		Material debris_mat = new Material(app.getAssetManager(), "Common/MatDefs/Misc/Particle.j3md");
		debris_mat.setTexture("Texture", app.getAssetManager().loadTexture("Effects/Toast/Game_Over_Toast-01.png"));
		debris.setMaterial(debris_mat);
		debris.setImagesX(1);
		debris.setImagesY(1);
		debris.setStartSize(1);
		debris.setRotateSpeed(0);
		debris.setStartColor(ColorRGBA.Orange);
		debris.setEndColor(ColorRGBA.Orange);
		debris.getParticleInfluencer().setInitialVelocity(new Vector3f(0, 1, 0));
		debris.getParticleInfluencer().setVelocityVariation(0);
		debris.setLowLife(1.5f);
		debris.setHighLife(1.5f);
		debris.setEndSize(1);

		return debris;
	}

	public static ParticleEmitter getToast(Application app, String format) {
		ParticleEmitter debris = new ParticleEmitter("Debris", ParticleMesh.Type.Triangle, 1);
		Material debris_mat = new Material(app.getAssetManager(), "Common/MatDefs/Misc/Particle.j3md");
		switch (format) {
		case LIVE_INC:
			debris_mat.setTexture("Texture", app.getAssetManager().loadTexture("Effects/Toast/LIVES_INC-01.png"));
			debris.setStartColor(new ColorRGBA(0, 1, 0, 1));
			debris.setEndColor(new ColorRGBA(0, 1, 0, 1));
			debris.setStartSize(1);
			debris.setEndSize(1);
			debris.getParticleInfluencer().setInitialVelocity(new Vector3f(0, 1, 0));
			break;
		case LIVE_INC2:
			debris_mat.setTexture("Texture", app.getAssetManager().loadTexture("Effects/Toast/LIVES_INC-01.png"));
			debris.setStartColor(new ColorRGBA(0, 1, 0, 1));
			debris.setEndColor(new ColorRGBA(0, 1, 0, 1));
			debris.setStartSize(1);
			debris.setEndSize(1);
			debris.getParticleInfluencer().setInitialVelocity(new Vector3f(0, 1, 0));
			break;
		case LIVE_DEC:
			debris_mat.setTexture("Texture", app.getAssetManager().loadTexture("Effects/Toast/LIVES_DEC-01.png"));
			debris.setStartColor(new ColorRGBA(1, 0, 0, 1));
			debris.setEndColor(new ColorRGBA(1, 0, 0, 1));
			debris.setStartSize(1);
			debris.setEndSize(1);
			debris.getParticleInfluencer().setInitialVelocity(new Vector3f(0, 1, 0));
			break;
		case LIVE_DEC3:
			debris_mat.setTexture("Texture", app.getAssetManager().loadTexture("Effects/Toast/LIVES_DEC-03.png"));
			debris.setStartColor(new ColorRGBA(1, 0, 0, 1));
			debris.setEndColor(new ColorRGBA(1, 0, 0, 1));
			debris.setStartSize(1);
			debris.setEndSize(1);
			debris.getParticleInfluencer().setInitialVelocity(new Vector3f(0, 1, 0));
			break;
//		case REDLASER_HIT_DL:
//			debris_mat.setTexture("Texture", app.getAssetManager().loadTexture("Effects/Toast/LASERbottom.png"));
//			debris.setStartColor(new ColorRGBA(1, 0, 0, 1));
//			debris.setEndColor(new ColorRGBA(1, 0, 0, 1));
//			debris.setStartSize(0);
//			debris.setEndSize(server.application.EchoThread.getRedLaser().size());
//			debris.getParticleInfluencer().setInitialVelocity(new Vector3f(0, 1, 0));
//			
//			break;
//		case REDLASER_HIT_DR:
//			debris_mat.setTexture("Texture", app.getAssetManager().loadTexture("Effects/Toast/LASERTop.png"));
//			debris.setStartColor(new ColorRGBA(1, 0, 0, 1));
//			debris.setEndColor(new ColorRGBA(1, 0, 0, 1));
//			debris.setStartSize(0);
//			debris.setEndSize(server.application.EchoThread.getRedLaser().size());
//			debris.getParticleInfluencer().setInitialVelocity(new Vector3f(0, 1, 0));
//			break;
//		case REDLASER_HIT_R:
//			debris_mat.setTexture("Texture", app.getAssetManager().loadTexture("Effects/Toast/LASER.png"));
//			debris.setStartColor(new ColorRGBA(1, 0, 0, 1));
//			debris.setEndColor(new ColorRGBA(1, 0, 0, 1));
//			debris.setStartSize(0);
//			debris.setEndSize(server.application.EchoThread.getRedLaser().size());
//			debris.getParticleInfluencer().setInitialVelocity(new Vector3f(1, 0, 0));
//			break;
//		case REDLASER_HIT_D:
//			debris_mat.setTexture("Texture", app.getAssetManager().loadTexture("Effects/Toast/LASER90.png"));
//			debris.setStartColor(new ColorRGBA(1, 0, 0, 1));
//			debris.setEndColor(new ColorRGBA(1, 0, 0, 1));
//			debris.setStartSize(0);
//			debris.setEndSize(server.application.EchoThread.getRedLaser().size());
//			debris.getParticleInfluencer().setInitialVelocity(new Vector3f(0, 1, 0));
//			
//			break;
//		case BLUELASER_HIT_R:
//			debris_mat.setTexture("Texture", app.getAssetManager().loadTexture("Effects/Toast/LASER.png"));
//			debris.setStartColor(new ColorRGBA(0, 1, 0, 1));
//			debris.setEndColor(new ColorRGBA(0, 1, 0, 1));
//			debris.setStartSize(0);
//			debris.setEndSize(server.application.EchoThread.getBlueLaser().size()-4);
//			debris.getParticleInfluencer().setInitialVelocity(new Vector3f(1, 0, 0));
//			debris.getParticleInfluencer().setVelocityVariation(0);
//	
//			
//			break;
//		case BLUELASER_HIT_DR:
//			debris_mat.setTexture("Texture", app.getAssetManager().loadTexture("Effects/Toast/LASERTop.png"));
//			debris.setStartColor(new ColorRGBA(0, 1, 0, 1));
//			debris.setEndColor(new ColorRGBA(0, 1, 0, 1));
//			debris.setStartSize(0);
//			debris.setEndSize(server.application.EchoThread.getBlueLaser().size()-4);
//			debris.getParticleInfluencer().setInitialVelocity(new Vector3f(1, 0, 0));
//			debris.getParticleInfluencer().setVelocityVariation(0);
//			break;
//		case BLUELASER_HIT_DL:
//			debris_mat.setTexture("Texture", app.getAssetManager().loadTexture("Effects/Toast/LASERbottom.png"));
//			debris.setStartColor(new ColorRGBA(0, 1, 0, 1));
//			debris.setEndColor(new ColorRGBA(0, 1, 0, 1));
//			debris.setStartSize(0);
//			debris.setEndSize(server.application.EchoThread.getBlueLaser().size()-4);
//			debris.getParticleInfluencer().setInitialVelocity(new Vector3f(1, 0, 0));
//			debris.getParticleInfluencer().setVelocityVariation(0);
//			break;
		case BLUELASER_HIT_D:
			debris_mat.setTexture("Texture", app.getAssetManager().loadTexture("Effects/Toast/LASER90.png"));
			debris.setStartColor(new ColorRGBA(0, 1, 0, 1));
			debris.setEndColor(new ColorRGBA(0, 1, 0, 1));
			debris.setStartSize(0);
			debris.setEndSize(7);
			debris.getParticleInfluencer().setInitialVelocity(new Vector3f(0, 1, 0));
			debris.getParticleInfluencer().setVelocityVariation(0);
			break;
		case CP_REACHED:
			debris_mat.setTexture("Texture", app.getAssetManager().loadTexture("Effects/Toast/CPReached-01.png"));
			debris.setStartColor(new ColorRGBA(0, 1, 0, 1));
			debris.setEndColor(new ColorRGBA(0, 1, 0, 1));
			debris.setStartSize(1);
			debris.setEndSize(1);
			debris.getParticleInfluencer().setInitialVelocity(new Vector3f(0, 1, 0));
			break;	
		default:
			return null;
		}

		debris.setMaterial(debris_mat);
		debris.setImagesX(1);
		debris.setImagesY(1);
	
		debris.setRotateSpeed(0);
		
		debris.getParticleInfluencer().setVelocityVariation(0);
		debris.setLowLife(1.5f);
		debris.setHighLife(1.5f);

		return debris;

	}

}
