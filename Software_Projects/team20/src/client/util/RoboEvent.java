package client.util;

import com.jme3.math.Quaternion;

/**
 * Object for transmitting the necessary informations for executing the
 * animation in the Roboter update loop. EventType should always be declared,
 * but the other variabels depend on the EventType
 * 
 * @author Ali Askari
 *
 */
public class RoboEvent {
	public EventType event;
	public float goalPos; // For MOVE-Events only
	public Quaternion goalRotate;
	public Coordinate position;
	public Compass orientation;
	public boolean flag;

	/**
	 * Constructor of RoboEvent for animation events with a necessary goal point
	 * for the animation such as MOVE or ROTATE
	 * 
	 * @param event
	 *            EventType such as MOVE
	 * @param goal
	 *            position to reach
	 */
	public RoboEvent(EventType event, float goal) {
		this.event = event;
		this.goalPos = goal;
	}

	public RoboEvent(EventType event, float goal, boolean moveByAffected, Compass switchingOrient) {
		this.event = event;
		this.goalPos = goal;
		this.flag = moveByAffected;
		this.orientation = switchingOrient;
	}

	/**
	 * Constructor of RoboEvent for animation events without any goals or other
	 * parameters such as ROTATE, DESTROY or WIN
	 * 
	 * @param event
	 *            EventType such as DESTROY or WIN
	 */
	public RoboEvent(EventType event) {
		this.event = event;
	}

	/**
	 * Contructor of RoboEvent for animations where position and orientation are
	 * of importance, such as COLLIDE_WITH_WALL
	 * 
	 * @param event
	 *            EventType such as COLLIDE_WITH_WALL or
	 *            COLLIDE_WITH_WALL_BACKWARD
	 * @param pos
	 *            The current position of the robot
	 * @param orientation
	 *            The current orientation (compass) of the robot
	 */
	public RoboEvent(EventType event, Coordinate pos, Compass orientation) {
		this.event = event;
		this.position = pos;
		this.orientation = orientation;
		this.flag = false;
	}
}
