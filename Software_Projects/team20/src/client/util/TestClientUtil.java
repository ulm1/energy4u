//package client.util;
//
//import static org.junit.Assert.assertEquals;
//
//import org.junit.Test;
//
//import com.jme3.app.Application;
//import com.jme3.effect.ParticleEmitter;
//import com.jme3.system.AppSettings;
//
//import client.application.GameofStones;
//import client.application.Roboter;
//
//public class TestClientUtil {
//
//	/**
//	 * Test first RoboEvent constructor in RoboEvent.
//	 * 
//	 * @author fabian
//	 */
//	@Test
//	public void TestRoboEventContructorOne() {
//		RoboEvent roEv = new RoboEvent(EventType.CHECKPOINT_REACHED, (float) 15.0);
//		assertEquals("EventType must be equal", EventType.CHECKPOINT_REACHED, roEv.event);
//		assertEquals((float) 15.0, roEv.goalPos, 0.001);
//
//		roEv = new RoboEvent(EventType.COLLIDE_WITH_WALL, (float) -1.0);
//		assertEquals("EventType must be equal", EventType.COLLIDE_WITH_WALL, roEv.event);
//		assertEquals((float) -1.0, roEv.goalPos, 0.001);
//
//		roEv = new RoboEvent(EventType.COLLIDE_WITH_WALL_BACKWARD, (float) 100.0);
//		assertEquals("EventType must be equal", EventType.COLLIDE_WITH_WALL_BACKWARD, roEv.event);
//		assertEquals((float) 100.0, roEv.goalPos, 0.001);
//
//		roEv = new RoboEvent(EventType.DESTROY, (float) 4.5);
//		assertEquals("EventType must be equal", EventType.DESTROY, roEv.event);
//		assertEquals((float) 4.5, roEv.goalPos, 0.001);
//
//		roEv = new RoboEvent(EventType.DISQUALIFIED, (float) -3.0);
//		assertEquals("EventType must be equal", EventType.DISQUALIFIED, roEv.event);
//		assertEquals((float) -3.0, roEv.goalPos, 0.001);
//
//		roEv = new RoboEvent(EventType.FALL_FROM_MAP, (float) 100);
//		assertEquals("EventType must be equal", EventType.FALL_FROM_MAP, roEv.event);
//		assertEquals((float) 100, roEv.goalPos, 0.001);
//
//		roEv = new RoboEvent(EventType.MOVE, (float) -20);
//		assertEquals("EventType must be equal", EventType.MOVE, roEv.event);
//		assertEquals((float) -20, roEv.goalPos, 0.001);
//
//		roEv = new RoboEvent(EventType.MOVE_BACKWARD, (float) 50);
//		assertEquals("EventType must be equal", EventType.MOVE_BACKWARD, roEv.event);
//		assertEquals((float) 50, roEv.goalPos, 0.001);
//
//		roEv = new RoboEvent(EventType.ROTATE_LEFT, (float) 2);
//		assertEquals("EventType must be equal", EventType.ROTATE_LEFT, roEv.event);
//		assertEquals((float) 2, roEv.goalPos, 0.001);
//
//		roEv = new RoboEvent(EventType.ROTATE_RIGHT, (float) -1.222);
//		assertEquals("EventType must be equal", EventType.ROTATE_RIGHT, roEv.event);
//		assertEquals((float) -1.222, roEv.goalPos, 0.001);
//
//		roEv = new RoboEvent(EventType.SPAWN, (float) 5.6788899);
//		assertEquals("EventType must be equal", EventType.SPAWN, roEv.event);
//		assertEquals((float) 5.6788899, roEv.goalPos, 0.001);
//
//		roEv = new RoboEvent(EventType.WIN, (float) -4);
//		assertEquals("EventType must be equal", EventType.WIN, roEv.event);
//		assertEquals((float) -4, roEv.goalPos, 0.001);
//	}
//
//	/**
//	 * Test second RoboEvent constructor in RoboEvent.
//	 * 
//	 * @author fabian
//	 */
//	@Test
//	public void TestRoboEventContructorTwo() {
//		RoboEvent roEv = new RoboEvent(EventType.CHECKPOINT_REACHED);
//		assertEquals("Events must be equal", EventType.CHECKPOINT_REACHED, roEv.event);
//
//		roEv = new RoboEvent(EventType.COLLIDE_WITH_WALL);
//		assertEquals("Events must be equal", EventType.COLLIDE_WITH_WALL, roEv.event);
//	}
//
//	/**
//	 * Test third RoboEvent constructor in RoboEvent.
//	 * 
//	 * @author fabian
//	 */
//	@Test
//	public void TestRoboEventConstructorThree() {
//		RoboEvent roEv = new RoboEvent(EventType.DESTROY, new Coordinate(3, 4), Compass.EAST);
//		assertEquals("Events must be equal", EventType.DESTROY, roEv.event);
//		assertEquals("x position must be equal", 3, roEv.position.x_Value);
//		assertEquals("y position must be equal", 4, roEv.position.y_Value);
//		assertEquals("Orientation must be equal", Compass.EAST, roEv.orientation);
//
//		roEv = new RoboEvent(EventType.ROTATE_RIGHT, new Coordinate(-2, 9), Compass.SOUTH);
//		assertEquals("Events must be equal", EventType.ROTATE_RIGHT, roEv.event);
//		assertEquals("x position must be equal", -2, roEv.position.x_Value);
//		assertEquals("y position must be equal", 9, roEv.position.y_Value);
//		assertEquals("Orientation must be equal", Compass.SOUTH, roEv.orientation);
//	}
//
//	/**
//	 * Test Field constructor in Field.
//	 * 
//	 * @author fabian
//	 */
//	@Test
//	public void TestFieldConstructor() {
//		Field newF = new Field(true, BorderType.EAST, 3);
//		assertEquals("Validity must be equal", true, newF.isValid);
//		assertEquals("Borders must be equal", BorderType.EAST, newF.border);
//		assertEquals("Checkpoints must be equal", 3, newF.checkpoint);
//
//		newF = new Field(false, BorderType.SOUTH, 1);
//		assertEquals("Validity must be equal", false, newF.isValid);
//		assertEquals("Borders must be equal", BorderType.SOUTH, newF.border);
//		assertEquals("Checkpoints must be equal", 1, newF.checkpoint);
//
//		newF = new Field(false, BorderType.SOUTH_EAST, 8);
//		assertEquals("Validity must be equal", false, newF.isValid);
//		assertEquals("Borders must be equal", BorderType.SOUTH_EAST, newF.border);
//		assertEquals("Checkpoints must be equal", 8, newF.checkpoint);
//	}
//
//	/**
//	 * Test Event constructor + setter methods in Event.
//	 * 
//	 * @author fabian
//	 */
//	@Test
//	public void TestEventConstructor() {
//		int[] affUID = { 1, 6, 2 };
//		Event newE = new Event(EventType.DISQUALIFIED, 1, affUID);
//		assertEquals("Events must be equal", EventType.DISQUALIFIED, newE.eventType);
//		assertEquals("IDs must be equal", 1, newE.userID);
//		assertEquals("Length of array must be equal", affUID.length, newE.affectedUID.length);
//
//		int[] affUID2 = {};
//		newE = new Event(EventType.FALL_FROM_MAP, 8, affUID2);
//		assertEquals("Events must be equal", EventType.FALL_FROM_MAP, newE.eventType);
//		assertEquals("IDs must be equal", 8, newE.userID);
//		assertEquals("Length of array must be equal", affUID2.length, newE.affectedUID.length);
//
//		newE.setOrientation(Compass.EAST);
//		newE.setPosition(new Coordinate(3, 4));
//		assertEquals("Orientation must be equal", Compass.EAST, newE.orientation);
//		// assertEquals("x position must be equal")
//
//		newE.setOrientation(Compass.NORTH);
//		newE.setPosition(new Coordinate(-3, 9));
//		assertEquals("Orientation must be equal", Compass.NORTH, newE.orientation);
//
//		newE.setOrientation(Compass.SOUTH);
//		newE.setPosition(new Coordinate(0, 0));
//		assertEquals("Orientation must be equal", Compass.SOUTH, newE.orientation);
//
//		newE.setOrientation(Compass.WEST);
//		newE.setPosition(new Coordinate(1415, 16600));
//		assertEquals("Orientation must be equal", Compass.WEST, newE.orientation);
//
//	}
//
//	@Test
//	public void TestAnimationQueueType() {
//		AnimationQueueType temp = new AnimationQueueType(new Roboter(2, 3, 1), true);
//		assertEquals("Roboter position must be equal", 2, temp.robo.xPos);
//		assertEquals("Roboter position must be equal", 3, temp.robo.yPos);
//		assertEquals("IDs must be equal", 1, temp.robo.RoboterID);
//		assertEquals("Affected must be equal", true, temp.affectedNext);
//
//		temp = new AnimationQueueType(new Roboter(1, 5, 10), false);
//		assertEquals("Roboter position must be equal", 1, temp.robo.xPos);
//		assertEquals("Roboter position must be equal", 5, temp.robo.yPos);
//		assertEquals("IDs must be equal", 10, temp.robo.RoboterID);
//		assertEquals("Affected must be equal", false, temp.affectedNext);
//	}
//
//	@Test
//	public void TestAnimationFactory() {
//
//		GameofStones newRobo = new GameofStones();
//		AppSettings settings = new AppSettings(false);
//
//		newRobo.setSettings(settings);
//
//		ParticleEmitter test = AnimationFactory.getExplosion(newRobo);
//	}
//
//}
