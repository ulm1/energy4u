package client.util;

/**
 * Event Object for updating the game field.
 * 
 * @author fabian
 */
public class Event {

	public EventType eventType;
	public int userID;
	public int[] affectedUID;
	public Compass orientation;
	public Coordinate position;

	/**
	 * Constructor for the Object Event.
	 * 
	 * @param eventType
	 *            The type of the event.
	 * @param userID
	 *            The ID of the user associated with the event.
	 * @param affectedUID
	 *            The list of users affected by the event.
	 */
	public Event(EventType eventType, int userID, int[] affectedUID) {
		this.eventType = eventType;
		this.userID = userID;
		this.affectedUID = affectedUID;
	}

	/**
	 * Setter method for the final orientation of the Roboter
	 * 
	 * @param orientation
	 *            The final orientation
	 * @author fabian
	 */
	public void setOrientation(Compass orientation) {
		this.orientation = orientation;
	}

	/**
	 * Setter method for the final position of the Roboter
	 * 
	 * @param position
	 *            The final position
	 * @author fabian
	 */
	public void setPosition(Coordinate position) {
		this.position = position;
	}

}
