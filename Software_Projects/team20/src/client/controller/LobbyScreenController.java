package client.controller;

import client.application.NetworkThread;
import client.application.GameofStones;
import client.model.Participant;
import com.jme3.app.state.AbstractAppState;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.controls.Label;
import de.lessvoid.nifty.controls.TextField;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

import javax.json.JsonObject;

/**
 *
 * 
 */
public class LobbyScreenController extends AbstractAppState implements ScreenController {

	
    private static Nifty nifty;
    private Screen screen;
    private GameofStones mainApp;
    public static String myUsername;
    static Label textLabel;
    static Label playerLabel;
    static ArrayList<Participant> currentLobby = new ArrayList<Participant>();
    static final  int ROW_COUNT = (int) Math.round(( GameofStones.SCREEN_HEIGHT * 0.9 * 0.9) / 16)-1;

    /**
     * Nifty GUI Node is integrated with the screen.
     * The first method gives you access to the main Nifty instance and the Screen class, the Java
     * representation of the active screen. Nifty will call this method when it initializes the screen. The
     * method is: bind(Nifty nifty, Screen screen).
     *
     */
    @Override
    public void bind(Nifty nifty,Screen screen) {
        this.nifty = nifty;
        this.screen = screen;
		this.mainApp = GameofStones.getMyself();
    }

    /**
     * This method can be called from the xml MenuScreen.
     * 
     */
    @Override
    public void onStartScreen() {
        // nothing
    }

    /**
     * As with the onStartScreen method:
     * Interface that are called in the screen
     * life cycle: onEndScreen().
     * 
     */
    @Override
    public void onEndScreen() {
    	// nothing
    }

    /**
     * The send TextEvent is executed here.
     * 
     */

    Socket connection = null;
       public void sendText() {
    	
		System.out.println("LobbyScreenController sendText is called");
        String message = "";
        int uID = NetworkThread.myId();
        TextField textField = nifty.getCurrentScreen().findNiftyControl("tfInput", TextField.class);
        message = textField.getRealText();
        textField.setText("");
        if(message.equals("")) { return; }
        JsonObject tempJO = NetworkController.encodeMessage(uID, message);
        receiveText(myUsername, message);
        System.out.println(tempJO);
        try {
        	connection = new Socket("localhost",5555);
		     NetworkController.sendData(connection.getOutputStream(),tempJO);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}    
    }
    
    
//    public void sendText() {
//	
//		System.out.println("LobbyScreenController sendText is called");
//        String message = "";
//        TextField textField = nifty.getCurrentScreen().findNiftyControl("tfInput", TextField.class);
//        message = textField.getRealText();
//        textField.setText("");
//        // don't send empty messages
//        if(message.equals("")) { return; }
//        // for testing purposes only
//         receiveText(myUsername, message);//*/
//         
//    }
    


    /**
     * This method is executed when a text comes from the NetworkController class.
     * @author aron, soeren
     * @param username
     * @param message
     *
     */

    public static void receiveText(String Username, String message) {
    	// read old text from lobby
        try {
            textLabel = nifty.getCurrentScreen().findNiftyControl("panel_text", Label.class);
        } catch (Exception e) {
            System.out.println("didn't find textLabel");
        }
        String old = textLabel.getText();
        System.out.println(old);
        String[] tempParts = old.split("\n");
        if (tempParts.length >= ROW_COUNT) {
            old = "";
            for (int i = tempParts.length; i > tempParts.length - ROW_COUNT; i--) {
                if (old.equals("")) {
                    old = tempParts[i-1];
                } else {
                    old = tempParts[i-1] + "\n" + old;
                }
            }
        }
        if (Username.equals("")) {
        	// write messages from system (someone joined or left the lobby)
            if (old.equals("")) {
                textLabel.setText( message);
            } else {
                textLabel.setText(old + "\n" +  message );
            }
        } else {
        	// write any other messages from users
            if (old.equals("")) {
                textLabel.setText("<" + Username + ">: " + message);
            } else {
                textLabel.setText(old + "\n" + "<" + Username + ">: " + message);
            }
        }
        nifty.getCurrentScreen().layoutLayers();
    }

    /**
     * Here the participants are entered into the lobby.
     * @author aron, soeren
     * @param names
     */

    private static void writeLobbyList(ArrayList<String> names) {
        names = sortStrings(names);
        // convert array of strings to one big string 
        String outputString = "";
        for (String Username : names) {
            if (outputString.equals("")) {
                outputString += " " + Username;
            } else {
                outputString += "\n" + " " + Username;
            }
        }
        // write the usernames
        playerLabel = nifty.getCurrentScreen().findNiftyControl("playerLabel", Label.class);
        playerLabel.setText(outputString);//*/
    }

    /**
     * Here the message is sorted.
     * @author aron, soeren
     * @param sList
     * @return
     */

    private static ArrayList<String> sortStrings(ArrayList<String> sList) {
    	// catch the simple cases
        if (sList == null) return null;
        if (sList.size() == 1) return sList;

        // do the sorting stuff
        String temp;
        for (int i = 0; i < sList.size() - 1; i++) {
            for (int j = i + 1; j < sList.size(); j++) {
                if (sList.get(i).compareToIgnoreCase(sList.get(j)) > 0) {
                    temp = sList.get(i);
                    sList.set(i, sList.get(j));
                    sList.set(j, temp);
                }
            }
        }
        return sList;
    }

    /**
     * Here the messages are transferred from the NetworkController.
     * @param msg
     * @param userName
     * update the lobby for incoming message Msg
     */
    public static void updateOnIncomingMessage(String msg, String userName) {
        System.out.println("LobbyScreenController updateOnIncomingMessage is called");

        receiveText(userName, msg);
    }

    /**
     * update the lobby for incoming lobby status 
     *
     * @param partList
     */
    public static void updateOnLobbyStatus(ArrayList<Participant> partList) {
    	// test if someone has left the lobby
        for (Participant oldie : currentLobby) {
        	boolean hasLeft = true;
        	for (Participant newby : partList) {
        		if (oldie.userID == newby.userID) { 
        			hasLeft = false;
        		}
        	}
        	if (hasLeft) {
        		receiveText("", oldie.partName + " hat die Lobby verlassen.");
        	}
        }

        // test if someone has joined the lobby
        for (Participant newby : partList) {
        	boolean hasJoined = true;
        	for (Participant oldie : currentLobby) {
        		if (newby.userID == oldie.userID) { 
        			hasJoined = false;
        		}
        	}
        	if (hasJoined) {
        		receiveText("", newby.partName + " hat die Lobby soeben betreten. Bitte den Server zur Spielteilnahme starten.");
        	}
        }

        // update list of current lobby
        currentLobby = partList; 
        ArrayList<String> partNames = new ArrayList<String>();
        for (Participant p : partList) {
            partNames.add(p.partName);
        }
        writeLobbyList(partNames);
    }

}
