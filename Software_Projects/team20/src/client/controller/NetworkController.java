package client.controller;

import java.awt.Dimension;
import java.awt.geom.Dimension2D;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonBuilderFactory;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;

import client.application.Roboter;
import client.model.Map;
import client.model.Participant;
import client.model.Player;
import client.util.Cards;
import client.util.Compass;
import client.util.Coordinate;
import client.util.Event;
import client.util.EventType;

public class NetworkController {

	public static final String PROTOCOL_VERSION = "1.0.0";

	public static JsonBuilderFactory factory = Json.createBuilderFactory(null);
	static ArrayList<Cards> cardl = new ArrayList<Cards>();

	// TESTED
	/**
	 * Create an instance of Map from a given JsonObject of type MAP.
	 * 
	 * @param jsonOb
	 *            The JsonObject containing the information about the map.
	 * @return The Map created from the input.
	 * @author fabian
	 */
	public static Map decodeGameConfigMessage(JsonObject jsonOb) {

		/**
		 * Handle the 'simple' variables.
		 */
		int minP = 1;
		int maxP = 1;
		int cT = 1;
		int maxS = 1;

		Dimension2D dim = new Dimension(
				jsonOb.getJsonObject("body").getJsonObject("map").getJsonObject("grid").getInt("width"),
				jsonOb.getJsonObject("body").getJsonObject("map").getJsonObject("grid").getInt("height"));


		/**
		 * Handle the checkpoints.
		 */
	

		// ronnie
		/**
		 * Handle the Health.
		 */


		/**
		 * Handle the Portals.
		 */
		JsonArray portalsr = jsonOb.getJsonObject("body").getJsonObject("map").getJsonArray("portal");
		ArrayList<Coordinate> portalsp = new ArrayList<Coordinate>();


		return new Map(minP, maxP, cT, maxS, dim);
	}

	// TESTED
	/**
	 * Extract a list of participants from a JsonObject of type LOBBY_STATUS
	 * 
	 * @param jsonOb
	 *            The JsonObject to decode.
	 * @return The list of participants.
	 * @author fabian
	 */

	public static ArrayList<Participant> decodeLobbyStatus(JsonObject jsonOb) {

		ArrayList<Participant> partList = new ArrayList<Participant>();

		JsonArray jsonArr = jsonOb.getJsonObject("body").getJsonArray("players");

		for (int j = 0; j < jsonArr.size(); j++) {

			JsonObject tempJsonOb = jsonArr.getJsonObject(j);

			String name = tempJsonOb.getString("name");
			// String role = tempJsonOb.getString("role");
			boolean ai = tempJsonOb.getBoolean("ai");
			int uid = tempJsonOb.getInt("uid");
			partList.add(new Player(name, ai, uid));

			// if (role.equals("USER")) {

			// } else {
			// partList.add(new Watcher(name, ai, uid));
			// }
		}
		return partList;
	}

	// initialPositions: contains the start positions of the robots as
	// Coordinate
	// finalPositions: contains the final positions of the robots as Coordinate
	// events: a list of events encoded as type EventType

	// TESTED
	/**
	 * Decode a JsonObject containing a round result.
	 * 
	 * @param jsonOb
	 *            The JsonObject containing the information.
	 * @author fabian
	 */
	public static ArrayList<Event> decodeRoundResult(JsonObject jsonOb) {

		JsonArray eventList = jsonOb.getJsonObject("body").getJsonArray("events");
		JsonArray playerList = jsonOb.getJsonObject("body").getJsonArray("finalStates");

		ArrayList<Event> returnEvents = new ArrayList<Event>();

		for (int j = 0; j < eventList.size(); j++) {
			JsonArray uidTemp = eventList.getJsonObject(j).getJsonArray("affectedUID");

			int[] newArr;
			if (uidTemp == null) {
				int[] affUID = new int[0];
				newArr = affUID;
			} else {

				int[] affUID = new int[uidTemp.size()];
				for (int k = 0; k < uidTemp.size(); k++) {
					affUID[k] = uidTemp.getInt(k);
				}
				newArr = affUID;
			}
			int uID = eventList.getJsonObject(j).getInt("uid");
			EventType event;
			String tempStrEvent = eventList.getJsonObject(j).getString("eventType");
			// System.out.println(tempStrEvent);
			
			switch(tempStrEvent){
			case "MOVE":
				event = EventType.MOVE;
				break;
			case "REDLASER_HIT_DL":
				event = EventType.REDLASER_HIT_DL;
				break;
			case "REDLASER_HIT_DR":
				event = EventType.REDLASER_HIT_DR;
				break;
			case "REDLASER_HIT_R":
				event = EventType.REDLASER_HIT_R;
				break;
			case "REDLASER_HIT_D":
				event = EventType.REDLASER_HIT_D;
				break;
				
			case "BLUELASER_HIT_R":
				event = EventType.BLUELASER_HIT_R;
				break;
				
			case "BLUELASER_HIT_DR":
				event = EventType.BLUELASER_HIT_DR;
				break;
			case "BLUELASER_HIT_DL":
				event = EventType.BLUELASER_HIT_DL;
				break;
			case "BLUELASER_HIT_D":
				event = EventType.BLUELASER_HIT_D;
				break;
			case "COLLIDE_WITH_ROBO":
				event = EventType.COLLIDE_WITH_Robo;
				break;
			case "ROTATE180":
				event = EventType.ROTATE180;
				break;
			case "MOVE_BACKWARD":
				event = EventType.MOVE_BACKWARD;
				break;
			case "BACKWARD_1":
				event = EventType.MOVE_BACKWARD;
				break;
			case "COLLIDE_WITH_WALL":
				event = EventType.COLLIDE_WITH_WALL;
				break;
			case "COLLIDE_WITH_WALL_BACKWARD":
				event = EventType.COLLIDE_WITH_WALL_BACKWARD;
				break;
			case "CHECKPOINT_REACHED":
				event = EventType.CHECKPOINT_REACHED;
				break;
			case "FALL_FROM_MAP":
				event = EventType.FALL_FROM_MAP;
				break;
			case "ROTATE_LEFT":
				event = EventType.ROTATE_LEFT;
				break;
			case "HEALTH":
				event = EventType.HEALTH;
				break;
			case "ROTATE_RIGHT":
				event = EventType.ROTATE_RIGHT;
				break;
			case "TURN_RIGHT":
				event = EventType.ROTATE_RIGHT;
				break;
			case "SPAWN":
				event = EventType.SPAWN;
				break;
			case "DESTROY":
				event = EventType.DESTROY;
				break;	
			case "WIN":
				event = EventType.WIN;
				break;
			default:
				event = EventType.DISQUALIFIED;
				break;
				
				
			}
			
			
			Event tempEvent = new Event(event, uID, newArr);

			int xPos = playerList.getJsonObject(j).getInt("fieldX");
			int yPos = playerList.getJsonObject(j).getInt("fieldY");
			String or = playerList.getJsonObject(j).getString("orientation");

			tempEvent.setPosition(new Coordinate(xPos, yPos));
			
			
//			if (tempStrEvent.equals("MOVE")) {
//				event = EventType.MOVE;
//			} else if (tempStrEvent.equals("REDLASER_HIT_DL")) {
//				event = EventType.REDLASER_HIT_DL;
//			} else if (tempStrEvent.equals("REDLASER_HIT_DR")) {
//				event = EventType.REDLASER_HIT_DR;
//			} else if (tempStrEvent.equals("REDLASER_HIT_R")) {
//				event = EventType.REDLASER_HIT_R;
//			} else if (tempStrEvent.equals("REDLASER_HIT_D")) {
//				event = EventType.REDLASER_HIT_D;
//			} else if (tempStrEvent.equals("BLUELASER_HIT_R")) {
//				event = EventType.BLUELASER_HIT_R;
//			} else if (tempStrEvent.equals("BLUELASER_HIT_DR")) {
//				event = EventType.BLUELASER_HIT_DR;
//			} else if (tempStrEvent.equals("BLUELASER_HIT_DL")) {
//				event = EventType.BLUELASER_HIT_DL;
//			} else if (tempStrEvent.equals("BLUELASER_HIT_D")) {
//				event = EventType.BLUELASER_HIT_D;
//
//			} else if (tempStrEvent.equals("COLLIDE_WITH_Robo")) {
//				event = EventType.COLLIDE_WITH_Robo;
//			} else if (tempStrEvent.equals("ROTATE180")) {
//				event = EventType.ROTATE180;
//			} else if (tempStrEvent.equals("MOVE3")) {
//				event = EventType.MOVE3;
//			} else if (tempStrEvent.equals("MOVE2")) {
//				event = EventType.MOVE2;
//			} else if (tempStrEvent.equals("MOVE_BACKWARD") || (tempStrEvent.equals("BACKWARD_1"))) {
//				event = EventType.MOVE_BACKWARD;
//			} else if (tempStrEvent.equals("COLLIDE_WITH_WALL")) {
//				event = EventType.COLLIDE_WITH_WALL;
//			} else if (tempStrEvent.equals("COLLIDE_WITH_WALL_BACKWARD")) {
//				event = EventType.COLLIDE_WITH_WALL_BACKWARD;
//			} else if (tempStrEvent.equals("CHECKPOINT_REACHED")) {
//				event = EventType.CHECKPOINT_REACHED;
//			} else if (tempStrEvent.equals("FALL_FROM_MAP")) {
//				event = EventType.FALL_FROM_MAP;
//			} else if (tempStrEvent.equals("ROTATE_LEFT")) {
//				event = EventType.ROTATE_LEFT;
//			} else if (tempStrEvent.equals("HEALTH")) {
//				event = EventType.HEALTH;
//			} else if (tempStrEvent.equals("ROTATE_RIGHT")) {
//				event = EventType.ROTATE_RIGHT;
//			} else if (tempStrEvent.equals("TURN_RIGHT")) {
//				event = EventType.ROTATE_RIGHT;
//			} else if (tempStrEvent.equals("SPAWN")) {
//				event = EventType.SPAWN;
//			} else if (tempStrEvent.equals("DESTROY")) {
//				event = EventType.DESTROY;
//			} else if (tempStrEvent.equals("WIN")) {
//				event = EventType.WIN;
//			} else {
//				event = EventType.DISQUALIFIED;
//			}

//			Event tempEvent = new Event(event, uID, newArr);
//
//			int xPos = playerList.getJsonObject(j).getInt("fieldX");
//			int yPos = playerList.getJsonObject(j).getInt("fieldY");
//			String or = playerList.getJsonObject(j).getString("orientation");

//			tempEvent.setPosition(new Coordinate(xPos, yPos));

			if (or.equals("LEFT")) {
				tempEvent.setOrientation(Compass.WEST);
			} else if (or.equals("DOWN")) {
				tempEvent.setOrientation(Compass.SOUTH);
			} else if (or.equals("RIGHT")) {
				tempEvent.setOrientation(Compass.EAST);
			} else {
				tempEvent.setOrientation(Compass.NORTH);
			}
			returnEvents.add(tempEvent);

		}
		return returnEvents;
	}

	public static ArrayList<Player> decodeGameInit(JsonObject jsonOb) {
		ArrayList<Player> playList = new ArrayList<Player>();
		JsonArray userList = jsonOb.getJsonObject("body").getJsonArray("users");
		JsonArray stateList = jsonOb.getJsonObject("body").getJsonArray("states");
		// System.out.println("Size of userList " + userList.size());
		// System.out.println("Size of stateList " + stateList.size());

		for (int k = 0; k < userList.size(); k++) {
			String name = userList.getJsonObject(k).getString("name");
			String role = userList.getJsonObject(k).getString("role");
			boolean isAI = userList.getJsonObject(k).getBoolean("ai");
			int uid = userList.getJsonObject(k).getInt("uid");
			// System.out.println(k);
			if (role.equals("USER")) {
				Player newPlay = new Player(name, isAI, uid);
				playList.add(newPlay);
			}
		}

		for (int k = 0; k < stateList.size(); k++) {
			int xVal = stateList.getJsonObject(k).getInt("fieldX");
			int yVal = stateList.getJsonObject(k).getInt("fieldY");
			int checkP = stateList.getJsonObject(k).getInt("checkpoint");
			int lives = stateList.getJsonObject(k).getInt("lives");
			int stateID = stateList.getJsonObject(k).getInt("uid");
			Roboter newRobot = new Roboter(xVal, yVal, stateID);
			playList.get(k).setRoboter(newRobot);
			playList.get(k).getRoboter().setLives(lives);
			playList.get(k).getRoboter().setLastCheckpoint(checkP);
			// System.out.println(k);
		}
		return playList;
	}

	// TESTED
	/**
	 * Create a JsonObject encoding a String containing a message.
	 * 
	 * @param uID
	 *            The user who sent the message.
	 * @param Msg
	 *            The content of the message.
	 * @return The JsonObject created.
	 * @author fabian
	 */
	public static JsonObject encodeMessage(int uID, String Msg) {

		long currentTime = System.currentTimeMillis();

		JsonObject jsonOb = factory.createObjectBuilder()
				.add("head",
						factory.createObjectBuilder().add("sent", currentTime).add("protocolVersion", PROTOCOL_VERSION)
								.add("type", "MESSAGE"))
				.add("body", factory.createObjectBuilder().add("uid", uID).add("content", Msg)).build();
		return jsonOb;
	}

	// testpurpose ronnie
	public static JsonObject encodeMessage(String Msg) {

		long currentTime = System.currentTimeMillis();

		JsonObject jsonOb = factory.createObjectBuilder()
				.add("head",
						factory.createObjectBuilder().add("sent", currentTime).add("protocolVersion", PROTOCOL_VERSION)
								.add("type", "MESSAGE"))
				.add("body", factory.createObjectBuilder().add("content", Msg)).build();
		return jsonOb;
	}

	/**
	 * Due to an error encounter from the server display an error message to the
	 * user and exit the game.
	 * 
	 * @param jsonOb
	 *            The JsonObject that caused the error.
	 * @author fabian
	 */
	public static void handleWatcherErrors(JsonObject jsonOb) {

		String errorType = jsonOb.getJsonObject("body").getString("errorType");

		if (errorType.equals("GAME_ABORT")) {
			System.out.println("Client :: The server ended the game");
		} else if (errorType.equals("UNSUPPORTED_PROTOCOL_VERSION")) {
			System.out.println("Client :: I use the wrong protocol");
		} else if (errorType.equals("PROTOCOL_VERSION")) {
			System.out.println("Client :: The order of the protocol is wrong");
		} else {
			System.out.println("Client :: Got unexpected error message");
		}

		// display an error message indicating that the program is ended

		System.out.println("Client :: Due to an error the game is ended");

		System.exit(-1);
	}

	// TESTED
	/**
	 * Send a JsonObject on a given output stream.
	 * 
	 * @param outStream
	 *            The stream on which the message is to be sent.
	 * @param data
	 *            The data to be sent.
	 * @author fabian
	 */
	public static void sendData(OutputStream outStream, JsonObject data) {
		String stringToSend = getNumOfBytes(data) + data.toString() + "\n";
		try {
			Writer out = new BufferedWriter(new OutputStreamWriter(outStream, "UTF8"));

			out.append(stringToSend);
			out.flush();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// public static void sendData(OutputStream outStream, JsonObject data) {
	// String jsonString = data.toString();
	// int length = jsonString.length();
	// // System.out.println("L�nge der nachricht: " + length);
	// ByteBuffer bb = ByteBuffer.allocate(4).order(ByteOrder.BIG_ENDIAN);
	// bb.putInt(length);
	// byte[] lengtho = bb.array();
	// try {
	// outStream.write(lengtho);
	//
	// ;
	//
	// // byte[] lengthInByte =
	// //
	// ByteBuffer.allocate(4).order(ByteOrder.BIG_ENDIAN).putInt(length).array();
	// byte[] jsonData = jsonString.getBytes(Charset.forName("UTF-8"));
	// // int b = jsonData.length;
	// // DataOutputStream dis = new DataOutputStream(outStream);
	// // dis.writeInt(b);
	// outStream.write(jsonData);
	// outStream.flush();
	// // outStream.close();
	//
	// } catch (IOException e) {
	// e.printStackTrace();
	// }
	// }

	// TESTED
	/**
	 * Create a JsonObject of type LOGIN to be sent to the server.
	 * 
	 * @param name
	 *            The name chosen for the client.
	 * @param role
	 *            The role of the client.
	 * @param isAI
	 *            Boolean describing whether the client is an AI.
	 * @return The JsonObject created.
	 * @author fabian
	 */
	public static JsonObject encodeLoginRequest(String name,boolean player, boolean role) {

		long currentTime = System.currentTimeMillis();

		JsonObject jsonOb = factory.createObjectBuilder()
				.add("header",
						factory.createObjectBuilder().add("UUID", "1").add("name", name).add("timestamp", currentTime).add("messageType", "login"))
		
				.add("body", factory.createObjectBuilder().add("isPlayer", player).add("isHuman", role)).build();
		System.out.println(jsonOb);
		return jsonOb;
		
	}

	// TESTED
	/**
	 * Determine the number of bytes of a JsonObject.
	 * 
	 * @param data
	 *            The JsonObject of which the size if to be determined.
	 * @return The size of the JsonObject as an Integer
	 * @author fabian
	 */
	public static int getNumOfBytes(JsonObject data) {

		String jsonString = data.toString();
		int stringLength = 0;

		try {
			byte[] utf8Bytes = jsonString.getBytes("UTF-8");
			stringLength = utf8Bytes.length;
		} catch (Exception e) {
			System.out.println("Problems with determining size of JsonObject");
			e.printStackTrace();
		}
		return stringLength;
	}

	public static JsonObject CardSelection(Cards[] myCards, String orientation, int ownUserID) {

		JsonObject jsonObb = null;
		int cardCode = 0;
		JsonObjectBuilder jsonOb = null;
		long currentTime = System.currentTimeMillis();
		JsonArrayBuilder jsonArray2 = Json.createArrayBuilder();

		jsonOb = factory.createObjectBuilder().add("head", factory.createObjectBuilder().add("sent", currentTime)
				.add("protocolVersion", PROTOCOL_VERSION).add("type", "CARD_SELECTION"));

		// System.out.println("L�����NGE " + myCards.length );

		for (Cards c : myCards) {

			if (c.cardType.equals("TURN_RIGHT")) {
				cardCode = 200 + ownUserID;
			}

			if (c.cardType.equals("TURN_LEFT")) {
				cardCode = 300 + ownUserID;

			}
			if (c.cardType.equals("TURN_180")) {
				cardCode = 100 + ownUserID;

			}

			if (c.cardType.equals("FORWARD_1")) {
				cardCode = 500 + ownUserID;

			}

			if (c.cardType.equals("FORWARD_2")) {
				cardCode = 600 + ownUserID;

			}

			if (c.cardType.equals("FORWARD_3")) {
				cardCode = 700 + ownUserID;

			}

			if (c.cardType.equals("BACKWARD_1")) {
				cardCode = 400 + ownUserID;
			}

			Cards card = new Cards(c.cardType, cardCode, orientation);
			// if (!cardl.contains(card)) {
			cardl.add(card);

			for (int i = cardl.size() - 1; i < 5;) {
				jsonArray2.add(factory.createObjectBuilder().add("cardType", cardl.get(i).cardType).add("cardCode",
						cardl.get(i).cardCode));
				break;

			}
			if (cardl.size() >= 5) {
				jsonOb.add("body",
						factory.createObjectBuilder().add("orientation", orientation).add("cards", jsonArray2));
				jsonObb = jsonOb.build();
				// return jsonObb;
			}
			// }
		}

		cardl = new ArrayList<Cards>();

		return jsonObb;
	}

	/**
	 * Get a message from a specific input stream.
	 * 
	 * @param inStream
	 *            An InputStream on which the message is to be received.
	 * @return JsonObject containing the contents of the message.
	 * @throws IOException
	 *             Thrown, if receiving a message fails.
	 * @author fabian
	 */
	public static JsonObject receiveMessage(InputStream inStream) throws IOException {

		BufferedReader in = null;

		try {
			in = new BufferedReader(new InputStreamReader(inStream, "UTF8"));
		} catch (Exception e) {
			e.printStackTrace();
		}

		String tempString;

		while (true) {
			tempString = in.readLine();
			if (tempString != null) {
				break;
			}
		}

		String num = "";

		while (true) {
			if (tempString.charAt(0) == '{') {
				break;
			}
			num = num + tempString.charAt(0);
			tempString = tempString.substring(1);
		}

		// 'num' contains the size of the received message

		JsonReader jsonReader = Json.createReader(new StringReader(tempString));
		JsonObject jsonOb = jsonReader.readObject();

		return jsonOb;
	}

	// TESTED
	/**
	 * Stop the running program for a fixed amount of time.
	 * 
	 * @param timeToWait
	 *            Number of milliseconds to program will wait
	 * @author fabian
	 */
	public static void wait(int timeToWait) {
		try {
			TimeUnit.MILLISECONDS.sleep(timeToWait);
		} catch (InterruptedException e) {
			System.exit(-1);
		}
	}

	public static ArrayList<Cards> decodeCardSet(JsonObject jsonOb) {

		ArrayList<Cards> myCards = new ArrayList<Cards>();
		JsonArray cardList = jsonOb.getJsonObject("body").getJsonArray("cards");
		for (int k = 0; k < 9; k++) {
			String cardType = cardList.getJsonObject(k).getString("cardType");
			int cardCode = cardList.getJsonObject(k).getInt("cardCode");

			// System.out.println("So ein schei� " + cardType + cardCode);

			Cards card = new Cards(cardType, cardCode);
			myCards.add(card);
		}

		return myCards;

	}

}