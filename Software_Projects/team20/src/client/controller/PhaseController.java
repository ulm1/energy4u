package client.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonBuilderFactory;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.JsonValue;

import com.jme3.app.state.AbstractAppState;

import client.application.ButtonDialog;
import client.application.GameofStones;
import client.util.PhaseType;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.controls.Label;
import de.lessvoid.nifty.controls.TextField;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.elements.render.TextRenderer;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;


public class PhaseController extends AbstractAppState implements ScreenController {

	private Nifty nifty;
	private Screen screen;
	private GameofStones mainApp;
	private Element niftyElement;
	static JsonObject j;

	@Override
	public void bind(Nifty nifty, Screen screen) {
		this.nifty = nifty;
		this.screen = screen;
		this.mainApp = GameofStones.getMyself();
		
		
		String phase = "RECRUITMENT";
		
		
		if(PhaseType.SUPPLY.equals(phase)){
		
		File json2InputFile = new File("JSON_Files/Versorgungsphase.json");

		InputStream iss = null;
		try {
			iss = new FileInputStream(json2InputFile);
		} catch (FileNotFoundException e1) {

			e1.printStackTrace();
		}
		JsonReader reader3 = Json.createReader(iss);
		JsonObject jsonOb1 = reader3.readObject();
		
		JsonObject income = jsonOb1.getJsonObject("supplyPhase").getJsonArray("supplyResults").getJsonObject(0);
		int einkommen = income.getJsonArray("incomeTerritories").getJsonObject(0).getInt("income");
		String e = Integer.toString(einkommen);
		
		
		 JsonObject in = jsonOb1.getJsonObject("supplyPhase").getJsonArray("supplyResults").getJsonObject(0);
	     String unterhalt =Integer.toString(in.getInt("upkeep")); 
	     String vorat  =Integer.toString(in.getInt("stock")); 
	    	
		
	     TextField mainTextField = screen.findNiftyControl("mainTextField", TextField.class);
	     mainTextField.setText(e);
		 mainTextField.disable();
	     
	     
	     
	    TextField mainTextField3 = screen.findNiftyControl("mainTextField3", TextField.class);
		mainTextField3.setText(unterhalt);
		mainTextField3.disable();
		
	    TextField mainTextField2 = screen.findNiftyControl("mainTextField2", TextField.class);
	    mainTextField2.setText(vorat);
	    mainTextField2.disable();
	    TextField fahn = screen.findNiftyControl("mainTextField4", TextField.class);
	    if(vorat.equals("0") && unterhalt.equals("0")){
	    	
	    	 
	    	   fahn.setText("YES");
	    	   fahn.disable();
	   	    
	    }
	    
	    else {
	    	
	    	fahn.disable();
	    	
	    }
		}
		
		if(PhaseType.RECRUITMENT.equals(phase)){

			
			
			
			
		}
		
		
		
		
		
	    
		
		
//		Label mainLabel = screen.findNiftyControl("mainLabel", Label.class);
//		mainLabel.setText("hallooo");
		
		 }
	
	


	@Override
	public void onStartScreen() {
		// nothing
	}


	@Override
	public void onEndScreen() {
		// nothing
	}
	public void Ok (String nextScreen){
		
		nifty.gotoScreen(nextScreen);
		
	
	}
	public void Rekrutieren (String nextScreen){
		
		
		     TextField mainTextField3 = screen.findNiftyControl("terTextField", TextField.class);
	
		     
		   System.out.println(mainTextField3.getText());

		   
	
			
		
		
		nifty.gotoScreen(nextScreen);
	
	}
	public void Bewegen (String nextScreen){
		
		nifty.gotoScreen(nextScreen);
	
	}
	public void QuitGame (String nextScreen){
		
		nifty.gotoScreen(nextScreen);
	
	}
	   
			public static JsonObject encodeRecuring(int id, int sword, int bow, int knight) {
				JsonBuilderFactory factory = Json.createBuilderFactory(null);
				
						JsonObjectBuilder jsonOb = factory.createObjectBuilder().add("recruitmentPhase", factory.createArrayBuilder() ) 
						.add("recruitments",factory.createObjectBuilder()).add("territoryId", id).add("recruitedSword",sword ).add("recruitedBow",bow)
							.add("recruitedKnight:", knight);
				JsonObject jsonObb = jsonOb.build();

				return jsonObb;
			}
	

}
