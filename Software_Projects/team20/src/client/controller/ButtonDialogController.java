package client.controller;

import client.application.GameofStones;
import client.application.ButtonDialog;
import com.jme3.app.state.AbstractAppState;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.NiftyEventSubscriber;
import de.lessvoid.nifty.controls.RadioButton;
import de.lessvoid.nifty.controls.RadioButtonGroupStateChangedEvent;
import de.lessvoid.nifty.controls.TextField;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.elements.render.ImageRenderer;
import de.lessvoid.nifty.elements.render.TextRenderer;
import de.lessvoid.nifty.render.NiftyImage;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;
import de.lessvoid.nifty.tools.Color;

import java.net.Socket;
import java.util.IllegalFormatException;

public class ButtonDialogController extends AbstractAppState implements ScreenController {

	private Nifty nifty;
	private Screen screen;
	private GameofStones mainApp;
	private Element popupError;

	@Override
	public void bind(Nifty nifty, Screen screen) {
		this.nifty = nifty;
		this.screen = screen;
		this.mainApp = GameofStones.getMyself();
		// create (but don't yet show) error popup
		createErrorPopup();
	}


	@Override
	public void onStartScreen() {
		// nothing
	}


	@Override
	public void onEndScreen() {
		// nothing
	}

	public void createErrorPopup() {
		if (popupError == null) {
			popupError = this.nifty.createPopup("popupError"); // create with id
		}
	}

	public void showErrorPopup(String title, String message) {
		ButtonDialog.popupErrorShows(true);

		TextRenderer errorTitle = popupError.findElementById("errorTitle").getRenderer(TextRenderer.class);
		TextRenderer errorMessage = popupError.findElementById("errorMessage").getRenderer(TextRenderer.class);

		errorTitle.setText(title);
		errorMessage.setText(message);

		nifty.showPopup(nifty.getCurrentScreen(), popupError.getId(), null);
	}


	public void hideErrorPopup() {
		ButtonDialog.popupErrorShows(false);
		nifty.closePopup(popupError.getId());
	}

	
	public void popupExit() {
		hideErrorPopup();
	}
	
	public void Exit() {
		System.exit(0);
	}

	public void btnConnectClick() {
		// check validity and throw error message if necessary
				showErrorPopup("Attention!",
						"Are you sure you want to Exit? You will forfit the Game!");
			
		}
	}

