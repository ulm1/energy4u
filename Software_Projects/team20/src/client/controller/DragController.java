package client.controller;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.json.JsonObject;

import com.jme3.app.state.AbstractAppState;
import com.jogamp.common.util.InterruptSource.Thread;

import client.application.NetworkThread;
import client.application.GameofStones;
import client.application.StartScreen;
import client.util.Cards;
import client.util.Event;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.NiftyEventSubscriber;
import de.lessvoid.nifty.NiftyIdCreator;
import de.lessvoid.nifty.builder.ImageBuilder;
import de.lessvoid.nifty.builder.LayerBuilder;
import de.lessvoid.nifty.builder.PanelBuilder;
import de.lessvoid.nifty.builder.TextBuilder;
import de.lessvoid.nifty.controls.Button;
import de.lessvoid.nifty.controls.Draggable;
import de.lessvoid.nifty.controls.DraggableDragCanceledEvent;
import de.lessvoid.nifty.controls.DraggableDragStartedEvent;
import de.lessvoid.nifty.controls.Droppable;
import de.lessvoid.nifty.controls.DroppableDroppedEvent;
import de.lessvoid.nifty.controls.Label;
import de.lessvoid.nifty.controls.RadioButton;
import de.lessvoid.nifty.controls.WindowClosedEvent;
import de.lessvoid.nifty.controls.button.builder.ButtonBuilder;
import de.lessvoid.nifty.controls.dragndrop.builder.DraggableBuilder;
import de.lessvoid.nifty.controls.window.builder.WindowBuilder;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;
import de.lessvoid.nifty.tools.Color;

public class DragController extends AbstractAppState implements ScreenController {

	private static Nifty nifty;
	private static Screen screen;
	private GameofStones mainApp;
	private Button resetButton;
	private Button startButton;

	static ArrayList<String> eTarg = new ArrayList<String>(5);

	// public static ArrayList<Cards> myCards = new ArrayList<Cards>(5);

	public static Cards[] myCards = new Cards[5];
	static String[] anweisung = { "FORWARD_3", "FORWARD_2", "FORWARD_1", "BACKWARD_1", "TURN_RIGHT", "TURN_LEFT",
			"TURN_180" };
	static String[] anweisung2 = new String[9];
	static String anweisung3;
	// static DroppableDroppedEvent[] dropp;
	// static String[] id;
	static ArrayList<DroppableDroppedEvent> dropp = new ArrayList<DroppableDroppedEvent>();
	static ArrayList<String> idE = new ArrayList<String>();
	static ArrayList<Element> cards = new ArrayList<Element>();
	static ArrayList<String> idE1 = new ArrayList<String>();
	static String[] cardID = { "Position" + 0, "Position" + 1, "Position" + 2, "Position" + 3, "Position" + 4,
			"Position" + 5, "Position" + 6, "Position" + 7, "Position" + 8 };

	@Nonnull
	private final static Random random = new Random();

	@Nullable
	private Droppable trash;

	@Nullable
	private Droppable evilStuff;

	@Override
	public void bind(Nifty nifty, Screen screen) {
		this.nifty = nifty;
		this.screen = screen;
		this.mainApp = GameofStones.getMyself();
		// trash = findDroppable("Trash");
		// Droppable goodStuff = findDroppable("GoodStuff");
		// evilStuff = findDroppable("EvilStuff");

		// this filter demonstrates a drop filter. in this case you can't drag
		// something from the "EvilStuff"
		// dropable to the "GoodStuff" dropable
		// goodStuff.addFilter(new DroppableDropFilter() {
		// @Override
		// public boolean accept(
		// @Nullable final Droppable source,
		// @Nonnull final Draggable draggable,
		// @Nonnull final Droppable target) {
		// return source != evilStuff;
		// }
		// });
	}

	
	
	public void Move() {

		// select radioButton if only matching label was clicked
		RadioButton rbWatcher = screen.findNiftyControl("rbWatcher", RadioButton.class);
		if (rbWatcher.isActivated() == false) {
			rbWatcher.select(); // triggers RadioButtonGroupStateChangedEvent
		}
	}
	
	public void Move2() {

		// select radioButton if only matching label was clicked
		RadioButton rb = screen.findNiftyControl("Mover", RadioButton.class);
		if (rb.isActivated() == false) {
			rb.select(); // triggers RadioButtonGroupStateChangedEvent
		} 
	}

	
	

	protected static void setId() {

	}

	public static void pauseWindow() {

		WindowBuilder b = new WindowBuilder("myWindow", "Attention") {
			{
				// you can't close this window
				width("320px"); // windows will need a size
				height("200px");
				backgroundColor("#323232");

				text(new TextBuilder() {
					{
						text("The Game has been Paused!");
						style("base-font");
						color("#FFFFFF ");
						valignCenter();
						width("100%");
					}
				});
			}
		};

		Element windows = screen.findElementById("windows");
		if (windows == null) {
			throw new IllegalStateException("Window parent control not found.");
		}

		b.build(nifty, screen, windows);

	}

	public static void resumeWindow() {

		WindowBuilder b = new WindowBuilder("myWindow", "Attention") {
			{
				backgroundColor("#323232");
				width("320px"); // windows will need a size
				height("200px");
				text(new TextBuilder() {
					{
						text("The Game has been Resumed!");
						style("base-font");
						color("#000");
						valignCenter();
						width("100%");
					}
				});
			}
		};

		Element windows = screen.findElementById("windows");
		if (windows == null) {
			throw new IllegalStateException("Window parent control not found.");
		}
		b.build(nifty, screen, windows);

	}

	public static void errorWindow() {

		WindowBuilder b = new WindowBuilder("myWindow", "GAME_ABORT") {
			{

				backgroundColor("#323232");
				width("320px"); // windows will need a size
				height("200px");
				text(new TextBuilder() {
					{
						text("The game leader stopped the game!");
						style("base-font");
						color("#000");
						valignCenter();
						width("100%");
					}
				});
			}
		};

		Element windows = screen.findElementById("windows");
		if (windows == null) {
			throw new IllegalStateException("Window parent control not found.");
		}
		b.build(nifty, screen, windows);

	}

	public static void disqualifyWindow() {

		WindowBuilder b = new WindowBuilder("myWindow", "Disqualified") {
			{
				backgroundColor("#FF0000");
				width("500px"); // windows will need a size
				height("400px");
				text(new TextBuilder() {
					{
						text("SORRY!!! YOU HAVE BEEN DISQUALIFIED :`( ");
						style("base-font");
						color("#FFFFFF");
						valignCenter();
						width("100%");
					}
				});
			}
		};

		Element windows = screen.findElementById("windows");
		if (windows == null) {
			throw new IllegalStateException("Window parent control not found.");
		}
		b.build(nifty, screen, windows);

	}

	public static void wonWindow() {

		WindowBuilder b = new WindowBuilder("myWindow", "WON") {
			{
				backgroundColor("#FF0000");
				width("500px"); // windows will need a size
				height("400px");
				text(new TextBuilder() {
					{
						text("CONGRATULATIONS!!! YOU HAVE WON THE GAME :D ");
						style("base-font");
						color("#FFFFFF");
						valignCenter();
						width("100%");
					}
				});
			}
		};

		Element windows = screen.findElementById("windows");
		if (windows == null) {
			throw new IllegalStateException("Window parent control not found.");
		}
		b.build(nifty, screen, windows);

	}

	public static void lostWindow() {

		WindowBuilder b = new WindowBuilder("myWindow", "LOST") {
			{
				backgroundColor("#FF0000");
				width("500px"); // windows will need a size
				height("400px");
				text(new TextBuilder() {
					{
						text("I'M SORRY YOU HAVE LOST THE GAME! :'( ");
						style("base-font");
						color("#FFFFFF");
						valignCenter();
						width("100%");
					}
				});
			}
		};

		Element windows = screen.findElementById("windows");
		if (windows == null) {
			throw new IllegalStateException("Window parent control not found.");
		}
		b.build(nifty, screen, windows);

	}

	// public void spawnWindow() {
	// Element windows = screen.findElementById("windows");
	// if (windows == null) {
	// throw new IllegalStateException("Window parent control not found.");
	// }
	// }

	@NiftyEventSubscriber(pattern = "window-.*")
	public void onAnyWindowClose(final String id, @Nonnull final WindowClosedEvent event) {
		setStatus("Window [" + id + "] " + (event.isHidden() ? "hidden" : "closed"));
	}

	@Nonnull
	private static String randomColor() {
		return "#" + Integer.toHexString(random.nextInt(200)) + Integer.toHexString(random.nextInt(200))
				+ Integer.toHexString(random.nextInt(200)) + "ff";
	}

	@NiftyEventSubscriber(pattern = ".*")
	public void showDragStartStatus(final String id, @Nonnull final DraggableDragStartedEvent event) {
		setStatus("Dragging [" + getId(event.getDraggable()) + "] from [" + getId(event.getSource()) + "].");

	}

	/**
	 * Called for all Draggables when the Drag operation stops.
	 *
	 * @param event
	 *            the DraggableDragCanceledEvent
	 */
	@NiftyEventSubscriber(pattern = ".*")
	public void showDragCancelStatus(final String id, @Nonnull final DraggableDragCanceledEvent event) {
		setStatus(
				"Canceled [" + getId(event.getDraggable()) + "] reverting back to [" + getId(event.getSource()) + "].");
	}

	/**
	 * Called for all Dropables when something is dropped on them.
	 *
	 * @param event
	 *            the DropableDroppedEvent
	 */
	@NiftyEventSubscriber(pattern = ".*")
	public void showDropStatus(final String id, @Nonnull final DroppableDroppedEvent event) {
		if ((event.getTarget() == trash) && (event.getSource() == evilStuff)) {
			setStatus("Evil [" + getId(event.getDraggable()) + "] has been eliminated.");
		} else if (event.getTarget() == evilStuff) {
			setStatus("[" + getId(event.getDraggable()) + "] has become evil");
		} else {
			setStatus("Dropped [" + getId(event.getDraggable()) + "] on [" + getId(event.getTarget()) + "].");

			String card = getId(event.getDraggable());
			String place = getId(event.getTarget());

			// System.out.println("Positionen: " + card);

			String cardAnweisung = fillCards(card);

			dropp.add(event);
			idE.add(id);

			if (containsString(getId(event.getTarget()), eTarg)) {
				setStatus("Position ist schon belegt");

			} else {
				int pos = 0;
				pos = uebersetzen(place);
				Cards c = new Cards(cardAnweisung, place);
				// System.out.println(c.cardType);
				eTarg.add(place);
				if (!myCardsBelegt()) {
					fillMyCards(c, pos);
					// myCards.add(c);
				} else {
					setStatus("Du darfst pro runde nicht mehr als 5 Karten legen");
				}

				// everytime a card is layed this method is called

				// as soon as the cards are layed it calls the getcards method
				// in the NetworkThread
				// NetworkThread.getCard(myCards);

			}

		}
	}

	public static boolean containsString(String testString, ArrayList<String> list) {
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).equals(testString)) {
				return true;
			}
		}
		return false;

	}

	/**
	 * Called when something is dropped on the Trash.
	 *
	 * @param event
	 *            the DropableDroppedEvent
	 */
	@NiftyEventSubscriber(id = "Trash")
	public void onTrashDrop(final String id, @Nonnull final DroppableDroppedEvent event) {
		event.getDraggable().getElement().markForRemoval();
	}

	@NiftyEventSubscriber(id = "Trash1")
	public void onTrashDrag(final String id, @Nonnull final DraggableDragCanceledEvent event) {
		event.getDraggable().getElement().markForRemoval();
	}

	@Nullable
	private String getId(@Nullable final Droppable droppable) {
		return (droppable != null) ? droppable.getElement().getId() : null;
	}

	@Nullable
	private String getId(@Nullable final Draggable draggable) {
		return (draggable != null) ? draggable.getElement().getId() : null;
	}

	private void setStatus(final String text) {
		Label status = screen.findNiftyControl("status", Label.class);
		if (status != null) {
			status.setText(text);
		}
	}

	@Nonnull
	private Droppable findDroppable(final String id) {
		final Droppable droppable = screen.findNiftyControl(id, Droppable.class);
		if (droppable == null) {
			throw new IllegalArgumentException("Requested id " + id + " does not match a droppable.");
		}
		return droppable;
	}

	@Override
	public void onStartScreen() {
		// nothing
	}

	@Override
	public void onEndScreen() {
		// nothing
	}

	public void reset() {

	}

	public void start() throws InterruptedException {

		// for(int i = 0 ; i<myCards.length; i++){
		// System.out.println("BWLL;DL " + myCards[i].cardType);
		// }

		if (NetworkThread.pause) {
			setStatus("PAUSE");
		} else {

			if (myCardsBelegt()) {
				// for(int i = 0; i<5; i++){
				// onTrashDrop(idE.get(i),dropp.get(i));
				// }

				removeCardsFromDisplay();
				if (eTarg.size() >= 5) {
					setDroppablesFree();
				}

				NetworkThread.getCard(myCards);

			} else {
				setStatus("Du musst 5 Karten setzen");
			}

		}
	}

	public void Exit() {
		System.exit(0);
	}

	public void btnConnectClick() {

	}

	public void btnSend() {

	}

	public static void safeAnweisungen(ArrayList<Cards> cardSet) {

		for (int i = 0; i < cardSet.size(); i++) {
			anweisung2[i] = cardSet.get(i).cardType;

		}
		

	}

	public static void removeCardsFromDisplay() {

		for (int i = 0; i < 9; i++) {
			Element draggables = screen.findElementById("CardPosition " + i);
			// System.out.println(" Delete " + draggables);
			// if (draggables != null) {
			draggables.markForRemoval();

			// System.out.println(draggables);
			// }

		}
	}

	public static void setDroppablesFree() {

		eTarg = new ArrayList<String>(5);
	}

	public static void setCardsFree() {

		myCards = new Cards[5];

	}

	public static int uebersetzen(String pos) {
		int position = 0;

		switch (pos) {
		case "Position 0":
			position = 1;
			break;
		case "Position 1":
			position = 2;
			break;
		case "Position 2":
			position = 3;
			break;
		case "Position 3":
			position = 4;
			break;
		case "Position 4":
			position = 5;
			break;
		default:
			System.out.println("Nicht vorhanden!!!");
		}
		return position;

	}

	public static String fillCards(String posImArray) {
		String anweisung = "";

		switch (posImArray) {
		case "CardPosition 0":
			anweisung = anweisung2[0];
			break;
		case "CardPosition 1":
			anweisung = anweisung2[1];
			break;
		case "CardPosition 2":
			anweisung = anweisung2[2];
			break;
		case "CardPosition 3":
			anweisung = anweisung2[3];
			break;

		case "CardPosition 4":
			anweisung = anweisung2[4];
			break;
		case "CardPosition 5":
			anweisung = anweisung2[5];
			break;
		case "CardPosition 6":
			anweisung = anweisung2[6];
			break;
		case "CardPosition 7":
			anweisung = anweisung2[7];
			break;

		case "CardPosition 8":
			anweisung = anweisung2[8];
			break;

		default:
			System.err.println("There is no such thing!!!");

		}

		// if (posImArray.equals("CardPosition 0")) {
		// anweisung = anweisung2[0];
		// }
		//
		// if (posImArray.equals("CardPosition 1")) {
		// anweisung = anweisung2[1];
		// }
		//
		// if (posImArray.equals("CardPosition 2")) {
		// anweisung = anweisung2[2];
		// }
		//
		// if (posImArray.equals("CardPositionn 3")) {
		// anweisung = anweisung2[3];
		// }
		//
		// if (posImArray.equals("CardPosition 4")) {
		// anweisung = anweisung2[4];
		// }
		//
		// if (posImArray.equals("CardPosition 5")) {
		// anweisung = anweisung2[5];
		// }
		//
		// if (posImArray.equals("CardPosition 6")) {
		// anweisung = anweisung2[6];
		// }
		//
		// if (posImArray.equals("CardPosition 7")) {
		// anweisung = anweisung2[7];
		// }
		//
		// if (posImArray.equals("CardPosition 8")) {
		// anweisung = anweisung2[8];
		// }

		return anweisung;
	}

	public static void fillMyCards(Cards c, int p) {

		switch (p) {
		case 1:
			myCards[0] = c;
			break;
		case 2:
			myCards[1] = c;
			break;
		case 3:
			myCards[2] = c;
			break;
		case 4:
			myCards[3] = c;
			break;
		case 5:
			myCards[4] = c;
			break;
		default:
			System.err.println("Nicht da!!!!");
		}

		// if(p == 1){
		// myCards[0] = c;
		// System.out.println(" K0 " + myCards[0] );
		// System.out.println(c.cardType);

		// }
		// if(p == 2){
		// myCards[1] = c;
		// System.out.println(" K1 " + myCards[1] );
		// System.out.println(c.cardType);

		// }

		// if(p == 3){
		// myCards[2] = c;
		// System.out.println(" K2 " + myCards[2] );
		// System.out.println(c.cardType);

		// }

		// if(p == 4){
		// myCards[3] = c;
		// System.out.println(" K3 " + myCards[3] );
		// System.out.println(c.cardType);

		// }

		// if(p == 5){
		// myCards[4] = c;
		// System.out.println(" K4 " + myCards[4] );
		// System.out.println(c.cardType);

		// }

	}

	public static boolean myCardsBelegt() {
		boolean vollBelegt = true;
		for (int i = 0; i < 5; i++) {
			if (myCards[i] == null) {
				vollBelegt = false;
				break;
			}
		}

		return vollBelegt;
	}

	public static String randomAnweisung() {

		int stelle = (int) (Math.random() * 8);

		String anw = anweisung[stelle];

		return anw;

	}

}