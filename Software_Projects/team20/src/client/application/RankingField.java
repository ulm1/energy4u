package client.application;

import java.util.ArrayList;

import javax.json.JsonArray;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.math.ColorRGBA;
import com.jme3.niftygui.NiftyJmeDisplay;
import com.jme3.scene.Node;
import com.jme3.ui.Picture;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.tools.Color;

public class RankingField extends AbstractAppState {
	
	private SimpleApplication app;
	private GameofStones mainApp;
	private Nifty nifty;
	public static int id;
	static JsonArray terr;
	static String terrain2;
	
	
	public Node rankingField = new Node("rankingField");
	
	private BitmapFont guiFont;

	// (1) definere Variable
	public BitmapText name, terrain, castle, tavern, income, house, units, allegiance, existingSwords, existingArchers, existingKnights;
	private ArrayList<Roboter> players = new ArrayList<Roboter>();
	
	
	private int rankingFieldWidth;
	
    /**
     * {@inheritDoc}
     */
	@Override
	public void initialize(AppStateManager stateManager, Application app) {
		super.initialize(stateManager, app);
	    this.app = (SimpleApplication) app;
		mainApp = GameofStones.getMyself();
		// initialize stuff that is independent of whether state is PAUSED or RUNNING
		
		NiftyJmeDisplay niftyDisplay = NiftyJmeDisplay.newNiftyJmeDisplay(
                 app.getAssetManager(),
                 app.getInputManager(),
                 app.getAudioRenderer(),
                 app.getGuiViewPort());
         nifty = niftyDisplay.getNifty();
         app.getGuiViewPort().addProcessor(niftyDisplay);

         
		
		
		
		guiFont = mainApp.getAssetManager().loadFont("Interface/Fonts/Default.fnt");
	}
	
	/**
     * {@inheritDoc}
     */
	@Override
    public void cleanup() {
		super.cleanup();
		// unregister all my listeners, detach all my nodes, etc....
    }
	
	/**
     * {@inheritDoc}
     */
	@Override
    public void setEnabled(boolean enabled) {
		// pause and unpause
		//super.setEnabled(enabled);
	
		if(enabled){

			// first time updating player data
			updatePlayers();
	
			
			rankPlayers();

			rankingFieldWidth = 750;
			float scaleRate = ((float)(GameofStones.SCREEN_WIDTH) / 2250);
			int bottomHeight = (int)(rankingFieldWidth*0.026);
			int topHeight = (int)(rankingFieldWidth*0.026); 
			int middleHeight = (int)(rankingFieldWidth*0.5); 
			// set background of top box
			Picture pTop = new Picture("rankFieldTop");
			pTop.setImage(app.getAssetManager(), "Ranking-top.png", true);
			pTop.setHeight(topHeight);
			pTop.setWidth(rankingFieldWidth);
			pTop.move(0, bottomHeight + (players.size()+1)*middleHeight, 0);
			rankingField.attachChild(pTop);
			
			// set background of bottom box
			Picture pBottom = new Picture("rankFieldBottom");
			pBottom.setImage(app.getAssetManager(), "Ranking-bottom.png", true);
			pBottom.setHeight(bottomHeight);
			pBottom.setWidth(rankingFieldWidth);
			rankingField.attachChild(pBottom);
			
			for (int i = 0; i <= players.size(); i++) {
				
				// set background of main part
				Node nMiddle = new Node("rankNode" + i);
				nMiddle.move(0, (players.size()-i)*middleHeight, 0);
				rankingField.attachChild(nMiddle);
				
				Picture pMiddle = new Picture("rankFieldMiddle" + i);
				pMiddle.setImage(app.getAssetManager(), "Ranking-middle.png", true);
				pMiddle.setHeight(middleHeight);
				pMiddle.setWidth(rankingFieldWidth);
				pMiddle.move(0, bottomHeight, 0);
				nMiddle.attachChild(pMiddle);
				
				// create text elements and attach them to nodes
				//(2) declare varabes
				name = new BitmapText(guiFont);
				terrain = new BitmapText(guiFont);
				castle = new BitmapText(guiFont);
				tavern = new BitmapText(guiFont);
				income = new BitmapText(guiFont);
				house = new BitmapText(guiFont);
				units = new BitmapText(guiFont);
				allegiance = new BitmapText(guiFont);
				existingSwords = new BitmapText(guiFont);
				existingArchers = new BitmapText(guiFont);
				existingKnights = new BitmapText(guiFont);
				
				
				name.setSize(2* guiFont.getCharSet().getRenderedSize());
				terrain.setSize(2* guiFont.getCharSet().getRenderedSize());
				castle.setSize(2* guiFont.getCharSet().getRenderedSize());
				tavern.setSize(2* guiFont.getCharSet().getRenderedSize());
				income.setSize(2* guiFont.getCharSet().getRenderedSize());
				house.setSize(2* guiFont.getCharSet().getRenderedSize());
				units.setSize(2* guiFont.getCharSet().getRenderedSize());
				allegiance.setSize(2* guiFont.getCharSet().getRenderedSize());
				existingSwords.setSize(2* guiFont.getCharSet().getRenderedSize());
				existingArchers.setSize(2* guiFont.getCharSet().getRenderedSize());
				existingKnights.setSize(2* guiFont.getCharSet().getRenderedSize());
				
				// write in black instead of white
				
				name.setColor(ColorRGBA.Black);
				terrain.setColor(ColorRGBA.Black);
				castle.setColor(ColorRGBA.Black);
				tavern.setColor(ColorRGBA.Black);
				income.setColor(ColorRGBA.Black);
				house.setColor(ColorRGBA.Black);
				units.setColor(ColorRGBA.Black);
				allegiance.setColor(ColorRGBA.Black);
				existingSwords.setColor(ColorRGBA.Black);
				existingArchers.setColor(ColorRGBA.Black);
				existingKnights.setColor(ColorRGBA.Black);

				
				
				if (i == 0) {
					// set column titles
		
					
					name.setText("id:");
					terrain.setText("TerrainType:");
					castle.setText("castle: ");
					tavern.setText("tavern: ");
					income.setText("income: ");
					house.setText("house: ");
					units.setText("units: ");
					
					
					int yMove = (int)(rankingFieldWidth*0.13);
					int firstX = (int)(rankingFieldWidth * 0.10);
					
					
					//name.move(firstX, yMove -100, 0);
					//terrain.move(firstX, yMove -200, 0);
					
				
					
				} else {
					// set data
					
		
					
				
					
		
						
					int firstX = (int)(rankingFieldWidth * 0.17);
					int secondX = (int)(rankingFieldWidth * 0.27 );
					int thirdX = (int)(rankingFieldWidth * 0.64);
					int fourthX = (int)(rankingFieldWidth * .80);
					
					
					name.move(secondX, 700, 0);
					terrain.move(secondX, 650, 0);
					castle.move(secondX,600,0);
					tavern.move(secondX,550,0);
					income.move(secondX,500,0);
					house.move(secondX,450,0);
					units.move(secondX,400,0);
				
				
				}
				
				nMiddle.attachChild(name);
				nMiddle.attachChild(terrain);
				nMiddle.attachChild(castle);
				nMiddle.attachChild(tavern);
				nMiddle.attachChild(income);
				nMiddle.attachChild(house);
				nMiddle.attachChild(units);
			
			}
	        
			rankingField.scale(scaleRate);
	        app.getGuiNode().attachChild(this.rankingField);
		} else {
	    	
			// take away everything not needed while this state is PAUSED
	    	app.getGuiNode().detachChild(this.rankingField);
		}
    }
	
	/**
     * {@inheritDoc}
     */
	@Override
	// Note that update is only called while the state is both attached and enabled.
    public void update(float tpf) {
    	// do the following while game is RUNNING
		updatePlayers();
		rankPlayers();
		updateRanking();
    }
	
	private void updatePlayers() {
		players = mainApp.gameField.roboterList;
	}
	
	private void rankPlayers() {
		
		// initialize
	
		}
		
		
	private void updateRanking() {
		
		
			
			name.setText("id: " + id);
			
					if(!(terr==null)){

					 terrain2 = terr.getJsonObject(id).getString("terrain");
					boolean cas2 = terr.getJsonObject(id).getBoolean("castle");
					boolean tav2= terr.getJsonObject(id).getBoolean("tavern");
					int in2= terr.getJsonObject(id).getInt("income");
					
					
					
					
					
			
					if(terr.getJsonObject(id).getString("house").equals("") == false)
					{
						String house2 = terr.getJsonObject(id).getString("house");
						house.setText("house: " +house2);
					}
					
					else {
						
						house.setText("house:");
					}
					
					
					if(terr.getJsonObject(id).getJsonArray("units").equals("") == false)
					{
						JsonArray units2= terr.getJsonObject(id).getJsonArray("units");
						units.setText("units: " + units2);
					}
					
					else {
						System.out.println("empty");
						
					}
					
					
		
				
		

//					if(terrain2.equals("desert")){
//						Voronoi.setDesert(id);
//						
//					}

			
					terrain.setText("terrain: " + terrain2 );
					castle.setText("castle: " + cas2 );
					tavern.setText("tavern: " + tav2);
					income.setText("income: " +in2);
				
					
					
					}	
				}
			//System.out.println(terrotories.getJsonObject(i).getInt("id"));
			

			
			
		
	

	public static void setId(int i) {
		id = i;
		
	}

	public static void setTerritories(JsonArray terrotories) {
	
		 terr = terrotories;
		
	}
}
