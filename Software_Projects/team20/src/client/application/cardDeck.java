package client.application;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.niftygui.NiftyJmeDisplay;
import com.jme3.scene.Node;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;

public class cardDeck extends AbstractAppState implements ScreenController {
    /**
     * Initialize the SimpleApplication as well as the instance of the RoboRally.
     * In addition, the Nifty is integrated and thus the ScreenController can be used.
     */
    private SimpleApplication app;
    private GameofStones mainApp;
    private Nifty nifty;
    NiftyJmeDisplay niftyDisplayMenu;
    Node cards = new Node("cards");
	private static boolean popupShows;

	 
	
	 @Override
	    public void initialize(AppStateManager stateManager, Application app) {
	        super.initialize(stateManager, app);
	        this.app = (SimpleApplication) app;
	        mainApp = GameofStones.getMyself();
	        
	        
	    }
	 
	  @Override
	    public void cleanup() {
	        super.cleanup();
	
	       
	    }
	 
	
	  @Override
	    public void setEnabled(boolean enabled) {
	        // pause and unpause
	        super.setEnabled(enabled);
	        
	        if(enabled){
	            niftyDisplayMenu = NiftyJmeDisplay.newNiftyJmeDisplay(
	                    app.getAssetManager(),
	                    app.getInputManager(),
	                    app.getAudioRenderer(),
	                    app.getGuiViewPort());
	            nifty = niftyDisplayMenu.getNifty();
	            
	            
	            nifty.fromXml("cardDeck/cardDeck.xml", "start", this);

	            app.getGuiViewPort().addProcessor(niftyDisplayMenu);
	            app.getInputManager().setCursorVisible(true);

	            app.getGuiNode().attachChild(this.cards);
	            
	        
	    			popupShows = true;
	    		
	    		} else {
	    			popupShows = false;

	    			// take away everything not needed while this state is PAUSED
	
	           
	            app.getGuiNode().detachChild(this.cards);
	          
	        }
	    }
	  
		public static void popupErrorShows(boolean bool) {
			popupShows = bool;
		}

	 
	@Override
	public void bind(Nifty arg0, Screen arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onEndScreen() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStartScreen() {
		// TODO Auto-generated method stub
		
	}
	
	private ActionListener actionListener = new ActionListener() {
		public void onAction(String name, boolean keyPressed, float tpf) {
			// get currently focused element as well as all buttons
			Element focEl = nifty.getCurrentScreen().getFocusHandler().getKeyboardFocusElement();
			Element button = nifty.getCurrentScreen().findElementById("btnConnect");

			if (keyPressed && name.equals("Connect")) {
				if (popupShows) {
					// if errorPopup is active, click okay button
					nifty.getCurrentScreen().getTopMostPopup().findElementById("button_ok")
							.onClickAndReleasePrimaryMouseButton();
					;
				} 
			
					
			}

	
		}
	};
}
	
	
	

	
	


