package client.application;

import java.awt.Dimension;

import javax.swing.JFrame;

import com.jme3.system.AppSettings;
import com.jme3.system.JmeCanvasContext;

public class Canvast extends com.jme3.app.SimpleApplication {

	@Override
	public void simpleInitApp() {
		flyCam.setDragToRotate(true);
		
	}
	
	public static void main(String[] args) {
	    java.awt.EventQueue.invokeLater(new Runnable() {
	      public void run() {
	         // ... see below ...
	    	  
	    	  AppSettings settings = new AppSettings(true);
	    	  settings.setWidth(640);
	    	  settings.setHeight(480);
	    	  
	    	  Canvast canvasApplication = new Canvast();
	    	  canvasApplication.setSettings(settings);
	    	  canvasApplication.createCanvas(); // create canvas!
	    	  JmeCanvasContext ctx = (JmeCanvasContext) canvasApplication.getContext();
	    	  ctx.setSystemListener(canvasApplication);
	    	  Dimension dim = new Dimension(640, 480);
	    	  ctx.getCanvas().setPreferredSize(dim);
	    	  JFrame window = new JFrame("Swing Application");
	    	  window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	  
	    	  window.pack();
	    	  window.setVisible(true);
	    	  canvasApplication.startCanvas();
	    	  
	      }
	    });
	  }

}
