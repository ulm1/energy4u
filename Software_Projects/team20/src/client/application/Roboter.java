package client.application;

import java.util.LinkedList;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.effect.ParticleEmitter;
import com.jme3.material.Material;
import com.jme3.material.RenderState.BlendMode;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.renderer.queue.RenderQueue.Bucket;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Box;

import client.util.AnimationFactory;
import client.util.Compass;
import client.util.Coordinate;
import client.util.EventType;
import client.util.RoboEvent;


public class Roboter extends AbstractAppState {

	/**
	 * The attributes of a Roboter.
	 * 
	 * @author ali
	 */
	final static float ANIMATION_SPEED_MOVE = 1;
	final static float ANIMATION_SPEED_ROTATE = 200;
	final static float ANIMATION_SPEED_FALL = 3;
	final static float POLAR = (float) (1 / Math.sqrt(2));

	public int xPos;
	public int yPos;
	public String username;
	public int lastCheckPoint, destructionCount, lives;
	public boolean myTurn = false;
	static int btempXs;
	static int btempYs;
	static int redX;
	static int redY;

	Geometry position;
	boolean isDestroyed;
	public Compass orientation;
	Compass queueOrientation;
	public int RoboterID;
	Node roboGeo;
	static Application app;
	static Node field;
	gameField gamefield;
	ColorRGBA primaryColor;
	ColorRGBA secondColor;

	Material roboMaterial, roboGroundMaterial, roboMiddleMaterial, roboTopMaterial;
	Box box;
	ParticleEmitter checkpointAnim, destroyAnim;
	LinkedList<RoboEvent> semaphore = new LinkedList<>();
	static String event;
	static boolean ev = true;

	/**
	 * Setter method for the last checkpoint the Roboter has reached.
	 * 
	 * @param lastCheckPoint
	 *            The last checkpoint of the Roboter
	 * @author fabian
	 */
	public void setLastCheckpoint(int lastCheckPoint) {
		this.lastCheckPoint = lastCheckPoint;
	}

	/**
	 * Setter method for the number of destruction of the Roboter.
	 * 
	 * @param destructionCount
	 *            The nubmer of destructions
	 * @author fabian
	 */
	public void setDestructionCount(int destructionCount) {
		this.destructionCount = 0;
	}

	/**
	 * Setter method for the x position of the Roboter.
	 * 
	 * @param xPos
	 *            The current x position
	 * @author fabian
	 */
	public void setXPos(int xPos) {
		this.xPos = xPos;
	}

	/**
	 * Setter method for the y position of the Roboter.
	 * 
	 * @param yPos
	 *            The current y position
	 * @author fabian
	 */
	public void setYPos(int yPos) {
		this.yPos = yPos;
	}

	/**
	 * Setter method for the number of lives remaining for the Roboter
	 * 
	 * @param lives
	 *            The number of lives
	 * @author fabian
	 */
	public void setLives(int lives) {
		this.lives = lives;
	}

	/**
	 * Constructor of Roboter
	 * 
	 * @param x
	 *            x position of the Roboter
	 * @param y
	 *            y position of the Roboter
	 * @param orient
	 *            Orientation of the Roboter
	 * @param RoboterID
	 *            The ID associated with the Roboter
	 * @param app
	 *            The abstract app-state associated with the game field
	 * @param gamefield
	 *            The game field on which the Roboter is shown
	 * @author ali
	 */
	public Roboter(int x, int y, Compass orient, int RoboterID, Application app, gameField gamefield) {
		this.gamefield = gamefield;

		this.yPos = y;
		this.xPos = x;
		this.orientation = orient;
		this.queueOrientation = orient;
		this.RoboterID = RoboterID;
		this.app = app;

	}

	/**
	 * Constructor of Roboter setting the x-, y-position and the ID
	 * 
	 * @param x
	 *            x position of the Roboter
	 * @param y
	 *            y position of the Roboter
	 * @param RoboterID
	 *            The ID associated with the Roboter
	 * @author fabian
	 */
	public Roboter(int x, int y, int RoboterID) {
		this.xPos = x;
		this.yPos = y;
		this.RoboterID = RoboterID;
	}

	/**
	 * Fix the variables not set by Roboter(x, y, ID)
	 * 
	 * @param orient
	 *            The current orientation of the Roboter
	 * @param app
	 *            The abstract app-state associated with the game field
	 * @param gamefield
	 *            The game field on which the Roboter is shown
	 * @author fabian
	 */
	public void setRemainingVariables(Compass orient, Application app, gameField gamefield, ColorRGBA primaryColor,
			ColorRGBA secondColor) {
		this.gamefield = gamefield;
		this.position = gamefield.getBox(this.xPos, this.yPos);
		this.gamefield = gamefield;
		this.orientation = orient;
		this.queueOrientation = orient;
		this.app = app;
		this.field = gamefield.field;
		this.primaryColor = primaryColor;
		this.secondColor = secondColor;
	}

	@Override
	public void initialize(AppStateManager stateManager, Application app) {
		super.initialize(stateManager, app);
		this.app = (SimpleApplication) app;

		box = new Box(0.5f, 0.5f, 0);
		
		roboTopMaterial = new Material(app.getAssetManager(), GameofStones.UNSHADED);
		roboTopMaterial.setTexture("ColorMap", app.getAssetManager().loadTexture("drag.png"));
		roboTopMaterial.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);

		
		Geometry roboTopGeo = new Geometry("RoboterTop" + RoboterID, box);
		
		roboTopGeo.setMaterial(roboTopMaterial);
		roboTopGeo.setQueueBucket(Bucket.Translucent);

		roboGeo = new Node("Roboter" + RoboterID);
		
		roboGeo.attachChild(roboTopGeo);

		roboGeo.setLocalRotation(getQuaternion(orientation));
		checkpointAnim = AnimationFactory.getShockwave(app);
		destroyAnim = AnimationFactory.getExplosion(app);

	}

	/**
	 * Make the Roboter visible.
	 */
	@Override
	public void setEnabled(boolean enabled) {
		// Pause and unpause

		super.setEnabled(enabled);
		if (enabled) {
			// init stuff that is in use while this state is RUNNING

			setPosition(gamefield.getBox(xPos, yPos));
			field.attachChild(roboGeo);

		} else {
			// take away everything not needed while this state is PAUSED

		}
	}

	/**
	 * Update the view of the Roboter.
	 */
	@Override
	public void update(float tpf) {
//
//		if (!(event == null) && (ev == true)) {
//
//			switch (event) {
//			case "fireBlueLaserD":
//				ParticleEmitter toast = AnimationFactory.getToast(app, AnimationFactory.BLUELASER_HIT_D);
//				toast.setLocalTranslation(7,7, -2);
//				field.attachChild(toast);
//				toast.setParticlesPerSec(0);
//				toast.setNumParticles(1);
//				toast.emitAllParticles();
//				ev = false;
//				break;
//
//			case "fireBlueLaser":
//				ParticleEmitter toast1 = AnimationFactory.getToast(app, AnimationFactory.BLUELASER_HIT_R);
//				toast1.setLocalTranslation(7,5,-2);
//				field.attachChild(toast1);
//				toast1.setParticlesPerSec(0);
//				toast1.setNumParticles(1);
//				toast1.emitAllParticles();
//				ev = false;
//				break;
//
//			case "fireBlueLaserDL":
//
//				ParticleEmitter toast2 = AnimationFactory.getToast(app, AnimationFactory.BLUELASER_HIT_DL);
//				toast2.setLocalTranslation(server.application.EchoThread.getBlueLaser().get(0).getCfieldX(),
//						server.application.EchoThread.getBlueLaser().get(0).getCfieldY() + 1, -2);
//				field.attachChild(toast2);
//				toast2.setParticlesPerSec(0);
//				toast2.setNumParticles(1);
//				toast2.emitAllParticles();
//				ev = false;
//				break;
//
//			case "fireBlueLaserDR":
//
//				ParticleEmitter toast3 = AnimationFactory.getToast(app, AnimationFactory.BLUELASER_HIT_DR);
//				toast3.setLocalTranslation(server.application.EchoThread.getBlueLaser().get(0).getCfieldX(),
//						server.application.EchoThread.getBlueLaser().get(0).getCfieldY() + 1, -2);
//				field.attachChild(toast3);
//				toast3.setParticlesPerSec(0);
//				toast3.setNumParticles(1);
//				toast3.emitAllParticles();
//				ev = false;
//				break;
//
//			case "fireRedLaserD":
//				ParticleEmitter toast4 = AnimationFactory.getToast(app, AnimationFactory.REDLASER_HIT_D);
//				toast4.setLocalTranslation(server.application.EchoThread.getRedLaser().get(0).getCfieldX(),
//						server.application.EchoThread.getRedLaser().get(0).getCfieldY() + 6, -2);
//				field.attachChild(toast4);
//				toast4.setParticlesPerSec(0);
//				toast4.setNumParticles(1);
//				toast4.emitAllParticles();
//				ev = false;
//				break;
//
//			case "fireRedLaser":
//				ParticleEmitter toast5 = AnimationFactory.getToast(app, AnimationFactory.REDLASER_HIT_R);
//				toast5.setLocalTranslation(1,6,-2);
//				field.attachChild(toast5);
//				toast5.setParticlesPerSec(0);
//				toast5.setNumParticles(1);
//				toast5.emitAllParticles();
//				ev = false;
//				break;
//
//			case "fireREDLASERDL":
//				ParticleEmitter toast6 = AnimationFactory.getToast(app, AnimationFactory.REDLASER_HIT_DL);
//				toast6.setLocalTranslation(server.application.EchoThread.getRedLaser().get(0).getCfieldX(),
//						server.application.EchoThread.getRedLaser().get(0).getCfieldY() + 1, -2);
//				field.attachChild(toast6);
//				toast6.setParticlesPerSec(0);
//				toast6.setNumParticles(1);
//				toast6.emitAllParticles();
//				ev = false;
//				break;
//
//			case "fireRedLaserDR":
//				ParticleEmitter toast7 = AnimationFactory.getToast(app, AnimationFactory.REDLASER_HIT_DR);
//				toast7.setLocalTranslation(server.application.EchoThread.getRedLaser().get(0).getCfieldX(),
//						server.application.EchoThread.getRedLaser().get(0).getCfieldY() + 1, -2);
//				field.attachChild(toast7);
//				toast7.setParticlesPerSec(0);
//				toast7.setNumParticles(1);
//				toast7.emitAllParticles();
//				ev = false;
//				break;
//
//			}
//		}

		Spatial myself = field.getChild("Roboter" + RoboterID);

		if (semaphore.peek() != null && myTurn) {

			/**
			 * The event to simulate is MOVE
			 */
			if (semaphore.peek().event == EventType.MOVE) {
				if (!semaphore.peek().flag) {
					float goalPos = semaphore.peek().goalPos;
					switch (orientation) {
					case NORTH:
						myself.move(0, ANIMATION_SPEED_MOVE * tpf, 0);
						if (myself.getLocalTranslation().getY() > goalPos) {
							myself.setLocalTranslation(myself.getLocalTranslation().getX(), goalPos,
									myself.getLocalTranslation().getZ());
							finishMove();
						}
						break;
					case SOUTH:
						myself.move(0, -ANIMATION_SPEED_MOVE * tpf, 0);
						if (myself.getLocalTranslation().getY() < goalPos) {
							myself.setLocalTranslation(myself.getLocalTranslation().getX(), goalPos,
									myself.getLocalTranslation().getZ());
							finishMove();
						}
						break;
					case EAST:
						myself.move(ANIMATION_SPEED_MOVE * tpf, 0, 0);
						if (myself.getLocalTranslation().getX() > goalPos) {
							myself.setLocalTranslation(goalPos, myself.getLocalTranslation().getY(),
									myself.getLocalTranslation().getZ());
							finishMove();
						}
						break;
					case WEST:
						myself.move(-ANIMATION_SPEED_MOVE * tpf, 0, 0);
						if (myself.getLocalTranslation().getX() < goalPos) {

							myself.setLocalTranslation(goalPos, myself.getLocalTranslation().getY(),
									myself.getLocalTranslation().getZ());
							finishMove();
						}
						break;
					}

				} else {
					float goalPos = semaphore.peek().goalPos;
					switch (semaphore.peek().orientation) {
					case NORTH:
						myself.move(0, ANIMATION_SPEED_MOVE * tpf, 0);
						if (myself.getLocalTranslation().getY() > goalPos) {
							myself.setLocalTranslation(myself.getLocalTranslation().getX(), goalPos,
									myself.getLocalTranslation().getZ());
							finishMove();
						}
						break;
					case SOUTH:
						myself.move(0, -ANIMATION_SPEED_MOVE * tpf, 0);
						if (myself.getLocalTranslation().getY() < goalPos) {
							myself.setLocalTranslation(myself.getLocalTranslation().getX(), goalPos,
									myself.getLocalTranslation().getZ());
							finishMove();
						}
						break;
					case EAST:
						myself.move(ANIMATION_SPEED_MOVE * tpf, 0, 0);
						if (myself.getLocalTranslation().getX() > goalPos) {
							myself.setLocalTranslation(goalPos, myself.getLocalTranslation().getY(),
									myself.getLocalTranslation().getZ());
							finishMove();
						}
						break;
					case WEST:
						myself.move(-ANIMATION_SPEED_MOVE * tpf, 0, 0);
						if (myself.getLocalTranslation().getX() < goalPos) {

							myself.setLocalTranslation(goalPos, myself.getLocalTranslation().getY(),
									myself.getLocalTranslation().getZ());
							finishMove();
						}
						break;
					}
				}

			}

			/**
			 * The event to simulate is MOVE_BACKWARD
			 */
			else if (semaphore.peek().event == EventType.MOVE_BACKWARD) {

				if (!semaphore.peek().flag) {
					float goalPos = semaphore.peek().goalPos;
					switch (orientation) {
					case NORTH:
						myself.move(0, -ANIMATION_SPEED_MOVE * tpf, 0);
						if (myself.getLocalTranslation().getY() < goalPos) {
							myself.setLocalTranslation(myself.getLocalTranslation().getX(), goalPos,
									myself.getLocalTranslation().getZ());
							finishMove();
						}
						break;
					case SOUTH:
						myself.move(0, ANIMATION_SPEED_MOVE * tpf, 0);
						if (myself.getLocalTranslation().getY() > goalPos) {
							myself.setLocalTranslation(myself.getLocalTranslation().getX(), goalPos,
									myself.getLocalTranslation().getZ());
							finishMove();
						}
						break;
					case EAST:
						myself.move(-ANIMATION_SPEED_MOVE * tpf, 0, 0);
						if (myself.getLocalTranslation().getX() < goalPos) {
							myself.setLocalTranslation(goalPos, myself.getLocalTranslation().getY(),
									myself.getLocalTranslation().getZ());
							finishMove();
						}
						break;
					case WEST:
						myself.move(ANIMATION_SPEED_MOVE * tpf, 0, 0);
						if (myself.getLocalTranslation().getX() > goalPos) {

							myself.setLocalTranslation(goalPos, myself.getLocalTranslation().getY(),
									myself.getLocalTranslation().getZ());
							finishMove();
						}
						break;
					}
				} else {
					float goalPos = semaphore.peek().goalPos;
					switch (semaphore.peek().orientation) {
					case NORTH:
						myself.move(0, -ANIMATION_SPEED_MOVE * tpf, 0);
						if (myself.getLocalTranslation().getY() < goalPos) {
							myself.setLocalTranslation(myself.getLocalTranslation().getX(), goalPos,
									myself.getLocalTranslation().getZ());
							finishMove();
						}
						break;
					case SOUTH:
						myself.move(0, ANIMATION_SPEED_MOVE * tpf, 0);
						if (myself.getLocalTranslation().getY() > goalPos) {
							myself.setLocalTranslation(myself.getLocalTranslation().getX(), goalPos,
									myself.getLocalTranslation().getZ());
							finishMove();
						}
						break;
					case EAST:
						myself.move(-ANIMATION_SPEED_MOVE * tpf, 0, 0);
						if (myself.getLocalTranslation().getX() < goalPos) {
							myself.setLocalTranslation(goalPos, myself.getLocalTranslation().getY(),
									myself.getLocalTranslation().getZ());
							finishMove();
						}
						break;
					case WEST:
						myself.move(ANIMATION_SPEED_MOVE * tpf, 0, 0);
						if (myself.getLocalTranslation().getX() > goalPos) {

							myself.setLocalTranslation(goalPos, myself.getLocalTranslation().getY(),
									myself.getLocalTranslation().getZ());
							finishMove();
						}
						break;
					}
				}

			}

			/**
			 * The event to simulate is ROTATE_RIGHT.
			 */
			else if (semaphore.peek().event == EventType.ROTATE_RIGHT) {

				myself.rotate(0, 0, -ANIMATION_SPEED_ROTATE * tpf * FastMath.DEG_TO_RAD);
				float rotationZ = myself.getLocalRotation().getZ();
				float rotationW = myself.getLocalRotation().getW();
				switch (orientation) {
				case NORTH:
					if (rotationZ < -POLAR) {
						orientation = getNextOrientation("right", orientation);
						myself.setLocalRotation(getQuaternion(orientation));
						finishMove();
					}
					break; // TODO: Set Rotation exactly!
				case SOUTH:
					if (rotationZ < POLAR) {
						orientation = getNextOrientation("right", orientation);
						myself.setLocalRotation(getQuaternion(orientation));
						finishMove();
					}
					break;
				case EAST:
					if (rotationW < 0) {
						orientation = getNextOrientation("right", orientation);
						myself.setLocalRotation(getQuaternion(orientation));
						finishMove();
					}
					break;
				case WEST:
					if (rotationZ < 0) {
						orientation = getNextOrientation("right", orientation);
						myself.setLocalRotation(getQuaternion(orientation));
						finishMove();
					}
					break;
				}

			}

			else if (semaphore.peek().event == EventType.ROTATE180) {

				myself.rotate(0, 0, -ANIMATION_SPEED_ROTATE * tpf * FastMath.DEG_TO_RAD);

				float rotationZ = myself.getLocalRotation().getZ();
				float rotationW = myself.getLocalRotation().getW();
				switch (orientation) {
				case NORTH:
					if (rotationZ < -POLAR) {
						orientation = getNextOrientation("right", orientation);
						orientation = getNextOrientation("right", orientation);
						myself.setLocalRotation(getQuaternion(orientation));
						finishMove();
					}
					break; // TODO: Set Rotation exactly!
				case SOUTH:
					if (rotationZ < POLAR) {
						orientation = getNextOrientation("right", orientation);
						orientation = getNextOrientation("right", orientation);

						myself.setLocalRotation(getQuaternion(orientation));
						finishMove();
					}
					break;
				case EAST:
					if (rotationW < 0) {
						orientation = getNextOrientation("right", orientation);
						orientation = getNextOrientation("right", orientation);

						myself.setLocalRotation(getQuaternion(orientation));
						finishMove();
					}
					break;
				case WEST:
					if (rotationZ < 0) {
						orientation = getNextOrientation("right", orientation);
						orientation = getNextOrientation("right", orientation);

						myself.setLocalRotation(getQuaternion(orientation));
						finishMove();
					}
					break;
				}

			}

			/**
			 * The event to simulate is ROTATE_LEFT.
			 */
			else if (semaphore.peek().event == EventType.ROTATE_LEFT) {

				myself.rotate(0, 0, ANIMATION_SPEED_ROTATE * tpf * FastMath.DEG_TO_RAD);
				float rotationZ = myself.getLocalRotation().getZ();
				float rotationW = myself.getLocalRotation().getW();
				switch (orientation) {
				case NORTH:
					if (rotationZ > POLAR) {
						orientation = getNextOrientation("left", orientation);
						myself.setLocalRotation(getQuaternion(orientation));
						finishMove();
					}
					break; // TODO: Set Rotation exactly!
				case SOUTH:
					if (rotationZ < POLAR) {
						orientation = getNextOrientation("left", orientation);
						myself.setLocalRotation(getQuaternion(orientation));
						finishMove();
					}
					break;
				case EAST:
					if (rotationZ > 0) {
						orientation = getNextOrientation("left", orientation);
						myself.setLocalRotation(getQuaternion(orientation));
						finishMove();
					}
					break;
				case WEST:
					if (rotationW < 0) {
						orientation = getNextOrientation("left", orientation);
						myself.setLocalRotation(getQuaternion(orientation));
						finishMove();
					}
					break;
				}

			}

			/**
			 * The event to simulate is FALL_FROM_MAP.
			 */
			else if (semaphore.peek().event == EventType.FALL_FROM_MAP) {

				float scaleRate = myself.getLocalScale().getX()
						- myself.getLocalScale().getX() * ANIMATION_SPEED_FALL * tpf;
				myself.setLocalScale(scaleRate);
				if (myself.getLocalScale().getX() < 0.01) {
					myself.setLocalScale(0);
					ParticleEmitter toast = AnimationFactory.getToast(app, AnimationFactory.LIVE_DEC3);
					System.out.println(this.lives);
					this.lives -= 3;
					toast.setLocalTranslation(myself.getLocalTranslation());
					field.attachChild(toast);
					toast.setParticlesPerSec(0);
					toast.setNumParticles(1);
					toast.emitAllParticles();
					finishMove();
				}
			}

			// increase Health

			else if (semaphore.peek().event == EventType.HEALTH) {
				this.lives += 1;
				ParticleEmitter toast = AnimationFactory.getToast(app, AnimationFactory.LIVE_INC);
				toast.setLocalTranslation(myself.getLocalTranslation());
				field.attachChild(toast);
				toast.setParticlesPerSec(0);
				toast.setNumParticles(1);
				toast.emitAllParticles();
				finishMove();
			}

			/**
			 * The event to simulate is Got Lives.
			 */

			/**
			 * The event to simulate is SPAWN.
			 */
			else if (semaphore.peek().event == EventType.SPAWN) {
				RoboEvent actualEvent = semaphore.peek();
				if (actualEvent.flag == false) {
					myself.setLocalTranslation(gamefield
							.getBox(actualEvent.position.x_Value, actualEvent.position.y_Value).getLocalTranslation());
					myself.setLocalRotation(getQuaternion(actualEvent.orientation));
					orientation = actualEvent.orientation;
					semaphore.peek().flag = true;
					myself.setLocalScale(0.2f);
				}
				float scaleRate = myself.getLocalScale().getX()
						+ myself.getLocalScale().getX() * ANIMATION_SPEED_FALL * tpf;

				myself.setLocalScale(scaleRate);
				if (myself.getLocalScale().getX() > 1) {
					myself.setLocalScale(1);
					finishMove();
				}
			}

			/**
			 * The event to simulate is COLLIDE_WITH_WALL. TODO
			 */
			else if (semaphore.peek().event == EventType.COLLIDE_WITH_WALL) {
				float startPos = semaphore.peek().goalPos;
				switch (orientation) {
				case NORTH:
					if (semaphore.peek().flag == false) {
						myself.move(0, -ANIMATION_SPEED_MOVE / 2 * tpf, 0);
						if (myself.getLocalTranslation().getY() < startPos - 0.2f) {
							myself.setLocalTranslation(myself.getLocalTranslation().getX(), startPos - 0.2f,
									myself.getLocalTranslation().getZ());
							semaphore.peek().flag = true;
						}
					} else {
						myself.move(0, ANIMATION_SPEED_MOVE * 1.5f * tpf, 0);
						if (myself.getLocalTranslation().getY() > startPos) {
							myself.setLocalTranslation(myself.getLocalTranslation().getX(), startPos,
									myself.getLocalTranslation().getZ());
							finishMove();
						}
					}
					break;
				case SOUTH:
					if (semaphore.peek().flag == false) {
						myself.move(0, ANIMATION_SPEED_MOVE / 2 * tpf, 0);
						if (myself.getLocalTranslation().getY() > startPos + 0.2f) {
							myself.setLocalTranslation(myself.getLocalTranslation().getX(), startPos + 0.2f,
									myself.getLocalTranslation().getZ());
							semaphore.peek().flag = true;
						}
					} else {
						myself.move(0, -ANIMATION_SPEED_MOVE * 1.5f * tpf, 0);
						if (myself.getLocalTranslation().getY() < startPos) {
							myself.setLocalTranslation(myself.getLocalTranslation().getX(), startPos,
									myself.getLocalTranslation().getZ());
							finishMove();
						}
					}
					break;
				case EAST:
					if (semaphore.peek().flag == false) {
						myself.move(-ANIMATION_SPEED_MOVE / 2 * tpf, 0, 0);
						if (myself.getLocalTranslation().getX() < startPos - 0.2f) {
							myself.setLocalTranslation(startPos - 0.2f, myself.getLocalTranslation().getY(),
									myself.getLocalTranslation().getZ());
							semaphore.peek().flag = true;
						}
					} else {
						myself.move(ANIMATION_SPEED_MOVE * 1.5f * tpf, 0, 0);
						if (myself.getLocalTranslation().getX() > startPos) {
							myself.setLocalTranslation(startPos, myself.getLocalTranslation().getY(),
									myself.getLocalTranslation().getZ());
							finishMove();
						}
					}
					break;
				case WEST:
					if (semaphore.peek().flag == false) {
						myself.move(ANIMATION_SPEED_MOVE / 2 * tpf, 0, 0);
						if (myself.getLocalTranslation().getX() > startPos + 0.2f) {
							myself.setLocalTranslation(startPos + 0.2f, myself.getLocalTranslation().getY(),
									myself.getLocalTranslation().getZ());
							semaphore.peek().flag = true;
						}
					} else {
						myself.move(-ANIMATION_SPEED_MOVE * 1.5f * tpf, 0, 0);
						if (myself.getLocalTranslation().getX() < startPos) {
							myself.setLocalTranslation(startPos, myself.getLocalTranslation().getY(),
									myself.getLocalTranslation().getZ());
							finishMove();
						}
					}

					break;
				}
			}

			/**
			 * The event to simulate is COLLIDE_WITH_WALL_BACKWARD.
			 */
			else if (semaphore.peek().event == EventType.COLLIDE_WITH_WALL_BACKWARD) {
				float startPos = semaphore.peek().goalPos;
				switch (orientation) {
				case NORTH:
					if (semaphore.peek().flag == false) {
						myself.move(0, ANIMATION_SPEED_MOVE / 2 * tpf, 0);
						if (myself.getLocalTranslation().getY() > startPos + 0.2f) {
							myself.setLocalTranslation(myself.getLocalTranslation().getX(), startPos + 0.2f,
									myself.getLocalTranslation().getZ());
							semaphore.peek().flag = true;
						}
					} else {
						myself.move(0, -ANIMATION_SPEED_MOVE * 1.5f * tpf, 0);
						if (myself.getLocalTranslation().getY() < startPos) {
							myself.setLocalTranslation(myself.getLocalTranslation().getX(), startPos,
									myself.getLocalTranslation().getZ());
							finishMove();
						}
					}
					break;
				case SOUTH:
					if (semaphore.peek().flag == false) {
						myself.move(0, -ANIMATION_SPEED_MOVE / 2 * tpf, 0);
						if (myself.getLocalTranslation().getY() < startPos - 0.2f) {
							myself.setLocalTranslation(myself.getLocalTranslation().getX(), startPos - 0.2f,
									myself.getLocalTranslation().getZ());
							semaphore.peek().flag = true;
						}
					} else {
						myself.move(0, ANIMATION_SPEED_MOVE * 1.5f * tpf, 0);
						if (myself.getLocalTranslation().getY() > startPos + 0.1f) {
							myself.setLocalTranslation(myself.getLocalTranslation().getX(), startPos + 0.1f,
									myself.getLocalTranslation().getZ());
							finishMove();
						}
					}
					break;
				case EAST:
					if (semaphore.peek().flag == false) {
						myself.move(ANIMATION_SPEED_MOVE / 2 * tpf, 0, 0);
						if (myself.getLocalTranslation().getX() > startPos + 0.2f) {
							myself.setLocalTranslation(startPos + 0.2f, myself.getLocalTranslation().getY(),
									myself.getLocalTranslation().getZ());
							semaphore.peek().flag = true;
						}
					} else {
						myself.move(-ANIMATION_SPEED_MOVE * 1.5f * tpf, 0, 0);
						if (myself.getLocalTranslation().getX() < startPos) {
							myself.setLocalTranslation(startPos, myself.getLocalTranslation().getY(),
									myself.getLocalTranslation().getZ());
							finishMove();
						}
					}
					break;
				case WEST:
					if (semaphore.peek().flag == false) {
						myself.move(-ANIMATION_SPEED_MOVE / 2 * tpf, 0, 0);
						if (myself.getLocalTranslation().getX() < startPos - 0.2f) {
							myself.setLocalTranslation(startPos - 0.2f, myself.getLocalTranslation().getY(),
									myself.getLocalTranslation().getZ());
							semaphore.peek().flag = true;
						}
					} else {
						myself.move(ANIMATION_SPEED_MOVE * 1.5f * tpf, 0, 0);
						if (myself.getLocalTranslation().getX() > startPos) {
							myself.setLocalTranslation(startPos, myself.getLocalTranslation().getY(),
									myself.getLocalTranslation().getZ());
							finishMove();
						}
					}

					break;
				}
			}

			/**
			 * The event to simulate is CHECKPOINT_REACHED.
			 */
			else if (semaphore.peek().event == EventType.CHECKPOINT_REACHED) {

				this.lastCheckPoint += 1;
				checkpointAnim.setLocalTranslation(myself.getLocalTranslation());
				field.attachChild(checkpointAnim);
				checkpointAnim.setParticlesPerSec(0);
				checkpointAnim.setNumParticles(1);
				checkpointAnim.emitAllParticles();

				ParticleEmitter toast = AnimationFactory.getToast(app, AnimationFactory.CP_REACHED);
				toast.setLocalTranslation(myself.getLocalTranslation());
				field.attachChild(toast);
				toast.setParticlesPerSec(0);
				toast.setNumParticles(1);
				toast.emitAllParticles();
				finishMove();
			}

			else if (semaphore.peek().event == EventType.REDLASER_HIT_DL) {

				checkpointAnim.setLocalTranslation(redX - 1, redY + 1, -2);
				field.attachChild(checkpointAnim);
				checkpointAnim.setParticlesPerSec(0);
				checkpointAnim.setNumParticles(1);
				checkpointAnim.emitAllParticles();

				ParticleEmitter toast = AnimationFactory.getToast(app, AnimationFactory.REDLASER_HIT_DL);
				this.lives -= 3;
				toast.setLocalTranslation(myself.getLocalTranslation());
				field.attachChild(toast);
				toast.setParticlesPerSec(0);
				toast.setNumParticles(1);
				toast.emitAllParticles();
				finishMove();
			} else if (semaphore.peek().event == EventType.REDLASER_HIT_DR) {

				checkpointAnim.setLocalTranslation(redX - 1, redY + 1, -2);
				field.attachChild(checkpointAnim);
				checkpointAnim.setParticlesPerSec(0);
				checkpointAnim.setNumParticles(1);
				checkpointAnim.emitAllParticles();

				ParticleEmitter toast = AnimationFactory.getToast(app, AnimationFactory.REDLASER_HIT_DR);
				toast.setLocalTranslation(myself.getLocalTranslation());
				this.lives -= 3;
				field.attachChild(toast);
				toast.setParticlesPerSec(0);
				toast.setNumParticles(1);
				toast.emitAllParticles();
				finishMove();
			} else if (semaphore.peek().event == EventType.REDLASER_HIT_R) {

				checkpointAnim.setLocalTranslation(redX + 1, redY + 1, -2);
				field.attachChild(checkpointAnim);
				checkpointAnim.setParticlesPerSec(0);
				checkpointAnim.setNumParticles(1);
				checkpointAnim.emitAllParticles();

				ParticleEmitter toast = AnimationFactory.getToast(app, AnimationFactory.REDLASER_HIT_R);
				toast.setLocalTranslation(myself.getLocalTranslation());
				field.attachChild(toast);
				toast.setParticlesPerSec(0);
				toast.setNumParticles(1);
				toast.emitAllParticles();
				finishMove();
			} else if (semaphore.peek().event == EventType.REDLASER_HIT_D) {

				checkpointAnim.setLocalTranslation(redX - 1, redY + 1, -2);
				field.attachChild(checkpointAnim);
				checkpointAnim.setParticlesPerSec(0);
				checkpointAnim.setNumParticles(1);
				checkpointAnim.emitAllParticles();

				ParticleEmitter toast = AnimationFactory.getToast(app, AnimationFactory.REDLASER_HIT_D);
				toast.setLocalTranslation(myself.getLocalTranslation());
				this.lives -= 3;
				field.attachChild(toast);
				toast.setParticlesPerSec(0);
				toast.setNumParticles(1);
				toast.emitAllParticles();
				finishMove();
			} else if (semaphore.peek().event == EventType.BLUELASER_HIT_R) {

				checkpointAnim.setLocalTranslation(btempXs - 1, btempYs + 1, -2);
				field.attachChild(checkpointAnim);
				checkpointAnim.setParticlesPerSec(0);
				checkpointAnim.setNumParticles(1);
				checkpointAnim.emitAllParticles();

				ParticleEmitter toast = AnimationFactory.getToast(app, AnimationFactory.BLUELASER_HIT_R);
				this.lives -= 3;
				toast.setLocalTranslation(myself.getLocalTranslation());
				field.attachChild(toast);
				toast.setParticlesPerSec(0);
				toast.setNumParticles(1);
				toast.emitAllParticles();
				finishMove();
			} else if (semaphore.peek().event == EventType.BLUELASER_HIT_DR) {

				checkpointAnim.setLocalTranslation(btempXs - 1, btempYs + 1, -2);
				field.attachChild(checkpointAnim);
				checkpointAnim.setParticlesPerSec(0);
				checkpointAnim.setNumParticles(1);
				checkpointAnim.emitAllParticles();

				ParticleEmitter toast = AnimationFactory.getToast(app, AnimationFactory.BLUELASER_HIT_DR);
				this.lives -= 3;
				toast.setLocalTranslation(myself.getLocalTranslation());
				field.attachChild(toast);
				toast.setParticlesPerSec(0);
				toast.setNumParticles(1);
				toast.emitAllParticles();
				finishMove();
			} else if (semaphore.peek().event == EventType.BLUELASER_HIT_DL) {

				checkpointAnim.setLocalTranslation(btempXs - 1, btempYs + 1, -2);
				field.attachChild(checkpointAnim);
				checkpointAnim.setParticlesPerSec(0);
				checkpointAnim.setNumParticles(1);
				checkpointAnim.emitAllParticles();

				ParticleEmitter toast = AnimationFactory.getToast(app, AnimationFactory.BLUELASER_HIT_DL);
				this.lives -= 3;
				toast.setLocalTranslation(myself.getLocalTranslation());
				field.attachChild(toast);
				toast.setParticlesPerSec(0);
				toast.setNumParticles(1);
				toast.emitAllParticles();
				finishMove();
			} else if (semaphore.peek().event == EventType.BLUELASER_HIT_D) {

				checkpointAnim.setLocalTranslation(btempXs - 1, btempYs + 1, -2);
				field.attachChild(checkpointAnim);
				checkpointAnim.setParticlesPerSec(0);
				checkpointAnim.setNumParticles(1);
				checkpointAnim.emitAllParticles();

				ParticleEmitter toast = AnimationFactory.getToast(app, AnimationFactory.BLUELASER_HIT_D);
				this.lives -= 3;
				toast.setLocalTranslation(myself.getLocalTranslation());
				field.attachChild(toast);
				toast.setParticlesPerSec(0);
				toast.setNumParticles(1);
				toast.emitAllParticles();
				finishMove();
			}

			/**
			 * The event to simulate is DESTROY.
			 */
			else if (semaphore.peek().event == EventType.DESTROY) {
				if (!semaphore.peek().flag) {
					float scaleRate = myself.getLocalScale().getX()
							- myself.getLocalScale().getX() * 6 * ANIMATION_SPEED_FALL * tpf;
					myself.setLocalScale(scaleRate);
					if (myself.getLocalScale().getX() < 0.2) {
						myself.setLocalScale(0);
						semaphore.peek().flag = true;
					}
				} else {
					float scaleRate = myself.getLocalScale().getX()
							- myself.getLocalScale().getX() * 6 * ANIMATION_SPEED_FALL * tpf;
					myself.setLocalScale(scaleRate);
					destroyAnim.setLocalTranslation(myself.getLocalTranslation());
					field.attachChild(destroyAnim);
					destroyAnim.setParticlesPerSec(0);
					destroyAnim.setNumParticles(6);
					destroyAnim.emitAllParticles();

					ParticleEmitter toast = AnimationFactory.getToast(app, AnimationFactory.LIVE_DEC);
					toast.setLocalTranslation(myself.getLocalTranslation());
					field.attachChild(toast);
					toast.setParticlesPerSec(0);
					toast.setNumParticles(1);
					toast.emitAllParticles();

					this.destructionCount += 1;

					finishMove();
				}

			}

			else if (semaphore.peek().event == EventType.WIN) {
				finishMove();
			}

			else if (semaphore.peek().event == EventType.DISQUALIFIED) {
				this.isDestroyed = true;
				this.lives -= 9;
				ParticleEmitter toast = AnimationFactory.getGameOver(app);
				toast.setLocalTranslation(myself.getLocalTranslation());
				field.attachChild(toast);
				toast.setParticlesPerSec(0);
				toast.setNumParticles(1);
				toast.emitAllParticles();

				finishMove();

			}
		}
	}

	public static void fireBlueLaser() {

		event = "fireBlueLaser";
		ev = true;

	}

	public static void fireREDLASERDL() {

		event = "fireREDLASERDL";
		ev = true;
	}

	public static void fireRedLaser() {

		event = "fireRedLaser";
		ev = true;
	}

	public static void fireBlueLaserDL() {

		event = "fireBlueLaserDL";
		ev = true;
	}

	public static void fireBlueLaserD() {

		event = "fireBlueLaserD";
		ev = true;
	}

	public static void fireRedLaserD() {

		event = "fireRedLaserD";
		ev = true;
	}

	public static void fireBlueLaserDR() {

		event = "fireBlueLaserDR";
		ev = true;

	}

	public static void fireRedLaserDR() {

		event = "fireRedLaserDR";
		ev = true;

	}

	/**
	 * Set the position of the Roboter in the gamefield box.
	 * 
	 * @param box
	 *            The current gamefield box
	 * @author ali
	 */
	public void setPosition(Geometry box) {
		this.position = box;
		roboGeo.setLocalTranslation(box.getLocalTranslation());
	}

	/**
	 * End the evaluation of a move and go to the next one.
	 * 
	 * @author ali
	 */
	public void finishMove() {
		semaphore.poll();
		myTurn = false;
		gamefield.nextMove();
	}

	/**
	 * Perform the event MOVE.
	 * 
	 * @author ali
	 */
	public void move() {
		switch (queueOrientation) {
		case NORTH:
			yPos = yPos - 1;
		//	position = gamefield.getBox(xPos, yPos);
			semaphore.add(new RoboEvent(EventType.MOVE, yPos));
			break;

		case SOUTH:
			yPos = yPos + 1;
			position = gamefield.getBox(xPos, yPos);
			semaphore.add(new RoboEvent(EventType.MOVE, (position.getLocalTranslation().getY())));
			break;
		case EAST:
			xPos = xPos + 1;
			position = gamefield.getBox(xPos, yPos);
			semaphore.add(new RoboEvent(EventType.MOVE, (position.getLocalTranslation().getX())));
			break;
		case WEST:
			xPos = xPos - 1;
			position = gamefield.getBox(xPos, yPos);
			semaphore.add(new RoboEvent(EventType.MOVE, (position.getLocalTranslation().getX())));
			break;
		}
	}

	// ronnie
	public void move2() {
		switch (queueOrientation) {
		case NORTH:
			yPos = yPos - 2;
			position = gamefield.getBox(xPos, yPos);
			semaphore.add(new RoboEvent(EventType.MOVE, (position.getLocalTranslation().getY())));
			break;

		case SOUTH:
			yPos = yPos + 2;
			position = gamefield.getBox(xPos, yPos);
			semaphore.add(new RoboEvent(EventType.MOVE, (position.getLocalTranslation().getY())));
			break;
		case EAST:
			xPos = xPos + 2;
			position = gamefield.getBox(xPos, yPos);
			semaphore.add(new RoboEvent(EventType.MOVE, (position.getLocalTranslation().getX())));
			break;
		case WEST:
			xPos = xPos - 2;
			position = gamefield.getBox(xPos, yPos);
			semaphore.add(new RoboEvent(EventType.MOVE, (position.getLocalTranslation().getX())));
			break;
		}
	}

	public void move3() {
		switch (queueOrientation) {
		case NORTH:
			yPos = yPos - 3;
			position = gamefield.getBox(xPos, yPos);
			semaphore.add(new RoboEvent(EventType.MOVE, (position.getLocalTranslation().getY())));
			break;

		case SOUTH:
			yPos = yPos + 3;
			position = gamefield.getBox(xPos, yPos);
			semaphore.add(new RoboEvent(EventType.MOVE, (position.getLocalTranslation().getY())));
			break;
		case EAST:
			xPos = xPos + 3;
			position = gamefield.getBox(xPos, yPos);
			semaphore.add(new RoboEvent(EventType.MOVE, (position.getLocalTranslation().getX())));
			break;
		case WEST:
			xPos = xPos - 3;
			position = gamefield.getBox(xPos, yPos);
			semaphore.add(new RoboEvent(EventType.MOVE, (position.getLocalTranslation().getX())));
			break;
		}
	}

	public void move(Compass affectedOrient) {
		switch (affectedOrient) {
		case NORTH:
			yPos = yPos - 1;
			position = gamefield.getBox(xPos, yPos);
			semaphore.add(new RoboEvent(EventType.MOVE, (position.getLocalTranslation().getY()), true, affectedOrient));
			break;

		case SOUTH:
			yPos = yPos + 1;
			position = gamefield.getBox(xPos, yPos);
			semaphore.add(new RoboEvent(EventType.MOVE, (position.getLocalTranslation().getY()), true, affectedOrient));
			break;
		case EAST:
			xPos = xPos + 1;
			position = gamefield.getBox(xPos, yPos);
			semaphore.add(new RoboEvent(EventType.MOVE, (position.getLocalTranslation().getX()), true, affectedOrient));
			break;
		case WEST:
			xPos = xPos - 1;
			position = gamefield.getBox(xPos, yPos);
			semaphore.add(new RoboEvent(EventType.MOVE, (position.getLocalTranslation().getX()), true, affectedOrient));
			break;
		}
	}

	/**
	 * Perform the event ROTATE_RIGHT.
	 * 
	 * @author ali
	 */
	public void rotateRight() {
		queueOrientation = getNextOrientation("right", queueOrientation);
		semaphore.add(new RoboEvent(EventType.ROTATE_RIGHT));
	}

	public void rotate180() {

		queueOrientation = getNextOrientation("right", queueOrientation);
		semaphore.add(new RoboEvent(EventType.ROTATE180));
	}

	/**
	 * Perform the event ROTATE_LEFT.
	 * 
	 * @author ali
	 */
	public void rotateLeft() {
		queueOrientation = getNextOrientation("left", queueOrientation);
		semaphore.add(new RoboEvent(EventType.ROTATE_LEFT));
	}

	/**
	 * Perform the event FALL_FROM_MAP.
	 * 
	 * @author ali
	 */
	public void fallFromMap() {
		semaphore.add(new RoboEvent(EventType.FALL_FROM_MAP));
	}

	public void Gethealth() {
		semaphore.add(new RoboEvent(EventType.HEALTH));
	}

	/**
	 * Perform the event SPAWN.
	 * 
	 * @param spawnPos
	 *            The position where the spawn takes place
	 * @param spawnOrient
	 *            The orientation of the Roboter after the spawn
	 * @author ali
	 */
	public void spawn(Coordinate spawnPos, Compass spawnOrient) {

		xPos = spawnPos.x_Value;
		yPos = spawnPos.y_Value;
		position = gamefield.getBox(xPos, yPos);

		queueOrientation = spawnOrient;
		semaphore.add(new RoboEvent(EventType.SPAWN, spawnPos, spawnOrient));
	}

	/**
	 * Perform the event MOVE_BACKWARD.
	 * 
	 * @author ali
	 */
	public void moveBackward() {
		switch (queueOrientation) {
		case NORTH:
			yPos = yPos + 1;
			position = gamefield.getBox(xPos, yPos);
			semaphore.add(new RoboEvent(EventType.MOVE_BACKWARD, (position.getLocalTranslation().getY())));
			break;

		case SOUTH:
			yPos = yPos - 1;
			position = gamefield.getBox(xPos, yPos);
			semaphore.add(new RoboEvent(EventType.MOVE_BACKWARD, (position.getLocalTranslation().getY())));
			break;
		case EAST:
			xPos = xPos - 1;
			position = gamefield.getBox(xPos, yPos);
			semaphore.add(new RoboEvent(EventType.MOVE_BACKWARD, (position.getLocalTranslation().getX())));
			break;
		case WEST:
			xPos = xPos + 1;
			position = gamefield.getBox(xPos, yPos);
			semaphore.add(new RoboEvent(EventType.MOVE_BACKWARD, (position.getLocalTranslation().getX())));
			break;
		}
	}

	public void moveBackward(Compass affectedOrient) {
		switch (affectedOrient) {
		case NORTH:
			yPos = yPos + 1;
			position = gamefield.getBox(xPos, yPos);
			semaphore.add(new RoboEvent(EventType.MOVE_BACKWARD, (position.getLocalTranslation().getY()), true,
					affectedOrient));
			break;

		case SOUTH:
			yPos = yPos - 1;
			position = gamefield.getBox(xPos, yPos);
			semaphore.add(new RoboEvent(EventType.MOVE_BACKWARD, (position.getLocalTranslation().getY()), true,
					affectedOrient));
			break;
		case EAST:
			xPos = xPos - 1;
			position = gamefield.getBox(xPos, yPos);
			semaphore.add(new RoboEvent(EventType.MOVE_BACKWARD, (position.getLocalTranslation().getX()), true,
					affectedOrient));
			break;
		case WEST:
			xPos = xPos + 1;
			position = gamefield.getBox(xPos, yPos);
			semaphore.add(new RoboEvent(EventType.MOVE_BACKWARD, (position.getLocalTranslation().getX()), true,
					affectedOrient));
			break;
		}
	}

	/**
	 * Perform the event COLLIDE_WITH_WALL.
	 * 
	 * @author ali
	 */
	public void collide() {
		switch (queueOrientation) {
		case NORTH:
			semaphore.add(new RoboEvent(EventType.COLLIDE_WITH_WALL, (position.getLocalTranslation().getY())));
			break;
		case SOUTH:
			semaphore.add(new RoboEvent(EventType.COLLIDE_WITH_WALL, (position.getLocalTranslation().getY())));
			break;
		case EAST:
			semaphore.add(new RoboEvent(EventType.COLLIDE_WITH_WALL, (position.getLocalTranslation().getX())));
			break;
		case WEST:
			semaphore.add(new RoboEvent(EventType.COLLIDE_WITH_WALL, (position.getLocalTranslation().getX())));
			break;
		}
	}

	public void colliderobob() {
		switch (queueOrientation) {
		case NORTH:
			semaphore.add(new RoboEvent(EventType.COLLIDE_WITH_Robo, (position.getLocalTranslation().getY())));
			break;
		case SOUTH:
			semaphore.add(new RoboEvent(EventType.COLLIDE_WITH_Robo, (position.getLocalTranslation().getY())));
			break;
		case EAST:
			semaphore.add(new RoboEvent(EventType.COLLIDE_WITH_Robo, (position.getLocalTranslation().getX())));
			break;
		case WEST:
			semaphore.add(new RoboEvent(EventType.COLLIDE_WITH_Robo, (position.getLocalTranslation().getX())));
			break;
		}
	}

	/**
	 * Perform the event COLLIDE_WITH_WALL_BACKWARD.
	 * 
	 * @author ali
	 */
	public void collideBackward() {
		switch (queueOrientation) {
		case NORTH:
			semaphore.add(new RoboEvent(EventType.COLLIDE_WITH_WALL_BACKWARD, (position.getLocalTranslation().getY())));
			break;
		case SOUTH:
			semaphore.add(new RoboEvent(EventType.COLLIDE_WITH_WALL_BACKWARD, (position.getLocalTranslation().getY())));
			break;
		case EAST:
			semaphore.add(new RoboEvent(EventType.COLLIDE_WITH_WALL_BACKWARD, (position.getLocalTranslation().getX())));
			break;
		case WEST:
			semaphore.add(new RoboEvent(EventType.COLLIDE_WITH_WALL_BACKWARD, (position.getLocalTranslation().getX())));
			break;
		}
	}

	/**
	 * Perform the event CHECKPOINT_REACHED.
	 * 
	 * @author ali
	 */
	public void reachCheckpoint() {
		semaphore.add(new RoboEvent(EventType.CHECKPOINT_REACHED));
	}

	/**
	 * Perform the event DESTROY.
	 * 
	 * @author ali
	 */

	public void REDLASER_HIT_DL() {
		semaphore.add(new RoboEvent(EventType.REDLASER_HIT_DL));
	}

	public void REDLASER_HIT_DR() {
		semaphore.add(new RoboEvent(EventType.REDLASER_HIT_DR));
	}

	public void REDLASER_HIT_R() {
		semaphore.add(new RoboEvent(EventType.REDLASER_HIT_R));
	}

	public void REDLASER_HIT_D() {
		semaphore.add(new RoboEvent(EventType.REDLASER_HIT_D));
	}

	public void BLUELASER_HIT_R() {
		semaphore.add(new RoboEvent(EventType.BLUELASER_HIT_R));
	}

	public void BLUELASER_HIT_DR() {
		semaphore.add(new RoboEvent(EventType.BLUELASER_HIT_DR));
	}

	public void BLUELASER_HIT_DL() {
		semaphore.add(new RoboEvent(EventType.BLUELASER_HIT_DL));
	}

	public void BLUELASER_HIT_D() {
		semaphore.add(new RoboEvent(EventType.BLUELASER_HIT_D));
	}

	public void destroy() {
		semaphore.add(new RoboEvent(EventType.DESTROY));
	}

	public void disqualify() {
		semaphore.add(new RoboEvent(EventType.DISQUALIFIED));
	}

	public void win() {
		semaphore.add(new RoboEvent(EventType.WIN));
	}

	/**
	 * Find the new orientation after a rotation.
	 * 
	 * @param side
	 *            A String which is either "right" or something else
	 * @param comp
	 *            The orientation of the Roboter
	 * @return The new direction
	 * @author ali
	 */
	private Compass getNextOrientation(String side, Compass comp) {
		boolean right = side.equals("right");
		switch (comp) {
		case NORTH:
			if (right) {
				return Compass.EAST;
			} else {
				return Compass.WEST;
			}
		case SOUTH:
			if (right) {
				return Compass.WEST;
			} else {
				return Compass.EAST;
			}
		case EAST:
			if (right) {
				return Compass.SOUTH;
			} else {
				return Compass.NORTH;
			}
		case WEST:
			if (right) {
				return Compass.NORTH;
			} else {
				return Compass.SOUTH;
			}
		default:
			System.out.println("ERROR at switchOrientation - Something that couldnt went wrong, went wrong");
		}
		return Compass.NORTH;
	}

	/**
	 * Translate a direction into a quaternion.
	 * 
	 * @param direction
	 *            The current direction of the Roboter
	 * @return The quaternion found from the direction
	 * @author ali
	 */
	private Quaternion getQuaternion(Compass direction) {
		Quaternion result = new Quaternion(0, 0, 0, 1);
		switch (direction) {
		case NORTH:
			return result = new Quaternion(0, 0, 0, 1);
		case SOUTH:
			return result = new Quaternion(0, 0, 1, 0);
		case EAST:
			return result = new Quaternion(0, 0, -POLAR, POLAR);
		case WEST:
			return result = new Quaternion(0, 0, POLAR, POLAR);
		default:
			return result;
		}
	}

	public static void bluelaserpos(int tempX, int tempY) {

		btempXs = tempX;
		btempYs = tempY;
	}

	public static void redlaserpos(int tempX, int tempY) {

		redX = tempX;
		redY = tempY;

	}

}
