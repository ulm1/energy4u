package client.application;

public class Portal {
	int tempX;
	int tempY;
	
	public Portal(int tempX, int tempY) {
		this.tempX = tempX;
		this.tempY =tempY;
	}

	public int getTempX() {
		return tempX;
	}

	public void setTempX(int tempX) {
		this.tempX = tempX;
	}

	public int getTempY() {
		return tempY;
	}

	public void setTempY(int tempY) {
		this.tempY = tempY;
	}

}
