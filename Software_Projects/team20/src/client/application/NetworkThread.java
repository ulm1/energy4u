package client.application;

import client.controller.DragController;
import client.controller.LobbyScreenController;
import client.controller.NetworkController;
import client.controller.PhaseController;
import client.model.Map;
import client.model.Participant;
import client.model.Player;
import client.util.Cards;
import client.util.Compass;
import client.util.Coordinate;
import client.util.Event;
import client.util.EventType;

//import server.ServerController;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;

import org.omg.CORBA.portable.OutputStream;

import com.jme3.network.Server;
import com.jogamp.common.util.InterruptSource.Thread;

import NetworkCommunication;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;

import org.java_websocket.WebSocket;
import org.java_websocket.WebSocketImpl;
import org.java_websocket.drafts.Draft;

import org.java_websocket.exceptions.InvalidHandshakeException;
import org.java_websocket.framing.CloseFrame;
import org.java_websocket.framing.Framedata;
import org.java_websocket.framing.Framedata.Opcode;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.handshake.HandshakeImpl1Client;
import org.java_websocket.handshake.Handshakedata;
import org.java_websocket.handshake.ServerHandshake;
import org.java_websocket.server.WebSocketServer;

import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.acl.Owner;
import java.util.ArrayList;
import java.util.IllegalFormatException;
import java.util.Iterator;
import org.java_websocket.WebSocket;

public class NetworkThread implements Runnable {

	public NetworkThread(String[] packedStartScreenData, GameofStones app) throws UnknownHostException {
		this.packedStartScreenData = packedStartScreenData;
		this.mainApp = app;
		// super();
		// TODO Auto-generated constructor stub
	}

	String[] packedStartScreenData;
	static GameofStones mainApp;
	static boolean laser = false;
	static ArrayList<Player> player_List = new ArrayList<Player>();
	static String[] orientations;
	public static boolean pause = false;

	// public NetworkThread(String[] packedStartScreenData, GameofStones app) {
	// this.packedStartScreenData = packedStartScreenData;
	// this.mainApp = app;
	//
	//
	// }

	// Von Ron hinzugefgt
	static String orientation;
	public static int ownUserID;

	@Override
	public void run() {
		Socket connection = null;

		boolean isAccepted = false;
		try {
			/**
			 * Try to connect to the server and specify login parameters.
			 */
			System.out.println("Client :: Connect to server");
			connection = new Socket("localhost", 8025);

			String sendRole;
			if (packedStartScreenData[1].equals("watcher")) {
				sendRole = "SPECTATOR";
			} else {
				sendRole = "USER";
			}

			/**
			 * Send login request and try to get a positive login response from the server.
			 */
			ownUserID = -1;
			while (!isAccepted) {
				
				
				
				NetworkController.sendData(connection.getOutputStream(),
						// packed has player information

						NetworkController.encodeLoginRequest(packedStartScreenData[0], true, true));
				
				
				try {

				    NetworkCommunication.ConnectToServer();

				} catch (IllegalFormatException e) {
					System.out.println("cant connect");
				}
				 

				JsonObject tempJO = NetworkController.receiveMessage(connection.getInputStream());

				if (tempJO != null) {
					String typeString = null;
					JsonObject type = null;
					try {

						typeString = tempJO.getJsonObject("header").getString("messageType");

					} catch (IllegalFormatException e) {
						System.out.println("Client :: Received unknown message");
					}

					/**
					 * Received the login response.
					 */
					if (typeString.equals("LOGIN_RESPONSE")) {

						String boolStr = null;
						try {
							boolStr = tempJO.getJsonObject("body").getString("status");
						} catch (Exception e) {
							System.out.println("Client :: Received login response without any status");
						}
						if (boolStr.equals("ACCEPTED")) {

							System.out.println("Client :: Got accepted from the server");
							isAccepted = true;
							ownUserID = tempJO.getJsonObject("body").getInt("uid");
							LobbyScreenController.myUsername = packedStartScreenData[0];

							break;
						}
					}

					/**
					 * Received the login response.
					 */
					if (type.equals(tempJO.getJsonObject("supplyPhase"))) {

						String boolStr = null;
						try {
							System.out.println("Client :: Received login response without any status");
						} catch (Exception e) {
							System.out.println("Client :: Received login response without any status");
						}

					}

					/**
					 * Received an error message.
					 */
					else if (typeString.equals("ERROR")) {
						NetworkController.handleWatcherErrors(tempJO);
					} else {
						System.out.println("Client :: Received unexpected Json message1");
					}
				}
			}

			/**
			 * Try to get the game configuration from the server.
			 */
			JsonObject tempJO;
			while (true) {
				tempJO = NetworkController.receiveMessage(connection.getInputStream());
				if (tempJO != null) {
					String typeString = null;
					try {
						typeString = tempJO.getJsonObject("head").getString("type");
					} catch (IllegalFormatException e) {
						System.out.println("Client :: Received unknown message");
					}

					/**
					 * Received the game configuration.
					 */
					if (typeString.equals("GAME_CFG")) {

						File json2InputFile = new File("JSON_Files/Versorgungsphase.json");

						InputStream iss = null;
						try {
							iss = new FileInputStream(json2InputFile);
						} catch (FileNotFoundException e1) {

							e1.printStackTrace();
						}
						JsonReader reader3 = Json.createReader(iss);
						JsonObject jsonOb1 = reader3.readObject();

						// PhaseController.Versogung(jsonOb1);

						System.out.println("Client :: Got the game config message");
						break;
					}

					/**
					 * Received an error message.
					 */
					else if (typeString.equals("ERROR")) {
						NetworkController.handleWatcherErrors(tempJO);
					} else {
						System.out.println("Client :: Received unexpected Json message1");
					}
				}
			}

			File json2InputFile = new File("JSON_Files/level.json");

			InputStream iss = null;
			try {
				iss = new FileInputStream(json2InputFile);
			} catch (FileNotFoundException e1) {

				e1.printStackTrace();
			}
			JsonReader reader3 = Json.createReader(iss);
			JsonObject jsonOb1 = reader3.readObject();
			for (int i = 0; i < 6; i++) {

				JsonArray terrotories = jsonOb1.getJsonObject("levelcfg").getJsonArray("territories");

				RankingField.setTerritories(terrotories);
				Voronoi.setTerritories(terrotories);
			}

			Map map = NetworkController.decodeGameConfigMessage(tempJO);

			mainApp.gameMap = map;

			GameofStones.switchAppState(GameofStones.startScreen, GameofStones.lobbyScreen);

			boolean runLobby = true;

			ArrayList<Participant> participantList = new ArrayList<Participant>();
			System.out.println(participantList);

			while (runLobby) {
				try {
					tempJO = NetworkController.receiveMessage(connection.getInputStream());

					if (tempJO != null) {
						String typeString = tempJO.getJsonObject("head").getString("type");

						/**
						 * Received a new lobby status.
						 */
						if (tempJO.getJsonObject("head").getString("type").equals("LOBBY_STATUS")) {
							System.out.println("Client :: Got a new lobby status");

							NetworkController.wait(1000);
							participantList = NetworkController.decodeLobbyStatus(tempJO);

							LobbyScreenController.updateOnLobbyStatus(participantList);

							// System.out.println(terrotories.getJsonObject(i).getInt("id"));

						}

						/**
						 * Received a new message.
						 */
						else if (typeString.equals("MESSAGE")) {
							System.out.println("Client :: Got a new message");

							String newMessage = tempJO.getJsonObject("body").getString("content");
							int userID = tempJO.getJsonObject("body").getInt("uid");

							/**
							 * Only display messages, that are not our own.
							 */
							if (userID != ownUserID) {
								int index = -1;
								for (int k = 0; k < participantList.size(); k++) {
									if (userID == participantList.get(k).userID) {
										index = k;
										break;
									}
								}
								LobbyScreenController.updateOnIncomingMessage(newMessage,
										participantList.get(index).partName);
							}
						}
						/**
						 * Received the game init.
						 */
						else if (typeString.equals("GAME_INIT")) {

							System.out.println("Client :: Got the game init message");
							System.out.println("");
							System.out.println("Client :: I THINK THE GAME STARTS NOW");

							runLobby = false;

						}

						/**
						 * Received an error message.
						 */
						else if (typeString.equals("ERROR")) {
							NetworkController.handleWatcherErrors(tempJO);
						} else {
							System.out.println("Client :: Received unexpected Json message1");
						}

					}
				} catch (Exception e) {
				}
			}
			ArrayList<Player> test = NetworkController.decodeGameInit(tempJO);
			mainApp.gameField = new gameField(mainApp.gameMap);
			mainApp.gameField.playerList = test;
			System.out.println("Erreicht die Zeile!! ++++++++++++++++++++++++++++++++++");
			GameofStones.switchAppState(GameofStones.lobbyScreen, mainApp.gameField);
			boolean runGame = true;
			while (runGame) {

				tempJO = NetworkController.receiveMessage(connection.getInputStream());

				if (tempJO != null) {

					String typeString = tempJO.getJsonObject("head").getString("type");

					/**
					 * Received a round result message.
					 */

					if (typeString.equals("CARD_SET")) {

						if (DragController.myCards.length == 5) {

							DragController.setCardsFree();

						}

						ArrayList<Cards> cardSet = new ArrayList<Cards>();
						cardSet = NetworkController.decodeCardSet(tempJO);

						DragController.safeAnweisungen(cardSet);

					}

					if (typeString.equals("ROUND_RESULT")) {

						System.out.println("Client :: Received a round results");
						ArrayList<Event> eventList = NetworkController.decodeRoundResult(tempJO);

						// Adding all the events to the animation queue of the
						// gameField
						// Spawn need to be handled in a special way
						for (Event event : eventList) {

							if (event.affectedUID.length == 0) {
								System.out.println(event.eventType);

								// Case if its a simple event
								if (event.affectedUID.length == 0) {
									System.out.println(event.eventType);

									if (event.eventType == EventType.SPAWN) {
										System.out.println("ready to spawn ...");

										mainApp.gameField.spawnRoboter(event.userID, event.position, event.orientation,
												-1);

										orientation = event.orientation.toString();
										System.out
												.println(event.eventType + " wurde hinzugefgt fr User " + event.userID);

										// DragController.wonWindow();

									} else {

										mainApp.gameField.animateRoboter(event.eventType, event.userID);
										laser = true;
										System.out
												.println(event.eventType + " wurde hinzugefgt fr User " + event.userID);
									}
								}
								// Case if there are other IDs effected by this
								// event

								else {
									if (event.eventType == EventType.SPAWN) {
										mainApp.gameField.spawnRoboter(event.userID, event.position, event.orientation,
												event.affectedUID[0]);
										System.out.println(event.eventType + " wurde hinzugefgt fr User" + event.userID
												+ " mit affected User");
									} else {

										mainApp.gameField.animateRoboter(event.eventType, event.userID,
												event.affectedUID);
										System.out.println(event.eventType + " wurde hinzugefgt fr User" + event.userID
												+ " mit affected User");

									}
								}
							}

						}

						// if(laser ==true) {
						// Thread.sleep(5000);
						// mainApp.gameField.animateRedLaser();
						//
						// Thread.sleep(2000);
						// mainApp.gameField.animateBlueLaser();
						// laser=false;
						//
						// Thread.yield();
						// }
					} else if (typeString.equals("PAUSE")) {
						System.out.println("Client :: Seems like the game is paused");
						DragController.pauseWindow();
						pause = true;

					} else if (typeString.equals("RESUME")) {
						System.out.println("Client :: The game continues");
						pause = false;
						DragController.resumeWindow();

					}

					/**
					 * Received an error message.
					 */
					else if (typeString.equals("ERROR")) {
						System.out.println("FEHLER");
						NetworkController.handleWatcherErrors(tempJO);
						DragController.errorWindow();
					} else {
						System.out.println("Client :: Received unexpected Json message at runGAME");
					}

				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// author Ronnie

	public static int myId() {
		return ownUserID;
	}

	public static void getCard(Cards[] myCards) {

		switch (orientation) {
		case "NORTH":
			orientation = "UP";
			break;
		case "SOUTH":
			orientation = "DOWN";
			break;
		case "EAST":
			orientation = "RIGHT";
			break;
		case "WEST":
			orientation = "LEFT";
			break;

		}

		// building up a connection to the server so I can send the cards
		Socket connection = null;
		try {
			connection = new Socket("localhost", 8025);
			NetworkController.sendData(connection.getOutputStream(),

					NetworkController.CardSelection(myCards, orientation, myId()));
		} catch (UnknownHostException e1) {

			e1.printStackTrace();
		} catch (IOException e1) {

			e1.printStackTrace();
		}
	}

}
