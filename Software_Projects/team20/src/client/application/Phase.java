package client.application;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.niftygui.NiftyJmeDisplay;
import com.jme3.scene.Node;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.builder.ControlBuilder;
import de.lessvoid.nifty.builder.ControlDefinitionBuilder;
import de.lessvoid.nifty.builder.EffectBuilder;
import de.lessvoid.nifty.builder.ImageBuilder;
import de.lessvoid.nifty.builder.LayerBuilder;
import de.lessvoid.nifty.builder.PanelBuilder;
import de.lessvoid.nifty.builder.ScreenBuilder;
import de.lessvoid.nifty.builder.TextBuilder;
import de.lessvoid.nifty.controls.button.builder.ButtonBuilder;
import de.lessvoid.nifty.controls.dragndrop.builder.DraggableBuilder;
import de.lessvoid.nifty.controls.dragndrop.builder.DroppableBuilder;
import de.lessvoid.nifty.controls.label.builder.LabelBuilder;
import de.lessvoid.nifty.examples.defaultcontrols.common.CommonBuilders;
import de.lessvoid.nifty.examples.defaultcontrols.common.DialogPanelControlDefinition;
import de.lessvoid.nifty.examples.defaultcontrols.dragndrop.DragAndDropDialogController;
import de.lessvoid.nifty.screen.DefaultScreenController;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;

public class Phase extends AbstractAppState implements ScreenController {
    /**
     * Initialize the SimpleApplication as well as the instance of the RoboRally.
     * In addition, the Nifty is integrated and thus the ScreenController can be used.
     */
    private SimpleApplication app;
    private GameofStones mainApp;
    private Nifty nifty;
    NiftyJmeDisplay niftyDisplayMenu;
    Node phase = new Node("phase");
    private static final CommonBuilders builders = new CommonBuilders();
	private static boolean popupShows;
	public static final String NAME = "dragAndDropDialog";

	 
	
	 @Override
	    public void initialize(AppStateManager stateManager, Application app) {
	        super.initialize(stateManager, app);
	        this.app = (SimpleApplication) app;
	        mainApp = GameofStones.getMyself();
	        
	        
	    }
	 
	  @Override
	    public void cleanup() {
	        super.cleanup();
	
	       
	    }
	 
	
	  @Override
	    public void setEnabled(boolean enabled) {
	        // pause and unpause
	        super.setEnabled(enabled);
	           
	        

	        if(enabled){
	            niftyDisplayMenu = NiftyJmeDisplay.newNiftyJmeDisplay(
	                    app.getAssetManager(),
	                    app.getInputManager(),
	                    app.getAudioRenderer(),
	                    app.getGuiViewPort());
	            nifty = niftyDisplayMenu.getNifty();
	            
	   

	            nifty.fromXml("cardDeck/phase.xml", "hud");

	    
	            nifty.loadStyleFile("nifty-default-styles.xml");
	            nifty.loadControlFile("nifty-default-controls.xml");
	            
	            
	     
	            app.getGuiViewPort().addProcessor(niftyDisplayMenu);
	            app.getInputManager().setCursorVisible(true);

	            app.getGuiNode().attachChild(this.phase);
	            
	        }
	  	}
	  
		public static void popupErrorShows(boolean bool) {
			popupShows = bool;
		}

	 
	@Override
	public void bind(Nifty arg0, Screen arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onEndScreen() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStartScreen() {
		// TODO Auto-generated method stub
		
	}
}
