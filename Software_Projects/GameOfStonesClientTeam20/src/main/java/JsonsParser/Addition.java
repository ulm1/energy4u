package JsonsParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * The Class Addition.
 */
public class Addition {

/** The label. */
@SerializedName("label")
@Expose
private String label;

/** The value. */
@SerializedName("value")
@Expose
private String value;

/**
 * Gets the label.
 *
 * @return the label
 */
public String getLabel() {
return label;
}

/**
 * Sets the label.
 *
 * @param label the new label
 */
public void setLabel(String label) {
this.label = label;
}

/**
 * Gets the value.
 *
 * @return the value
 */
public String getValue() {
return value;
}

/**
 * Sets the value.
 *
 * @param value the new value
 */
public void setValue(String value) {
this.value = value;
}

}