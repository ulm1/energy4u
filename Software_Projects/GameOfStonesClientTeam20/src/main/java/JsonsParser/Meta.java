package JsonsParser;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * The Class Meta.
 */
public class Meta {

/** The name. */
@SerializedName("name")
@Expose
private String name;

/** The creation date. */
@SerializedName("creationDate")
@Expose
private Integer creationDate;

/** The creator. */
@SerializedName("creator")
@Expose
private String creator;

/**
 * Gets the name.
 *
 * @return the name
 */
public String getName() {
return name;
}

/**
 * Sets the name.
 *
 * @param name the new name
 */
public void setName(String name) {
this.name = name;
}

/**
 * Gets the creation date.
 *
 * @return the creation date
 */
public Integer getCreationDate() {
return creationDate;
}

/**
 * Sets the creation date.
 *
 * @param creationDate the new creation date
 */
public void setCreationDate(Integer creationDate) {
this.creationDate = creationDate;
}

/**
 * Gets the creator.
 *
 * @return the creator
 */
public String getCreator() {
return creator;
}

/**
 * Sets the creator.
 *
 * @param creator the new creator
 */
public void setCreator(String creator) {
this.creator = creator;
}

}