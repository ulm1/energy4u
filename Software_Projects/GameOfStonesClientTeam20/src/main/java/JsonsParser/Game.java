package JsonsParser;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * The Class Game.
 */
public class Game {

/** The min players. */
@SerializedName("minPlayers")
@Expose
private Integer minPlayers;

/** The max players. */
@SerializedName("maxPlayers")
@Expose
private Integer maxPlayers;

/** The edges. */
@SerializedName("edges")
@Expose
private List<List<Boolean>> edges = null;

/** The min units for castle conquest. */
@SerializedName("minUnitsForCastleConquest")
@Expose
private Integer minUnitsForCastleConquest;

/** The max units for castle recruit. */
@SerializedName("maxUnitsForCastleRecruit")
@Expose
private Integer maxUnitsForCastleRecruit;

/** The max units for non castle recruit. */
@SerializedName("maxUnitsForNonCastleRecruit")
@Expose
private Integer maxUnitsForNonCastleRecruit;

/** The recruitment cost. */
@SerializedName("recruitmentCost")
@Expose
private RecruitmentCost recruitmentCost;

/** The upkeep cost. */
@SerializedName("upkeepCost")
@Expose
private UpkeepCost upkeepCost;

/** The starving chance. */
@SerializedName("starvingChance")
@Expose
private Double starvingChance;

/** The resource decay. */
@SerializedName("resourceDecay")
@Expose
private Double resourceDecay;

/**
 * Gets the min players.
 *
 * @return the min players
 */
public Integer getMinPlayers() {
return minPlayers;
}

/**
 * Sets the min players.
 *
 * @param minPlayers the new min players
 */
public void setMinPlayers(Integer minPlayers) {
this.minPlayers = minPlayers;
}

/**
 * Gets the max players.
 *
 * @return the max players
 */
public Integer getMaxPlayers() {
return maxPlayers;
}

/**
 * Sets the max players.
 *
 * @param maxPlayers the new max players
 */
public void setMaxPlayers(Integer maxPlayers) {
this.maxPlayers = maxPlayers;
}

/**
 * Gets the edges.
 *
 * @return the edges
 */
public List<List<Boolean>> getEdges() {
return edges;
}

/**
 * Sets the edges.
 *
 * @param edges the new edges
 */
public void setEdges(List<List<Boolean>> edges) {
this.edges = edges;
}

/**
 * Gets the min units for castle conquest.
 *
 * @return the min units for castle conquest
 */
public Integer getMinUnitsForCastleConquest() {
return minUnitsForCastleConquest;
}

/**
 * Sets the min units for castle conquest.
 *
 * @param minUnitsForCastleConquest the new min units for castle conquest
 */
public void setMinUnitsForCastleConquest(Integer minUnitsForCastleConquest) {
this.minUnitsForCastleConquest = minUnitsForCastleConquest;
}

/**
 * Gets the max units for castle recruit.
 *
 * @return the max units for castle recruit
 */
public Integer getMaxUnitsForCastleRecruit() {
return maxUnitsForCastleRecruit;
}

/**
 * Sets the max units for castle recruit.
 *
 * @param maxUnitsForCastleRecruit the new max units for castle recruit
 */
public void setMaxUnitsForCastleRecruit(Integer maxUnitsForCastleRecruit) {
this.maxUnitsForCastleRecruit = maxUnitsForCastleRecruit;
}

/**
 * Gets the max units for non castle recruit.
 *
 * @return the max units for non castle recruit
 */
public Integer getMaxUnitsForNonCastleRecruit() {
return maxUnitsForNonCastleRecruit;
}

/**
 * Sets the max units for non castle recruit.
 *
 * @param maxUnitsForNonCastleRecruit the new max units for non castle recruit
 */
public void setMaxUnitsForNonCastleRecruit(Integer maxUnitsForNonCastleRecruit) {
this.maxUnitsForNonCastleRecruit = maxUnitsForNonCastleRecruit;
}

/**
 * Gets the recruitment cost.
 *
 * @return the recruitment cost
 */
public RecruitmentCost getRecruitmentCost() {
return recruitmentCost;
}

/**
 * Sets the recruitment cost.
 *
 * @param recruitmentCost the new recruitment cost
 */
public void setRecruitmentCost(RecruitmentCost recruitmentCost) {
this.recruitmentCost = recruitmentCost;
}

/**
 * Gets the upkeep cost.
 *
 * @return the upkeep cost
 */
public UpkeepCost getUpkeepCost() {
return upkeepCost;
}

/**
 * Sets the upkeep cost.
 *
 * @param upkeepCost the new upkeep cost
 */
public void setUpkeepCost(UpkeepCost upkeepCost) {
this.upkeepCost = upkeepCost;
}

/**
 * Gets the starving chance.
 *
 * @return the starving chance
 */
public Double getStarvingChance() {
return starvingChance;
}

/**
 * Sets the starving chance.
 *
 * @param starvingChance the new starving chance
 */
public void setStarvingChance(Double starvingChance) {
this.starvingChance = starvingChance;
}

/**
 * Gets the resource decay.
 *
 * @return the resource decay
 */
public Double getResourceDecay() {
return resourceDecay;
}

/**
 * Sets the resource decay.
 *
 * @param resourceDecay the new resource decay
 */
public void setResourceDecay(Double resourceDecay) {
this.resourceDecay = resourceDecay;
}

}