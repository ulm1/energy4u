package JsonsParser;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * The Class SupplyResult.
 */
public class supplyResults {

/** The house. */
@SerializedName("house")
@Expose
private String house;

/** The income territories. */
@SerializedName("incomeTerritories")
@Expose
private List<IncomeTerritory> incomeTerritories = null;

/** The upkeep. */
@SerializedName("upkeep")
@Expose
private Long upkeep;

/** The stock. */
@SerializedName("stock")
@Expose
private Long stock;

/**
 * Gets the house.
 *
 * @return the house
 */
public String getHouse() {
return house;
}

/**
 * Sets the house.
 *
 * @param house the new house
 */
public void setHouse(String house) {
this.house = house;
}

/**
 * Gets the income territories.
 *
 * @return the income territories
 */
public List<IncomeTerritory> getIncomeTerritories() {
return incomeTerritories;
}

/**
 * Sets the income territories.
 *
 * @param incomeTerritories the new income territories
 */
public void setIncomeTerritories(List<IncomeTerritory> incomeTerritories) {
this.incomeTerritories = incomeTerritories;
}

/**
 * Gets the upkeep.
 *
 * @return the upkeep
 */
public Long getUpkeep() {
return upkeep;
}

/**
 * Sets the upkeep.
 *
 * @param upkeep the new upkeep
 */
public void setUpkeep(Long upkeep) {
this.upkeep = upkeep;
}

/**
 * Gets the stock.
 *
 * @return the stock
 */
public Long getStock() {
return stock;
}

/**
 * Sets the stock.
 *
 * @param stock the new stock
 */
public void setStock(Long stock) {
this.stock = stock;
}

}