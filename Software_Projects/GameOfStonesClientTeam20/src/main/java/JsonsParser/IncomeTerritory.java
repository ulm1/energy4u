package JsonsParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * The Class IncomeTerritory.
 */
public class IncomeTerritory {

/** The territory id. */
@SerializedName("territoryId")
@Expose
private Long territoryId;

/** The income. */
@SerializedName("income")
@Expose
private Long income;

/**
 * Gets the territory id.
 *
 * @return the territory id
 */
public Long getTerritoryId() {
return territoryId;
}

/**
 * Sets the territory id.
 *
 * @param territoryId the new territory id
 */
public void setTerritoryId(Long territoryId) {
this.territoryId = territoryId;
}

/**
 * Gets the income.
 *
 * @return the income
 */
public Long getIncome() {
return income;
}

/**
 * Sets the income.
 *
 * @param income the new income
 */
public void setIncome(Long income) {
this.income = income;
}

}