package JsonsParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * The Class Matchcfg.
 */
public class Matchcfg {

/** The number of rounds before winter. */
@SerializedName("numberOfRoundsBeforeWinter")
@Expose
private Long numberOfRoundsBeforeWinter;

/** The time per phase. */
@SerializedName("timePerPhase")
@Expose
private Long timePerPhase;

/** The winter loss chance. */
@SerializedName("winterLossChance")
@Expose
private Double winterLossChance;

/** The max seconds before winter. */
@SerializedName("maxSecondsBeforeWinter")
@Expose
private Long maxSecondsBeforeWinter;

/**
 * Gets the number of rounds before winter.
 *
 * @return the number of rounds before winter
 */
public Long getNumberOfRoundsBeforeWinter() {
return numberOfRoundsBeforeWinter;
}

/**
 * Sets the number of rounds before winter.
 *
 * @param numberOfRoundsBeforeWinter the new number of rounds before winter
 */
public void setNumberOfRoundsBeforeWinter(Long numberOfRoundsBeforeWinter) {
this.numberOfRoundsBeforeWinter = numberOfRoundsBeforeWinter;
}

/**
 * Gets the time per phase.
 *
 * @return the time per phase
 */
public Long getTimePerPhase() {
return timePerPhase;
}

/**
 * Sets the time per phase.
 *
 * @param timePerPhase the new time per phase
 */
public void setTimePerPhase(Long timePerPhase) {
this.timePerPhase = timePerPhase;
}

/**
 * Gets the winter loss chance.
 *
 * @return the winter loss chance
 */
public Double getWinterLossChance() {
return winterLossChance;
}

/**
 * Sets the winter loss chance.
 *
 * @param winterLossChance the new winter loss chance
 */
public void setWinterLossChance(Double winterLossChance) {
this.winterLossChance = winterLossChance;
}

/**
 * Gets the max seconds before winter.
 *
 * @return the max seconds before winter
 */
public Long getMaxSecondsBeforeWinter() {
return maxSecondsBeforeWinter;
}

/**
 * Sets the max seconds before winter.
 *
 * @param maxSecondsBeforeWinter the new max seconds before winter
 */
public void setMaxSecondsBeforeWinter(Long maxSecondsBeforeWinter) {
this.maxSecondsBeforeWinter = maxSecondsBeforeWinter;
}

}