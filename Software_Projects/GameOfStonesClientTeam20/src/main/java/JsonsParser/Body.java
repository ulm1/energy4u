package JsonsParser;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * The Class Body.
 */
public class Body {

/** The is player. */
@SerializedName("isPlayer")
@Expose
private Boolean isPlayer;

/** The is human. */
@SerializedName("isHuman")
@Expose
private Boolean isHuman;

/** The from house. */
@SerializedName("fromHouse")
@Expose
private String fromHouse;

/** The to house. */
@SerializedName("toHouse")
@Expose
private String toHouse;

/** The is ready. */
@SerializedName("isReady")
@Expose
private Boolean isReady;

/** The message. */
@SerializedName("message")
@Expose
private String message;

/** The accept pause. */
@SerializedName("acceptPause")
@Expose
private Boolean acceptPause;


private boolean accepted;
private String reason;


private Levelcfg levelcfg;
private Matchcfg game;

private supplyResults supplyResults[]; 


private players players[];

public Levelcfg getLevelcfg() {
	return levelcfg;
}

public void setLevelcfg(Levelcfg levelcfg) {
	this.levelcfg = levelcfg;
}

public Matchcfg getGame() {
	return game;
}

public void setGame(Matchcfg game) {
	this.game = game;
}


@SerializedName("recruitments")
@Expose
private List<Recruitment> recruitments = null;


/** The movements. */
@SerializedName("movements")
@Expose
private List<Movement> movements = null;

/** The battles. */
@SerializedName("battles")
@Expose
private List<Battle> battles = null;

/**
 * Gets the checks if is player.
 *
 * @return the checks if is player
 */
public Boolean getIsPlayer() {
return isPlayer;
}

/**
 * Sets the checks if is player.
 *
 * @param isPlayer the new checks if is player
 */
public void setIsPlayer(Boolean isPlayer) {
this.isPlayer = isPlayer;
}

/**
 * Gets the checks if is human.
 *
 * @return the checks if is human
 */
public Boolean getIsHuman() {
return isHuman;
}

/**
 * Sets the checks if is human.
 *
 * @param isHuman the new checks if is human
 */
public void setIsHuman(Boolean isHuman) {
this.isHuman = isHuman;
}

/**
 * Gets the from house.
 *
 * @return the from house
 */
public String getFromHouse() {
return fromHouse;
}

/**
 * Sets the from house.
 *
 * @param fromHouse the new from house
 */
public void setFromHouse(String fromHouse) {
this.fromHouse = fromHouse;
}

/**
 * Gets the to house.
 *
 * @return the to house
 */
public String getToHouse() {
return toHouse;
}

/**
 * Sets the to house.
 *
 * @param toHouse the new to house
 */
public void setToHouse(String toHouse) {
this.toHouse = toHouse;
}

/**
 * Gets the checks if is ready.
 *
 * @return the checks if is ready
 */
public Boolean getIsReady() {
return isReady;
}

/**
 * Sets the checks if is ready.
 *
 * @param isReady the new checks if is ready
 */
public void setIsReady(Boolean isReady) {
this.isReady = isReady;
}

/**
 * Gets the message.
 *
 * @return the message
 */
public String getMessage() {
return message;
}

/**
 * Sets the message.
 *
 * @param message the new message
 */
public void setMessage(String message) {
this.message = message;
}

/**
 * Gets the accept pause.
 *
 * @return the accept pause
 */
public Boolean getAcceptPause() {
return acceptPause;
}

/**
 * Sets the accept pause.
 *
 * @param acceptPause the new accept pause
 */
public void setAcceptPause(Boolean acceptPause) {
this.acceptPause = acceptPause;
}

/**
 * Gets the recruitments.
 *
 * @return the recruitments
 */
public List<Recruitment> getRecruitments() {
return recruitments;
}

/**
 * Sets the recruitments.
 *
 * @param recruitments the new recruitments
 */
public void setRecruitments(List<Recruitment> recruitments) {
this.recruitments = recruitments;
}

/**
 * Gets the movements.
 *
 * @return the movements
 */
public List<Movement> getMovements() {
return movements;
}

/**
 * Sets the movements.
 *
 * @param movements the new movements
 */
public void setMovements(List<Movement> movements) {
this.movements = movements;
}

/**
 * Gets the battles.
 *
 * @return the battles
 */
public List<Battle> getBattles() {
return battles;
}

/**
 * Sets the battles.
 *
 * @param battles the new battles
 */
public void setBattles(List<Battle> battles) {
this.battles = battles;
}

public supplyResults[] getSupplyResults() {
	return supplyResults;
}

public void setSupplyResults(supplyResults[] supplyResults) {
	this.supplyResults = supplyResults;
}

public players[] getPlayers() {
	return players;
}

public void setPlayers(players players[]) {
	this.players = players;
}

public boolean isAccepted() {
	return accepted;
}

public void setAccepted(boolean accepted) {
	this.accepted = accepted;
}

public String getReason() {
	return reason;
}

public void setReason(String reason) {
	this.reason = reason;
}




}