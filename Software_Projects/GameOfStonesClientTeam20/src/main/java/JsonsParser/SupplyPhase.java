package JsonsParser;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * The Class SupplyPhase.
 */
public class SupplyPhase {

/** The supply results. */
@SerializedName("supplyResults")
@Expose
private List<supplyResults> supplyResults = null;

/**
 * Gets the supply results.
 *
 * @return the supply results
 */
public List<supplyResults> getSupplyResults() {
return supplyResults;
}

/**
 * Sets the supply results.
 *
 * @param supplyResults the new supply results
 */
public void setSupplyResults(List<supplyResults> supplyResults) {
this.supplyResults = supplyResults;
}

}