package JsonsParser;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * The Class Territory.
 */
public class Territory {

/** The id. */
@SerializedName("id")
@Expose
private Integer id;

/** The x. */
@SerializedName("x")
@Expose
private Integer x;

/** The y. */
@SerializedName("y")
@Expose
private Integer y;

/** The terrain. */
@SerializedName("terrain")
@Expose
private String terrain;

/** The castle. */
@SerializedName("castle")
@Expose
private Boolean castle;

/** The income. */
@SerializedName("income")
@Expose
private Integer income;

/** The label. */
@SerializedName("label")
@Expose
private String label[];

/** The house. */
@SerializedName("house")
@Expose
private String house;

/** The units. */
@SerializedName("units")
@Expose
private List<Unit> units = null;

private boolean tavern;
/**
 * Gets the id.
 *
 * @return the id
 */
public Integer getId() {
return id;
}

/**
 * Sets the id.
 *
 * @param id the new id
 */
public void setId(Integer id) {
this.id = id;
}

/**
 * Gets the x.
 *
 * @return the x
 */
public Integer getX() {
return x;
}

/**
 * Sets the x.
 *
 * @param x the new x
 */
public void setX(Integer x) {
this.x = x;
}

/**
 * Gets the y.
 *
 * @return the y
 */
public Integer getY() {
return y;
}

/**
 * Sets the y.
 *
 * @param y the new y
 */
public void setY(Integer y) {
this.y = y;
}

/**
 * Gets the terrain.
 *
 * @return the terrain
 */
public String getTerrain() {
return terrain;
}

/**
 * Sets the terrain.
 *
 * @param terrain the new terrain
 */
public void setTerrain(String terrain) {
this.terrain = terrain;
}

/**
 * Gets the castle.
 *
 * @return the castle
 */
public Boolean getCastle() {
return castle;
}

/**
 * Sets the castle.
 *
 * @param castle the new castle
 */
public void setCastle(Boolean castle) {
this.castle = castle;
}

/**
 * Gets the income.
 *
 * @return the income
 */
public Integer getIncome() {
return income;
}

/**
 * Sets the income.
 *
 * @param income the new income
 */
public void setIncome(Integer income) {
this.income = income;
}

/**
 * Gets the label.
 *
 * @return the label
 */


/**
 * Gets the house.
 *
 * @return the house
 */
public String getHouse() {
return house;
}

public String[] getLabel() {
	return label;
}

public void setLabel(String[] label) {
	this.label = label;
}

/**
 * Sets the house.
 *
 * @param house the new house
 */
public void setHouse(String house) {
this.house = house;
}

/**
 * Gets the units.
 *
 * @return the units
 */
public List<Unit> getUnits() {
return units;
}

/**
 * Sets the units.
 *
 * @param units the new units
 */
public void setUnits(List<Unit> units) {
this.units = units;
}

/**
 * Gets the unit of house.
 *
 * @param house the house
 * @return the unit of house
 */
public Unit getUnitOfHouse(String house) {
	for(Unit u : units) {
		if(house.equals(u.getAllegiance())) {
			return u;
		}
	}
	
	return null;
}

public boolean isTavern() {
	return tavern;
}

public void setTavern(boolean tavern) {
	this.tavern = tavern;
}

}