package JsonsParser;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * The Class Table.
 */
public class Table {

/** The terrain. */
@SerializedName("terrain")
@Expose
private String terrain;

/** The percentages. */
@SerializedName("percentages")
@Expose
private List<List<Double>> percentages = null;

/**
 * Gets the terrain.
 *
 * @return the terrain
 */
public String getTerrain() {
return terrain;
}

/**
 * Sets the terrain.
 *
 * @param terrain the new terrain
 */
public void setTerrain(String terrain) {
this.terrain = terrain;
}

/**
 * Gets the percentages.
 *
 * @return the percentages
 */
public List<List<Double>> getPercentages() {
return percentages;
}

/**
 * Sets the percentages.
 *
 * @param percentages the new percentages
 */
public void setPercentages(List<List<Double>> percentages) {
this.percentages = percentages;
}

}