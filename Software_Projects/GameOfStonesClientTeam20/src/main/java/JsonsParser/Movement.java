package JsonsParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * The Class Movement.
 */
public class Movement {

/** The amount of swordsmen. */
@SerializedName("amountSword")
@Expose
private Integer amountSword;

/** The amount of archers. */
@SerializedName("amountBow")
@Expose
private Integer amountBow;

/** The amount of knights. */
@SerializedName("amountKnight")
@Expose
private Integer amountKnight;

/** The territory id of the territory from which units are to be moved. */
@SerializedName("territoryIdFrom")
@Expose
private Integer territoryIdFrom;

/** The territory id of the territory to which the units are to be moved. */
@SerializedName("territoryIdTo")
@Expose
private Integer territoryIdTo;

/**
 * Gets the amount of swordsmen.
 *
 * @return the amount sword
 */
public Integer getAmountSword() {
return amountSword;
}

/**
 * Sets the amount of swordsmen.
 *
 * @param amountSword the new amount of swordsmen
 */
public void setAmountSword(Integer amountSword) {
this.amountSword = amountSword;
}

/**
 * Gets the amount of archers.
 *
 * @return the amount of archers
 */
public Integer getAmountBow() {
return amountBow;
}

/**
 * Sets the amount bow.
 *
 * @param amountBow the new amount bow
 */
public void setAmountBow(Integer amountBow) {
this.amountBow = amountBow;
}

/**
 * Gets the amount of knights.
 *
 * @return the amount of knights
 */
public Integer getAmountKnight() {
return amountKnight;
}

/**
 * Sets the amount of knights.
 *
 * @param amountKnight the new amount of knights
 */
public void setAmountKnight(Integer amountKnight) {
this.amountKnight = amountKnight;
}

/**
 * Gets the territory id of the territory from which units are to be moved.
 *
 * @return the territory id
 */
public Integer getTerritoryIdFrom() {
return territoryIdFrom;
}

/**
 * Sets the territory id of the territory from which units are to be moved.
 *
 * @param territoryIdFrom the new territory id from
 */
public void setTerritoryIdFrom(Integer territoryIdFrom) {
this.territoryIdFrom = territoryIdFrom;
}

/**
 * Gets the territory id of the territory to which the units are to be moved.
 *
 * @return the territory id
 */
public Integer getTerritoryIdTo() {
return territoryIdTo;
}

/**
 * Sets the territory id of the territory to which the units are to be moved.
 *
 * @param territoryIdTo the new territory id
 */
public void setTerritoryIdTo(Integer territoryIdTo) {
this.territoryIdTo = territoryIdTo;
}

}