package JsonsParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * The Class Unit.
 */
public class Unit {

/** The allegiance. */
@SerializedName("allegiance")
@Expose
private String allegiance;

/** The existing swords. */
@SerializedName("existingSwords")
@Expose
private Integer existingSwords;

/** The existing archers. */
@SerializedName("existingArchers")
@Expose
private Integer existingArchers;

/** The existing knights. */
@SerializedName("existingKnights")
@Expose
private Integer existingKnights;

/**
 * Generate output message object from corresponding game model object.
 *
 * @param u the u
 */
//public Unit(shared.gameconfig.Unit u) {
//	allegiance = u.getAllegiance();
//	existingSwords = u.getExistingSwords();
//	existingArchers = u.getExistingArchers();
//	existingKnights = u.getExistingKnights();
//}

/**
 * Gets the allegiance.
 *
 * @return the allegiance
 */
public String getAllegiance() {
return allegiance;
}

/**
 * Sets the allegiance.
 *
 * @param allegiance the new allegiance
 */
public void setAllegiance(String allegiance) {
this.allegiance = allegiance;
}

/**
 * Gets the existing swords.
 *
 * @return the existing swords
 */
public Integer getExistingSwords() {
return existingSwords;
}

/**
 * Sets the existing swords.
 *
 * @param existingSwords the new existing swords
 */
public void setExistingSwords(Integer existingSwords) {
this.existingSwords = existingSwords;
}

/**
 * Gets the existing archers.
 *
 * @return the existing archers
 */
public Integer getExistingArchers() {
return existingArchers;
}

/**
 * Sets the existing archers.
 *
 * @param existingArchers the new existing archers
 */
public void setExistingArchers(Integer existingArchers) {
this.existingArchers = existingArchers;
}

/**
 * Gets the existing knights.
 *
 * @return the existing knights
 */
public Integer getExistingKnights() {
return existingKnights;
}

/**
 * Sets the existing knights.
 *
 * @param existingKnights the new existing knights
 */
public void setExistingKnights(Integer existingKnights) {
this.existingKnights = existingKnights;
}


}