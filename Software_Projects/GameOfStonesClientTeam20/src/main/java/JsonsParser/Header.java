package JsonsParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * The Class Header.
 */
public class Header {

	/** The u UID. */
	@SerializedName("UUID")
	@Expose
	private String uUID;
	
	/** The name. */
	@SerializedName("name")
	@Expose
	private String name;
	
	/** The timestamp. */
	@SerializedName("timestamp")
	@Expose
	private Long timestamp;
	
	/** The message type. */
	@SerializedName("messageType")
	@Expose
	private String messageType;

	/**
	 * Gets the uuid.
	 *
	 * @return the uuid
	 */
	public String getUUID() {
		return uUID;
	}

	/**
	 * Sets the uuid.
	 *
	 * @param uUID the new uuid
	 */
	public void setUUID(String uUID) {
		this.uUID = uUID;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the timestamp.
	 *
	 * @return the timestamp
	 */
	public Long getTimestamp() {
		return timestamp;
	}

	/**
	 * Sets the timestamp.
	 *
	 * @param timestamp the new timestamp
	 */
	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * Gets the message type.
	 *
	 * @return the message type
	 */
	public String getMessageType() {
		return messageType;
	}

	/**
	 * Sets the message type.
	 *
	 * @param messageType the new message type
	 */
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

}