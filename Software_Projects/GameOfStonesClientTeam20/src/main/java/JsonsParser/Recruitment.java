package JsonsParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * The Class Recruitment.
 */
public class Recruitment {

/** The territory id. */
@SerializedName("territoryId")
@Expose
private Integer territoryId;

/** The recruited sword. */
@SerializedName("recruitedSword")
@Expose
private Integer recruitedSword;

/** The recruited bow. */
@SerializedName("recruitedBow")
@Expose
private Integer recruitedBow;

/** The recruited knight. */
@SerializedName("recruitedKnight")
@Expose
private Integer recruitedKnight;

/**
 * Gets the territory id.
 *
 * @return the territory id
 */
public Integer getTerritoryId() {
return territoryId;
}

/**
 * Sets the territory id.
 *
 * @param territoryId the new territory id
 */
public void setTerritoryId(Integer territoryId) {
this.territoryId = territoryId;
}

/**
 * Gets the recruited sword.
 *
 * @return the recruited sword
 */
public Integer getRecruitedSword() {
return recruitedSword;
}

/**
 * Sets the recruited sword.
 *
 * @param recruitedSword the new recruited sword
 */
public void setRecruitedSword(Integer recruitedSword) {
this.recruitedSword = recruitedSword;
}

/**
 * Gets the recruited bow.
 *
 * @return the recruited bow
 */
public Integer getRecruitedBow() {
return recruitedBow;
}

/**
 * Sets the recruited bow.
 *
 * @param recruitedBow the new recruited bow
 */
public void setRecruitedBow(Integer recruitedBow) {
this.recruitedBow = recruitedBow;
}

/**
 * Gets the recruited knight.
 *
 * @return the recruited knight
 */
public Integer getRecruitedKnight() {
return recruitedKnight;
}

/**
 * Sets the recruited knight.
 *
 * @param recruitedKnight the new recruited knight
 */
public void setRecruitedKnight(Integer recruitedKnight) {
this.recruitedKnight = recruitedKnight;
}

}