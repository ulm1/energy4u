package JsonsParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * The Class Battle.
 */
public class Battle {

/** The territory id. */
@SerializedName("territoryId")
@Expose
private Integer territoryId;

/** The attacked house. */
@SerializedName("attackedHouse")
@Expose
private String attackedHouse;

/** The attacking house. */
//Makes implementation of the battle algorithm a bit easier
private String attackingHouse;

/**
 * Gets the territory id.
 *
 * @return the territory id
 */
public Integer getTerritoryId() {
return territoryId;
}

/**
 * Sets the territory id.
 *
 * @param territoryId the new territory id
 */
public void setTerritoryId(Integer territoryId) {
this.territoryId = territoryId;
}

/**
 * Gets the attacked house.
 *
 * @return the attacked house
 */
public String getAttackedHouse() {
return attackedHouse;
}

/**
 * Sets the attacked house.
 *
 * @param attackedHouse the new attacked house
 */
public void setAttackedHouse(String attackedHouse) {
this.attackedHouse = attackedHouse;
}

/**
 * Gets the attacking house.
 *
 * @return the attacking house
 */
public String getAttackingHouse() {
	return attackingHouse;
}

/**
 * Sets the attacking house.
 *
 * @param attackingHouse the new attacking house
 */
public void setAttackingHouse(String attackingHouse) {
	this.attackingHouse = attackingHouse;
}

}