package JsonsParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * The Class UpkeepCost.
 */
public class UpkeepCost {

/** The swords. */
@SerializedName("swords")
@Expose
private Integer swords;

/** The archers. */
@SerializedName("archers")
@Expose
private Integer archers;

/** The knights. */
@SerializedName("knights")
@Expose
private Integer knights;

/**
 * Gets the swords.
 *
 * @return the swords
 */
public Integer getSwords() {
return swords;
}

/**
 * Sets the swords.
 *
 * @param swords the new swords
 */
public void setSwords(Integer swords) {
this.swords = swords;
}

/**
 * Gets the archers.
 *
 * @return the archers
 */
public Integer getArchers() {
return archers;
}

/**
 * Sets the archers.
 *
 * @param archers the new archers
 */
public void setArchers(Integer archers) {
this.archers = archers;
}

/**
 * Gets the knights.
 *
 * @return the knights
 */
public Integer getKnights() {
return knights;
}

/**
 * Sets the knights.
 *
 * @param knights the new knights
 */
public void setKnights(Integer knights) {
this.knights = knights;
}

}