package JsonsParser;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * The Class Levelcfg.
 */
public class Levelcfg {

/** The meta. */
@SerializedName("meta")
@Expose
private Meta meta;

/** The game. */
@SerializedName("game")
@Expose
private Game game;

/** The territories. */
@SerializedName("territories")
@Expose
private List<Territory> territories = null;

/** The additions. */
@SerializedName("additions")
@Expose
private List<Addition> additions = null;

/** The table. */
@SerializedName("table")
@Expose
private List<Table> table = null;

/**
 * Gets the meta.
 *
 * @return the meta
 */
public Meta getMeta() {
return meta;
}

/**
 * Sets the meta.
 *
 * @param meta the new meta
 */
public void setMeta(Meta meta) {
this.meta = meta;
}

/**
 * Gets the game.
 *
 * @return the game
 */
public Game getGame() {
return game;
}

/**
 * Sets the game.
 *
 * @param game the new game
 */
public void setGame(Game game) {
this.game = game;
}

/**
 * Gets the territories.
 *
 * @return the territories
 */
public List<Territory> getTerritories() {
return territories;
}

/**
 * Sets the territories.
 *
 * @param territories the new territories
 */
public void setTerritories(List<Territory> territories) {
this.territories = territories;
}

/**
 * Gets the additions.
 *
 * @return the additions
 */
public List<Addition> getAdditions() {
return additions;
}

/**
 * Sets the additions.
 *
 * @param additions the new additions
 */
public void setAdditions(List<Addition> additions) {
this.additions = additions;
}

/**
 * Gets the table.
 *
 * @return the table
 */
public List<Table> getTable() {
return table;
}

/**
 * Gets the table.
 *
 * @param terrain the terrain
 * @return the table
 */
public Table getTable(String terrain) {
	for(Table t: table) {
		if(terrain.equals(t.getTerrain())) return t;
	}
	
	return null;
}

/**
 * Sets the table.
 *
 * @param table the new table
 */
public void setTable(List<Table> table) {
this.table = table;
}

}