package JsonsParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;




/**
 * The Class ClientData.
 */
public class ClientData {

	/** The header. */
	@SerializedName("header")
	@Expose
	private Header header;
	
	/** The body. */
	@SerializedName("body")
	@Expose
	private Body body;

	/**
	 * Gets the header.
	 *
	 * @return the header
	 */
	public Header getHeader() {
		return header;
	}

	/**
	 * Sets the header.
	 *
	 * @param header the new header
	 */
	public void setHeader(Header header) {
		this.header = header;
	}

	/**
	 * Gets the body.
	 *
	 * @return the body
	 */
	public Body getBody() {
		return body;
	}

	/**
	 * Sets the body.
	 *
	 * @param body the new body
	 */
	public void setBody(Body body) {
		this.body = body;
	}

}