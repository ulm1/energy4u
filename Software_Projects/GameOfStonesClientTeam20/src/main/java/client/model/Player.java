package client.model;

import java.util.Observable;

public class Player extends Participant {

	public String userName;
	public String color;
	public int userID;


	public Player(String userName, boolean isAI, int userID) {
		super(userName, "USER", isAI, userID);
		this.userName = userName;
		this.isAI = isAI;
		this.userID = userID;
	}

	public void setColor(String color) {
		this.color = color;
	}

	@Override
	public void update(Observable o, Object arg) {

	}

}