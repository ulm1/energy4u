package client.model;

import java.util.Observer;

public abstract class Participant implements Observer {

	public String partName;
	public String role;
	public boolean isAI;
	public int userID;
	public Participant(String name, String role, boolean ai, int uid) {
		this.partName = name;
		this.role = role;
		this.isAI = ai;
		this.userID = uid;
	}

}