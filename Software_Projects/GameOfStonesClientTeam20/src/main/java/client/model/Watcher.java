package client.model;

import java.util.Observable;

public class Watcher extends Participant {

	public Watcher(String userName, boolean isAI, int userID) {
		super(userName, "SPECTATOR", isAI, userID);
	}
	@Override
	public void update(Observable o, Object arg) {

	}
}