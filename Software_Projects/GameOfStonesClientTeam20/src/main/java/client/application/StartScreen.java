package client.application;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.niftygui.NiftyJmeDisplay;
import com.jme3.scene.Node;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.controls.TextField;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;
import de.lessvoid.nifty.tools.Color;
import de.lessvoid.nifty.tools.SizeValue;

public class StartScreen extends AbstractAppState implements ScreenController {

	private SimpleApplication app;
	private GameofStones mainApp;
	private Nifty nifty;

	Node startScreen = new Node("startScreen");
	private static boolean popupShows;

	// initial startScreen entries
	public static String initUsername = "username";
	public static String initMainColor = "red";
	public static Color initMainColorCode = new Color("#EF0000");
	public static String initSideColor = "red";
	public static Color initSideColorCode = new Color("#FF1010");
	public static String initIpAddress= "IPaddress";
	public static String initPortNumber= "Port";
	NiftyJmeDisplay niftyDisplay;

	public static boolean updatePanelPlayer = false;
	public static boolean updatePanelWatcher = false;
	public static int PanelHeight;
	public static int currentPanelHeight;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void initialize(AppStateManager stateManager, Application app) {
		super.initialize(stateManager, app);
		this.app = (SimpleApplication) app;
		mainApp = GameofStones.getMyself();
		// initialize stuff that is independent of whether state is PAUSED or
		// RUNNING
		popupShows = false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void cleanup() {
		super.cleanup();
		nifty.exit();
		app.getGuiViewPort().removeProcessor(niftyDisplay);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setEnabled(boolean enabled) {
		// pause and unpause
		super.setEnabled(enabled);

		if (enabled) {
			popupShows = false;

			// custom key bindings : map input to named actions
			this.app.getInputManager().addMapping("Connect", new KeyTrigger(KeyInput.KEY_RETURN),
					new KeyTrigger(KeyInput.KEY_NUMPADENTER));
			this.app.getInputManager().addMapping("Next", new KeyTrigger(KeyInput.KEY_TAB));
			this.app.getInputManager().addMapping("Quit", new KeyTrigger(KeyInput.KEY_ESCAPE));

			// add the names to the action listener
			this.app.getInputManager().addListener(actionListener, "Connect");
			this.app.getInputManager().addListener(actionListener, "Next");
			this.app.getInputManager().addListener(actionListener, "Quit");

			// initialize stuff that is in use while this state is RUNNING
			niftyDisplay = NiftyJmeDisplay.newNiftyJmeDisplay(app.getAssetManager(), app.getInputManager(),
					app.getAudioRenderer(), app.getGuiViewPort());
			nifty = niftyDisplay.getNifty();
			nifty.fromXml("client/util/startScreen.xml", "startScreen", this);

			// attach the nifty display to the GUI view port as a processor
			app.getGuiViewPort().addProcessor(niftyDisplay);
			app.getInputManager().setCursorVisible(true);

			Screen startScreen = nifty.getCurrentScreen();

			TextField tfUsername = startScreen.findNiftyControl("tfUsername", TextField.class);
			tfUsername.setText(initUsername);

		
			

			TextField tfIpAddress = startScreen.findNiftyControl("tfIpAddress", TextField.class);
			tfIpAddress.setText(initIpAddress);

			TextField tfPortNumber = startScreen.findNiftyControl("tfPortNumber", TextField.class);
			tfPortNumber.setText(initPortNumber);

			app.getRootNode().attachChild(this.startScreen);
		} else {
			popupShows = false;

			// take away everything not needed while this state is PAUSED
			app.getRootNode().detachChild(startScreen);

		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	// enabled.
	public void update(float tpf) {
		// do the following while game is RUNNING
		if (updatePanelPlayer) {

			// calculate new height of color choosing robot panel
			currentPanelHeight += 6 * tpf * PanelHeight;
			if (currentPanelHeight > PanelHeight)
				currentPanelHeight = PanelHeight;


			if (currentPanelHeight == PanelHeight)
				updatePanelPlayer = false;

		} else if (updatePanelWatcher) {

			currentPanelHeight -= 6 * tpf * PanelHeight;
			if (currentPanelHeight < 0) {
				currentPanelHeight = 0;
			}

			
			if (currentPanelHeight == 0) {
				updatePanelWatcher = false;
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void bind(Nifty nifty, Screen screen) {

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onStartScreen() {
		// nothing
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onEndScreen() {
		// nothing
	}

	public static void popupErrorShows(boolean bool) {
		popupShows = bool;
	}

	public void backToMenu() {

	}

	private ActionListener actionListener = new ActionListener() {
		public void onAction(String name, boolean keyPressed, float tpf) {
			// get currently focused element as well as all buttons
			Element focEl = nifty.getCurrentScreen().getFocusHandler().getKeyboardFocusElement();
			Element textUsername = nifty.getCurrentScreen().findElementById("tfUsername");
			Element radioPlayer = nifty.getCurrentScreen().findElementById("rbPlayer");
			Element radioWatcher = nifty.getCurrentScreen().findElementById("rbWatcher");
			Element textIpAddress = nifty.getCurrentScreen().findElementById("tfIpAddress");
			Element textPortNum = nifty.getCurrentScreen().findElementById("tfPortNumber");
			Element button = nifty.getCurrentScreen().findElementById("btnConnect");

			if (keyPressed && name.equals("Connect")) {
				if (popupShows) {
					// if errorPopup is active, click okay button
					nifty.getCurrentScreen().getTopMostPopup().findElementById("button_ok")
							.onClickAndReleasePrimaryMouseButton();
					;
				} else if (focEl.equals(radioPlayer)) {
					// when focus on radioButton 'player', click that option
					radioPlayer.onClickAndReleasePrimaryMouseButton();
				} else if (focEl.equals(radioWatcher)) {
					// when focus on radioButton 'watcher', click that option
					radioWatcher.onClickAndReleasePrimaryMouseButton();
				} else if (focEl.equals(textUsername) || focEl.equals(textIpAddress) || focEl.equals(textPortNum)
						|| focEl.equals(button)) {
					// when focus not on any radioButton, click btnConnect
					button.onClickAndReleasePrimaryMouseButton();
				}
			}

			if (!keyPressed && !popupShows && name.equals("Next")) {
				if (focEl.equals(radioPlayer) || focEl.equals(radioWatcher) || focEl.equals(button)) {
					// when focus on any kind of button, click header (only
					// re-initializes empty textFields)
					nifty.getCurrentScreen().findElementById("headerImg").onClickAndReleasePrimaryMouseButton();
				} else {
					// when focus not on any kind of button, click focused
					// element
					focEl.onClickAndReleasePrimaryMouseButton();
				}

			}

			if (keyPressed && !popupShows && name.equals("Quit")) {
				backToMenu();
			}
		}
	};
}
