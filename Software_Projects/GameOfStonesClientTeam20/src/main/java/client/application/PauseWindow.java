package client.application;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.niftygui.NiftyJmeDisplay;
import com.jme3.scene.Node;

import client.controller.PhaseController;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;

public class PauseWindow extends AbstractAppState implements ScreenController {
    /**
     * Initialize the SimpleApplication
     * In addition, the Nifty is integrated and thus the ScreenController can be used.
     */
    private SimpleApplication app;
    private GameofStones mainApp;
    private Nifty nifty;
    NiftyJmeDisplay niftyDisplayMenu;
    Node phase = new Node("pause");
	private static boolean popupShows;
	public static final String NAME = "dragAndDropDialog";

	 
	
	 @Override
	    public void initialize(AppStateManager stateManager, Application app) {
	        super.initialize(stateManager, app);
	        this.app = (SimpleApplication) app;
	        mainApp = GameofStones.getMyself();
	        
	        
	    }
	 
	  @Override
	    public void cleanup() {
	        super.cleanup();
	
	       
	    }
	
	 
		@Override
		// Note that update is only called while the state is both attached and enabled.
	    public void update(float tpf) {
			
			PhaseController.updateText();
			
	    	// do the following while game is RUNNING

	    }
		
		String currentPhase;
	
	  @Override
	    public void setEnabled(boolean enabled) {
	        // pause and unpause
	        super.setEnabled(enabled);
	           
	        

	        if(enabled){
	            niftyDisplayMenu = NiftyJmeDisplay.newNiftyJmeDisplay(
	                    app.getAssetManager(),
	                    app.getInputManager(),
	                    app.getAudioRenderer(),
	                    app.getGuiViewPort());
	            nifty = niftyDisplayMenu.getNifty();
	            
	   

	            nifty.fromXml("PhasePanel/phase.xml", "start");

	    
	            nifty.loadStyleFile("nifty-default-styles.xml");
	            nifty.loadControlFile("nifty-default-controls.xml");
	            
	            
	     
	            app.getGuiViewPort().addProcessor(niftyDisplayMenu);
	            app.getInputManager().setCursorVisible(true);

	            app.getGuiNode().attachChild(this.phase);
	            
	        }
	  	}
	  
		public static void popupErrorShows(boolean bool) {
			popupShows = bool;
		}

	 
	@Override
	public void bind(Nifty arg0, Screen arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onEndScreen() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStartScreen() {
		// TODO Auto-generated method stub
		
	}


}
