package client.application.GameFieldAdditions;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.jme3.asset.AssetManager;
import com.jme3.asset.plugins.FileLocator;
import com.jme3.material.Material;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Quad;
import com.jme3.texture.Image;
import com.jme3.texture.Texture;
import com.jme3.texture.Texture2D;
import com.jme3.texture.plugins.AWTLoader;
import com.jme3.ui.Picture;

import JsonsParser.Territory;
import client.application.GameFieldEnums.Houses;

public class TerritoryAllegiance {

	private final  Vector3f pointOfCapitol;
	private final int Id;
	private final AssetManager assetManager;
	private final Node flagNode;
	private final Territory territory;
	
	private Geometry flagGeo;
	private Mesh flagMesh;
	private Material flagMat;
	private Texture texture;
	
	public TerritoryAllegiance(Vector3f point, int i, AssetManager asManager, Territory territory)
	{
		pointOfCapitol = point;
		Id = i;
		assetManager = asManager;
		this.territory = territory;
		
		flagNode = new Node();
		float iconSize = 0.5f;
		flagMesh = new Quad(iconSize, iconSize);
		flagGeo = new Geometry("quad", flagMesh);
		flagMat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		texture = new Texture2D();
		String house = territory.getHouse();
		if( house != null)
		{
			String[] splittedHouse = house.split(" ");
			
			if(splittedHouse.length < 2)
			{
				throw new IllegalArgumentException();
			}
			
			Houses terHouse = Houses.valueOf(splittedHouse[1]);
			BufferedImage bfImg;
			try {
				bfImg = ImageIO.read(terHouse.flag);
				Image img = new AWTLoader().load(bfImg, true);
				texture.setImage(img);
				flagMat.setTransparent(false);
			} catch (IOException e) {
				e.printStackTrace();
			}
			flagNode.attachChild(flagGeo);
		}
		else
		{
			BufferedImage bfImg;
			try {
				bfImg = ImageIO.read(new File("./Assets/Stark.png"));
				Image img = new AWTLoader().load(bfImg, true);
				texture.setImage(img);
			} catch (IOException e) {
				e.printStackTrace();
			}
			flagMat.setTransparent(true);
			flagNode.detachAllChildren();
		}
		flagMat.setTexture("ColorMap", texture);
		flagGeo.setMaterial(flagMat);
		flagNode.setLocalTranslation(new Vector3f(pointOfCapitol.x - iconSize/2, pointOfCapitol.y - iconSize/2 , pointOfCapitol.z));
		if(territory.isTavern())
		{
			Mesh TavernMesh = new Quad(iconSize / 2, iconSize/ 2);
			Geometry TavernGeo = new Geometry("quad", TavernMesh);
			Material TavernMat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
			Texture TavernTexture = new Texture2D();
			try {
				BufferedImage bfImg = ImageIO.read(new File("./Assets/Wirtshaus.png"));
				Image img = new AWTLoader().load(bfImg, true);
				TavernTexture.setImage(img);
			} catch (IOException e) {
				e.printStackTrace();
			}
			TavernMat.setTexture("ColorMap", TavernTexture);
			TavernGeo.setMaterial(TavernMat);
			TavernGeo.setLocalTranslation(- iconSize/2 , iconSize/2, 0);
			flagNode.attachChild(TavernGeo);
		}
		if(territory.getCastle())
		{
			Mesh CastleMesh = new Quad(iconSize / 2, iconSize/ 2);
			Geometry CastleGeo = new Geometry("quad", CastleMesh);
			Material CastleMat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
			Texture CastleTexture = new Texture2D();
			try {
				BufferedImage bfImg = ImageIO.read(new File("./Assets/Castle.png"));
				Image img = new AWTLoader().load(bfImg, true);
				CastleTexture.setImage(img);
			} catch (IOException e) {
				e.printStackTrace();
			}
			CastleMat.setTexture("ColorMap", CastleTexture);
			CastleGeo.setMaterial(CastleMat);
			CastleGeo.setLocalTranslation(- iconSize/2 , 0, 0);
			flagNode.attachChild(CastleGeo);
		}
		
	}
	
	public Vector3f getCapitolLocation()
	{
		return pointOfCapitol;
	}
	
	public int getId()
	{
		return Id;
	}
	
	public Node getFlagNode()
	{
		return flagNode;
	}
	
	public void updateAllegianceChange()
	{
		String house = territory.getHouse();
		if( house != null && !house.equals(""))
		{	
			String[] splittedHouse = house.split(" ");
			
			if(splittedHouse.length < 2)
			{
				throw new IllegalArgumentException();
			}
			
			Houses terHouse = Houses.valueOf(splittedHouse[1]);
			BufferedImage bfImg;
			
			try {
				bfImg = ImageIO.read(terHouse.flag);
				Image img = new AWTLoader().load(bfImg, true);
				texture.setImage(img);
				flagMat.setTransparent(false);
			} catch (IOException e) {
				e.printStackTrace();
			}
			flagNode.attachChild(flagGeo);
		}
		else
		{
			flagNode.detachAllChildren();
		}
	}
}
