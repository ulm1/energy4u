package client.application.GameFieldAdditions;

import com.jme3.asset.AssetManager;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.VertexBuffer;

/**
 * This class is designed to store and generate everything needed to depicture a path on the GameMap.
 * @author Berfin
 *
 */
public class Path {
	
	private Geometry lineGeo;
	private Material lineMat;
	
	/**
	 * This constructor build a bath between the two given points with the help of the Assetmanager.
	 * @param firstPoint: Its the first point on the map
	 * @param secondPoint: Its the second point on the map
	 * @param aManager: This is the Assetmanager. It is used to read the material definitions form the configuration files.
	 */
	public Path(Vector3f firstPoint, Vector3f secondPoint, AssetManager aManager)
	{
		Mesh lineMesh = new Mesh();
		
		lineMesh.setMode(Mesh.Mode.Lines);
		float[] linePoints = new float[6];
		
		linePoints[0] = firstPoint.x;
		linePoints[1] = firstPoint.y;
		linePoints[2] = firstPoint.z;
		linePoints[3] = secondPoint.x;
		linePoints[4] = secondPoint.y;
		linePoints[5] = secondPoint.z;
		
		lineMesh.setBuffer(VertexBuffer.Type.Position, 3, linePoints);
		lineMesh.setBuffer(VertexBuffer.Type.Index, 2, new short[] {0,1});
		
		lineGeo = new Geometry("line", lineMesh);
		lineMat = new Material(aManager, "Common/MatDefs/Misc/Unshaded.j3md");
		lineMat.setColor("Color", ColorRGBA.Black);
		lineGeo.setMaterial(lineMat);
		
	}
	
	/**
	 * This functions returns the generated Line Geometry figure.
	 * 
	 * @return the geometry object containing the line
	 */
	public Geometry getGeometryLine()
	{
		return lineGeo;
	}
	
	
	/**
	 * This function changes the color of this path.
	 * @param selectedColor: is the new color the path should get.
	 */
	public void changeColor(ColorRGBA selectedColor)
	{
		lineMat.setColor("Color", selectedColor);
	}
	
}
