package client.application;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.TexturePaint;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.json.JsonArray;
import javax.json.JsonObject;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.collision.CollisionResult;
import com.jme3.collision.CollisionResults;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.MouseAxisTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.input.controls.Trigger;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Ray;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Box;
import com.jme3.scene.shape.Quad;
import com.jme3.texture.Image;
import com.jme3.texture.Texture;
import com.jme3.texture.Texture2D;
import com.jme3.texture.plugins.AWTLoader;

import JsonsParser.Territory;
import client.application.GameFieldAdditions.Path;
import client.application.GameFieldAdditions.TerritoryAllegiance;
import client.controller.StartScreenController;
import client.model.Player;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.controls.Console;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;

public class GameMap extends AbstractAppState implements ScreenController {

	private final int mapDimensions = 10;
	
	private SimpleApplication app;
	private final static Trigger TRIGGER_EXIT = new MouseButtonTrigger(MouseInput.BUTTON_RIGHT);
	private final static String MAPPING_EXIT = "Exit";
	private UpdateBox updateBox;
	private ArrayList<TerritoryAllegiance> normalisierteTerritoriumsPunkte;
	private Path[][] linePaths;
	private Nifty nifty;
	static Node vor;
	// private GamePopupController pop;
	StartScreenController pop;

	private LinkedList<Vector2f> points;
	
	int X_START = 0;
	int Y_START = -2;
	int Z_START = -2;
	int CHECKPOINTS = 0;
	int PORTAL = 0;
	int HEALTH = 0;
	static int x;
	static int y;

	int FIELD_WIDTH;
	int FIELD_HEIGHT;
	static ArrayList<MyPolgon> po;

	String id = "Box";
	String idB = "Border";
	String idO = "Obstacle";
	Geometry geo;
	// Voronoi v;


	ArrayList<Player> playerList;
	ArrayList<ColorRGBA> colorList;

	int idNumber = 1;
	int borderNumber = 0;
	boolean toggle_rotation = true;
	Node field = new Node("field");
	static int oID = 1; // static IDs for referring to Spatials Obstalces
	static int nID = 1; // static IDs for referring to nodes
	int activeAnimations = 0;
	float cursorPosX;
	float cursorPosY;
	float colorV = 0.6f;
	float colorS = 0.9f;

	private boolean GAME_IS_PAUSED = false;

	// Declaring the necessary Material and Shapes
	Material gameField, checkpointField, border, borderCorner, obstacle, healthField, portalField, lbField, lrField;
	Box box, obstacleBox;

	public GameMap() {

	}

	/**
	 * JME AppState Method for initializing the AppState. In this specific case, the
	 * method initializes all Shape and Material ClassVariables to there intended
	 * visualization. Change here the images if you want to.
	 */
	public void initialize(AppStateManager stateManager, Application app) {
		super.initialize(stateManager, app);
		this.app = (SimpleApplication) app;

		// Initializing the necessary shapes
		box = new Box(0.5f, 0.5f, 0);
		obstacleBox = new Box(0.5f * 0.1664f, 0.5f * 1.1664f, 0);

		// Initializing necessary materials


		app.getInputManager().addMapping(MAPPING_EXIT, TRIGGER_EXIT);

		app.getInputManager().addMapping("CLICK", new MouseButtonTrigger(MouseInput.BUTTON_LEFT));
		app.getInputManager().addMapping("DRAG", new MouseButtonTrigger(MouseInput.BUTTON_LEFT));
		app.getInputManager().addMapping("ZOOM_IN", new MouseAxisTrigger(MouseInput.AXIS_WHEEL, true));
		app.getInputManager().addMapping("ZOOM_OUT", new MouseAxisTrigger(MouseInput.AXIS_WHEEL, false));

		app.getInputManager().addListener(actionListener, "CLICK");
		// app.getInputManager().addListener(analogListener, "DRAG");
		app.getInputManager().addListener(actionListener, "ZOOM_IN");
		app.getInputManager().addListener(actionListener, "ZOOM_OUT");

	}

	@Override
	public void cleanup() {
		super.cleanup();
		// unregister all my listeners, detach all my nodes, etc....
	}

	/**
	 * JME Method for enabling the AppState
	 */

	static List<Territory> terr = null;

	public boolean edges[][];
	public int territoryMap[][];

	public LinkedList<Vector2f> borders[];

	static double p = 3;
	//static BufferedImage I;
	public static int px[], py[], color[], cells = 24, size = 1000;
	ArrayList<Polygon> pol = new ArrayList<Polygon>();
	Geometry geometry;
	Polygon newPolygon;
	ArrayList<MyPolgon> p2 = new ArrayList<MyPolgon>();
	Material material;
	Node voronoi = new Node("voronoi");

	static double distance(int x1, int x2, int y1, int y2) {
		double d;
		// d = Math.abs(x1 -x2) + Math.abs(y1 - y2);// Manhatten Distance
		// d = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)); // Euclidian
		d = Math.pow(Math.pow(Math.abs(x1 - x2), p) + Math.pow(Math.abs(y1 - y2), p), (1 / p)); // Minkovski
		return d;
	}
	
	
	public void GameMap() {
		int closeWidth = GameofStones.SCREEN_WIDTH / 5;
		int closeHeight = closeWidth / 2;
		int n = 0;
		Random rand = new Random();
		//I = new BufferedImage(size, size, BufferedImage.TYPE_INT_RGB);
		px = new int[cells];
		py = new int[cells];
		color = new int[cells];
		points = new LinkedList<Vector2f>();
		
		
		edges = new boolean[cells][cells];
		territoryMap = new int[1000][1000];
		borders = new LinkedList[cells];
		linePaths = new Path[cells][cells];
		for (int i = 0; i < cells; i++) {
			px[i] = rand.nextInt(size);
			py[i] = rand.nextInt(size);
			color[i] = rand.nextInt(16777215);
			
			points.add(new Vector2f(px[i], py[i]));
			
			edges[i][0] = i == 0 ? false : true;
			edges[0][i] = i == 0 ? false : true;

			Mesh mesh = new Quad(mapDimensions, mapDimensions);
			geometry = new Geometry("quad", mesh);

			borders[i] = new LinkedList<Vector2f>();

		}

		int previousN = -1;
		for (int x = 0; x < size; x++) {
			previousN = -1;
			for (int y = 0; y < size; y++) {
				n = 0;
				for (int i = 1; i < cells; i++) {
					if (distance(px[i], x, py[i], y) < distance(px[n], x, py[n], y)) {
						n = i;

					}
				}
				if (y == size - 1 || y == 0 || x == 0) {
					borders[n].addLast(new Vector2f(x, y));
				} else if (previousN == n) {
					continue;
				} else // case Border was crossed horizontally
				{
					borders[n].addFirst(new Vector2f(x, y));
					borders[previousN].addLast(new Vector2f(x, y - 1));
				}

				previousN = n;

				territoryMap[x][y] = n;

			}
		}

		BufferedImage img = null;
		BufferedImage imgw = null;
		BufferedImage imgm = null;
		BufferedImage imgg = null;
		BufferedImage imgh = null;
		
		try {
			img = ImageIO.read(new File("./Assets/Wald.jpg"));
			imgw = ImageIO.read(new File("./Assets/W�ste.jpg"));
			imgm = ImageIO.read(new File("./Assets/Gebirge.jpg"));
			imgg = ImageIO.read(new File("./Assets/Grasland.jpg"));
			imgh = ImageIO.read(new File("./Assets/Sumpf.jpg"));
		} catch (IOException e) {
			System.out.println("Error: " + e);
		}

		BufferedImage bfImg = new BufferedImage(size, size, BufferedImage.TYPE_INT_RGB);

		for (int i = 0; i < cells; i++) {
			int sizeArray = borders[i].size();
			int[] xCords = new int[sizeArray];
			int[] yCords = new int[sizeArray];

			for (int j = 0; j < sizeArray; j++) {
				Vector2f cord = (Vector2f) borders[i].get(j);
				xCords[j] = (int) cord.x;
				yCords[j] = (int) cord.y;
			}

			newPolygon = new Polygon(xCords, yCords, sizeArray);
			
			MyPolgon p = new MyPolgon(newPolygon, pol.size());
			pol.add(newPolygon);
			p2.add(p);

			// Roboter.setPosition(px[0] - 5, py[0] - 5);

			Graphics2D pic = bfImg.createGraphics();

			String terrain2 = terr.get(p2.size() - 1).getTerrain();

			switch (terrain2) {

			case "forest":
				TexturePaint pd = new TexturePaint(img, new Rectangle(pol.get(i).getBounds().x,
						pol.get(i).getBounds().y, pol.get(i).getBounds().width, pol.get(i).getBounds().height));
				pic.setPaint(pd);
				break;
			case "desert":
				pd = new TexturePaint(imgw, new Rectangle(pol.get(i).getBounds().x, pol.get(i).getBounds().y,
						pol.get(i).getBounds().width, pol.get(i).getBounds().height));
				pic.setPaint(pd);
				break;
			case "mountains":
				pd = new TexturePaint(imgm, new Rectangle(pol.get(i).getBounds().x, pol.get(i).getBounds().y,
						pol.get(i).getBounds().width, pol.get(i).getBounds().height));
				pic.setPaint(pd);
				break;
			case "plain":
				pd = new TexturePaint(imgg, new Rectangle(pol.get(i).getBounds().x, pol.get(i).getBounds().y,
						pol.get(i).getBounds().width, pol.get(i).getBounds().height));
				pic.setPaint(pd);
				break;

			case "swamp":
				pd = new TexturePaint(imgh, new Rectangle(pol.get(i).getBounds().x, pol.get(i).getBounds().y,
						pol.get(i).getBounds().width, pol.get(i).getBounds().height));
				pic.setPaint(pd);
				break;

			default:
				pic.setColor(Color.green);
				break;

			}

			pic.fillPolygon(newPolygon);

		}

		AWTLoader loader = new AWTLoader();
		Image load = loader.load(bfImg, true);
		
		
		Texture t = new Texture2D();
		
		t.setImage(load);
		
		material = new Material(app.getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md");
		material.setTexture("ColorMap", t);
		
		// material.setTextureParam("ColorMap", varType, new
		// Texture2D(awtLoader.load(bufferedImage, true)));
		geometry.setMaterial(material);
		

		Box box2 = new Box(1, 1, 1);
		Geometry red = new Geometry("Box", box2);
		// red.setLocalTranslation(new Vector3f(1,3,1));
		Material mat2 = new Material(app.getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md");
		mat2.setColor("Color", ColorRGBA.Red);
		red.setMaterial(mat2);
		// (voronoi.attachChild(red);

		float leftCornerY = -2f;
		float leftCornerX = -3;
		voronoi.setLocalTranslation(leftCornerX, leftCornerY, -1);
		
		voronoi.attachChild(geometry);
		app.getRootNode().attachChild(voronoi);
		Vector3f worldTranslationOfTarget = voronoi.getWorldTranslation();
	
		normalisierteTerritoriumsPunkte = new ArrayList<TerritoryAllegiance>();
		for(int i = 0; i < points.size(); i++)
		{
			Vector2f pointOnTheMap = points.get(i);
			float x = (pointOnTheMap.x) * ((float)mapDimensions) / ((float)size) + worldTranslationOfTarget.x;
			float y = mapDimensions - (pointOnTheMap.y) * ((float)mapDimensions) / ((float)size) + worldTranslationOfTarget.y;
			float z = worldTranslationOfTarget.z;
			
			TerritoryAllegiance newTerritory = new TerritoryAllegiance(new Vector3f(x, y, z), i, app.getAssetManager(), updateBox.terr.get(i));
			
			normalisierteTerritoriumsPunkte.add(newTerritory);
			app.getRootNode().attachChild(newTerritory.getFlagNode());	
		}
		
		//Hier findet die Berechnung der Wege zwischen den einzelnen Landschaften statt, welche vom Server gesendet wurden.
		for (int i = 0; i < cells; i++) {
			for (int j = i + 1; j < cells; j++) {
				if (i == j)
					continue;
				if (edges[i][j]) {
					
					Vector3f firstPoint  = normalisierteTerritoriumsPunkte.get(i).getCapitolLocation();
					Vector3f secondPoint = normalisierteTerritoriumsPunkte.get(j).getCapitolLocation();
					
					linePaths[i][j] = new Path(firstPoint, secondPoint, app.getAssetManager());
					linePaths[j][i] = linePaths[i][j];
					
					app.getRootNode().attachChild(linePaths[i][j].getGeometryLine());
					edges[i][j] = false;
					edges[j][i] = false;
				}
			}
		}
		
		
		
	}

	@Override
	public void setEnabled(boolean enabled) {
		// Pause and unpause
		int closeWidth = GameofStones.SCREEN_WIDTH / 5;
		int closeHeight = closeWidth / 2;
		super.setEnabled(enabled);
		if (enabled) {
			// init stuff that is in use while this state is RUNNING
		
			GameMap();
			attachGuiElements();

			app.getCamera().setLocation(new Vector3f(0, 0, 15));
			// createColorSet(playerList.size());
		

			ButtonDialog buttonDialog = new ButtonDialog();
			app.getStateManager().attach(buttonDialog);
			buttonDialog.initialize(app.getStateManager(), app);
			app.getStateManager().getState(ButtonDialog.class).setEnabled(true);
			attachGuiElements();
			app.getGuiNode().getChild("buttonDialog").move(0, GameofStones.SCREEN_HEIGHT / 50 + closeHeight, 0);

			// test where button is created

			Phase c = new Phase();
			app.getStateManager().attach(c);
			c.initialize(app.getStateManager(), app);
			app.getStateManager().getState(Phase.class).setEnabled(true);
			attachGuiElements();
			app.getGuiNode().getChild("phase").move(0, GameofStones.SCREEN_HEIGHT / 50 + closeHeight, 0);

			// where UpdateBox is created basically attached
			updateBox = new UpdateBox();
			app.getStateManager().attach(updateBox);
			updateBox.initialize(app.getStateManager(), app);
			app.getStateManager().getState(UpdateBox.class).setEnabled(true);
			attachGuiElements();
			app.getGuiNode().getChild("updateBox").move(0, GameofStones.SCREEN_HEIGHT * 9 / 10
					- ((86 * (1 + 1) + 179 + 182) * (float) (GameofStones.SCREEN_WIDTH) / 2250f), 0);

		} else {
			// take away everything not needed while this state is PAUSED
			app.getRootNode().detachChild(voronoi);
			app.getRootNode().detachAllChildren();
			app.getGuiNode().detachAllChildren();
		

		}
	}

	// Note that update is only called while the state is both attached and
	// enabled.
	@Override
	public void update(float tpf) {
		// do the following while game is RUNNING
		

	}

	private void attachGuiElements() {
	
	}



	

	private ActionListener actionListener = new ActionListener() {
		@SuppressWarnings("null")
		public void onAction(String name, boolean keyPressed, float tpf) {
			if (name.equals("CLICK") && keyPressed) {

				CollisionResults results = new CollisionResults();
				Vector2f click2d = app.getInputManager().getCursorPosition();
				Vector3f click3d = app.getCamera().getWorldCoordinates(new Vector2f(click2d.getX(), click2d.getY()),
						0f);
				Vector3f dir = app.getCamera().getWorldCoordinates(new Vector2f(click2d.getX(), click2d.getY()), 1f)
						.subtractLocal(click3d);
				Ray ray = new Ray(click3d, dir);
				app.getRootNode().collideWith(ray, results);
				CollisionResult colRes = results.getClosestCollision(); // it should only exist one collision;
				if(colRes == null) return;
				Geometry hitTarget = colRes.getGeometry();
				if(hitTarget.getName().equals("quad"))
				{	
					int index = 0;
					float shortestDistance = 10000;
					Vector3f zwSave = colRes.getContactPoint();
					Vector3f worldTranslationOfTarget = hitTarget.getWorldTranslation();
					float mapOffset = ((float)size) / ((float)mapDimensions);
					float calculatedX = (zwSave.x - worldTranslationOfTarget.x) * mapOffset;
					float calculatedY = size - (zwSave.y - worldTranslationOfTarget.y) * mapOffset;
					Vector2f hitPoint = new Vector2f( calculatedX, calculatedY);
					
					for ( int i = 0; i < points.size(); i++) 
					{
						Vector2f pointsOnTheMap = points.get(i);
						float newDistance = pointsOnTheMap.distance(hitPoint);
						if(newDistance < shortestDistance)
						{
							shortestDistance = newDistance;
							index = i;
						}
					}
					updateBox.setId(index);
					changeActivePaths(index);
				}

				

			} else if (name.equals("ZOOM_IN")) {
				Vector3f camPos = app.getCamera().getLocation();
				app.getCamera().setLocation(new Vector3f(camPos.x, camPos.y, camPos.z - 1));
			} else if (name.equals("ZOOM_OUT")) {
				Vector3f camPos = app.getCamera().getLocation();
				app.getCamera().setLocation(new Vector3f(camPos.x, camPos.y, camPos.z + 1));
			}
		}
	};

	public String findNiftyControl(String console, Class<Console> consoleClass) {
		return console;
	}

	@Override
	public void bind(Nifty arg0, Screen arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onEndScreen() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStartScreen() {
		// TODO Auto-generated method stub

	}



	public static void setPosition(int i, int j) {
		x = i;
		y = j;

	}

	public static void Node(Node voronoi) {
		vor = voronoi;

	}

	public static void setPOL(ArrayList<MyPolgon> p2) {
		po = p2;

	}

	public void setTerritories(List<Territory> house) {
		terr = house;

	}

	
	/**
	 * This function sets the edges for the GameMap generation.
	 * @param edges2: this is a two Dimensional List, which contains all Edges between all vertexes.
	 */
	public void setEdges(List<List<Boolean>> edges2) {
		
		int sizeOfList = edges2.size();
		
		edges = new boolean[sizeOfList][sizeOfList];
		
		for(int i = 0; i < sizeOfList; i++)
		{
			if(edges2.get(i).size() != sizeOfList)
			{
				throw new IllegalArgumentException();
			}
			
			List<Boolean> edges2List = edges2.get(i);
			
			for(int j = 0; j < sizeOfList; j++)
			{
				edges[i][j] = edges2List.get(j);
			}
		}
		
	}
	
	
	/**
	 * This function gets triggered, if the selected ID gets change to hightlight the paths, which can be used from this Territory
	 * @param selectedId: Is the new selected ID.
	 */
	private void changeActivePaths(int selectedId)
	{
		for(int i = 0; i < cells; i++)
		{
			for(int j = 0; j < i; j++)
			{
				if(linePaths[i][j] == null)
				{
					continue;
				}
				
				if(i == selectedId || j == selectedId )
				{
					linePaths[i][j].changeColor(ColorRGBA.Red);
				}
				else
				{
					linePaths[i][j].changeColor(ColorRGBA.Black);
				}
			}
		}
	}
}
