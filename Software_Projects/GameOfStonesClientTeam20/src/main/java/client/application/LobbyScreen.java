package client.application;


import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.niftygui.NiftyJmeDisplay;
import com.jme3.scene.Node;

import client.controller.LobbyScreenController;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;
import de.lessvoid.nifty.tools.SizeValue;

public class LobbyScreen extends AbstractAppState implements ScreenController {
    /**
     * Initialize the SimpleApplication
     * In addition, the Nifty is integrated and thus the ScreenController can be used.
     */

    // initialize nifty library
    private SimpleApplication app;
    private GameofStones mainApp;
    private Nifty nifty;
	public static int initRoboPanelHeight;
    /**
     * Include the popupShows and the menuScreen as node.
     */
    Node lobbyScreen = new Node("lobbyScreen");
    NiftyJmeDisplay niftyDisplayLobby;

    /**
     * This method initialises the change of the individual AppState
     * and makes the MenuScreen accessible.
     * @param stateManager
     * @param app
     */
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        this.app = (SimpleApplication) app;
		mainApp = GameofStones.getMyself();
        // initialize stuff that is independent of whether state is PAUSED or RUNNING
    }

    /**
     * Here, a previous screen is switched off and the register is reset
     */
    @Override
    public void cleanup() {
    	super.cleanup();
    	app.getGuiViewPort().removeProcessor(niftyDisplayLobby);
    	nifty.exit();
        // unregister all my listeners, detach all my nodes.
    }

  
    @Override
    public void setEnabled(boolean enabled) {
        // pause and unpause
        super.setEnabled(enabled);

        if(enabled){

            // custom key bindings : map input to named actions
            this.app.getInputManager().addMapping("Send",
                    new KeyTrigger(KeyInput.KEY_RETURN),
                    new KeyTrigger(KeyInput.KEY_NUMPADENTER));

            // add the names to the action listener
            this.app.getInputManager().addListener(actionListenerLobby, "Send");

            // initialize stuff that is in use while this state is RUNNING
            niftyDisplayLobby = NiftyJmeDisplay.newNiftyJmeDisplay(
                    app.getAssetManager(),
                    app.getInputManager(),
                    app.getAudioRenderer(),
                    app.getGuiViewPort());
            nifty = niftyDisplayLobby.getNifty();

            // initialize the lobbyScreen
            nifty.fromXml("client/util/lobbyScreen.xml", "startLobby", this);
            
            Screen startScreen = nifty.getCurrentScreen();
    		Element Panel = startScreen.findElementById("color");
			initRoboPanelHeight = Panel.getHeight();


            app.getGuiViewPort().addProcessor(niftyDisplayLobby);
            //app.getInputManager().setCursorVisible(true);

            app.getRootNode().attachChild(this.lobbyScreen);
        } else {

            app.getRootNode().detachChild(this.lobbyScreen);
        }
    }

    /**
     * Nifty GUI Node is integrated with the screen.
     * The first method gives you access to the main Nifty instance and the Screen class, the Java
     * representation of the active screen. Nifty will call this method when it initializes the screen. The
     * method is: bind(Nifty nifty, Screen screen).
     * @param nifty
     * @param screen
     */
    @Override
    public void bind(Nifty nifty, Screen screen) {

    }
    
    
    @Override
	// Note that update is only called while the state is both attached and
	// enabled.
	public void update(float tpf) {
    	
    	
    	if(LobbyScreenController.changedHouse == false) {
    	LobbyScreenController.update();
    	}
    	
    	
		// do the following while game is RUNNING
		if (StartScreen.updatePanelPlayer) {

			// calculate new height of color choosing robot panel
			StartScreen.currentPanelHeight += 6 * tpf * initRoboPanelHeight;
			if (StartScreen.currentPanelHeight > initRoboPanelHeight)
				StartScreen.currentPanelHeight = initRoboPanelHeight;

			// update height of color choosing robot panel
			Element Panel = nifty.getCurrentScreen().findElementById("panel_color");
			Element Space = nifty.getCurrentScreen().findElementById("panel_color_empty");
			Panel.setConstraintHeight(new SizeValue(StartScreen.currentPanelHeight + "px"));
			Space.setConstraintHeight(new SizeValue(initRoboPanelHeight - StartScreen.currentPanelHeight + "px"));
			Panel.getParent().layoutElements();
			Space.getParent().layoutElements();
			Panel.show();

			// clean up when panel is restored to initial size
			if (StartScreen.currentPanelHeight == initRoboPanelHeight)
				StartScreen.updatePanelPlayer = false;

		} 
	}

 
    @Override
    public void onStartScreen() {
        //nothing
    }


    @Override
    public void onEndScreen() {
        //nothing
    }


    private ActionListener actionListenerLobby = new ActionListener() {
        public void onAction(String name, boolean keyPressed, float tpf) {

            if (keyPressed && name.equals("Send")) {
                nifty.getCurrentScreen().findElementById("btnSend").onClickAndReleasePrimaryMouseButton();
            }
        }
    };
}
