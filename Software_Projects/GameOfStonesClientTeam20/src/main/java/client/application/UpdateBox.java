package client.application;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.math.ColorRGBA;
import com.jme3.niftygui.NiftyJmeDisplay;
import com.jme3.scene.Node;
import com.jme3.ui.Picture;

import JsonsParser.Territory;
import JsonsParser.Unit;
import client.controller.PhaseController;
import client.model.Recruitment;
import de.lessvoid.nifty.Nifty;

public class UpdateBox extends AbstractAppState {
	
	public int id;

	public static List<Territory> terr;
	public static HashMap<String, HashMap<Integer, Recruitment>> houseToRecruitment = new HashMap<>();
	static String terrain2;
	
	private SimpleApplication app;
	private GameofStones mainApp;
	private Nifty nifty;
	


	public Node rankingField = new Node("updateBox");

	private BitmapFont guiFont;

	// (1) definere Variable
	public BitmapText name, terrain, castle, tavern, income, house, units, allegiance, existingSwords, existingArchers,
			existingKnights;


	private int rankingFieldWidth;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void initialize(AppStateManager stateManager, Application app) {
		super.initialize(stateManager, app);
		this.app = (SimpleApplication) app;
		mainApp = GameofStones.getMyself();
		// initialize stuff that is independent of whether state is PAUSED or RUNNING

		NiftyJmeDisplay niftyDisplay = NiftyJmeDisplay.newNiftyJmeDisplay(app.getAssetManager(), app.getInputManager(),
				app.getAudioRenderer(), app.getGuiViewPort());
		nifty = niftyDisplay.getNifty();
		app.getGuiViewPort().addProcessor(niftyDisplay);

		guiFont = mainApp.getAssetManager().loadFont("Interface/Fonts/Default.fnt");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void cleanup() {
		super.cleanup();
		// unregister all my listeners, detach all my nodes, etc....
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setEnabled(boolean enabled) {
		// pause and unpause
		// super.setEnabled(enabled);

		if (enabled) {

			// first time updating player data
			updatePlayers();

			rankPlayers();

			rankingFieldWidth = 750;
			float scaleRate = ((float) (GameofStones.SCREEN_WIDTH) / 2250);
			int bottomHeight = (int) (rankingFieldWidth * 0.026);
			int topHeight = (int) (rankingFieldWidth * 0.026);
			int middleHeight = (int) (rankingFieldWidth * 0.5);
			// set background of top box
			Picture pTop = new Picture("rankFieldTop");
			pTop.setImage(app.getAssetManager(), "Ranking-top.png", true);
			pTop.setHeight(topHeight);
			pTop.setWidth(rankingFieldWidth);
			pTop.move(0, bottomHeight + (12 + 1) * middleHeight, 0);
			rankingField.attachChild(pTop);

			// set background of bottom box
			Picture pBottom = new Picture("rankFieldBottom");
			pBottom.setImage(app.getAssetManager(), "Ranking-bottom.png", true);
			pBottom.setHeight(bottomHeight);
			pBottom.setWidth(rankingFieldWidth);
			rankingField.attachChild(pBottom);

			for (int i = 0; i <= 12; i++) {

				// set background of main part
				Node nMiddle = new Node("rankNode" + i);
				nMiddle.move(0, (12 - i) * middleHeight, 0);
				rankingField.attachChild(nMiddle);

				Picture pMiddle = new Picture("rankFieldMiddle" + i);
				pMiddle.setImage(app.getAssetManager(), "Ranking-middle.png", true);
				pMiddle.setHeight(middleHeight);
				pMiddle.setWidth(rankingFieldWidth);
				pMiddle.move(0, bottomHeight, 0);
				nMiddle.attachChild(pMiddle);

				// create text elements and attach them to nodes
				// (2) declare varabes
				name = new BitmapText(guiFont);
				terrain = new BitmapText(guiFont);
				castle = new BitmapText(guiFont);
				tavern = new BitmapText(guiFont);
				income = new BitmapText(guiFont);
				house = new BitmapText(guiFont);
				units = new BitmapText(guiFont);
				allegiance = new BitmapText(guiFont);
				existingSwords = new BitmapText(guiFont);
				existingArchers = new BitmapText(guiFont);
				existingKnights = new BitmapText(guiFont);

				name.setSize(2 * guiFont.getCharSet().getRenderedSize());
				terrain.setSize(2 * guiFont.getCharSet().getRenderedSize());
				castle.setSize(2 * guiFont.getCharSet().getRenderedSize());
				tavern.setSize(2 * guiFont.getCharSet().getRenderedSize());
				income.setSize(2 * guiFont.getCharSet().getRenderedSize());
				house.setSize(2 * guiFont.getCharSet().getRenderedSize());
				units.setSize(2 * guiFont.getCharSet().getRenderedSize());
				allegiance.setSize(2 * guiFont.getCharSet().getRenderedSize());
				existingSwords.setSize(2 * guiFont.getCharSet().getRenderedSize());
				existingArchers.setSize(2 * guiFont.getCharSet().getRenderedSize());
				existingKnights.setSize(2 * guiFont.getCharSet().getRenderedSize());

				// write in black instead of white

				name.setColor(ColorRGBA.Black);
				terrain.setColor(ColorRGBA.Black);
				castle.setColor(ColorRGBA.Black);
				tavern.setColor(ColorRGBA.Black);
				income.setColor(ColorRGBA.Black);
				house.setColor(ColorRGBA.Black);
				units.setColor(ColorRGBA.Black);
				allegiance.setColor(ColorRGBA.Black);
				existingSwords.setColor(ColorRGBA.Black);
				existingArchers.setColor(ColorRGBA.Black);
				existingKnights.setColor(ColorRGBA.Black);

				if (i == 0) {
					// set column titles

					name.setText("id:");
					terrain.setText("TerrainType:");
					castle.setText("castle: ");
					tavern.setText("tavern: ");
					income.setText("income: ");
					house.setText("house: ");
					allegiance.setText("allegiance: ");
					existingSwords.setText("existingSwords: ");
					existingArchers.setText("existingArchers: ");
					existingKnights.setText("existingKnights: ");

					int yMove = (int) (rankingFieldWidth * 0.13);
					int firstX = (int) (rankingFieldWidth * 0.10);

					// name.move(firstX, yMove -100, 0);
					// terrain.move(firstX, yMove -200, 0);

				} else {
					// set data

					int firstX = (int) (rankingFieldWidth * 0.17);
					int secondX = (int) (rankingFieldWidth * 0.27);
					int thirdX = (int) (rankingFieldWidth * 0.64);
					int fourthX = (int) (rankingFieldWidth * .80);

					name.move(secondX, 650, 0);
					terrain.move(secondX, 600, 0);
					castle.move(secondX, 550, 0);
					tavern.move(secondX, 500, 0);
					income.move(secondX, 450, 0);
					house.move(secondX, 400, 0);
					allegiance.move(secondX, 350, 0);
					existingSwords.move(secondX, 300, 0);
					existingArchers.move(secondX, 250, 0);
					existingKnights.move(secondX, 200, 0);

				}

				nMiddle.attachChild(name);
				nMiddle.attachChild(terrain);
				nMiddle.attachChild(castle);
				nMiddle.attachChild(tavern);
				nMiddle.attachChild(income);
				nMiddle.attachChild(house);
				nMiddle.attachChild(allegiance);
				nMiddle.attachChild(existingSwords);
				nMiddle.attachChild(existingArchers);
				nMiddle.attachChild(existingKnights);

			}

			rankingField.scale(scaleRate);
			app.getGuiNode().attachChild(this.rankingField);
		} else {

			// take away everything not needed while this state is PAUSED
			app.getGuiNode().detachChild(this.rankingField);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	// Note that update is only called while the state is both attached and enabled.
	public void update(float tpf) {
		// do the following while game is RUNNING
		updatePlayers();
		rankPlayers();
		updateRanking();
	}

	private void updatePlayers() {

	}

	private void rankPlayers() {

		// initialize

	}

	private void updateRanking() {

		name.setText("id: " + id);

		if (!(terr == null)) {

			terrain2 = terr.get(id).getTerrain();

			boolean cas2 = terr.get(id).getCastle();
			boolean tav2 = terr.get(id).isTavern();
			int in2 = terr.get(id).getIncome();
			String house2 = null;
			if (terr.get(id).getHouse() != null && !terr.get(id).getHouse().equals("")) {
				house2 = terr.get(id).getHouse();
				house.setText("house: " + house2);
			} else {
				house.setText("house:");
			}
			allegiance.setText("allegiance: " );
			//TODO: Thats not working even if its static #forever 0
			existingSwords.setText("Swords" + 0);
			existingArchers.setText("Archers: " + 0 );
			existingKnights.setText( "Knights: " + 0);
			if (terr.get(id).getUnits() != null &&  house2 != null) {
				List<Unit> ListOfunits = terr.get(id).getUnits();
				for(Unit unitUnderTest : ListOfunits)
				{
					if(unitUnderTest != null && unitUnderTest.getAllegiance().equals(house2) )
					{
						allegiance.setText("allegiance: " + unitUnderTest.getAllegiance() );
						existingSwords.setText("Swords" + unitUnderTest.getExistingSwords());
						existingArchers.setText("Archers: " + unitUnderTest.getExistingArchers() );
						existingKnights.setText( "Knights: " +unitUnderTest.getExistingKnights());
						
					}
				}

			}

			terrain.setText("terrain: " + terrain2);
			castle.setText("castle: " + cas2);
			tavern.setText("tavern: " + tav2);
			income.setText("income: " + in2);

		}

	}	

	

	// System.out.println(terrotories.getJsonObject(i).getInt("id"));

	/**
	 * This function is setting the selected id to a new value and is triggering the update function, if the id has changed
	 * @param i: is the new selected territory id.
	 */
	public void setId(int i) {
		
		if(id != i)
		{
			id = i;
			updateRanking();
		}
	}

	public static void setTerritories(List<Territory> house2) {

		terr = house2;
		
		for(Territory ter : terr){
			for(Unit units : ter.getUnits()) {
				HashMap<Integer, Recruitment> curRecFromAlligence = houseToRecruitment.get(units.getAllegiance());
				if(curRecFromAlligence == null) {
					HashMap<Integer , Recruitment> newRecForAlligence = new HashMap<>();
					newRecForAlligence.put(ter.getId(), 
							new Recruitment(units.getExistingArchers(), units.getExistingKnights(), units.getExistingSwords()));
					houseToRecruitment.put(units.getAllegiance(), newRecForAlligence);
				}else {
					Recruitment recruitment = curRecFromAlligence.get(ter.getId());
					if(recruitment == null) {
						curRecFromAlligence.put(ter.getId(), new Recruitment(units.getExistingArchers(), units.getExistingKnights(), units.getExistingSwords()));
					}else {
						recruitment.combineRecruitment(new Recruitment(units.getExistingArchers(), units.getExistingKnights(), units.getExistingSwords()));
					}
				}
			}
		}

	}
	
	
}
