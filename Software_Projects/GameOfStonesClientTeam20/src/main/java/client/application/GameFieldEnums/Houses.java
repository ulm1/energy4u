package client.application.GameFieldEnums;

import java.io.File;

public enum Houses {
	
	Lannister(new File("./Assets/Lanister.png")),
	Arryns(new File("./Assets/Arryns.png")),
	Graufreud(new File("./Assets/Graufreud.png")),
	Martell(new File("./Assets/Martell.png")),
	Targaryen(new File("./Assets/Targaryen.png")),
	Stark(new File("./Assets/Stark.png")),
	Baratheon(new File("./Assets/Baratheon.png"));
	
	public File flag;
	
	Houses(File picture)
	{
		flag = picture;
	}
}
