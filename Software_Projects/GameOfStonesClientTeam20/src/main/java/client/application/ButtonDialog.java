package client.application;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.niftygui.NiftyJmeDisplay;
import com.jme3.scene.Node;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.controls.TextField;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;
import de.lessvoid.nifty.tools.Color;
import de.lessvoid.nifty.tools.SizeValue;

public class ButtonDialog extends AbstractAppState implements ScreenController {

	private SimpleApplication app;
	private GameofStones mainApp;
	private Nifty nifty;

	Node buttonDialog = new Node("buttonDialog");
	private static boolean popupShows;

	// initial startScreen entries

	NiftyJmeDisplay niftyDisplay;
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void initialize(AppStateManager stateManager, Application app) {
		super.initialize(stateManager, app);
		this.app = (SimpleApplication) app;
		mainApp = GameofStones.getMyself();
		// initialize stuff that is independent of whether state is PAUSED or
		// RUNNING
		popupShows = false;
		
		
		NiftyJmeDisplay niftyDisplay = NiftyJmeDisplay.newNiftyJmeDisplay(
                app.getAssetManager(),
                app.getInputManager(),
                app.getAudioRenderer(),
                app.getGuiViewPort());
        nifty = niftyDisplay.getNifty();
        app.getGuiViewPort().addProcessor(niftyDisplay);
    	nifty.fromXml("client/util/ButtonDialog.xml", "startScreen", this);
		
		
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void cleanup() {
		super.cleanup();
		nifty.exit();
		app.getGuiViewPort().removeProcessor(niftyDisplay);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setEnabled(boolean enabled) {
		// pause and unpause
		super.setEnabled(enabled);

		if (enabled) {
			popupShows = true;
			app.getGuiNode().attachChild(this.buttonDialog);
		} else {
			popupShows = false;

			// take away everything not needed while this state is PAUSED
			app.getGuiNode().detachChild(buttonDialog);

		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override

	public void update(float tpf) {

	
	}

		

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void bind(Nifty nifty, Screen screen) {

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onStartScreen() {
		// nothing
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onEndScreen() {
		// nothing
	}

	public static void popupErrorShows(boolean bool) {
		popupShows = bool;
	}

	public void backToMenu() {

	}

	private ActionListener actionListener = new ActionListener() {
		public void onAction(String name, boolean keyPressed, float tpf) {
			// get currently focused element as well as all buttons
			Element focEl = nifty.getCurrentScreen().getFocusHandler().getKeyboardFocusElement();
			Element button = nifty.getCurrentScreen().findElementById("btnConnect");

			if (keyPressed && name.equals("Connect")) {
				if (popupShows) {
					// if errorPopup is active, click okay button
					nifty.getCurrentScreen().getTopMostPopup().findElementById("button_ok")
							.onClickAndReleasePrimaryMouseButton();
					;
				} 
			
					
			}

		

			if (keyPressed && !popupShows && name.equals("Quit")) {
				// switch to MenuScreen
				backToMenu();
			}
		}
	};
}
