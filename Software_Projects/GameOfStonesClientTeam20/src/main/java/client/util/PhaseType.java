package client.util;

public enum PhaseType {
	
	SUPPLY ,
	
	/** The recruitment phase. */
	RECRUITMENT ,

	/** The movement phase. */
	MOVEMENT ,
	
	/** The battle phase. */
	BATTLE ,
	
	/** The end phase. */
	END, None ;
	
}
