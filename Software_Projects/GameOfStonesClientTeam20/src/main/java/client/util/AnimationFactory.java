package client.util;

import com.jme3.app.Application;
import com.jme3.effect.ParticleEmitter;
import com.jme3.effect.ParticleMesh;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;



public class AnimationFactory {


	public static final String Winter = "CP";

	public static ParticleEmitter getToast(Application app, String format) {
		ParticleEmitter debris = new ParticleEmitter("Debris", ParticleMesh.Type.Triangle, 1);
		Material debris_mat = new Material(app.getAssetManager(), "Common/MatDefs/Misc/Particle.j3md");
		switch (format) {


		case Winter:
			debris_mat.setTexture("Texture", app.getAssetManager().loadTexture("Effects/Explosion/roundspark.png"));
			debris.setStartColor(new ColorRGBA(ColorRGBA.White));
			debris.setEndColor(new ColorRGBA(ColorRGBA.White));
			debris.setStartSize(6);
			debris.setEndSize(1);
			debris.getParticleInfluencer().setInitialVelocity(new Vector3f(0, 0, 2));
			break;	
		default:
			return null;
		}

		debris.setMaterial(debris_mat);
		debris.setImagesX(1);
		debris.setImagesY(1);
	
		debris.setRotateSpeed(0);
		
		debris.getParticleInfluencer().setVelocityVariation(0);
		debris.setLowLife(1.5f);
		debris.setHighLife(1.5f);

		return debris;

	}

}
