package client.controller;

import client.application.MenuScreen;
import client.application.GameofStones;
import com.jme3.app.state.AbstractAppState;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.elements.render.TextRenderer;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;


public class MenuScreenController extends AbstractAppState implements ScreenController {

    private Nifty nifty;
    private Screen screen;
    private GameofStones mainApp;
    private Element popupHelp;
    public boolean mscPopupHelpShows;
    int heigth = GameofStones.SCREEN_HEIGHT;
    int width = GameofStones.SCREEN_HEIGHT;
    
    public static String textMenuScreen, textStartScreen, textLobbyScreen, textMapScreen;

    /**
     * {@inheritDoc}
     */
    @Override
    public void bind(Nifty nifty, Screen screen) {
        this.nifty = nifty;
        this.screen = screen;
        this.mainApp = GameofStones.getMyself();
        // create (but don't yet show) help popup
        createHelpPopup();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onStartScreen() {
        // nothing
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onEndScreen() {
        // nothing
    }

    /**
     * Creates a popup that can later be filled with different <br>
     * error messages. (Note: The popup will not be shown until <br>
     * the showHelpPopup-method is called.)
     *
     * 
     */
    public void createHelpPopup() {
        if (popupHelp == null) {
            popupHelp = this.nifty.createPopup("popupHelp"); // create with id
        }
    }

    /**
     * Shows a previously created error popup filled with <br>
     * an error title and an error message.
     *
     * 
     */
    public void showHelpPopup() {
        MenuScreen.popupHelpShows(true);
        // set texts
        textMenuScreen = "Willkommen in der Hauptmenuehilfe!\n" +
                "Hier koennen Sie das Spiel starten und beenden " +
                "(beenden ist im gesamten Verlauf " +
                "ueber die Escapetaste moeglich, " +
                "dies gilt nicht für die anderen Tasten.). " +
                "Die Schnelltasten im Hauptmenue " +
                "sind: h für Hilfe, s für Start " +
                "oder Tabulator für das Navigieren " +
                "in den einzelnen Funktionen, welche S" +
                "ie mit Enter bestaetigen koennen. " +
                "Natuerlich lassen sich auch " +
                "saemtliche Button und Textfelder" +
                " mit der linken Maustaste bedienen.\n";
        textStartScreen = "Das Startmenue: " +
                "welches Sie aus dem Hauptmenue " +
                "erreichen bietet Ihnen die Möglichkeit," +
                " einen Namen zu Setzen und zwischen " +
                "den Rollen, Spieler oder Beobachter " +
                "zu wechseln. Hier koennen Sie mit " +
                "Tabulator durch die einzelnen " +
                "Textfelder oder Button navigieren " +
                "und mit Enter bestaetigen. " +
                "Eine Verbindung zu dem Server " +
                "erstellen Sie, wenn Sie als IP: " +
                "localhost und als Port 55813 " +
                "eintragen. Der finale Verbindungsvorgang " +
                "startet, wenn Sie den Button " +
                "verbinden betaetigen. ";
        textLobbyScreen = "Das Lobbymenue: " +
                "hier koennen Sie alle Teilnehmer " +
                "sehen und mit Ihnen kommunizieren." +
                " Das Spiel wird von hier Automatisch " +
                "vom Server gestartet und erlaubt ueber" +
                " die Nachrichtenfunktionen hinaus " +
                "keine weitere Eingabe. Nachrichten " +
                "koennen Sie in dem Textfeld eingeben " +
                "und mit Enter oder linke Maustaste " +
                "klick auf Senden, verschicken.  ";
        textMapScreen = "Die Spielkarte: " +
                "hier ist das Spielfeld und " +
                "die Roboter dargestellt, " +
                "zusaetzlich eine Rankingliste " +
                "der einzelnen Teilnehmer. Moeg" +
                "liche Interaktionen sind: mit dem " +
                "Mausrad koennen Sie das Feld " +
                "vergroeßern oder verkleinern. " +
                "Mit linker Maustaste gedrueckt " +
                "halten, koennen Sie das Feld ver" +
                "schieben. ";

        // write texts
        popupHelp.findElementById("help_1").getRenderer(TextRenderer.class).setText(textMenuScreen);
        popupHelp.findElementById("help_2").getRenderer(TextRenderer.class).setText(textStartScreen);
        popupHelp.findElementById("help_3").getRenderer(TextRenderer.class).setText(textLobbyScreen);
        popupHelp.findElementById("help_4").getRenderer(TextRenderer.class).setText(textMapScreen);

        Element btnBack = popupHelp.findElementById("button_back");
        nifty.showPopup(nifty.getCurrentScreen(), popupHelp.getId(), btnBack);
    }

    /**
     * Hides the error popup (by closing it) until it is shown again.
     *
     * 
     */
    public void hideHelpPopup() {
        MenuScreen.popupHelpShows(false);
        nifty.closePopup(popupHelp.getId());
    }

    /**
     * Method called by StartScreen-button links to hideErrorPopup-method.
     *
     * 
     */
    public void popupExit() {
        hideHelpPopup();
    }

    /**
     * 
     */
    public void startStartScreen() {
        mainApp.switchAppState(MenuScreen.myself, GameofStones.startScreen);
    }

    /**
     *
     */
    public void getHelp() {
        showHelpPopup();
    }

    /**
     * 
     */
    public void quitMenu() {
        System.exit(0);
    }

    public String getFontName() {
        int screenHeight = mainApp.getCamera().getHeight();
        System.out.println("font. screenHeight=" + screenHeight);
        String fontname = "console.fnt";
        if (screenHeight < 481) {
            fontname = "console.fnt";
        } else if (screenHeight < 601) {
        	fontname = "aurulent-sans-16.fnt";
        } else if (screenHeight < 1025) {
            fontname = "verdana-small-regular.fnt";
        } else if (screenHeight < 1394){
            fontname = "verdana-48-regular.fnt";
        }else if (screenHeight < 1401){
        fontname = "menu.fnt";}
        return fontname;
    }
}
