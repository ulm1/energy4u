package client.controller;

import client.application.GameofStones;
import client.application.NetworkThread;
import client.application.StartScreen;
import com.jme3.app.state.AbstractAppState;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.NiftyEventSubscriber;
import de.lessvoid.nifty.controls.RadioButton;
import de.lessvoid.nifty.controls.RadioButtonGroupStateChangedEvent;
import de.lessvoid.nifty.controls.TextField;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.elements.render.ImageRenderer;
import de.lessvoid.nifty.elements.render.TextRenderer;
import de.lessvoid.nifty.render.NiftyImage;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;
import de.lessvoid.nifty.tools.Color;

import java.net.Socket;
import java.util.IllegalFormatException;

public class StartScreenController extends AbstractAppState implements ScreenController {

	private Nifty nifty;
	private Screen screen;
	private GameofStones mainApp;
	private Element popupError;

	String Username = "";
	boolean isPlayer = false;
	boolean isWatcher = false;
	String mainColor = StartScreen.initMainColor;
	Color mainColorCode = StartScreen.initMainColorCode;
	String sideColor = StartScreen.initSideColor;
	Color sideColorCode = StartScreen.initSideColorCode;
	String ipAddress = "";
	String portNumber = "";
	String house = "";

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void bind(Nifty nifty, Screen screen) {
		this.nifty = nifty;
		this.screen = screen;
		this.mainApp = GameofStones.getMyself();
		// create (but don't yet show) error popup
		createErrorPopup();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onStartScreen() {
		// nothing
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onEndScreen() {
		// nothing
	}

	/**
	 * Creates a popup that can later be filled with different <br>
	 * error messages. (Note: The popup will not be shown until <br>
	 * the showErrorPopup-method is called.)
	 * 
	 * 
	 */
	public void createErrorPopup() {
		if (popupError == null) {
			popupError = this.nifty.createPopup("popupError"); // create with id
		}
	}

	/**
	 * Shows a previously created error popup filled with <br>
	 * an error title and an error message.
	 * 
	 * @param title
	 *            String that contains the error title.
	 * @param message
	 *            String containing the error message.
	 * 
	 *
	 */
	public void showErrorPopup(String title, String message) {
		StartScreen.popupErrorShows(true);

		TextRenderer errorTitle = popupError.findElementById("errorTitle").getRenderer(TextRenderer.class);
		TextRenderer errorMessage = popupError.findElementById("errorMessage").getRenderer(TextRenderer.class);

		errorTitle.setText(title);
		errorMessage.setText(message);

		nifty.showPopup(nifty.getCurrentScreen(), popupError.getId(), null);
	}

	/**
	 * Hides the error popup (by closing it) until it is shown again.
	 * 
	 * 
	 */
	public void hideErrorPopup() {
		StartScreen.popupErrorShows(false);
		nifty.closePopup(popupError.getId());
	}

	/**
	 * Method called by StartScreen-button links to hideErrorPopup-method.
	 * 
	 * 
	 */
	public void popupExit() {
		hideErrorPopup();
	}

	/**
	 * Check the validity of the user's startScreen input data <br>
	 * and throws, when wanted, an error message of invalid data.
	 * 
	 * @param command
	 *            A simple String command. <br>
	 *            If "throwErrorMessage" : The method will throw an error
	 *            message. <br>
	 *            If anything else : The method will not throw any error
	 *            messages.
	 * @return Boolean that is true, if the user's input data is valid.
	 * 
	 */
	private boolean checkInput(String command) {
		// get necessary screen elements
		TextField tfUsername = screen.findNiftyControl("tfUsername", TextField.class);
		RadioButton rbPlayer = screen.findNiftyControl("rbPlayer", RadioButton.class);
		RadioButton rbWatcher = screen.findNiftyControl("rbWatcher", RadioButton.class);
		TextField tfIpAddress = screen.findNiftyControl("tfIpAddress", TextField.class);
		TextField tfPortNumber = screen.findNiftyControl("tfPortNumber", TextField.class);

		// get input data from screen elements
		Username = tfUsername.getRealText();
		if (rbPlayer.isActivated()) {
			isPlayer = true;
		} else {
			isPlayer = false;
		}
		if (rbWatcher.isActivated()) {
			isWatcher = true;
		} else {
			isWatcher = false;
		}
		ipAddress = tfIpAddress.getRealText();
		portNumber = tfPortNumber.getRealText();

		// reset empty input data
		if (tfUsername.getRealText().trim().equals("")) {
			tfUsername.setText(StartScreen.initUsername);
		}
		if (tfIpAddress.getRealText().trim().equals("")) {
			tfIpAddress.setText(StartScreen.initIpAddress);
		}
		if (tfPortNumber.getRealText().trim().equals("")) {
			tfPortNumber.setText(StartScreen.initPortNumber);
		}

		// check user's inputs for validity and throw error-message if anything
		// is invalid
		if (command.equals("throwErrorMessage")) {

			// check validity of user-name
			if (Username.trim().equals("")) {
				showErrorPopup("Ung\u00fcltiger Username", "Leider wird der von Ihnen angegebene Username ( " + Username
						+ " ) von dieser Anwendung nicht unterst\u00fctzt.");
				return false;
			}

			// check validity of participant-type
			if ((isPlayer == false && isWatcher == false) || (isPlayer && isWatcher)) {
				showErrorPopup("Ung\u00fcltige Spieler-Beobachter-Auswahl",
						"Entschuldigung, dieser Fehler h\u00e4tte nicht passieren d\u00fcrfen."
								+ " Bitte stellen Sie sicher, dass Sie genau eine der Optionen \"Spieler\""
								+ " und \"Beobachter\" ausgew\u00e4hlt haben.");
				return false;
			}

			// check validity of IP address
			boolean IPValid = checkIPAdressValidity(ipAddress);
			if (!IPValid) {
				showErrorPopup("Ung\u00fcltige IP-Addresse",
						"Leider entspricht die von Ihnen angegebene IP-Adresse keinem g\u00fcltigen"
								+ " und von dieser Anwendung unterst\u00fctzten Format.");
				return false;
			}

			// check validity of port number
			try {
				Integer.parseInt(portNumber);
			} catch (NumberFormatException e) {
				showErrorPopup("Ung\u00fcltige Portnummer",
						"Leider entspricht die von Ihnen angegebene Portnummer keinem g\u00fcltigen"
								+ " und von dieser Anwendung unterst\u00fctzten Format.");
				return false;
			}

			// check if server is available
			boolean connectionError = false;
			try {
				int temporaryPortNum = Integer.parseInt(portNumber);
				System.out.println(temporaryPortNum);
				Socket connection = new Socket(ipAddress, temporaryPortNum);

				if (connection == null || !connection.isConnected()) {
					connectionError = true;
				}
				connection.close();

			} catch (Exception e) {
				connectionError = true;
			}

			if (connectionError) {
				showErrorPopup("Verbindungsfehler",
						"Eine Verbindung mit dieser IP-Addresse und der angegebenen Portnummer"
								+ " ist nicht m\u00f6glich.");
				return false;
			}
		}
		return true;
	}

	/**
	 * Check if a given String is a valid IPv4 address.
	 * 
	 * @param ipAddress2
	 *            The String to be checked.
	 * @return Boolean that is false, if the format requirements are not met.
	 *
	 */
	private boolean checkIPAdressValidity(String ipAddress2) {

		/**
		 * Check if the connection is local.
		 */
		if (ipAddress2.equals("localhost")) {
			return true;
		}

		/**
		 * Check if the IPv4 format is given.
		 */
		try {
			String[] splitArr = ipAddress2.split("\\.");

			if (splitArr.length != 4) {
				return false;
			}
			for (int i = 0; i < splitArr.length; i++) {
				try {
					int tempInt = Integer.parseInt(splitArr[i]);
					if (tempInt > 255 || tempInt < 0) {
						return false;
					}
				} catch (NumberFormatException e) {
					return false;
				}
			}
		} catch (IllegalFormatException e) {
			return false;
		}

		return true;
	}

	/**
	 * Packs all of the user's startScreen input data into one array of Strings.
	 * 
	 * @return StringArray with following entries: <br>
	 *         index 0 : username <br>
	 *         index 1 : is either "player" or "watcher" <br>
	 *         index 2 : mainColor in words (e.g. "red") <br>
	 *         index 3 : mainColorCode (e.g. "#FF0000") <br>
	 *         index 4 : sideColor in words (e.g. "red") <br>
	 *         index 5 : sideColorCode (e.g. "#FF0000") <br>
	 *         index 6 : ipAddress <br>
	 *         index 7 : portNumber
	 *
	 */
	private String[] packInputData() {
		String[] input = new String[8];

		input[0] = Username;
		if (isPlayer) {
			input[1] = "player";
		} else {
			input[1] = "watcher";
		}
		input[2] = mainColor;
		input[3] = house;
		input[4] = ipAddress;
		input[5] = portNumber;

		return input;
	}

	public void headerClick() {
		checkInput("");
	}

	public void tfUsernameClick() {
		checkInput("");
		// clear textField if input is initial input
		TextField tfUsername = screen.findNiftyControl("tfUsername", TextField.class);
		if (tfUsername.getRealText().equals(StartScreen.initUsername)) {
			tfUsername.setText("");
		}
	}

	public void rbPlayerClick() {
		checkInput("");
		// select radioButton if only matching label was clicked
		RadioButton rbPlayer = screen.findNiftyControl("rbPlayer", RadioButton.class);
		if (rbPlayer.isActivated() == false) {
			rbPlayer.select(); // triggers RadioButtonGroupStateChangedEvent
		} else {
			// activate color choosing
			int height = 0;
			StartScreen.currentPanelHeight = height;
			StartScreen.updatePanelPlayer = true;
		}
	}

	/**
	 * 
	 * @param id
	 *            Final string that contains the id that has published the
	 *            event.
	 * @param event
	 *            Final radioButtonGroupStateChangedEvent triggered by newly
	 *            selected radioButton.
	 * 
	 */
	@NiftyEventSubscriber(id = "participantGroup")
	public void onRadioGroupChanged(final String id, final RadioButtonGroupStateChangedEvent event) {
		if (event.getSelectedId().equals("rbPlayer")) {
			rbPlayerClick();
		} else {
			rbWatcherClick();
		}
	}

	
	public void rbWatcherClick() {
		
		// select radioButton if only matching label was clicked
		RadioButton rbWatcher = screen.findNiftyControl("rbWatcher", RadioButton.class);
		if (rbWatcher.isActivated() == false) {
			rbWatcher.select(); // triggers RadioButtonGroupStateChangedEvent
		} else {
			// deactivate color choosing
			int height = screen.findElementById("panel_robocolor").getHeight();
			StartScreen.currentPanelHeight = height;
			StartScreen.updatePanelWatcher = true;
		}
	}

	public void tfIpAddressClick() {
		checkInput("");
		// clear textField if input is initial input
		TextField tfIpAddress = screen.findNiftyControl("tfIpAddress", TextField.class);
		if (tfIpAddress.getRealText().equals(StartScreen.initIpAddress)) {
			tfIpAddress.setText("");
		}
	}

	public void tfPortNumberClick() {
		checkInput("");
		// clear textField if input is initial input
		TextField tfPortNumber = screen.findNiftyControl("tfPortNumber", TextField.class);
		if (tfPortNumber.getRealText().equals(StartScreen.initPortNumber)) {
			tfPortNumber.setText("");
		}
	}

	public void btnConnectClick() {
		
		mainApp.startConnection(packInputData(),"start"); 

	}
}
