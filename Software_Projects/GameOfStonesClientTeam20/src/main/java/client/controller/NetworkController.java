package client.controller;

import java.awt.Dimension;
import java.awt.geom.Dimension2D;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonBuilderFactory;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import client.application.NetworkThread;
import client.model.Participant;
import client.model.Player;


public class NetworkController {

	public static final String PROTOCOL_VERSION = "1.0.0";

	public static JsonBuilderFactory factory = Json.createBuilderFactory(null);

	

	static String uid2;
	
	
	public static String getUid2() {
		return uid2;
	}

	public static void setUid2(String uid2) {
		NetworkController.uid2 = uid2;
	}

	public static String getName2() {
		return name2;
	}

	public static void setName2(String name2) {
		NetworkController.name2 = name2;
	}

	static String name2;

	public static JsonObject encodeLoginRequest(String name,boolean player, boolean role) {
		
		
		

		long currentTime = System.currentTimeMillis();

		JsonObject jsonOb = factory.createObjectBuilder()
				.add("header",
						factory.createObjectBuilder().add("UUID", "null").add("name", name).add("timestamp", currentTime).add("messageType", "login"))
		
				.add("body", factory.createObjectBuilder().add("isPlayer", player).add("isHuman", role)).build();
		System.out.println(jsonOb);
		return jsonOb;
		
	}
	
	
	
	public static JsonObject ready(String name, boolean ready,String uid) {
		
		
		

		long currentTime = System.currentTimeMillis();

		JsonObject jsonOb = factory.createObjectBuilder()
				.add("header",
						factory.createObjectBuilder().add("UUID",uid).add("name", name).add("timestamp", currentTime).add("messageType", "ready"))
		
				.add("body", factory.createObjectBuilder().add("isReady", ready)).build();

		return jsonOb;
		
	}
	
	
	public static JsonObject ping(String name, boolean ready,String uid) {
		
		
		setUid2(uid);
		setName2(name);
		
	

		long currentTime = System.currentTimeMillis();

		JsonObject jsonOb = factory.createObjectBuilder()
				.add("header",
						factory.createObjectBuilder().add("UUID",uid).add("name", name).add("timestamp", currentTime).add("messageType", "ping"))
		
				.add("body", factory.createObjectBuilder()).build();
		System.out.println(jsonOb);
		return jsonOb;
		
	}
	
	
	
	
	public static JsonObject pauseRequest(String name,String uid) {
		
		
		

		long currentTime = System.currentTimeMillis();

		JsonObject jsonOb = factory.createObjectBuilder()
				.add("header",
						factory.createObjectBuilder().add("UUID",uid).add("name", name).add("timestamp", currentTime).add("messageType", "pauseRequest"))
		
				.add("body", factory.createObjectBuilder()).build();
		System.out.println(jsonOb);
		
		NetworkThread.setMyMessage(jsonOb.toString());
		
		
		return jsonOb;
		
	}


	public static void newHouse(String house,String currentHouse, NetworkThread network) {
		long currentTime = System.currentTimeMillis();

		JsonObject jsonOb = factory.createObjectBuilder()
				.add("header",
						factory.createObjectBuilder().add("UUID",uid2).add("name", name2).add("timestamp", currentTime).add("messageType", "houseRequest"))
		
				.add("body",factory.createObjectBuilder().add("fromHouse", currentHouse).add("toHouse",house)).build();
		System.out.println(jsonOb);
		
		network.onWebsocketMessage(network.getConnection(), jsonOb.toString());
		
		
	}

	public static void setLogout(NetworkThread network) {
		long currentTime = System.currentTimeMillis();

		JsonObject jsonOb = factory.createObjectBuilder()
				.add("header",
						factory.createObjectBuilder().add("UUID",NetworkThread.uid).add("name", NetworkThread.name).add("timestamp", currentTime).add("messageType", "logout"))
		
				.add("body", factory.createObjectBuilder()).build();
		network.onWebsocketMessage(network.getConnection(), jsonOb.toString());
		
		
	}

}