package client.controller;

import client.application.NetworkThread;
import client.application.StartScreen;
import client.application.GameofStones;
import client.model.Participant;
import com.jme3.app.state.AbstractAppState;

import JsonsParser.Territory;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.controls.Label;
import de.lessvoid.nifty.controls.RadioButton;
import de.lessvoid.nifty.controls.TextField;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.elements.render.ImageRenderer;
import de.lessvoid.nifty.render.NiftyImage;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;
import de.lessvoid.nifty.tools.Color;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import javax.json.JsonObject;

/**
 *
 * 
 */
public class LobbyScreenController extends AbstractAppState implements ScreenController {

	private static Nifty nifty;
	private static Screen screen;
	private GameofStones mainApp;
	public static String myUsername;
	static Label textLabel;
	static Label playerLabel;
	static ArrayList<Participant> currentLobby = new ArrayList<Participant>();
	static final int ROW_COUNT = (int) Math.round((GameofStones.SCREEN_HEIGHT * 0.9 * 0.9) / 16) - 1;

	String Username = "";
	boolean isPlayer = false;
	boolean isWatcher = false;
	String mainColor = StartScreen.initMainColor;
	Color mainColorCode = StartScreen.initMainColorCode;
	String sideColor = StartScreen.initSideColor;
	Color sideColorCode = StartScreen.initSideColorCode;
	String ipAddress = "";
	String portNumber = "";
	static String house = "";
	static String myHouse;
	public static boolean changedHouse = false;

	@Override
	public void bind(Nifty nifty, Screen screen) {
		this.nifty = nifty;
		this.screen = screen;
		this.mainApp = GameofStones.getMyself();
	}

	@Override
	public void onStartScreen() {
		// nothing
	}

	@Override
	public void onEndScreen() {
		// nothing
	}

	public void rbWatcherClick() {

		// select radioButton if only matching label was clicked
		RadioButton rbWatcher = screen.findNiftyControl("rbWatcher", RadioButton.class);
		if (rbWatcher.isActivated() == false) {
			rbWatcher.select(); // triggers RadioButtonGroupStateChangedEvent
		} else {
			// deactivate color choosing
			int height = screen.findElementById("panel_robocolor").getHeight();
			StartScreen.currentPanelHeight = height;
			StartScreen.updatePanelWatcher = true;
		}
	}

	public static boolean isChangedHouse() {
		return changedHouse;
	}

	public static void setChangedHouse(boolean changedHouse) {
		LobbyScreenController.changedHouse = changedHouse;
	}

	public void mainColorClicked(String color, String colorCode, String name) {

		mainColor = color;
		house = name;
		mainColorCode = new Color(colorCode);
		Houseinfo("main", "Middle", house);
	}

	private void Houseinfo(String colorType, String layer, String name) {
		String imgPath = name + ".png";

		Element oldImg = screen.findElementById("GroundImg");

		NiftyImage newImg = nifty.getRenderEngine().createImage(screen, imgPath, false);

		oldImg.getRenderer(ImageRenderer.class).setImage(newImg);
	}

	public void sideColorClicked(String color, String colorCode, String name) {

		sideColor = color;
		sideColorCode = new Color(colorCode);
		Houseinfo("side", "Ground", name);
	}

	static NetworkThread network;

	public void sendText() {

		network.setReady(true);

	}

	public void logout() {

		NetworkController.setLogout(network);

	}

	public void choose() {

		switch (house) {
		case "Lanister":
			house = "House Lannister";
			break;
		case "Targaryen":
			house = "House Targaryen";
			break;
		case "Stark":
			house = "House Stark";
			break;
		case "Baratheon":
			house = "House Baratheon";
			break;
		case "Graufreud":
			house = "House Greyjoy";
			break;
		case "Arryns":
			house = "House Arryn";
			break;
		case "Martell":
			house = "House Martell";
			break;
		default:
			System.out.println("Invalid month.");
			break;
		}
		System.out.println(house);

		if (!house.equals(myHouse) && house.isEmpty() == false) {
			NetworkController.newHouse(house, myHouse, network);
		}

	}

	public static void setHouse(String house2) {
		myHouse = house2;

	}

	static List<Territory> h;
	static boolean start = false;

	public static void update() {

		TextField textFieldLobby = screen.findNiftyControl("textFieldLobby", TextField.class);

		if (textFieldLobby != null) {
			textFieldLobby.setText(myHouse);

			textFieldLobby.disable();
		}
		changedHouse = true;

	}

	public static void setNetwork(NetworkThread networkThread) {
		network = networkThread;

	}

	public static void setTerr(List<Territory> house2) {
		h = house2;

	}

}
