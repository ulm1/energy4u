import 'package:digitaleye/Localization/app_translations_delegate.dart';
import 'package:digitaleye/Localization/application.dart';
import 'package:digitaleye/Screens/IndicatorList/IndicatorList.dart';
import 'package:digitaleye/Screens/TabBarScreens/DashBoard/Dashboard.dart';
import 'package:digitaleye/services/database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:digitaleye/Helper/CommonWidgets/CommonWidgets.dart';
import 'package:digitaleye/Helper/Constant.dart';
import 'package:digitaleye/Helper/SharedManager.dart';
import 'package:digitaleye/Localization/app_translations.dart';
import 'package:syncfusion_flutter_charts/charts.dart';


void main()=>runApp(new TestIndicators2());


class TestIndicators2 extends StatefulWidget {
  @override
  final List<Event> events;

  const TestIndicators2( {Key key, this.events}) : super(key: key);


  _TestIndicatorsState2 createState() => _TestIndicatorsState2();
}


String days;
double values;
int i = 0;



class _TestIndicatorsState2 extends State<TestIndicators2> {
  bool isLoading = true;










    _setChardIndicatorView()  {
      TimeOfDay roomBooked2 = TimeOfDay.fromDateTime(DateTime.now().add(Duration(hours:20,minutes: 0))); // 4:30pm

      print(widget.events);



    return new Container(
      height: 200,
      child:SfCartesianChart(
            primaryXAxis: CategoryAxis(),
            // Enable legend
            legend: Legend(isVisible: false),
            // Enable tooltip
            tooltipBehavior: TooltipBehavior(enable: false),
            series: <LineSeries<SalesData, String>>[
              LineSeries<SalesData, String>(
                dataSource:  <SalesData>[
                  SalesData('Mon', 1),
                  SalesData('Tue', 1),
                  SalesData('Wed', 1),
                  SalesData('Thu', 1),
                  SalesData('Fri', 1),
                  SalesData('Sat', 1),
                  SalesData('Sun', 1),
                ],
                xValueMapper: (SalesData sales, _) => sales.year,
                yValueMapper: (SalesData sales, _) => sales.sales,
                // Enable data label
                dataLabelSettings: DataLabelSettings(isVisible: true)
              )
            ]
          ),
    );
  }

_setTestIndicatorView( ){
  return new GridView.count(

    crossAxisCount: 1,
    children: List<Widget>.generate(19,(index){

      return new Container(
        height: 350,
        padding: new EdgeInsets.all(15),
        child: new Material(
          elevation: 2.0,
          color: Colors.white,
          borderRadius: new BorderRadius.circular(8),
          child: new Column(
            children: <Widget>[
              new Expanded(
                child: new Container(
                  child: Padding(
                    padding: new EdgeInsets.all(8),
                    child: new Row(
                      children: <Widget>[
                        new Icon(Icons.pin_drop,color: AppColor.themeColor,size: 20,),
                        SizedBox(width: 3,),
                        setCommonText("hello", AppColor.themeColor, 16.0, FontWeight.w600,1)
                      ],
                    ),
                  ),
                ),
              ),
              new Expanded(
                flex: 5,
                child: new Container(
                  child:isLoading
                      ? Container(
                    child: Center(child: CircularProgressIndicator()),
                  )
                      : _setChardIndicatorView(),
                ),
              ),
              Divider(color: Colors.grey,),
              new Expanded(
                child: new Container(
                  child: new Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      new InkWell(
                        onTap: (){
                          Navigator.of(context,rootNavigator: false).push(MaterialPageRoute(builder: (context)=>IndicatorList()));
                        },
                         child: new Row(
                           children: <Widget>[
                             new Icon(Icons.blur_circular,color:AppColor.themeColor,size: 20,),
                             SizedBox(width: 3,),
                             setCommonText("Symptoms", AppColor.themeColor, 16.0, FontWeight.w500, 1),
                           ],
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      );
    }),
  );
}

AppTranslationsDelegate _newLocaleDelegate;
@override
  void initState() {
  isLoading = false;

  super.initState();
    SharedManager.shared.isOnboarding = true;
    _newLocaleDelegate = AppTranslationsDelegate(newLocale:null);
      application.onLocaleChanged = onLocaleChange;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner:false,
      home: Scaffold(
        body: _setTestIndicatorView(),
         appBar: new AppBar(
          centerTitle: true,
          // title: setHeaderTitle(AppTranslations.of(context).text(AppTitle.appTitle),Colors.white),
          title: setHeaderTitle(AppTranslations.of(context).text(AppTitle.drawerIndicators),Colors.white),
          backgroundColor: AppColor.themeColor,
          elevation: 1.0,
          actions: setCommonCartNitificationView(context),
        ),
        drawer: SharedManager.shared.setDrawer(context, PersonalInfo.name,PersonalInfo.email),
    ),
    routes: {
        '/TestIndicator': (BuildContext context) => TestIndicators2()
      },
      localizationsDelegates: [
          _newLocaleDelegate,
          //provides localised strings
          GlobalMaterialLocalizations.delegate,
          //provides RTL support
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: [
          SharedManager.shared.language
        ],
    );
  }
  void onLocaleChange(Locale locale) {
      setState(() {
        _newLocaleDelegate = AppTranslationsDelegate(newLocale: locale);
      });
    }
}

class SalesData {
  SalesData(this.year, this.sales);
  final String year;
  final double sales;
}