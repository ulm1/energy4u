import 'package:digitaleye/Localization/app_translations_delegate.dart';
import 'package:digitaleye/Localization/application.dart';
import 'package:digitaleye/Screens/TestIndicatorDetails/TestIndicatorDetails.dart';
import 'package:digitaleye/Screens/contanst/contanst.dart';
import 'package:digitaleye/services/database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart' as foundation;
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:digitaleye/Helper/CommonWidgets/CommonWidgets.dart';
import 'package:digitaleye/Helper/Constant.dart';
import 'package:digitaleye/Helper/SharedManager.dart';
import 'package:syncfusion_flutter_charts/charts.dart';



void main()=> runApp(new IndicatorList());

bool get isIos =>
    foundation.defaultTargetPlatform == foundation.TargetPlatform.iOS;

class IndicatorList extends StatefulWidget {
  @override
  _IndicatorListState createState() => _IndicatorListState();
}


class _IndicatorListState extends State<IndicatorList> {



  AppTranslationsDelegate _newLocaleDelegate;
  @override
  void initState() {

    super.initState();
    _newLocaleDelegate = AppTranslationsDelegate(newLocale:null);
      application.onLocaleChanged = onLocaleChange;
  }



  @override
  Widget build(BuildContext context) {
      return MaterialApp(
        debugShowCheckedModeBanner:false,
        localizationsDelegates: [
          _newLocaleDelegate,
          //provides localised strings
          GlobalMaterialLocalizations.delegate,
          //provides RTL support
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: [
          SharedManager.shared.language
        ],
        home: DefaultTabController(
        length: 1,
        initialIndex: 0,
        child: Scaffold(
          appBar: new AppBar(
          centerTitle: true,
          // title: setHeaderTitle(AppTranslations.of(context).text(AppTitle.appTitle),Colors.white),
          title: setHeaderTitle("Symptoms Overview",Colors.white),
          backgroundColor: AppColor.themeColor,
          elevation: 1.0,
          leading: new IconButton(
                icon: Icon(Icons.arrow_back_ios,color:Colors.white),
                onPressed: (){
                  Navigator.of(context).pop();
                },
              ),
              bottom: TabBar(
                indicatorColor: Colors.red,
                tabs: <Widget>[

                  Tab(
                    text: "",
                  )
                ],
              ),
            ),
            body: TabBarView(
              children: <Widget>[IosSecondPage()],
            )),
      ),
      );
    }

  void onLocaleChange(Locale locale) {
      setState(() {



        _newLocaleDelegate = AppTranslationsDelegate(newLocale: locale);
      });
    }
    }



List<PremiumIndicator> listPremium = [
//1
    PremiumIndicator("Blurred Vision:",
    "refers to a lack of sharpness of vision resulting in the inability to see fine detail.",Icon(Icons.visibility_off,color: Colors.red,)),
  //2
    PremiumIndicator("Double Vision:",
    "occurs when a person sees a double image where there should only be one.",Icon(Icons.people,color: Colors.orange,)),
  //3
  PremiumIndicator("Dry Eyes:",
     "The normally white sclera is in this case coloured red and small veins clearly stand out.",Icon(Icons.format_color_reset,color: Colors.teal,)),
  //4
  PremiumIndicator("Eye Irritation:",
     "Itchy, watery, swollen, and red eyes are signs of allergic conjunctivitis, an inflammation of the membrane that covers the whites of your eyes.",Icon(Icons.invert_colors,color: Colors.green)),
  //5
    PremiumIndicator("Headaches:",
     "Headache is defined as a pain arising from the head or upper neck of the body.",Icon(Icons.person,color: Colors.grey,)),
  //6
  PremiumIndicator("Neckpain:",
     "Neck pain is the sensation of discomfort in the neck area.",Icon(Icons.person_remove_alt_1_sharp,color: AppColor.themeColor)),

//7
  PremiumIndicator("Backpain:",
      "refers to Pain felt in the low or upper back.",Icon(Icons.airline_seat_legroom_extra,color: Colors.red,)),
  //8
  PremiumIndicator("Concentration Problems:",
      "difficulty is a decreased ability to focus your thoughts on something.",Icon(Icons.person_search,color: Colors.orange,)),
  //9
  PremiumIndicator("Red Eyes:",
      "having the eyes reddened or inflamed .",Icon(Icons.remove_red_eye_outlined,color: Colors.teal,)),
  //10

  PremiumIndicator("Colored Halos:",
      "are bright circles that surround a light source, like headlights. Glare is light that enters your eye and interferes with your vision.",Icon(Icons.account_circle,color: Colors.grey,)),
  //11
  PremiumIndicator("Burning Eyes:",
      "describes a feeling of burning and irritation of the eyes.",Icon(Icons.local_fire_department,color: AppColor.themeColor)),

//12
  PremiumIndicator("Watery Eyes:",
      "Watering eye, epiphora or tearing, is a condition in which there is an overflow of tears onto the face, often without a clear explanation.",Icon(Icons.waterfall_chart,color: Colors.red,)),
  //13
  PremiumIndicator("Excessive Blinking:",
      "is when you blink more than you want to. The most common cause in adults is a problem on the surface of your eye.",Icon(Icons.remove_red_eye_outlined,color: Colors.teal,)),
  //14
  PremiumIndicator("Light Sensitivity:",
      "is where the light level in the environment is too bright and causes discomfort.",Icon(Icons.lightbulb_outline,color: Colors.grey,)),
  //15
  PremiumIndicator("Shoulder Pain:",
      "Shoulder pain may arise from the shoulder joint itself or from any of the many surrounding muscles, ligaments or tendons.",Icon(Icons.person_remove_alt_1,color: AppColor.themeColor)),
//16
  PremiumIndicator("Heavy Eyelids:",
      "Ptosis is the term used for a drooping upper eyelid.",Icon(Icons.remove_red_eye_outlined,color: Colors.red,)),

  //17
  PremiumIndicator("Eye Strain:",
      "fatigue of the eyes, such as that caused by reading or looking at a computer screen for too long.",Icon(Icons.remove_red_eye_rounded,color: Colors.grey,)),
  //18
  PremiumIndicator("Worsening Eyesight:",
      "also known as vision impairment or vision loss, is a decreased ability to see ",Icon(Icons.blur_on_rounded,color: AppColor.themeColor)),


];



class IosSecondPage extends StatelessWidget {
  const IosSecondPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height:MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: new Padding(
        padding: new EdgeInsets.only(top: 15),
        child: _setPremiumIndicatorList(listPremium),
      ),
    );
  }
}

double monVal;
double tueVal;
double WedVal;
double ThurVal;
double FriVal;
double SatVal;
double SunVal;
double total;
double today;
String minDate;
double minAmount;
String maxDate;
double maxAmount;
String title;




_setPremiumIndicatorList(List<PremiumIndicator>data){
  DatabaseService s = new DatabaseService();
  return new ListView.builder(
    itemCount: data.length,
    itemBuilder: (context,index){
      return new InkWell(
        onTap: () async {
          String value = "";
          String title = "";
          if(index == 0) {
            title = "Blurred Vision";
             value = "blurred";}
          if(index == 1) {
            title = "Double Vision";
            value = "double";
          } if(index == 2) {
            title = "Dry Eyes";
            value = "dry";
          } if(index == 3) {
            title = "Eye Irritation";
            value = "eye";
          } if(index == 4) {
            title = "Headaches";
            value = "head";
          } if(index == 5) {
            title = "Neckpain";
            value = "pain";
          } if(index == 6) {
            title = "Backpain";
            value = "back";
          } if(index == 7) {
            title = "Concentration Problems";
            value = "concentrating";
          } if(index == 8) {
            title = "Red Eyes";
            value = "red";
          } if(index == 9) {
            title = "Colored Halos";
            value = "colored";
          } if(index == 10) {
            title = "Burning Eyes";
            value = "burning";
          } if(index == 11) {
            title = "Watery Eyes";
            value = "watery";
          } if(index == 12) {
            title = "Excessive Blinking";
            value = "excessive";
          } if(index == 13) {
            title = "Light Sensitivity";
            value = "light";
          } if(index == 14) {
            title = "Shoulder Pain";
            value = "shoulder";
          } if(index == 15) {
            title = "Heavy Eyelids";
            value = "heavy";
          } if(index == 16) {
            title = "Eye Strain";
            value = "strain";
          } if(index == 17) {
            title = "Worsening Eyesight";
            value = "worsening";
                   }
            double monVal =await s.getIntFromSharedPref("Monday" + value);
            double tueVal =await s.getIntFromSharedPref("Tuesday" +value);
            double WedVal =await s.getIntFromSharedPref("Wednesday" +value);
            double ThurVal =await s.getIntFromSharedPref("Thursday" + value);
            double FriVal =await s.getIntFromSharedPref("Friday" + value);
            double SatVal =await s.getIntFromSharedPref("Saturday" + value);
            double SunVal =await s.getIntFromSharedPref("Sunday" + value);
            double today =await s.getIntFromSharedPref(value);
            String minDate =RouterName.Minheaddate;
            double minAmount =await s.getIntFromSharedPref("min"+value);
            String maxDate =RouterName.Maxheaddate;
            double maxAmount =await s.getIntFromSharedPref("max" +value);
            String ti =  title + " Details";
            double total =monVal + tueVal + WedVal + ThurVal + FriVal +SatVal + SunVal;
            Navigator.of(context).push(MaterialPageRoute(builder: (context)=>
                TestIndicatorDetails(monVal,tueVal,WedVal,ThurVal,FriVal,
                    SatVal,SunVal,total,today,minDate,minAmount,maxDate,maxAmount,ti,today.toString())
            ));

      },

          child: new Container(
          // height:100,
          padding: new EdgeInsets.all(8),
          child: new Material(
            color: Colors.white,
            elevation: 2.0,
            borderRadius: BorderRadius.circular(8),
            child: new Padding(
              padding: EdgeInsets.all(8),
              child: new Row(
                children: <Widget>[
                  data[index].icon,
                  SizedBox(width: 12,),
                  new Container(
                    height: 40,
                    width: 1,
                    color: AppColor.themeColor,
                  ),
                  SizedBox(width: 12,),
                  new Expanded(
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        setCommonText(data[index].title,Colors.black, 17.0, FontWeight.w500, 1),
                        SizedBox(height: 3,),
                        setCommonText(data[index].subTitle,Colors.grey, 16.0, FontWeight.w400, 3),
                      ],
                    )
                  ),
                  SizedBox(width: 12,),
                  new Icon(Icons.check_circle,color:AppColor.themeColor),
                ],
              )
            ),
          ),
        ),
      );
    },
  );
}


class BasicIndicator{
      String title;
      Icon icon;
      BasicIndicator(this.title,this.icon);
}

class PremiumIndicator{
    String title;
    String subTitle;
    Icon icon;

    PremiumIndicator(this.title,this.subTitle,this.icon);
}