import 'package:digitaleye/Screens/contanst/contanst.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:digitaleye/Helper/CommonWidgets/CommonWidgets.dart';
import 'package:digitaleye/Helper/Constant.dart';
import 'package:digitaleye/Helper/SharedManager.dart';
import 'package:digitaleye/Localization/app_translations.dart';
import 'package:digitaleye/Localization/app_translations_delegate.dart';
import 'package:digitaleye/Localization/application.dart';

import '../../globals.dart';
import 'QuestList.dart';

void main() => runApp(new QuestBlogsScreen(firebaseId));


class QuestBlogsScreen extends StatefulWidget {

  final String firebaseId;

  QuestBlogsScreen(this.firebaseId);

  @override
  _QuestBlogsScreenState createState() => _QuestBlogsScreenState();
}

class _QuestBlogsScreenState extends State<QuestBlogsScreen> {

  AppTranslationsDelegate _newLocaleDelegate;
  @override
  void initState() {
    super.initState();
    _newLocaleDelegate = AppTranslationsDelegate(newLocale:null);
    application.onLocaleChanged = onLocaleChange;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner:false,
      home: DefaultTabController(
        length: 1,
        initialIndex: 0,
        child: Scaffold(
          appBar: new AppBar(
            centerTitle: true,
            // title: setHeaderTitle(AppTranslations.of(context).text(AppTitle.appTitle),Colors.white),
            title: setHeaderTitle(AppTranslations.of(context).text(AppTitle.drawerBlogs),Colors.white),
            backgroundColor: AppColor.themeColor,
            elevation: 1.0,
            bottom: TabBar(
              indicatorColor: Colors.red,
              tabs: <Widget>[
                Tab(
                  text:"",
                ),
              ],
            ),
            actions: setCommonCartNitificationView(context),
          ),
          body: TabBarView(
            children: <Widget>[
              QuestList(),
            ],
          ),
          drawer: SharedManager.shared.setDrawer(context,PersonalInfo.name,PersonalInfo.email),
        ),
      ),
      routes: {
        '/DrugsBlogsScreen': (BuildContext context) => QuestBlogsScreen(RouterName.id.toString())
      },
      theme: SharedManager.shared.getThemeType(),
      localizationsDelegates: [
        _newLocaleDelegate,
        //provides localised strings
        GlobalMaterialLocalizations.delegate,
        //provides RTL support
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        SharedManager.shared.language
      ],
    );
  }
  //This is for localization
  void onLocaleChange(Locale locale) {
    setState(() {
      _newLocaleDelegate = AppTranslationsDelegate(newLocale: locale);
    });
  }

}