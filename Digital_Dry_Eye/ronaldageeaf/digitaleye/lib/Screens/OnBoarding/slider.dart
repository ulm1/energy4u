import 'package:digitaleye/Helper/Constant.dart';
import 'package:flutter/cupertino.dart';

class Slider {
  final String sliderImageUrl;
  final String sliderHeading;
  final String sliderSubHeading;
  final String skipBtn;

  Slider(
      {@required this.sliderImageUrl,
        @required this.sliderHeading,
        @required this.sliderSubHeading,
        this.skipBtn});
}

final sliderArrayList = [
  Slider(
      sliderImageUrl: 'Assets/images/s1.jpg',
      sliderHeading: Constants.SLIDER_HEADING_1,
      sliderSubHeading: Constants.SLIDER_DESC1,
      skipBtn: ""),
  Slider(
      sliderImageUrl: 'Assets/images/s3.jpg',
      sliderHeading: Constants.SLIDER_HEADING_3,
      sliderSubHeading: Constants.SLIDER_DESC3,
      skipBtn: ""),
  Slider(
      sliderImageUrl: 'Assets/images/s2.jpg',
      sliderHeading: Constants.SLIDER_HEADING_2,
      sliderSubHeading: Constants.SLIDER_DESC2,
      skipBtn: ""),
];