import 'package:digitaleye/Helper/Constant.dart';
import 'package:digitaleye/Screens/Notifications/utils/notificationHelper.dart';
import 'package:flutter/material.dart';

import '../../../main.dart';


class NotificationSwitchBuilder extends StatefulWidget {
  @override
  _NotificationSwitchBuilderState createState() =>
      _NotificationSwitchBuilderState();
}

class _NotificationSwitchBuilderState extends State<NotificationSwitchBuilder> {
  bool isSwitched = true;
  String text = "On";
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Center(
          child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
            Switch(
              value: isSwitched,
              onChanged: (value) {


                if (!value) {
                  turnOffNotification(flutterLocalNotificationsPlugin);
                  text = "Off";
                }

                if (value) {
                  text = "On";
                }
                setState(() {
                  isSwitched = value;
                });
              },
              activeTrackColor: Colors.lightBlueAccent,
              activeColor: AppColor.themeColor,
            ),
            Text(
              text,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ])),
    );
  }
}
