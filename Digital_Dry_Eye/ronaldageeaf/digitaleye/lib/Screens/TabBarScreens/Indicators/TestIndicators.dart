import 'package:digitaleye/Localization/app_translations_delegate.dart';
import 'package:digitaleye/Localization/application.dart';
import 'package:digitaleye/Screens/IndicatorList/IndicatorList.dart';
import 'package:digitaleye/Screens/addResults/AddResultsScreen.dart';
import 'package:digitaleye/Screens/contanst/contanst.dart';
import 'package:digitaleye/services/database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:digitaleye/Helper/CommonWidgets/CommonWidgets.dart';
import 'package:digitaleye/Helper/Constant.dart';
import 'package:digitaleye/Helper/SharedManager.dart';
import 'package:digitaleye/Localization/app_translations.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

import '../../../main.dart';


void main()=>runApp(new TestIndicators());


class TestIndicators extends StatefulWidget {
  @override

  _TestIndicatorsState createState() => _TestIndicatorsState();
}


String days;
double values;

class _TestIndicatorsState extends State<TestIndicators> {
  bool isLoading = true;
  double value1 = 0;
  double value2 = 0;
  double value3 = 0;
  double value4 = 0;
  double value5 = 0;
  double value6 = 0;
  double value7 = 0;

  List profileList = [];




  DatabaseService s = new DatabaseService();

  getData() async {

    double value11 = await s.getIntFromSharedPref("MondayAverage");
    double value21 = await s.getIntFromSharedPref("TuesdayAverage");
    double value31 = await s.getIntFromSharedPref("WednesdayAverage");
    double value41 = await s.getIntFromSharedPref("ThursdayAverage");
    double value51 = await s.getIntFromSharedPref("FridayAverage");
    double value61 = await s.getIntFromSharedPref("SaturdayAverage");
    double value71 =await s.getIntFromSharedPref("SundayAverage");


    setState(()  {
      value1 = value11;
      value2 = value21;
      value3 = value31;
      value4 = value41;
      value5 = value51;
      value6 = value61;
      value7 = value71;

    });

  }



    _setChardIndicatorView()  {
      TimeOfDay roomBooked2 = TimeOfDay.fromDateTime(DateTime.now().add(Duration(hours:20,minutes: 0))); // 4:30pm

    getData();


    return new Container(
      height: 200,
      child:SfCartesianChart(
            primaryXAxis: CategoryAxis(),
            // Enable legend
            legend: Legend(isVisible: false),
            // Enable tooltip
            tooltipBehavior: TooltipBehavior(enable: false),
            series: <LineSeries<SalesData, String>>[
              LineSeries<SalesData, String>(
                dataSource:  <SalesData>[
                  SalesData('Mon', value1),
                  SalesData('Tue', value2),
                  SalesData('Wed', value3),
                  SalesData('Thu', value4),
                  SalesData('Fri', value5),
                  SalesData('Sat', value6),
                  SalesData('Sun', value7),
                ],
                xValueMapper: (SalesData sales, _) => sales.year,
                yValueMapper: (SalesData sales, _) => sales.sales,
                // Enable data label
                dataLabelSettings: DataLabelSettings(isVisible: true)
              )
            ]
          ),
    );
  }

_setTestIndicatorView(){


  return new GridView.count(

    crossAxisCount: 1,
    children: List<Widget>.generate(1,(index){

      return new Container(
        height: 350,
        padding: new EdgeInsets.all(15),
        child: new Material(
          elevation: 2.0,
          color: Colors.white,
          borderRadius: new BorderRadius.circular(8),
          child: new Column(
            children: <Widget>[
              new Expanded(
                child: new Container(
                  child: Padding(
                    padding: new EdgeInsets.all(8),
                    child: new Row(
                      children: <Widget>[
                        new Icon(Icons.pin_drop,color: AppColor.themeColor,size: 20,),
                        SizedBox(width: 3,),
                        setCommonText("Symptoms Overwiew", AppColor.themeColor, 16.0, FontWeight.w600,1)
                      ],
                    ),
                  ),
                ),
              ),
              new Expanded(
                flex: 5,
                child: new Container(
                  child:isLoading
                      ? Container(
                    child: Center(child: CircularProgressIndicator()),
                  )
                      : _setChardIndicatorView(),
                ),
              ),
              Divider(color: Colors.grey,),
              new Expanded(
                child: new Container(
                  child: new Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      new InkWell(
                        onTap: (){
                          Navigator.of(context,rootNavigator: false).push(MaterialPageRoute(builder: (context)=>IndicatorList()));
                        },
                         child: new Row(
                           children: <Widget>[
                             new Icon(Icons.blur_circular,color:AppColor.themeColor,size: 20,),
                             SizedBox(width: 3,),
                             setCommonText("Symptoms", AppColor.themeColor, 16.0, FontWeight.w500, 1),
                           ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                  child: _setBottomView())            ],
          ),
        ),
      );
    }),
  );
}



  _setBottomView(){
    // final Map<dynamic, dynamic> languagesMap = {
    //   languagesList[0]: languageCodesList[0],
    //   languagesList[1]: languageCodesList[1]
    // };

    return new Container(
      height:profileList.length * 80.0,
      color: Colors.grey[200],
      padding: new EdgeInsets.all(15),
      child: new GridView.count(
        crossAxisCount: 1,
        childAspectRatio: 5.5,
        physics: NeverScrollableScrollPhysics(),
        children: List<Widget>.generate(profileList.length,(index){
          return new Container(
            height: 70,
            padding: new EdgeInsets.only(top:5,bottom: 5),
            child: new InkWell(
              onTap: (){


                if(index ==0){
                  Navigator.of(context,rootNavigator: false).push(MaterialPageRoute(builder: (context)=>AddReults(RouterName.id.toString())));
                }

                else{
                  SharedManager.shared.currentIndex = 0;
                  Navigator.of(context,rootNavigator: false).pushAndRemoveUntil(MaterialPageRoute(builder: (context)=>HomePage(null)),ModalRoute.withName('/MyHomePage'));
                  // Navigator.of(context,rootNavigator: true).pushAndRemoveUntil(MaterialPageRoute(builder: (context)=>LoginPage()),ModalRoute.withName('/login'));
                }
              },
              child: new Material(
                elevation: 2.0,
                borderRadius: new BorderRadius.circular(5),
                child: new Padding(
                  padding: new EdgeInsets.only(left: 15,right: 15),
                  child: new Row(
                    children: <Widget>[
                      profileList[index]['icon'],
                      SizedBox(width: 12,),
                      new Container(height: 30,color: Colors.grey[300],width: 2,),
                      SizedBox(width: 12,),
                      new Expanded(
                          child: setCommonText(profileList[index]['title'], Colors.grey, 16.0, FontWeight.w500,1)
                      ),
                      SizedBox(width: 12,),
                      new Icon(Icons.arrow_forward_ios,size: 18,color:AppColor.themeColor),
                    ],
                  ),
                ),
              ),
            ),
          );
        }),
      ),
    );
  }

AppTranslationsDelegate _newLocaleDelegate;
@override
  void initState() {
  isLoading = false;

  super.initState();
    SharedManager.shared.isOnboarding = true;
    _newLocaleDelegate = AppTranslationsDelegate(newLocale:null);
      application.onLocaleChanged = onLocaleChange;
  }

  @override


  Widget build(BuildContext context) {
    this.profileList = [
      // {"title":AppTranslations.of(context).text(AppTitle.profileGoalSetting),"icon":Icon(Icons.local_hospital,color: AppColor.themeColor,size: 18,)},
      {"title":AppTranslations.of(context).text(AppTitle.profileWeight),"icon":Icon(Icons.settings,color: AppColor.themeColor,size: 18,)},
      {"title":AppTranslations.of(context).text(AppTitle.drawerLogout),"icon":Icon(Icons.settings_power,color: AppColor.themeColor,size: 18,)},
    ];
    return MaterialApp(
      debugShowCheckedModeBanner:false,
      home: Scaffold(
        body: Column(
          children: <Widget>[

        Expanded(
          child: _setTestIndicatorView()),
            SizedBox(height: 12,),


            _setBottomView(),

          ],
        ),
         appBar: new AppBar(
          centerTitle: true,
          // title: setHeaderTitle(AppTranslations.of(context).text(AppTitle.appTitle),Colors.white),
          title: setHeaderTitle("Profile",Colors.white),
          backgroundColor: AppColor.themeColor,
          elevation: 1.0,
          actions: setCommonCartNitificationView(context),
        ),
        drawer: SharedManager.shared.setDrawer(context, PersonalInfo.name,PersonalInfo.email),
    ),
    routes: {
        '/TestIndicator': (BuildContext context) => TestIndicators()
      },
      localizationsDelegates: [
          _newLocaleDelegate,
          //provides localised strings
          GlobalMaterialLocalizations.delegate,
          //provides RTL support
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: [
          SharedManager.shared.language
        ],
    );
  }
  void onLocaleChange(Locale locale) {
      setState(() {
        _newLocaleDelegate = AppTranslationsDelegate(newLocale: locale);
      });
    }
}

class SalesData {
  SalesData(this.year, this.sales);
  final String year;
  final double sales;
}