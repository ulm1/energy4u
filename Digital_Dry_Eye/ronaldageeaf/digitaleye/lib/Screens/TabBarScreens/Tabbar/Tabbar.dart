import 'package:digitaleye/Localization/app_translations_delegate.dart';
import 'package:digitaleye/Localization/application.dart';
import 'package:digitaleye/Screens/QuestionnaireBlogs/QuestionnaireListScreen.dart';
import 'package:digitaleye/Screens/TabBarScreens/DashBoard/Dashboard.dart';
import 'package:digitaleye/Screens/TabBarScreens/DoctorList/DoctorList.dart';
import 'package:digitaleye/Screens/TabBarScreens/Indicators/TestIndicators.dart';
import 'package:digitaleye/Screens/TabBarScreens/UserProfile/UserProfile.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:digitaleye/Helper/Constant.dart';
import 'package:digitaleye/Helper/SharedManager.dart';
import 'package:digitaleye/Localization/app_translations.dart';

import '../../../admin.dart';
import '../../../globals.dart';
import '../../../main.dart';

import '../../../globals.dart';


void main() => runApp(new TabBarScreen(firebaseId));


class TabBarScreen extends StatefulWidget {
  final String firebaseId;

  TabBarScreen(this.firebaseId);

  @override
  _TabBarScreenState createState() => _TabBarScreenState();
}

class _TabBarScreenState extends State<TabBarScreen> {




  bool isButtonClick = false;
  List<Widget> listScreen = [
    DashboardScreen(firebaseId),
    DoctorsList(),
    QuestBlogsScreen(firebaseId),
    TestIndicators(),
    UserProfile(),
  ];

  AppTranslationsDelegate _newLocaleDelegate;


  @override
  void initState() {
    super.initState();
    SharedManager.shared.isOnboarding = true;
    _newLocaleDelegate = AppTranslationsDelegate(newLocale:null);
    application.onLocaleChanged = onLocaleChange;
  }


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner:false,
        home: new Scaffold(
          body:listScreen[SharedManager.shared.currentIndex],
          bottomNavigationBar: BottomNavigationBar(
              type: BottomNavigationBarType.fixed,
              currentIndex: SharedManager.shared.currentIndex,
              onTap: onTabTapped,
              items:[
                new BottomNavigationBarItem(
                  icon: Icon(Icons.home,),
                  activeIcon: Icon(Icons.home,),
                  title: Text(AppTranslations.of(context).text(AppTitle.drawerHome)),
                ),
                new BottomNavigationBarItem(
                  icon: Icon(Icons.camera_alt),
                  title: Text("Scanner"),
                ),
                new BottomNavigationBarItem(
                  icon: Icon(Icons.grain,color: Colors.white.withAlpha(0),),
                  title: Text(AppTranslations.of(context).text(AppTitle.drawerBlogs)),
                ),
                new BottomNavigationBarItem(
                  icon: Icon(Icons.person),
                  title: Text(AppTranslations.of(context).text(AppTitle.drawerProfile)),
                ),
                new BottomNavigationBarItem(
                  icon: Icon(Icons.power_settings_new),
                  title: Text(AppTranslations.of(context).text(AppTitle.drawerLogout)),
                ),
              ]
          ),
          floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
          floatingActionButton: FloatingActionButton(
            child: new Icon(Icons.speaker_group,color:isButtonClick?Colors.blue:Colors.black,),
            onPressed: (){
              setState(() {
                isButtonClick = true;
                SharedManager.shared.currentIndex = 2;
              });
            },
            backgroundColor: Colors.grey[300],
          ),

        ),
        localizationsDelegates: [
          _newLocaleDelegate,
          //provides localised strings
          GlobalMaterialLocalizations.delegate,
          //provides RTL support
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: [
          SharedManager.shared.language
        ],
        routes: {
          '/TabBar' : (BuildContext context) => TabBarScreen(firebaseId)
        },
        theme: SharedManager.shared.getThemeType()
    );
  }

  void onTabTapped(int index) {
    setState(() {
      if(index != 2){

        isButtonClick = false;
        SharedManager.shared.currentIndex = index;
      }
      if (index == 4)
      {
        SharedManager.shared.currentIndex = 0;
        Navigator.of(context).
        pushAndRemoveUntil(MaterialPageRoute(builder: (context)=>LaunchingApp(null))
            ,ModalRoute.withName('/MyHomePage'));
      }
      else{
        SharedManager.shared.currentIndex = index;
      }
    });
  }

  //This is for localization
  void onLocaleChange(Locale locale) {
    setState(() {
      _newLocaleDelegate = AppTranslationsDelegate(newLocale: locale);
    });
  }

}

