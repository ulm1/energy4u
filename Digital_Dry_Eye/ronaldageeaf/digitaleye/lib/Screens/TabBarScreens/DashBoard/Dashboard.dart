
import 'package:digitaleye/Localization/app_translations_delegate.dart';
import 'package:digitaleye/Localization/application.dart';

import 'package:digitaleye/Screens/QuestionnaireList/QuestionnaireList.dart';
import 'package:digitaleye/Screens/AddDeviceScreen/FindDoctorScreen.dart';
import 'package:digitaleye/Screens/IndicatorList/IndicatorList.dart';
import 'package:digitaleye/Screens/IndicatorList/TestIndicators2.dart';
import 'package:digitaleye/Screens/LoginPage/ComonLoginWidgets.dart';
import 'package:digitaleye/Screens/TabBarScreens/Indicators/TestIndicators.dart';
import 'package:digitaleye/Screens/contanst/contanst.dart';
import 'package:digitaleye/services/database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:digitaleye/Helper/CommonWidgets/CommonWidgets.dart';
import 'package:digitaleye/Helper/Constant.dart';
import 'package:digitaleye/Helper/SharedManager.dart';
import 'package:digitaleye/Localization/app_translations.dart';


import '../../../globals.dart';


void main() => runApp(new DashboardScreen(firebaseId));
bool notifi = false;

class DashboardScreen extends StatefulWidget {
  final String firebaseId;
  DashboardScreen(this.firebaseId);


  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class   Event {
  final String title;
  final double monvalue;
  final double tuevalue;
  final double wedvalue;
  final double thurvalue;
  final double frivalue;
  final double satvalue;
  final double sunvalue;
  final double totalvalue;
  final double todayvalue;
  final double minvalue;
  final double maxvalue;

  Event(this.title, this.monvalue, this.tuevalue, this.wedvalue, this.thurvalue, this.frivalue, this.satvalue, this.sunvalue, this.totalvalue, this.todayvalue, this.minvalue, this.maxvalue);

}


class _DashboardScreenState extends State<DashboardScreen> {




  @override
  void dispose() {
    super.dispose();
  }


  List listData = [
      {"title":"dashbFindHospital","icon":Icon(Icons.query_builder,color:Colors.white,size: 40,),"availability":"hospitalAvaliability","isSelect":false},
         {"title":"dashbAppointment","icon":Icon(Icons.insert_invitation,color:Colors.white,size: 40,),"availability":"appointmentAvaliability","isSelect":false},
        {"title":"dashbPriceServices","icon":Icon(Icons.local_hospital,color:Colors.white,size: 40,),"availability":"priceServicesAvaliability","isSelect":false},

  ];

_setMainInformationView(){

  return new Container(
    // height: 185,
    padding: new EdgeInsets.all(20),
    // color: Colors.red,
    child: new Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        new Row(
          children: <Widget>[

            //
            setCommonText(AppTranslations.of(context).text(AppTitle.dashbHello)+ RouterName.usern,Colors.black,25.0,FontWeight.w500,1),
            setCommonText(CreateAccountString.fullName+" ,",Colors.black,25.0,FontWeight.w500,1),
          ],
        ),
        SizedBox(height: 5,),
        setCommonText(AppTranslations.of(context).text(AppTitle.dashbTitleNote),Colors.grey,25.0,FontWeight.w500,2),
        SizedBox(height: 5,),
      ],
    ),
  );
}

  List<Event> events = [];

  getData() async {
    DatabaseService s = new DatabaseService();
    String value = "";
    String title = "";

    List<Event> events = [];
    while(i < 19) {
      if (i == 0) {
        title = "Blurred Vision";
        value = "blurred";
      }
      if (i == 1) {
        title = "Double Vision";
        value = "double";
      }
      if (i == 2) {
        title = "Dry Eyes";
        value = "dry";
      }
      if (i == 3) {
        title = "Eye Irritation";
        value = "eye";
      }
      if (i == 4) {
        title = "Headaches";
        value = "head";
      }
      if (i == 5) {
        title = "Neckpain";
        value = "pain";
      }
      if (i == 6) {
        title = "Backpain";
        value = "back";
      }
      if (i == 7) {
        title = "Concentration Problems";
        value = "concentrating";
      }
      if (i == 8) {
        title = "Red Eyes";
        value = "red";
      }
      if (i == 9) {
        title = "Colored Halos";
        value = "colored";
      }
      if (i == 10) {
        title = "Burning Eyes";
        value = "burning";
      }
      if (i == 11) {
        title = "Watery Eyes";
        value = "watery";
      }
      if (i == 12) {
        title = "Excessive Blinking";
        value = "excessive";
      }
      if (i == 13) {
        title = "Light Sensitivity";
        value = "light";
      }
      if (i == 14) {
        title = "Shoulder Pain";
        value = "shoulder";
      }
      if (i == 15) {
        title = "Heavy Eyelids";
        value = "heavy";
      }
      if (i == 16) {
        title = "Eye Strain";
        value = "strain";
      }
      if (i == 17) {
        title = "Worsening Eyesight";
        value = "worsening";
      }


      monVal = await s.getIntFromSharedPref(
          "Monday" + value);
      tueVal = await s.getIntFromSharedPref(
          "Tuesday" + value);
      WedVal = await s.getIntFromSharedPref(
          "Wednesday" + value);
      ThurVal = await s.getIntFromSharedPref(
          "Thursday" + value);
      FriVal = await s.getIntFromSharedPref(
          "Friday" + value);
      SatVal = await s.getIntFromSharedPref(
          "Saturday" + value);
      SunVal = await s.getIntFromSharedPref(
          "Sunday" + value);
      double total = monVal + tueVal + WedVal + ThurVal + FriVal +
          SatVal + SunVal;






      double today = await s.getIntFromSharedPref(value);
      double minAmount = await s.getIntFromSharedPref(
          "min" + value);
      double maxAmount = await s.getIntFromSharedPref(
          "max" + value);
      String ti = title + " Details";

      Event event = new Event(title, monVal, tueVal, WedVal, ThurVal, FriVal, SatVal, SunVal, total, today, minAmount, maxAmount);
      events.add(event);

      i++;
      print(i);
    }
    /*
    double total = monVal + tueVal + WedVal + ThurVal + FriVal +
        SatVal + SunVal;
    Event event = new Event(
        title,
        monVal,
        tueVal,
        WedVal,
        ThurVal,
        FriVal,
        SatVal,
        SunVal,
        total,
        today,
        minAmount,
        maxAmount);
    events.add(event);


     */

    double value11 = await s.getIntFromSharedPref("MondayAverage");
    double value21 = await s.getIntFromSharedPref("TuesdayAverage");
    double value31 = await s.getIntFromSharedPref("WednesdayAverage");
    double value41 = await s.getIntFromSharedPref("ThursdayAverage");
    double value51 = await s.getIntFromSharedPref("FridayAverage");
    double value61 = await s.getIntFromSharedPref("SaturdayAverage");
    double value71 =await s.getIntFromSharedPref("SundayAverage");


    setState(()  {


    });

    return events;

  }




_setGridViewListing(){



  return new Container(
    height: 520,
    // color: Colors.yellow,
    padding: new EdgeInsets.all(20),
    child: new GridView.count(
      physics: NeverScrollableScrollPhysics(),
        crossAxisCount: 2,
        childAspectRatio: (3/2.5),
        children: new List<Widget>.generate(listData.length, (index) {
          return new GridTile(
            child: new InkWell(
                onTap: ()  async {
                   List<Event> myevents = await getData() ;
                   print(myevents);


                  setState(() {
                    for(var i = 0; i < listData.length; i++){
                       listData[i]['isSelect'] = false;
                    }
                    listData[index]['isSelect'] = true;
                  });
                  // FindDoctorScreen
                  switch (index) {

                    case 0:
                      Navigator.of(context).push(MaterialPageRoute(builder: (context)=>FindDoctorScreen(widget.firebaseId)));
                      break;
                      case 1:
                        Navigator.of(context).push(MaterialPageRoute(builder: (context)=>QuestionnaireList()));
                      break;
                      case 2:
                      Navigator.of(context).push(MaterialPageRoute(builder: (context)=>IndicatorList()));
                      break;
                    default:
                  }
                },
                child: new Container(
                padding: new EdgeInsets.all(5),
                child: new Material(
                 color: (listData[index]['isSelect'])?AppColor.themeColor:Colors.black54,
                  elevation: 2.0,
                  borderRadius: BorderRadius.circular(5),
                    child: new Container(
                    padding: new EdgeInsets.all(12),
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                         listData[index]['icon'],
                         new Column(
                           mainAxisAlignment: MainAxisAlignment.start,
                           crossAxisAlignment: CrossAxisAlignment.start,
                           children: <Widget>[
                             setCommonText(AppTranslations.of(context).text(listData[index]['title']), Colors.white,16.0, FontWeight.w700,2),
                             SizedBox(height: 3,),
                             setCommonText(AppTranslations.of(context).text(listData[index]['availability']), Colors.white,12.0, FontWeight.w500,2),
                           ],
                         )
                      ],
                    ),
                  ),
                )
              ),
            ),
          );
        }),
      ),
    );
}

AppTranslationsDelegate _newLocaleDelegate;
  @override
  void initState() {
    super.initState();

    _newLocaleDelegate = AppTranslationsDelegate(newLocale:null);
      application.onLocaleChanged = onLocaleChange;


  }




  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner:false,
      home: new Scaffold(
        appBar: new AppBar(
          centerTitle: true,
          title: setHeaderTitle(AppTranslations.of(context).text(AppTitle.appTitle),Colors.white),
          backgroundColor: AppColor.themeColor,
          elevation: 1.0,
          actions: setCommonCartNitificationView(context),
        ),
        drawer: SharedManager.shared.setDrawer(context, "Digital Eye", ""),
        body: new Container(
          color: Colors.white,
          child: new ListView(
            children: <Widget>[
              _setMainInformationView(),
              _setGridViewListing()
            ],
          ),
        ),
      ),
      routes: {
        '/Dashboard': (BuildContext context) => DashboardScreen(widget.firebaseId)
      },
      theme: SharedManager.shared.getThemeType(),
      localizationsDelegates: [
          _newLocaleDelegate,
          //provides localised strings
          GlobalMaterialLocalizations.delegate,
          //provides RTL support
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: [
          SharedManager.shared.language
        ],
    );
  }

  //This is for localization
  void onLocaleChange(Locale locale) {
      setState(() {

        _newLocaleDelegate = AppTranslationsDelegate(newLocale: locale);
      });
    }
}