import 'dart:async';
import 'dart:io';
import 'dart:math';
import 'dart:ui' show lerpDouble;
import 'dart:ui';
import 'package:digitaleye/Helper/Constant.dart';
import 'package:digitaleye/Screens/TabBarScreens/DoctorList/PreviewScreen.dart';
import 'package:digitaleye/Screens/TabBarScreens/DoctorList/face_contour_painter.dart';
import 'package:digitaleye/Screens/contanst/contanst.dart';
import 'package:google_ml_vision/google_ml_vision.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:camera/camera.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'colors.dart';
import 'scanner_utils.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'dart:ui' as ui;
import 'package:firebase_core/firebase_core.dart' as firebase_core;
import 'package:syncfusion_flutter_gauges/gauges.dart';
import 'package:digitaleye/pages/result.dart' as R;
import 'package:image/image.dart' as IMG;

import 'utils.dart';

enum AnimationState { search, barcodeNear, barcodeFound, endSearch }
var postleft = 50.0;
var posttop = 139.75;
var postright = 361.45;
var postbottom = 259.10;
bool doesContain = false;
Rect validRect;
String _scannerHint = "";


class CameraPreviewScanner extends StatefulWidget {


  @override
  _FaceContourDetectionScreenState createState() => _FaceContourDetectionScreenState();
}

class _FaceContourDetectionScreenState
    extends State<CameraPreviewScanner> {
  final FaceDetector faceDetector = GoogleVision.instance.faceDetector(
      FaceDetectorOptions(
          enableClassification: true,
          enableLandmarks: false,
          enableContours: true,
          enableTracking: true));
  List<Face> faces;
  CameraController _camera;
  bool cameraEnabled = true;
  bool _isDetecting = false;
  CameraLensDirection _direction = CameraLensDirection.front;
  String _barcodePictureFilePath;
  String _croppedPictureFilePath;
  double progressValue = 0;
  bool _isloading = false;
  Timer _timer;
  Size imageSize1 = new Size(0, 0);
  bool boolPicture = false;






  @override
  void initState() {
    RouterName.scannerhint = "Place your Eyes inside the white square";
    super.initState();
    _initializeCamera();
  }

  Future<void> _initializeCamera() async {
    final CameraDescription description =
    await ScannerUtils.getCamera(_direction);


    _camera = CameraController(
      description,
      defaultTargetPlatform == TargetPlatform.iOS
          ? ResolutionPreset.medium
          : ResolutionPreset.medium,
    );
    await _camera.initialize();

    print("initialize Camera");


    _camera.startImageStream((CameraImage image) {
      if (_isDetecting) return;

      _isDetecting = true;


      ScannerUtils.detect(
        image: image,
        detectInImage: faceDetector.processImage,
        imageRotation: description.sensorOrientation,
      ).then(
            (dynamic result) {
          setState(() {
            faces = result;
            boolPicture = RouterName.boolPicture;
            print("hi" + boolPicture.toString());
            _scannerHint=  RouterName.scannerhint;
            if(RouterName.boolPicture){
              RouterName.scannerhint = "Hold Still....Taking Picture";
              Timer(Duration(seconds: 2), () {
                takePicture();
              });


          }});

          _isDetecting = false;
        },
      ).catchError(
            (_) {
          _isDetecting = false;
        },
      );
    });
  }

  Widget _buildResults() {
    const Text noResultsText = const Text('No results!');

    if (faces == null || _camera == null || !_camera.value.isInitialized) {
      return noResultsText;
    }

    CustomPainter painter;

    final Size imageSize = Size(
      _camera.value.previewSize.height,
      _camera.value.previewSize.width,
    );

    setState(() {
      imageSize1 = imageSize;

    });

    if (faces is! List<Face>) return noResultsText;
    painter = FaceContourPainter(imageSize, faces, _direction,boolPicture);

    return CustomPaint(
      painter: painter,
    );
  }

  Widget _buildImage() {
    return Container(
      constraints: const BoxConstraints.expand(),
      child: _camera == null
          ? const Center(
        child: Text(
          'Initializing Camera...',
          style: TextStyle(
            color: Colors.blueAccent,
            fontSize: 30.0,
          ),
        ),
      )
          : Stack(
        fit: StackFit.expand,
        children: <Widget>[
          CameraPreview(_camera),
          _buildResults(),
          Positioned(
            bottom: 0.0,
            left: 0.0,
            right: 0.0,
            child: Container(
              color: Colors.white,
              height: 50.0,
              child: ListView(
                children: faces
                    .map((face) =>
                    Text(face.boundingBox.center.toString()))
                    .toList(),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _toggleCameraDirection() async {
    if (_direction == CameraLensDirection.back) {
      _direction = CameraLensDirection.front;
    } else {
      _direction = CameraLensDirection.back;
    }

    await _camera.stopImageStream();
    await _camera.dispose();

    setState(() {
      _camera = null;
    });

    _initializeCamera();
  }


  Future<void> _takePicture() async {
    final Directory extDir = await getApplicationDocumentsDirectory();

    final String dirPath = '${extDir.path}/Pictures/barcodePics';
    await Directory(dirPath).create(recursive: true);

    final String timestamp = DateTime.now().millisecondsSinceEpoch.toString();

    final String filePath = '$dirPath/$timestamp.jpg';

    try {
      await _camera.takePicture(filePath);
    } on CameraException catch (e) {
      print(e);
    }



    setState(() {
      _barcodePictureFilePath = filePath;
      RouterName.boolPicture = false;

    });
    cropSquare(_barcodePictureFilePath, _barcodePictureFilePath, false).then(
             (value) =>Navigator.push(context, MaterialPageRoute(builder: (context) =>PreviewScreen(imgPath: _barcodePictureFilePath,fileName: "$timestamp.png",))));


    Navigator.pop(context);
    //cropSquare(_barcodePictureFilePath, _barcodePictureFilePath, false).then(
      //      (value) => _showBottomSheet());

    _camera.dispose();
    _camera = null;
    faces = null;

        //_showBottomSheet());
  }

  void _showBottomSheet() {
    showModalBottomSheet<void>(
      context: context,
      builder: (BuildContext context) {
        return Container(
          width: double.infinity,
          height: 368,
          child: Column(
            children: <Widget>[
              Container(
                height: 56,
                alignment: Alignment.centerLeft,
                decoration: const BoxDecoration(
                  border: Border(bottom: BorderSide(color: Colors.grey)),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(16),
                  child: Text(
                    'Results',
                    // TODO(bmparr): Switch body2 -> bodyText1 once https://github.com/flutter/flutter/pull/48547 makes it to stable.
                    // ignore: deprecated_member_use
                    style: Theme.of(context).textTheme.body2,
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  padding: const EdgeInsets.all(24),
                  child: Column(
                    children: <Widget>[
                      Expanded(
                        flex: 2,
                        child: Image.file(File(_barcodePictureFilePath),fit: BoxFit.contain,),
                      ),
                      Expanded(
                        flex: 2,
                        child: Text(progressValue.toString()),
                      ),


                      Expanded(
                        child: Container(
                          padding: const EdgeInsets.only(bottom: 4),
                          alignment: Alignment.bottomCenter,
                          child: ButtonTheme(
                            minWidth: 312,
                            height: 48,
                            child: RaisedButton.icon(
                              onPressed: () => handleTaskExample2(_barcodePictureFilePath)
                                  .then((value) => Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => R.Results(),
                                    //   builder: (context) =>R.Results(imgPath: _barcodePictureFilePath,fileName: '$timestamp.jpg',)
                                  ))),
                              color: AppColor.themeColor,
                              label: const Text("Submit", style: TextStyle(
                                color: Colors.white,
                              ),),
                              icon: const Icon(Icons.done,color: Colors.white,),
                              elevation: 8.0,
                              shape: const BeveledRectangleBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(7.0),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        );
      },
    ).then((_) => _reset());

  }


  void _reset() {

    Navigator.pop(context);

    /*
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => CameraPreviewScanner()));
            /
     */
  }


  Future cropSquare(
      String srcFilePath, String destFilePath, bool flip) async {

    final MediaQueryData data = MediaQuery.of(context);
    var bytes = await File(srcFilePath).readAsBytes();
    IMG.Image src = IMG.decodeImage(bytes);


    final EdgeInsets padding = data.padding;
    final double maxLogicalHeight =
        data.size.height - padding.top - padding.bottom;


    final double imageHeight = defaultTargetPlatform == TargetPlatform.iOS
        ? src.height.toDouble()
        : src.width.toDouble();

    final double imageScale = imageHeight / maxLogicalHeight;
    final double halfWidth = imageScale  / 2;
    final double halfHeight = imageScale *70 / 2;

    final Size imageSize = Size(
      src.height.toDouble(),
      src.width.toDouble(),
    );
    final Offset center = imageSize.topCenter(Offset(0, 160));

    final Rect validRect = Rect.fromLTRB(
      center.dx - halfWidth,
      center.dy - halfHeight,
      center.dx + halfWidth,
      center.dy + halfHeight,
    );
    print( "eye in pic " + boolPicture.toString());

    int x  =    (center.dx - halfWidth).round() ;
    int y =  (center.dy - halfHeight).round();
    int height =  (center.dx + halfWidth) .toInt();
    int width =   (center.dy + halfHeight).round();


    setState(() {
      _isloading = true;
    });

    var cropSize = min(src.width, src.height);
    int offsetX = (src.width - min(src.width, src.height)) ~/ 2;
    int offsetY = (src.height - min(src.width, src.height)) ~/ 2;

    print("offsetY" + offsetY.toString());
    print("offsetX" + offsetX.toString());


    final int xvalue = defaultTargetPlatform == TargetPlatform.iOS
        ? height - y
        : height + x;
//x = 480
//y = 0
//
    IMG.Image destImage = IMG.copyCrop(
        src,
        xvalue,
        //offsetY,
        offsetY,
        validRect.height.toInt()+ 10,
        src.width);

    if (true) {
      destImage = IMG.flipVertical(destImage);
    }


    var jpg = IMG.encodeJpg(destImage);
    await File(destFilePath).writeAsBytes(jpg);
  }







  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _camera == null
          ? const Center(
        child: Text(
          'Initializing Camera...',
          style: TextStyle(
            color: Colors.blueAccent,
            fontSize: 30.0,
          ),
        ),
      )
          : LiveCameraWithFaceDetection(
        faces: faces,
        camera: _camera,
        cameraEnabled: cameraEnabled,
          boolPicture:boolPicture,
      ),


    );


  }


  showAlertDialog(BuildContext context){
    AlertDialog alert=AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(),
          Container(margin: EdgeInsets.only(left: 5),child:Text("Loading" + progressValue.toString() )),
        ],),
    );
    showDialog(barrierDismissible: false,
      context:context,
      builder:(BuildContext context){
        return alert;
      },
    );
  }



  Future<void> handleTaskExample2(String filePath) async {
    print("hello");
    File largeFile = File(filePath);

    final String timestamp = DateTime.now().millisecondsSinceEpoch.toString();

    firebase_storage.UploadTask task = firebase_storage.FirebaseStorage.instance
        .ref()
        .child(RouterName.id)
        .child('$timestamp.jpg')
        .putFile(largeFile);

    task.snapshotEvents.listen((firebase_storage.TaskSnapshot snapshot) {
      print('Task state: ${snapshot.state}');
      print(
          'Progress: ${(snapshot.bytesTransferred / snapshot.totalBytes) * 100} %');

      setState(() {
        progressValue =
            ((snapshot.bytesTransferred / snapshot.totalBytes) * 100)
                .toDouble();
        doesContain = false;

      });
    }, onError: (e) {
      // The final snapshot is also available on the task via `.snapshot`,
      // this can include 2 additional states, `TaskState.error` & `TaskState.canceled`
      print(task.snapshot);

      if (e.code == 'permission-denied') {
        print('User does not have permission to upload to this reference.');
      }
    });

    // We can still optionally use the Future alongside the stream.
    try {
      await task;
      print('Upload complete.');
    } on firebase_core.FirebaseException catch (e) {
      if (e.code == 'permission-denied') {
        print('User does not have permission to upload to this reference.');
      }
      // ...
    }
  }



  void takePicture() {
    _camera.stopImageStream().then((_) => _takePicture());
  }
}

class LiveCameraWithFaceDetection extends StatelessWidget {
  final List<Face> faces;
  final CameraController camera;
  final bool cameraEnabled;
  final bool boolPicture;



  const LiveCameraWithFaceDetection(
      {Key key, this.faces, this.camera, this.cameraEnabled = true,this.boolPicture})
      : super(key: key);

  Widget getGradientProgressStyle() {
    double progressValue = 0;

    return Container(
        height: 120,
        width: 120,
        child: SfRadialGauge(axes: <RadialAxis>[
          RadialAxis(
              showLabels: false,
              showTicks: false,
              startAngle: 270,
              endAngle: 270,
              radiusFactor: 0.8,
              axisLineStyle: AxisLineStyle(
                thickness: 0.1,
                color: const Color.fromARGB(30, 0, 169, 181),
                thicknessUnit: GaugeSizeUnit.factor,
                cornerStyle: CornerStyle.startCurve,
              ),
              pointers: <GaugePointer>[
                RangePointer(
                    value: progressValue,
                    width: 0.1,
                    sizeUnit: GaugeSizeUnit.factor,
                    enableAnimation: true,
                    animationDuration: 100,
                    animationType: AnimationType.linear,
                    cornerStyle: CornerStyle.startCurve,
                    gradient: const SweepGradient(
                        colors: <Color>[Color(0xFF00a9b5), Color(0xFFa4edeb)],
                        stops: <double>[0.25, 0.75])),
                MarkerPointer(
                  value: progressValue,
                  markerType: MarkerType.circle,
                  enableAnimation: true,
                  animationDuration: 100,
                  animationType: AnimationType.linear,
                  color: const Color(0xFF87e8e8),
                )
              ],
              annotations: <GaugeAnnotation>[
                GaugeAnnotation(
                    positionFactor: 0,
                    widget: Text(progressValue.toStringAsFixed(0) + '%'))
              ]),
        ]));
  }


  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;

    return Container(
      constraints: const BoxConstraints.expand(),
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          cameraEnabled
              ? CameraPreview(camera)
              : Container(
            color: Colors.black,
          ),
          (faces != null && camera.value.isInitialized)
              ? CustomPaint(
            painter: FaceContourPainter(
                Size(
                  camera.value.previewSize.height,
                  camera.value.previewSize.width,
                ),
                faces,
                camera.description.lensDirection,boolPicture),
          )
              : const Text('No results!'),

    Positioned(
    left: 0,
    right: 0,
    top: 0,
    child: Container(
    height: 56,
    decoration: BoxDecoration(
    gradient: LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: const <Color>[Colors.black87, Colors.black87],
    ),
    ),
    ),
    ),
    Positioned(
    left: 0.0,
    bottom: 0.0,
    right: 0.0,
    //change the size of the container
    height: screenHeight / 2,
    child: Container(
    decoration: BoxDecoration(
    border: Border.all(
    color: Colors.red[500],
    ),
    color: Colors.white,
    borderRadius: BorderRadius.all(Radius.circular(20))),
    child: Padding(
    padding: const EdgeInsets.all(80.0),
    child: Center(
    child: Column(
    children: [
    //getGradientProgressStyle(),
    Text(
    _scannerHint ?? 'Scan your Eyes',
    style: Theme.of(context).textTheme.button,
    textScaleFactor: 1.3,
    ),
    ],
    ),
    ),
    ),
    ),
    ),

        ],
      ),
    );
  }


}