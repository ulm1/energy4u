
import 'dart:ui';
import 'package:camera/camera.dart';
import 'package:digitaleye/Screens/contanst/contanst.dart';
import 'package:flutter/material.dart';
import 'package:google_ml_vision/google_ml_vision.dart';





class FacePaint extends CustomPaint {
  final CustomPainter painter;

  FacePaint({this.painter}) : super(painter: painter);
}

class FaceContourPainter extends CustomPainter {
  final Size imageSize;
  final List<Face> faces;
  bool boolPicture;

  double  rectanglewidth = 340;
  double  rectangleheight = 100;

  final CameraLensDirection cameraLensDirection;



  FaceContourPainter(this.imageSize, this.faces, this.cameraLensDirection, this.boolPicture);

  @override
  void paint(Canvas canvas, Size size) {
    final Offset center = size.topCenter(Offset(0, 160));
    final double halfWidth = 340 / 2;
    final double halfHeight = 100 / 2;

    final paintRectStyle = Paint()
      ..color = Colors.white
      ..strokeWidth = 5.0
      ..style = PaintingStyle.stroke;

    final paint = Paint()..color = Colors.yellow;
    final paint2 = Paint()..color = Colors.transparent;


    for (var i = 0; i < faces.length; i++) {

      //Scale rect to image size
      final rect = _scaleRect(
        rect: faces[i].boundingBox,
        imageSize: imageSize,
        widgetSize: size,
      );

//      canvas.drawRect(rect, paintRectStyle);

      final List<Offset> facePoints =
          faces[i].getContour(FaceContourType.face).positionsList;

          faces[i].getContour(FaceContourType.rightEyebrowTop).positionsList;
      final List<Offset> leftEye =
          faces[i].getContour(FaceContourType.leftEye).positionsList;
      final List<Offset> rightEye =
          faces[i].getContour(FaceContourType.rightEye).positionsList;


      canvas.drawPoints(
          PointMode.polygon,
          _scalePoints(
              offsets: leftEye, imageSize: imageSize, widgetSize: size),
          Paint()
            ..strokeWidth = 0.5
            ..color = Colors.white);

      canvas.drawPoints(
          PointMode.polygon,
          _scalePoints(
              offsets: rightEye, imageSize: imageSize, widgetSize: size),
          Paint()
            ..strokeWidth = 0.5
            ..color = Colors.white);

      final Rect rectt = Rect.fromLTRB(
        center.dx - halfWidth,
        center.dy - halfHeight,
        center.dx + halfWidth,
        center.dy + halfHeight,
      );

      canvas.drawRect(rectt, paintRectStyle);

      if ((rectt.contains(leftEye.first) && rectt.contains(rightEye.first) )== false){
        RouterName.scannerhint = "Place your Eyes inside the white square";

      }

      do {
        if (rectt.contains(leftEye.first) && rectt.contains(rightEye.first) &&
            faces[i].leftEyeOpenProbability > 0.9 &&
            faces[i].rightEyeOpenProbability > 0.9) {
          RouterName.boolPicture = true;
          canvas.restore();
          return;
        }

        if (faces[i].rightEyeOpenProbability < 0.9) {
          RouterName.scannerhint = "Open your eyes wider";
          return;
        }

        if (faces[i].leftEyeOpenProbability < 0.9) {
          RouterName.scannerhint = "Open your eyes wider";
          return;
        }
      }
      while(rectt.contains(leftEye.first) && rectt.contains(rightEye.first));
    };

  }



  Offset _scalePoint({
    Offset offset,
    @required Size imageSize,
    @required Size widgetSize,
  }) {
    final double scaleX = widgetSize.width / imageSize.width;
    final double scaleY = widgetSize.height / imageSize.height;

    if(cameraLensDirection == CameraLensDirection.front){
      return Offset(widgetSize.width - (offset.dx * scaleX), offset.dy * scaleY);
    }
    return Offset(offset.dx * scaleX, offset.dy * scaleY);
  }

  List<Offset> _scalePoints({
    List<Offset> offsets,
    @required Size imageSize,
    @required Size widgetSize,
  }) {
    final double scaleX = widgetSize.width / imageSize.width;
    final double scaleY = widgetSize.height / imageSize.height;

    if(cameraLensDirection == CameraLensDirection.front){
      return offsets
          .map((offset) => Offset(widgetSize.width - (offset.dx * scaleX), offset.dy * scaleY))
          .toList();
    }
    return offsets
        .map((offset) => Offset(offset.dx * scaleX, offset.dy * scaleY))
        .toList();
  }

  Rect _scaleRect({
    @required Rect rect,
    @required Size imageSize,
    @required Size widgetSize,
  }) {
    final double scaleX = widgetSize.width / imageSize.width;
    final double scaleY = widgetSize.height / imageSize.height;

    if(cameraLensDirection == CameraLensDirection.front){
      print("qui");
      return Rect.fromLTRB(
        widgetSize.width - rect.left.toDouble() * scaleX,
        rect.top.toDouble() * scaleY,
        widgetSize.width - rect.right.toDouble() * scaleX,
        rect.bottom.toDouble() * scaleY,
      );
    }

    return Rect.fromLTRB(
      rect.left.toDouble() * scaleX,
      rect.top.toDouble() * scaleY,
      rect.right.toDouble() * scaleX,
      rect.bottom.toDouble() * scaleY,
    );
  }


  @override
  bool shouldRepaint(FaceContourPainter oldDelegate) {
    return imageSize != oldDelegate.imageSize || faces != oldDelegate.faces;
  }
}