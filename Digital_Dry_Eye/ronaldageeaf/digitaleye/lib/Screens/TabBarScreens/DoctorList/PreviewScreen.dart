import 'dart:io';
import 'dart:typed_data';

import 'package:digitaleye/Helper/CommonWidgets/CommonWidgets.dart';
import 'package:digitaleye/Helper/Constant.dart';
import 'package:digitaleye/Screens/TabBarScreens/DoctorList/camera_preview_scanner.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:math';
import 'dart:ui' show lerpDouble;
import 'dart:ui';

import 'package:digitaleye/Screens/contanst/contanst.dart';
import 'package:google_ml_vision/google_ml_vision.dart';
import 'package:flutter/foundation.dart';
import 'package:camera/camera.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'colors.dart';
import 'scanner_utils.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'dart:ui' as ui;
import 'package:firebase_core/firebase_core.dart' as firebase_core;
import 'package:syncfusion_flutter_gauges/gauges.dart';
import 'package:digitaleye/pages/result.dart' as R;
import 'package:image/image.dart' as IMG;

class PreviewScreen extends StatefulWidget {
  final String imgPath;
  final String fileName;
  PreviewScreen({this.imgPath, this.fileName});


  @override
  _PreviewScreenState createState() => _PreviewScreenState();
}


class _PreviewScreenState extends State<PreviewScreen> {

  double _progressValue;
  bool _loading;

  @override
  void initState() {
    super.initState();
    _loading = false;
    _progressValue = 0.0;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          // title: setHeaderTitle(AppTranslations.of(context).text(AppTitle.appTitle),Colors.white),
          title: setHeaderTitle("Results", Colors.white),
          backgroundColor: AppColor.themeColor,
          elevation: 1.0,
          automaticallyImplyLeading: false,
        ),

        body:_loading
            ? Center(child: CircularProgressIndicator())
            : Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Expanded(
                flex: 2,
                child: Image.file(File(widget.imgPath),fit: BoxFit.contain,),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  width: double.infinity,
                  height: 60,
                  color: Colors.black,
                  child: Center(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[

                        Expanded(
                            child: FlatButton(
                              onPressed: () {
                                Navigator.pop(context);
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => CameraPreviewScanner()));
                                },
                              child: Text(
                                "RETAKE",
                                style: TextStyle(
                                  color:AppColor.themeColor,
                                ),
                              ),
                            )),
                        Expanded(
                          child: RaisedButton(
                            onPressed: (){
                              handleTaskExample2(widget.imgPath)
                                  .then((value) => Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => R.Results(),
                                    //   builder: (context) =>R.Results(imgPath: _barcodePictureFilePath,fileName: '$timestamp.jpg',)
                                  )));
                            },
                            color: AppColor.themeColor,
                            elevation: 4.0,
                            child: Text(
                              "Submit",
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        )
    );
  }



  Future<void> handleTaskExample2(String filePath) async {
    print("hello");
    File largeFile = File(filePath);
    _loading = true;

    final String timestamp = DateTime.now().millisecondsSinceEpoch.toString();

    firebase_storage.UploadTask task = firebase_storage.FirebaseStorage.instance
        .ref()
        .child(RouterName.id)
        .child('$timestamp.jpg')
        .putFile(largeFile);

    task.snapshotEvents.listen((firebase_storage.TaskSnapshot snapshot) {
      print('Task state: ${snapshot.state}');
      print(
          'Progress: ${(snapshot.bytesTransferred / snapshot.totalBytes) * 100} %');

      setState(() {

        while(_progressValue<((snapshot.bytesTransferred / snapshot.totalBytes) * 100)
            .toDouble()){
          _progressValue++;
        }
        print(_progressValue);

      });
    }, onError: (e) {
      // The final snapshot is also available on the task via `.snapshot`,
      // this can include 2 additional states, `TaskState.error` & `TaskState.canceled`
      print(task.snapshot);

      if (e.code == 'permission-denied') {
        print('User does not have permission to upload to this reference.');
      }
    });

    // We can still optionally use the Future alongside the stream.
    try {
      await task;
      print('Upload complete.');
      _loading = false;

    } on firebase_core.FirebaseException catch (e) {
      if (e.code == 'permission-denied') {
        print('User does not have permission to upload to this reference.');
      }
      // ...
    }
  }





  Future getBytes () async {
    Uint8List bytes = File(widget.imgPath).readAsBytesSync() as Uint8List;
    return ByteData.view(bytes.buffer);
  }
}