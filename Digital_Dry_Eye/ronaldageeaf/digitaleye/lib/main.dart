import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui';

import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';
import 'package:rxdart/subjects.dart';
import 'package:timezone/data/latest.dart' as tz;
import 'package:timezone/timezone.dart' as tz;
import 'dart:io';

import 'package:digitaleye/pages/add_question.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:redux/redux.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'Helper/CommonWidgets/CommonWidgets.dart';
import 'Localization/app_translations.dart';
import 'Screens/IndicatorList/home_page.dart';
import 'Screens/Notifications/store/AppState.dart';
import 'Screens/Notifications/store/store.dart';
import 'Screens/Notifications/utils/notificationHelper.dart';
import 'dart:async' show Future;
import 'dart:io' show File;
import 'dart:math';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart'
    show getApplicationDocumentsDirectory;

import 'package:flutter/cupertino.dart';
import 'Localization/app_translations_delegate.dart';
import 'Notifications/notification_service.dart';
import 'Notifications/schedule_notifications.dart';
import 'Screens/TabBarScreens/Tabbar/Tabbar.dart';
import 'Screens/create_account_height/create_account_height_page.dart';
import 'globals.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'Helper/Constant.dart';
import 'Helper/SharedManager.dart';
import 'Localization/application.dart';
import 'Screens/LoginPage/LoginPage.dart';
import 'Screens/TabBarScreens/DoctorList/camera_preview_scanner.dart';
import 'Screens/contanst/contanst.dart';
import 'Screens/create_account/create_account_page.dart';
import 'Screens/create_account_birthday/create_account_birthday_page.dart';
import 'Screens/create_account_fullname/create_account_fullname_page.dart';
import 'Screens/create_account_gender/create_account_gender_page.dart';
import 'dart:async';
import 'dart:ui';
import 'package:path_provider/path_provider.dart';

import 'Screens/Notifications/utils/notificationHelper.dart';

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
FlutterLocalNotificationsPlugin();

/// Streams are created so that app can respond to notification-related events
/// since the plugin is initialised in the `main` function
final BehaviorSubject<ReceivedNotification> didReceiveLocalNotificationSubject =
BehaviorSubject<ReceivedNotification>();

final BehaviorSubject<String> selectNotificationSubject =
BehaviorSubject<String>();

const MethodChannel platform =
MethodChannel('dexterx.dev/flutter_local_notifications_example');

class ReceivedNotification {
  ReceivedNotification({
    @required this.id,
    @required this.title,
    @required this.body,
    @required this.payload,
  });

  final int id;
  final String title;
  final String body;
  final String payload;
}

String selectedNotificationPayload;

/// IMPORTANT: running the following code on its own won't work as there is
/// setup required for each platform head project.
///
/// Please download the complete example app from the GitHub repository where
/// all the setup has been done
Future<void> main() async {
  // needed if you intend to initialize in the `main` function
  WidgetsFlutterBinding.ensureInitialized();

  await _configureLocalTimeZone();

  final NotificationAppLaunchDetails notificationAppLaunchDetails =
  await flutterLocalNotificationsPlugin.getNotificationAppLaunchDetails();
  String initialRoute = HomePage.routeName;
  if (notificationAppLaunchDetails?.didNotificationLaunchApp ?? false) {
    selectedNotificationPayload = notificationAppLaunchDetails.payload;
    initialRoute = SecondPage.routeName;
  }

  const AndroidInitializationSettings initializationSettingsAndroid =
  AndroidInitializationSettings('app_icon');

  /// Note: permissions aren't requested here just to demonstrate that can be
  /// done later
  final IOSInitializationSettings initializationSettingsIOS =
  IOSInitializationSettings(
      requestAlertPermission: false,
      requestBadgePermission: false,
      requestSoundPermission: false,
      onDidReceiveLocalNotification:
          (int id, String title, String body, String payload) async {
        didReceiveLocalNotificationSubject.add(ReceivedNotification(
            id: id, title: title, body: body, payload: payload));
      });
  const MacOSInitializationSettings initializationSettingsMacOS =
  MacOSInitializationSettings(
      requestAlertPermission: false,
      requestBadgePermission: false,
      requestSoundPermission: false);
  final InitializationSettings initializationSettings = InitializationSettings(
      android: initializationSettingsAndroid,
      iOS: initializationSettingsIOS,
      macOS: initializationSettingsMacOS);
  await flutterLocalNotificationsPlugin.initialize(initializationSettings,
      onSelectNotification: (String payload) async {
        if (payload != null) {
          debugPrint('notification payload: $payload');
        }
        selectedNotificationPayload = payload;
        selectNotificationSubject.add(payload);
      });
  SharedPreferences.getInstance().then((prefs) async {
    await SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
    prefrenceObjects = prefs;
    runApp(
      MaterialApp(
          debugShowCheckedModeBanner: false,
        initialRoute: initialRoute,
        routes: <String, WidgetBuilder>{
          HomePage.routeName: (_) => LaunchingApp(notificationAppLaunchDetails),
          SecondPage.routeName: (_) => LaunchingApp(notificationAppLaunchDetails)
        },
      ),
    );
  });





}

Future<void> _configureLocalTimeZone() async {
  final timeZone = TimeZone();

  tz.initializeTimeZones();
  // String timeZoneName = await platform.invokeMethod('getTimeZoneName');
  final String timeZoneName = await timeZone.getTimeZoneName();

  // Find the 'current location'
  final location = await timeZone.getLocation(timeZoneName);

  tz.setLocalLocation(tz.getLocation(timeZoneName));
}

class PaddedRaisedButton extends StatelessWidget {
  const PaddedRaisedButton({
    @required this.buttonText,
    @required this.onPressed,
    Key key,
  }) : super(key: key);

  final String buttonText;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) => Padding(
    padding: const EdgeInsets.fromLTRB(0, 0, 0, 8),
    child: RaisedButton(
      onPressed: onPressed,
      child: Text(buttonText),
    ),
  );
}

class LaunchingApp extends StatelessWidget {
  final NotificationAppLaunchDetails notificationAppLaunchDetails;

  const LaunchingApp(
      this.notificationAppLaunchDetails, {
        Key key,
      }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
    return FutureBuilder(
      // Initialize FlutterFire
      future: Firebase.initializeApp(),
      builder: (context, snapshot) {
        // Check for errors
        if (snapshot.hasError) {
          print("error");
        }
        if (snapshot.connectionState == ConnectionState.done) {
          return MaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'Digital Eye APP',
            theme: ThemeData(
              primarySwatch: Colors.blue,
            ),
            home: new HomePage(notificationAppLaunchDetails),
          );
        }
        return CircularProgressIndicator();
      },
    );
  }
}


class HomePage extends StatefulWidget {
  const HomePage(
      this.notificationAppLaunchDetails, {
        Key key,
      }) : super(key: key);

  static const String routeName = '/';

  final NotificationAppLaunchDetails notificationAppLaunchDetails;

  bool get didNotificationLaunchApp =>
      notificationAppLaunchDetails?.didNotificationLaunchApp ?? false;


  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  AppTranslationsDelegate _newLocaleDelegate;
  final _controller = new PageController();
  static const _kDuration = const Duration(milliseconds: 300);
  static const _kCurve = Curves.ease;
  final _kArrowColor = Colors.black;
  final List<Widget> _pages = <Widget>[LoginPage()];

  @override
  void initState() {
    super.initState();
    _newLocaleDelegate = AppTranslationsDelegate(newLocale: null);
    application.onLocaleChanged = onLocaleChange;
    _requestPermissions();
    _configureDidReceiveLocalNotificationSubject();
    _configureSelectNotificationSubject();
  }

  void _requestPermissions() {
    flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
        IOSFlutterLocalNotificationsPlugin>()
        ?.requestPermissions(
      alert: true,
      badge: true,
      sound: true,
    );
    flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
        MacOSFlutterLocalNotificationsPlugin>()
        ?.requestPermissions(
      alert: true,
      badge: true,
      sound: true,
    );
  }

  void _configureDidReceiveLocalNotificationSubject() {
    didReceiveLocalNotificationSubject.stream
        .listen((ReceivedNotification receivedNotification) async {
      await showDialog(
        context: context,
        builder: (BuildContext context) => CupertinoAlertDialog(
          title: receivedNotification.title != null
              ? Text(receivedNotification.title)
              : null,
          content: receivedNotification.body != null
              ? Text(receivedNotification.body)
              : null,
          actions: <Widget>[
            CupertinoDialogAction(
              isDefaultAction: true,
              onPressed: () async {
                Navigator.of(context, rootNavigator: true).pop();
                await Navigator.push(
                  context,
                  MaterialPageRoute<void>(
                    builder: (BuildContext context) =>
                        SecondPage(receivedNotification.payload),
                  ),
                );
              },
              child: const Text('Ok'),
            )
          ],
        ),
      );
    });
  }

  void _configureSelectNotificationSubject() {
    selectNotificationSubject.stream.listen((String payload) async {
      await Navigator.pushNamed(context, '/secondPage');
    });
  }

  @override
  void dispose() {
    didReceiveLocalNotificationSubject.close();
    selectNotificationSubject.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    _enableNotification();

    return new MaterialApp(
        debugShowCheckedModeBanner: false,
        home: new Scaffold(
          body: new IconTheme(
            data: new IconThemeData(color: _kArrowColor),
            child: SharedManager.shared.isOnboarding
                ? LoginPage()
                : Container(
              color: Colors.white,
              child: new Stack(
                children: <Widget>[
                  new PageView.builder(
                    physics: new AlwaysScrollableScrollPhysics(),
                    controller: _controller,
                    itemCount: _pages.length,
                    itemBuilder: (BuildContext context, int index) {
                      return _pages[index % _pages.length];
                    },
                  ),
                  new Positioned(
                    bottom: 0.0,
                    left: 0.0,
                    right: 0.0,
                    child: new Container(
                      color: AppColor.themeColor.withOpacity(0.0),
                      padding: const EdgeInsets.all(20.0),
                      child: new Center(
                        child: new DotsIndicator(
                          controller: _controller,
                          itemCount: _pages.length,
                          color: AppColor.themeColor,
                          onPageSelected: (int page) {
                            _controller.animateToPage(
                              page,
                              duration: _kDuration,
                              curve: _kCurve,
                            );
                          },
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        localizationsDelegates: [
          _newLocaleDelegate,
          //provides localised strings
          GlobalMaterialLocalizations.delegate,
          //provides RTL support
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: [SharedManager.shared.language],
        routes: {
          '/$CameraPreviewScanner': (BuildContext context) =>
              CameraPreviewScanner(),
          RouterName.CREATE_ACCOUNT: (context) =>
              CreateAccountPage.ProviderPage(),
          RouterName.CREATE_ACCOUNT_BIRTHDAY: (context) =>
              CreateAccountBirthdayPage.ProviderPage(),
          RouterName.CREATE_ACCOUNT_GENDER: (context) =>
              CreateAccountGenderPage.ProviderPage(),
          RouterName.CREATE_ACCOUNT_FULLNAME: (context) =>
              CreateAccountFullnamePage.ProviderPage(),
          RouterName.CREATE_ACCOUNT_HEIGHT: (context) =>
              CreateAccountHightPage.ProviderPage(),

          //RouterName.CREATE_ACCOUNT_HEIGHT: (context) =>
          //   CreateAccountHeightPage.ProviderPage(),

         // '/MyHomePage': (BuildContext context) => MyHomePage(),

          '/AddQuestion': (BuildContext context) =>
          new AddQuestion(RouterName.quizId, RouterName.id.toString()),

          '/TabBarScreen': (BuildContext context) =>
          new TabBarScreen(RouterName.id.toString()),

          '/CameraPreviewScanners': (BuildContext context) =>
          new CameraPreviewScanner(),

          '/NotificationsScreen': (BuildContext context) =>
          new NotificationsScreen()




        },
        theme: SharedManager.shared.getThemeType());
  }

  //This is for localization
  void onLocaleChange(Locale locale) {
    setState(() {
      _newLocaleDelegate = AppTranslationsDelegate(newLocale: locale);
    });
  }




  Future<void> _enableNotification() async {
    await _scheduleDailyTenAMNotification();
    await _scheduleNightNotification();
    await  _scheduleDailyMorningNotification();
  }

  Future<void> _cancelNotification() async {
    await flutterLocalNotificationsPlugin.cancel(0);
  }


  Future<void> _checkPendingNotificationRequests() async {
    final List<PendingNotificationRequest> pendingNotificationRequests =
    await flutterLocalNotificationsPlugin.pendingNotificationRequests();
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        content:
        Text('${pendingNotificationRequests.length} pending notification '
            'requests'),
        actions: <Widget>[
          FlatButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: const Text('OK'),
          ),
        ],
      ),
    );
  }

  Future<void> _cancelAllNotifications() async {
    await flutterLocalNotificationsPlugin.cancelAll();
  }


  Future<void> _scheduleDailyMorningNotification() async {
    await flutterLocalNotificationsPlugin.zonedSchedule(
        0,
        'Questionnaire',
        'Your next Questionnaire awaits!',
        _nextInstanceOfAM(),
        const NotificationDetails(
          android: AndroidNotificationDetails(
              'daily notification channel id',
              'daily notification channel name',
              'daily notification description'),
        ),
        androidAllowWhileIdle: true,
        uiLocalNotificationDateInterpretation:
        UILocalNotificationDateInterpretation.absoluteTime,
        matchDateTimeComponents: DateTimeComponents.time);
  }


  Future<void> _scheduleDailyTenAMNotification() async {
    await flutterLocalNotificationsPlugin.zonedSchedule(
        0,
        'Questionnaire',
        'Your next Questionnaire awaits!',
        _nextInstanceOfTenAM(),
        const NotificationDetails(
          android: AndroidNotificationDetails(
              'daily notification channel id',
              'daily notification channel name',
              'daily notification description'),
        ),
        androidAllowWhileIdle: true,
        uiLocalNotificationDateInterpretation:
        UILocalNotificationDateInterpretation.absoluteTime,
        matchDateTimeComponents: DateTimeComponents.time);
  }


  Future<void> _scheduleNightNotification() async {
    await flutterLocalNotificationsPlugin.zonedSchedule(
        1,
        'Questionnaire',
        'Your next Questionnaire awaits!',
        _nextInstanceOfNight(),
        const NotificationDetails(
          android: AndroidNotificationDetails(
              'daily notification channel id',
              'daily notification channel name',
              'daily notification description'),
        ),
        androidAllowWhileIdle: true,
        uiLocalNotificationDateInterpretation:
        UILocalNotificationDateInterpretation.absoluteTime,
        matchDateTimeComponents: DateTimeComponents.time);
  }

  tz.TZDateTime _nextInstanceOfTenAM() {
    final tz.TZDateTime now = tz.TZDateTime.now(tz.local);
    tz.TZDateTime scheduledDate =
    tz.TZDateTime(tz.local, now.year, now.month, now.day, 14);
    if (scheduledDate.isBefore(now)) {
      scheduledDate = scheduledDate.add(const Duration(days: 1));
    }
    return scheduledDate;
  }

  tz.TZDateTime _nextInstanceOfAM() {
    final tz.TZDateTime now = tz.TZDateTime.now(tz.local);
    tz.TZDateTime scheduledDate =
    tz.TZDateTime(tz.local, now.year, now.month, now.day, 9);
    if (scheduledDate.isBefore(now)) {
      scheduledDate = scheduledDate.add(const Duration(days: 1));
    }
    return scheduledDate;
  }

  tz.TZDateTime _nextInstanceOfNight() {
    final tz.TZDateTime now = tz.TZDateTime.now(tz.local);
    tz.TZDateTime scheduledDate =
    tz.TZDateTime(tz.local, now.year, now.month, now.day, 19);
    if (scheduledDate.isBefore(now)) {
      scheduledDate = scheduledDate.add(const Duration(days: 1));
    }
    return scheduledDate;
  }


}

class NotificationsScreen extends StatefulWidget {

  @override
  _NotificationsScreenState createState() => _NotificationsScreenState();
}

class _NotificationsScreenState extends State<NotificationsScreen> {

  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
        debugShowCheckedModeBanner:false,
        home: Scaffold(
          appBar: new AppBar(
            centerTitle: true,
            // title: setHeaderTitle(AppTranslations.of(context).text(AppTitle.appTitle),Colors.white),
            title: setHeaderTitle(AppTranslations.of(context).text(AppTitle.notification),Colors.white),
            backgroundColor: AppColor.themeColor,
            elevation: 1.0,
            leading: new IconButton(
              icon: Icon(Icons.arrow_back_ios,color:Colors.white),
              onPressed: (){
                Navigator.of(context).pop();
              },
            ),
          ),
          body:  SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(8),
              child: Center(
                child: Column(
                  children: <Widget>[
                    const Padding(
                      padding: EdgeInsets.fromLTRB(0, 0, 0, 8),
                      child: Text(''),
                    ),

                    InkWell(
                      child: new Text(
                        "Notification Settings",
                        textDirection: SharedManager.shared.direction,
                        style: new TextStyle(
                            color: AppColor.themeColor,
                            fontSize: 14,
                            fontWeight: FontWeight.w600),
                      ),
                      onTap: () async {

                      },
                    ),

                    InkWell(
                      child: new Container(
                        height: 60,
                        width: MediaQuery.of(context).size.width / 1.5,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: new Material(
                            color: AppColor.themeColor,
                            borderRadius: BorderRadius.circular(20),
                            elevation: 5.0,
                            child: new Center(
                              child: new Text(
                                "Turn on Notifications",
                                textDirection: SharedManager.shared.direction,
                                style: new TextStyle(
                                    color: Colors.white,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w500),
                              ),
                            ),
                          ),
                        ),
                      ),
                      onTap: () async {
                        await _scheduleDailyTenAMNotification();
                        await _scheduleNightNotification();
                      }
                    ),

                    InkWell(
                        child: new Container(
                          height: 60,
                          width: MediaQuery.of(context).size.width / 1.5,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: new Material(
                              color: AppColor.themeColor,
                              borderRadius: BorderRadius.circular(20),
                              elevation: 5.0,
                              child: new Center(
                                child: new Text(
                                  "Check pending Notifications",
                                  textDirection: SharedManager.shared.direction,
                                  style: new TextStyle(
                                      color: Colors.white,
                                      fontSize: 12,
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                            ),
                          ),
                        ),
                        onTap: () async {
                          await _checkPendingNotificationRequests();
                        }
                    ),

                    InkWell(
                        child: new Container(
                          height: 60,
                          width: MediaQuery.of(context).size.width / 1.5,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: new Material(
                              color: AppColor.themeColor,
                              borderRadius: BorderRadius.circular(20),
                              elevation: 5.0,
                              child: new Center(
                                child: new Text(
                                  "Cancel all Notifications",
                                  textDirection: SharedManager.shared.direction,
                                  style: new TextStyle(
                                      color: Colors.white,
                                      fontSize: 12,
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                            ),
                          ),
                        ),
                        onTap: () async {
                          await _cancelAllNotifications();
                        }
                    ),

                  ],
                ),
              ),
            ),
          ),
        ),
        theme: SharedManager.shared.getThemeType(),
        localizationsDelegates: [
          //provides localised strings
          GlobalMaterialLocalizations.delegate,
          //provides RTL support
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: [
          SharedManager.shared.language
        ],
      );
  }


  Future<void> _cancelNotification() async {
    await flutterLocalNotificationsPlugin.cancel(0);
  }


  Future<void> _checkPendingNotificationRequests() async {
    final List<PendingNotificationRequest> pendingNotificationRequests =
    await flutterLocalNotificationsPlugin.pendingNotificationRequests();
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        content:
        Text('${pendingNotificationRequests.length} pending notification '
            'requests'),
        actions: <Widget>[
          FlatButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: const Text('OK'),
          ),
        ],
      ),
    );
  }

  Future<void> _cancelAllNotifications() async {
    await flutterLocalNotificationsPlugin.cancelAll();
  }


  Future<void> _scheduleDailyTenAMNotification() async {
    await flutterLocalNotificationsPlugin.zonedSchedule(
        0,
        'Questionnaire',
        'Your next Questionnaire awaits!',
        _nextInstanceOfTenAM(),
        const NotificationDetails(
          android: AndroidNotificationDetails(
              'daily notification channel id',
              'daily notification channel name',
              'daily notification description'),
        ),
        androidAllowWhileIdle: true,
        uiLocalNotificationDateInterpretation:
        UILocalNotificationDateInterpretation.absoluteTime,
        matchDateTimeComponents: DateTimeComponents.time);
  }


  Future<void> _scheduleNightNotification() async {
    await flutterLocalNotificationsPlugin.zonedSchedule(
        1,
        'Questionnaire',
        'Your next Questionnaire awaits!',
        _nextInstanceOfNight(),
        const NotificationDetails(
          android: AndroidNotificationDetails(
              'daily notification channel id',
              'daily notification channel name',
              'daily notification description'),
        ),
        androidAllowWhileIdle: true,
        uiLocalNotificationDateInterpretation:
        UILocalNotificationDateInterpretation.absoluteTime,
        matchDateTimeComponents: DateTimeComponents.time);
  }

  tz.TZDateTime _nextInstanceOfTenAM() {
    final tz.TZDateTime now = tz.TZDateTime.now(tz.local);
    tz.TZDateTime scheduledDate =
    tz.TZDateTime(tz.local, now.year, now.month, now.day, 14);
    if (scheduledDate.isBefore(now)) {
      scheduledDate = scheduledDate.add(const Duration(days: 1));
    }
    return scheduledDate;
  }

  tz.TZDateTime _nextInstanceOfNight() {
    final tz.TZDateTime now = tz.TZDateTime.now(tz.local);
    tz.TZDateTime scheduledDate =
    tz.TZDateTime(tz.local, now.year, now.month, now.day, 20);
    if (scheduledDate.isBefore(now)) {
      scheduledDate = scheduledDate.add(const Duration(days: 1));
    }
    return scheduledDate;
  }

}

class SecondPage extends StatefulWidget {
  const SecondPage(
      this.payload, {
        Key key,
      }) : super(key: key);

  static const String routeName = '/secondPage';

  final String payload;

  @override
  State<StatefulWidget> createState() => SecondPageState();
}


class DotsIndicator extends AnimatedWidget {
  DotsIndicator({
    this.controller,
    this.itemCount,
    this.onPageSelected,
    this.color: Colors.white,
  }) : super(listenable: controller);

  /// The PageController that this DotsIndicator is representing.
  final PageController controller;

  /// The number of items managed by the PageController
  final int itemCount;

  /// Called when a dot is tapped
  final ValueChanged<int> onPageSelected;

  /// The color of the dots.
  ///
  /// Defaults to `Colors.white`.
  final Color color;

  // The base size of the dots
  static const double _kDotSize = 8.0;

  // The increase in the size of the selected dot
  static const double _kMaxZoom = 2.0;

  // The distance between the center of each dot
  static const double _kDotSpacing = 25.0;

  Widget _buildDot(int index) {
    double selectedness = Curves.easeOut.transform(
      max(
        0.0,
        1.0 - ((controller.page ?? controller.initialPage) - index).abs(),
      ),
    );
    double zoom = 1.0 + (_kMaxZoom - 1.0) * selectedness;
    return new Container(

      width: _kDotSpacing,
      child: new Center(
        child: new Material(
          color: color,
          type: MaterialType.circle,
          child: new Container(
            width: _kDotSize * zoom,
            height: _kDotSize * zoom,
            child: new InkWell(
              onTap: () => onPageSelected(index),
            ),
          ),
        ),
      ),
    );
  }

  Widget build(BuildContext context) {
    return new Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: new List<Widget>.generate(itemCount, _buildDot),
    );
  }
}

class SecondPageState extends State<SecondPage> {
  String _payload;
  @override
  void initState() {
    super.initState();
    _payload = widget.payload;
  }

  @override
  Widget build(BuildContext context) => Scaffold(

    appBar: AppBar(
      title: Text('Second Screen with payload: ${_payload ?? ''}'),

    ),

    body: Center(
      child: RaisedButton(
        onPressed: () {
          Navigator.pop(context);
        },
        child: const Text('Go back!'),
      ),
    ),
  );
}