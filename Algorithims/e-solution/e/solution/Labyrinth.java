package e.solution;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Labyrinth {

	private static int C, R, L;
	static ArrayList<String> list = new ArrayList<>();
	static ArrayList<String> steps = new ArrayList<>();

	/**
	 * Driver Method Reads Input from console as explained in the post
	 *
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {

		Scanner sc = new Scanner(System.in);
		System.out.println("Bitte geben Sie Ihr Labyrinth ein oder tippen Sie 'q', um das Programm zu verlassen");

		while (sc.findInLine("0 0 0") == null) {

			L = sc.nextInt();
			R = sc.nextInt();
			C = sc.nextInt();
			// System.out.println(X + " " + Y + " " + Z);
			sc.nextLine();

			Cell[][][] maze = new Cell[C][R][L];
			int sx = -1, sy = -1, sz = -1;
			for (int z = 0; z < L; z++) {
				for (int y = 0; y < R; y++) {
					String line = sc.nextLine();
					// System.out.println(line);
					if (line.length() != C)
						throw new Exception(
								"Fehler beim Lesen Ihres Labyrinths, bitte geben Sie das Format des Labyrinths richtig eingegeben haben");
					for (int x = 0; x < C; x++) {
						char c = line.charAt(x);
						if (c == 'S') {
							sx = x;
							sy = y;
							sz = z;
						}
						maze[x][y][z] = new Cell(c, new int[] { x, y, z });
					}
				}
				if (sc.hasNextLine())
					sc.nextLine();
			}
			// System.out.println(sx + " " + sy + " " + sz);
			solve(maze, sx, sy, sz);
		} // mainq
		list.forEach(item -> System.out.println(item));
		sc.close();
	}
	// -----------------------------------------------------------------------------------------------

	/**
	 * Searches for the Destination cell in a BFS fashion
	 *
	 * @param matrix 3D matrix where we need to find the destination cell from
	 *               source
	 * @param x      source x location
	 * @param y      source y location
	 * @param z      source z location
	 * @throws Exception
	 */
	public static void solve(Cell[][][] matrix, int x, int y, int z) throws Exception {
		// delta values to find neighboring cells of current cell
		int[][] delta = new int[][] { { -1, 0, 0 }, // negative X
				{ 1, 0, 0 }, // positive X
				{ 0, -1, 0 }, // negative Y
				{ 0, 1, 0 }, // positive Y
				{ 0, 0, -1 }, // negative Z
				{ 0, 0, 1 } }; // positive Z
		Queue<Cell> queue = new LinkedList<Cell>();

		Cell source = matrix[x][y][z];
		queue.offer(source);
		String value = "Gefangen :-(";
		while (!queue.isEmpty()) {

			Cell parent = queue.poll();

			for (int i = 0; i < delta.length; i++) {
				// location of neighbor cell
				int nx = parent.getLocation()[0] + delta[i][0];
				int ny = parent.getLocation()[1] + delta[i][1];
				int nz = parent.getLocation()[2] + delta[i][2];

				if (isValidLocation(nx, ny, nz)) {

					Cell neighbor = matrix[nx][ny][nz];

					if (neighbor.ispath() || neighbor.isObstacle() || neighbor.isSource() || neighbor.isDestination()) {
						// System.out.println("Matrix is Valid");
					} else {
						throw new Exception(
								"Fehler beim Lesen Ihres Labyrinths, bitte geben Sie das Format des Labyrinths richtig eingegeben haben");
					}

					if (!neighbor.isVisited() && !neighbor.isObstacle()) {
						neighbor.setParent(parent);
						neighbor.setVisited();

						// System.out.println(neighbor);
						neighbor.setMotion(getMotion(parent, neighbor));

						if (neighbor.isDestination()) {

							printResult(neighbor, value);
							return;
						} else {
							queue.offer(neighbor);
						}
					}

				}
			}

		}
		list.add(value);

	} // solve

	// ---------------------------------------------------------------------------------------------------

	/**
	 * Motion required to move from Source cell to Destination Cell
	 * 
	 * @param source      Source Cell
	 * @param destination Destination Cell
	 * @return X+ if the motion needed is positive unit distance in X direction X-
	 *         if the motion needed is negative unit distance in X direction Y+ if
	 *         the motion needed is positive unit distance in Y direction Y- if the
	 *         motion needed is negative unit distance in Y direction Z+ if the
	 *         motion needed is positive unit distance in Z direction Z- if the
	 *         motion needed is negative unit distance in Z direction
	 * @throws Exception
	 */

	private static String getMotion(Cell source, Cell destination) throws Exception {

		int dx = destination.getLocation()[0] - source.getLocation()[0];
		int dy = destination.getLocation()[1] - source.getLocation()[1];
		int dz = destination.getLocation()[2] - source.getLocation()[2];

		if (dx == 1 && dy == 0 && dz == 0) {

			return "Steps: Right, ";
		}
		if (dx == -1 && dy == 0 && dz == 0) {

			return "Steps: Left, ";
		}
		if (dy == 1 && dx == 0 && dz == 0) {

			return "Steps: Southsss, ";
		}
		if (dy == -1 && dx == 0 && dz == 0) {

			return "Steps: North, ";
		}
		if (dz == 1 && dy == 0 && dx == 0) {

			return "Steps: Down, ";
		}
		if (dz == -1 && dy == 0 && dx == 0) {

			return "Steps: Up, ";
		}
		throw new Exception("getMotion: Something is wrong");
	} // getMotion

	// --------------------------------------------------------------------------------------------------

	/**
	 * Print the result once the destination cell is found This is Recursive
	 * implementation
	 * 
	 * @param dest Destination Cell
	 */
	public static void printResult(Cell dest, String value) {
		if (dest.isSource()) {
			list.add(value);
			return;
		}
		steps.add("e");
		value = "Entkommen in " + steps.size() + " minuten";
		printResult(dest.getParent(), value);
	} // printResult

	// --------------------------------------------------------------------------------------------------

	/**
	 * Determine is the location represented by inputs is a valid location in the
	 * given input matrix
	 * 
	 * @param x x location
	 * @param y y location
	 * @param z z location
	 * @return True if the location is within the matrix else False
	 */
	public static boolean isValidLocation(int x, int y, int z) {
		return !(x < 0 || x >= C || y < 0 || y >= R || z < 0 || z >= L);
	} // isValidLocation

	// ---------------------------------------------------------------------------------------------------

} //

//====================================================================================================

