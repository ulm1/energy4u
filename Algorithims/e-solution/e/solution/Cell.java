package e.solution;
/**
 * Class representing a Cell in a Matrix
 */
public class Cell {
	private int[] location;
	private char c;
	private boolean visited;
	private Cell parent;
	private String motion;

	/**
	 * Constructor
	 * 
	 * @param c        character in the cell
	 * @param location [x,y,z] location triplet
	 */
	public Cell(char c, int[] location) {
		this.c = c;
		this.location = location;
		this.visited = false;
		this.parent = null;
	} // Cell

	// -------------------------------------------------------------------------------------------------

	/**
	 * Sets the cell as visited
	 */

	int cd = 0;

	public void setVisited() {

		visited = true;
	} // setVisited

	// -------------------------------------------------------------------------------------------------

	/**
	 * Sets parent of the cell during the search to a destination via shortest path
	 * Parent is the parent of this cell in the path
	 * 
	 * @param parent
	 */
	public void setParent(Cell parent) {
		this.parent = parent;
	} // setParent

	// -------------------------------------------------------------------------------------------------

	/**
	 * Determine whether the cell is visited
	 * 
	 * @return true if the cell is visited, else false
	 */
	public boolean isVisited() {
		return this.visited;
	} // isVisited

	// -------------------------------------------------------------------------------------------------

	/**
	 * Get parent of this cell in the path, if already set
	 * 
	 * @return the parent cell if it has been set, else it will be null
	 */
	public Cell getParent() {
		return this.parent;
	} // getParent

	// -------------------------------------------------------------------------------------------------

	/**
	 * Determine if this cell is a destination cell
	 * 
	 * @return True if this cell is a destination cell, else false
	 */
	public boolean isDestination() {
		return this.c == 'E';
	} // isDestination

	// -------------------------------------------------------------------------------------------------

	/**
	 * Determine if this cell is a path cell
	 * 
	 * @return True if this cell is a destination cell, else false
	 */
	public boolean ispath() {
		return this.c == '.';
	} // isDestination

	// -------------------------------------------------------------------------------------------------

	/**
	 * Determine if this cell is an obstacle
	 * 
	 * @return True if this cell is an obstacle, false otherwise
	 */
	public boolean isObstacle() {
		return this.c == '#';
	} // isObstacle

	// -------------------------------------------------------------------------------------------------

	/**
	 * Determine if this cell is a source
	 * 
	 * @return True if this cell is source, false otherwise
	 */
	public boolean isSource() {
		return this.c == 'S';
	} // isSource

	// -------------------------------------------------------------------------------------------------

	/**
	 * Get the motion which is required to reach this cell from the parent cell in
	 * the shortest path
	 * 
	 * @return the motion X+/X-/Y+/Y-/Z+/Z-
	 */
	public String getMotion() {
		return this.motion;
	} // getMotion

	// -------------------------------------------------------------------------------------------------

	/**
	 * Sest the motion to reach this cell from the parent cell
	 * 
	 * @param motion the motion X+/X-/Y+/Y-/Z+/Z-
	 */

	public void setMotion(String motion) {

		this.motion = motion;
	} // setMotion

	// -------------------------------------------------------------------------------------------------

	/**
	 * get the x, y, z location triplet of this cell in the input matrix
	 * 
	 * @return
	 */
	public int[] getLocation() {

		return this.location;
	} // getLocation

	// -------------------------------------------------------------------------------------------------

	/**
	 * String representation of this cell Written for debug pupose
	 * 
	 * @return
	 */
	public String toStringss() {
		return this.c + " " + this.visited + " " + (this.parent != null ? this.parent.c : null);
	} // toString

	// -------------------------------------------------------------------------------------------------
}