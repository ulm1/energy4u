
package e.solution;
import java.util.Scanner;
import java.math.BigInteger;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Fibonacci {

	/*
	 * The most effecient way is to use a Hashmap for the most optimized
	 * performence. The choice of HashMap is due to Hashes being (most of the time)
	 * O(1), which is pretty quick, it doesn't need to iterate to find a value The
	 * Arraylist to store all the users output values for the calculated fib value
	 */
	private Map<Integer, BigInteger> memory = new HashMap<>();
	static ArrayList<String> list = new ArrayList<>();

	/*
	 * The reason for using BigInteger instead of double is precision. double has
	 * limited precision, so large integers can't be represented accurately.
	 */
	public BigInteger fibonacii(int n) {

		if (memory.get(n) != null) {
			return memory.get(n);
		}
		if (n == 0) {
			return BigInteger.valueOf(0);
		}
		if (n <= 2 && n > 0) {
			return BigInteger.valueOf(1);
		}
		BigInteger fib = fibonacii(n - 1).add(fibonacii(n - 2));

		memory.put(n, fib);

		return fib;
	}

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Bitte geben Sie Ihre Fibonacci Zahl ein");

		int n = 0;
		if (!sc.hasNextInt()) {
			System.out.println("Bitte nur ganzzahlige Werte eingeben");
			System.out.println("Bitte versuchen Sie es erneut");

			sc.nextLine();

		}
		
		String line = null;
		while (sc.findInLine("(?=\\S)") != null) {
			try {

				
				
				n = sc.nextInt();
				// System.out.println(X + " " + Y + " " + Z);
				sc.nextLine();
				Long startTime = Instant.now().toEpochMilli();
				
				if (n > 500)
					throw new Exception(
							"Fehler : Bitte nur werte kleiner gleich 500 als eingabe, Starten Sie das Programm erneut");
				
				BigInteger finResult = new Fibonacci().fibonacii(n);
				/*
				 * calculates the execution time for test purposes stores the user value in a
				 * list and displays the list as soon as the user presses enter
				 */
				Long endTime = Instant.now().toEpochMilli();
				list.add("Die Fibonacci Zahl f�r " + n + " is " + finResult + " execution time " + (endTime - startTime)
						+ " ms");
				startTime = Instant.now().toEpochMilli();
				finResult = new Fibonacci().fibonacii(n);
				endTime = Instant.now().toEpochMilli();

			} catch (Exception e) {
				System.out.println("Bitte nur ganzzahlige Werte eingeben, Bitte versuchen Sie es erneut");
				return;
			}

		}
		list.forEach(item -> System.out.println(item));
		sc.close();

	}
}