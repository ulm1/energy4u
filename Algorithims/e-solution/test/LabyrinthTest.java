package test;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import java.util.logging.Logger;
import e.solution.*;

/**
 * 
 */

/**
 * @author ra7
 *
 */

public class LabyrinthTest {

	private Logger log = Logger.getLogger(this.getClass().getName());

	@Test
	public void main() {
		try {
			log.info("Starting execution of main");
			String[] args = null;
			Labyrinth labyrinth = new Labyrinth();
			labyrinth.main(args);
			assertTrue(true);

		} catch (Exception exception) {
			System.out.print("Exception in execution ofmain-" + exception);
			exception.printStackTrace();
			assertFalse(false);
		}
	}
	int C, R, L = 2;
	@Test
	public void solve() {
		try {
			log.info("Starting execution of solve");
            Cell[][][] matrix = new Cell[C][R][L];
			int x = 0;
			int y = 0;
			int z = 0;
			Labyrinth labyrinth = new Labyrinth();
			labyrinth.solve(matrix, x, y, z);
			assertTrue(true);

		} catch (Exception exception) {
			System.out.print("Exception in execution ofsolve-" + exception);
			exception.printStackTrace();
			assertFalse(false);
		}
	}
	//Test to see if a current cell is a destination

	@Test
	public void isDestination() {
		try {
			log.info("Starting execution of isDestination");
			boolean expectedValue = true;

			Cell labyrinth = new Cell('E', null);
			boolean actualValue = labyrinth.isDestination();
			log.info("Expected Value=" + expectedValue + " . Actual Value=" + actualValue);
			System.out.println("Expected Value=" + expectedValue + " . Actual Value=" + actualValue);
			assertEquals(expectedValue, actualValue);

		} catch (Exception exception) {
			System.out.print("Exception in execution of execute1GetAllLogFromFirstMovF-" + exception);
			exception.printStackTrace();
			assertFalse(false);
		}
	}
	//Test to see if a current cell is a path

	@Test
	public void ispath() {
		try {
			log.info("Starting execution of ispath");
			boolean expectedValue = true;

			Cell labyrinth = new Cell('.', null);
			boolean actualValue = labyrinth.ispath();
			log.info("Expected Value=" + expectedValue + " . Actual Value=" + actualValue);
			System.out.println("Expected Value=" + expectedValue + " . Actual Value=" + actualValue);
			assertEquals(expectedValue, actualValue);

		} catch (Exception exception) {
			System.out.print("Exception in execution of execute1GetAllLogFromFirstMovF-" + exception);
			exception.printStackTrace();
			assertFalse(false);
		}
	}
	//Test to see if a current cell is an obstacle

	@Test
	public void isObstacle() {
		try {
			log.info("Starting execution of isObstacle");
			boolean expectedValue = true;
			int[] location = { 1, 0, 0 };
			char value = '#';
			Cell labyrinth = new Cell(value, location);
			boolean actualValue = labyrinth.isObstacle();
			log.info("Expected Value=" + expectedValue + " . Actual Value=" + actualValue);
			System.out.println("Expected Value=" + expectedValue + " . Actual Value=" + actualValue);
			assertEquals(expectedValue, actualValue);

		} catch (Exception exception) {
			System.out.print("Exception in execution of execute1GetAllLogFromFirstMovF-" + exception);
			exception.printStackTrace();
			assertFalse(false);
		}
	}
	//Test to see if a current cell is the source cell

	@Test
	public void isSource() {
		try {
			log.info("Starting execution of isSource");
			boolean expectedValue = true;
			int[] location = { 1, 0, 0 };
			char value = 'S';
			Cell labyrinth = new Cell(value, location);
      		boolean actualValue = labyrinth.isSource();
			log.info("Expected Value=" + expectedValue + " . Actual Value=" + actualValue);
			System.out.println("Expected Value=" + expectedValue + " . Actual Value=" + actualValue);
			assertEquals(expectedValue, actualValue);

		} catch (Exception exception) {
			System.out.println("Exception in execution of execute1GetAllLogFromFirstMovF-" + exception);
			exception.printStackTrace();
			assertFalse(false);
		}
	}

	@Test
	public void printResult() {
		try {
			log.info("Starting execution of printResult");
			String value = "";
			int[] location = { 1, 0, 0 };
			char val = 'S';
			Cell dest = new Cell(val, location);
			Labyrinth labyrinth = new Labyrinth();
			labyrinth.printResult(dest, value);
			assertTrue(true);

		} catch (Exception exception) {
			System.out.print("Exception in execution ofprintResult-" + exception);
			exception.printStackTrace();
			assertFalse(false);
		}
	}
	//Test to see if a destination is valid

	@Test
	public void isValidLocation() {
		try {
			log.info("Starting execution of isValidLocation");
			boolean expectedValue = false;
			int x = 0;
			int y = 0;
			int z = 0;
			Labyrinth labyrinth = new Labyrinth();
			boolean actualValue = labyrinth.isValidLocation(x, y, z);
			log.info("Expected Value=" + expectedValue + " . Actual Value=" + actualValue);
			System.out.println("Expected Value=" + expectedValue + " . Actual Value=" + actualValue);
			assertEquals(expectedValue, actualValue);

		} catch (Exception exception) {
			System.out.print("Exception in execution of execute1GetAllLogFromFirstMovF-" + exception);
			exception.printStackTrace();
			assertFalse(false);
		}
	}

	
	//Test to see if a Cell has been visited
	
	@Test
	public void setVisited() {
		try {
			log.info("Starting execution of setVisited");

			int[] location = { 1, 0, 0 };

			char value = '.';
			Cell labyrinth = new Cell(value, location);
			labyrinth.setVisited();
			assertTrue(true);

		} catch (Exception exception) {
			System.out.print("Exception in execution ofsetVisited-" + exception);
			exception.printStackTrace();
			assertFalse(false);
		}
	}

	//Test set the ParentCell

	@Test
	public void setParent() {
		try {
			log.info("Starting execution of setParent");
			int[] location_expected = { 0, 0, 0 };
			char value_expected = 'S';
			Cell parent = new Cell(value_expected, location_expected);
			int[] location = { 1, 0, 0 };
			char value = '.';
			Cell labyrinth = new Cell(value, location);
			labyrinth.setParent(parent);
			assertTrue(true);

		} catch (Exception exception) {
			System.out.println("Exception in execution ofsetParent-" + exception);
			exception.printStackTrace();
			assertFalse(false);
		}
	}
	//Test to see if a current cell is visited

	@Test
	public void isVisited() {
		try {
			log.info("Starting execution of isVisited");
			boolean expectedValue = false;
			int[] location = { 1, 0, 0 };
			char value = '.';
			Cell labyrinth = new Cell(value, location);
			boolean actualValue = labyrinth.isVisited();
			log.info("Expected Value=" + expectedValue + " . Actual Value=" + actualValue);
			System.out.println("Expected Value=" + expectedValue + " . Actual Value=" + actualValue);
			assertEquals(expectedValue, actualValue);

		} catch (Exception exception) {
			System.out.print("Exception in execution of execute1GetAllLogFromFirstMovF-" + exception);
			exception.printStackTrace();
			assertFalse(false);
		}
	}
	//Test to get the parentCell of the current cell

	@Test
	public void getParent() {
		try {
			log.info("Starting execution of getParent");

			int[] location_expected = { 0, 0, 0 };
			char value_expected = 'S';
			Cell expectedValue = new Cell(value_expected, location_expected);
			int[] location = { 1, 0, 0 };
			char value = '.';
			Cell labyrinth = new Cell(value, location);
			Cell actualValue = labyrinth.getParent();
			log.info("Expected Value=" + expectedValue + " . Actual Value=" + actualValue);
			System.out.println("Expected Value=" + expectedValue + " . Actual Value=" + actualValue);
			assertEquals(expectedValue, actualValue);

		} catch (Exception exception) {
			System.out.print("Exception in execution of execute1GetAllLogFromFirstMovF-" + exception);
			exception.printStackTrace();
			assertFalse(false);
		}
	}
}
