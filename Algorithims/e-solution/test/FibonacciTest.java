package test;
import static org.junit.jupiter.api.Assertions.*;

import java.math.BigInteger;

import org.junit.jupiter.api.Test;

import java.util.logging.Logger;
import e.solution.*;
public class FibonacciTest {

	private Logger log = Logger.getLogger(FibonacciTest.class.getName());

	@Test
	public void fibonacii() {
		try {
			log.info("Starting execution of fibonacii");
			BigInteger expectedValue = null;

			int n = 0;

			Fibonacci fibonacci = new Fibonacci();
			BigInteger actualValue = fibonacci.fibonacii(n);
			log.info("Expected Value=" + expectedValue + " . Actual Value=" + actualValue);
			System.out.println("Expected Value=" + expectedValue + " . Actual Value=" + actualValue);
			assertEquals(expectedValue, actualValue);

		} catch (Exception exception) {
			System.out.print("Exception in execution of execute1GetAllLogFromFirstMovF-" + exception);
			exception.printStackTrace();
			assertFalse(false);
		}
	}

	@Test
	public void randomNumb() {
		try {
			log.info("Starting execution of fibonacii");
			BigInteger expectedValue = BigInteger.valueOf(13);

			int n = 7;

			Fibonacci fibonacci = new Fibonacci();
			BigInteger actualValue = fibonacci.fibonacii(n);
			log.info("Expected Value=" + expectedValue + " . Actual Value=" + actualValue);
			System.out.println("Expected Value=" + expectedValue + " . Actual Value=" + actualValue);
			assertEquals(expectedValue, actualValue);

		} catch (Exception exception) {
			System.out.print("Exception in execution of execute1GetAllLogFromFirstMovF-" + exception);
			exception.printStackTrace();
			assertFalse(false);
		}
	}

	@Test
	public void main() {
		try {
			log.info("Starting execution of main");
			String[] args = null;

			Fibonacci fibonacci = new Fibonacci();
			fibonacci.main(args);
			assertTrue(true);

		} catch (Exception exception) {
			System.out.print("Exception in execution ofmain-" + exception);
			exception.printStackTrace();
			assertFalse(false);
		}
	}
}
