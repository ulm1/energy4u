# Energy4U

## Getting started

Willkommen Energy4U zu meinem Code. Um loszulegen, klonen Sie die Dateien einfach in Ihr Projektverzeichnis.

git clone https://gitlab.com/ulm1/energy4u.git

und öffnen Sie den Code mit dem Eclipse-Editor. Der Code für das Labyrinth und Fibonacci befindet sich im Ordner e/solutions. Ich möchte Ihnen danken, dass Sie sich die Zeit genommen haben, meinen Code zu testen. Wenn Sie Fragen haben, können Sie mich gerne unter ronald.agee@uni-ulm.de kontaktieren.

## Name
Program Code

## Description
This project contains a fast Fibonacci algorithm that calculates any Fibonacci number up to Fibonacci 500. The program was developed to be extremely fast and save on run time complexity. Furthermore the second project consists of a matrix which allows the user to find the shortest path.
The maze consists of uniform blocks of stone or air. It takes 1 minute to move one step north, south, west, east, up or down. Diagonal movement is not possible. Moreover, the maze is surrounded by stones on all sides. As input, the program receives a series of descriptions of mazes from which to escape. Each description starts with three integer values L, R, C (all <= 30). L is the number of levels of the maze.
R is the length of the maze, and C the width.Then follow L levels of the maze, each with R lines containing C characters.


# Labyrinth 

You are in a 3D labyrinth and want to get home as fast as possible. The maze consists of uniform blocks of stone or air. You need 1 minute to move one step north, south, west, east, up or down. Diagonal movement is not possible. Moreover, the maze is surrounded by stones on all sides.

Is it possible to escape, and if so, what is the minimum time you will be walking?

Input:

As input, your program receives a series of descriptions of mazes from which it is possible to escape. Each description starts with three integer values L, R, C (all <= 30).

L is the number of levels of the maze.
R is the length of the maze, and C is the width.

Next are L levels of the maze, each with R rows containing C characters. Each character represents a cuboid: '#' for stone, '.' for air. At its starting position is an 'S', and the exit is an 'E'. Each level is terminated by a single blank line. The input is terminated by three zeros for L, R, C.

The program should output exactly one line for each maze entered, in the form:

Escape in 11 minute(s)!
resp.
Caught :-(

if there is no way to escape.

Example input:
```
3 4 5
S....
.###.
.##..
###.#

#####
#####
##.##
##...

#####
#####
#.###
####E

1 3 3
S##
#E#
###

0 0 0

```

Example output:
<br/>
Escaped in 11 minute(s)! <br/>
Caught :-(

---------------

# Fibonacci numbers

The Fibonacci numbers (0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55 ...) are recursively defined by:
F(0) = 0 
F(1) = 1
F(i) = F(i-1) + F(i-2) for i >= 2

Write a program that computes Fibonacci numbers.

As input, your program is given a series of numbers less than or equal to 5000 (one number per line). For each of these numbers, your program should calculate the corresponding Fibonacci number and output it on one line.

Example input: <br/>

```
5 
7 
11 
```
Example output: <br/>
The Fibonacci number for 5 is: 5 <br/>
The Fibonacci number for 7 is: 13 <br/>
The Fibonacci number for 11 is: 89 <br/>

General conditions:
We test your program with a sequence of about 523 randomly selected numbers





## Installation
To get started, simply clone the files into your project directory.

git clone 

and open the code with the Eclipse editor. The code for the maze and Fibonacci is in the e/solutions folder.

## Usage
The maze program is terminated only when the value "0 0 0" is read, as specified in the requirements. The Fibonacci program is terminated when the user presses the Enter key on an empty line

## Support
If you have any questions, please feel free to contact me at ronald.agee@uni-ulm.de.

